<?php

 
// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */ 
/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter . By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *      'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *      'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *      array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *      array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
    'engine' => 'File',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
));

CakeLog::config('error', array(
    'engine' => 'File',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
));

CakePlugin::load('Barcode');
CakePlugin::load('CakePHPExcel', 
    array(
        'routes' => true
    )
);

CakePlugin::load('AclExtras');

CakePlugin::load('DebugKit');

CakePlugin::load('Acl', array('bootstrap' => true,'routes'=>true));

CakePlugin::load('Configuration');
CakePlugin::load('EmailTemplate',array('bootstrap' => true));
CakePlugin::load('Blog', array('bootstrap' => true,'routes'=>true));
CakePlugin::load('Newsletter',array('bootstrap' => true,'routes'=>true));
CakePlugin::load('Faq');
CakePlugin::load('Content',array('bootstrap' => true,'routes'=>true));

CakePlugin::load('Upload');

Configure::write('Users.roles', array(
    '1' => 'Administrator',
    '9' => 'Sales',
    '2' => 'Manager', 
    '3' => 'User', 
    '12' => 'HOS',
    '13' => 'HOD',
    '6' => 'Visitor'  
));

Configure::write('Users.defaultRole', '1');

Configure::write('Users.menus',
    array(
        '1' => array(
            'Dashboard' => array('url' => 'users/dashboard', 'ico' => 'home'), 
            'Reports' => array('url' => 'reports', 'ico' => 'line-chart', 'has_sub' => array(
                'Jobs In Progress' => 'reports/progress',  
                'Sales' => 'reports/sales',
                'Material Transfer' => 'reports/material_transfer',
                //'Warehouse Report' => 'reports/warehouse',
                'User Activity' => 'reports/index',
            )),  
            'Store' => array('url' => 'inventorystocks', 'ico' => 'cubes', 'has_sub' => array( 
                'Stock' => 'inventory_stocks/index',
                'Add Stock' => 'inventory_stocks/add', 
                'Stock Aging' => 'inventory_stocks/stock_aging', 
                'Request Material (MRN)' => 'inventory_material_requests',
                'Material Requested (MRN)' => 'inventory_material_requests/store',
                'Return Material (RMA)' => 'inventory_material_requests/returnindex',
                'Receive Material (RMA)' => 'inventory_material_requests/returnstore', 
                'Warehouse Received' => 'inventory_delivery_orders/index',
                'MRN Verification' => 'inventory_material_requests/verify',
                'Return Material (RMA)' => 'inventory_material_requests/returnindex',
                'Receive Material (RMA)' => 'inventory_material_requests/returnstore', 
                'Warehouse Received' => 'inventory_delivery_orders/index', 
                'History' => 'inventory_material_histories',
                'Transfer Item' => 'inventory_stocks/transfer',
                'Packaging' => 'finished_good_packagings',
                'Delivery Order / Notes' => 'finished_good_deliveries/store',
                'Warehouse Locations' => 'inventory_locations/index',
                'Stock Take' => 'inventory_stocks/stock_take',
                'Stock Take Verification' => 'inventory_stocks/stock_take_verify', 
                'Store Setting' => 'inventory_stores/index',
                'Rack Setting' => 'inventory_racks/index',
            )), 
            'Sales' => array('url' => 'customer', 'ico' => 'tags', 'has_sub' => array(
                'Customers' => 'customers/index',
                'Address' => 'customer_addresses/index',
                'Contact Persons' => 'customer_contact_persons/index',
                'Customer Enquiry/Tender' => 'sale_tenders/index',
                'Quotation' => 'sale_quotations/index',
                'Quotation Approval' => 'sale_quotations/approval',
                'Job (Awarded)' => 'sale_jobs/index',
                'Sales Order' => 'sale_orders/index',
                'Handing Over Document' => 'sale_handing_overs/index',
                'HOD Verification' => 'sale_handing_overs/approval',
                'Requisition For Invoicing' => 'sale_invoices',
                'Delivery Request' => 'finished_good_deliveries',
                'T&C Templates' => 'print_templates',
                'Sales Report' => 'sale_orders/report'   
            )),
            'STS' => array('url' => 'customer', 'ico' => 'tags', 'has_sub' => array( 
                'Quotation' => 'sts_quotations/index',
                'Quotation Approval' => 'sts_quotations/approval',
                'Job (Awarded)' => 'sts_sale_jobs/index',
                'Sales Order' => 'sts_sale_orders/index',
                'Handing Over Document' => 'sts_handing_overs/index',
                'HOD Verification' => 'sts_handing_overs/approval' 
            )),
            'Project' => array('url' => 'customer', 'ico' => 'tags', 'has_sub' => array( 
                'Quotation' => 'project_quotations/index',
                'Quotation Approval' => 'project_quotations/approval',
                'Job (Awarded)' => 'project_sale_jobs/index',
                'Sales Order' => 'project_sale_orders/index',
                'Handing Over Document' => 'project_handing_overs/index',
                'HOD Verification' => 'project_handing_overs/approval' 
            )),
            'Purchasing' => array('url' => 'project_schedules/index', 'ico' => 'shopping-bag', 'has_sub' => array(
                'Purchase Requisition' => 'inventory_purchase_requisitions/index',
                'PR Verification' => 'inventory_purchase_requisitions/verifyindex',
            )),
            'Procurement' => array('url' => 'procurement', 'ico' => 'podcast', 'has_sub' => array(
                'Purchase Requisition' => 'inventory_purchase_requisitions/procurementindex', 
                'Purchase Orders' => 'inventory_purchase_orders/create', 
                'Supplier' => 'inventory_suppliers/index',
                //'Supplier Address' => 'inventory_supplier_addresses/index',
                'Price Books' => 'inventory_supplier_items/index',
                'Price Book Required' => 'inventory_supplier_item_requireds',
                'Relevant Apporving Authority' => 'internal_lofas/index',
                'Term of Delivery' => 'term_of_deliveries/index',
            )),
            'Manufacturing' => array('url' => 'inventory_stocks', 'ico' => 'industry', 'has_sub' => array(
                'Production Order' => 'production_orders/index',  
                'Finished Goods' => 'finished_goods',
            )),
            'Inventory' => array('url' => 'inventory', 'ico' => 'cubes', 'has_sub' => array(
                'Item General' => 'inventory_items/index',
                'Items Category' => 'inventory_item_categories/index', 
                'Inventory Stock' => 'inventory_stocks/index', 
            )),
            'Planning' => array('url' => 'project_schedules/index', 'ico' => 'calendar', 'has_sub' => array(
                'Costing' => 'costings/index',
                'Costing Approval' => 'costings/approval',
                'M.P.S' => 'project_schedules/index', 
                'Job Budget' => 'project_budgets',
                'Plan Order (MRP)' => 'plannings/index', 
                'Plan Order Verification' => 'plannings/verification',
                'Plan Production' => 'production_orders/plan',
                'Currency Setting' => 'general_currencies/index',
            )), 
            'Engineering' => array('url' => 'boms/index', 'ico' => 'puzzle-piece', 'has_sub' => array(
                'B.O.M' => 'boms/index',
                'Add B.O.M' => 'boms/add',
                'Drawing' => 'project_drawings/index'
            )), 
            
            'Q.A.T' => array('url' => 'finished_goods', 'ico' => 'search', 'has_sub' => array(
                'Incoming Inspection' => 'inventory_qc_items/index',
                'Inspection & Testing' => 'finished_goods/submit_qc', 
                'F.A.T' => 'finished_goods/final_products',
                'Final Inspection' => 'finished_goods/final_inspection',
                'QAT Remark Status' => 'finished_good_comments/index',
            )),
            'Finance' => array('url' => 'account_department', 'ico' => 'money', 'has_sub' => array(
                'RFI' => 'sale_invoices/financeindex', 
                'Departments Budget' => 'account_department', 
                'Budget Request' => 'account_department_transfer_budget/index',
                'Transaction History' => 'account_department_budget_histories/index'
             )), 
            'Users' => array('url' => 'users', 'ico' => 'user', 'has_sub' => array(
                'Manage Groups' => 'admin/groups', 
                'Manage Users' => 'admin/users' 
            )), 
            'Settings' => array('url' => 'admin/settings', 'ico' => 'gears', 'has_sub' => array( 
                'Approval List' => 'approvals/index',
                'News' => 'news/index', 
                'Department' => 'internal_departments/index', 
                'Site Setting' => 'admin/configuration/configurations/prefix/Site',
                'Reading' => 'admin/configuration/configurations/prefix/Reading',
                'Writing' => 'admin/configuration/configurations/prefix/Writing',
                'Comment' => 'admin/configuration/configurations/prefix/Comment',
                'Social' => 'admin/configuration/configurations/prefix/Social',
                'RDX' => 'admin/configuration/configurations/prefix/CakeRDX',
                'Email Template' => 'admin/email_template/email_templates',
 
                'Business Category' => 'sale_order_categories/index',
 
                'Term Of Payments' => 'term_of_payments', 
                )),
            //'Email' => array('url' => 'admin/email_template/email_templates', 'ico' => 'envelope-o'),
            //'Content' => array('url' => 'admin/contents', 'ico' => 'book'),
             
            'Permissions' => array('url' => 'admin/acl/aros/group_permissions', 'ico' => 'users', 'has_sub' => array(
                'Group Permissions' => 'admin/acl/aros/group_permissions',
                'Edit Group Permission' => 'admin/acl/aros/edit_group_permissions',
                'User Permissions' => 'admin/acl/aros/user_permissions' 
                ))  
        )
    )
);


require_once('constant.php');
 