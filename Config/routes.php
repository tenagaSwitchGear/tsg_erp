<?php
 
 
	Router::connect('/admin', array('admin' => true, 'controller' => 'users', 'action' => 'dashboard'));

	Router::connect('/users', array('admin' => true, 'controller' => 'users', 'action' => 'dashboard'));
	
	// When user open base path, it will show dashboard, or redirect to login page
	Router::connect('/', array('controller' => 'users', 'action' => 'dashboard'));
	
	


	Router::connect('/sts_sale_orders/index', array('controller' => 'sale_orders', 'action' => 'index')); 
	Router::connect('/sts_sale_orders/add', array('controller' => 'sale_orders', 'action' => 'add')); 
	Router::connect('/sts_sale_orders/edit', array('controller' => 'sale_orders', 'action' => 'edit')); 
	Router::connect('/sts_sale_orders/view', array('controller' => 'sale_orders', 'action' => 'view'));
	
	Router::connect('/sts_handing_overs/index', array('controller' => 'sale_handing_overs', 'action' => 'index')); 
	Router::connect('/sts_handing_overs/approval', array('controller' => 'sale_handing_overs', 'action' => 'approval')); 
	Router::connect('/sts_handing_overs/index/*', array('controller' => 'sale_handing_overs', 'action' => 'index'));
	Router::connect('/sts_handing_overs/view/*', array('controller' => 'sale_handing_overs', 'action' => 'view')); 
	Router::connect('/sts_handing_overs/add', array('controller' => 'sale_handing_overs', 'action' => 'add'));

	Router::connect('/sts_sale_jobs/index', array('controller' => 'sale_jobs', 'action' => 'index'));
	Router::connect('/sts_sale_jobs/add', array('controller' => 'sale_jobs', 'action' => 'add'));
	Router::connect('/sts_sale_jobs/view/*', array('controller' => 'sale_jobs', 'action' => 'view'));





	Router::connect('/project_quotations/index', array('controller' => 'sale_quotations', 'action' => 'index'));
	Router::connect('/project_quotations/index/*', array('controller' => 'sale_quotations', 'action' => 'index'));
	Router::connect('/project_quotations/add', array('controller' => 'sale_quotations', 'action' => 'add'));
	Router::connect('/project_quotations/approval', array('controller' => 'sale_quotations', 'action' => 'approval'));
	Router::connect('/project_quotations/verification', array('controller' => 'sale_quotations', 'action' => 'verification')); 
	Router::connect('/project_quotations/view', array('controller' => 'sale_quotations', 'action' => 'view'));
	Router::connect('/project_quotations/edit', array('controller' => 'sale_quotations', 'action' => 'edit'));

	Router::connect('/project_sale_jobs/index', array('controller' => 'sale_jobs', 'action' => 'index'));
	Router::connect('/project_sale_jobs/add', array('controller' => 'sale_jobs', 'action' => 'add'));
	Router::connect('/project_sale_jobs/view', array('controller' => 'sale_jobs', 'action' => 'view'));
	Router::connect('/project_sale_jobs/edit', array('controller' => 'sale_jobs', 'action' => 'edit'));
	Router::connect('/project_handing_overs/index', array('controller' => 'sale_handing_overs', 'action' => 'index')); 
	Router::connect('/project_handing_overs/approval', array('controller' => 'sale_handing_overs', 'action' => 'approval'));
	Router::connect('/project_handing_overs/add', array('controller' => 'sale_handing_overs', 'action' => 'add'));
	Router::connect('/project_handing_overs/index/*', array('controller' => 'sale_handing_overs', 'action' => 'index'));
	Router::connect('/project_handing_overs/view/*', array('controller' => 'sale_handing_overs', 'action' => 'view')); 

	Router::connect('/project_sale_orders/index', array('controller' => 'sale_orders', 'action' => 'index')); 
	Router::connect('/project_sale_orders/add', array('controller' => 'sale_orders', 'action' => 'add')); 
	Router::connect('/project_sale_orders/edit', array('controller' => 'sale_orders', 'action' => 'edit')); 
	Router::connect('/project_sale_orders/view', array('controller' => 'sale_orders', 'action' => 'view'));
	
	
	//Router::connect('', array('admin' => true, 'controller' => 'users', 'action' => 'dashboard'));
	/**
	 * Here, we are connecting '/' (base path) to controller called 'Pages',
	 * its action called 'display', and we pass a param to select the view file
	 * to use (in this case, /app/View/Pages/home.ctp)...
	 */
	
	
	

	
	Router::mapResources('apis');
	Router::parseExtensions('json');
	Router::parseExtensions('pdf');
	Router::parseExtensions('csv');


/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */ 

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();



/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
