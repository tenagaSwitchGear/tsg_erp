


App::uses('BarcodeHelper','Vendor'); // Letak kat atas class name selepas App::uses('AppController', 'Controller');

 

$data_to_encode = 'ITEM-CODE-GRN-NO';
		    
$barcode = new BarcodeHelper();
        
// Generate Barcode data
$barcode->barcode();
$barcode->setType('C128');
$barcode->setCode($data_to_encode);
$barcode->setSize(80,200);
    
// Generate filename            
$random = rand(0,1000000);
$file = 'img/Barcodes/code_'.$random.'.png';
    
// Generates image file on server            
$barcode->writeBarcodeFile($file);