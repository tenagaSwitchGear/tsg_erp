<?php
App::uses('AppController', 'Controller');
/**
 * SaleHandingOverUsers Controller
 *
 * @property SaleHandingOverUser $SaleHandingOverUser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleHandingOverUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleHandingOverUser->recursive = 0;
		$this->set('saleHandingOverUsers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleHandingOverUser->exists($id)) {
			throw new NotFoundException(__('Invalid sale handing over user'));
		}
		$options = array('conditions' => array('SaleHandingOverUser.' . $this->SaleHandingOverUser->primaryKey => $id));
		$this->set('saleHandingOverUser', $this->SaleHandingOverUser->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleHandingOverUser->create();
			if ($this->SaleHandingOverUser->save($this->request->data)) {
				$this->Session->setFlash(__('The sale handing over user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale handing over user could not be saved. Please, try again.'));
			}
		}
		$users = $this->SaleHandingOverUser->User->find('list');
		$saleHandingOvers = $this->SaleHandingOverUser->SaleHandingOver->find('list');
		$this->set(compact('users', 'saleHandingOvers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleHandingOverUser->exists($id)) {
			throw new NotFoundException(__('Invalid sale handing over user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleHandingOverUser->save($this->request->data)) {
				$this->Session->setFlash(__('The sale handing over user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale handing over user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleHandingOverUser.' . $this->SaleHandingOverUser->primaryKey => $id));
			$this->request->data = $this->SaleHandingOverUser->find('first', $options);
		}
		$users = $this->SaleHandingOverUser->User->find('list');
		$saleHandingOvers = $this->SaleHandingOverUser->SaleHandingOver->find('list');
		$this->set(compact('users', 'saleHandingOvers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleHandingOverUser->id = $id;
		if (!$this->SaleHandingOverUser->exists()) {
			throw new NotFoundException(__('Invalid sale handing over user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleHandingOverUser->delete()) {
			$this->Session->setFlash(__('The sale handing over user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale handing over user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
