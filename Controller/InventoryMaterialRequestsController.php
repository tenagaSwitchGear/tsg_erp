<?php
App::uses('AppController', 'Controller');
/**
 * InventoryMaterialRequests Controller
 *
 * @property InventoryMaterialRequest $InventoryMaterialRequest
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryMaterialRequestsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	/*public function adminindex() {
		$role = $this->Session->read('Auth.User.group_id');
		if($role != 1) {
			$user_id = $this->Session->read('Auth.User.id');
			$conditions['ModelName.user_id'] = $user_id;
		} else {
			$conditions['ModelName.user_id !='] = 0;
		}
 
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'ModelName.id DESC','limit'=>$record_per_page);
		$this->set('variable', $this->Paginator->paginate());
	}*/

	public function beforeFilter() {
		parent::beforeFilter();
	}

/** 
 * index method
 *
 * @return void
 */
	public function index($status = null) {
		if($status == null) {
			$status = 1;
		} else {
			$status = $status; 
		}
		if(isset($_GET['search'])) { 
			if($_GET['ref'] != '') {
				$conditions['InventoryMaterialRequest.code LIKE'] = '%'.trim($_GET['ref']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['InventoryMaterialRequest.created >='] = $_GET['from'];
			} 
 			
 			if($_GET['to'] != '') {
				$conditions['InventoryMaterialRequest.created <='] = $_GET['to'];
			} 
 
			$this->request->data['InventoryMaterialRequest'] = $_GET;
		} 

		$user_id = $this->Session->read('Auth.User.id');
		$conditions['InventoryMaterialRequest.user_id'] = $user_id;
		$conditions['InventoryMaterialRequest.status'] = $status;
		$conditions['InventoryMaterialRequest.type'] = array(0, 1);
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'InventoryMaterialRequest.id DESC','limit'=>$record_per_page);
		$this->set('inventoryMaterialRequests', $this->Paginator->paginate());
	}

	public function returnstore($status = null) {
		if($status == null) {
			$status = 1;
		} else {
			$status = $status;
		}
		$user_id = $this->Session->read('Auth.User.id'); 
		$conditions['InventoryMaterialRequest.status'] = $status;
		$conditions['InventoryMaterialRequest.type'] = array(2);
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'InventoryMaterialRequest.id DESC','limit'=>$record_per_page);
		$this->set('inventoryMaterialRequests', $this->Paginator->paginate());
	}

	public function returnindex($status = null) {
		if($status == null) {
			$status = 1;
		} else {
			$status = $status;
		}

		if(isset($_GET['search'])) {
			if($_GET['from'] != '') {
				$conditions['InventoryMaterialRequest.created >='] = $_GET['from'];
			}

			if($_GET['to'] != '') {
				$conditions['InventoryMaterialRequest.created <='] = $_GET['to'];
			}

			if($_GET['name'] != '') {
				$conditions['InventoryMaterialRequest.code LIKE'] = '%'.$_GET['name'].'%';
			} 
			 
			$this->request->data['InventoryMaterialRequest'] = $_GET;
		} 

		$user_id = $this->Session->read('Auth.User.id');
		$conditions['InventoryMaterialRequest.user_id'] = $user_id;
		$conditions['InventoryMaterialRequest.status'] = $status;
		$conditions['InventoryMaterialRequest.type'] = array(2);
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'InventoryMaterialRequest.id DESC','limit'=>$record_per_page);
		$this->set('inventoryMaterialRequests', $this->Paginator->paginate());
	}



	public function returnitem() {
		if ($this->request->is('post')) {
			$this->InventoryMaterialRequest->create();

			$user_id = $this->Session->read('Auth.User.id');

			if($this->request->data['InventoryMaterialRequest']['production_order_id'] == '') {
				$this->request->data['InventoryMaterialRequest']['production_order_id'] = 0;
			}
			if($this->request->data['InventoryMaterialRequest']['sale_job_id'] == '') {
				$this->request->data['InventoryMaterialRequest']['sale_job_id'] = 0;
			} 

			$this->request->data['InventoryMaterialRequest']['sale_job_item_id'] = 0; 
			$this->request->data['InventoryMaterialRequest']['created'] = $this->date;
			$this->request->data['InventoryMaterialRequest']['require_date'] = $this->date;
			$this->request->data['InventoryMaterialRequest']['modified'] = $this->date;
			$this->request->data['InventoryMaterialRequest']['user_id'] = $user_id;
			$this->request->data['InventoryMaterialRequest']['type'] = 2;  
			$this->request->data['InventoryMaterialRequest']['status'] = 1; 

			$counter = $this->InventoryMaterialRequest->find('all', array(
				'conditions' => array(
					'InventoryMaterialRequest.type' => '2'
					),
				'recursive' => -1
				)); 
			$number = count($counter) + 1;
			$this->request->data['InventoryMaterialRequest']['code'] = $this->generate_code('RMA', $number);

			// Items 

			if(isset($this->request->data['item_id'])) {
				$item_id = $this->request->data['item_id'];
				$quantity = $this->request->data['quantity'];
				$general_unit_id = $this->request->data['general_unit_id'];  
				
				if ($this->InventoryMaterialRequest->save($this->request->data)) {
					$request_id = $this->InventoryMaterialRequest->getLastInsertId();

					$this->loadModel('InventoryMaterialRequestItem');

					$item = array();
					for($i = 0; $i < count($item_id); $i++) {
						$item[] = array(
							'inventory_material_request_id' => $request_id,
							'inventory_item_id' => $item_id[$i],
							'quantity' => $quantity[$i],
							'general_unit_id' => $general_unit_id[$i],
							'issued_quantity' => 0,
							'issued_balance' => 0,
							'created' => $this->date,
							'modified' => $this->date,
							'note' => '',
							'status' => 0,
							'type' => 1 // RMN
							);
						// Move to HOD Section. $this->reserve_item($item_id[$i], $quantity[$i]);
					}
					$this->InventoryMaterialRequestItem->saveAll($item, array('deep' => true)); 
					$this->Session->setFlash(__('The inventory material request has been saved.'), 'success');
					return $this->redirect(array('action' => 'returnindex'));
				} else {
					$this->Session->setFlash(__('The inventory material request could not be saved. Please, try again.'), 'error');
				} 
			} else {
				$this->Session->setFlash(__('Please add item.'), 'error');
			}
			
		}
		$productionOrders = $this->InventoryMaterialRequest->ProductionOrder->find('list');
		$saleJobs = $this->InventoryMaterialRequest->SaleJob->find('list');
		$saleJobItems = $this->InventoryMaterialRequest->SaleJobItem->find('list');
		$users = $this->InventoryMaterialRequest->User->find('list');
		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');
		$this->set(compact('productionOrders', 'saleJobs', 'saleJobItems', 'users', 'units'));
	} 

	public function verify($status = null) { 
		if($status == null) {
			$status = 1;
		} else {
			$status = $status;
		}
		if(isset($_GET['search'])) { 
			if($_GET['ref'] != '') {
				$conditions['InventoryMaterialRequest.code LIKE'] = '%'.trim($_GET['ref']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['InventoryMaterialRequest.created >='] = $_GET['from'];
			} 
 			
 			if($_GET['to'] != '') {
				$conditions['InventoryMaterialRequest.created <='] = $_GET['to'];
			} 
 			if($_GET['user'] != '') {
				$conditions['User.username LIKE'] = '%'.trim($_GET['user']).'%';
			}
			$this->request->data['InventoryMaterialRequest'] = $_GET;
		} 
		if(isset($_GET['ref'])) {
			$conditions['InventoryMaterialRequest.type'] = 2;
		} else {
			$conditions['InventoryMaterialRequest.type'] = array(0, 1);
		}
		$group = $this->Session->read('Auth.User.group_id');
		if($group != 1) {
			$this->loadModel('Approval');
			$approval = $this->Approval->find('all', array(
				'conditions' => array(
					'Approval.user_id' => $this->user_id,
					'Approval.name' => 'MRN'
					)
				)); 
			$appr = array();
			if($approval) {
				foreach ($approval as $app) {
					$appr[] = $app['Approval']['group_id'];
				}
				$conditions['User.group_id'] = $appr;
			} else {
				$conditions['User.group_id'] = 0;
			}
			
		}
		$conditions['InventoryMaterialRequest.status'] = $status;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'InventoryMaterialRequest.id DESC','limit'=>$record_per_page);
		$this->set('inventoryMaterialRequests', $this->Paginator->paginate());
	} 

	public function verifyview($id = null, $status = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request'));
		} 

		$group = $this->Session->read('Auth.User.group_id');
		if($group != 1) {
			$this->loadModel('Approval');
			$approval = $this->Approval->find('all', array(
				'conditions' => array(
					'Approval.user_id' => $this->user_id,
					'Approval.name' => 'MRN'
					)
				)); 
			if($approval) {
				$appr = array();
				foreach ($approval as $app) {
					$appr[] = $app['Approval']['group_id'];
				}
				$conditions['User.group_id'] = $appr;
				$options = array('conditions' => array(
					'InventoryMaterialRequest.' . $this->InventoryMaterialRequest->primaryKey => $id,
					'User.group_id' => $appr
					));
			} else {
				$this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
				return $this->redirect(array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('InventoryMaterialRequest.' . $this->InventoryMaterialRequest->primaryKey => $id));
		}

		$mrn = $this->InventoryMaterialRequest->find('first', $options);

		/* Cancel MRN */
		if($status != null) {
			if($status == 7) {
				$this->request->data['InventoryMaterialRequest']['id'] = $id;
				$this->request->data['InventoryMaterialRequest']['status'] = 7;
				if($this->InventoryMaterialRequest->save($this->request->data)) {
 
					$name = 'Cancelled MRN : ' . $mrn['InventoryMaterialRequest']['code'];
					$link = 'inventory_material_requests/view/'.$id;
					$type = 'Cancelled MRN';
					
					$this->insert_log($this->user_id, $name, $link, $type);

					$this->Session->setFlash(__('MRN Has been cancelled.'), 'success');
					return $this->redirect(array('action' => 'verify'));
				}
			}
		}

		if ($this->request->is('post')) {  

			$this->InventoryMaterialRequest->id = $id; 
			if($this->InventoryMaterialRequest->save($this->request->data)) {
				$this->loadModel('InventoryMaterialRequestVerification');
				$data = array(
					'inventory_material_request_id' => $id,
					'user_id' => $this->Session->read('Auth.User.id'),
					'note' => $this->request->data['InventoryMaterialRequestVerification']['note'],
					'created' => $this->date,
					'status' => $this->request->data['InventoryMaterialRequest']['status']
					);

				// Array
				$item = $_POST['item_id'];
				$quantity = $_POST['quantity'];

				if($this->InventoryMaterialRequestVerification->save($data)) {

					// Reserve items if verified
					if($this->request->data['InventoryMaterialRequest']['status'] == 2) {
						for($i = 0; $i < count($item); $i++) {

							$this->reserve_item($item[$i], $quantity[$i]);
						}	
					}
					//print_r($_POST);
					
					$this->Session->setFlash(__('The material request status has been saved.'), 'success');
					return $this->redirect(array('action' => 'verify'));
				} else {
					$this->Session->setFlash(__('The material request status cannot be saved.'), 'error');
				}
			} 
		}

		
		$this->set('inventoryMaterialRequest', $mrn);

		$items = $this->InventoryMaterialRequest->InventoryMaterialRequestItem->find('all', array('conditions' => array(
			'InventoryMaterialRequestItem.inventory_material_request_id' => $id
			)));
		$data = array();
		foreach ($items as $item) {
			$data[] = array(
				'InventoryItem' => $item['InventoryItem'],
				'GeneralUnit' => $item['GeneralUnit'],
				'InventoryMaterialRequestItem' => $item['InventoryMaterialRequestItem'],
				'InventoryMaterialRequest' => $item['InventoryMaterialRequest'],
				'ProductionOrderChild' => $item['ProductionOrderChild'],
				'ProductionOrderItem' => $item['ProductionOrderItem'],
				'Stock' => $this->sum_stock_balance($item['InventoryMaterialRequestItem']['inventory_item_id']),
				'Demand' => $this->sum_demand($item['InventoryMaterialRequestItem']['inventory_item_id'], $id) 
				);
		}
		$this->set('items', $data); 

		// Verification history
		$this->loadModel('InventoryMaterialRequestVerification');

		$verifications = $this->InventoryMaterialRequestVerification->find('all', array(
			'conditions' => array(
				'InventoryMaterialRequestVerification.inventory_material_request_id' => $id
				)
			));
		$this->set('verifications', $verifications);
	}
 

	public function store($status = null) { 
		if($status == null) {
			$stat = 2;
		} else {
			$stat = $status;
		}
		if(isset($_GET['search'])) { 
			if($_GET['ref'] != '') {
				$conditions['InventoryMaterialRequest.code LIKE'] = '%'.trim($_GET['ref']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['InventoryMaterialRequest.created >='] = $_GET['from'];
			} 
 			
 			if($_GET['to'] != '') {
				$conditions['InventoryMaterialRequest.created <='] = $_GET['to'];
			} 
			if($_GET['user'] != '') {
				$conditions['User.username LIKE'] = '%'.trim($_GET['user']).'%';
			}
 
			$this->request->data['InventoryMaterialRequest'] = $_GET;
		} 
		$conditions['InventoryMaterialRequest.status'] = $stat;
		$conditions['InventoryMaterialRequest.type'] = array(0, 1);
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'InventoryMaterialRequest.id DESC','limit'=>$record_per_page);
		$this->set('inventoryMaterialRequests', $this->Paginator->paginate());
	} 

	private function update_item($request_id, $user_id, $store_pic, $inv_item_id, $req_item_id, $qty, $mrn_no) {
		$this->loadModel('InventoryMaterialRequestItem');

		$req_item = $this->InventoryMaterialRequestItem->find('first', array(
				'conditions' => array(
					'InventoryMaterialRequestItem.id' => $req_item_id
					)
				));

		//if($qty <= $req_item['InventoryMaterialRequestItem']['quantity'] && $qty > 0) { 
		if($qty <= $req_item['InventoryMaterialRequestItem']['quantity']) {
			$this->loadModel('ProductionOrder');
			$prod = $this->ProductionOrder->find('first', array(
				'conditions' => array(
					'ProductionOrder.id' => $req_item['InventoryMaterialRequest']['production_order_id']
					)
				));	
			if($prod) {
				$this->update_stock($inv_item_id, $qty, $req_item_id, $req_item['InventoryMaterialRequest']['production_order_id'], $prod['ProductionOrder']['sale_job_childs_id'], $prod['ProductionOrder']['sale_job_items_id']);
			} else {
				$this->update_stock($inv_item_id, $qty, $req_item_id, 0, 0, 0);
			}
			
			
			$issued_quantity = $req_item['InventoryMaterialRequestItem']['issued_quantity'] + $qty; 
			$issued_balance  = $req_item['InventoryMaterialRequestItem']['quantity'] - $issued_quantity; 

			$update_bal = $this->InventoryMaterialRequestItem->updateAll(                     
				    array(
				    	'InventoryMaterialRequestItem.issued_quantity' =>  "'".$issued_quantity."'",
				    	'InventoryMaterialRequestItem.issued_balance' =>  "'".$issued_balance."'" 
				    	),
				    array(
				    	'InventoryMaterialRequestItem.id' =>  $req_item_id
				    	)
				);	

			if(!$update_bal) {
				die('310');
			}

			/* Production Order Item (Update issued Qty) */
			if($req_item['InventoryMaterialRequestItem']['production_order_item_id'] != 0) {
				$this->loadModel('ProductionOrderItem');
				$pdo_item = $this->ProductionOrderItem->find('first', array(
					'conditions' => array(
						'ProductionOrderItem.id' => $req_item['InventoryMaterialRequestItem']['production_order_item_id']
						) 
					));

				$pdo_issued_qty = $pdo_item['ProductionOrderItem']['quantity_issued'] + $qty;
				$pdo_issued_balance = $pdo_item['ProductionOrderItem']['quantity_bal'] - $qty;

				$this->ProductionOrderItem->updateAll(                     
				    array(
				    	'ProductionOrderItem.quantity_issued' =>  "'$pdo_issued_qty'",
				    	'ProductionOrderItem.quantity_bal' =>  "'$pdo_issued_balance'" 
				    	),
				    array(
				    	'ProductionOrderItem.id' =>  $pdo_item['ProductionOrderItem']['id']
				    	)
				);	
			} 
			

			$quantity = $req_item['InventoryItem']['quantity'] - $qty;
			$quantity_available = $req_item['InventoryItem']['quantity_available'] - $qty;
			$quantity_reserved = $req_item['InventoryItem']['quantity_reserved'] - $qty;

			// Update general item
			$this->loadModel('InventoryItem');
			$this->InventoryItem->updateAll(                     
				    array(
				    	'InventoryItem.quantity' => "'$quantity'",
				    	'InventoryItem.quantity_available' =>  "'$quantity_available'",
				    	'InventoryItem.quantity_reserved' =>  "'$quantity_reserved'"
				    	),
				    array(
				    	'InventoryItem.id' =>  $inv_item_id
				    	)
				);	

			$this->loadModel('InventoryStock');
			$stock = $this->InventoryStock->find('first', array(
				'conditions' => array(
					'InventoryStock.inventory_item_id' => $inv_item_id,
					'InventoryStock.expiry_date >=' => date('Y-m-d')
					),
				'order' => array(
					'InventoryStock.created' => 'ASC'
					),
				'recursive' => -1
				));
			$sum_balance = $this->sum_stock_balance($inv_item_id);
			if($sum_balance >= $qty && $qty > 0) {
				// Insert history
				$this->loadModel('InventoryMaterialHistory'); 
				$this->InventoryMaterialHistory->create();
				$item = array(
					'reference' => $mrn_no,
					'user_id' => $user_id, 
					'store_pic' => $store_pic,
					'inventory_item_id' => $inv_item_id,
					'inventory_stock_id' => !empty($stock) ? $stock['InventoryStock']['id'] : 0,
					'quantity' => $req_item['InventoryMaterialRequestItem']['quantity'], // Req qty
					'issued_quantity' => $qty,
					'quantity_balance' => $this->sum_stock_balance($inv_item_id),
					'inventory_material_request_id' => $request_id,
					'inventory_material_request_item_id' => $req_item_id,
					'created' => $this->date,
					'status' => 1,
					'type' => 1, // 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 5: RMA, 6: grn, 
					'transfer_type' => 'Out'
					);
				$this->InventoryMaterialHistory->save($item);	
			}
			
		}   
	}

	private function update_child($request_id, $user_id, $store_pic, $inv_item_id, $req_item_id, $qty, $mrn_no, $prod_order_child_id) {
		$this->loadModel('InventoryMaterialRequestItem');

		$req_item = $this->InventoryMaterialRequestItem->find('first', array(
				'conditions' => array(
					'InventoryMaterialRequestItem.id' => $req_item_id
					)
				));

		//if($qty <= $req_item['InventoryMaterialRequestItem']['quantity'] && $qty > 0) { 
		if($qty <= $req_item['InventoryMaterialRequestItem']['quantity']) {
			$this->loadModel('ProductionOrder');
			$prod = $this->ProductionOrder->find('first', array(
				'conditions' => array(
					'ProductionOrder.id' => $req_item['InventoryMaterialRequest']['production_order_id']
					)
				));	
			if($prod) {
				$this->update_stock($inv_item_id, $qty, $req_item_id, $req_item['InventoryMaterialRequest']['production_order_id'], $prod['ProductionOrder']['sale_job_childs_id'], $prod['ProductionOrder']['sale_job_items_id']);
			} else {
				$this->update_stock($inv_item_id, $qty, $req_item_id, 0, 0, 0);
			}
			
			
			$issued_quantity = $req_item['InventoryMaterialRequestItem']['issued_quantity'] + $qty; 
			$issued_balance  = $req_item['InventoryMaterialRequestItem']['quantity'] - $issued_quantity; 

			$update_bal = $this->InventoryMaterialRequestItem->updateAll(                     
				    array(
				    	'InventoryMaterialRequestItem.issued_quantity' =>  "'".$issued_quantity."'",
				    	'InventoryMaterialRequestItem.issued_balance' =>  "'".$issued_balance."'" 
				    	),
				    array(
				    	'InventoryMaterialRequestItem.id' =>  $req_item_id
				    	)
				);	

			if(!$update_bal) {
				die('310');
			}

			/* Production Order Item (Update issued Qty) */
			if($req_item['InventoryMaterialRequestItem']['production_order_child_id'] != 0) {
				$this->loadModel('ProductionOrderChild');
				$pdo_item = $this->ProductionOrderChild->find('first', array(
					'conditions' => array(
						'ProductionOrderChild.id' => $prod_order_child_id
						) 
					));

				$pdo_issued_qty = $pdo_item['ProductionOrderChild']['issued_qty'] + $qty;
				$pdo_issued_balance = $pdo_item['ProductionOrderChild']['quantity_bal'] - $qty;

				$this->ProductionOrderChild->updateAll(                     
				    array(
				    	'ProductionOrderChild.issued_qty' =>  "'$pdo_issued_qty'",
				    	'ProductionOrderChild.quantity_bal' =>  "'$pdo_issued_balance'" 
				    	),
				    array(
				    	'ProductionOrderChild.id' =>  $pdo_item['ProductionOrderChild']['id']
				    	)
				);	
			} 
			

			$quantity = $req_item['InventoryItem']['quantity'] - $qty;
			$quantity_available = $req_item['InventoryItem']['quantity_available'] - $qty;
			$quantity_reserved = $req_item['InventoryItem']['quantity_reserved'] - $qty;

			// Update general item
			$this->loadModel('InventoryItem');
			$this->InventoryItem->updateAll(                     
				    array(
				    	'InventoryItem.quantity' => "'$quantity'",
				    	'InventoryItem.quantity_available' =>  "'$quantity_available'",
				    	'InventoryItem.quantity_reserved' =>  "'$quantity_reserved'"
				    	),
				    array(
				    	'InventoryItem.id' =>  $inv_item_id
				    	)
				);	

			$this->loadModel('InventoryStock');
			$stock = $this->InventoryStock->find('first', array(
				'conditions' => array(
					'InventoryStock.inventory_item_id' => $inv_item_id 
					),
				'order' => array(
					'InventoryStock.created' => 'ASC'
					),
				'recursive' => -1
				));
			$sum_balance = $this->sum_stock_balance($inv_item_id);
			if($sum_balance >= $qty && $qty > 0) {
				// Insert history
				$this->loadModel('InventoryMaterialHistory'); 
				$this->InventoryMaterialHistory->create();
				$item = array(
					'reference' => $mrn_no,
					'user_id' => $user_id, 
					'store_pic' => $store_pic,
					'inventory_item_id' => $inv_item_id,
					'inventory_stock_id' => !empty($stock) ? $stock['InventoryStock']['id'] : 0,
					'quantity' => $req_item['InventoryMaterialRequestItem']['quantity'], // Req qty
					'issued_quantity' => $qty,
					'quantity_balance' => $this->sum_stock_balance($inv_item_id),
					'inventory_material_request_id' => $request_id,
					'inventory_material_request_item_id' => $req_item_id,
					'created' => $this->date,
					'status' => 1,
					'type' => 1, // 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 5: RMA, 6: grn, 
					'transfer_type' => 'Out'
					);
				$this->InventoryMaterialHistory->save($item);	
			}
			
		}   
	}

	private function sum_stock_balance($inventory_item_id) {
		$this->loadModel('InventoryStock');
		$stocks = $this->InventoryStock->find('all', array(
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $inventory_item_id 
				),
			'fields' => array(
				'SUM(InventoryStock.quantity) AS quantity' 
				),
			'recursive' => -1
			));
		foreach ($stocks as $stock) {
			return $stock[0]['quantity'];
		}
	}

	private function close_mrn($id) {
		$this->loadModel('InventoryMaterialRequestItem');

		$items = $this->InventoryMaterialRequestItem->find('all', array(
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_material_request_id' => $id,
				'InventoryMaterialRequestItem.issued_balance !=' => 0,
				),
			'recursive' => -1
			));

		if(count($items) == 0) {
			// Close mrn = 4
			$status = 4;
		} else {
			// Partial = 5
			$status = 5;
		}

		$this->loadModel('InventoryMaterialRequest');
		$this->InventoryMaterialRequest->updateAll(                     
			    array(
			    	'InventoryMaterialRequest.status' => "'$status'"
			    	),
			    array(
			    	'InventoryMaterialRequest.id' =>  $id
			    	)
			);	
	}

	private function update_stock($item_id, $quantity, $request_item_id, $production_order_id = 0, $job_child_id = 0, $sale_job_item_id = 0) {
		//Input variables 
	    //Output variables
	    if($quantity > 0) {
	    	$totalcost = 0;
		    $qtyout = 0; 
		    
	 		$this->loadModel('InventoryStock');
	 		$this->loadModel('InventoryItemIssueCost');
	 		$stocks = $this->InventoryStock->find('all', array(
	 			'conditions' => array(
	 				'InventoryStock.inventory_item_id' => $item_id,
	 				'InventoryStock.quantity >' => 0
	 				),
	 			'order' => array(
	 				'InventoryStock.id' => 'ASC'
	 				)
	 			));
	 		foreach ($stocks as $stock) {
	 			if($quantity > 0) {
	                $batchout = 0;
	                $rem = max($stock['InventoryStock']['quantity'] - $quantity, 0);
	                if($rem == 0)
	                    $batchout = $stock['InventoryStock']['quantity']; //This means there are no items of this cost remaining
	                else
	                    $batchout = $quantity; //This means there are items remaining and therefore our next loop (within the while) will check for the next expensive item

	                $totalcost += ($batchout * $stock['InventoryStock']['price_per_unit']);
	                $quantity -= $batchout;
	                $qtyout += $batchout;
	                
	                //$mysqli->query($sql);
	                $balance = $stock['InventoryStock']['quantity'] - $batchout;
	                $issued_quantity = $stock['InventoryStock']['issued_quantity'] + $batchout;
	                $this->InventoryStock->updateAll(
	                	array(
	                		'InventoryStock.quantity' => "'$balance'",
	                		'InventoryStock.issued_quantity' => "'$issued_quantity'"
	                		),
	                	array(
	                		'InventoryStock.id' => $stock['InventoryStock']['id']
	                		)
	                	); 
	            }
	 		}
	 		$cost = array(
	                	'inventory_material_request_item_id' => $request_item_id,
	                	'production_order_id' => $production_order_id,
	                	'created' => $this->date,
	                	'amount' => $totalcost,
	                	'inventory_item_id' => $item_id,
	                	'sale_job_child_id' => $job_child_id,
	                	'sale_job_item_id' => $sale_job_item_id
	                	);
	 		$this->InventoryItemIssueCost->create();
	 		$this->InventoryItemIssueCost->save($cost);
	    } 
	}

	public function printmrn($store = null) { 
		$this->layout = 'print'; 
		$conditions = array();

		//$conditions['InventoryMaterialRequestItem.status'] = 0; 
		$conditions['InventoryMaterialRequest.type'] = array(0, 1); 
 		if(isset($_GET['search'])) { 
 			if($_GET['name'] != '') {
 				$name = (int)$_GET['name'];
 				$conditions['InventoryItem.name LIKE'] = '%'.$name.'%'; 
 			}
 			if($_GET['code'] != '') {
 				$code = $_GET['code'];
 				$conditions['InventoryItem.code LIKE'] = '%'.$code.'%'; 
 			}
 			if($_GET['inventory_store_id'] != '') {
 				$store = (int)$_GET['inventory_store_id'];
 				$conditions['InventoryStock.inventory_store_id'] = $store; 
 			}
 			$this->request->data['InventoryMaterialRequestItem'] = $_GET;
 		}

 		$this->set('store', $store);	 

 		$this->loadModel('InventoryMaterialRequestItem');
 		$record_per_page = Configure::read('Reading.nodes_per_page');
 
 		$this->Paginator->settings = array(
			'joins' => array( 
		        array(
		            'table' => 'inventory_stocks',
		            'alias' => 'InventoryStock', 
		            'type' => 'LEFT',
		            'foreignKey' => FALSE,
		            'conditions' => 'InventoryStock.inventory_item_id = InventoryMaterialRequestItem.inventory_item_id' 
		        ),
		        array(
		            'table' => 'inventory_stores',
		            'alias' => 'InventoryStore', 
		            'type' => 'LEFT',
		            'foreignKey' => FALSE,
		            'conditions' => 'InventoryStock.inventory_store_id = InventoryStore.id' 
		        )
		    ), 
		    'fields' => array( 
		    	'InventoryStock.*',
		    	'InventoryStore.*',
		    	'InventoryItem.*',
		    	'GeneralUnit.*',
		    	'InventoryMaterialRequestItem.*', 
		    	'InventoryMaterialRequest.*',
		    	),
			'conditions' => $conditions, 
			'contain' => array( 
				'InventoryItem',
				'GeneralUnit' 
				),
			'group' => 'InventoryMaterialRequestItem.id',
			'limit' => 1000,
			'order' => 'InventoryStock.rack ASC'
			); 
 		$dataitems = $this->Paginator->paginate('InventoryMaterialRequestItem');
 		$items = array();
 		foreach ($dataitems as $data) {
 			$items[] = array(  
 				'InventoryStock' => $data['InventoryStock'],
 				'InventoryStore' => $data['InventoryStore'],
 				'InventoryItem' => $data['InventoryItem'], 
 				'InventoryMaterialRequestItem' => $data['InventoryMaterialRequestItem'],
 				'StockBalance' => $this->sum_stock($data['InventoryMaterialRequestItem']['inventory_item_id']),
 				'GeneralUnit' => $data['GeneralUnit'],
 				'InventoryMaterialRequest' => $data['InventoryMaterialRequest'], 
 				);
 		} 

		$this->set('items', $items); 

		$this->loadModel('inventoryStore');

		$stores = $this->inventoryStore->find('list');

		$this->loadModel('inventoryLocation');
		$locations = $this->inventoryLocation->find('list');

		$this->set(compact('stores', 'locations'));
	}

	public function itemlist($store = null) { 

		$conditions = array();

		//$conditions['InventoryMaterialRequestItem.status'] = 0; 
		$conditions['InventoryMaterialRequest.type'] = array(0, 1); 
 		if(isset($_GET['search'])) { 
 			if($_GET['name'] != '') {
 				$name = (int)$_GET['name'];
 				$conditions['InventoryItem.name LIKE'] = '%'.$name.'%'; 
 			}
 			if($_GET['code'] != '') {
 				$code = $_GET['code'];
 				$conditions['InventoryItem.code LIKE'] = '%'.$code.'%'; 
 			}
 			if($_GET['inventory_store_id'] != '') {
 				$store = (int)$_GET['inventory_store_id'];
 				$conditions['InventoryStock.inventory_store_id'] = $store; 
 			}
 			$this->request->data['InventoryMaterialRequestItem'] = $_GET;
 		}

 		$this->set('store', $store);	 

 		$this->loadModel('InventoryMaterialRequestItem');
 		$record_per_page = Configure::read('Reading.nodes_per_page');
 
 		$this->Paginator->settings = array(
			'joins' => array( 
		        array(
		            'table' => 'inventory_stocks',
		            'alias' => 'InventoryStock', 
		            'type' => 'LEFT',
		            'foreignKey' => FALSE,
		            'conditions' => 'InventoryStock.inventory_item_id = InventoryMaterialRequestItem.inventory_item_id' 
		        ),
		        array(
		            'table' => 'inventory_stores',
		            'alias' => 'InventoryStore', 
		            'type' => 'LEFT',
		            'foreignKey' => FALSE,
		            'conditions' => 'InventoryStock.inventory_store_id = InventoryStore.id' 
		        )
		    ), 
		    'fields' => array( 
		    	'InventoryStock.*',
		    	'InventoryStore.*',
		    	'InventoryItem.*',
		    	'GeneralUnit.*',
		    	'InventoryMaterialRequestItem.*', 
		    	'InventoryMaterialRequest.*',
		    	),
			'conditions' => $conditions, 
			'contain' => array( 
				'InventoryItem',
				'GeneralUnit' 
				),
			'group' => 'InventoryMaterialRequestItem.id',
			'limit' => $record_per_page
			); 
 		$dataitems = $this->Paginator->paginate('InventoryMaterialRequestItem');
 		$items = array();
 		foreach ($dataitems as $data) {
 			$items[] = array(  
 				'InventoryStock' => $data['InventoryStock'],
 				'InventoryStore' => $data['InventoryStore'],
 				'InventoryItem' => $data['InventoryItem'], 
 				'InventoryMaterialRequestItem' => $data['InventoryMaterialRequestItem'],
 				'StockBalance' => $this->sum_stock($data['InventoryMaterialRequestItem']['inventory_item_id']),
 				'GeneralUnit' => $data['GeneralUnit'],
 				'InventoryMaterialRequest' => $data['InventoryMaterialRequest'], 
 				);
 		} 

		$this->set('items', $items); 

		$this->loadModel('inventoryStore');

		$stores = $this->inventoryStore->find('list');

		$this->loadModel('inventoryLocation');
		$locations = $this->inventoryLocation->find('list');

		$this->set(compact('stores', 'locations'));
	}

	public function edititem($mrn_id = null, $item_id = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid MRN'));
		} 
	}

	public function storeview($id = null, $store = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid MRN'));
		} 

		$options = array('conditions' => array('InventoryMaterialRequest.' . $this->InventoryMaterialRequest->primaryKey => $id));
		$request = $this->InventoryMaterialRequest->find('first', $options);

		if ($this->request->is('post')) { 

			$this->loadModel('User');
			$user = $this->User->find('first', array(
				'conditions' => array(
					'User.id' => $request['InventoryMaterialRequest']['user_id']
					),
				'recursive' => -1
				));

			if(isset($_POST['notification'])) {
				$this->InventoryMaterialRequest->id = $id;
				$this->request->data['InventoryMaterialRequest']['status'] = 3;
				if($this->InventoryMaterialRequest->save($this->request->data)) {
					
					// Send email send_email($to, $subject, $template, $data = array())
					$data = array(
						'to' => $user['User']['email'],
						'subject' => $request['InventoryMaterialRequest']['code'],
						'template' => 'store-item-ready',
						'content' => array(
							'username' => $user['User']['username'],
							'mrn_number' => $request['InventoryMaterialRequest']['code'],
							'note' => $this->request->data['InventoryMaterialRequest']['store_note']
							)
						);
					$this->send_email($data);
					$this->Session->setFlash(__('Notification has been sent.'), 'success');
					return $this->redirect(array('action' => 'store'));
				}
			} 

			if(isset($_POST['issued'])) {
				// Issued item
				$item = $_POST['item_id'];
				$qty = $_POST['issued_quantity'];
				$req_qty = $_POST['request_quantity'];
				$req_item_id = $_POST['req_item_id'];
				$production_order_child_id = $_POST['production_order_child_id']; 

				$this->loadModel('InventoryMaterialHistory'); 
				$store_pic = $this->Session->read('Auth.User.id');

				for($i = 0; $i < count($item); $i++) { 
					// Issued only filled quantity 
					if($qty[$i] != '' || $qty[$i] != 0 && $qty[$i] <= $req_qty[$i]) {
						// Deduct items reserved & qty
						if($production_order_child_id == 0) {
							// Item
							$this->update_item($id, $request['InventoryMaterialRequest']['user_id'], $store_pic, $item[$i], $req_item_id[$i], $qty[$i], $request['InventoryMaterialRequest']['code']); 
						} else {
							// Child
							$this->update_child($id, $request['InventoryMaterialRequest']['user_id'], $store_pic, $item[$i], $req_item_id[$i], $qty[$i], $request['InventoryMaterialRequest']['code'], $production_order_child_id); 
						}
						
					} 
				}
				// count each request item balance, if still exist then go to 'Partials' status
				$this->close_mrn($id);
				$this->Session->setFlash(__('The material request has been saved.'), 'success');
				return $this->redirect(array('action' => 'store/3')); 
			}
		} 

		$this->set('inventoryMaterialRequest', $request);

		$conditions = array();

		$conditions['InventoryMaterialRequestItem.inventory_material_request_id'] = $id; 

 		if(isset($store)) { 
 			$store = (int)$store;
 			$conditions['InventoryItem.inventory_store_id'] = $store; 
 		}

 		$this->set('store', $store);	 

 		$this->loadModel('InventoryMaterialRequestItem');

 		$dataitems = $this->InventoryMaterialRequestItem->find('all', array(
			'joins' => array(
		        array(
		            'table' => 'inventory_stores',
		            'alias' => 'InventoryStore', 
		            'type' => 'LEFT',
		            'foreignKey' => FALSE,
		            'conditions' => 'InventoryItem.inventory_store_id = InventoryStore.id' 
		        ) 
		    ), 
		    'fields' => array( 
		    	'InventoryStore.*',
		    	'InventoryItem.*',
		    	'GeneralUnit.*',
		    	'InventoryMaterialRequestItem.*', 
		    	),
			'conditions' => $conditions, 
			'contain' => array( 
				'InventoryItem',
				'GeneralUnit' 
				),
			//'recursive' => -1
			)); 

 		$items = array();
 		foreach ($dataitems as $data) {
 			$items[] = array(
 				'InventoryStore' => $data['InventoryStore'],
 				'InventoryItem' => $data['InventoryItem'], 
 				'InventoryMaterialRequestItem' => $data['InventoryMaterialRequestItem'],
 				'StockBalance' => $this->sum_stock($data['InventoryMaterialRequestItem']['inventory_item_id']),
 				'GeneralUnit' => $data['GeneralUnit'],
 				'SumDemand' => $this->sum_demand($data['InventoryItem']['id'], $id)
 				);
 		} 

		$this->set('items', $items); 

		$this->loadModel('InventoryStore');

		$stores = $this->InventoryStore->find('list');

		$this->loadModel('InventoryLocation');
		$locations = $this->InventoryLocation->find('list');

		$this->set(compact('stores', 'locations'));
	} 

	private function sum_demand($item_id, $mrn_id) {
		$this->loadModel('InventoryMaterialRequestItem');
		$demands = $this->InventoryMaterialRequestItem->find('all', array(
			'fields' => array(
				'SUM(InventoryMaterialRequestItem.issued_balance) AS demand'
				),
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_item_id' => $item_id,
				'InventoryMaterialRequest.type' => array(0, 1),
				'InventoryMaterialRequest.status !=' => 4,
				'InventoryMaterialRequest.id !=' => $mrn_id
				)
			));
		if($demands) {
			foreach ($demands as $demand) { 
				return $demand[0]['demand']; 
			}
		} else {
			return 0;
		}
	}

	private function sum_stock($item_id) {
		$this->loadModel('InventoryStock');
		$stocks = $this->InventoryStock->find('all', array(
			'fields' => array(
				'SUM(InventoryStock.quantity) AS balance'
				),
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $item_id
				)
			));
		if($stocks) {
			foreach ($stocks as $stock) {
				return $stock[0]['balance'];
			}	
		} else {
			return 0;
		}
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request'));
		}

		$group = $this->Session->read('Auth.User.group_id');  
		$user_id = $this->Session->read('Auth.User.id');  
		if($group == 1 || $group == 13 || $group == 24) { 
			
	 	} else {
	 		$conditions['InventoryMaterialRequest.user_id'] = $user_id;
	 	}  

	 	$conditions['InventoryMaterialRequest.id'] = $id;
		$options = array('conditions' => $conditions);
		$this->set('inventoryMaterialRequest', $this->InventoryMaterialRequest->find('first', $options));

		$items = $this->InventoryMaterialRequest->InventoryMaterialRequestItem->find('all', array('conditions' => array(
			'InventoryMaterialRequestItem.inventory_material_request_id' => $id
			)));
		$this->set('items', $items);

		$this->loadModel('InventoryMaterialRequestVerification');

		$verifications = $this->InventoryMaterialRequestVerification->find('all', array(
			'conditions' => array(
				'InventoryMaterialRequestVerification.inventory_material_request_id' => $id
				)
			));
		$this->set('verifications', $verifications);
	}

	public function returnview($id = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request'));
		}
		$group = $this->Session->read('Auth.User.group_id');  
		$user_id = $this->Session->read('Auth.User.id');  
		if($group != 1 || $group != 13) { 
			$conditions['InventoryMaterialRequest.user_id'] = $user_id;
	 	} 
	 	$conditions['InventoryMaterialRequest.id'] = $id;
		$options = array('conditions' => $conditions);
		$this->set('inventoryMaterialRequest', $this->InventoryMaterialRequest->find('first', $options));

		$items = $this->InventoryMaterialRequest->InventoryMaterialRequestItem->find('all', array('conditions' => array(
			'InventoryMaterialRequestItem.inventory_material_request_id' => $id
			)));
		$this->set('items', $items);
	}

	public function returnviewrma($id = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request'));
		}
		$group = $this->Session->read('Auth.User.group_id');  
		$user_id = $this->Session->read('Auth.User.id');  
		if($group != 1 || $group != 13) { 
			$conditions['InventoryMaterialRequest.user_id'] = $user_id;
	 	} 
	 	$conditions['InventoryMaterialRequest.id'] = $id;
		$options = array('conditions' => $conditions);
		$rma = $this->InventoryMaterialRequest->find('first', $options);
		$this->set('inventoryMaterialRequest', $rma);

		$this->loadModel('InventoryMaterialRequestItem');
		$items = $this->InventoryMaterialRequestItem->find('all', array(
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_material_request_id' => $id
			),
				'recursive' => 2 
			));


		if ($this->request->is('post')) {
			$this->loadModel('InventoryStock');
			$inv_item_id = $this->request->data['InventoryMaterialRequest']['item_id'];
			$req_item_id = $this->request->data['InventoryMaterialRequest']['req_item_id'];
			$quantity = $this->request->data['InventoryMaterialRequest']['quantity'];
			$unit = $this->request->data['InventoryMaterialRequest']['general_unit_id'];
			$check = $this->request->data['InventoryMaterialRequest']['check'];
			$add = $this->request->data['InventoryMaterialRequest']['add'];

			$location = $this->request->data['InventoryMaterialRequest']['location'];
			$store = $this->request->data['InventoryMaterialRequest']['store'];
			$rack = $this->request->data['InventoryMaterialRequest']['rack'];

			$price_per_unit = $this->request->data['InventoryMaterialRequest']['price_per_unit'];

			if(!isset($check)) {
				$this->Session->setFlash(__('Please select at least 1 item.'), 'error');
				return $this->redirect(array('action' => 'returnviewrma', $id));
			}
			$i = 0; 
			for($i >= 0; $i < count($inv_item_id); $i++) { 
				if($add[$i] != 0) {
					$this->InventoryStock->create();
					$data = array(
						'inventory_item_id' => $inv_item_id[$i],
						'quantity' => $quantity[$i],
						'general_unit_id' => $unit[$i], 
						'user_id' => $this->user_id,
						'inventory_delivery_order_item_id' => 0,
						'general_unit_converter_id' => 0,
						'price_per_unit' => $price_per_unit[$i],
						'issued_quantity' => 0,
						'qc_status' => 0,
						'receive_date' => $this->date,
						'inventory_location_id' => $location[$i],
						'inventory_store_id' => $store[$i],
						'inventory_rack_id' => 0,
						'inventory_delivery_location_id' => 0,
						'finished_good_id' => 0,
						'rack' => $rack[$i],
						'created' => $this->date,
						'user_id' => $user_id,
						'inventory_supplier_id' => 0,
						'barcode' => null,
						'type' => 0
						);
					$save_stock = $this->InventoryStock->save($data, false);
					if(!$save_stock) {
						die('806');
					}
					$stock_id = $this->InventoryStock->getLastInsertId();

					$this->loadModel('InventoryMaterialHistory'); 
					$this->InventoryMaterialHistory->create();
					$history = array(
						'reference' => $rma['InventoryMaterialRequest']['code'],
						'user_id' => $rma['InventoryMaterialRequest']['user_id'], 
						'store_pic' => $this->user_id,
						'inventory_item_id' => $inv_item_id[$i],
						'inventory_stock_id' => $stock_id,
						'quantity' => $quantity[$i], // Req qty
						'general_unit_id' => $unit[$i], 
						'issued_quantity' => $quantity[$i],
						'quantity_balance' => $this->sum_stock_balance($inv_item_id[$i]),
						'inventory_material_request_id' => $id,
						'inventory_material_request_item_id' => $req_item_id[$i],
						'created' => $this->date,
						'status' => 1,
						'type' => 5, // 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 5: RMA, 6: grn, 
						'transfer_type' => 'In'
						);
					$save_history = $this->InventoryMaterialHistory->save($history);
					if(!$save_history) {
						die('830');
					}
				}
			}  
			
			$update = $this->InventoryMaterialRequest->updateAll(
				array(
					'InventoryMaterialRequest.status' => "'4'" 
				),
				array(
					'InventoryMaterialRequest.id' => $id 
				)); 
			if($update) {
				$user_id = $this->user_id;
				$name = 'Receive RMA (' . $rma['InventoryMaterialRequest']['code'] . ')';
				$link = 'inventory_material_requests/returnviewrma/'.$id;
				$type = 'Receive RMA';

				$this->insert_log($user_id, $name, $link, $type);

				$this->Session->setFlash(__('RMA Has been saved.'), 'success');
				return $this->redirect(array('action' => 'returnindex'));
			}
		}

		$this->loadModel('InventoryStore');
		$stores = $this->InventoryStore->find('list', array('recursive' => -1));

		$this->loadModel('InventoryLocation');
		$locations = $this->InventoryLocation->find('list', array('recursive' => -1));

		$item_lists = array();
		foreach ($items as $item) {
			$item_lists[] = array(
				'InventoryMaterialRequest' => $item['InventoryMaterialRequest'],
				'InventoryItem' => $item['InventoryItem'],
				'GeneralUnit' => $item['InventoryItem']['GeneralUnit'],
				'InventoryMaterialRequestItem' => $item['InventoryMaterialRequestItem'],
				'Stock' => $this->find_last_stock($item['InventoryMaterialRequestItem']['inventory_item_id']) 
				);
		}

		$this->set('items', $item_lists);

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list', array('recursive' => -1));
		$this->set(compact('units', 'locations', 'stores'));
	}
 	
 	private function find_last_stock($inventory_item_id) {
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->find('first', array(
			'conditions' => array(
					'InventoryStock.inventory_item_id' => $inventory_item_id 
				),
			'order' => 'InventoryStock.id DESC',
			'recursive' => -1
			));
		return $stock;
	}

	private function find_last_price($inventory_item_id) {
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->find('first', array(
			'conditions' => array(
					'InventoryStock.inventory_item_id' => $inventory_item_id 
				),
			'order' => 'InventoryStock.id DESC',
			'recursive' => -1
			));
		if($stock) { 
			return $stock['InventoryStock']['price_per_unit'];	
		} else {
			return 0;
		}
	}

 	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryMaterialRequest->create();

			$user_id = $this->Session->read('Auth.User.id');

			$this->request->data['InventoryMaterialRequest']['production_order_id'] = 0; 
			$this->request->data['InventoryMaterialRequest']['sale_job_item_id'] = 0;
			$this->request->data['InventoryMaterialRequest']['created'] = $this->date;
			$this->request->data['InventoryMaterialRequest']['modified'] = $this->date;
			$this->request->data['InventoryMaterialRequest']['user_id'] = $user_id;
			$this->request->data['InventoryMaterialRequest']['type'] = 0; 

			$counter = $this->InventoryMaterialRequest->find('all', array(
				'conditions' => array(
					'InventoryMaterialRequest.type' => array(0, 1)
					),
				'recursive' => -1
				)); 
			$number = count($counter) + 1;
			$mrn_no = $this->generate_code('MRN', $number);
			$this->request->data['InventoryMaterialRequest']['code'] = $mrn_no;
 
			// Items 

			if(isset($this->request->data['item_id'])) {
				$item_id = $this->request->data['item_id'];
				$quantity = $this->request->data['quantity'];
				$general_unit_id = $this->request->data['general_unit_id'];  
				
				if ($this->InventoryMaterialRequest->save($this->request->data)) {
					$request_id = $this->InventoryMaterialRequest->getLastInsertId();

					$this->loadModel('InventoryMaterialRequestItem');

					$item = array();
					for($i = 0; $i < count($item_id); $i++) {
						$item[] = array(
							'inventory_material_request_id' => $request_id,
							'inventory_item_id' => $item_id[$i],
							'quantity' => $quantity[$i],
							'general_unit_id' => $general_unit_id[$i],
							'issued_quantity' => 0,
							'issued_balance' => $quantity[$i],
							'created' => $this->date,
							'modified' => $this->date,
							'note' => '',
							'status' => 0,
							'type' => 0 // MRN  
							);
						// Move to HOD Section. $this->reserve_item($item_id[$i], $quantity[$i]);
					}
					$this->InventoryMaterialRequestItem->saveAll($item, array('deep' => true)); 

					// Find Approval
					$group_id =$this->Session->read('Auth.User.group_id');
					$this->loadModel('Approval');
					$approvals = $this->Approval->find('all', array(
						'conditions' => array(
							'Approval.name' => 'MRN',
							'Approval.group_id' => $group_id
							)
						));
					if($approvals) {
						foreach ($approvals as $approval) {
							$data = array(
								'to' => $approval['User']['email'],
								'template' => 'mrn',
								'subject' => 'MRN Require Your Approval ' . $mrn_no,
								'content' => array(
									'mrn_number' => $mrn_no,
									'from' => $this->Session->read('Auth.User.username'),
									'username' => $approval['User']['username'],  
									'link' => 'inventory_material_requests/verifyview/'.$request_id
									)
								);
							$this->send_email($data);
						}	
						$this->Session->setFlash(__('The inventory material request has been saved.'), 'success');
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('Your MRN has been saved, But User approval not found. Please ask Admin for approval.'), 'error');
						return $this->redirect(array('action' => 'index'));
					}
					
				} else {
					$this->Session->setFlash(__('The inventory material request could not be saved. Please, try again.'), 'error');
				} 
			} else {
				$this->Session->setFlash(__('Please add item.'), 'error');
			} 
		}
		$productionOrders = $this->InventoryMaterialRequest->ProductionOrder->find('list');
		$saleJobs = $this->InventoryMaterialRequest->SaleJob->find('list');
		$saleJobItems = $this->InventoryMaterialRequest->SaleJobItem->find('list');
		$users = $this->InventoryMaterialRequest->User->find('list');
		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');
		$this->set(compact('productionOrders', 'saleJobs', 'saleJobItems', 'users', 'units'));
	}

	private function reserve_item($item_id, $quantity) {
		$this->loadModel('InventoryItem');

		$item = $this->InventoryItem->find('first', array(
			'conditions' => array(
				'InventoryItem.id' => $item_id
				)
			)); 
		$data = array(); 
		if($quantity > $item['InventoryItem']['quantity_available']) {
			$shortage = $quantity - $item['InventoryItem']['quantity_available'];
			$quantity_shortage = $shortage + $item['InventoryItem']['quantity_shortage']; 
			$quantity_reserved = $quantity + $item['InventoryItem']['quantity_reserved']; 
			$this->InventoryItem->updateAll(                     
			    array(
			    	'InventoryItem.quantity_shortage' => $quantity_shortage,
			    	'InventoryItem.quantity_available' => 0,
			    	'InventoryItem.quantity_reserved' => $quantity_reserved
			    	),
			    array(
			    	'InventoryItem.id' =>  $item_id
			    	)
			); 
		} else {
			// If available stock balance 
			$quantity_available = $item['InventoryItem']['quantity_available'] - $quantity; 
			$quantity_reserved = $quantity + $item['InventoryItem']['quantity_reserved'];
			$this->InventoryItem->updateAll(                     
			    array(
			    	'InventoryItem.quantity_available' => $quantity_available, 
			    	'InventoryItem.quantity_reserved' => $quantity_reserved
			    	),
			    array(
			    	'InventoryItem.id' => $item_id
			    	)
			); 
		}  
	}

	public function edit($id = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request'));
		}
		$this->loadModel('InventoryMaterialRequestItem');
		if ($this->request->is(array('post', 'put'))) {

			$this->request->data['InventoryMaterialRequest']['modified'] = $this->date;
			if ($this->InventoryMaterialRequest->save($this->request->data)) {

				// Delete first
				$this->InventoryMaterialRequestItem->deleteAll(array(
					'InventoryMaterialRequestItem.inventory_material_request_id' => $id
					), false);

				// Insert new
				$item_id = $this->request->data['item_id'];
				$quantity = $this->request->data['quantity'];
				$general_unit_id = $this->request->data['general_unit_id']; 

				$item = array();
				for($i = 0; $i < count($item_id); $i++) {
					$item[] = array(
						'inventory_material_request_id' => $id,
						'inventory_item_id' => $item_id[$i],
						'quantity' => $quantity[$i],
						'general_unit_id' => $general_unit_id[$i],
						'issued_quantity' => 0,
						'issued_balance' => 0,
						'created' => $this->date,
						'modified' => $this->date,
						'note' => '',
						'status' => 0,
						);
					// Move to HOD Section. $this->reserve_item($item_id[$i], $quantity[$i]);
				}
				$this->InventoryMaterialRequestItem->saveAll($item, array('deep' => true)); 

				$this->Session->setFlash(__('The inventory material request has been modified.'), 'success');
				return $this->redirect(array('action' => 'view/'.$id));
			} else {
				$this->Session->setFlash(__('The inventory material request could not be saved. Please, try again.'), 'error');
			}
		} 

		$options = array('conditions' => array('InventoryMaterialRequest.' . $this->InventoryMaterialRequest->primaryKey => $id));
		$request = $this->InventoryMaterialRequest->find('first', $options);
		$this->request->data = $request;

		// If status = 0 / 1 / 6, can edit
		$allow = array(0, 1, 6);
		if(!in_array($request['InventoryMaterialRequest']['status'], $allow)) {
			$this->Session->setFlash(__('You are not allow to edit verified / transfered data. Please create new Material Request'), 'error');
			return $this->redirect(array('action' => 'view/'.$id));
		}

		$this->set('inventoryMaterialRequest', $request); 
		$items = $this->InventoryMaterialRequestItem->find('all', array(
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_material_request_id' => $id
				)
			));

		$this->set('items', $items);
		 
		$productionOrders = $this->InventoryMaterialRequest->ProductionOrder->find('list');
		$saleJobs = $this->InventoryMaterialRequest->SaleJob->find('list');
		$saleJobItems = $this->InventoryMaterialRequest->SaleJobItem->find('list');
		$users = $this->InventoryMaterialRequest->User->find('list');

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$this->set(compact('productionOrders', 'saleJobs', 'saleJobItems', 'users', 'units'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function editrma($id = null) {
		if (!$this->InventoryMaterialRequest->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request'));
		}
		$this->loadModel('InventoryMaterialRequestItem');
		if ($this->request->is(array('post', 'put'))) {

			$this->request->data['InventoryMaterialRequest']['modified'] = $this->date;
			if ($this->InventoryMaterialRequest->save($this->request->data)) {

				// Delete first
				$this->InventoryMaterialRequestItem->deleteAll(array(
					'InventoryMaterialRequestItem.inventory_material_request_id' => $id
					), false);

				// Insert new
				$item_id = $this->request->data['item_id'];
				$quantity = $this->request->data['quantity'];
				$general_unit_id = $this->request->data['general_unit_id']; 

				$item = array();
				for($i = 0; $i < count($item_id); $i++) {
					$item[] = array(
						'inventory_material_request_id' => $id,
						'inventory_item_id' => $item_id[$i],
						'quantity' => $quantity[$i],
						'general_unit_id' => $general_unit_id[$i],
						'issued_quantity' => 0,
						'issued_balance' => 0,
						'created' => $this->date,
						'modified' => $this->date,
						'note' => '',
						'status' => 0,
						);
					// Move to HOD Section. $this->reserve_item($item_id[$i], $quantity[$i]);
				}
				$this->InventoryMaterialRequestItem->saveAll($item, array('deep' => true)); 

				$this->Session->setFlash(__('The inventory material request has been modified.'), 'success');
				return $this->redirect(array('action' => 'returnview/'.$id));
			} else {
				$this->Session->setFlash(__('The inventory material request could not be saved. Please, try again.'), 'error');
			}
		} 

		$options = array('conditions' => array('InventoryMaterialRequest.' . $this->InventoryMaterialRequest->primaryKey => $id));
		$request = $this->InventoryMaterialRequest->find('first', $options);
		$this->request->data = $request;

		// If status = 0 / 1 / 6, can edit
		$allow = array(0, 1, 6);
		if(!in_array($request['InventoryMaterialRequest']['status'], $allow)) {
			$this->Session->setFlash(__('You are not allow to edit verified / transfered data. Please create new RMA'), 'error');
			return $this->redirect(array('action' => 'view/'.$id));
		}

		$this->set('inventoryMaterialRequest', $request); 
		$items = $this->InventoryMaterialRequestItem->find('all', array(
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_material_request_id' => $id
				)
			));

		$this->set('items', $items);
		 
		$productionOrders = $this->InventoryMaterialRequest->ProductionOrder->find('list');
		$saleJobs = $this->InventoryMaterialRequest->SaleJob->find('list');
		$saleJobItems = $this->InventoryMaterialRequest->SaleJobItem->find('list');
		$users = $this->InventoryMaterialRequest->User->find('list');

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$this->set(compact('productionOrders', 'saleJobs', 'saleJobItems', 'users', 'units'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryMaterialRequest->id = $id;
		if (!$this->InventoryMaterialRequest->exists()) {
			throw new NotFoundException(__('Invalid inventory material request'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryMaterialRequest->delete()) {
			$this->Session->setFlash(__('The inventory material request has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory material request could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
