<?php
App::uses('AppController', 'Controller');
/**
 * GeneralCurrencies Controller
 *
 * @property GeneralCurrency $GeneralCurrency
 * @property PaginatorComponent $Paginator
 */
class GeneralCurrenciesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->GeneralCurrency->recursive = 0;
		$this->set('generalCurrencies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->GeneralCurrency->exists($id)) {
			throw new NotFoundException(__('Invalid general currency'));
		}
		$options = array('conditions' => array('GeneralCurrency.' . $this->GeneralCurrency->primaryKey => $id));
		$this->set('generalCurrency', $this->GeneralCurrency->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->GeneralCurrency->create();
			if ($this->GeneralCurrency->save($this->request->data)) {
				$this->Session->setFlash(__('The general currency has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The general currency could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->GeneralCurrency->exists($id)) {
			throw new NotFoundException(__('Invalid general currency'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->GeneralCurrency->save($this->request->data)) {
				$this->Session->setFlash(__('The general currency has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The general currency could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GeneralCurrency.' . $this->GeneralCurrency->primaryKey => $id));
			$this->request->data = $this->GeneralCurrency->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->GeneralCurrency->id = $id;
		if (!$this->GeneralCurrency->exists()) {
			throw new NotFoundException(__('Invalid general currency'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->GeneralCurrency->delete()) {
			$this->Session->setFlash(__('The general currency has been deleted.'));
		} else {
			$this->Session->setFlash(__('The general currency could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
