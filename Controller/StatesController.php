<?php
App::uses('AppController', 'Controller');
/**
 * States Controller
 *
 * @property State $State
 * @property PaginatorComponent $Paginator 
 */
class StatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }
	
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		
		/* Filter With Paging */
        if(!empty($this->data['State'])) {         
            $this->Session->write('Filter.State',$this->data);
        }
		elseif((!empty($this->passedArgs['page']) || strpos($this->referer(),'states/index'))  &&  $this->Session->check('Filter.State')) {
            $this->request->data = $this->Session->check('Filter.State')? $this->Session->read('Filter.State') : '';
        }
        else
        {
            $this->Session->delete('Filter.State');
        }
        /* End Filter With Paging */
		$conditions = array();
		if(!empty($this->data)) {
			if(!empty($this->data['State']['name'])) {
				$conditions['State.name LIKE']='%'.$this->data['State']['name'];
			}
			
		}
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'State.created DESC','limit'=>$record_per_page);
		$this->set('states', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->State->exists($id)) {
			throw new NotFoundException(__('Invalid state'));
		}
		$options = array('conditions' => array('State.' . $this->State->primaryKey => $id));
		$this->set('state', $this->State->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		
		if ($this->request->is('post')) {
			$this->State->create();
			if ($this->State->save($this->request->data)) {
				$this->Session->setFlash(__('The state has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The state could not be saved. Please, try again.'),'error');
			}
		}
		$countries = $this->State->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->State->exists($id)) {
			throw new NotFoundException(__('Invalid state'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->State->save($this->request->data)) {
				$this->Session->setFlash(__('The state has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The state could not be saved. Please, try again.'),'error');
			}
		} else {
			$options = array('conditions' => array('State.' . $this->State->primaryKey => $id));
			$this->request->data = $this->State->find('first', $options);
		}
		$countries = $this->State->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->State->id = $id;
		if (!$this->State->exists()) {
			throw new NotFoundException(__('Invalid state'));
		}
		//$this->request->allowMethod('post', 'delete');
		$user_id	=	$this->Session->read('Auth.User.id');
		$SystemId   = Configure::read('SystemId');
		
		if(!in_array($user_id,$SystemId))
		{
			$this->Session->setFlash(__('You are not authorized to view this location.'),'error');
		}
		else if($this->State->delete()) {
			$this->Session->setFlash(__('The state has been deleted.'),'success');
		} else {
			$this->Session->setFlash(__('The state could not be deleted. Please, try again.'),'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_status method
 *
 * @return void
 */
 
	public function admin_status($id,$status){
		if (!$this->State->exists($id)) {

			$this->Session->setFlash(__('Invalid State Id.'),'error');
			$this->redirect(array('action' => 'index'));

		}
		$this->State->id = $id;
		if($this->State->saveField('status',$status))
			$this->Session->setFlash(__('Status successfully updated.'),'success');
		else
			$this->Session->setFlash(__('Status could not be update. Please, try again.'),'error');
			
		$this->redirect(array('action' => 'index'));
	}


	
	public function admin_ajax_state_list($country_id = NULL){
		$this->layout = false;
		$states = $this->State->find('list', array('conditions'=> array('State.country_id' => $country_id)));
		$this->set(compact('states'));
	}
	
}
