<?php
App::uses('AppController', 'Controller');
/**
 * TermOfPayments Controller
 *
 * @property TermOfPayment $TermOfPayment
 * @property PaginatorComponent $Paginator
 */
class TermOfPaymentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TermOfPayment->recursive = 0;
		$this->set('termOfPayments', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TermOfPayment->exists($id)) {
			throw new NotFoundException(__('Invalid term of payment'));
		}
		$options = array('conditions' => array('TermOfPayment.' . $this->TermOfPayment->primaryKey => $id));
		$this->set('termOfPayment', $this->TermOfPayment->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TermOfPayment->create();
			$this->request->data['TermOfPayment']['created'] = date('Y-m-d H:i:s');

			if ($this->TermOfPayment->save($this->request->data)) {
				$this->Session->setFlash(__('The term of payment has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The term of payment could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TermOfPayment->exists($id)) {
			throw new NotFoundException(__('Invalid term of payment'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['TermOfPayment']['modified'] = date('Y-m-d H:i:s');
			if ($this->TermOfPayment->save($this->request->data)) {
				$this->Session->setFlash(__('The term of payment has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The term of payment could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('TermOfPayment.' . $this->TermOfPayment->primaryKey => $id));
			$this->request->data = $this->TermOfPayment->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TermOfPayment->id = $id;
		if (!$this->TermOfPayment->exists()) {
			throw new NotFoundException(__('Invalid term of payment'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TermOfPayment->delete()) {
			$this->Session->setFlash(__('The term of payment has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The term of payment could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
