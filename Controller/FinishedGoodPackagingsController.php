<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodPackagings Controller
 *
 * @property FinishedGoodPackaging $FinishedGoodPackaging
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FinishedGoodPackagingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
		parent::beforeFilter();
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodPackaging->recursive = 0;
		$this->set('finishedGoodPackagings', $this->Paginator->paginate());
	}

	public function ready() {
		$this->loadModel('FinishedGood');
		$conditions = array('FinishedGood.fat_status' => 'Approved', 'FinishedGood.final_inspect' => 'Approved'); 
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page,
			'recursive' => 0
			); 
		$this->set('finalGoods', $this->Paginator->paginate('FinishedGood'));
	}

	public function waiting() {

		$this->loadModel('SaleJobChild');

		if(isset($_GET['search'])) {
			if($_GET['station_name'] != '') {
				$conditions['SaleJobChild.station_name LIKE'] = '%'.$_GET['station_name'].'%';
			}

			if($_GET['name'] != '') {
				$conditions['SaleJobChild.name LIKE'] = '%'.$_GET['name'].'%';
			}

			if($_GET['delivery_date'] != '') {
				$conditions['SaleJobChild.delivery_date >='] = $_GET['delivery_date'];
				$conditions['SaleJobChild.delivery_date <='] = $_GET['delivery_date'];
			} 
			$this->request->data['SaleJobChild'] = $_GET;
		} 

		// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer,  
		$conditions['SaleJobChild.status'] = array(0); 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order'=>'SaleJobChild.delivery_date ASC',
			'limit'=>$record_per_page
			); 
		$data = array();
		$stations = $this->Paginator->paginate('SaleJobChild');
		foreach ($stations as $station) {
			$data[] = array(
				'id' => $station['SaleJobChild']['id'],
				'name' => $station['SaleJobChild']['name'], 
				'station_name' => $station['SaleJobChild']['station_name'],
				'delivery_date' => $station['SaleJobChild']['delivery_date'],
				'fat_date' => $station['SaleJobChild']['fat_date'],
				'status' => $station['SaleJobChild']['status'],
				'job' => $station['SaleJob'],
				'customer' => $this->customer($station['SaleJob']['customer_id']),
				'finished_good' => $this->check_finished_good($station['SaleJobChild']['id']),
				'items' => $this->sum_job_item_qty($station['SaleJobChild']['id'])
				);
		}  
		$this->set('jobs', $data);  
	} 

	private function sum_job_item_qty($child_id) {
		$this->loadModel('SaleJobItem');
		$sums = $this->SaleJobItem->find('all', array(
			'fields' => array(
				'SUM(SaleJobItem.quantity) AS qty'
				),
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $child_id
				),
			'recursive' => -1
			));
		if($sums) {
			foreach ($sums as $sum) {
				return $sum[0]['qty'];
			}	
		} else {
			return 0;
		}
		
	}

	private function check_finished_good($id) {
		$this->loadModel('ProductionOrder');
		$items = $this->ProductionOrder->find('all', array(
			'conditions' => array(
				'ProductionOrder.sale_job_childs_id' => $id
				),
			'recursive' -1
			));
		$data = array();
		foreach ($items as $po) {
			$data[] = $po['ProductionOrder']['id'];
		}

		$this->loadModel('FinishedGood');
		$finised_goods = $this->FinishedGood->find('all', array(
			'conditions' => array(
				'FinishedGood.production_order_id' => $data,
				'FinishedGood.status' => 'Approved',
				'FinishedGood.fat_status' => 'Approved',
				'FinishedGood.final_inspect' => 'Approved',
				),
			'recursive' -1
			));
		return count($finised_goods);
	}

	private function customer($customer_id) {
		$this->loadModel('Customer');
		$customer = $this->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $customer_id
				)
			));
		return $customer;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid finished good packaging'));
		}
		$options = array('conditions' => array('FinishedGoodPackaging.' . $this->FinishedGoodPackaging->primaryKey => $id));
		$this->set('finishedGoodPackaging', $this->FinishedGoodPackaging->find('first', $options));

		$this->loadModel('FinishedGoodPackagingItem');
		$fg_item = $this->FinishedGoodPackagingItem->find('all', array(
			'conditions' => array(
				'FinishedGoodPackagingItem.finished_good_packaging_id' => $id
			),
			'recursive' => -1
		));
		$fg_item_array = array();
		foreach ($fg_item as $fg) {
			$fg_item_array[] = array(
				'FinishedGoodPackagingItem' => $fg_item[0]['FinishedGoodPackagingItem'],
				'FinishedGood' => $this->get_finishgood($fg_item[0]['FinishedGoodPackagingItem']['finished_good_id']),
			);
		}

		//print_r($fg_item_array);
		//exit;

		$options = array('conditions' => array('FinishedGoodPackagingItem.finished_good_packaging_id' => $id), 'recursive' => -1);
		$this->set('finishedGoodPackagingItem', $fg_item_array);

		$this->loadModel('FinishedGoodPackagingBox');
		$options = array('conditions' => array('FinishedGoodPackagingBox.finished_good_packaging_id' => $id), 'recursive' => -1);
		$this->set('finishedGoodPackagingBox', $this->FinishedGoodPackagingBox->find('all', $options));
	}

	private function get_finishgood($id){
		$this->loadModel('FinishedGood');
		$finishgood = $this->FinishedGood->find('all', array(
			'conditions' => array(
				'FinishedGood.id' => $id
			),
			'recursive' => -1
		));

		return $finishgood[0]['FinishedGood'];
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodPackaging->create();

			$this->request->data['FinishedGoodPackaging']['user_id'] = $this->user_id;
			//print_r($_POST);
			//exit;
			//$remark = $this->request->data['remark'];

			//$this->request->data['remark'] = $remark;
			
			if ($this->FinishedGoodPackaging->save($this->request->data)) {

				$packaging_id = $this->FinishedGoodPackaging->getLastInsertId();

				$box_name = $this->request->data['box_name'];
				$box_size = $this->request->data['box_size'];
				$box_weight = $this->request->data['box_weight'];
				$box_qty = $this->request->data['box_qty'];
				$box_remark = $this->request->data['remark'];
				$box_nett_weight = $this->request->data['box_nett_weight'];

				$finished_good_id = $this->request->data['finished_good_id'];
				$sale_job_item_id = $this->request->data['sale_job_item_id']; 

				$sale_job_child_id = $this->request->data['FinishedGoodPackaging']['sale_job_child_id'];
				$this->loadModel('FinishedGoodPackagingBox');

				for($x = 0; $x < count($box_name); $x++) {
					$this->FinishedGoodPackagingBox->create();
					$box = array(
						'finished_good_packaging_id' => $packaging_id,
						'name' => $box_name[$x],
						'measurement' => $box_size[$x],
						'weight' => $box_weight[$x],
						'quantity' => $box_qty[$x],
						'remark' => $box_remark[$x],
						'nett_weight' => $box_nett_weight[$x]
						);
					$this->FinishedGoodPackagingBox->save($box);
				}

				$this->loadModel('FinishedGoodPackagingItem');
				$this->loadModel('FinishedGood');
				for($i = 0; $i < count($finished_good_id); $i++) {
					$this->FinishedGoodPackagingItem->create();
					$item = array(
						'finished_good_packaging_id' => $packaging_id,
						'finished_good_id' => $finished_good_id[$i],
						'sale_job_item_id' => $sale_job_item_id[$i],
						'created' => $this->date 
						);
					$this->FinishedGoodPackagingItem->save($item);

					$this->FinishedGood->create();
					$item_fg = array(
						'id' => $finished_good_id[$i],
						'packing_status' => 1
						);
					$this->FinishedGood->save($item_fg);
				} 


				// Update sale order
                $this->update_sale_order_progress($sale_job_child_id, 'Packing');

				$this->Session->setFlash(__('The finished good packaging has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good packaging could not be saved. Please, try again.'), 'error');
			}
		}
		$saleJobs = $this->FinishedGoodPackaging->SaleJob->find('list');
		$saleJobChildren = $this->FinishedGoodPackaging->SaleJobChild->find('list');
		$customers = $this->FinishedGoodPackaging->Customer->find('list'); 
		$users = $this->FinishedGoodPackaging->User->find('list');
		$this->set(compact('saleJobs', 'saleJobChildren', 'customers', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodPackaging->exists($id)) {
			throw new NotFoundException(__('Invalid finished good packaging'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodPackaging->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good packaging has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good packaging could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('FinishedGoodPackaging.' . $this->FinishedGoodPackaging->primaryKey => $id));
			$this->request->data = $this->FinishedGoodPackaging->find('first', $options);
		}
		$saleJobs = $this->FinishedGoodPackaging->SaleJob->find('list');
		$saleJobChildren = $this->FinishedGoodPackaging->SaleJobChild->find('list');
		$customers = $this->FinishedGoodPackaging->Customer->find('list');
		$productionOrders = $this->FinishedGoodPackaging->ProductionOrder->find('list');
		$users = $this->FinishedGoodPackaging->User->find('list');
		$this->set(compact('saleJobs', 'saleJobChildren', 'customers', 'productionOrders', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodPackaging->id = $id;
		if (!$this->FinishedGoodPackaging->exists()) {
			throw new NotFoundException(__('Invalid finished good packaging'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodPackaging->delete()) {
			$this->Session->setFlash(__('The finished good packaging has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The finished good packaging could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
