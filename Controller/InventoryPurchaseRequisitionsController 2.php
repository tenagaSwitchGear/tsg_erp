<?php
App::uses('AppController', 'Controller');
/**
 * InventoryPurchaseRequisitions Controller
 *
 * @property InventoryPurchaseRequisition $InventoryPurchaseRequisition
 * @property PaginatorComponent $Paginator
 */
class InventoryPurchaseRequisitionsController extends AppController {

/**
 * Components
 * 
 * @var array
 */
    public $components = array('Paginator');

    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
    public function index($status = null) {
        $role = $this->Session->read('Auth.User.group_id');
        $id = $this->Session->read('Auth.User.id');
        if(isset($_GET['search'])) {
            //print_r($_GET);
            //exit;
            if($_GET['pr_no'] != '') {
                $conditions['InventoryPurchaseRequisition.pr_no LIKE'] = '%'.trim($_GET['pr_no']).'%'; 
            }

            if($_GET['date_start'] != '') {
                $conditions['InventoryPurchaseRequisition.created >='] = $_GET['date_start']; 
            }

            if($_GET['date_end'] != '') { 
                $conditions['InventoryPurchaseRequisition.created <='] = $_GET['date_end']; 
            }

            if($_GET['status'] != 'x') {
                $conditions['InventoryPurchaseRequisition.status LIKE'] = $_GET['status']; 
            } 

            $this->request->data['InventoryPurchaseRequisition'] = $_GET;
        }  

        // If not admin, show only personal PR
        if($role != 1) {
            $conditions['InventoryPurchaseRequisition.user_id'] = $id;
        }

        if($status != null) {
            $status = explode(',', $status);
            $conditions['InventoryPurchaseRequisition.status'] = $status;
        }
        $conditions['InventoryPurchaseRequisition.user_id !='] = 0;
        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => 'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            );
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
         
    } 

    public function procurementindex($status = null) {

        $role = $this->Session->read('Auth.User.group_id');
        $id = $this->Session->read('Auth.User.id');
        if(isset($_GET['search'])) {
            //print_r($_GET);
            //exit;
            if($_GET['pr_no'] != '') {
                $conditions['InventoryPurchaseRequisition.pr_no LIKE'] = $_GET['pr_no'];
                if($role != 1) {
                    $gid = $this->Session->read('Auth.User.group_id');

                    if($gid == 11){
                        $conditions['InventoryPurchaseRequisition.status'] = 9;
                    }else{
                        $user_id = $this->Session->read('Auth.User.id');
                        $conditions['InventoryPurchaseRequisition.user_id'] = $user_id;
                    }            
                } else {
                    $conditions['InventoryPurchaseRequisition.user_id !='] = 0;
                }
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseRequisition.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
                //$conditions['InventoryPurchaseRequisition.created <='] = $_GET['date_end'];
                if($role != 1) {
                    $gid = $this->Session->read('Auth.User.group_id');

                    if($gid == 11){
                        $conditions['InventoryPurchaseRequisition.status'] = 9;
                    }else{
                        $user_id = $this->Session->read('Auth.User.id');
                        $conditions['InventoryPurchaseRequisition.user_id'] = $user_id;
                    }            
                } else {
                    $conditions['InventoryPurchaseRequisition.user_id !='] = 0;
                }
            }

            if($_GET['status'] != 'x') {
                $conditions['InventoryPurchaseRequisition.status LIKE'] = $_GET['status'];
                if($role != 1) {
                    $gid = $this->Session->read('Auth.User.group_id');

                    if($gid == 11){
                        $conditions['InventoryPurchaseRequisition.status'] = 9;
                    }else{
                        $user_id = $this->Session->read('Auth.User.id');
                        $conditions['InventoryPurchaseRequisition.user_id'] = $user_id;
                    }            
                } else {
                    $conditions['InventoryPurchaseRequisition.user_id !='] = 0;
                }
            }

            if($_GET['pr_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions = '';
            }

            $this->request->data['InventoryPurchaseRequisition'] = $_GET;
        }
        
        if($status != null) {
            if($status >= 3 && $status <= 12) {
                $conditions['InventoryPurchaseRequisition.status'] = $status;
            } else {
                $conditions['InventoryPurchaseRequisition.status'] = 3;
            }
        } else {
            $conditions['InventoryPurchaseRequisition.status'] = 3;
        }
        

        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page);
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate()); 
    }

    public function rejected() {
        $id = $this->Session->read('Auth.User.id');
        $conditions['InventoryPurchaseRequisition.user_id'] = $id;
        $conditions['InventoryPurchaseRequisition.status'] = '0';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function approved() {
        $id = $this->Session->read('Auth.User.id');
        $conditions['InventoryPurchaseRequisition.user_id'] = $id;
        $conditions['InventoryPurchaseRequisition.status'] = '9';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function pending() {

        $group = $this->Session->read('Auth.User.group_id');
        if($group != 1) {
            $this->loadModel('Approval');
            $approval = $this->Approval->find('first', array(
                'conditions' => array(
                    'Approval.user_id' => $this->user_id,
                    'Approval.name' => 'Purchase Requisition'
                    )
                )); 
            if($approval) {
                $conditions['User.group_id'] = $approval['Approval']['group_id'];

            } 
        }
        
        $conditions['InventoryPurchaseRequisition.status'] = '2';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function draft() {
        $id = $this->Session->read('Auth.User.id');
        $conditions['InventoryPurchaseRequisition.user_id'] = $id;
        $conditions['InventoryPurchaseRequisition.status'] = '1';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function insufficient_budget() {
        $id = $this->Session->read('Auth.User.id');
        $conditions['InventoryPurchaseRequisition.user_id'] = $id;
        $conditions['InventoryPurchaseRequisition.status'] = '5';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function hod() {
        $conditions['InventoryPurchaseRequisition.status'] = '3';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function gm() {
        $conditions['InventoryPurchaseRequisition.status'] = '4';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function procurement() {
        
        $conditions['InventoryPurchaseRequisition.status'] = 9;
        
 
        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array('conditions' => $conditions,'order'=>'InventoryPurchaseRequisition.id DESC','limit'=>$record_per_page);
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
        //$this->InventoryPurchaseRequisition->recursive = 0;
        //$this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate());
    }

    public function index_po() {
        //print_r($_GET);
        $this->loadModel('InventoryPurchaseOrder');
        $this->loadModel('InventoryPurchaseOrderItem');
        $this->InventoryPurchaseOrder->create();
        
        
        $id = $_GET['id'];
        $data = $this->InventoryPurchaseOrder->query("SELECT id FROM inventory_purchase_orders ORDER BY id DESC");
        //print_r($data);

        if(empty($data)){
            $bil = '1';
        }else{
            $bil = $data[0]['inventory_purchase_requisition_items']['id']+1;
        }

        $PO_no = $this->generate_code('PO', $bil);

        $options = array('conditions' => array('InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id));
        $this->request->data = $this->InventoryPurchaseRequisition->find('first', $options);
        //print_r($this->request->data);
        //exit;
        $data_pr = $this->request->data['InventoryPurchaseRequisition'];
        //print_r($data_pr);
        $data_po = array(
            'user_id' => $data_pr['user_id'], 
            'group_id' => $data_pr['group_id'], 
            'general_purchase_requisition_type_id' => $data_pr['general_purchase_requisition_type_id'], 
            'created' => date('Y-m-d H:i:s'), 
            'dateline' => $data_pr['dateline'], 
            'status' => $data_pr['status'], 
            'po_no' => $PO_no, 
            'inventory_supplier_id' => $this->request->data['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id'],
            'account_department_budget_id' => $data_pr['account_department_budget_id'],
            'term_of_payment_id' => $data_pr['term_of_payment_id'],
            'remark_id' => $data_pr['remark_id'],
            'warranty_id' => $data_pr['warranty_id']
        );

        $data = $this->InventoryPurchaseOrder->query("SELECT po_no FROM inventory_purchase_orders WHERE po_no='".$data_pr['pr_no']."'");
        $data_item_pr = $this->request->data['InventoryPurchaseRequisitionItem'];
        
        if(count($data)=='0'){
            if($this->InventoryPurchaseOrder->save($data_po)){
                $data_item_pr = $this->request->data['InventoryPurchaseRequisitionItem'];

                //print_r($data_item_pr);
                //exit;
                $count = count($this->request->data['InventoryPurchaseRequisitionItem']);
                
                $lastID = $this->InventoryPurchaseOrder->getLastInsertID();
                for($i=0; $i<$count; $i++){
                    $this->InventoryPurchaseOrderItem->create();
                    //print_r($data_item_pr[$i]);
                    $data_po_item = array(
                        'inventory_supplier_id' => $data_item_pr[$i]['inventory_supplier_id'],
                        'inventory_supplier_item_id' => $data_item_pr[$i]['inventory_supplier_item_id'], 
                        'inventory_purchase_order_id' => $lastID,
                        'inventory_item_id' => $data_item_pr[$i]['inventory_item_id'],
                        'quantity' => $data_item_pr[$i]['quantity'],
                        'general_unit_id' => $data_item_pr[$i]['general_unit_id'],
                        'price_per_unit' => $data_item_pr[$i]['price_per_unit'],
                        'amount' => $data_item_pr[$i]['amount'],
                        'discount' => $data_item_pr[$i]['discount'],
                        'discount_type' => $data_item_pr[$i]['discount_type'],
                        'tax'   => $data_item_pr[$i]['tax']
                    );

                    //print_r($data_po_item);
                    //exit;
                    $this->InventoryPurchaseOrderItem->save($data_po_item);
                
                }
                //exit;
                $this->Session->setFlash(__('The Purchase Order has been created.'), 'success');
                return $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__('The Purchase Order could not be created. Please, try again.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
        }else{
            $this->Session->setFlash(__('The Purchase Order already created.'), 'error');
            return $this->redirect(array('action' => 'index'));
        }
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null, $approval_status = null) {
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'));
        }

        if($approval_status != null) {
            if($approval_status == 'Approval') {
                $update = $this->InventoryPurchaseRequisition->updateAll(array(
                    'InventoryPurchaseRequisition.status' => "1"
                    ),
                array(
                    'InventoryPurchaseRequisition.id' => $id
                    )
                );
                if($update) {
                    $this->Session->setFlash(__('The purchase requisition status has been updated.'), 'success');
                    return $this->redirect(array('action' => 'index'));
                }
            }
        }

        if ($this->request->is(array('post', 'put'))) {
            //print_r($_POST);
            //exit;
            $this->loadModel('InventoryPurchaseRequisitionItem');
            $this->loadModel('InternalDepartment');
            $this->loadModel('AccountDepartmentBudgets');
            $this->loadModel('AccountDepartmentBudgetHistory');
            //exit;
            $detail_pr = $this->InventoryPurchaseRequisition->query("SELECT * FROM inventory_purchase_requisitions WHERE id=".$_POST['data']['InventoryPurchaseRequisition']['id']);

            $detail_pr_item = $this->InventoryPurchaseRequisitionItem->query("SELECT * FROM inventory_purchase_requisition_items WHERE inventory_purchase_requisition_id=".$detail_pr[0]['inventory_purchase_requisitions']['id']);
            //print_r($_SESSION);


            $budget = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets WHERE id=".$detail_pr[0]['inventory_purchase_requisitions']['account_department_budget_id']);
            //print_r($budget);
            
            if($this->request->data['InventoryPurchaseRequisition']['role'] == 'HOD'){
                $status = '9';
            }else if($this->request->data['InventoryPurchaseRequisition']['role'] == 'HOS'){
                $status = '3';
            }else{
                $status = '0';
            }

            if($this->request->data['InventoryPurchaseRequisition']['role'] == 'HOD'){
            $total_amount = "0";
            $total_tax = "0";
            foreach($detail_pr_item as $pr_item){
                //print_r($pr_item['inventory_purchase_requisition_items']);
               
                $amount = $pr_item['inventory_purchase_requisition_items']['quantity'] * $pr_item['inventory_purchase_requisition_items']['price_per_unit'];
                if($pr_item['inventory_purchase_requisition_items']['discount_type'] == '1'){
                    $discount = $amount * ($pr_item['inventory_purchase_requisition_items']['discount']/100);
                }else{
                    $discount = $pr_item['inventory_purchase_requisition_items']['discount'];
                }

                if($pr_item['inventory_purchase_requisition_items']['tax'] != '0'){
                    $tax = ($amount - $discount) * ($pr_item['inventory_purchase_requisition_items']['tax']/100);
                }else{
                    $tax = '0';
                }

                $total_amount = $total_amount + $amount - $discount + $tax;
                $total_tax = $total_tax + $tax;
            }

            $deduct_from_budget = $total_amount - $total_tax;
    
            $this->AccountDepartmentBudgets->create();
            $data_budget = array(
                'id' => $budget[0]['account_department_budgets']['id'],
                'total' => $budget[0]['account_department_budgets']['total'] - $deduct_from_budget,
                'used' => $budget[0]['account_department_budgets']['used'] + $deduct_from_budget
            );

            $this->AccountDepartmentBudgets->save($data_budget);

            $this->AccountDepartmentBudgetHistory->create();
            
            $data_budget_history = array(
                'account_department_budget_id' => $budget[0]['account_department_budgets']['id'],
                'inventory_purchase_requisition_id' => $_POST['data']['InventoryPurchaseRequisition']['id'],
                'amount' => $deduct_from_budget
            );
            //print_r($data_budget_history);
            //exit;
            $this->AccountDepartmentBudgetHistory->save($data_budget_history);
            }
            $data_pr = array('id' => $_POST['data']['InventoryPurchaseRequisition']['id'], 'status' => $status);
            //exit;
        
           
            if ($this->InventoryPurchaseRequisition->save($data_pr)) {
                $this->Session->setFlash(__('The purchase requisition status has been updated.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The purchase requisition status could not be update. Please, try again.'), 'error');
            }
        }

        $pr = $this->InventoryPurchaseRequisition->find('first', array(
                'conditions' => array(
                    'InventoryPurchaseRequisition.id' => $id
                ) 
            ));

        // Approval remark lists
        $this->loadModel('InventoryPurchaseRequisitionRemark');

        $approvalremarks = $this->InventoryPurchaseRequisitionRemark->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionRemark.inventory_purchase_requisition_id' => $id
                ),
            'order' => 'InventoryPurchaseRequisitionRemark.id DESC'
            ));
        $this->set('approvalremarks', $approvalremarks); 
        // End approval remark


        if($pr['InventoryPurchaseRequisition']['pr_type'] == 1) {
            $cat = 'CAPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 2) {
            $cat = 'OPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 3 || $pr['InventoryPurchaseRequisition']['pr_type'] == 4) {
            $cat = 'SERVING_CLIENT';
        }
    
        $this->loadModel('InternalLofa'); 

        $lofas = $this->InternalLofa->find('all', array(
            'conditions' => array(
                'InternalLofa.category' => $cat
                )
            ));
        $this->set('lofas', $lofas);

        // View current budget  
        $this->loadModel('InventoryPurchaseRequisitionItem');
        $waitings = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'fields' => array(
                'SUM(InventoryPurchaseRequisitionItem.total_rm) AS total_waiting'
                ),
            'conditions' => array(
                'InventoryPurchaseRequisition.status' => array(1, 3, 5, 7, 9),
                'InventoryPurchaseRequisition.account_department_budget_id' => $pr['InventoryPurchaseRequisition']['account_department_budget_id'],
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id !=' => $id
                )
            ));
        $total_waiting = 0;
        foreach ($waitings as $waiting) {
            $total_waiting = $waiting[0]['total_waiting'];
        }
        $this->set('total_waiting', $total_waiting);

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');

        $options = array('conditions' => array('InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id), 'recursive'=>2);
        $this->set('inventoryPurchaseRequisition', $this->InventoryPurchaseRequisition->find('first', $options));
        
        $n = $this->InventoryPurchaseRequisition->find('first', $options);
        //print_r($n);
        //exit;

        $this->loadModel('InternalDepartment');
        $internal = $this->InternalDepartment->query("SELECT * FROM internal_departments WHERE user_id=".$_SESSION['Auth']['User']['id']." AND group_id=".$_SESSION['Auth']['User']['group_id']);

        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->loadModel('AccountDepartmentBudgets');
        $budget = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets WHERE id=".$n['InventoryPurchaseRequisition']['account_department_budget_id']);

        $this->loadModel('InventorySupplierItems');
        $inventory_item = $this->InventorySupplierItems->find('all', array(
            'conditions' => array('InventorySupplierItems.inventory_supplier_id' => $n['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id']),
            //'fields' => array('invitem.*', 'InventorySupplierItems.*')  
        ));

        $this->loadModel('InventoryPurchaseRequisitionItem');
        $items = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array('InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id),
            //'fields' => array('invitem.*', 'InventorySupplierItems.*')  
        ));

        if(!empty($internal)){
            if($internal[0]['internal_departments']['id'] == '2'){
                $role = "HOD";
            } else if($internal[0]['internal_departments']['id'] == '1'){
                $role = "HOS";
            }else{
                $role = "Staff";
            }
        }else{
            $role = "Staff";
        }

        $sts = array('1'=>'Approved', '2'=>'Rejected');
        $this->set(compact('generalUnits', 'sts', 'budget', 'terms', 'warranty', 'remark', 'role'));

        $this->set('items', $items);
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {
        if ($this->request->is('post')) { 
            $this->loadModel('SaleJob'); 
                $item_arr = $_POST['inventory_item_id'];
                $nitem = array();
                for($i=0; $i<count($_POST['inventory_item_id']); $i++){
                    if($_POST['inventory_item_id'][$i] != 'xxx'){
                        $nitem[] = $_POST['inventory_item_id'][$i];
                    }
                }

                $_POST['inventory_item_id'] = $nitem;

                $this->loadModel('InventoryPurchaseRequisitionItem');
                $this->loadModel('Remark');
                $this->loadModel('TermOfPayment');
                $this->loadModel('Warranty');
                $this->InventoryPurchaseRequisition->create();
                $dateline = $this->request->data['InventoryPurchaseRequisition']['dateline'];
                $dateline = date('Y-m-d H:i:s', strtotime($dateline));
                $created = date('Y-m-d H:i:s');
                $budget = $this->request->data['InventoryPurchaseRequisition']['account_department_budget_id'];
                $a_budget = $_POST['a_budget'];
                $terms = $this->request->data['InventoryPurchaseRequisition']['term_of_payment_id'];
                $remark = $this->request->data['InventoryPurchaseRequisition']['remark'];
                $warranty = $this->request->data['InventoryPurchaseRequisition']['warranty_id'];

                $user_id = $_SESSION['Auth']['User']['id'];
                $group_id = $_SESSION['Auth']['User']['group_id'];

                $genPR_type = '1'; 
                $prno = $this->InventoryPurchaseRequisition->find('first', array(
                    'order' => 'InventoryPurchaseRequisition.id DESC'
                    ));
 

                // Check if selected
                if($warranty != '') {
                    $data_warranty = $this->Warranty->query("SELECT id FROM warranties WHERE id=".$warranty);

                    if(!empty($data_warranty)){
                        $warranty_id = $this->request->data['InventoryPurchaseRequisition']['warranty_id'];
                    }else{
                        $this->Warranty->create();

                        $warranty_data = array('name' => $this->request->data['InventoryPurchaseRequisition']['new_warranty']);

                        $this->Warranty->save($warranty_data);
                        
                        $warranty_id = $this->Warranty->getLastInsertID();
                    }    
                } else {
                    $warranty_id = 0;
                }
                
                $bil = $prno['InventoryPurchaseRequisition']['id'] + 1;
                $PR_no = $this->generate_code('PR', $bil);
            
                //$PR_no = 'TSG/'.date('Y').'/D-'.$_SESSION['Auth']['User']['group_id'].'-'.$bil;

                $status = '2';

                if($remark != '') {
                    $data_remark = $this->Remark->query("SELECT id FROM remarks WHERE id=".$remark);

                    if(!empty($data_remark)){
                        $remark_id = $this->request->data['InventoryPurchaseRequisition']['remark'];
                    }else{
                        $this->Remark->create();

                        $remark_data = array('name' => $this->request->data['InventoryPurchaseRequisition']['new_remark']);

                        $this->Remark->save($remark_data);
                        
                        $remark_id = $this->Remark->getLastInsertID();
                    }    
                } else {
                    $remark_id = 0;
                }
                
                if($terms != '') {
                     $data_term = $this->TermOfPayment->query("SELECT id FROM term_of_payments WHERE id=".$terms);

                    if(!empty($data_term)){
                        $term_of_payment_id = $terms;
                    }else{
                        $this->TermOfPayment->create();

                        $term_data = array('name' => $this->request->data['InventoryPurchaseRequisition']['new_term'], 'created' => date('Y-m-d H:i:s'));

                        $this->TermOfPayment->save($term_data);
                        
                        $term_of_payment_id = $this->TermOfPayment->getLastInsertID();
                    }    
                } else {
                    $term_of_payment_id = 0;
                }
                

                if(empty($this->request->data['InventoryPurchaseRequisition']['sale_job_id'])){
                    $this->request->data['InventoryPurchaseRequisition']['sale_job_id'] = 0;
                }

                if(empty($this->request->data['InventoryPurchaseRequisition']['sale_job_child_id'])){
                    $this->request->data['InventoryPurchaseRequisition']['sale_job_child_id'] = 0;
                }

                $this->request->data['InventoryPurchaseRequisition']['pr_no'] = $PR_no;
                $this->request->data['InventoryPurchaseRequisition']['remark_id'] = $remark_id; 
                $this->request->data['InventoryPurchaseRequisition']['group_id'] = $_SESSION['Auth']['User']['group_id'];
                $this->request->data['InventoryPurchaseRequisition']['general_purchase_requisition_type_id'] = 1;

                $count = count($_POST['inventory_item_id']);
                if($count == 0) {
                    $this->Session->setFlash(__('Please add item to purchase.'), 'error');
                    return $this->redirect(array('action' => 'add'));
                }

                if ($this->InventoryPurchaseRequisition->save($this->request->data)) {
                    $ivn_pr = $this->InventoryPurchaseRequisition->getLastInsertID();
                    

                    $checkbudget = 0;
                
                    for($i=0; $i<$count; $i++) {
                        $this->InventoryPurchaseRequisitionItem->create();
                        $item_id = $_POST['inventory_item_id'][$i];
                        $s_id = $_POST['supplier_id'][$i];
                        
                        $this->loadModel('InventorySupplierItems');
                        $supplier_item_ids = $this->InventorySupplierItems->query("SELECT id, inventory_item_id FROM inventory_supplier_items WHERE inventory_item_id=".$item_id." AND inventory_supplier_id=".$s_id);
                      
                        if($supplier_item_ids) {
                            $supplier_item_id = $supplier_item_ids[0]['inventory_supplier_items']['id'];
                        } else {
                            $supplier_item_id = 0;
                        } 
                     
                        $general_currency_id = $_POST['general_currency_id'][$i];
                        $qt = $_POST['quantity'][$i];
                        $gu = $_POST['general_unit'][$i];
                        $ppunit = $_POST['unit_price'][$i];
                        $amount = $qt * $ppunit;
                        $tax = $_POST['tax'][$i];
                        $remark_item = $_POST['remark'][$i];

                        $this->loadModel('GeneralCurrency');

                        $currency = $this->GeneralCurrency->find('first', array(
                            'conditions' => array(
                                'GeneralCurrency.id' => $general_currency_id
                                )
                            ));

                        $total_rm = $amount * $currency['GeneralCurrency']['rate'];

                        $discount = $_POST['discount'][$i];

                        if($discount == null){
                            $discount = '0';
                        }

                        $discount_type = $_POST['discount_type'][$i];

                        if($discount_type == '0'){
                            $amount = $amount - $discount;
                        }else{
                            $amount = $amount - ($amount * ($discount/100));
                        }

                        $approval_role = 'HOS';
                     

                        if($tax != NULL){
                            $amount = $amount;
                            $tax = $tax;
                        }else{
                            $amount = $amount;
                            $tax = '0';
                        } 

                        $data = array('inventory_supplier_id' => $s_id, 'inventory_purchase_requisition_id' => $ivn_pr, 'inventory_item_id' => $item_id, 'inventory_supplier_item_id' => $supplier_item_id, 'quantity' => $qt, 'general_unit_id' => $gu, 'price_per_unit' => $ppunit, 'amount' => $amount, 'discount' => $discount, 'discount_type' => $discount_type, 'tax' => $tax, 'remark' => $remark_item, 'general_currency_id' => $general_currency_id, 'total_rm' => $total_rm);

                        $checkbudget += $total_rm;

                        $this->InventoryPurchaseRequisitionItem->save($data);
                    }
                    //exit;
                    $checkbudget = $checkbudget;

                    if($checkbudget > $a_budget){
                        $this->InventoryPurchaseRequisition->create();
                        $data_new_pr = array(
                            'id'        => $ivn_pr,
                            'created'   => $created,
                            'dateline'  => $dateline,
                            'status'    => '10'
                        );
                        $this->InventoryPurchaseRequisition->save($data_new_pr);
                        $this->Session->setFlash(__('Insufficient Budget. The inventory purchase requisition has been saved.'), 'success');
                        return $this->redirect(array('action' => 'index'));
                    } else {
                         $data_new_pr = array(
                            'id'        => $ivn_pr,
                            'created'   => $created,
                            'dateline'  => $dateline,
                            'status'    => '1'
                        );

                        if($this->InventoryPurchaseRequisition->save($data_new_pr)){
                            $request_id = $this->InventoryPurchaseRequisition->getLastInsertId();
                            //echo $request_id;
                            //exit;
                            // Find Approval
                            $group_id =$this->Session->read('Auth.User.group_id');
                            $this->loadModel('Approval');
                            $approvals = $this->Approval->find('all', array(
                                'conditions' => array(
                                    'Approval.name' => 'Purchase Requisition',
                                    'Approval.group_id' => $group_id,
                                    'Approval.role' => $approval_role
                                    )
                                ));
                            if($approvals) {
                                foreach ($approvals as $approval) {
                                    $data = array(
                                        'to' => $approval['User']['email'],
                                        'template' => 'purchase_requisition',
                                        'subject' => 'Purchase Requisition Require Your Action ' . $PR_no,
                                        'content' => array(
                                            'pr_number' => $PR_no,
                                            'from' => $this->Session->read('Auth.User.username'),
                                            'username' => $approval['User']['username'],  
                                            'link' => 'inventory_purchase_requisitions/verifyview/'.$request_id
                                            )
                                        );
                                    $this->send_email($data);
                                } 
                            }  

                            $this->Session->setFlash(__('The inventory purchase requisition has been saved.'), 'success');
                            return $this->redirect(array('action' => 'index'));
                        } else{
                            $this->Session->setFlash(__('The inventory purchase requisition could not be saved. Please, try again.'), 'error');
                        }
                    }           
                } else {
                    $this->Session->setFlash(__('The inventory purchase requisition could not be saved. Please, try again.'), 'error');
                } 
        }

        $users = $this->InventoryPurchaseRequisition->User->find('list'); 
        $generalPurchaseRequisitionTypes = $this->InventoryPurchaseRequisition->GeneralPurchaseRequisitionType->find('list');
        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');
        $this->loadModel('InventoryItems');
        $items = $this->InventoryItems->find('list', array(
            'fields' => array('InventoryItems.id'),
            'fields' => array('InventoryItems.name') 
        ));

        $this->loadModel('InventoryLocation');
        $locations = $this->InventoryLocation->find('list', array(
            'order' => 'InventoryLocation.name ASC'
        ));

        $this->loadModel('InventorySuppliers');
        $supplier = $this->InventorySuppliers->find('list', array(
            'fields' => array('InventorySuppliers.id'),
            'fields' => array('InventorySuppliers.name'),
            'group' => 'InventorySuppliers.name' 
        ));

        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('InventorySupplierItems');
        $supplierItem = $this->InventorySupplierItems->find('all');

        $this->loadModel('AccountDepartmentBudget'); 
 
        $budgets = $this->AccountDepartmentBudget->find('list', array(
            'conditions' => array(
                'AccountDepartment.group_id' => $_SESSION['Auth']['User']['group_id'],
                'AccountDepartmentBudget.year' => date('Y'),
                'AccountDepartmentBudget.type' => 1,
                'AccountDepartmentBudget.parent_id !=' => 0
            ),
            'recursive' => 0
        ));

        $this->set(compact('budgets'));
        $this->set(compact('locations'));

        $this->loadModel('TermOfDelivery');
        $tod = $this->TermOfDelivery->find('list');

        $other = "Others...";
        array_push($terms, $other);

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $other = "Others...";
        array_push($remark, $other);

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $other = "Others...";
        array_push($warranty, $other);

        $po_type = array('1'=>'PO', '2'=>'Non-PO');

        $type = array('1'=>'Auto', '2'=>'Manual');
        $this->set(compact('users', 'generalPurchaseRequisitionTypes', 'type', 'generalUnits', 'items', 'supplier', 'supplierItem', 'terms', 'remark', 'warranty', 'po_type', 'tod'));
    }

    private function get_role($category, $nilai){
        $this->loadModel('InternalLofa');
        $roles = $this->InternalLofa->find('all', array(
            'conditions' => array(
                'InternalLofa.category' => $category
            ),
            'recursive' => -1
        ));

        foreach ($roles as $role) {
            if($nilai > $role['InternalLofa']['price_min'] && $nilai < $role['InternalLofa']['price_max']){
                $r = $role['InternalLofa']['role'];
            }else{
                $r = $role['InternalLofa']['role'];
            }
        }
        return $r;
    }

    private function get_groupbudget($id){
        $this->loadModel('AccountDepartmentBudgets');
        $array = array();

        $budgets = $this->AccountDepartmentBudgets->find('all', array(
            'conditions' => array(
                'AccountDepartmentBudgets.account_department_id' => $id,
                'AccountDepartmentBudgets.parent_id' => 0
            ),
            'recursive' => -1
        ));

        foreach ($budgets as $budget) {
            $array[] = array(
                'Parent' => $budget['AccountDepartmentBudgets'],
                'Child' => $this->get_childbudget($budget['AccountDepartmentBudgets']['id'])
            );
        }

        return $array;
    }

    private function get_childbudget($id){
        $this->loadModel('AccountDepartmentBudgets');
        $child = $this->AccountDepartmentBudgets->find('all', array(
            'conditions' => array(
                'AccountDepartmentBudgets.parent_id' => $id 
            ),
            'recursive' => -1
        ));

        return $child;
    }

    public function verifyview($id = null) {
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid purchase requisition request'));
        }

        $prs = $this->InventoryPurchaseRequisition->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisition.id' => $id
            ),
            'recursive' => 2
        ));

        $amount = 0;
        foreach ($prs[0]['InventoryPurchaseRequisitionItem'] as $pr) {
           $amount += + $pr['amount'];
        }

        $this->set('amount', $amount);

        $group = $this->Session->read('Auth.User.group_id');

        $pr = $this->InventoryPurchaseRequisition->find('first', array(
                'conditions' => array(
                    'InventoryPurchaseRequisition.id' => $id
                ) 
            ));

        if($pr['InventoryPurchaseRequisition']['pr_type'] == 1) {
            $cat = 'CAPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 2) {
            $cat = 'OPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 3 || $pr['InventoryPurchaseRequisition']['pr_type'] == 4) {
            $cat = 'SERVING_CLIENT';
        }

        $this->loadModel('InternalLofa');
        $this->loadModel('User');

        $lofas = $this->InternalLofa->find('all', array(
            'conditions' => array(
                'InternalLofa.category' => $cat
                )
            ));

        $this->set('lofas', $lofas);

        $intlofa = $this->InternalLofa->find('first', array(
            'conditions' => array(
                'InternalLofa.category' => $cat,
                'InternalLofa.price_min <=' => $amount,
                'InternalLofa.price_max >=' => $amount
                )
            ));
 
        // Approval remark lists
        $this->loadModel('InventoryPurchaseRequisitionRemark');

        $approvalremarks = $this->InventoryPurchaseRequisitionRemark->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionRemark.inventory_purchase_requisition_id' => $id
                ),
            'order' => 'InventoryPurchaseRequisitionRemark.id DESC'
            ));
        $this->set('approvalremarks', $approvalremarks); 
        // End approval remark

        // If not admin
        if($group != 1) {
            $this->loadModel('Approval');
            $approval = $this->Approval->find('first', array(
                'conditions' => array(
                    'Approval.user_id' => $this->user_id,
                    'Approval.name' => 'Purchase Requisition'
                    )
                )); 
            if($approval) {
                $conditions['User.group_id'] = $approval['Approval']['group_id'];

                $options = array('conditions' => array(
                    'InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id,
                    'User.group_id' => $approval['Approval']['group_id']
                    )); 

                if($approval['Approval']['role'] == 'HOS') {
                    $status = array(2 => 'Reject', 3 => 'Verify'); 
                }
                if($approval['Approval']['role'] == 'HOD') {
                    if($intlofa ['InternalLofa']['role'] == 'MD') {
                        $status = array(6 => 'Reject', 7 => 'Approve (It will sent to MD for approval)');
                    } else {
                        $status = array(6 => 'Reject', 9 => 'Approve');
                    }
                    
                }
                if($approval['Approval']['role'] == 'MD') {
                    $status = array(8 => 'Reject', 9 => 'Approve');
                } 
                $role = $approval['Approval']['role'];
            } else {
                //$this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
                //return $this->redirect(array('action' => 'index'));
            }
        } else {
            $options = array('conditions' => array('InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id), 'recursive' => 2);

            // Admin approve as MD
            $status = array(8 => 'Reject', 9 => 'Approve (Admin will skip LOFA)');
            $role = 'Admin';
        }

        $options = array('conditions' => array(
            'InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id 
            )); 

        
        $this->set('status', $status);

        // View current budget  
        $this->loadModel('InventoryPurchaseRequisitionItem');
        $waitings = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'fields' => array(
                'SUM(InventoryPurchaseRequisitionItem.total_rm) AS total_waiting'
                ),
            'conditions' => array(
                'InventoryPurchaseRequisition.status' => array(1, 3, 5, 7, 9),
                'InventoryPurchaseRequisition.account_department_budget_id' => $pr['InventoryPurchaseRequisition']['account_department_budget_id'],
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id !=' => $id
                )
            ));
        $total_waiting = 0;
        foreach ($waitings as $waiting) {
            $total_waiting = $waiting[0]['total_waiting'];
        }
        $this->set('total_waiting', $total_waiting);

        if ($this->request->is(array('put', 'post'))) { 
              
            

            $stat = $this->request->data['InventoryPurchaseRequisition']['status'];
            $total_amount = $this->request->data['InventoryPurchaseRequisition']['total'];

            if($this->InventoryPurchaseRequisition->save($this->request->data)) { 
                
                $this->request->data['InventoryPurchaseRequisitionRemark']['user_id'] = $this->user_id;
                $this->request->data['InventoryPurchaseRequisitionRemark']['inventory_purchase_requisition_id'] = $id;
                $this->request->data['InventoryPurchaseRequisitionRemark']['status'] = $stat;
                $this->request->data['InventoryPurchaseRequisitionRemark']['created'] = $this->date;
                $this->request->data['InventoryPurchaseRequisitionRemark']['type'] = $role;
                
                $this->loadModel('InventoryPurchaseRequisitionRemark');
                if(!$this->InventoryPurchaseRequisitionRemark->save($this->request->data['InventoryPurchaseRequisitionRemark'])) {
                    die('Error 1026. Please contact administrator with code PR1026');
                }

                // HOS Approved -> Waiting PRC verify
                if($stat == 3) { 

                    // SALAH!, send to PRC

                    $procs = $this->User->find('all', array(
                        'conditions' => array(
                            'User.group_id' => 11
                            )
                        )); 
                    $mail = array();
                    foreach ($procs as $user) {
                        $data = array(
                            'to' => $user['User']['email'],
                            'template' => 'purchase_requisition',
                            'subject' => 'Purchase Requisition Require Your Action: ' . $pr['InventoryPurchaseRequisition']['pr_no'],
                            'content' => array(
                                'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                'from' => $pr['User']['username'] . ' ('.$pr['User']['firstname'].')',
                                'username' => $user['User']['username'],  
                                'link' => 'inventory_purchase_requisitions/procurementview/'.$id 
                                )
                            );
                        $this->send_email($data);
                    }  
                }
                if($stat == 7) {
                    // Find lofa first
                    $lofa = $this->InternalLofa->find('first', array(
                        'conditions' => array(
                            'InternalLofa.price_min <=' => $total_amount,
                            'InternalLofa.price_max >=' => $total_amount,
                            'InternalLofa.category' => $cat
                            )
                        ));

                    if($lofa['InternalLofa']['role'] == 'HOD') {
                        // UPDATE STATUS = 9
                        $this->_update_pr_status($id, $stat);
                        // SEND EMAIL TO PROCUREMENT cc to user 
                        $procs = $this->User->find('all', array(
                            'conditions' => array(
                                'User.group_id' => 11
                                )
                            ));
                        $mail = array();
                        foreach ($procs as $user) {
                            $data = array(
                                'to' => $user['User']['email'],
                                'template' => 'purchase_requisition',
                                'subject' => 'Purchase Requisition Require Your Action: ' . $pr['InventoryPurchaseRequisition']['pr_no'],
                                'content' => array(
                                    'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                    'from' => $this->Session->read('Auth.User.username'),
                                    'username' => $user['User']['username'],  
                                    'link' => 'inventory_purchase_requisitions/procurementview/'.$id,
                                    'department' => $lofa['InternalLofa']['role']
                                    )
                                );
                            $this->send_email($data);
                        }  
                    } else {
                        // find MD
                        $this->loadModel('Approval');
                        $approvals = $this->Approval->find('all', array(
                            'conditions' => array(
                                'Approval.name' => 'Purchase Requisition', 
                                'Approval.role' => 'MD',
                                )
                            ));
                        if($approvals) {
                            foreach ($approvals as $approval) {
                                $data = array(
                                    'to' => $approval['User']['email'],
                                    'template' => 'purchase_requisition',
                                    'subject' => 'Purchase Requisition Require Your Approval: ' . $pr['InventoryPurchaseRequisition']['pr_no'],
                                    'content' => array(
                                        'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                        'from' => $this->Session->read('Auth.User.username'),
                                        'username' => $approval['User']['username'],  
                                        'link' => 'inventory_purchase_requisitions/verifyview/'.$id
                                        )
                                    );
                                $this->send_email($data);
                            }  
                        }
                    }

                    // Send mail to end user
                    $data = array(
                        'to' => $pr['User']['email'],
                        'template' => 'purchase_requisition_status',
                        'subject' => 'Purchase Requisition ' . $pr['InventoryPurchaseRequisition']['pr_no'] . ' Status',
                        'content' => array(
                            'status' => 'Approved',
                            'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                            'from' => $this->Session->read('Auth.User.username'),
                            'username' => $pr['User']['username'],  
                            'link' => 'inventory_purchase_requisitions/view/'.$id,
                            'department' => $lofa['InternalLofa']['role']
                            )
                        );
                    $this->send_email($data);
                }
                if($stat == 9) { 
                    $this->_update_pr_status($id, $stat);
                    // SEND EMAIL TO PROCUREMENT cc to user 
                    $procs = $this->User->find('all', array(
                        'conditions' => array(
                            'User.group_id' => 11
                            )
                        ));
                    $mail = array();
                    foreach ($procs as $proc) {
                        $data = array(
                            'to' => $proc['User']['email'],
                            'template' => 'purchase_requisition',
                            'subject' => 'Purchase Requisition Require Your Action: ' . $pr['InventoryPurchaseRequisition']['pr_no'],
                            'content' => array(
                                'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                'from' => $this->Session->read('Auth.User.username'),
                                'username' => $proc['User']['username'],  
                                'link' => 'inventory_purchase_requisitions/procurementview/'.$id,
                                'department' => 'MD'
                                )
                            );
                        $this->send_email($data);
                    }  

                    $data = array(
                        'to' => $pr['User']['email'],
                        'template' => 'purchase_requisition',
                        'subject' => 'Purchase Requisition ' . $pr['InventoryPurchaseRequisition']['pr_no'] . ' Status',
                        'content' => array(
                            'status' => 'Approved',
                            'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                            'from' => $this->Session->read('Auth.User.username'),
                            'username' => $pr['User']['username'],  
                            'link' => 'inventory_purchase_requisitions/view/'.$id,
                            'department' => 'MD'
                            )
                        );
                    $this->send_email($data);
                }  

                // Reject
                if($stat == 2 || $stat == 6 || $stat == 8) {   
                    $this->_update_pr_status($id, $stat);
                    $data = array(
                        'to' => $pr['User']['email'],
                        'template' => 'purchase_requisition',
                        'subject' => 'Purchase Requisition ' . $pr['InventoryPurchaseRequisition']['pr_no'] . ' Status',
                        'content' => array(
                            'status' => 'Rejected',
                            'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                            'from' => $this->Session->read('Auth.User.username'),
                            'username' => $pr['User']['username'],  
                            'link' => 'inventory_purchase_requisitions/view/'.$id 
                            )
                        );
                    $this->send_email($data);
                } 

                $this->Session->setFlash(__('Purchase Requisition status has been saved.'), 'success');
                return $this->redirect(array('action' => 'verifyindex'));
            } else {
                $this->Session->setFlash(__('The Purchase Requisition status cannot be saved.'), 'error');
            }
        } 
        
        $this->set('inventoryPurchaseRequisitions', $this->InventoryPurchaseRequisition->find('first', $options));

        $items =  $this->InventoryPurchaseRequisition->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id
                )
            ));
        $this->set('items', $items);
    }

    private function _update_pr_status($id, $status) {
        $update = $this->InventoryPurchaseRequisition->updateAll(
            array(
                'InventoryPurchaseRequisition.status' => "$status"
                ),
            array(
                'InventoryPurchaseRequisition.id' => $id
                )
            );
        if(!$update) {
            return false;
        }
        return true;
    }

    public function verifyviewprc($id = null) {
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid purchase requisition request'));
        }

        $prs = $this->InventoryPurchaseRequisition->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisition.id' => $id
            ),
            'recursive' => 2
        ));

        $amount = 0;
        foreach ($prs[0]['InventoryPurchaseRequisitionItem'] as $pr) {
           $amount = $amount + $pr['amount'];
        }

        $this->set('amount', $amount);

        $group = $this->Session->read('Auth.User.group_id');
        if($group != 1) {
            $this->loadModel('Approval');
            $approval = $this->Approval->find('first', array(
                'conditions' => array(
                    'Approval.user_id' => $this->user_id,
                    'Approval.name' => 'Purchase Requisition'
                    )
                )); 
            if($approval) {
                $conditions['User.group_id'] = $approval['Approval']['group_id'];
                $options = array('conditions' => array(
                    'InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id,
                    'User.group_id' => $approval['Approval']['group_id']
                    ));
            } else {
                $this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
        } else {
            $options = array('conditions' => array('InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id), 'recursive' => 2);
        }

        if ($this->request->is(array('put', 'post'))) { 
            
            $PR_no = $this->request->data['InventoryPurchaseRequisition']['pr_no'];
            $this->request->data['InventoryPurchaseRequisition']['status'] = 3;
            
            if($this->InventoryPurchaseRequisition->save($this->request->data)) { 
                if($this->request->data['InventoryPurchaseRequisition']['status'] == 3){
                    $request_id = $this->request->data['InventoryPurchaseRequisition']['id'];
                     
                    $group_id = 11;
                    $this->loadModel('Approval');
                    $approvals = $this->Approval->find('all', array(
                        'conditions' => array(
                            'Approval.name' => 'Purchase Requisition',
                            'Approval.group_id' => $group_id,
                            )
                        ));
                    if($approvals) {
                        foreach ($approvals as $approval) {
                            $data = array(
                                'to' => 'mursyidi88@gmail.com',
                                'template' => 'purchase_requisition',
                                'subject' => 'Purchase Requisition Require Your Approval ' . $PR_no,
                                'content' => array(
                                    'pr_number' => $PR_no,
                                    'from' => $this->Session->read('Auth.User.username'),
                                    'username' => $approval['User']['username'],  
                                    'link' => 'inventory_purchase_requisitions/verifyviewprc/'.$request_id
                                    )
                                );
                            $this->send_email($data);
                        } 
                    }
                }

                $this->Session->setFlash(__('The material request status has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The material request status cannot be saved.'), 'error');
            }
        }

        
        $this->set('inventoryPurchaseRequisitions', $this->InventoryPurchaseRequisition->find('first', $options));
    }

    public function ajaxitem() {
        $this->autoRender = false;
        $this->layout = 'ajax'; 
        if(isset($this->request->query['term'])) {
            $term = $this->request->query['term'];
            $conditions = array();

            $conditions['OR']['SaleJobChild.station_name LIKE'] = '%'.$term.'%'; 
            $conditions['OR']['SaleJobChild.name LIKE'] = '%'.$term.'%'; 

            $items = $this->SaleJob->SaleJobChild->find('all', array(
                'conditions' => $conditions,
                'group' => array('SaleJobChild.id'),
                'order' => array('SaleJobChild.id' => 'ASC')
                ));
             
            $json = array();
            foreach ($items as $item) {
                $json[] = array(
                    'id' => $item['SaleJob']['id'], 
                    'sale_job_child_id' => $item['SaleJobChild']['id'],
                    'name' => $item['SaleJobChild']['station_name'], 
                    'station' => $item['SaleJobChild']['name'], 
                    'customer' => $this->get_customer($item['SaleJob']['customer_id']),
                    'customer_id' => $item['SaleJob']['customer_id'], 
                    'sale_tender_id' => $item['SaleJob']['sale_tender_id'], 
                    'sale_quotation_id' => $item['SaleJob']['sale_quotation_id'],
                    //'has_sub' => $this->get_sub_station($item['SaleJob']['id'])
                    ); 
            }
            echo json_encode($json);    
        }
        
    }

    public function ajaxitemgeneral() {
        $this->autoRender = false;
        $this->layout = 'ajax'; 
        if(isset($this->request->query['supplier_id'])) {
            $id = (int)$this->request->query['supplier_id'];

            $this->loadModel('InventoryItem');
            $itemsgeneral = $this->InventoryItem->find('all', array('recursive' => -1));

            //print_r($itemsgeneral);
            $json = array();
            foreach ($itemsgeneral as $item) {
                $json[] = array('id' => $item['InventoryItem']['id'], 'name' => $item['InventoryItem']['name'], 'price_per_unit' => $item['InventoryItem']['unit_price']
                    );
            }
            //$json[] = print_r($itemsgeneral);
            /*for($i=0; $i<count($itemsgeneral); $i++){
                $idg = $itemsgeneral[$i]['inventory_items']['id'];

                $this->loadModel('InventorySupplierItem');
                $items = $this->InventorySupplierItem->query("SELECT * FROM inventory_supplier_items WHERE inventory_item_id=".$idg." AND inventory_supplier_id=".$id);

                if(empty($items)){
                    $json[] = array('id' => $itemsgeneral[$i]['inventory_items']['id'], 'name' => $itemsgeneral[$i]['inventory_items']['name'], 'price_per_unit' => $itemsgeneral[$i]['inventory_items']['unit_price'], 'general_unit' => $itemsgeneral[$i]['inventory_items']['general_unit_id']
                    );
                }
            }*/

            echo json_encode($json); 
        }
        
    }

    public function ajaxitemdetails() {
        $this->autoRender = false;
        $this->layout = 'ajax'; 
        if(isset($this->request->query['id'])) {
            $id = (int)$this->request->query['id'];
            $this->loadModel('InventoryItem');
            $items = $this->InventoryItem->query("SELECT * FROM inventory_items WHERE id=".$id);
            $json = array();

            $json[] = $items[0]['inventory_items']; 
            echo json_encode($json);   
        }
        
    }

    public function ajaxbudget() {
        $this->autoRender = false;
        //$this->layout = 'ajax'; 
        $this->loadModel('AccountDepartmentBudget');
        if(isset($_POST['id'])) {
             
            $budgets = $this->AccountDepartmentBudget->find('first', array(
                'conditions' => array(
                    'AccountDepartmentBudget.id' => $_POST['id']
                    ))
                );

            $total = $budgets['AccountDepartmentBudget']['balance'];
            echo $total;         
        } 
    }

    private function get_currency_rate($currency_id) {
        $this->loadModel('GeneralCurrency');
        $curr = $this->GeneralCurrency->find('first', array(
            'conditions' => array(
                'GeneralCurrency.id' => $currency_id
                )
            ));
        return $curr;
    }

    public function edit($id = null) { 

        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'));
        }
        if ($this->request->is(array('post', 'put'))) { 

            //var_dump($this->request->data);

            $this->loadModel('InventoryPurchaseRequisitionItem');
            $this->loadModel('Remark');
            $this->loadModel('TermOfPayment');
            $this->loadModel('Warranty');  
             
            $term_of_payment_id = $_POST['term_of_payment_id'];
            $warranty = $this->request->data['InventoryPurchaseRequisition']['warranty_id'];

            $user_id = $this->user_id;
            $group_id = $_SESSION['Auth']['User']['group_id']; 

            if($warranty != '0'){
                $data_warranty = $this->Warranty->query("SELECT id FROM warranties WHERE id=".$warranty);

                if(!empty($data_warranty)){
                    $warranty_id = $this->request->data['InventoryPurchaseRequisition']['warranty_id'];
                }else{
                    $this->Warranty->create();

                    $warranty_data = array('name' => $this->request->data['InventoryPurchaseRequisition']['new_warranty']);

                    $this->Warranty->save($warranty_data);
                
                    $warranty_id = $this->Warranty->getLastInsertID();
                }
            }else{
                $warranty_id = 0;
            }

            $data = $this->InventoryPurchaseRequisitionItem->query("SELECT id FROM inventory_purchase_requisition_items ORDER BY id DESC");

            if($this->request->data['InventoryPurchaseRequisition']['remark_id'] != '0'){
                $data_remark = $this->Remark->query("SELECT id FROM remarks WHERE id=".$this->request->data['InventoryPurchaseRequisition']['remark_id']);
 
                if(!empty($data_remark) && $this->request->data['InventoryPurchaseRequisition']['remark_id'] != 0){
                    $remark_id = $this->request->data['InventoryPurchaseRequisition']['remark_id'];
                }else{
                    $this->Remark->create();

                    $remark_data = array('name' => $this->request->data['InventoryPurchaseRequisition']['new_remark']);

                    $this->Remark->save($remark_data);
                
                    $remark_id = $this->Remark->getLastInsertID();
                }
            }else{
                $remark_id = 0;
            }

            $data_term = $this->TermOfPayment->query("SELECT id FROM term_of_payments WHERE id=".$_POST['term_of_payment_id']);
            
            if(!empty($data_term)){
                $term_of_payment_id = $_POST['term_of_payment_id'];
            }else{
                $this->TermOfPayment->create();

                $term_data = array('name' => $this->request->data['InventoryPurchaseRequisition']['new_term'], 'created' => date('Y-m-d H:i:s'));

                $this->TermOfPayment->save($term_data);
                
                $term_of_payment_id = $this->TermOfPayment->getLastInsertID();
            }

            $PR_no = $this->request->data['InventoryPurchaseRequisition']['pr_no'];
            
             
            $dateline = $this->request->data['InventoryPurchaseRequisition']['dateline'];
            $this->request->data['InventoryPurchaseRequisition']['modified'] = $this->date; 
            $this->request->data['InventoryPurchaseRequisition']['remark_id'] = $remark_id;
            $this->request->data['InventoryPurchaseRequisition']['term_of_payment_id'] = $term_of_payment_id; 
            $this->request->data['InventoryPurchaseRequisition']['warranty_id'] = $warranty_id;  

            if ($this->InventoryPurchaseRequisition->save($this->request->data)) {
                
                $this->InventoryPurchaseRequisitionItem->deleteAll(array(
                    'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id
                    )
                );

                $count = count($_POST['inventory_item_id']);       
                $checkbudget = 0;     
                $approval_role = 'HOS';      
            
                for($i=0; $i<$count; $i++){
                    $this->InventoryPurchaseRequisitionItem->create();
                    $this->loadModel('InventorySupplierItems');
                    $inv_id = $_POST['inventory_item_id'][$i];
                    $supplier = $_POST['supplier_id'][$i];
                    
                    
                    $supplier_item_ids = $this->InventorySupplierItems->query("SELECT id, inventory_item_id, inventory_supplier_id FROM inventory_supplier_items WHERE inventory_item_id=".$inv_id." AND inventory_supplier_id=".$supplier);
                     

                    $supplier_item_id = $supplier_item_ids[0]['inventory_supplier_items']['id'];
                 

                    $qt = $_POST['quantity'][$i];
                    $gu = $_POST['general_unit'][$i];
                    $ppunit = $_POST['unit_price'][$i];
                    $amount = $qt * $ppunit;
                    $tax = $_POST['tax'][$i];
                    $remark_item = $_POST['remark'][$i];

                    $currency_id = $_POST['currency_id'][$i];

                    $discount_type = $_POST['discount_type'][$i];
                    if($discount_type != 'x'){
                        $discount_type_id = $discount_type;
                        $discount = $_POST['discount'][$i];

                        if($discount_type == '0'){
                            $amount = $amount - $discount;
                        }else{
                            $amount = $amount - ($amount * ($discount/100));
                        }
                    }else{
                        $discount_type_id = '0';
                        $discount = '0';
                    }

                    if($tax != NULL){
                        $amount = $amount;
                        $tax = $tax;
                    }else{
                        $amount = $amount;
                        $tax = '0';
                    }

                    $ppunit = $_POST['unit_price'][$i];
                    $currency = $this->get_currency_rate($currency_id);

                    $total_rm = $currency['GeneralCurrency']['rate'] * $amount;

                    $data = array(
                        'inventory_supplier_id' => $supplier, 
                        'inventory_purchase_requisition_id' => $id, 
                        'inventory_item_id' => $inv_id, 
                        'inventory_supplier_item_id' => $supplier_item_id, 
                        'quantity' => $qt, 
                        'general_unit_id' => $gu, 
                        'price_per_unit' => $ppunit, 
                        'amount' => $amount, 
                        'discount' => $discount, 
                        'discount_type' => $discount_type_id, 
                        'tax' => $tax, 
                        'remark' => $remark_item,
                        'general_currency_id' => $currency_id,
                        'total_rm' => $total_rm
                        ); 

                    $this->InventoryPurchaseRequisitionItem->save($data);

                    $checkbudget += $total_rm;
                    
                } 
                
                if($checkbudget > $_POST['a_budget']){
                     
                    $data_new_pr = array(
                        'id'        => $id,
                        //'created'   => date('Y-m-d H:i:s'),
                        //'term_of_payment_id' => $term_of_payment_id,
                        //'remark_id' => $remark_id,
                        //'warranty_id' => $warranty_id,
                        //'dateline'  => $dateline,
                        'status'    => '10'
                    );
                    $this->InventoryPurchaseRequisition->save($data_new_pr);
                    $this->Session->setFlash(__('Insufficient Budget. The inventory purchase requisition has been saved.'), 'success');
                    return $this->redirect(array('action' => 'index'));
                }else{ 
                    $data_new_pr = array(
                        'id'        => $id,
                        //'created'   => $created,
                        //'term_of_payment_id' => $term_of_payment_id,
                        //'remark_id' => $remark_id,
                        //'warranty_id' => $warranty_id,
                        //'dateline'  => $dateline,
                        'status'    => '1'
                    );

                    if($this->InventoryPurchaseRequisition->save($data_new_pr)){
                
                         
                        $group_id =$this->Session->read('Auth.User.group_id');
                        $this->loadModel('Approval');
                        $approvals = $this->Approval->find('all', array(
                            'conditions' => array(
                                'Approval.name' => 'Purchase Requisition',
                                'Approval.group_id' => $group_id,
                                'Approval.role' => $approval_role
                                )
                            ));
                        if($approvals) {
                            foreach ($approvals as $approval) {
                                $data = array(
                                    'to' => $approval['User']['email'],
                                    'template' => 'purchase_requisition',
                                    'subject' => 'Purchase Requisition Require Your Verification: ' . $PR_no,
                                    'content' => array(
                                        'pr_number' => $PR_no,
                                        'from' => $this->Session->read('Auth.User.username'),
                                        'username' => $approval['User']['username'],  
                                        'link' => 'inventory_purchase_requisitions/verifyview/'.$id
                                        )
                                    );
                                $this->send_email($data);
                            } 
                        } else {
                            $this->Session->setFlash(__('The Purchase Requisition has been saved. But nobody will verify your'), 'success');
                            return $this->redirect(array('action' => 'index'));
                        }
                    }
                  
                    $this->Session->setFlash(__('The inventory purchase requisition has been saved.'), 'success');
                    return $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('The inventory purchase requisition could not be saved. Please, try again.'), 'error');
            }
        } else { 
        }

        $options = array('conditions' => array('InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id), 'recursive'=>2);

        $pr = $this->InventoryPurchaseRequisition->find('first', $options);
       
        $arr = array();
        foreach ($pr['InventoryPurchaseRequisitionItem'] as $p) {
            $arr[] = array(
                'id' => $p['id'],
                'inventory_purchase_requisition_id' => $p['inventory_purchase_requisition_id'],
                'quantity' => $p['quantity'],
                'general_unit_id' => $p['general_unit_id'],
                'inventory_item_id' => $p['inventory_item_id'],
                'inventory_supplier_item_id' => $p['inventory_supplier_item_id'],
                'inventory_item_category_id' => $p['inventory_item_category_id'],
                'inventory_supplier_id' => $p['inventory_supplier_id'],
                'price_per_unit' => $p['price_per_unit'],
                'amount' => $p['amount'],
                'discount' => $p['discount'],
                'discount_type' => $p['discount_type'],
                'tax' => $p['tax'],
                'remark' => $p['remark'],
                'InventoryPurchaseRequisition' => $p['InventoryPurchaseRequisition'],
                'GeneralUnit' => $p['GeneralUnit'],
                'InventoryItem' => $p['InventoryItem'],
                'InventorySupplier' => $p['InventorySupplier'],
                'supplierList' => $this->getsupplierlist($p['InventoryItem']['id'])
            );
        }
 

        $this->set('pr', $arr);

        $this->set('purchaseRequisition', $pr);
        $this->request->data = $pr;

        $users = $this->InventoryPurchaseRequisition->User->find('list'); 
        $generalPurchaseRequisitionTypes = $this->InventoryPurchaseRequisition->GeneralPurchaseRequisitionType->find('list');
        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');
        $this->loadModel('InventoryItems');
        $items = $this->InventoryItems->find('list', array(
            'fields' => array('InventoryItems.id'),
            'fields' => array('InventoryItems.name') 
        ));

        $this->loadModel('InventoryItemCategory');
        $inventoryItemCategories = $this->InventoryItemCategory->find('list', array(
            'fields' => array('InventoryItemCategory.id'),
            'fields' => array('InventoryItemCategory.name')
            
        ));

        $this->loadModel('InventorySuppliers');
        $supplier = $this->InventorySuppliers->find('list', array(
            'fields' => array('InventorySuppliers.id'),
            'fields' => array('InventorySuppliers.name')
            
        ));
         
        $id_supplier = $this->request->data['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id'];
        $this->loadModel('InventorySupplierItems');
        $supplierItem = $this->InventorySupplierItems->find('all', array(
            'conditions' => array('InventorySupplierItems.inventory_supplier_id' => $id_supplier)   
        ));

        $this->loadModel('AccountDepartmentBudget'); 

        // IF AUTO, FIND BUDGET FOR JOB
        if($pr['InventoryPurchaseRequisition']['general_purchase_requisition_type_id'] == 2 || $pr['InventoryPurchaseRequisition']['sale_job_child_id'] != 0) {
            $cond['AccountDepartmentBudget.sale_job_child_id'] = $pr['InventoryPurchaseRequisition']['sale_job_child_id'];
        }

        // If not admin, show only group budget
        //if($this->Session->read('Auth.User.group_id') != 1) {
            $cond['AccountDepartment.group_id'] = $this->Session->read('Auth.User.group_id');
        //} 

        $cond['AccountDepartmentBudget.parent_id !='] = 0;

        $budgets = $this->AccountDepartmentBudget->find('list', array(
            'conditions' => $cond,
            'recursive' => 0
        ));

        $this->set(compact('budgets')); 

        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list'); 

        $other = "Others...";
        array_push($terms, $other);

        $other = array("invitem" => array("id" => "xxx", "name" => "NEW ITEM"), "InventorySupplierItems" => array("id" => "xxx", "name" => "NEW ITEM"));
        array_push($supplierItem, $other);
 
        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $other = "Others...";
        array_push($remark, $other);

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('InventoryPurchaseRequisitionItem');
        $items_2 = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array('InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id) 
        ));

        $this->loadModel('TermOfDelivery');
        $tod = $this->TermOfDelivery->find('list');

        $other = "Others...";
        array_push($warranty, $other);

        $po_type = array('1'=>'PO', '2'=>'Non-PO');

        $type = array('1'=>'Auto', '2'=>'Manual');
        $this->set(compact('users', 'generalPurchaseRequisitionTypes', 'type', 'generalUnits', 'items', 'inventoryItemCategories', 'supplier', 'supplierItem', 'data_budget', 'terms', 'remark', 'warranty', 'items_2', 'po_type', 'tod'));
    }

 
    public function verifyindex($status = null) {
        
        if($status != null) {
            $status = explode(',', $status);
        }

        $role = $this->Session->read('Auth.User.group_id');
        $id = $this->Session->read('Auth.User.id');

        
        if(isset($_GET['search'])) {
            //print_r($_GET);
            //exit;
            if($_GET['pr_no'] != '') {
                $conditions['InventoryPurchaseRequisition.pr_no LIKE'] = $_GET['pr_no'];
                if($role != 1) {
                    $gid = $this->Session->read('Auth.User.group_id');

                    if($gid == 11){
                        $conditions['InventoryPurchaseRequisition.status'] = 9;
                    }else{
                        $user_id = $this->Session->read('Auth.User.id');
                        $conditions['InventoryPurchaseRequisition.user_id'] = $user_id;
                    }            
                } else {
                    $conditions['InventoryPurchaseRequisition.user_id !='] = 0;
                }
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseRequisition.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
                //$conditions['InventoryPurchaseRequisition.created <='] = $_GET['date_end'];
                if($role != 1) {
                    $gid = $this->Session->read('Auth.User.group_id');

                    if($gid == 11){
                        $conditions['InventoryPurchaseRequisition.status'] = 9;
                    }else{
                        $user_id = $this->Session->read('Auth.User.id');
                        $conditions['InventoryPurchaseRequisition.user_id'] = $user_id;
                    }            
                } else {
                    $conditions['InventoryPurchaseRequisition.user_id !='] = 0;
                }
            }

            if($_GET['status'] != 'x') {
                $conditions['InventoryPurchaseRequisition.status LIKE'] = $_GET['status'];
                if($role != 1) {
                    $gid = $this->Session->read('Auth.User.group_id');

                    if($gid == 11){
                        $conditions['InventoryPurchaseRequisition.status'] = 9;
                    }else{
                        $user_id = $this->Session->read('Auth.User.id');
                        $conditions['InventoryPurchaseRequisition.user_id'] = $user_id;
                    }            
                } else {
                    $conditions['InventoryPurchaseRequisition.user_id !='] = 0;
                }
            }

            if($_GET['pr_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions = '';
            }

            $this->request->data['InventoryPurchaseRequisition'] = $_GET;
        }
        
        
        $this->loadModel('Approval');
        $approvals = $this->Approval->find('all', array(
            'conditions' => array(
                'Approval.user_id' => $this->user_id, 
                'Approval.name' => 'Purchase Requisition' 
                )
            ));
        $app = array();
        $roles = array();
        foreach ($approvals as $approval) {
            $app[] = $approval['Approval']['group_id'];
            $roles[] = $approval['Approval']['role'];
        } 
        if($role != 1) {
            if($approvals) {

                if(in_array('HOS', $roles)) { 
                    $conditions['User.group_id'] = $app;
                    if($status != null) {
                        $conditions['InventoryPurchaseRequisition.status'] = $status;
                    } else {
                        $conditions['InventoryPurchaseRequisition.status'] = 1;
                    } 
                }
                if(in_array('HOD', $roles)) { 
                    $conditions['User.group_id'] = $app;
                    if($status != null) {
                        $conditions['InventoryPurchaseRequisition.status'] = $status;
                    } else {
                        $conditions['InventoryPurchaseRequisition.status'] = 5;
                    } 
                }
                if(in_array('MD', $roles)) { 
                    if($status != null) {
                        $conditions['InventoryPurchaseRequisition.status'] = $status;
                    } else {
                        $conditions['InventoryPurchaseRequisition.status'] = 7;
                    } 
                }
                 
            } else {
                $this->Session->setFlash(__('You are not authorize to view this area.'), 'error');
                return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
            }
        } else {
            // Admin
            if($status == null) {
                $conditions['InventoryPurchaseRequisition.status'] = 1;
            } else {
                $conditions['InventoryPurchaseRequisition.status'] = $status;
            }
        } 

        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order'=>'InventoryPurchaseRequisition.id DESC',
            'limit'=>$record_per_page);
        $this->set('inventoryPurchaseRequisitions', $this->Paginator->paginate()); 
    }

    private function get_item_by_supplier($supplier_id, $pr_id) {
        $this->loadModel('InventoryPurchaseRequisitionItem');
        $lists = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $pr_id,
                'InventoryPurchaseRequisitionItem.inventory_supplier_id' => $supplier_id
                ) 
            ));
        return $lists;
    }

    public function procurementcreatepo($id = null) {
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'));
        }
        $pr = $this->InventoryPurchaseRequisition->find('first', array(
            'conditions' => array(
                'InventoryPurchaseRequisition.id' => $id
                )
            ));

        
        if($this->request->is('post')) {
            $supplier = $this->request->data['supplier'];
            $group_id = $this->Session->read('Auth.User.group_id');
            $username = $this->Session->read('Auth.User.username');
            $this->loadModel('InventoryPurchaseOrder');
             

            $quantity = $this->request->data['quantity'];
            $general_unit_id = $this->request->data['general_unit_id'];
            $inventory_item_id = $this->request->data['inventory_item_id'];
            $inventory_supplier_item_id = $this->request->data['inventory_supplier_item_id'];  
            $price_per_unit = $this->request->data['price_per_unit'];
            $amount = $this->request->data['amount'];
            $discount = $this->request->data['discount'];
            $discount_type = $this->request->data['discount_type'];
            $tax = $this->request->data['tax'];
            $planning_item_jobs = $this->request->data['planning_item_jobs'];
            $delivery_remark = $this->request->data['delivery_remark'];
            $general_currency_id = $this->request->data['general_currency_id'];
            $total_rm = $this->request->data['total_rm'];
            $remark = $this->request->data['remark'];
            
            $email_po = '';

            $this->loadModel('InventoryPurchaseOrderItem');
            for($i = 0; $i < count($supplier); $i++) {

                $po = $this->InventoryPurchaseOrder->find('all', array(
                    'order' => 'InventoryPurchaseOrder.id DESC',
                    'recursive' => -1
                    ));
                $number = count($po) + 1;
                $po_no = $this->generate_code('PO', $number);

                $email_po .= $po_no . ' ';

                $this->InventoryPurchaseOrder->create();
                $header = array(
                    'user_id' => $this->user_id, 
                    'group_id' => $group_id, 
                    'general_purchase_requisition_type_id' => $pr['InventoryPurchaseRequisition']['general_purchase_requisition_type_id'],  
                    'created' => $this->date,  
                    'dateline' => $pr['InventoryPurchaseRequisition']['dateline'], 
                    'status' => 0, 
                    'po_no' => $po_no, 
                    'account_department_budget_id' => $pr['InventoryPurchaseRequisition']['account_department_budget_id'], 
                    'inventory_supplier_id' => $supplier[$i], 
                    'term_of_payment_id' => $pr['InventoryPurchaseRequisition']['term_of_payment_id'], 
                    'remark_id' => $pr['InventoryPurchaseRequisition']['term_of_payment_id'],  
                    'purchase_status' => 0, 
                    'pic' => $username, 
                    'inventory_delivery_location_id' => $pr['InventoryPurchaseRequisition']['inventory_delivery_location_id'],  
                    'warranty_id' => $pr['InventoryPurchaseRequisition']['warranty_id'],  
                    'inventory_purchase_requisition_id' => $id,
                    'pr_type' => 0,
                    'inventory_location_id' => $pr['InventoryPurchaseRequisition']['inventory_location_id'],
                    'term_of_delivery_id' => $pr['InventoryPurchaseRequisition']['term_of_delivery_id'],
                    'bulk_discount' => $pr['InventoryPurchaseRequisition']['bulk_discount']
                    ); 

                if($this->InventoryPurchaseOrder->save($header)) {
                    $po_id = $this->InventoryPurchaseOrder->getLastInsertId();
                    for($x = 0; $x < count($inventory_item_id[$supplier[$i]]); $x++) { 
                        $this->InventoryPurchaseOrderItem->create();
                        $insert_item = array(
                            'inventory_purchase_order_id' => $po_id,
                            'quantity' => $quantity[$supplier[$i]][$x],
                            'general_unit_id' => $general_unit_id[$supplier[$i]][$x],
                            'inventory_item_id' => $inventory_item_id[$supplier[$i]][$x],
                            'inventory_supplier_item_id' => $inventory_supplier_item_id[$supplier[$i]][$x],
                            'inventory_item_category_id' => 0,
                            'inventory_supplier_id' => $supplier[$i],
                            'price_per_unit' => $price_per_unit[$supplier[$i]][$x],
                            'amount' => $amount[$supplier[$i]][$x],
                            'discount' => $discount[$supplier[$i]][$x],
                            'discount_type' => $discount_type[$supplier[$i]][$x],
                            'tax'=> $tax[$supplier[$i]][$x],
                            'planning_item_jobs' => $planning_item_jobs[$supplier[$i]][$x],
                            'delivery_remark' => $delivery_remark[$supplier[$i]][$x],
                            'general_currency_id' => $general_currency_id[$supplier[$i]][$x],
                            'total_rm' => $total_rm[$supplier[$i]][$x],
                            'remark' => $remark[$supplier[$i]][$x]

                            );
                        $insert = $this->InventoryPurchaseOrderItem->save($insert_item);

                        if(!$insert) {
                            debug($this->InventoryPurchaseOrderItem->invalidFields());
                            die('2010');
                        }
                    }

                    $data = array(
                        'to' => $pr['User']['email'],
                        'template' => 'purchase_requisition_status',
                        'subject' => 'Purchase Requisition Status (' . $pr['InventoryPurchaseRequisition']['pr_no'] . ')',
                        'content' => array(
                            'status' => 'converted to Purchase Order (' . $email_po . ')',
                            'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                            'from' => $this->Session->read('Auth.User.username'),
                            'department' => 'Procurement',
                            'username' => $pr['User']['username'],  
                            'link' => 'inventory_purchase_requisitions/view/'.$id
                            )
                        );
                    $this->send_email($data);

                }
            }

            // Update pr status
            $update = $this->InventoryPurchaseRequisition->updateAll(array(
                'InventoryPurchaseRequisition.status' => "11"
                ),
            array(
                'InventoryPurchaseRequisition.id' => $id
                ));

            if($update) {
                $this->Session->setFlash(__('Purchase Order for '.$pr['InventoryPurchaseRequisition']['pr_no']. ' has been created.'), 'success');
                return $this->redirect(array('action' => 'procurementindex'));
            }
        }

        
        $this->loadModel('InventoryPurchaseRequisitionItem');
        $lists = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id,
                'InventoryPurchaseRequisitionItem.inventory_supplier_id !=' => 0
                ),
            'group' => 'InventoryPurchaseRequisitionItem.inventory_supplier_id'
            ));
        $items = array();
        foreach ($lists as $list) {
            $items[] = array( 
                'InventorySupplier' => $list['InventorySupplier'],  
                'Items' => $this->get_item_by_supplier($list['InventoryPurchaseRequisitionItem']['inventory_supplier_id'], $id)
                );
        } 

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');

        $this->set('pr', $pr);
        $this->set('items', $items);
        $this->set('generalUnits', $generalUnits);
    }

    public function mode_of_payments($id = null){
        

    }

    public function procurementview($id) {
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'));
        }

        $pr = $this->InventoryPurchaseRequisition->find('first', array(
            'conditions' => array(
                'InventoryPurchaseRequisition.id' => $id
                )
            ));

        // Other purchase for Budget control 
        $this->loadModel('InventoryPurchaseRequisitionItem');
        $waitings = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'fields' => array(
                'SUM(InventoryPurchaseRequisitionItem.total_rm) AS total_waiting'
                ),
            'conditions' => array(
                'InventoryPurchaseRequisition.status' => array(1, 3, 5, 7, 9),
                'InventoryPurchaseRequisition.account_department_budget_id' => $pr['InventoryPurchaseRequisition']['account_department_budget_id'],
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id !=' => $id
                )
            ));
        $total_waiting = 0;
        foreach ($waitings as $waiting) {
            $total_waiting = $waiting[0]['total_waiting'];
        }
        $this->set('total_waiting', $total_waiting);
        // End budget control

        // Approval authority (LOFA)
        $this->loadModel('InternalLofa');
        $this->loadModel('User');

        if($pr['InventoryPurchaseRequisition']['pr_type'] == 1) {
            $cat = 'CAPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 2) {
            $cat = 'OPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 3 || $pr['InventoryPurchaseRequisition']['pr_type'] == 4) {
            $cat = 'SERVING_CLIENT';
        }

        $lofas = $this->InternalLofa->find('all', array(
            'conditions' => array(
                'InternalLofa.category' => $cat
                )
            ));

        $this->set('lofas', $lofas); 
        // End LOFA


        // Approval remark lists
        $this->loadModel('InventoryPurchaseRequisitionRemark');

        $approvalremarks = $this->InventoryPurchaseRequisitionRemark->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionRemark.inventory_purchase_requisition_id' => $id
                ),
            'order' => 'InventoryPurchaseRequisitionRemark.id DESC'
            ));
        $this->set('approvalremarks', $approvalremarks); 
        // End approval remark

        if($this->request->is(array('post', 'put'))) {      
            if($this->InventoryPurchaseRequisition->save($this->request->data)) {
                $status = $this->request->data['InventoryPurchaseRequisition']['status']; 
                
                $this->request->data['InventoryPurchaseRequisitionRemark']['user_id'] = $this->user_id;
                $this->request->data['InventoryPurchaseRequisitionRemark']['inventory_purchase_requisition_id'] = $id;
                $this->request->data['InventoryPurchaseRequisitionRemark']['status'] = $status;
                $this->request->data['InventoryPurchaseRequisitionRemark']['created'] = $this->date;
                $this->request->data['InventoryPurchaseRequisitionRemark']['type'] = 'Procurement';
                if($this->InventoryPurchaseRequisitionRemark->save($this->request->data['InventoryPurchaseRequisitionRemark'])) {
                    if($status == 5) {
                        // Find HOD for approval
                        $group_id = $pr['User']['group_id'];
                        $this->loadModel('Approval');
                        $approvals = $this->Approval->find('all', array(
                            'conditions' => array(
                                'Approval.name' => 'Purchase Requisition',
                                'Approval.type' => 'Approval',
                                'Approval.role' => 'HOD',
                                'Approval.group_id' => $group_id
                                )
                            ));
                        if($approvals) {
                            foreach ($approvals as $approval) {
                                $data = array(
                                    'to' => $approval['User']['email'],
                                    'template' => 'purchase_requisition',
                                    'subject' => 'Purchase Requisition Require Your Approval ' . $pr['InventoryPurchaseRequisition']['pr_no'],
                                    'content' => array(
                                        'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                        'from' => $pr['User']['username'],
                                        'username' => $approval['User']['username'],  
                                        'link' => 'inventory_purchase_requisitions/verifyview/'.$id
                                        )
                                    );
                                $this->send_email($data);
                            }   
                            $this->Session->setFlash(__('Purchase requisition has been sent for approval.'), 'success');
                            return $this->redirect(array('action' => 'procurementindex'));
                        } else {
                            $this->Session->setFlash(__('Purchase requisition has been saved, But User approval not found. Please ask Admin for approval.'), 'error');
                            return $this->redirect(array('action' => 'procurementindex'));
                        }    
                    } else {
                        // Mail to enduser
                        $data = array(
                            'to' => $pr['User']['email'],
                            'template' => 'purchase_requisition_status',
                            'subject' => 'Purchase Requisition ' . $pr['InventoryPurchaseRequisition']['pr_no'] . ' Status',
                            'content' => array(
                                'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                'status' => 'Rejected',
                                'department' => 'Procurement', 
                                'username' => $pr['User']['username'],  
                                'link' => 'inventory_purchase_requisitions/view/'.$id
                                )
                            );
                        $this->send_email($data);
                    }
                    
                }
            } 
        }

        $this->loadModel('InventoryPurchaseRequisitionItem');
        $lists = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id
                )
            ));
        $items = array();
        foreach ($lists as $list) {
            $items[] = array(
                'InventoryPurchaseRequisitionItem' => $list['InventoryPurchaseRequisitionItem'],
                'InventoryItem' => $list['InventoryItem'],
                'InventorySupplier' => $list['InventorySupplier'], 
                'GeneralUnit' => $list['GeneralUnit'],
                'GeneralCurrency' => $list['GeneralCurrency'],
                'InventorySupplierItem' => $list['InventorySupplierItem'],
                'Suppliers' => $this->get_supplier_by_item_id($list['InventoryPurchaseRequisitionItem']['inventory_item_id'])
                );
        }
        $this->request->data = $pr;

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');

        $this->set('pr', $pr);
        $this->set('items', $items);
        $this->set('generalUnits', $generalUnits);
        
        // Old
        $this->loadModel('AccountDepartmentBudgets');
        $this->loadModel('AccountDepartments');

        $budgets = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets JOIN account_departments on account_department_budgets.account_department_id = account_departments.id WHERE group_id=".$_SESSION['Auth']['User']['group_id']);
        $data_budget = array();
        foreach($budgets as $budget){
            $data_budget[] = $budget['account_department_budgets'];
        }

        $this->set('data_budget', $data_budget);

        // TOP 
        $termOfPayment = $this->InventoryPurchaseRequisition->TermOfPayment->find('list'); 

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list'); 

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->set(compact('termOfPayment', 'remark', 'warranty'));

    }

    public function printing($id) {
        $this->layout = 'print';  
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'));
        }

        $pr = $this->InventoryPurchaseRequisition->find('first', array(
            'conditions' => array(
                'InventoryPurchaseRequisition.id' => $id
                )
            ));

        // Other purchase for Budget control
        $this->loadModel('InventoryPurchaseRequisitionItem');
        $waitings = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'fields' => array(
                'SUM(InventoryPurchaseRequisitionItem.total_rm) AS total_waiting'
                ),
            'conditions' => array(
                'InventoryPurchaseRequisition.status' => array(1, 3, 5, 7, 9),
                'InventoryPurchaseRequisition.account_department_budget_id' => $pr['InventoryPurchaseRequisition']['account_department_budget_id'],
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id !=' => $id
                )
            ));
        $total_waiting = 0;
        foreach ($waitings as $waiting) {
            $total_waiting = $waiting[0]['total_waiting'];
        }
        $this->set('total_waiting', $total_waiting);
        // End budget control

        // Approval authority (LOFA)
        $this->loadModel('InternalLofa');
        $this->loadModel('User');

        if($pr['InventoryPurchaseRequisition']['pr_type'] == 1) {
            $cat = 'CAPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 2) {
            $cat = 'OPEX';
        }
        if($pr['InventoryPurchaseRequisition']['pr_type'] == 3 || $pr['InventoryPurchaseRequisition']['pr_type'] == 4) {
            $cat = 'SERVING_CLIENT';
        }

        $lofas = $this->InternalLofa->find('all', array(
            'conditions' => array(
                'InternalLofa.category' => $cat
                )
            ));

        $this->set('lofas', $lofas); 
        // End LOFA


        // Approval remark lists
        $this->loadModel('InventoryPurchaseRequisitionRemark');

        $approvalremarks = $this->InventoryPurchaseRequisitionRemark->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionRemark.inventory_purchase_requisition_id' => $id
                ),
            'order' => 'InventoryPurchaseRequisitionRemark.id DESC'
            ));
        $this->set('approvalremarks', $approvalremarks); 
        // End approval remark

        if($this->request->is(array('post', 'put'))) {      
            if($this->InventoryPurchaseRequisition->save($this->request->data)) {
                $status = $this->request->data['InventoryPurchaseRequisition']['status']; 
                
                $this->request->data['InventoryPurchaseRequisitionRemark']['user_id'] = $this->user_id;
                $this->request->data['InventoryPurchaseRequisitionRemark']['inventory_purchase_requisition_id'] = $id;
                $this->request->data['InventoryPurchaseRequisitionRemark']['status'] = $status;
                $this->request->data['InventoryPurchaseRequisitionRemark']['created'] = $this->date;
                $this->request->data['InventoryPurchaseRequisitionRemark']['type'] = 'Procurement';
                if($this->InventoryPurchaseRequisitionRemark->save($this->request->data['InventoryPurchaseRequisitionRemark'])) {
                    if($status == 5) {
                        // Find HOD for approval
                        $group_id = $pr['User']['group_id'];
                        $this->loadModel('Approval');
                        $approvals = $this->Approval->find('all', array(
                            'conditions' => array(
                                'Approval.name' => 'Purchase Requisition',
                                'Approval.type' => 'Approval',
                                'Approval.role' => 'HOD',
                                'Approval.group_id' => $group_id
                                )
                            ));
                        if($approvals) {
                            foreach ($approvals as $approval) {
                                $data = array(
                                    'to' => $approval['User']['email'],
                                    'template' => 'purchase_requisition',
                                    'subject' => 'Purchase Requisition Require Your Approval ' . $pr['InventoryPurchaseRequisition']['pr_no'],
                                    'content' => array(
                                        'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                        'from' => $pr['User']['username'],
                                        'username' => $approval['User']['username'],  
                                        'link' => 'inventory_purchase_requisitions/verifyview/'.$id
                                        )
                                    );
                                $this->send_email($data);
                            }   
                            $this->Session->setFlash(__('Purchase requisition has been sent for approval.'), 'success');
                            return $this->redirect(array('action' => 'procurementindex'));
                        } else {
                            $this->Session->setFlash(__('Purchase requisition has been saved, But User approval not found. Please ask Admin for approval.'), 'error');
                            return $this->redirect(array('action' => 'procurementindex'));
                        }    
                    } else {
                        // Mail to enduser
                        $data = array(
                            'to' => $pr['User']['email'],
                            'template' => 'purchase_requisition_status',
                            'subject' => 'Purchase Requisition ' . $pr['InventoryPurchaseRequisition']['pr_no'] . ' Status',
                            'content' => array(
                                'pr_number' => $pr['InventoryPurchaseRequisition']['pr_no'],
                                'status' => 'Rejected',
                                'department' => 'Procurement', 
                                'username' => $pr['User']['username'],  
                                'link' => 'inventory_purchase_requisitions/view/'.$id
                                )
                            );
                        $this->send_email($data);
                    }
                    
                }
            } 
        }

        $this->loadModel('InventoryPurchaseRequisitionItem');
        $lists = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id
                )
            ));
        $items = array();
        foreach ($lists as $list) {
            $items[] = array(
                'InventoryPurchaseRequisitionItem' => $list['InventoryPurchaseRequisitionItem'],
                'InventoryItem' => $list['InventoryItem'],
                'InventorySupplier' => $list['InventorySupplier'], 
                'GeneralUnit' => $list['GeneralUnit'],
                'GeneralCurrency' => $list['GeneralCurrency'],
                'InventorySupplierItem' => $list['InventorySupplierItem'],
                'Suppliers' => $this->get_supplier_by_item_id($list['InventoryPurchaseRequisitionItem']['inventory_item_id'])
                );
        }
        $this->request->data = $pr;

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');

        $this->set('pr', $pr);
        $this->set('items', $items);
        $this->set('generalUnits', $generalUnits);
        
        // Old
        $this->loadModel('AccountDepartmentBudgets');
        $this->loadModel('AccountDepartments');

        $budgets = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets JOIN account_departments on account_department_budgets.account_department_id = account_departments.id WHERE group_id=".$_SESSION['Auth']['User']['group_id']);
        $data_budget = array();
        foreach($budgets as $budget){
            $data_budget[] = $budget['account_department_budgets'];
        }

        $this->set('data_budget', $data_budget);

        // TOP 
        $termOfPayment = $this->InventoryPurchaseRequisition->TermOfPayment->find('list'); 

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list'); 

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->set(compact('termOfPayment', 'remark', 'warranty'));

    }

    public function procurementedit($id) {
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'));
        }
        $this->loadModel('InventoryPurchaseRequisitionItem');
        if($this->request->is(array('post', 'put'))) {  
            //print_r($_POST);
            //exit;

            if($this->InventoryPurchaseRequisition->save($this->request->data)) {
                if($this->InventoryPurchaseRequisitionItem->saveMany($this->request->data['InventoryPurchaseRequisitionItem'])) {
                    $this->Session->setFlash(__('The purchase requisition has been updated.'), 'success'); 
                    return $this->redirect(array('action' => 'procurementview', $id));
                }
            } 
        }

        $pr = $this->InventoryPurchaseRequisition->find('first', array(
            'conditions' => array(
                'InventoryPurchaseRequisition.id' => $id
                )
            ));
        
        $lists = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id
                )
            ));
        $items = array();
        foreach ($lists as $list) {
            $items[] = array(
                'InventoryPurchaseRequisitionItem' => $list['InventoryPurchaseRequisitionItem'],
                'InventoryItem' => $list['InventoryItem'],
                'InventorySupplier' => $list['InventorySupplier'], 
                'GeneralUnit' => $list['GeneralUnit'],
                'GeneralCurrency' => $list['GeneralCurrency'],
                'InventorySupplierItem' => $list['InventorySupplierItem'],
                'Suppliers' => $this->get_supplier_by_item_id($list['InventoryPurchaseRequisitionItem']['inventory_item_id'])
                );
        }
        $this->request->data = $pr;

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');

        $this->set('pr', $pr);
        $this->set('items', $items);
        $this->set('generalUnits', $generalUnits);
        
        // Old
        $this->loadModel('AccountDepartmentBudget');
        $this->loadModel('AccountDepartment');

        /*$budgets = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets JOIN account_departments on account_department_budgets.account_department_id = account_departments.id WHERE group_id=".$_SESSION['Auth']['User']['group_id']);
        $data_budget = array();
        foreach($budgets as $budget){
            $data_budget[] = $budget['account_department_budgets'];
        }

        $this->set('data_budget', $data_budget);*/
        $budgets = $this->AccountDepartmentBudget->find('list', array(
            'conditions' => array(
                'AccountDepartment.group_id' => $pr['User']['group_id'],
                'AccountDepartmentBudget.type' => $pr['InventoryPurchaseRequisition']['pr_type']  
                ),
            'recursive'=> 0
            ));


        // TOP 
        $termOfPayment = $this->InventoryPurchaseRequisition->TermOfPayment->find('list'); 

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list'); 

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->set(compact('termOfPayment', 'remark', 'warranty', 'budgets'));  
    }

    private function get_supplier_by_item_id($item_id) {
        $this->loadModel('InventorySupplierItem');
        $lists = $this->InventorySupplierItem->find('all', array(
            'conditions' => array(
                'InventorySupplierItem.inventory_item_id' => $item_id,
                'InventorySupplierItem.expiry_date <=' => $this->date
                )
            ));
        return $lists;
    }
 
    private function getsupplierlist($itemid) {
        $this->loadModel('InventorySupplierItem');
        $supp_arr = array();
        $supplierlist = $this->InventorySupplierItem->find('all', array(
            'conditions' => array(
                'InventorySupplierItem.inventory_item_id' => $itemid
            )
        ));

        foreach ($supplierlist as $s) {
            $supp_arr[] = array(
                'id' => $s['InventorySupplierItem']['id'],
                'name' => $s['InventorySupplier']['name'],
            );
        } 
        return $supp_arr; 
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        $this->InventoryPurchaseRequisition->id = $id;
        if (!$this->InventoryPurchaseRequisition->exists()) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'), 'error');
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->InventoryPurchaseRequisition->delete()) {
            $this->Session->setFlash(__('The inventory purchase requisition has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The inventory purchase requisition could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index'));
    }
}