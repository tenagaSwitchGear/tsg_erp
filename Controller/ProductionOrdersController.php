<?php
App::uses('AppController', 'Controller');
/**
 * ProductionOrders Controller
 * 
 * @property ProductionOrder $ProductionOrder
 * @property PaginatorComponent $Paginator
 */
class ProductionOrdersController extends AppController {
/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Js');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function plan($stat = null, $type = null) {
		$this->loadModel('SaleJobItem');
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$conditions = array();

		if($stat == null) {
			$conditions['SaleJobItem.production_status'] = 'Waiting';
		} else {
			$conditions['SaleJobItem.production_status'] = $stat;
		}

		if($type == null) {
			$conditions['SaleJobItem.bom_id !='] = 0;
		} else {
			$conditions['SaleJobItem.bom_id'] = 0; // Non bom
		}

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'SaleJobChild.fat_date ASC',
			'limit' => $record_per_page
			); 
		$items = $this->Paginator->paginate('SaleJobItem'); 
		$this->set('items', $items);
	}

	public function purchaseitem($stat = null, $type = null) {
		$this->loadModel('SaleJobItem');
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$conditions = array();

		if($stat == null) {
			$conditions['SaleJobItem.production_status'] = 'Waiting';
		} else {
			$conditions['SaleJobItem.production_status'] = $stat;
		}

		if($type == null) {
			$conditions['SaleJobItem.bom_id !='] = 0;
		} else {
			$conditions['SaleJobItem.bom_id'] = 0; // Non bom
		}

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'SaleJobChild.fat_date ASC',
			'limit' => $record_per_page
			); 
		$items = $this->Paginator->paginate('SaleJobItem'); 
		$this->set('items', $items);
	}

	public function replacebom($id = null) {
		$this->loadModel('ProductionOrder');  
		$item = $this->ProductionOrder->find('first', array(
			'conditions' => array(
				'ProductionOrder.id' => $id
				)
			));  
		if($this->request->is(array('post', 'put'))) {
			  
			$this->request->data['ProductionOrder']['modified'] = $this->date;

			$bom_id = $this->request->data['ProductionOrder']['bom_id'];
			 
			$quantity = $this->request->data['ProductionOrder']['quantity'];

			$sale_job_childs_id = $this->request->data['ProductionOrder']['sale_job_childs_id'];
			
			if ($this->ProductionOrder->save($this->request->data)) { 
				// Delete PDC first
				$this->loadModel('ProductionOrderChild');  
				$this->loadModel('ProductionOrderItem');  
 				$deleteChild = $this->ProductionOrderChild->deleteAll(
 					array(
 						'ProductionOrderChild.production_order_id' => $id
 						), 
 				    false
 					);

 				$deleteItem = $this->ProductionOrderItem->deleteAll(
 					array(
 						'ProductionOrderItem.production_order_id' => $id
 						), 
 				    false
 					);
 				if($deleteChild && $deleteItem) {
	 				// Reinsert bom
					$this->_copy_bom_child($bom_id, $id, $sale_job_childs_id, $quantity);
					 
					$this->insert_log($this->user_id, 'Replace Production Order BOM: ' . $item['ProductionOrder']['name'], 'production_orders/view/'.$id, 'Replace Production Order BOM'); 

					$this->Session->setFlash(__('New BOM has been replaced successfully.'), 'success');
					return $this->redirect(array('action' => 'view', $id));	
 				} else {
 					$this->Session->setFlash(__('Unable to delete current BOM. Please contact developer with error code: E122.'), 'error');
 				}
				
			} else {
				$this->Session->setFlash(__('The production order could not be saved. Please, try again.'), 'error');
			} 
		}
		
		$this->set('productionOrder', $item);
		$this->request->data = $item;
	}

	 

	public function viewplan($id) {
		$this->loadModel('SaleJobItem');  

		if($this->request->is('post')) {
			 
			$this->ProductionOrder->create();
			//$this->request->data['ProductionOrder']['bom_id'] = 2;
			/*
			$bom_id = $this->request->data['ProductionOrder']['bom_id'];
			$station_id = $this->request->data['ProductionOrder']['sale_job_childs_id'];
			$no_of_order = $this->request->data['ProductionOrder']['quantity'];
			

			$this->loadModel('ProjectBom');
			$conditions['ProjectBom.id'] = $bom_id;
		    $bom = $this->Bom->find('first', array('conditions' => $conditions));
		    
			$this->request->data['ProductionOrder']['bom_name'] = $bom['Bom']['name'];;
			$this->request->data['ProductionOrder']['bom_code'] = $bom['Bom']['code'];
			$this->request->data['ProductionOrder']['bom_description'] = $bom['Bom']['description'];
			*/

			$this->request->data['ProductionOrder']['user_id'] = $this->Session->read('Auth.User.id');
			$this->request->data['ProductionOrder']['created'] = $this->date;
			$this->request->data['ProductionOrder']['modified'] = $this->date;

			$bom_id = $this->request->data['ProductionOrder']['bom_id'];
			$station_id = $this->request->data['ProductionOrder']['sale_job_childs_id'];
			$no_of_order = $this->request->data['ProductionOrder']['quantity'];

			$job_item_id = $this->request->data['ProductionOrder']['sale_job_items_id'];

			$inventory_item_id = $this->request->data['ProductionOrder']['inventory_item_id'];

			$po = $this->ProductionOrder->find('all', array(
				'recursive' => -1 
				));

			$count = count($po) + 1; 

			$this->request->data['ProductionOrder']['name'] = $this->generate_code('PDC', $count);

			
			if ($this->ProductionOrder->save($this->request->data)) { 
				// Update status production_status on sale_job_item
				$production_order_id = $this->ProductionOrder->getLastInsertId();

				if($bom_id != 0) {
					// This is BOM
					$this->_copy_bom_child($bom_id, $production_order_id, $station_id, $no_of_order);
				} else {
					// Purchased item
					$this->_copy_purchased_item($job_item_id, $inventory_item_id, $production_order_id, $station_id, $no_of_order);
				}
				

				// Update production order status
				$this->SaleJobItem->updateAll(
					array(
						'SaleJobItem.production_status' => "'Started'"
						),
					array(
						'SaleJobItem.id' => $job_item_id
						)
					);  
				// Update sale order
                $progress = $this->update_sale_order_progress($station_id, 'Work In Progress');

                if($progress === false) {
                    $this->Session->setFlash(__('Unable to update progress, Please contact administrator. Error Code: E139.'), 'error');
                    return $this->redirect(array('action' => 'index'));
                }

				$this->Session->setFlash(__('The production order has been saved.'), 'success');
				return $this->redirect(array('action' => 'plan'));
			} else {
				$this->Session->setFlash(__('The production order could not be saved. Please, try again.'));
			}
			 
		}
		$item = $this->SaleJobItem->find('first', array(
			'conditions' => array(
				'SaleJobItem.id' => $id
				)
			));  
		$this->set('productionOrder', $item);
	}

	public function editbom($id = null, $child_id = null, $parent_id = 0) {

		$this->set('child_id', $child_id);
		$this->set('parent_id', $parent_id);

		$options = array('conditions' => array('ProductionOrder.' . $this->ProductionOrder->primaryKey => $id));
		$productionOrder = $this->ProductionOrder->find('first', $options);

		if($this->request->is('post')) {
			$this->loadModel('InventoryMaterialRequest');

			$counter = $this->InventoryMaterialRequest->find('first', array(
				'order' => array(
					'InventoryMaterialRequest.id' => 'desc'
					),
				'recursive' => -1
				));

			$number = $counter['InventoryMaterialRequest']['id'] + 1;
			$mrn_number = $this->generate_code('MRN', $number);
			$this->InventoryMaterialRequest->create();
			$date = date('Y-m-d H:i:s');
			$data = array(
				'production_order_id' => $this->request->data['ProductionOrder']['production_order_id'],
				'production_order_child_id' => $this->request->data['ProductionOrder']['production_order_child_id'],
				'created' => $date,
				'modified' => $date,
				'user_id' => $this->Session->read('Auth.User.id'),  // parent id will refer to bom_parent to prevent duplicate
				'type' => 1,  
				'status' => 1, 
				'code' => $mrn_number,
				'sale_job_id' => $productionOrder['ProductionOrder']['sale_job_id'],
				'sale_job_child_id' => $productionOrder['ProductionOrder']['sale_job_childs_id'],
				'sale_job_id' => $productionOrder['ProductionOrder']['sale_job_id'],
				'note' => '',
				);  
			$this->InventoryMaterialRequest->save($data);
			$inventory_material_request_id = $this->InventoryMaterialRequest->getLastInsertId();
			
			$this->loadModel('InventoryMaterialRequestItem');
			
			foreach($this->request->data['quantity_req'] as $production_order_item_id=>$req_item)
			{
				if(!empty($req_item[0]))
				{	
					$max = $this->request->data['max_qty'][$production_order_item_id][0];
					if($req_item[0] <= $max) {
						$quantity = $req_item[0];
					} else {
						$quantity = $max; 
					}
					
					$type = $this->request->data['type'][$production_order_item_id][0];
					$inventory_item_id = $this->request->data['inventory_item_id'][$production_order_item_id][0];
					$general_unit_id = $this->request->data['general_unit_id'][$production_order_item_id][0];
					$quantity_total = $this->request->data['quantity_total'][$production_order_item_id][0];
					$created = $date;
					$modified = $date;
					$status = 1;
					
					if($type == 'child') {
						$po_child_id = $this->request->data['po_child_id'][$production_order_item_id][0]; 
						 
						// Aub assembly
						$this->InventoryMaterialRequestItem->create();
						$data = array(
							'inventory_material_request_id' => $inventory_material_request_id,
							'production_order_item_id' => $production_order_item_id,
							'inventory_item_id' => $inventory_item_id,
							'quantity' => $quantity,
							'quantity_total' => $quantity_total,
							'issued_quantity' => 0,
							'issued_balance' => $quantity,
							'general_unit_id' => $general_unit_id,
							'created' => $created,
							'modified' => $modified,
							'status' => $status,
							'type' => 2, // MRN Auto
							'production_order_child_id' => $production_order_item_id
							);  
						$this->InventoryMaterialRequestItem->save($data);
						
						$this->loadModel('ProductionOrderChild'); 
						
						$update = $this->ProductionOrderChild->updateAll(
							array('ProductionOrderChild.quantity_req' => "ProductionOrderChild.quantity_req + ".$quantity),
							array('ProductionOrderChild.id' => $production_order_item_id)
							);	  

						if(!$update) { 
							die('359');
						}  
					} else {
						$this->InventoryMaterialRequestItem->create();
						$data = array(
							'inventory_material_request_id' => $inventory_material_request_id,
							'production_order_item_id' => $production_order_item_id,
							'inventory_item_id' => $inventory_item_id,
							'quantity' => $quantity,
							'issued_quantity' => 0,
							'issued_balance' => $quantity,
							'general_unit_id' => $general_unit_id,
							'created' => $created,
							'modified' => $modified,
							'status' => $status,
							'type' => 2, // MRN Auto
							'production_order_child_id' => 0
							);  
						$this->InventoryMaterialRequestItem->save($data);
						
						$this->loadModel('ProductionOrderItem');
						
						$this->ProductionOrderItem->updateAll(
						array('ProductionOrderItem.quantity_req' => 'ProductionOrderItem.quantity_req + '.$quantity),
						array('ProductionOrderItem.id' => $production_order_item_id)
						);	
					} 
				}
			}
			
			$this->Session->setFlash(__('Material Request sent successfully.'), 'success');
		}
		if (!$this->ProductionOrder->exists($id)) {
			throw new NotFoundException(__('Invalid production order'));
		}
		
		$materials = $this->productionBomChildsAndItems($id, $child_id, $parent_id);
		
		$this->set('materials', $materials);
		
		$status = array(0 => 'Pending', 1 => 'WIP', 2 => 'Final Assembly', 3 => 'FAT', 4 => 'Rejected', 5 => 'Rework', 6 => 'Approve', 7 => 'Final Inspection', 8 => 'Packing', 9 => 'Completed');
		
		$this->set('status', $status);
		
		$this->set('child_id', $child_id);
		$this->set('parent_id', $parent_id);
		$this->set('materials', $materials);
		
		
		
		$data = $this->ProductionOrder->query("SELECT poi.*, ii.name as item_name, ii.code as item_code, gu.name as unit_name, gu.id as unit_id FROM 
									 production_order_items AS poi
									 JOIN inventory_items AS ii ON(ii.id = poi.inventory_item_id)
									 LEFT JOIN general_units AS gu ON(gu.id = ii.general_unit_id) 
									 WHERE poi.production_order_id = '".$id."'");
									 
		$items = array();
		
		foreach($data as $item)
		{
			$item['poi']['item_name'] = $item['ii']['item_name'];
			$item['poi']['item_code'] = $item['ii']['item_code'];
			$item['poi']['unit_name'] = $item['gu']['unit_name'];
			$item['poi']['unit_id'] = $item['gu']['unit_id'];
			$items[] = $item['poi'];
		}
		
		$productionOrder["ProductionOrderItem"] = $items;
		
		$this->set('productionOrder', $productionOrder);

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$this->set(compact('units'));
	}

	public function managesub($id = null, $child_id = null, $parent_id = null) {
		if (!$this->ProductionOrder->exists($id)) {
			throw new NotFoundException(__('Invalid production order'));
		}
		$this->set('child_id', $child_id);
		$this->set('parent_id', $parent_id);

		$options = array('conditions' => array('ProductionOrder.' . $this->ProductionOrder->primaryKey => $id));
		$productionOrder = $this->ProductionOrder->find('first', $options);

		$this->loadModel('ProductionOrderChild');

		$child = $this->ProductionOrderChild->find('first', array(
			'conditions' => array(
				'ProductionOrderChild.id' => $child_id
				)
			));


		if($this->request->is(array('post', 'put'))) {  
			if($this->ProductionOrderChild->save($this->request->data['ProductionOrderChild'])) {
				$this->Session->setFlash(__('Item has been saved'), 'success');
				return $this->redirect(array('action' => 'editbom', $id));
			} else {
				$this->Session->setFlash(__('Item could not be saved. Error code E415. Please contact developer.'), 'error');
			}
		} 
		
		$materials = $this->productionBomChildsAndItems($id, $child_id, $parent_id);
		
		$this->set('materials', $materials);
		
		$status = array(0 => 'Pending', 1 => 'WIP', 2 => 'Final Assembly', 3 => 'FAT', 4 => 'Rejected', 5 => 'Rework', 6 => 'Approve', 7 => 'Final Inspection', 8 => 'Packing', 9 => 'Completed');
		
		$this->set('status', $status);
		
		$this->set('child_id', $child_id);
		$this->set('parent_id', $parent_id);
		$this->set('materials', $materials);
		
		
		
		$data = $this->ProductionOrder->query("SELECT poi.*, ii.name as item_name, ii.code as item_code, gu.name as unit_name, gu.id as unit_id FROM 
									 production_order_items AS poi
									 JOIN inventory_items AS ii ON(ii.id = poi.inventory_item_id)
									 LEFT JOIN general_units AS gu ON(gu.id = ii.general_unit_id) 
									 WHERE poi.production_order_id = '".$id."'");
									 
		$items = array();
		
		foreach($data as $item)
		{
			$item['poi']['item_name'] = $item['ii']['item_name'];
			$item['poi']['item_code'] = $item['ii']['item_code'];
			$item['poi']['unit_name'] = $item['gu']['unit_name'];
			$item['poi']['unit_id'] = $item['gu']['unit_id'];
			$items[] = $item['poi'];
		}
		
		$productionOrder["ProductionOrderItem"] = $items;
		
		$this->set('productionOrder', $productionOrder);

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$this->request->data = $child;

		$this->set(compact('units', 'child'));
	}

	public function manageitem($id = null, $child_id = null, $parent_id = null) {
		if (!$this->ProductionOrder->exists($id)) {
			throw new NotFoundException(__('Invalid production order'));
		}
		$this->set('child_id', $child_id);
		$this->set('parent_id', $parent_id);

		$options = array('conditions' => array('ProductionOrder.' . $this->ProductionOrder->primaryKey => $id));
		$productionOrder = $this->ProductionOrder->find('first', $options);
 
		if($this->request->is('post')) {
			// Delete item 
			$this->loadModel('ProductionOrderItem');
			$this->ProductionOrderItem->deleteAll(
				array(
					'ProductionOrderItem.production_order_id' => $id,
					'ProductionOrderItem.production_order_child_id' => $child_id,
					'ProductionOrderItem.bom_id' => $productionOrder['ProductionOrder']['bom_id']
					),
				false
				);
			// insert item $productionOrder['ProductionOrder']['quantity'] 

			$item_id = $this->request->data['ProductionOrder']['inventory_item_id']; 
			$quantity = $this->request->data['ProductionOrder']['quantity'];
			$general_unit_id = $this->request->data['ProductionOrder']['general_unit_id'];
			$total_quantity = $this->request->data['ProductionOrder']['total_quantity']; 

			$item = array();
			for($i = 0; $i < count($item_id); $i++) {
				$item[] = array(
					'production_order_id' => $id, 
					'production_order_child_id' => $child_id, 
					'bom_id' => $productionOrder['ProductionOrder']['bom_id'], 
					'inventory_item_id' => $item_id[$i], 
					'quantity' => $quantity[$i], 
					'quantity_total' => $total_quantity[$i], 
					'quantity_issued' => 0, 
					'quantity_bal' => $total_quantity[$i], 
					'quantity_req' => 0, 
					'general_unit_id' => $general_unit_id[$i],
					'no_of_order' => $productionOrder['ProductionOrder']['quantity'] 
					);
			}
			if($this->ProductionOrderItem->saveAll($item)) {
				// Log
				$this->insert_log($this->user_id, 'Edit Production BOM Items', 'production_orders/manageitem/'.$id.'/'.$child_id.'/'.$parent_id, 'Edit Production BOM');
				$this->Session->setFlash(__('Item has been saved'), 'success');
				return $this->redirect(array('action' => 'editbom', $id));
			} else {
				$this->insert_log($this->user_id, 'Failed Edit Production BOM Items: E605', 'production_orders/manageitem/'.$id.'/'.$child_id.'/'.$parent_id, 'Edit Production BOM');
				$this->Session->setFlash(__('Unable to save items. Please contact developer. Error Code E605.'), 'error');
				return $this->redirect(array('action' => 'editbom', $id));
			}  
		} 
		
		$materials = $this->productionBomChildsAndItems($id, $child_id, $parent_id);
		
		$this->set('materials', $materials);
		
		$status = array(0 => 'Pending', 1 => 'WIP', 2 => 'Final Assembly', 3 => 'FAT', 4 => 'Rejected', 5 => 'Rework', 6 => 'Approve', 7 => 'Final Inspection', 8 => 'Packing', 9 => 'Completed');
		
		$this->set('status', $status);
		
		$this->set('child_id', $child_id);
		$this->set('parent_id', $parent_id);
		$this->set('materials', $materials);
		
		
		
		$data = $this->ProductionOrder->query("SELECT poi.*, ii.name as item_name, ii.code as item_code, gu.name as unit_name, gu.id as unit_id FROM 
									 production_order_items AS poi
									 JOIN inventory_items AS ii ON(ii.id = poi.inventory_item_id)
									 LEFT JOIN general_units AS gu ON(gu.id = ii.general_unit_id) 
									 WHERE poi.production_order_id = '".$id."'");
									 
		$items = array();
		
		foreach($data as $item)
		{
			$item['poi']['item_name'] = $item['ii']['item_name'];
			$item['poi']['item_code'] = $item['ii']['item_code'];
			$item['poi']['unit_name'] = $item['gu']['unit_name'];
			$item['poi']['unit_id'] = $item['gu']['unit_id'];
			$items[] = $item['poi'];
		}
		
		$productionOrder["ProductionOrderItem"] = $items;
		
		$this->set('productionOrder', $productionOrder);

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$this->set(compact('units'));
	}

/**
 * index method
 *
 * @return void
 */
	public function index($stat = null) { 

		$conditions = array();

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['ProductionOrder.name'] = trim($_GET['name']);
			}

			if($_GET['station'] != '') {
				$conditions['SaleJobChild.station_name'] = trim($_GET['station']);
			}

			if($_GET['from'] != '') {
				$conditions['ProductionOrder.created >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['ProductionOrder.created <='] = trim($_GET['to']);
			}

			$this->request->data['ProductionOrder'] = $_GET;
		}

		if($stat == null) {
			$conditions['ProductionOrder.status'] = 1;
		} else {
			$conditions['ProductionOrder.status'] = (int)$stat;
		}
		
		$record_per_page = Configure::read('Reading.nodes_per_page');
		
		$status = array(0 => 'Pending', 1 => 'WIP', 2 => 'Final Assembly', 3 => 'FAT', 4 => 'Rejected', 5 => 'Rework', 6 => 'Approve', 7 => 'Final Inspection', 8 => 'Packing', 9 => 'Completed');
		
		$this->set('status', $status);

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'ProductionOrder.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('productionOrders', $this->Paginator->paginate());
	}
 
	
	public function complete($id = null)
	{
		if (!$this->ProductionOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Production Order'));
		}
		
		$options = array('conditions' => array('ProductionOrder.' . $this->ProductionOrder->primaryKey => $id));
		$data = $this->ProductionOrder->find('first', $options);
		
		$quantity = $data['ProductionOrder']['quantity'];
		$this->loadModel('FinishedGood');
		for($i=1; $i<=$quantity;$i++)
		{
			
			$this->FinishedGood->create(); 
			$insert_data = array(
				'inventory_item_id' => $data['SaleJobItem']['inventory_item_id'],
				'sale_job_child_id' => $data['ProductionOrder']['sale_job_childs_id'], 
				'sale_job_item_id' => $data['ProductionOrder']['sale_job_items_id'],
				'production_order_id' => $data['ProductionOrder']['id'],
				'production_order_name' => $data['ProductionOrder']['name'],
				'bom_id'   => $data['ProductionOrder']['bom_id'],
				'bom_name' => $data['ProductionOrder']['bom_name'],
				'bom_code' => $data['ProductionOrder']['bom_code'],
				'created'  => $this->date,
				'modified' => $this->date,
				'status'   => 'Draft',
				'user_id'  => $this->Session->read('Auth.User.id')
				);  
			$this->FinishedGood->save($insert_data);			
		}
		
		$this->ProductionOrder->updateAll(
		array('ProductionOrder.status' => '9'),
		array('ProductionOrder.id' => $id)
		);
		$this->Session->setFlash(__('Production Order Completed. Please go to Finished Goods to add Serial Number.'), 'success');
		return $this->redirect(array('action' => 'view', $id));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null, $child_id = null, $parent_id = 0) {

		$options = array('conditions' => array('ProductionOrder.' . $this->ProductionOrder->primaryKey => $id));
		$productionOrder = $this->ProductionOrder->find('first', $options);

		if($this->request->is('post')) {
			$this->loadModel('InventoryMaterialRequest');

			$counter = $this->InventoryMaterialRequest->find('first', array(
				'order' => array(
					'InventoryMaterialRequest.id' => 'desc'
					),
				'recursive' => -1
				));

			$number = $counter['InventoryMaterialRequest']['id'] + 1;
			$mrn_number = $this->generate_code('MRN', $number);
			$this->InventoryMaterialRequest->create();
			$date = date('Y-m-d H:i:s');
			$data = array(
				'production_order_id' => $this->request->data['ProductionOrder']['production_order_id'],
				'production_order_child_id' => $this->request->data['ProductionOrder']['production_order_child_id'],
				'created' => $date,
				'modified' => $date,
				'user_id' => $this->Session->read('Auth.User.id'),  // parent id will refer to bom_parent to prevent duplicate
				'type' => 1,  
				'status' => 1, 
				'code' => $mrn_number,
				'sale_job_id' => $productionOrder['ProductionOrder']['sale_job_id'],
				'sale_job_child_id' => $productionOrder['ProductionOrder']['sale_job_childs_id'],
				'sale_job_id' => $productionOrder['ProductionOrder']['sale_job_id'],
				'note' => '',
				);  
			$this->InventoryMaterialRequest->save($data);
			$inventory_material_request_id = $this->InventoryMaterialRequest->getLastInsertId();
			
			$this->loadModel('InventoryMaterialRequestItem');
			
			foreach($this->request->data['quantity_req'] as $production_order_item_id=>$req_item)
			{
				if(!empty($req_item[0]))
				{	
					$max = $this->request->data['max_qty'][$production_order_item_id][0];
					if($req_item[0] <= $max) {
						$quantity = $req_item[0];
					} else {
						$quantity = $max; 
					}
					
					$type = $this->request->data['type'][$production_order_item_id][0];
					$inventory_item_id = $this->request->data['inventory_item_id'][$production_order_item_id][0];
					$general_unit_id = $this->request->data['general_unit_id'][$production_order_item_id][0];
					$quantity_total = $this->request->data['quantity_total'][$production_order_item_id][0];
					$created = $date;
					$modified = $date;
					$status = 1;
					
					if($type == 'child') {
						$po_child_id = $this->request->data['po_child_id'][$production_order_item_id][0]; 
						 
						// Aub assembly
						$this->InventoryMaterialRequestItem->create();
						$data = array(
							'inventory_material_request_id' => $inventory_material_request_id,
							'production_order_item_id' => $production_order_item_id,
							'inventory_item_id' => $inventory_item_id,
							'quantity' => $quantity,
							'quantity_total' => $quantity_total,
							'issued_quantity' => 0,
							'issued_balance' => $quantity,
							'general_unit_id' => $general_unit_id,
							'created' => $created,
							'modified' => $modified,
							'status' => $status,
							'type' => 2, // MRN Auto
							'production_order_child_id' => $production_order_item_id
							);  
						$this->InventoryMaterialRequestItem->save($data);
						
						$this->loadModel('ProductionOrderChild'); 
						
						$update = $this->ProductionOrderChild->updateAll(
							array('ProductionOrderChild.quantity_req' => "ProductionOrderChild.quantity_req + ".$quantity),
							array('ProductionOrderChild.id' => $production_order_item_id)
							);	  

						if(!$update) { 
							die('359');
						}  
					} else {
						$this->InventoryMaterialRequestItem->create();
						$data = array(
							'inventory_material_request_id' => $inventory_material_request_id,
							'production_order_item_id' => $production_order_item_id,
							'inventory_item_id' => $inventory_item_id,
							'quantity' => $quantity,
							'issued_quantity' => 0,
							'issued_balance' => $quantity,
							'general_unit_id' => $general_unit_id,
							'created' => $created,
							'modified' => $modified,
							'status' => $status,
							'type' => 2, // MRN Auto
							'production_order_child_id' => 0
							);  
						$this->InventoryMaterialRequestItem->save($data);
						
						$this->loadModel('ProductionOrderItem');
						
						$this->ProductionOrderItem->updateAll(
						array('ProductionOrderItem.quantity_req' => 'ProductionOrderItem.quantity_req + '.$quantity),
						array('ProductionOrderItem.id' => $production_order_item_id)
						);	
					} 
				}
			}
			
			$this->Session->setFlash(__('Material Request sent successfully.'), 'success');
		}
		if (!$this->ProductionOrder->exists($id)) {
			throw new NotFoundException(__('Invalid production order'));
		}
		
		$materials = $this->productionBomChildsAndItems($id, $child_id, $parent_id);
		
		$this->set('materials', $materials);
		
		$status = array(0 => 'Pending', 1 => 'WIP', 2 => 'Final Assembly', 3 => 'FAT', 4 => 'Rejected', 5 => 'Rework', 6 => 'Approve', 7 => 'Final Inspection', 8 => 'Packing', 9 => 'Completed');
		
		$this->set('status', $status);
		
		$this->set('child_id', $child_id);
		$this->set('parent_id', $parent_id);
		$this->set('materials', $materials);
		
		
		
		$data = $this->ProductionOrder->query("SELECT poi.*, ii.name as item_name, ii.code as item_code, gu.name as unit_name FROM 
									 production_order_items AS poi
									 JOIN inventory_items AS ii ON(ii.id = poi.inventory_item_id)
									 LEFT JOIN general_units AS gu ON(gu.id = ii.general_unit_id) 
									 WHERE poi.production_order_id = '".$id."'");
									 
		$items = array();
		
		foreach($data as $item)
		{
			$item['poi']['item_name'] = $item['ii']['item_name'];
			$item['poi']['item_code'] = $item['ii']['item_code'];
			$item['poi']['unit_name'] = $item['gu']['unit_name'];
			
			$items[] = $item['poi'];
		}
		
		$productionOrder["ProductionOrderItem"] = $items;
		
		$this->set('productionOrder', $productionOrder);
	}

	private function check_sub_assembly_has_item($child_id) {
		$this->loadModel('ProductionOrderItem');
		$rows = $this->ProductionOrderItem->find('all', array(
			'conditions' => array(
				'ProductionOrderItem.production_order_child_id' => $child_id 
				),
			'recursive' => -1
			));
		if($rows) {
			return 1;
		} else {
			return 0; // Purchased
		}
	}

	private function check_sub_assembly_has_sub($bom_parent, $production_order_id) {
		$this->loadModel('ProductionOrderChild');
		$rows = $this->ProductionOrderChild->find('all', array(
			'conditions' => array(
				'ProductionOrderChild.parent_id' => $bom_parent,
				'ProductionOrderChild.production_order_id' => $production_order_id,
				),
			'recursive' => -1
			));
		if($rows) {
			return 1;
		} else {
			return 0; // Purchased
		}
	}
	
	public function productionBomChildsAndItems($po_id = null, $po_child_id = null, $parent_id = null) {
		$result = array(); 		
		if (!$this->ProductionOrder->exists($po_id)) {
			throw new NotFoundException(__('Invalid Production Order'));
		}
		
		$this->loadModel('ProductionOrderChild');

		$rows = $this->ProductionOrderChild->find('all', array(
			'conditions' => array(
				'ProductionOrderChild.production_order_id' => $po_id,
				'ProductionOrderChild.parent_id' => $parent_id
				)
			));
		// If no rows found, then try to show items
		/*$this->loadModel('ProductionOrderItem'); 
		$items = $this->ProductionOrderItem->find('all', array(
			'conditions' => array( 
				'ProductionOrderItem.production_order_id' => $po_id,
				'ProductionOrderItem.production_order_child_id' => $po_child_id
				)
			));*/
		if($po_child_id != null) {
			$breadcrumb = $this->ProductionOrderChild->find('first', array(
			'conditions' => array(
				'ProductionOrderChild.production_order_id' => $po_id,
				'ProductionOrderChild.bom_parent' => $parent_id
				),
			'recursive' => -1
			));
			$this->set('breadcrumb', $breadcrumb);
		}
		
		
		$data = $this->ProductionOrder->query("SELECT poi.*, ii.name as item_name, ii.code as item_code, gu.name as unit_name, gu.id as unit_id FROM 
									 production_order_items AS poi
									 JOIN inventory_items AS ii ON(ii.id = poi.inventory_item_id)
									 LEFT JOIN general_units AS gu ON(gu.id = ii.general_unit_id) 
									 WHERE poi.production_order_id = '".$po_id."' AND poi.production_order_child_id = '".$po_child_id."'");
									 
		$items = array();
		
		foreach($data as $item)
		{
			$item['poi']['item_name'] = $item['ii']['item_name'];
			$item['poi']['item_code'] = $item['ii']['item_code'];
			$item['poi']['unit_name'] = $item['gu']['unit_name'];
			$item['poi']['unit_id'] = $item['gu']['unit_id'];
			$items[] = $item['poi'];
		}
		
		//$productionOrder["ProductionOrderItem"] = $items;
		
		if($rows || $items) {
			foreach ($rows as $row) { 
				$result['childs'][] = array(
					'ProductionOrderChild' => $row['ProductionOrderChild'],
					'GeneralUnit' => $row['GeneralUnit'],
					'InventoryItem' => $row['InventoryItem'], 
					'has_item' => $this->check_sub_assembly_has_item($row['ProductionOrderChild']['id']),
					'has_sub' => $this->check_sub_assembly_has_sub($row['ProductionOrderChild']['bom_parent'], $po_id)
					); 
			}
			
			foreach ($items as $item) { 
				$result['items'][] = $item;
			}
		}
		return $result;
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if($this->request->is('post')) {
			$date = date('Y-m-d H:i:s');
			$this->ProductionOrder->create();
			//$this->request->data['ProductionOrder']['bom_id'] = 2;

			$this->request->data['ProductionOrder']['user_id'] = $this->Session->read('Auth.User.id');
			$this->request->data['ProductionOrder']['created'] = $date;
			$this->request->data['ProductionOrder']['modified'] = $date;
			$bom_id = $this->request->data['ProductionOrder']['bom_id'];
			$station_id = $this->request->data['ProductionOrder']['sale_job_childs_id'];
			$no_of_order = $this->request->data['ProductionOrder']['quantity'];
			$this->loadModel('Bom');
			$conditions['Bom.id'] = $bom_id;
		    $bom = $this->Bom->find('first', array('conditions' => $conditions));
			$this->request->data['ProductionOrder']['bom_name'] = $bom['Bom']['name'];;
			$this->request->data['ProductionOrder']['bom_code'] = $bom['Bom']['code'];
			$this->request->data['ProductionOrder']['bom_description'] = $bom['Bom']['description'];

			$po = $this->ProductionOrder->find('all', array(
				'recursive' => -1
				));

			$count = count($po) + 1;

			$this->request->data['ProductionOrder']['name'] = $this->generate_code('PDC', $count);

			
			if ($this->ProductionOrder->save($this->request->data)) {
				$production_order_id = $this->ProductionOrder->getLastInsertId();
				$this->_copy_bom_child($bom_id, $production_order_id, $station_id, $no_of_order);
				$this->Session->setFlash(__('The production order has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The production order could not be saved. Please, try again.'));
			}
		}
	}

	private function _copy_purchased_item($sale_job_item_id, $inventory_item_id, $production_order_id, $station_id, $no_of_order) {
		$this->loadModel('SaleJobItem');
		$this->loadModel('ProductionOrderChild');
		$item = $this->SaleJobItem->find('first', array(
			'conditions' => array(
					'SaleJobItem.id' => $sale_job_item_id
				) 
			)); 
		$this->ProductionOrderChild->create();
		$data = array(
			'name' => $item['InventoryItem']['name'],
			'production_order_id' => $production_order_id,
			'sale_job_childs_id' => $station_id,
			'sale_job_items_id' => $item['SaleJobItem']['id'],
			'parent_id' => 0,  // parent id will refer to bom_parent to prevent duplicate
			'bom_parent' => 0,
			'no_of_order' => $no_of_order,
			'margin' => 0,
			'margin_unit' => 0,
			'inventory_item_id' => $inventory_item_id,
			'quantity_bal' => $no_of_order,
			'quantity_req' => 0,
			'issued_qty' => 0,
			'general_unit_id' => $item['SaleJobItem']['general_unit_id'],
			'quantity' => $item['SaleJobItem']['quantity'],
			'type' => 1
			);  
		$this->ProductionOrderChild->save($data);  
	}
	
	private function _copy_bom_child($bom_id, $production_order_id, $station_id, $no_of_order) {
		$this->loadModel('BomChild');
		$this->loadModel('ProductionOrderChild');
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
					'BomChild.bom_id' => $bom_id
				),
			'group' => 'BomChild.id'
			));
		$data = array();
		foreach ($childs as $child) {
			$this->ProductionOrderChild->create();
			$data = array(
				'name' => $child['BomChild']['name'],
				'production_order_id' => $production_order_id,
				'sale_job_childs_id' => $station_id,
				'sale_job_items_id' => $bom_id,
				'parent_id' => $child['BomChild']['parent_id'],  // parent id will refer to bom_parent to prevent duplicate
				'bom_parent' => $child['BomChild']['id'],
				'no_of_order' => $no_of_order,
				'margin' => 0,
				'margin_unit' => 1,
				'inventory_item_id' => $child['BomChild']['inventory_item_id'],
				'quantity_bal' => $no_of_order,
				'quantity_req' => 0,
				'issued_qty' => 0,
				'general_unit_id' => $child['BomChild']['general_unit_id'],
				'quantity' => $child['BomChild']['quantity'],
				'total_quantity' => $child['BomChild']['quantity'] * $no_of_order,
				'type' => 0 
				);  
			$this->ProductionOrderChild->save($data);
			$child_id = $this->ProductionOrderChild->getLastInsertId();
			$this->_copy_bom_item($bom_id, $child['BomChild']['id'], $production_order_id, $station_id, $no_of_order, $child_id);
		}  
	}

	private function _copy_bom_item($bom_id, $childs, $production_order_id, $station_id, $no_of_order, $child_id) {
		$this->loadModel('BomItem');
		 
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $childs	
				),
			'group' => 'BomItem.id'
			)); 
		 
		$this->loadModel('ProductionOrderItem');
		$insert = array();
		foreach ($items as $item) {
			$quantity_total = $item['BomItem']['quantity'] * $no_of_order;
			$price_total = $item['InventoryItem']['unit_price'] * $quantity_total;
			$insert[] = array( 
				'production_order_id' => $production_order_id,
				'production_order_child_id' => $child_id,
				'bom_id' => $bom_id,
				'inventory_item_id' => $item['BomItem']['inventory_item_id'],
				'quantity' => $item['BomItem']['quantity'],
				'no_of_order' => $no_of_order,
				'quantity_total' => $quantity_total, // jumlah order * kuantiti barang
				'quantity_bal' => $quantity_total,
				'general_unit_id' => $item['BomItem']['general_unit_id'],
				'price' => $item['InventoryItem']['unit_price'],
				'price_total' => $price_total,
				'margin' => 0,  // belum markup
				'margin_unit' => 1 // percent 2 == amount 
				);
		}
		
		$this->ProductionOrderItem->saveAll($insert, array('deep' => true));		 
	} 

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProductionOrder->exists($id)) {
			throw new NotFoundException(__('Invalid production order'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProductionOrder->save($this->request->data)) {
				$this->Session->setFlash(__('The production order has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The production order could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProductionOrder.' . $this->ProductionOrder->primaryKey => $id));
			$this->request->data = $this->ProductionOrder->find('first', $options);
		}
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProductionOrder->id = $id;
		if (!$this->ProductionOrder->exists()) {
			throw new NotFoundException(__('Invalid production order'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProductionOrder->delete()) {
			$this->Session->setFlash(__('The production order has been deleted.'));
		} else {
			$this->Session->setFlash(__('The production order could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function ajaxfindstationsyjobid() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['job_id'])) {
			$id = (int)$this->request->query['job_id'];
			$this->loadModel('SaleJobChild');
			$items = $this->SaleJobChild->find('all', array('conditions' => array('SaleJobChild.sale_job_id' => $id)));
			$json = array();
			foreach ($items as $item) {
				$json[] = array('id' => $item['SaleJobChild']['id'], 'name' => $item['SaleJobChild']['name']);
			}
			echo json_encode($json);	
		}
	}

	public function ajaxfindpo() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$conditions['OR']['ProductionOrder.name LIKE'] = '%'.$term.'%'; 

			$items = $this->ProductionOrder->find('all', array(
				'conditions' => $conditions,
				'group' => array('ProductionOrder.id'),
				'order' => array('ProductionOrder.id' => 'DESC')
				));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['ProductionOrder']['id'], 
					'name' => $item['ProductionOrder']['name'], 
					'bom_code' => $item['ProductionOrder']['bom_code'] 
					); 
			}
			echo json_encode($json);	
		}
	}
	
	public function ajaxfinditemsbystationid() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['job_childs_id'])) {
			$id = (int)$this->request->query['job_childs_id'];
			$this->loadModel('SaleJobItem');
			$items = $this->SaleJobItem->find('all', array('conditions' => array('SaleJobItem.sale_job_child_id' => $id)));
			$json = array();
			foreach ($items as $item) {
				$json[] = array('id' => $item['SaleJobItem']['id'], 'name' => $item['InventoryItem']['code'], 'bom_id' => $item['SaleJobItem']['bom_id'], 'quantity' => $item['SaleJobItem']['quantity']);
			}
			echo json_encode($json);	
		}		
	}
	
	public function productionBomChildsList($po_id) {		  
		$result = array(); 
		
		if (!$this->ProductionOrder->exists($po_id)) {
			throw new NotFoundException(__('Invalid Production Order'));
		}

		$this->loadModel('ProductionOrderChild');

		$rows = $this->ProductionOrderChild->find('all', array(
			'conditions' => array(
				'ProductionOrderChild.production_order_id' => $po_id
				)
			));
			
		foreach ($rows as $row) { 
		    $result[] = $row['ProductionOrderChild'];
		}

		return $result;
	}
	
	public function productionBomChilds($po_id, $bom_id) {		  
		$result = array(); 
		
		if (!$this->ProductionOrder->exists($po_id)) {
			throw new NotFoundException(__('Invalid Production Order'));
		}

		$this->loadModel('ProductionOrderChild');

		$rows = $this->ProductionOrderChild->find('all', array(
			'conditions' => array(
				'ProductionOrderChild.production_order_id' => $po_id,
				'ProductionOrderChild.sale_job_items_id' => $bom_id,
				'ProductionOrderChild.parent_id' => 0
				)
			));
			
		$childs = array();

		foreach ($rows as $row) { 
		    $childs['child'] = $row['ProductionOrderChild']; 

		    $childs['accordian'] = $this->check_child_or_items($po_id, $bom_id, $row['ProductionOrderChild']['id'], $row['ProductionOrderChild']['bom_parent']);

		    $result[] = $childs;
		}

		return $result;
	}

	public function productionBomChildsORItems($po_id = null, $bom_id = null, $po_child_id = null, $parent_id = null) {
		$childs_list = array();
		$items_list = array();  
		$result = array(); 		
		if (!$this->ProductionOrder->exists($po_id)) {
			throw new NotFoundException(__('Invalid Production Order'));
		}
		
		$this->loadModel('ProductionOrderChild');

		$rows = $this->ProductionOrderChild->find('all', array(
			'conditions' => array(
				'ProductionOrderChild.production_order_id' => $po_id,
				'ProductionOrderChild.sale_job_items_id' => $bom_id,
				'ProductionOrderChild.parent_id' => $parent_id
				)
			));
		// If no rows found, then try to show items
		$this->loadModel('ProductionOrderItem'); 
		$items = $this->ProductionOrderItem->find('all', array(
			'conditions' => array( 
				'ProductionOrderItem.production_order_id' => $po_id,
				'ProductionOrderItem.bom_id' => $bom_id,
				'ProductionOrderItem.production_order_child_id' => $po_child_id
				)
			));
		if($rows || $items) {
			foreach ($rows as $row) { 
				$childs_list['child'] = $row['ProductionOrderChild'];

				$childs_list['accordian'] = $this->check_child_or_items($po_id, $bom_id, $row['ProductionOrderChild']['id'], $row['ProductionOrderChild']['bom_parent']);

				$result['childs'][] = $childs_list;
			}
			
			foreach ($items as $item) { 
				$items_list['item'] = $item['ProductionOrderItem']; 

				$items_list['accordian'] = 'No';

				$result['items'][] = $items_list;
			}
		}
		return $result;
	}

	private function check_child_or_items($po_id, $bom_id, $po_child_id, $parent_id) {
		$this->loadModel('ProductionOrderChild');
		$rows = $this->ProductionOrderChild->find('all', array(
			'conditions' => array(
				'ProductionOrderChild.production_order_id' => $po_id,
				'ProductionOrderChild.sale_job_items_id' => $bom_id,
				'ProductionOrderChild.parent_id' => $parent_id
				)
			));
		if(count($rows) > 0) {
			return "Yes";
		} else {
			$this->loadModel('ProductionOrderItem');
			$rows = $this->ProductionOrderItem->find('all', array(
				'conditions' => array( 
				    'ProductionOrderItem.production_order_id' => $po_id,
					'ProductionOrderItem.bom_id' => $bom_id,
					'ProductionOrderItem.production_order_child_id' => $po_child_id
					)
				));
			if(count($rows) > 0) {
				return "Yes";
			} else {
				return 'No';
			}
		}	
	}

	public function getprogress() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$status = array();
		
		$status[] = array('value' => '0', 'text' => '0%');
		$status[] = array('value' => '20', 'text' => '20%');
		$status[] = array('value' => '40', 'text' => '40%');
		$status[] = array('value' => '60', 'text' => '60%');
		$status[] = array('value' => '80', 'text' => '80%');
		$status[] = array('value' => '100', 'text' => '100%');
		echo json_encode($status);
	}
	
	public function setprogress() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if(isset($_POST['value'])) {
			$id = $_POST['pk'];
	        $value = $_POST['value'];
			$json = array();
			if(is_numeric($value) && is_numeric($id)) {
				$update = $this->ProductionOrder->updateAll(
				array('ProductionOrder.progress' => "'".$value."'"),
				array('ProductionOrder.id' => $id)
				);  

				if($update) {
					$json['status'] = true;
				} else {
					$json['status'] = false;
				}
				$json['data'] = $_POST;
				echo json_encode($json);

			} else {
				header('HTTP 400 Bad Request', true, 400);
				echo "This field is required!";		
			}	
		}
		
	}

}