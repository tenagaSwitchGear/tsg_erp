<?php
App::uses('AppController', 'Controller');
/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class AccountDepartmentTransferBudgetController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccountDepartmentTransferBudget->recursive = 0;
		$this->set('accountDepartmentTransferBudget', $this->Paginator->paginate());  
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccountDepartmentTransferBudget->exists($id)) {
			throw new NotFoundException(__('Invalid Budget'));
		}
		$options = array('conditions' => array('accountDepartmentTransferBudget.' . $this->AccountDepartmentTransferBudget->primaryKey => $id));
		$this->set('accountDepartmentTransferBudget', $this->AccountDepartmentTransferBudget->find('first', $options));
	}

	public function ajaxbudget() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['acc_budget_id'])) {
			$id = (int)$this->request->query['acc_budget_id'];
			$this->loadModel('AccountDepartment');
			$gen_budget = $this->AccountDepartment->query("SELECT * FROM account_departments WHERE group_id=".$id);
			if(!empty($gen_budget)){
				$group_id = $gen_budget[0]['account_departments']['id'];

				$this->loadModel('AccountDepartmentBudgets');
				$sub_budget = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets WHERE account_department_id=".$group_id);
			
				$json = array();
				foreach ($sub_budget as $budget) {
					$json[] = array('id' => $budget['account_department_budgets']['id'], 'name' => $budget['account_department_budgets']['name']);
				}
				echo json_encode($json);
			}else{
				$json = array();

				$json[] = array('id'=>"99", 'name'=>"No budget allocated!");

				echo json_encode($json);
			}
		}
	}

	public function ajaxbudgetdetail() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['id'])) {
			$id = (int)$this->request->query['id'];
			$this->loadModel('AccountDepartmentBudgets');
			$budget = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets WHERE id=".$id);
			if(!empty($budget)){
				$budget_allocated = $budget[0]['account_department_budgets']['total'];
				$budget_used = $budget[0]['account_department_budgets']['used'];
				$budget_remaining = $budget_allocated - $budget_used;

				$budget_remaining = number_format($budget_remaining, 4);
				echo json_encode($budget_remaining);
			}else{
				
			}
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null) {
		
		if ($this->request->is('post')) {
			$this->AccountDepartmentTransferBudget->create();
			$group_id_to = $_POST['group_id_to'];
			$group_id_from = $_POST['group_id_from'];
			$account_department_budget_id_to = $_POST['transfer_to_id'];
			$account_department_budget_id_from = $_POST['transfer_from_id'];
			$amount = $_POST['amount'];
			$revised = $_POST['revised'];
			$created = date('Y-m-d H:i:s');
			$user_id = $_SESSION['Auth']['User']['id'];

			$data_transfer_budget = array('group_id_to' => $group_id_to, 'group_id_from' => $group_id_from, 'account_department_budget_id_to' => $account_department_budget_id_to, 'account_department_budget_id_from' => $account_department_budget_id_from, 'amount' => $amount, 'revised_budget' => $revised, 'created' => $created, 'status' => '0', 'request_by' => $user_id);

			if($this->AccountDepartmentTransferBudget->save($data_transfer_budget)){
				$this->Session->setFlash(__('The Transfer Budgets has been created.'), 'success');
				return $this->redirect(array('action' => 'index'));
			}else{
				$this->Session->setFlash(__('The Transfer Budgets could not be created. Please, try again.'), 'error');
			}
		}

		$this->loadModel('Group');
		$groups = $this->Group->find('list');

		$this->loadModel('AccountDepartmentBudgets');
		$budgets = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets INNER JOIN account_departments on account_department_budgets.account_department_id = account_departments.id INNER JOIN groups on account_departments.group_id = groups.id GROUP BY account_department_id");

		$department = $this->Group->query("SELECT * from groups WHERE id=".$_SESSION['Auth']['User']['group_id']);
	
		$this->set(compact('groups', 'budgets', 'department'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if ($this->request->is('post')) {
			$get_date = $this->AccountDepartmentTransferBudget->query("SELECT * FROM account_department_transfer_budgets WHERE id=".$_POST['id']);
			$this->AccountDepartmentTransferBudget->create();
			if($_POST['actions'] == '1'){
				$data = array(
					'id' => $_POST['id'],
					'created' => $_POST['created'],
					'review_date' => date('Y-m-d H:i:s'),
					'review_by' => $_SESSION['Auth']['User']['id'],
					'status'  => '1'
				);
				if($this->AccountDepartmentTransferBudget->save($data)){
					$this->Session->setFlash(__('The Transfer Budgets has been updated.'), 'success');
					return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The Transfer Budgets could not be updated. Please, try again.'), 'error');
				}
			}else if($_POST['actions'] == '2'){
				$data = array(
					'id' => $_POST['id'],
					'created' => $_POST['created'],
					'review_date' => $get_date[0]['account_department_transfer_budgets']['review_date'],
					'verify_date' => date('Y-m-d H:i:s'),
					'verify_by' => $_SESSION['Auth']['User']['id'],
					'status' => '2'
				);
				if($this->AccountDepartmentTransferBudget->save($data)){
					$this->Session->setFlash(__('The Transfer Budgets has been updated.'), 'success');
					return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The Transfer Budgets could not be updated. Please, try again.'), 'error');
				}
			}else if($_POST['actions'] == '3'){
				$data = array(
					'id' => $_POST['id'],
					'created' => $_POST['created'],
					'review_date' => $get_date[0]['account_department_transfer_budgets']['review_date'],
					'verify_date' => $get_date[0]['account_department_transfer_budgets']['verify_date'],
					'recommended_date' => date('Y-m-d H:i:s'),
					'recommended_by' => $_SESSION['Auth']['User']['id'],
					'status' => '3'
				);
				if($this->AccountDepartmentTransferBudget->save($data)){
					$this->Session->setFlash(__('The Transfer Budgets has been updated.'), 'success');
					return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The Transfer Budgets could not be updated. Please, try again.'), 'error');
				}
			}else if($_POST['actions'] == '4'){
				$data = array(
					'id' => $_POST['id'],
					'created' => $_POST['created'],
					'review_date' => $get_date[0]['account_department_transfer_budgets']['review_date'],
					'verify_date' => $get_date[0]['account_department_transfer_budgets']['verify_date'],
					'recommended_date' => $get_date[0]['account_department_transfer_budgets']['recommended_date'],
					'approval_date' => date('Y-m-d H:i:s'),
					'approved_by' => $_SESSION['Auth']['User']['id'],
					'status'      => '4'
				);
				if($this->AccountDepartmentTransferBudget->save($data)){
					$this->Session->setFlash(__('The Transfer Budgets has been updated.'), 'success');
					return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The Transfer Budgets could not be updated. Please, try again.'), 'error');
				}
			}else if($_POST['actions'] == '5'){
				$data = array(
					'id' => $_POST['id'],
					'created' => $_POST['created'],
					'review_date' => $get_date[0]['account_department_transfer_budgets']['review_date'],
					'verify_date' => $get_date[0]['account_department_transfer_budgets']['verify_date'],
					'recommended_date' => $get_date[0]['account_department_transfer_budgets']['recommended_date'],
					'approval_date' => date('Y-m-d H:i:s'),
					'approved_by' => $_SESSION['Auth']['User']['id'],
					'status'      => '5'
				);
				if($this->AccountDepartmentTransferBudget->save($data)){
					$this->Session->setFlash(__('The Transfer Budgets has been updated.'), 'success');
					return $this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash(__('The Transfer Budgets could not be updated. Please, try again.'), 'error');
				}
			}
		}
		$options = array('conditions' => array('accountDepartmentTransferBudget.' . $this->AccountDepartmentTransferBudget->primaryKey => $id));
		$this->set('accountDepartmentTransferBudget', $this->AccountDepartmentTransferBudget->find('first', $options));

		$user_id = $_SESSION['Auth']['User']['id'];
		$group_id = $_SESSION['Auth']['User']['group_id'];

		$this->loadModel('InternalDepartment');
		$check = $this->InternalDepartment->query("SELECT * FROM internal_departments WHERE user_id=".$user_id);
		
		$transfer_budget = $this->AccountDepartmentTransferBudget->find('first', array('conditions'=> array('AccountDepartmentTransferBudget.id' => $id)));
		
		if(empty($check)){
			if($group_id == '18'){
				$role = '2';
			}else if($group_id == $transfer_budget['GroupsTo']['id']){
				$role = '4';
			}
		}else{
			if($group_id == '18'){
				$role = '3';
			}else if($group_id == $transfer_budget['GroupsTo']['id']){
				$role = '4';
			}else if($group_id == '19'){
				$role = '5';
			}
		}

		$this->set(compact('role'));
	}

	

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id_2 = null, $id = null) {
		$this->loadModel('AccountDepartmentBudgets');
		$this->AccountDepartmentBudgets->id = $id;
		if (!$this->AccountDepartmentBudgets->exists()) {
			throw new NotFoundException(__('Invalid Department Account Budget Id'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AccountDepartmentBudgets->delete()) {
			$this->Session->setFlash(__('The Department Account Budget has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The Department Account Budget could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('controller' => 'accountdepartment', 'action' => 'view', $id_2));
	}
}