<?php
App::uses('AppController', 'Controller');
/**
 * SaleBoms Controller
 *
 * @property SaleBom $SaleBom
 * @property PaginatorComponent $Paginator
 */
class SaleBomsController extends AppController {


/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

	// ID = SaleQuotationId
	public function costing($id = null) {
		$this->loadModel('SaleQuotationItemCost'); 
		$this->loadModel('SaleQuotationItem');
		$costs = $this->SaleQuotationItemCost->find('all', array(
			'conditions' => array(
				'SaleQuotationItemCost.sale_quotation_item_id' => $id,
				'SaleQuotationItemCost.type !=' => 'Soft Cost'
				)
			));

		$soft_costs = $this->SaleQuotationItemCost->find('all', array(
			'conditions' => array(
				'SaleQuotationItemCost.sale_quotation_item_id' => $id,
				'SaleQuotationItemCost.type' => 'Soft Cost'
				)
			));

		$item = $this->SaleQuotationItem->find('first', array(
			'conditions' => array(
				'SaleQuotationItem.id' => $id
				)
			));

		if ($this->request->is('post')) {
			if($costs || $soft_costs) {
				// Delete existing record, then insert
				$this->SaleQuotationItemCost->deleteAll(array('SaleQuotationItemCost.sale_quotation_item_id' => $id), false);
			}
			
			$name = $this->request->data['SaleQuotationItemCost']['name'];
			$quantity = $this->request->data['SaleQuotationItemCost']['quantity'];
			$item_id = $this->request->data['SaleQuotationItemCost']['sale_quotation_item_id'];

			$inventory_item_id = $this->request->data['SaleQuotationItemCost']['inventory_item_id'];

			$unit_id = $this->request->data['SaleQuotationItemCost']['general_unit_id']; 

			$price = $this->request->data['SaleQuotationItemCost']['price']; 
			$margin = $this->request->data['SaleQuotationItemCost']['margin']; 
			$total_price = $this->request->data['SaleQuotationItemCost']['total_price']; 
			$type = $this->request->data['SaleQuotationItemCost']['type']; 
			$planning_price = 0;
			for($i = 0; $i < count($name); $i++) {
				$this->SaleQuotationItemCost->create();
				$planning_price += $total_price[$i];
				$data = array(
					'name' => $name[$i], 
					'inventory_item_id' => $inventory_item_id[$i],
					'quantity' => $quantity[$i],
					'sale_quotation_item_id' => $id,
					'general_unit_id' => $unit_id[$i],
					'price' => $price[$i],
					'margin' => $margin[$i],
					'total_price' => $total_price[$i],
					'type' => $type[$i]
					);
				$this->SaleQuotationItemCost->save($data);	
			}

			// Update item planning_price
			$this->SaleQuotationItem->id = $id;
			$this->SaleQuotationItem->saveField('planning_costing', $planning_price);
			
			$this->Session->setFlash(__('Costing has been saved.'), 'success');
			return $this->redirect(array('controller' => 'costings', 'action' => 'view/' . $item['SaleQuotationItem']['sale_quotation_id'])); 	
		}

		
		 

		$this->set('item', $item);  

		$this->set('costs', $costs); 
		$this->set('softcosts', $soft_costs); 

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');

		$this->set(compact('units'));
	} 

	public function margin($id = null) {

		$this->loadModel('SaleQuotationItem'); 
		$item = $this->SaleQuotationItem->find('first', array(
			'conditions' => array(
				'SaleQuotationItem.id' => $id
				)
			));
		$this->set('item', $item);  
		if ($this->request->is('post')) {

		}
	}

	public function spare_not_use() {
		if ($this->request->is('post')) {
			$customer = $this->SaleQuotation->SaleTender->findCustomerByTenderId($this->request->data['SaleQuotation']['sale_tender_id']);
			$this->request->data['SaleQuotation']['customer_id'] = $customer['SaleTender']['customer_id']; 

			$this->SaleQuotation->create();

			$name = $this->request->data['SaleQuotationItem']['name'];
			$product_id = $this->request->data['SaleQuotationItem']['product_id'];
			$boms = $this->request->data['SaleQuotationItem']['bom_id'];
			$orders = $this->request->data['SaleQuotationItem']['quantity']; 

			$unit_price = $this->request->data['SaleQuotationItem']['unit_price'];
			$general_unit_id = $this->request->data['SaleQuotationItem']['general_unit_id'];
 			$discount = $this->request->data['SaleQuotationItem']['discount'];
			$tax = $this->request->data['SaleQuotationItem']['tax'];
 
			if ($this->SaleQuotation->save($this->request->data)) {
				$quotation_id = $this->SaleQuotation->getLastInsertId(); 
				
				// Copy Bom to salebom
				for($i = 0; $i < count($boms); $i++) {
					if($boms[$i] != 0) {
						$this->_copy_bom($boms[$i], $quotation_id, $this->request->data['SaleQuotation']['sale_tender_id'], $orders[$i], 0, 0);
					}  
				} 

				// iTEMS
				$this->loadModel('SaleQuotationItem');
				for($i = 0; $i < count($product_id); $i++) {
					// Insert items
					$this->SaleQuotationItem->create(); 
					$item = array(
						'name'              => $name[$i],
						'quantity'          => $orders[$i],
						'general_unit_id'   => $general_unit_id[$i],
						'unit_price'        => $unit_price[$i],
						'discount'          => $discount[$i],
						'tax'               => $tax[$i],
						'total_price'       => $this->total_price($orders[$i], $unit_price[$i], $discount[$i], $tax[$i]),
						'product_id'        => $product_id[$i],
						'sale_quotation_id' => $quotation_id,
						'type'              => 0
						); 
					$this->SaleQuotationItem->save($item);
				}

				// If status == 1, then submit to hod
				if($this->request->data['SaleQuotation']['status'] == 1) {
					$this->submit_verification('salequotations/view/'.$quotation_id.'?ref=email', 'Verification For Tender Quotation', 'Tender Quotation', 'HOD', 'Verification');
				}

				$this->Session->setFlash(__('The sale quotation has been saved.'), 'success');
				return $this->redirect(array('action' => 'quotation/' . $quotation_id));
			} else {
				$this->Session->setFlash(__('The sale quotation could not be saved. Please, try again.'), 'error');
			}
		}

		// Currency

		$this->loadModel('GeneralCurrency');

		$currencies = $this->GeneralCurrency->find('all');
		$this->set('currencies', $currencies);

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');

		$this->loadModel('InventoryItemCategory');
		$item_categories = $this->InventoryItemCategory->find('list');

		$saleTenders = $this->SaleQuotation->SaleTender->find('list');
		$customers = $this->SaleQuotation->Customer->find('list');
		$saleBoms = $this->SaleQuotation->SaleBom->find('list');
		$this->loadModel('BomCategory');
		$categories = $this->BomCategory->find('all');
		$bomcategories = $this->BomCategory->find('list');
		$this->set('categories', $categories);
		$this->set(compact('saleTenders', 'customers', 'saleBoms', 'bomcategories', 'units', 'item_categories'));
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleBom->recursive = 0;
		$this->set('saleBoms', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleBom->exists($id)) {
			throw new NotFoundException(__('Invalid sale bom'));
		}
		$options = array('conditions' => array('SaleBom.' . $this->SaleBom->primaryKey => $id));
		$this->set('saleBom', $this->SaleBom->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleBom->create();
			if ($this->SaleBom->save($this->request->data)) {
				$this->Session->setFlash(__('The sale bom has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale bom could not be saved. Please, try again.'));
			}
		}
		$boms = $this->SaleBom->Bom->find('list');
		$saleTenders = $this->SaleBom->SaleTender->find('list');
		$users = $this->SaleBom->User->find('list');
		$this->set(compact('boms', 'saleTenders', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleBom->exists($id)) {
			throw new NotFoundException(__('Invalid sale bom'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleBom->save($this->request->data)) {
				$this->Session->setFlash(__('The sale bom has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale bom could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleBom.' . $this->SaleBom->primaryKey => $id));
			$this->request->data = $this->SaleBom->find('first', $options);
		}
		$boms = $this->SaleBom->Bom->find('list');
		$saleTenders = $this->SaleBom->SaleTender->find('list');
		$users = $this->SaleBom->User->find('list');
		$this->set(compact('boms', 'saleTenders', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleBom->id = $id;
		if (!$this->SaleBom->exists()) {
			throw new NotFoundException(__('Invalid sale bom'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleBom->delete()) {
			$this->Session->setFlash(__('The sale bom has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale bom could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
