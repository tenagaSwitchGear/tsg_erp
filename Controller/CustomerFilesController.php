<?php
App::uses('AppController', 'Controller');
/**
 * CustomerFiles Controller
 *
 * @property CustomerFile $CustomerFile
 * @property PaginatorComponent $Paginator
 */
class CustomerFilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CustomerFile->recursive = 0;
		$this->set('customerFiles', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CustomerFile->exists($id)) {
			throw new NotFoundException(__('Invalid customer file'));
		}
		$options = array('conditions' => array('CustomerFile.' . $this->CustomerFile->primaryKey => $id));
		$this->set('customerFile', $this->CustomerFile->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			//print_r($_POST);
			//exit;
			$this->CustomerFile->create();
			if ($this->CustomerFile->save($this->request->data)) {
				$this->Session->setFlash(__('The customer file has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer file could not be saved. Please, try again.'), 'error');
			}
		}
		$customers = $this->CustomerFile->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CustomerFile->exists($id)) {
			throw new NotFoundException(__('Invalid customer file'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomerFile->save($this->request->data)) {
				$this->Session->setFlash(__('The customer file has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer file could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('CustomerFile.' . $this->CustomerFile->primaryKey => $id));
			$this->request->data = $this->CustomerFile->find('first', $options);
		}
		$customers = $this->CustomerFile->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CustomerFile->id = $id;
		if (!$this->CustomerFile->exists()) {
			throw new NotFoundException(__('Invalid customer file'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomerFile->delete()) {
			$this->Session->setFlash(__('The customer file has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The customer file could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
