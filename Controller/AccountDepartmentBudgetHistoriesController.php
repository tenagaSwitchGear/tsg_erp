<?php
App::uses('AppController', 'Controller');
/**
 * AccountDepartmentBudgetHistories Controller
 *
 * @property AccountDepartmentBudgetHistory $AccountDepartmentBudgetHistory
 * @property PaginatorComponent $Paginator
 */
class AccountDepartmentBudgetHistoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }
/**
 * index method
 *
 * @return void
 */
	public function index() { 

		$conditions = array();  

		$record_per_page = Configure::read('Reading.nodes_per_page');

		if(isset($_GET['search'])) {
			if($_GET['po_no'] != '') {
				$conditions['InventoryPurchaseOrder.po_no LIKE'] = '%'.trim($_GET['po_no']).'%';
			}
			if($_GET['from'] != '') {
				$conditions['AccountDepartmentBudgetHistory.create >='] = '%'.trim($_GET['from']).'%';
			}
			if($_GET['to'] != '') {
				$conditions['AccountDepartmentBudgetHistory.create <='] = '%'.trim($_GET['to']).'%';
			}
			$this->request->data['AccountDepartmentBudgetHistory'] = $_GET;
		}

		$conditions['AccountDepartmentBudgetHistory.user_id !='] = 0;
		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'AccountDepartmentBudgetHistory.id DESC',
			'limit' => $record_per_page
			);   

		$this->AccountDepartmentBudgetHistory->recursive = 0;
		$this->set('accountDepartmentBudgetHistories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccountDepartmentBudgetHistory->exists($id)) {
			throw new NotFoundException(__('Invalid account department budget history'));
		}
		$options = array('conditions' => array('AccountDepartmentBudgetHistory.' . $this->AccountDepartmentBudgetHistory->primaryKey => $id));
		$this->set('accountDepartmentBudgetHistory', $this->AccountDepartmentBudgetHistory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AccountDepartmentBudgetHistory->create();
			if ($this->AccountDepartmentBudgetHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The account department budget history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account department budget history could not be saved. Please, try again.'));
			}
		}
		$accountDepartmentBudgets = $this->AccountDepartmentBudgetHistory->AccountDepartmentBudget->find('list');
		$inventoryPurchaseOrders = $this->AccountDepartmentBudgetHistory->InventoryPurchaseOrder->find('list');
		$users = $this->AccountDepartmentBudgetHistory->User->find('list');
		$this->set(compact('accountDepartmentBudgets', 'inventoryPurchaseOrders', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AccountDepartmentBudgetHistory->exists($id)) {
			throw new NotFoundException(__('Invalid account department budget history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AccountDepartmentBudgetHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The account department budget history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account department budget history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AccountDepartmentBudgetHistory.' . $this->AccountDepartmentBudgetHistory->primaryKey => $id));
			$this->request->data = $this->AccountDepartmentBudgetHistory->find('first', $options);
		}
		$accountDepartmentBudgets = $this->AccountDepartmentBudgetHistory->AccountDepartmentBudget->find('list');
		$inventoryPurchaseOrders = $this->AccountDepartmentBudgetHistory->InventoryPurchaseOrder->find('list');
		$users = $this->AccountDepartmentBudgetHistory->User->find('list');
		$this->set(compact('accountDepartmentBudgets', 'inventoryPurchaseOrders', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AccountDepartmentBudgetHistory->id = $id;
		if (!$this->AccountDepartmentBudgetHistory->exists()) {
			throw new NotFoundException(__('Invalid account department budget history'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AccountDepartmentBudgetHistory->delete()) {
			$this->Session->setFlash(__('The account department budget history has been deleted.'));
		} else {
			$this->Session->setFlash(__('The account department budget history could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
