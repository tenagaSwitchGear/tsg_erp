<?php
App::uses('AppController', 'Controller');
/**
 * InventoryItemIssueCosts Controller
 *
 * @property InventoryItemIssueCost $InventoryItemIssueCost
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryItemIssueCostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryItemIssueCost->recursive = 0;
		$this->set('inventoryItemIssueCosts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryItemIssueCost->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item issue cost'));
		}
		$options = array('conditions' => array('InventoryItemIssueCost.' . $this->InventoryItemIssueCost->primaryKey => $id));
		$this->set('inventoryItemIssueCost', $this->InventoryItemIssueCost->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryItemIssueCost->create();
			if ($this->InventoryItemIssueCost->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory item issue cost has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory item issue cost could not be saved. Please, try again.'));
			}
		}
		$inventoryMaterialRequestItems = $this->InventoryItemIssueCost->InventoryMaterialRequestItem->find('list');
		$productionOrders = $this->InventoryItemIssueCost->ProductionOrder->find('list');
		$inventoryItems = $this->InventoryItemIssueCost->InventoryItem->find('list');
		$this->set(compact('inventoryMaterialRequestItems', 'productionOrders', 'inventoryItems'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryItemIssueCost->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item issue cost'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryItemIssueCost->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory item issue cost has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory item issue cost could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryItemIssueCost.' . $this->InventoryItemIssueCost->primaryKey => $id));
			$this->request->data = $this->InventoryItemIssueCost->find('first', $options);
		}
		$inventoryMaterialRequestItems = $this->InventoryItemIssueCost->InventoryMaterialRequestItem->find('list');
		$productionOrders = $this->InventoryItemIssueCost->ProductionOrder->find('list');
		$inventoryItems = $this->InventoryItemIssueCost->InventoryItem->find('list');
		$this->set(compact('inventoryMaterialRequestItems', 'productionOrders', 'inventoryItems'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryItemIssueCost->id = $id;
		if (!$this->InventoryItemIssueCost->exists()) {
			throw new NotFoundException(__('Invalid inventory item issue cost'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryItemIssueCost->delete()) {
			$this->Session->setFlash(__('The inventory item issue cost has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory item issue cost could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
