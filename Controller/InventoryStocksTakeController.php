<?php
App::uses('AppController', 'Controller');
/**
 * InventoryStocks Controller
 *
 * @property InventoryStock $InventoryStock
 * @property PaginatorComponent $Paginator
 */
class InventoryStocksTakeController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->query("SELECT * FROM inventory_stocks INNER JOIN inventory_items ON inventory_stocks.inventory_item_id = inventory_items.id");
		$InventoryStocksTake = $stock;

		//$stock = $this->InventoryStock->find('list');
		$this->set(compact('InventoryStocksTake', $this->Paginator->paginate()));
	}

	public function transfer() {
		$this->InventoryStock->recursive = 0;
		$this->set('inventoryStocks', $this->Paginator->paginate());
	}

	public function exchange($id = null) {
		if (!$this->InventoryStock->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item'));
		}

		if ($this->request->is('post')) {
			
			//print_r($_POST);

			$id = $_POST['inventory_stocks_id'];
			$get_details_stock = $this->InventoryStock->query("SELECT * FROM inventory_stocks INNER JOIN inventory_items on inventory_stocks.inventory_item_id = inventory_items.id WHERE inventory_stocks.id='".$id."'");

			if($_POST['transfer_type']=='2'){
				$warehouse_id = $_POST['inventory_delivery_location_id'];
				$store_id = $_POST['inventory_store_id'];
				$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];
				$new_quantity = $_POST['quantity'];

				if($new_quantity < $quantity){
					$new_quantity_2 = $quantity - $new_quantity;
					$this->InventoryStock->create();
					$data_item = array(
						'id' 								=> $id,
						'quantity' 							=> $new_quantity_2
					);

					$this->InventoryStock->save($data_item);

					$this->InventoryStock->create();
					$data_item_2 = array(
						'general_unit_id' => $get_details_stock[0]['inventory_stocks']['general_unit_id'],
						'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
						'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
						'quantity' => $new_quantity,
						'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
						'created' => date('Y-m-d H:i:s'),
						'inventory_delivery_location_id' => $warehouse_id,
						'location_id' => $store_id
					);

					if($this->InventoryStock->save($data_item_2)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $new_quantity,
							'status'   => '1',
							'type'     => '2'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}else if($new_quantity == $quantity){
					$this->InventoryStock->create();
					$data_item = array(
						'id'			=> $id,
						'inventory_delivery_location_id' => $warehouse_id,
						'location_id' => $store_id,
						'rack_id' => '0'
					);

					if($this->InventoryStock->save($data_item)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $quantity,
							'status'   => '1',
							'type'     => '2'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}
			}else if($_POST['transfer_type']=='3'){
				$store_id = $_POST['inventory_store_id'];
				$new_quantity = $_POST['quantity'];
				$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];

				if($new_quantity < $quantity){
					$new_quantity_2 = $quantity - $new_quantity;
					$this->InventoryStock->create();
					$data_item = array(
						'id' 								=> $id,
						'quantity' 							=> $new_quantity_2
					);

					$this->InventoryStock->save($data_item);

					$this->InventoryStock->create();
					$data_item_2 = array(
						'general_unit_id' => $get_details_stock[0]['inventory_stocks']['general_unit_id'],
						'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
						'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
						'quantity' => $new_quantity,
						'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
						'created' => date('Y-m-d H:i:s'),
						'inventory_delivery_location_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
						'location_id' => $store_id
					);

					if($this->InventoryStock->save($data_item_2)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $new_quantity,
							'status'   => '1',
							'type'     => '3'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}else if($new_quantity == $quantity){
					$this->InventoryStock->create();
					$data_item = array(
						'id'			=> $id,
						'location_id' => $store_id,
						'rack_id' => '0'
					);

					if($this->InventoryStock->save($data_item)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $quantity,
							'status'   => '1',
							'type'     => '3'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}
			}else if($_POST['transfer_type'] == '4'){
				$this->loadModel('InventoryItem');
				$code = $_POST['code'];
							
				$check_code = $this->InventoryItem->query("SELECT * FROM inventory_items WHERE code='".$code."'");
				
				if(empty($check_code)){
					$this->Session->setFlash(__('The code could not be found saved.'), 'error');
				}else{
					$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];
					$new_quantity = $_POST['quantity'];

					if($new_quantity < $quantity){
						$new_quantity_2 = $quantity - $new_quantity;
						$this->InventoryStock->create();
						$data_item = array(
							'id' 								=> $id,
							'quantity' 							=> $new_quantity_2
						);

						$this->InventoryStock->save($data_item);

						$this->InventoryStock->create();
						$data_item_2 = array(
							'general_unit_id' => $check_code[0]['inventory_items']['general_unit_id'],
							'inventory_item_id' => $check_code[0]['inventory_items']['id'],
							'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
							'quantity' => $new_quantity,
							'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
							'created' => date('Y-m-d H:i:s'),
							'inventory_delivery_location_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
							'location_id' => $get_details_stock[0]['inventory_stocks']['location_id']
						);

						if($this->InventoryStock->save($data_item_2)){
							$this->loadModel('InventoryMaterialHistory');
							$this->InventoryMaterialHistory->create();
							$data_history = array(
								'user_id' => $_SESSION['Auth']['User']['id'],
								'inventory_item_id' => $check_code[0]['inventory_items']['id'],
								'quantity' => $new_quantity,
								'status'   => '1',
								'type'     => '4'
							);

							if($this->InventoryMaterialHistory->save($data_history)){
								$new_quantity_inventory_item = $check_code[0]['inventory_items']['quantity'] + $new_quantity;
								$new_quantity_inventory_item_2 = $get_details_stock[0]['inventory_items']['quantity'] - $new_quantity;

								$this->InventoryItem->create();
								$item = array(
									'id' => $check_code[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item
								);

								$this->InventoryItem->save($item);

								$this->InventoryItem->create();
								$item_2 = array(
									'id' => $get_details_stock[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item_2
								);

								if($this->InventoryItem->save($item_2)){
									$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
									return $this->redirect(array('action' => 'transfer'));
								}else{
									$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
								}
							}else{
								$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
							}					
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}
					}else if($new_quantity == $quantity){
						$data_item = array(
							'id' => $id,
							'inventory_item_id' => $check_code[0]['inventory_items']['id'],
							'general_unit_id' => $check_code[0]['inventory_items']['general_unit_id']
						);

						if($this->InventoryStock->save($data_item_2)){
							$this->loadModel('InventoryMaterialHistory');
							$this->InventoryMaterialHistory->create();
							$data_history = array(
								'user_id' => $_SESSION['Auth']['User']['id'],
								'inventory_item_id' => $check_code[0]['inventory_items']['id'],
								'quantity' => $new_quantity,
								'status'   => '1',
								'type'     => '4'
							);

							if($this->InventoryMaterialHistory->save($data_history)){
								$new_quantity_inventory_item = $check_code[0]['inventory_items']['quantity'] + $new_quantity;
								$new_quantity_inventory_item_2 = $get_details_stock[0]['inventory_items']['quantity'] - $new_quantity;

								$this->InventoryItem->create();
								$item = array(
									'id' => $check_code[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item
								);

								$this->InventoryItem->save($item);

								$this->InventoryItem->create();
								$item_2 = array(
									'id' => $get_details_stock[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item_2
								);

								if($this->InventoryItem->save($item_2)){
									$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
									return $this->redirect(array('action' => 'transfer'));
								}else{
									$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
								}
							}else{
								$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
							}	
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}
					}
				}
			}
		}

		$this->loadModel('TransferType');
		$transfer = $this->TransferType->find('list');

		$this->loadModel('InventoryDeliveryLocation');
		$warehouse = $this->InventoryDeliveryLocation->find('list');

		$options = array('conditions' => array('InventoryStock.' . $this->InventoryStock->primaryKey => $id));
		$this->set('inventoryItem', $this->InventoryStock->find('first', $options));

		$ditem = $this->InventoryStock->find('first', array('conditions'=> array('InventoryStock.id' => $id)));

		$this->loadModel('InventoryStore');

		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE id=".$ditem['InventoryItem']['inventory_store_id']);
		
		$store_2 = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE inventory_delivery_location_id=".$ditem['InventoryStock']['inventory_delivery_location_id']);

		$this->loadModel('InventoryDeliveryLocation');

		$this->set(compact('transfer', 'warehouse', 'store', 'warehouse_location', 'store_2'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->loadModel('TransferType');
		$transfer = $this->TransferType->find('list');

		$this->loadModel('InventoryDeliveryLocation');
		$warehouse = $this->InventoryDeliveryLocation->find('list');

		$options = array('conditions' => array('InventoryStock.' . $this->InventoryStock->primaryKey => $id));
		$this->set('inventoryStock', $this->InventoryStock->find('first', $options));

		$ditem = $this->InventoryStock->find('first', array('conditions'=> array('InventoryStock.id' => $id)));

		$this->loadModel('InventoryStore');

		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE id=".$ditem['InventoryStock']['inventory_store_id']);
		
		$store_2 = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE inventory_delivery_location_id=".$ditem['InventoryStock']['inventory_delivery_location_id']);

		$this->loadModel('InventoryDeliveryLocation');

		$this->set(compact('transfer', 'warehouse', 'store', 'store_2'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryStock->create();

			$this->request->data['InventoryStock']['issued_quantity'] = 0;
			$this->request->data['InventoryStock']['created'] = $this->date;
			$this->request->data['InventoryStock']['user_id'] = $this->Session->read('Auth.User.id');
			$this->request->data['InventoryStock']['qty_in'] = $this->request->data['InventoryStock']['quantity'];
			
			if ($this->InventoryStock->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory stock has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory stock could not be saved. Please, try again.'), 'error');
			}
		}
		$generalUnits = $this->InventoryStock->GeneralUnit->find('list');
		$inventoryItems = $this->InventoryStock->InventoryItem->find('list');
		$inventoryDeliveryOrderItems = $this->InventoryStock->InventoryDeliveryOrderItem->find('list');
		$generalUnitConverters = $this->InventoryStock->GeneralUnitConverter->find('list');
		$inventoryLocations = $this->InventoryStock->InventoryLocation->find('list');
		
		$inventoryStores = $this->InventoryStock->InventoryStore->find('list');
		
 
		
		$inventoryDeliveryLocations = $this->InventoryStock->InventoryDeliveryLocation->find('list'); 
		
		$this->set(compact('generalUnits', 'inventoryItems', 'inventoryDeliveryOrderItems', 'generalUnitConverters', 'inventoryLocations', 'inventoryStores', 'inventoryDeliveryLocations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryStock->exists($id)) {
			throw new NotFoundException(__('Invalid inventory stock'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryStock->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory stock has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory stock could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('InventoryStock.' . $this->InventoryStock->primaryKey => $id));
		$this->request->data = $this->InventoryStock->find('first', $options);
		
		$generalUnits = $this->InventoryStock->GeneralUnit->find('list');
		$inventoryItems = $this->InventoryStock->InventoryItem->find('list');
		$inventoryDeliveryOrderItems = $this->InventoryStock->InventoryDeliveryOrderItem->find('list');
		$generalUnitConverters = $this->InventoryStock->GeneralUnitConverter->find('list');
		$inventoryLocations = $this->InventoryStock->InventoryLocation->find('list');
		$inventoryStores = $this->InventoryStock->InventoryStore->find('list');
		$inventoryRacks = $this->InventoryStock->InventoryRack->find('list');
		$inventoryDeliveryLocations = $this->InventoryStock->InventoryDeliveryLocation->find('list'); 
		$this->set(compact('generalUnits', 'inventoryItems', 'inventoryDeliveryOrderItems', 'generalUnitConverters', 'inventoryLocations', 'inventoryStores', 'inventoryRacks', 'inventoryDeliveryLocations'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryStock->id = $id;
		if (!$this->InventoryStock->exists()) {
			throw new NotFoundException(__('Invalid inventory stock'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryStock->delete()) {
			$this->Session->setFlash(__('The inventory stock has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory stock could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}