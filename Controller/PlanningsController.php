<?php
App::uses('AppController', 'Controller');
/**
 * Plannings Controller
 * 
 * @property Planning $Planning
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PlanningsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxsaveplanorder');
	} 

	public function ajaxreject() {
		$this->autoRender = false;
		if(isset($_POST)) {
			$json = array();
			$note = $this->request->data['note']; 
			$supplier_id = $this->request->data['supplier_id'];
			$planning_id = $this->request->data['planning_id'];

			$json['status'] = false;
			if($note == '') {
				$json['msg'] = 'Please fill note';
			} else {
				$this->loadModel('PlanningItemRemark');
				$this->PlanningItemRemark->create();
				$data = array(
					'note' => $note,
					'inventory_supplier_id' => $supplier_id,
					'planning_id' => $planning_id,
					'user_id' => $this->user_id,
					'created' => $this->date
					);
				$save = $this->PlanningItemRemark->save($data);
				if($save) {
					$json['status'] = true;
					$json['msg'] = 'Remark has been saved';
				} else {
					$json['msg'] = 'System error';
				}
			}
		}
	}

	public function ajaxsaveplanorder() {
		$this->autoRender = false;

		if(isset($_GET)) {
			$json = array();
			//head
			$is_sub_assembly = $this->request->query['is_sub_assembly']; 
			$item_id = $this->request->query['item_id']; 
			$on_hand = $this->request->query['on_hand']; 
			$ordered = $this->request->query['ordered']; 
			$planning_general_unit = $this->request->query['planning_general_unit']; 
			$planning_id = $this->request->query['planning_id']; 
			$planning_qty = $this->request->query['planning_qty']; 
			$production = $this->request->query['production'];  
			$supplier = $this->request->query['supplier']; 
			$total_sum = $this->request->query['total_sum']; // after conversion
			$general_unit_id = $this->request->query['general_unit_id']; 
			$unit_price = $this->request->query['unit_price'];  
			$price = $this->request->query['price'];
			$required = $this->request->query['required'];
			$general_currency_id = $this->request->query['general_currency_id']; 

			// Job
			$buffer = $this->request->query['buffer']; 
			$inventory_supplier_item_id = $this->request->query['inventory_supplier_item_id']; 
			$is_sub_assembly = $this->request->query['is_sub_assembly'];  
			$partial_date = $this->request->query['partial_date']; 
			$quantity = $this->request->query['quantity']; 
			$required_qty = $this->request->query['required_qty']; 
			$sale_job_child_id = $this->request->query['sale_job_child_id']; 
			$sub_price = $this->request->query['sub_price']; 
			$total_price_rm = $this->request->query['total_price_rm']; 
			$planning_capture_id = $this->request->query['planning_capture_id']; 

			$is_buy = $this->request->query['is_buy']; // 1 = purchase

			$implode_job = implode(', ', $sale_job_child_id);

			$buffer_qty = array_sum($buffer);
			$each_qty = array_sum($quantity);

			$sub_price_sum = array_sum($sub_price);
			$total_price_rm_sum = array_sum($total_price_rm);

			$total_line_qty = $each_qty + $buffer_qty;
			$json['status'] = false;
			if($planning_qty == '' || $planning_qty == 0) {
				$json['msg'] = 'Purchase Qty cannot be empty or 0';
			} elseif($total_sum != $total_line_qty) {
				$json['msg'] = 'Sum of quantity (Y) not equal to Total (X) quantity. Please edit Buffer';
			} elseif($supplier == '') {
				$json['msg'] = 'Please add supplier on Price Book';
			} elseif($unit_price == 0 || $unit_price == '') {
				$json['msg'] = 'Invalid Unit/Price';
			} else {
				$this->loadModel('PlanningItem');
				$this->loadModel('PlanningItemJob');
				$plan_item = array(
					'planning_id' => $planning_id, 
					'inventory_item_id' => $item_id, 
					'quantity' => $required,
					'general_unit_id' => $general_unit_id, 
					'buffer' => $buffer_qty, 
					'total' => $total_sum,
					'planning_qty' => $planning_qty,  
					'planning_general_unit' => $planning_general_unit == '' ? 0 : $planning_general_unit, 
					'inventory_supplier_id' => $supplier, 
					'created' => $this->date,  
					'remark' => 'Auto generated MRP', 
					'status' => 0,
					'inventory_supplier_item_id' => $inventory_supplier_item_id, 
					'general_currency_id' => $general_currency_id, 
					'purchased_quantity' => 0, //conversation * plan qty
					'balance' => $planning_qty + $buffer_qty, 
					'is_sub_assembly' => $is_sub_assembly, 
					'on_hand' => $on_hand, 
					'mrn' => 0, 
					'job_list_json' => $implode_job, 
					'job_mrn_list_json' => 0, 
					'bom_qty' => $required, 
					'required_qty' => $required,
					'price' => $price,
					'unit_price' => $unit_price,
					'total_price' => $sub_price_sum,
					'total_price_rm' => $total_price_rm_sum
					);
				$this->PlanningItem->save($plan_item);
				$planning_item_id = $this->PlanningItem->getLastInsertId();

				for($i = 0; $i < count($required_qty); $i++) {
					if($is_buy[$i] == 1) {
						$this->PlanningItemJob->create();
						if($buffer[$i] == '') {
							$buff = 0;
						} else {
							$buff = $buffer[$i];
						}
						$total_each_qty = $buff + $quantity[$i];
						$total_price = $total_each_qty * $unit_price;
						$item_jobs = array(
							'sale_job_child_id' => $sale_job_child_id[$i], 
							'unit_price' => $unit_price, 
							'planning_item_id' => $planning_item_id, 
							'planning_id' => $planning_id, 
							'created' => $this->date, 
							'status' => 0, 
							'quantity' => $quantity[$i], 
							'total_price' => $sub_price[$i], 
							'total_price_rm' => $total_price_rm[$i], 
							'general_currency_id' => $general_currency_id, 
							'partial_date' => $partial_date[$i],
							'buffer' => $buff
							);	
						$this->PlanningItemJob->save($item_jobs);	
					} 
				}
				$this->loadModel('PlanningCaptureItem'); 
				 
				$update = $this->PlanningCaptureItem->query('UPDATE `planning_capture_items` SET `import_status` = "1" WHERE `planning_id` = "'.$planning_id.'" AND inventory_item_id = "'.$item_id.'" AND planning_capture_id = "'.$planning_capture_id.'"');

				if(!$update) {
					$json['msg'] = 'System failed to update status. Please contact administrator with Error code: PPC134';
				}
				
				$json['status'] = true;
				$json['msg'] = 'Success!. Data has been saved';
			}  
			echo json_encode($json);
		}
	}

	public function verification($plan_status = null) {

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['Planning.name LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['Planning.created >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['Planning.created <='] = trim($_GET['to']);
			}  
			 
			$this->request->data['Planning'] = $_GET;
		} 

		if($plan_status == null) {
			$status = 1;
		} else {
			$status = (int)$plan_status;
		}
		// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer,  
		$conditions['Planning.status'] = $status; 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array( 
			'conditions' => $conditions,
			'order' => 'Planning.id DESC', 
			'limit' => $record_per_page
			); 
		
		$this->set('plannings', $this->Paginator->paginate());
	}

/**
 * index method
 *
 * @return void
 */
	public function index($plan_status = null) {

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['Planning.name LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['Planning.created >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['Planning.created <='] = trim($_GET['to']);
			}  
			 
			$this->request->data['Planning'] = $_GET;
		} 

		if($plan_status == null) {
			$status = 0;
		} else {
			$status = (int)$plan_status;
		}
		// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer,  
		$conditions['Planning.status'] = $status; 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array( 
			'conditions' => $conditions,
			'order' => 'Planning.id DESC', 
			'limit' => $record_per_page
			); 
		
		$this->set('plannings', $this->Paginator->paginate());
	}

	public function job() {

		$this->loadModel('SaleJobChild'); 
		$record_per_page = Configure::read('Reading.nodes_per_page');
 
		$this->Paginator->settings = array('conditions' => array(
			'SaleJobChild.planning_status' => array(0)
			),   
			'order'=>'SaleJobChild.id DESC',
			'limit'=>$record_per_page 
			); 
		$this->set('plannings', $this->Paginator->paginate('SaleJobChild'));
	}

	public function planned() {

		$this->loadModel('SaleJobChild'); 
		$record_per_page = Configure::read('Reading.nodes_per_page');
 
		$this->Paginator->settings = array('conditions' => array(
			'SaleJobChild.planning_status' => array(1)
			),   
			'order'=>'SaleJobChild.id DESC',
			'limit'=>$record_per_page 
			); 
		$this->set('plannings', $this->Paginator->paginate('SaleJobChild'));
	}

	public function viewjob($id = null, $item_id = null) {
		$this->loadModel('SaleJob');
		if (!$this->SaleJob->exists($id)) {
			throw new NotFoundException(__('Invalid planning'));
		}
		$options = array('conditions' => array('SaleJob.id' => $id));

		$this->loadModel('SaleJobItem');
		$job_items = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_id' => $id
				)
			));

		$this->set('saleJob', $this->SaleJob->find('first', $options));
		$this->set('items', $job_items);

		$bom = array();
		foreach ($job_items as $item) {
			$bom[] = $item['SaleJobItem']['bom_id'];
		}

		// Combine all items
		$this->loadModel('BomChild');
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $bom
				)
			));
		$child_id = array();
		foreach ($childs as $child) {
			$child_id[] = $child['BomChild']['id'];
		}

		$this->loadModel('BomItem');
		$bomitems = $this->BomItem->find('all', array( 
			'conditions' => array(
				'BomItem.bom_child_id' => $child_id
				),
			'group' => 'BomItem.inventory_item_id' 
			));
   
		//echo '<pre>';
		//var_dump($bomitems);
		$items = array();
		foreach ($bomitems as $bomitem) {
			$items[] = array(
				'item_id' => $bomitem['BomItem']['id'],
				'total' => $this->sum_item_quantity($child_id, $bomitem['BomItem']['inventory_item_id']),
				'name' => $bomitem['InventoryItem']['name'],
				'unit' => $bomitem['GeneralUnit']['name'],
				'code' => $bomitem['InventoryItem']['code'],
				'supplier' => $this->get_default_supplier($bomitem['BomItem']['inventory_item_id']),
				'suppliers' => $this->get_all_supplier($bomitem['BomItem']['inventory_item_id']) 
				);
			
		}
		//var_dump($items);
		$this->set('bomitems', $items);
		
	} 

	private function get_item_qty_by_job_child($job_child_id, $item_id, $is_sub_assembly) {
		if($is_sub_assembly == 1) {
			$this->loadModel('ProjectBomChild');
	 		$items = $this->ProjectBomChild->find('all', array(
	 			'conditions' => array(
	 				'ProjectBomChild.sale_job_child_id' => $job_child_id,
	 				//'ProjectBom.status' => 0
	 				),
	 			'recursive' => -1
	 			));	
		} else {
			$this->loadModel('ProjectBomItem');
	 		$items = $this->ProjectBomItem->find('all', array(
	 			'conditions' => array(
	 				'ProjectBomItem.sale_job_child_id' => $job_child_id,
	 				//'ProjectBom.status' => 0
	 				),
	 			'recursive' => -1
	 			));	
		}
		
	}

	public function import() {
		$this->autoRender = false;

		$this->loadModel('SaleJob');
		
		if (isset($_GET['jobs'])) {
			$id = $_GET['jobs']; 
		} else {
			// Redirect to job
			$this->Session->setFlash(__('Please select Job.'), 'error');
            return $this->redirect(array('action' => 'job', $id));
		} 

		$this->loadModel('ProjectBom');
 		$project_bom = $this->ProjectBom->find('all', array(
 			'conditions' => array(
 				'ProjectBom.sale_job_child_id' => $id,
 				//'ProjectBom.status' => 0
 				),
 			'recursive' => -1
 			));

 		$job_child = array(); 
 		foreach ($project_bom as $project) {
 			$job_child[] = $project['ProjectBom']['id'];
 		} 

 		$progress = $this->update_sale_order_progress($id, 'Plan Order');

 		if($progress === false) {
 			$this->Session->setFlash(__('Unable to update progress, Please contact administrator. Error Code: E416.'), 'error');
            return $this->redirect(array('action' => 'job', $id));
 		}
		
		$options = array('conditions' => array('SaleJob.id' => $id));

		$this->loadModel('SaleJobItem');
		$job_items = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $id
				)
			));

		$this->set('saleJob', $this->SaleJob->find('first', $options));
		$this->set('items', $job_items);

		$this->loadModel('Planning');
		$count = $this->Planning->find('all', array(
			'reqursive' => -1
			));
		$number = count($count) + 1;
		$code = $this->generate_code('MRP', $number);

		$implode_job = implode(', ', $id);

		$this->request->data['Planning']['name'] = $code;
		$this->request->data['Planning']['user_id'] = $this->Session->read('Auth.User.id');
		$this->request->data['Planning']['created'] = $this->date; 
		$this->request->data['Planning']['jobs'] = $implode_job;   

		$this->Planning->save($this->request->data['Planning']);
		$planning_id = $this->Planning->getLastInsertId();
 		
 		$this->loadModel('PlanningCapture');
		$this->request->data['PlanningCapture']['code'] = date('YmdHis');
		$this->request->data['PlanningCapture']['created'] = date('YmdHis'); 
		$this->request->data['PlanningCapture']['user_id'] = $this->user_id;
		$this->request->data['PlanningCapture']['status'] = 0;
		$this->request->data['PlanningCapture']['jobs'] = $implode_job;
		$this->request->data['PlanningCapture']['planning_id'] = $planning_id;
		$this->PlanningCapture->save($this->request->data);

		$capture_id = $this->PlanningCapture->getLastInsertId();

		$this->loadModel('ProjectBomItem');
		$bomitems = $this->ProjectBomItem->find('all', array( 
			'conditions' => array(
				'ProjectBomItem.project_bom_id' => $job_child 
				),
			//'group' => 'ProjectBomItem.inventory_item_id',
			'order' => array('InventoryItem.inventory_item_category_id' => 'ASC'),
			'contain' => array(
				'InventoryItem'
				)
			));
    
		$items = array();
		foreach ($bomitems as $bomitem) { 
			$supplier = $this->get_default_supplier($bomitem['ProjectBomItem']['inventory_item_id']);
			$items[] = array(
				'inventory_item_id' => $bomitem['ProjectBomItem']['inventory_item_id'],
				'quantity' => $bomitem['ProjectBomItem']['quantity'],
				'no_of_order' => $bomitem['ProjectBomItem']['no_of_order'],
				'quantity_total' => $bomitem['ProjectBomItem']['quantity_total'], 
				'inventory_supplier_id' => !empty($supplier) ? $supplier['InventorySupplier']['id'] : 0,
				'inventory_supplier_item_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['id'] : 0,   
				'general_unit_id' => $bomitem['GeneralUnit']['id'], 
				'sale_job_id' => $bomitem['ProjectBomItem']['sale_job_id'], 
				'sale_job_child_id' => $bomitem['ProjectBomItem']['sale_job_child_id'], 
				'price_per_unit' => !empty($supplier) ? $supplier['InventorySupplierItem']['price_per_unit'] : 0, 
				'project_bom_id' => $bomitem['ProjectBomItem']['project_bom_id'], 
				'is_sub_assembly' => 0, 
				'type' => 'Plan Material',
				'ref' => $bomitem['ProjectBom']['code'] . ' > ' . $bomitem['ProjectBomChild']['name'],
				'planning_capture_id' =>  $capture_id,
				'planning_id' => $planning_id
				); 
		} 

		$this->loadModel('ProjectBomChild');
		$childs = $this->ProjectBomChild->find('all', array( 
			'conditions' => array(
				'ProjectBomChild.project_bom_id' => $job_child,
				'ProjectBomChild.item_type' => 'Purchase'
				),
			//'group' => 'ProjectBomChild.inventory_item_id',
			//'order' => array('InventoryItem.inventory_item_category_id' => 'ASC'),
			'contain' => array(
				'InventoryItem'
				)
			));
     
		foreach ($childs as $bomitem) {
			$supplier = $this->get_default_supplier($bomitem['ProjectBomChild']['inventory_item_id']);
			$items[] = array(
				'inventory_item_id' => $bomitem['ProjectBomChild']['inventory_item_id'],
				'quantity' => 1,
				'no_of_order' => 1,
				'quantity_total' => 1, 
				'inventory_supplier_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['inventory_supplier_id'] : 0,
				'inventory_supplier_item_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['id'] : 0, 
				'general_unit_id' => 48, 
				'sale_job_id' => $bomitem['ProjectBom']['sale_job_id'], 
				'sale_job_child_id' => $bomitem['ProjectBom']['sale_job_child_id'], 
				'price_per_unit' => !empty($supplier) ? $supplier['InventorySupplierItem']['price_per_unit'] : 0, 
				'project_bom_id' => $bomitem['ProjectBom']['id'],
				'is_sub_assembly' => 1,
				'type' => 'Plan Sub Assembly',
				'ref' => $bomitem['ProjectBom']['code'],
				'planning_capture_id' =>  $capture_id,
				'planning_id' => $planning_id
				); 
		}
 

		// MRN Area
		/*
		$this->loadModel('InventoryMaterialRequestItem'); 

		$mrns = $this->InventoryMaterialRequestItem->find('all', array( 
			'conditions' => array(
				'InventoryMaterialRequestItem.status' => 0,
				'InventoryMaterialRequest.status' => array(2, 3), // Verified , partial. 4 closed
				'InventoryMaterialRequestItem.issued_balance !=' => 0,
				'InventoryMaterialRequestItem.type' => array(0)
				) 
			));
     
		foreach ($mrns as $bomitem) {
			$supplier = $this->get_default_supplier($bomitem['InventoryMaterialRequestItem']['inventory_item_id']);
			$items[] = array(
				'inventory_item_id' => $bomitem['InventoryMaterialRequestItem']['inventory_item_id'],
				'quantity' => $bomitem['InventoryMaterialRequestItem']['issued_balance'],
				'no_of_order' => 0,
				'quantity_total' => $bomitem['InventoryMaterialRequestItem']['issued_balance'], 
				'inventory_supplier_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['inventory_supplier_id'] : 0,
				'inventory_supplier_item_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['id'] : 0,   
				'general_unit_id' => $bomitem['GeneralUnit']['id'], 
				'sale_job_id' => $bomitem['InventoryMaterialRequest']['sale_job_id'], 
				'sale_job_child_id' => $bomitem['InventoryMaterialRequest']['sale_job_child_id'], 
				'price_per_unit' => !empty($supplier) ? $supplier['InventorySupplierItem']['price_per_unit'] : 0, 
				'project_bom_id' => 0, 
				'is_sub_assembly' => 0,
				'type' => 'MRN',
				'ref' => $bomitem['InventoryMaterialRequest']['code'],
				'planning_capture_id' =>  $capture_id,
				'planning_id' => $planning_id
				); 
		}
		*/

		/*
		$this->loadModel('ProductionOrderItem');
		$po = $this->ProductionOrderItem->find('all', array( 
			'conditions' => array( 
				'ProductionOrder.status >' => 0
				),
			//'group' => 'ProjectBomChild.inventory_item_id',
			//'order' => array('InventoryItem.inventory_item_category_id' => 'ASC'), 
			));
     
		foreach ($po as $bomitem) {
			$supplier = $this->get_default_supplier($bomitem['ProductionOrderItem']['inventory_item_id']);
			$items[] = array(
				'inventory_item_id' => $bomitem['ProductionOrderItem']['inventory_item_id'], 
				'quantity' => $bomitem['ProductionOrderItem']['quantity_bal'],
				'no_of_order' => $bomitem['ProductionOrderItem']['no_of_order'],
				'quantity_total' => $bomitem['ProductionOrderItem']['quantity_bal'],
				'inventory_supplier_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['inventory_supplier_id'] : 0,
				'inventory_supplier_item_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['id'] : 0, 
				'general_unit_id' => $bomitem['ProductionOrderItem']['general_unit_id'], 
				'sale_job_id' => $bomitem['ProductionOrder']['sale_job_id'], 
				'sale_job_child_id' => $bomitem['ProductionOrder']['sale_job_childs_id'], 
				'price_per_unit' => !empty($supplier) ? $supplier['InventorySupplierItem']['price_per_unit'] : 0, 
				'project_bom_id' => 0, 
				'is_sub_assembly' => 0,
				'type' => 'Production Order Item',
				'ref' => $bomitem['ProductionOrder']['name'],
				'planning_capture_id' =>  $capture_id
				); 
		}

		$this->loadModel('ProductionOrderChild');
		$po_childs = $this->ProductionOrderChild->find('all', array( 
			'conditions' => array( 
				'ProductionOrderChild.quantity_bal >' => 0,
				'InventoryItem.type' => 'Purchase'
				),
			//'group' => 'ProjectBomChild.inventory_item_id',
			//'order' => array('InventoryItem.inventory_item_category_id' => 'ASC'), 
			));
     
		foreach ($po_childs as $bomitem) {
			$supplier = $this->get_default_supplier($bomitem['ProductionOrderChild']['inventory_item_id']);
			$items[] = array(
				'inventory_item_id' => $bomitem['ProductionOrderChild']['inventory_item_id'], 
				'quantity' => 1,
				'no_of_order' => $bomitem['ProductionOrderChild']['no_of_order'],
				'quantity_total' => $bomitem['ProductionOrderChild']['quantity_bal'],
				'inventory_supplier_id' => !empty($supplier) ? $supplier['ProductionOrderChild']['inventory_supplier_id'] : 0,
				'inventory_supplier_item_id' => !empty($supplier) ? $supplier['InventorySupplierItem']['id'] : 0, 
				'general_unit_id' => 48, 
				'sale_job_id' => $bomitem['ProductionOrderChild']['sale_job_id'], 
				'sale_job_child_id' => $bomitem['ProductionOrderChild']['sale_job_childs_id'], 
				'price_per_unit' => !empty($supplier) ? $supplier['InventorySupplierItem']['price_per_unit'] : 0, 
				'project_bom_id' => 0, 
				'is_sub_assembly' => 0,
				'type' => 'Production Order Sub Assembly',
				'ref' => $bomitem['ProductionOrder']['name'],
				'planning_capture_id' =>  $capture_id
				); 
		}
		*/
		
		// Update Job
		$this->loadModel('ProjectBom');
		$this->ProjectBom->updateAll(array(
			'ProjectBom.status' => "'1'"
			), array(
			'ProjectBom.id' => $job_child
			));

		$this->loadModel('SaleJobChild');

		$this->SaleJobChild->updateAll(array(
			'SaleJobChild.planning_status' => "'1'"
			), array(
			'SaleJobChild.id' => $id
			));
		
		$this->loadModel('PlanningCaptureItem');
		$this->PlanningCaptureItem->saveAll($items, array('deep' => true));

		$this->Session->setFlash(__('Data has been saved. Please review before make purchase.'), 'success');
        return $this->redirect(array('action' => 'importedreview', $capture_id)); 
 
	} 

	public function importedreview($id = null) {

		if ($this->request->is('post')) {   
			
 			$buffer = $this->request->data['PlanningItem']['buffer']; 
			$item = $this->request->data['PlanningItem']['item_id'];
			$quantity = $this->request->data['PlanningItem']['quantity'];
			$supplier = $this->request->data['PlanningItem']['supplier'];
			$unit = $this->request->data['PlanningItem']['unit']; 
			$inventory_supplier_item_id = $this->request->data['PlanningItem']['inventory_supplier_item_id']; 
			$is_sub_assembly = $this->request->data['PlanningItem']['is_sub_assembly'];

			$planning_general_unit = $this->request->data['PlanningItem']['planning_general_unit'];  

			$planning_qty = $this->request->data['PlanningItem']['planning_qty'];  

			$purchase = $this->request->data['PlanningItem']['purchase'];  
			$on_hand = $this->request->data['PlanningItem']['on_hand']; 

			$required_qty = $this->request->data['PlanningItem']['required_qty']; 
 
			$planning_id = $this->request->data['planning_id']; 

				
				// Update Status = 1: Purchase 

				$this->loadModel('PlanningItem'); 
				for($i = 0; $i < count($item); $i++) { 
					
					if($purchase[$i] == 1 && $planning_qty[$i] != 0 || $buffer[$i] != '') {  //  && $total_qty != 0
						$total_qty = $buffer[$i] + $planning_qty[$i];
						$this->PlanningItem->create();
						$planning = array(
							'planning_id' => $planning_id,
							'inventory_item_id' => $item[$i],
							'quantity' => $quantity[$i],
							'general_unit_id' => $unit[$i],
							'buffer' => $buffer[$i] == '' ? 0 : $buffer[$i],
							'planning_qty' => $planning_qty[$i],
							'total' => $total_qty,
							'planning_general_unit' => $planning_general_unit[$i],
							'inventory_supplier_id' => $supplier[$i],
							'general_currency_id' => $this->get_price_book_currency($supplier[$i], $item[$i]),
							'created' => $this->date,  
							'status' => 0,
							'inventory_supplier_item_id' => $inventory_supplier_item_id[$i],
							'is_sub_assembly' => $is_sub_assembly[$i],
							'on_hand' => $on_hand[$i],
							'mrn' => 0, 
							'job_list_json' => 0,
							'job_mrn_list_json' => 0,
							'required_qty' => $required_qty[$i],
							'bom_qty' => 0,
							); 	 
						$this->PlanningItem->save($planning);
					} 
				}	
				
				$status = $this->request->data['Planning']['status'];
				if($status == 1) {
					$this->send_approval($planning_id, $code);
				} 
				$this->Session->setFlash(__('Item has been exported successfully.'), 'success');
            	//return $this->redirect(array('action' => 'index'));
			 
		}

		if(isset($_GET['page'])) { 
			$page = (int)$_GET['page']; 
			if($page < 2) {
				$offset = 0;
				$back = 1;
			} else {
				$offset = ($page - 1) * 30;
				$back = $page - 1;
			}
			$paging = $page + 1;
			
		} else {
			$offset = 0; 
			$paging = 2;
			$back = 1;
		}
		
		$next = $paging;
		$this->set('id', $id);
		$this->set('next', $next);
		$this->set('back', $back);
		$this->loadModel('PlanningCaptureItem'); 
		$plan = $this->PlanningCaptureItem->find('first', array(
			'conditions' => array(
				'PlanningCaptureItem.planning_capture_id' => $id
				),
			'recursive' => -1
			));
		$this->set('plan', $plan);


		
		$items = $this->PlanningCaptureItem->query("
			SELECT PlanningCaptureItem.*, 
				SUM(PlanningCaptureItem.quantity_total) AS required,
				GeneralUnit.*,
				InventoryItem.code,
				InventoryItem.name,
				InventorySupplierItem.*,
				InventorySupplier.*,
				SaleJobChild.*,
				SupplierUnit.*,
				SupplierCurrency.* 
				FROM planning_capture_items AS PlanningCaptureItem 
				LEFT JOIN general_units AS GeneralUnit ON(GeneralUnit.id = PlanningCaptureItem.general_unit_id) 
				LEFT JOIN inventory_items AS InventoryItem ON(InventoryItem.id = PlanningCaptureItem.inventory_item_id) 
				LEFT OUTER JOIN inventory_supplier_items AS InventorySupplierItem ON(InventorySupplierItem.inventory_item_id = PlanningCaptureItem.inventory_item_id AND InventorySupplierItem.is_default = 1) 
				LEFT OUTER JOIN inventory_suppliers AS InventorySupplier ON(InventorySupplier.id = InventorySupplierItem.inventory_supplier_id) 
				LEFT OUTER JOIN sale_job_childs AS SaleJobChild ON(SaleJobChild.id = PlanningCaptureItem.sale_job_child_id)
				LEFT OUTER JOIN general_units AS SupplierUnit ON(SupplierUnit.id = InventorySupplierItem.conversion_unit_id)
				LEFT OUTER JOIN general_currencies AS SupplierCurrency ON(SupplierCurrency.id = InventorySupplierItem.general_currency_id)
				WHERE PlanningCaptureItem.planning_capture_id IN($id)
				AND PlanningCaptureItem.import_status IN(0)
				GROUP BY PlanningCaptureItem.inventory_item_id
				ORDER BY InventoryItem.code ASC 
				LIMIT 30 OFFSET ".$offset);
		//GROUP BY PlanningCaptureItem.inventory_item_id
		$data = array();
		foreach ($items as $item) {
			$production = $this->sum_production_order_item($item['PlanningCaptureItem']['inventory_item_id']);
			$data[] = array(
				'id' => $item['PlanningCaptureItem']['id'], 
				'planning_capture_id' => $item['PlanningCaptureItem']['planning_capture_id'], 
				'is_sub_assembly' => $item['PlanningCaptureItem']['is_sub_assembly'],
				'inventory_item_id' => $item['PlanningCaptureItem']['inventory_item_id'],
				'code' => $item['InventoryItem']['code'],
				'name' => $item['InventoryItem']['name'],
				'qty' => $item['PlanningCaptureItem']['quantity_total'],
				'ref' => $item['PlanningCaptureItem']['ref'],
				'unit_price' => $item['InventorySupplierItem']['price'],
				'price_per_unit' => $item['InventorySupplierItem']['price_per_unit'],
				'unit' => $item['GeneralUnit']['name'],
				'general_unit_id' => $item['GeneralUnit']['id'], 
				'supplier' => $item['InventorySupplier'],
				'pricebook' => $item['InventorySupplierItem'],
				'job' => $item['SaleJobChild'], 
				'required' => $item[0]['required'],
				'onhold' => $this->sum_rejected($item['PlanningCaptureItem']['inventory_item_id']),
				'ordered' => $this->sum_ordered($item['PlanningCaptureItem']['inventory_item_id']),
				'onhand' => $this->sum_stock($item['PlanningCaptureItem']['inventory_item_id']), 
				'production' => !empty($production) ? $production : 0,
				'similars' => $this->ajaxfindsimilar($item['PlanningCaptureItem']['inventory_item_id'], $id),
				'supplier_unit' => $item['SupplierUnit'],
				'suppliers' => $this->get_other_supplier($item['PlanningCaptureItem']['inventory_item_id'], $item['InventorySupplier']['id']),
				'SupplierCurrency' => $item['SupplierCurrency'],
				);
		}
		$this->set('items', $data);

		$this->loadModel('PlanningCapture');
		$planning = $this->PlanningCapture->find('first', array(
			'conditions' => array(
				'PlanningCapture.id' => $id
				),
			'recursive' => -1
			));
		$this->set('planning', $planning);
	} 

	private function price_book($item_id) {

	}

	private function ajaxfindsimilar($item_id, $planning_capture_id) { 
		$this->loadModel('PlanningCaptureItem'); 
		$items = $this->PlanningCaptureItem->query("
			SELECT PlanningCaptureItem.*,
				SUM(PlanningCaptureItem.quantity_total) AS qty,
				SaleJobChild.*,
				InventoryItem.code,
				InventoryItem.name,
				InventorySupplierItem.*,
				InventorySupplier.*
				FROM planning_capture_items AS PlanningCaptureItem  
				LEFT JOIN inventory_items AS InventoryItem ON(InventoryItem.id = PlanningCaptureItem.inventory_item_id) 
				LEFT OUTER JOIN sale_job_childs AS SaleJobChild ON(SaleJobChild.id = PlanningCaptureItem.sale_job_child_id)
				LEFT OUTER JOIN inventory_supplier_items AS InventorySupplierItem ON(InventorySupplierItem.id = PlanningCaptureItem.inventory_supplier_item_id)
				LEFT OUTER JOIN inventory_suppliers AS InventorySupplier ON(InventorySupplier.id = PlanningCaptureItem.inventory_supplier_id)
				
				WHERE PlanningCaptureItem.inventory_item_id IN($item_id)
				AND PlanningCaptureItem.planning_capture_id IN($planning_capture_id)
				GROUP BY SaleJobChild.id
				LIMIT 50 
			");
		$data = array();
		foreach ($items as $item) {
			$data[] = array(
				'PlanningCaptureItem' => $item['PlanningCaptureItem'],
				'Quantity' => $item[0]['qty'],
				'SaleJobChild' => $item['SaleJobChild'],
				'InventoryItem' => $item['InventoryItem'],
				'InventorySupplierItem' => $item['InventorySupplierItem'], 
				);
		}
		return $data;  
	}

	public function review() {

		$this->loadModel('SaleJob');
		
		if (isset($_GET['jobs'])) {
			$id = $_GET['jobs'];
		} else {
			// Redirect to job
			$this->Session->setFlash(__('Please select Job.'), 'error');
            return $this->redirect(array('action' => 'job', $id));
		} 

		$this->loadModel('ProjectBom');
 		$project_bom = $this->ProjectBom->find('all', array(
 			'conditions' => array(
 				'ProjectBom.sale_job_child_id' => $id,
 				//'ProjectBom.status' => 0
 				),
 			'recursive' => -1
 			));

 		$job_child = array();

 		foreach ($project_bom as $project) {
 			$job_child[] = $project['ProjectBom']['id'];
 		}

 		$implode_job = implode(', ', $id);

		if ($this->request->is('post')) {  
			$count = $this->Planning->find('all', array(
				'reqursive' => -1
				));
			$number = count($count) + 1;
			$code = $this->generate_code('PP', $number);
 
			$this->request->data['Planning']['name'] = $code;
			$this->request->data['Planning']['user_id'] = $this->Session->read('Auth.User.id');
			$this->request->data['Planning']['created'] = $this->date;
			$this->request->data['Planning']['modified'] = $this->date;

			

			$this->request->data['Planning']['jobs'] = $implode_job;  
			
 			$buffer = $this->request->data['PlanningItem']['buffer']; 
			$item = $this->request->data['PlanningItem']['item_id'];
			$quantity = $this->request->data['PlanningItem']['quantity'];
			$supplier = $this->request->data['PlanningItem']['supplier'];
			$unit = $this->request->data['PlanningItem']['unit']; 
			$inventory_supplier_item_id = $this->request->data['PlanningItem']['inventory_supplier_item_id']; 
			$is_sub_assembly = $this->request->data['PlanningItem']['is_sub_assembly'];

			$planning_general_unit = $this->request->data['PlanningItem']['planning_general_unit'];  

			$planning_qty = $this->request->data['PlanningItem']['planning_qty'];  

			$purchase = $this->request->data['PlanningItem']['purchase'];  
			$on_hand = $this->request->data['PlanningItem']['on_hand'];
			$mrn = $this->request->data['PlanningItem']['mrn_qty'];

			$required_qty = $this->request->data['PlanningItem']['required_qty'];
			$bom_qty = $this->request->data['PlanningItem']['bom_qty'];

			$job_list_json = $this->request->data['PlanningItem']['job_list_json'];

			if($this->Planning->save($this->request->data['Planning'])) { 
				$planning_id = $this->Planning->getLastInsertId();
				// Update Status = 1: Purchase
				$updatePurchase = $this->ProjectBom->updateAll(array(
					'ProjectBom.status' => "'1'"
					), array(
					'ProjectBom.id' => $job_child
					));

				$this->loadModel('SaleJobChild');

				$station = $this->SaleJobChild->updateAll(array(
					'SaleJobChild.planning_status' => "'1'"
					), array(
					'SaleJobChild.id' => $id
					));
				if(!$updatePurchase || !$station) {
					$this->Planning->deleteAll(array(
						'Planning.id' => $planning_id
					), false);

					$this->Session->setFlash(__('System error, Please contact Administrator. Code: PP204.'), 'error');
            		return $this->redirect(array('action' => 'job'));
				}
				

				$this->loadModel('PlanningItem'); 
				for($i = 0; $i < count($item); $i++) { 
					
					if($purchase[$i] == 1 && $planning_qty[$i] != 0 || $buffer[$i] != '') {  //  && $total_qty != 0
						$total_qty = $buffer[$i] + $planning_qty[$i];
						$this->PlanningItem->create();
						$planning = array(
							'planning_id' => $planning_id,
							'inventory_item_id' => $item[$i],
							'quantity' => $quantity[$i],
							'general_unit_id' => $unit[$i],
							'buffer' => $buffer[$i] == '' ? 0 : $buffer[$i],
							'planning_qty' => $planning_qty[$i],
							'total' => $total_qty,
							'planning_general_unit' => $planning_general_unit[$i],
							'inventory_supplier_id' => $supplier[$i],
							'general_currency_id' => $this->get_price_book_currency($supplier[$i], $item[$i]),
							'created' => $this->date,  
							'status' => 0,
							'inventory_supplier_item_id' => $inventory_supplier_item_id[$i],
							'is_sub_assembly' => $is_sub_assembly[$i],
							'on_hand' => $on_hand[$i],
							'mrn' => $mrn[$i], 
							'job_list_json' => $job_list_json[$i],
							'job_mrn_list_json' => 0,
							'required_qty' => $required_qty[$i],
							'bom_qty' => $bom_qty[$i],
							); 	 
						$this->PlanningItem->save($planning);
					} 
				}	
				
				$status = $this->request->data['Planning']['status'];
				if($status == 1) {
					$this->send_approval($planning_id, $code);
				} 
				$this->Session->setFlash(__('Item has been exported successfully.'), 'success');
            	//return $this->redirect(array('action' => 'index'));
			} else {
				//debug($this->Planning->validationErrors);
				$this->Session->setFlash(__('Cant save.'), 'error');
			}
			
		}
		
		$options = array('conditions' => array('SaleJob.id' => $id));

		$this->loadModel('SaleJobItem');
		$job_items = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $id
				)
			));

		$this->set('saleJob', $this->SaleJob->find('first', $options));
		$this->set('items', $job_items);
 		
 		
		$this->loadModel('ProjectBomItem');
		$bomitems = $this->ProjectBomItem->find('all', array( 
			'conditions' => array(
				'ProjectBomItem.project_bom_id' => $job_child,
				'ProjectBomChild.item_type' => NULL
				),
			'group' => 'ProjectBomItem.inventory_item_id',
			'order' => array('InventoryItem.code' => 'ASC') 
			));
    
		$items = array();
		foreach ($bomitems as $bomitem) {
			$sum_mrn = $this->sum_mrn($bomitem['InventoryItem']['id']);
			$items[] = array(
				'items' => $this->get_all_bom_items($job_child, $bomitem['InventoryItem']['id']),
				'productionitems' => $this->get_all_item_productions($bomitem['InventoryItem']['id']),
				'mrnitems' => $this->get_all_item_mrn($bomitem['InventoryItem']['id']),
				'item_id' => $bomitem['InventoryItem']['id'],
				'total' => $this->sum_item_quantity($job_child, $bomitem['ProjectBomItem']['inventory_item_id']), 
				'supplier' => $this->get_default_supplier($bomitem['ProjectBomItem']['inventory_item_id']),
				'suppliers' => array(),
				'name' => $bomitem['InventoryItem']['name'],
				'unit' => $bomitem['GeneralUnit']['id'],
				'unit_name' => $bomitem['GeneralUnit']['name'],
				'code' => $bomitem['InventoryItem']['code'],
				'in_stock' => $this->sum_stock($bomitem['InventoryItem']['id']), 
				'reserved' => $bomitem['InventoryItem']['quantity_reserved'],
				'waiting_qc' => $this->sum_waiting_qc($bomitem['InventoryItem']['id']),
				'mrn_qty' => $sum_mrn,
				'mrn' => $sum_mrn + $this->sum_production_order_item($bomitem['InventoryItem']['id']),
				'rejected' => $this->sum_rejected($bomitem['InventoryItem']['id']),
				'ordered' => $this->sum_ordered($bomitem['InventoryItem']['id']),
				'balance' => $this->sum_stock($bomitem['InventoryItem']['id']),
				'is_sub_assembly' => 0 
				); 
		} 
		// get_mrn_job($item_id) 
		// get_production_order_item_job($item_id)

		$this->loadModel('ProjectBomChild');
		$childs = $this->ProjectBomChild->find('all', array( 
			'conditions' => array(
				'ProjectBomChild.project_bom_id' => $job_child,
				'ProjectBomChild.item_type' => 'Purchase'
				),
			'group' => 'ProjectBomChild.inventory_item_id',
			'order' => array('InventoryItem.code' => 'ASC') 
			));
     
		foreach ($childs as $bomitem) {
			$sum_mrn = $this->sum_mrn($bomitem['InventoryItem']['id']);
			$items[] = array(
				'items' => $this->get_all_bom_subitems($job_child, $bomitem['InventoryItem']['id']),
				'productionitems' => $this->get_all_item_production_childs($bomitem['InventoryItem']['id']),
				'mrnitems' => $this->get_all_item_mrn($bomitem['InventoryItem']['id']),
				'item_id' => $bomitem['InventoryItem']['id'],
				'total' => $this->sum_sub_assembly_quantity($job_child, $bomitem['ProjectBomChild']['inventory_item_id']), 
				'supplier' => $this->get_default_supplier($bomitem['ProjectBomChild']['inventory_item_id']),
				'suppliers' => $this->get_other_supplier($bomitem['ProjectBomChild']['inventory_item_id']),
				'name' => $bomitem['InventoryItem']['name'],
				'unit' => 48,
				'unit_name' => 'unt',
				'code' => $bomitem['InventoryItem']['code'],
				'in_stock' => $this->sum_stock($bomitem['InventoryItem']['id']), 
				'reserved' => $bomitem['InventoryItem']['quantity_reserved'],
				'waiting_qc' => $this->sum_waiting_qc($bomitem['InventoryItem']['id']),
				'mrn_qty' => $sum_mrn,
				'mrn' => $sum_mrn + $this->sum_production_order_child($bomitem['InventoryItem']['id']),
				'rejected' => $this->sum_rejected($bomitem['InventoryItem']['id']),
				'ordered' => $this->sum_ordered($bomitem['InventoryItem']['id']),
				'balance' => $this->sum_stock($bomitem['InventoryItem']['id']),
				'is_sub_assembly' => 1 
				); 
		}

		//var_dump($items);
		$this->set('bomitems', $items);

		$this->set('supplierJson', $this->get_supplier_json());

		$this->set('unitJson', $this->general_unit_json());
	} 

	private function get_all_item_production_childs($item_id) {
		$this->loadModel('ProductionOrderChild');
		$items = $this->ProductionOrderChild->query("
			SELECT ProductionOrderChild.*,
				SaleJobChild.*,
				ProductionOrder.*
				FROM production_order_childs AS ProductionOrderChild 
				LEFT JOIN production_orders AS ProductionOrder ON(ProductionOrder.id = ProductionOrderChild.production_order_id) 
				LEFT JOIN sale_job_childs AS SaleJobChild ON(SaleJobChild.id = ProductionOrder.sale_job_childs_id) 
				WHERE ProductionOrderChild.inventory_item_id = $item_id
				AND ProductionOrder.status >= 1
			");
		 
		return $items; 
	}

	private function get_all_item_productions($item_id) {
		$this->loadModel('ProductionOrderItem'); 
		$items = $this->ProductionOrderItem->query("
			SELECT ProductionOrderItem.*,
				SaleJobChild.* ,
				GeneralUnit.*,
				ProductionOrder.*
				FROM production_order_items AS ProductionOrderItem 
				LEFT JOIN general_units AS GeneralUnit ON(GeneralUnit.id = ProductionOrderItem.general_unit_id)
				LEFT JOIN production_orders AS ProductionOrder ON(ProductionOrder.id = ProductionOrderItem.production_order_id) 
				LEFT JOIN sale_job_childs AS SaleJobChild ON(SaleJobChild.id = ProductionOrder.sale_job_childs_id)
				WHERE ProductionOrderItem.inventory_item_id = $item_id
				AND ProductionOrder.status >= 1
				AND ProductionOrderItem.quantity_bal > 0
			");
		return $items; 
	}

	private function get_all_item_mrn($item_id) {
		$this->loadModel('InventoryMaterialRequestItem'); 
		$items = $this->InventoryMaterialRequestItem->query("
			SELECT InventoryMaterialRequestItem.*,
				SaleJobChild.*,
				GeneralUnit.*,
				InventoryMaterialRequest.*
				FROM inventory_material_request_items AS InventoryMaterialRequestItem
				LEFT JOIN inventory_material_requests AS InventoryMaterialRequest ON(InventoryMaterialRequest.id = InventoryMaterialRequestItem.inventory_material_request_id)
				LEFT JOIN sale_job_childs AS SaleJobChild ON(SaleJobChild.id = InventoryMaterialRequest.sale_job_child_id)
				LEFT JOIN general_units AS GeneralUnit ON(GeneralUnit.id = InventoryMaterialRequestItem.general_unit_id) 
				WHERE InventoryMaterialRequestItem.inventory_item_id = $item_id 
				AND InventoryMaterialRequestItem.issued_balance > 0
			");
		return $items; 
	}

	private function get_all_bom_items($job_child, $item_id) {
		$this->loadModel('ProjectBomItem');
		$bomitems = $this->ProjectBomItem->find('all', array( 
			'conditions' => array(
				'ProjectBomItem.project_bom_id' => $job_child,
				'ProjectBomItem.inventory_item_id' => $item_id,
				'ProjectBomChild.item_type' => NULL
				), 
			'order' => array('InventoryItem.code' => 'ASC'),
			'contain' => array(
				'InventoryItem'
				)
			));
		return $bomitems;
	}

	private function get_all_bom_subitems($job_child, $item_id) {
		$this->loadModel('ProjectBomChild');
		$childs = $this->ProjectBomChild->find('all', array( 
			'conditions' => array(
				'ProjectBomChild.project_bom_id' => $job_child,
				'ProjectBomChild.inventory_item_id' => $item_id,
				'ProjectBomChild.item_type' => 'Purchase'
				), 
			'order' => array('InventoryItem.code' => 'ASC'),
			'contain' => array(
				'InventoryItem'
				)
			));
		return $childs;
	}

	/* public function review() {

		$this->loadModel('SaleJob');
		
		if (isset($_GET['jobs'])) {
			$id = $_GET['jobs'];
		} else {
			// Redirect to job
			$this->Session->setFlash(__('Please select Job.'), 'error');
            return $this->redirect(array('action' => 'job', $id));
		} 

		$this->loadModel('ProjectBom');
 		$project_bom = $this->ProjectBom->find('all', array(
 			'conditions' => array(
 				'ProjectBom.sale_job_child_id' => $id,
 				//'ProjectBom.status' => 0
 				),
 			'recursive' => -1
 			));

 		$job_child = array();

 		foreach ($project_bom as $project) {
 			$job_child[] = $project['ProjectBom']['id'];
 		}

 		$implode_job = implode(', ', $id);

		if ($this->request->is('post')) {  
			$count = $this->Planning->find('all', array(
				'reqursive' => -1
				));
			$number = count($count) + 1;
			$code = $this->generate_code('PP', $number);
 
			$this->request->data['Planning']['name'] = $code;
			$this->request->data['Planning']['user_id'] = $this->Session->read('Auth.User.id');
			$this->request->data['Planning']['created'] = $this->date;
			$this->request->data['Planning']['modified'] = $this->date;

			

			$this->request->data['Planning']['jobs'] = $implode_job;  
			
 			$buffer = $this->request->data['PlanningItem']['buffer']; 
			$item = $this->request->data['PlanningItem']['item_id'];
			$quantity = $this->request->data['PlanningItem']['quantity'];
			$supplier = $this->request->data['PlanningItem']['supplier'];
			$unit = $this->request->data['PlanningItem']['unit']; 
			$inventory_supplier_item_id = $this->request->data['PlanningItem']['inventory_supplier_item_id']; 
			$is_sub_assembly = $this->request->data['PlanningItem']['is_sub_assembly'];

			$planning_general_unit = $this->request->data['PlanningItem']['planning_general_unit'];  

			$planning_qty = $this->request->data['PlanningItem']['planning_qty'];  

			$purchase = $this->request->data['PlanningItem']['purchase'];  
			$on_hand = $this->request->data['PlanningItem']['on_hand'];
			$mrn = $this->request->data['PlanningItem']['mrn_qty'];

			$required_qty = $this->request->data['PlanningItem']['required_qty'];
			$bom_qty = $this->request->data['PlanningItem']['bom_qty'];

			$job_list_json = $this->request->data['PlanningItem']['job_list_json'];

			if($this->Planning->save($this->request->data['Planning'])) { 
				$planning_id = $this->Planning->getLastInsertId();
				// Update Status = 1: Purchase
				$updatePurchase = $this->ProjectBom->updateAll(array(
					'ProjectBom.status' => "'1'"
					), array(
					'ProjectBom.id' => $job_child
					));

				$this->loadModel('SaleJobChild');

				$station = $this->SaleJobChild->updateAll(array(
					'SaleJobChild.planning_status' => "'1'"
					), array(
					'SaleJobChild.id' => $id
					));
				if(!$updatePurchase || !$station) {
					$this->Planning->deleteAll(array(
						'Planning.id' => $planning_id
					), false);

					$this->Session->setFlash(__('System error, Please contact Administrator. Code: PP204.'), 'error');
            		return $this->redirect(array('action' => 'job'));
				}
				

				$this->loadModel('PlanningItem'); 
				for($i = 0; $i < count($item); $i++) { 
					
					if($purchase[$i] == 1 && $planning_qty[$i] != 0 || $buffer[$i] != '') {  //  && $total_qty != 0
						$total_qty = $buffer[$i] + $planning_qty[$i];
						$this->PlanningItem->create();
						$planning = array(
							'planning_id' => $planning_id,
							'inventory_item_id' => $item[$i],
							'quantity' => $quantity[$i],
							'general_unit_id' => $unit[$i],
							'buffer' => $buffer[$i] == '' ? 0 : $buffer[$i],
							'planning_qty' => $planning_qty[$i],
							'total' => $total_qty,
							'planning_general_unit' => $planning_general_unit[$i],
							'inventory_supplier_id' => $supplier[$i],
							'general_currency_id' => $this->get_price_book_currency($supplier[$i], $item[$i]),
							'created' => $this->date,  
							'status' => 0,
							'inventory_supplier_item_id' => $inventory_supplier_item_id[$i],
							'is_sub_assembly' => $is_sub_assembly[$i],
							'on_hand' => $on_hand[$i],
							'mrn' => $mrn[$i], 
							'job_list_json' => $job_list_json[$i],
							'job_mrn_list_json' => 0,
							'required_qty' => $required_qty[$i],
							'bom_qty' => $bom_qty[$i],
							); 	 
						$this->PlanningItem->save($planning);
					} 
				}	
				
				$status = $this->request->data['Planning']['status'];
				if($status == 1) {
					$this->send_approval($planning_id, $code);
				} 
				$this->Session->setFlash(__('Item has been exported successfully.'), 'success');
            	//return $this->redirect(array('action' => 'index'));
			} else {
				//debug($this->Planning->validationErrors);
				$this->Session->setFlash(__('Cant save.'), 'error');
			}
			
		}
		
		$options = array('conditions' => array('SaleJob.id' => $id));

		$this->loadModel('SaleJobItem');
		$job_items = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $id
				)
			));

		$this->set('saleJob', $this->SaleJob->find('first', $options));
		$this->set('items', $job_items);
 		
 		
		$this->loadModel('ProjectBomItem');
		$bomitems = $this->ProjectBomItem->find('all', array( 
			'conditions' => array(
				'ProjectBomItem.project_bom_id' => $job_child,
				'ProjectBomChild.item_type' => NULL
				),
			//'group' => 'ProjectBomItem.inventory_item_id',
			'order' => array('InventoryItem.code' => 'ASC'),
			'contain' => array(
				'InventoryItem'
				)
			));
    
		$items = array();
		foreach ($bomitems as $bomitem) {
			$sum_mrn = $this->sum_mrn($bomitem['InventoryItem']['id']);
			$items[] = array(
				'item_id' => $bomitem['InventoryItem']['id'],
				'total' => $this->sum_item_quantity($job_child, $bomitem['ProjectBomItem']['inventory_item_id']), 
				'supplier' => $this->get_default_supplier($bomitem['ProjectBomItem']['inventory_item_id']),
				'suppliers' => array(),
				'name' => $bomitem['InventoryItem']['name'],
				'unit' => $bomitem['GeneralUnit']['id'],
				'unit_name' => $bomitem['GeneralUnit']['name'],
				'code' => $bomitem['InventoryItem']['code'],
				'in_stock' => $this->sum_stock($bomitem['InventoryItem']['id']), 
				'reserved' => $bomitem['InventoryItem']['quantity_reserved'],
				'waiting_qc' => $this->sum_waiting_qc($bomitem['InventoryItem']['id']),
				'mrn_qty' => $sum_mrn,
				'mrn' => $sum_mrn + $this->sum_production_order_item($bomitem['InventoryItem']['id']),
				'rejected' => $this->sum_rejected($bomitem['InventoryItem']['id']),
				'ordered' => $this->sum_ordered($bomitem['InventoryItem']['id']),
				'balance' => $this->sum_stock($bomitem['InventoryItem']['id']),
				'is_sub_assembly' => 0,
				'jobs' => $implode_job . $this->get_mrn_job($bomitem['InventoryItem']['id']) . $this->get_production_order_item_job($bomitem['InventoryItem']['id'])
				); 
		} 
		// get_mrn_job($item_id) 
		// get_production_order_item_job($item_id)

		$this->loadModel('ProjectBomChild');
		$childs = $this->ProjectBomChild->find('all', array( 
			'conditions' => array(
				'ProjectBomChild.project_bom_id' => $job_child,
				'ProjectBomChild.item_type' => 'Purchase'
				),
			//'group' => 'ProjectBomChild.inventory_item_id',
			'order' => array('InventoryItem.code' => 'ASC'),
			'contain' => array(
				'InventoryItem'
				)
			));
     
		foreach ($childs as $bomitem) {
			$sum_mrn = $this->sum_mrn($bomitem['InventoryItem']['id']);
			$items[] = array(
				'item_id' => $bomitem['InventoryItem']['id'],
				'total' => $this->sum_sub_assembly_quantity($job_child, $bomitem['ProjectBomChild']['inventory_item_id']), 
				'supplier' => $this->get_default_supplier($bomitem['ProjectBomChild']['inventory_item_id']),
				'suppliers' => $this->get_other_supplier($bomitem['ProjectBomChild']['inventory_item_id']),
				'name' => $bomitem['InventoryItem']['name'],
				'unit' => 48,
				'unit_name' => 'unt',
				'code' => $bomitem['InventoryItem']['code'],
				'in_stock' => $this->sum_stock($bomitem['InventoryItem']['id']), 
				'reserved' => $bomitem['InventoryItem']['quantity_reserved'],
				'waiting_qc' => $this->sum_waiting_qc($bomitem['InventoryItem']['id']),
				'mrn_qty' => $sum_mrn,
				'mrn' => $sum_mrn + $this->sum_production_order_child($bomitem['InventoryItem']['id']),
				'rejected' => $this->sum_rejected($bomitem['InventoryItem']['id']),
				'ordered' => $this->sum_ordered($bomitem['InventoryItem']['id']),
				'balance' => $this->sum_stock($bomitem['InventoryItem']['id']) - $bomitem['InventoryItem']['quantity_reserved'],
				'is_sub_assembly' => 1,
				'jobs' => $implode_job . $this->get_mrn_job($bomitem['InventoryItem']['id']) . $this->get_production_order_item_job($bomitem['InventoryItem']['id'])
				); 
		}

		//var_dump($items);
		$this->set('bomitems', $items);

		$this->set('supplierJson', $this->get_supplier_json());

		$this->set('unitJson', $this->general_unit_json());
	}    */

	private function get_price_book_currency($supplier_id, $item_id) {
		$this->loadModel('InventorySupplierItem');
		$currency = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_supplier_id' => $supplier_id,
				'InventorySupplierItem.inventory_item_id' => $item_id
				),
			'recursive' => -1
			));
		if($currency) {
			return $currency['InventorySupplierItem']['general_currency_id'];
		} else {
			return 0;
		}
	}

	private function general_unit_json() {
		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('all', array(
			'recursive' => -1,
			'order' => array(
				'GeneralUnit.name' => 'ASC'
				)
			));
		$json = array();
		foreach ($units as $unit) {
			$json[] = array(
				'id' => $unit['GeneralUnit']['id'],
				'name' => $unit['GeneralUnit']['name']
				);
		}
		return $json;
	}

	private function send_approval($id, $plan_order_no) {
		$this->loadModel('User');
		$conditions['OR']['User.group_id'] = 6;
		$conditions['OR']['User.group_id'] = 12;
		$conditions['OR']['User.role'] = 'HOD';
		$conditions['OR']['User.role'] = 'HOS';
		$users = $this->User->find('all', array(
			'conditions' => array(
				'User.group_id' => 12,
				'User.role' => 'HOD'
				),
			'reqursive' => -1
			));

		if($users) {
			foreach ($users as $user) {
				$data['to'] = $user['User']['email'];
				$data['template'] = 'plan_order_verification';
				$data['subject'] = 'Plan Order Verification';
				$data['content'] = array(
					'username' => $user['User']['username'], 
					'link' => 'plannings/view/' . $id,
					'plan_order_no' => $plan_order_no
					);
				$this->send_email($data);
			} 
		} 
		return true;
	}

	private function sum_production_order_child($item_id) {
		$this->loadModel('ProductionOrderChild');
		$items = $this->ProductionOrderChild->find('all', array(
			'fields' => array(
				'SUM(ProductionOrderChild.quantity_bal) AS quantity'
				),
			'conditions' => array(
				'ProductionOrderChild.inventory_item_id' => $item_id,
				'ProductionOrder.status !=' => 0
				),
			'reqursive' => -1
			));
		 
		foreach ($items as $item) {
			return $item[0]['quantity'];
		} 
	}

	private function sum_production_order_item($item_id) {
		$this->loadModel('ProductionOrderItem');
		$items = $this->ProductionOrderItem->find('all', array(
			'fields' => array(
				'SUM(ProductionOrderItem.quantity_bal) AS quantity'
				),
			'conditions' => array(
				'ProductionOrderItem.inventory_item_id' => $item_id,
				'ProductionOrder.status !=' => 0
				) 
			));
		if(!empty($items)) {
			foreach ($items as $item) {
				return $item[0]['quantity'];
			} 	
		} else {
			return 0;
		}
	}

	// get_mrn_job($item_id) 
	// get_production_order_item_job($item_id)
	private function get_production_order_item_job($item_id) {
		$this->loadModel('ProductionOrderItem');
		$items = $this->ProductionOrderItem->find('all', array( 
			'conditions' => array(
				'ProductionOrderItem.inventory_item_id' => $item_id,
				'ProductionOrder.status !=' => 0,
				'ProductionOrderItem.quantity_bal !=' => 0
				),
			'group' => 'ProductionOrder.sale_job_childs_id',
			'reqursive' => 0
			));
		$arr = ''; 	
		foreach ($items as $item) {
			$arr .= ', ' . $item['ProductionOrder']['sale_job_childs_id'];
		} 
		return $arr;
	}

	private function get_mrn_job($item_id) {
		$this->loadModel('InventoryMaterialRequestItem');
		$items = $this->InventoryMaterialRequestItem->find('all', array( 
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_item_id' => $item_id,
				'InventoryMaterialRequestItem.type' => array(0, 1)
				),
			'reqursive' => -1,
			'group' => 'InventoryMaterialRequest.sale_job_child_id',
			));
		$arr = ''; 	
		foreach ($items as $item) {
			$arr .= ', ' . $item['InventoryMaterialRequest']['sale_job_child_id'];
		} 
		return $arr;
	}

	private function sum_mrn($item_id) {
		$this->loadModel('InventoryMaterialRequestItem');
		$items = $this->InventoryMaterialRequestItem->find('all', array(
			'fields' => array(
				'SUM(InventoryMaterialRequestItem.issued_balance) AS quantity'
				),
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_item_id' => $item_id,
				'InventoryMaterialRequestItem.type' => array(0, 1)
				),
			'reqursive' => -1
			));
		if($items) {
			foreach ($items as $item) {
				return $item[0]['quantity'];
			}
		} 
		return 0; 
	}

	// Approved Purchase Requisition
	private function sum_ordered($item_id) {
		$this->loadModel('InventoryDeliveryOrderItem');
		$items = $this->InventoryDeliveryOrderItem->find('all', array(
			'fields' => array(
				'SUM(InventoryDeliveryOrderItem.quantity_remaining) AS quantity'
				),
			'conditions' => array(
				'InventoryDeliveryOrderItem.inventory_item_id' => $item_id,
				'InventoryDeliveryOrderItem.quantity_remaining >' => 0,
				),
			'reqursive' => -1
			));
		if($items) {
			foreach ($items as $item) {
				return $item[0]['quantity'];
			}
		}
		return 0;
	}

	// QC Status = 0 = waiting, 1 = completed, 2 = rework, 3 rejected 
	private function sum_waiting_qc($item_id) {
		$this->loadModel('InventoryQcItem');
		$stocks = $this->InventoryQcItem->find('all', array(
			'fields' => array( 
				'SUM(InventoryQcItem.quantity) AS quantity'
				),
			'conditions' => array(
				'InventoryQcItem.inventory_item_id' => $item_id,
				'InventoryQcItem.status' => 0
				),
			'reqursive' => -1
			));
		if($stocks) {
			foreach ($stocks as $stock) {
				return $stock[0]['quantity'];
			}	
		}
		return 0;
	}

	private function sum_rejected($item_id) {
		$this->loadModel('InventoryStockRejected');
		$stocks = $this->InventoryStockRejected->find('all', array(
			'fields' => array( 
				'SUM(InventoryStockRejected.quantity) AS quantity'
				),
			'conditions' => array(
				'InventoryStockRejected.inventory_item_id' => $item_id,
				'InventoryStockRejected.status' => 1
				),
			'reqursive' => -1
			)); 
		foreach ($stocks as $stock) {
			return $stock[0]['quantity'];
		}	 
	}

	private function sum_stock($item_id) {
		$this->loadModel('InventoryStock');
		$stocks = $this->InventoryStock->find('all', array(
			'fields' => array('SUM(InventoryStock.quantity) AS quantity'),
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $item_id
				),
			'reqursive' => -1
			));
		
		foreach ($stocks as $stock) {
			return $stock[0]['quantity'];
		}	 
	}

	private function sum_sub_assembly_quantity($project_bom_id, $item_id) {
		$this->loadModel('ProjectBomChild');
		$items = $this->ProjectBomChild->find('all', array(
			'fields' => array('SUM(ProjectBomChild.quantity) AS total'),
			'conditions' => array(
				'ProjectBomChild.project_bom_id' => $project_bom_id,
				'ProjectBomChild.inventory_item_id' => $item_id
				),
			'recursive' => -1  
			));
		//$data = array();
		foreach ($items as $item) {
			//$data[] = $item['BomItem']['quantity'];
			return $item[0]['total'];
		}
		//return $data;
	}

	private function sum_item_quantity($project_bom_id, $item_id) {
		$this->loadModel('ProjectBomItem');
		$items = $this->ProjectBomItem->find('all', array(
			'fields' => array('SUM(ProjectBomItem.quantity_total) AS total'),
			'conditions' => array(
				'ProjectBomItem.project_bom_id' => $project_bom_id,
				'ProjectBomItem.inventory_item_id' => $item_id
				),
			'recursive' => -1  
			));
		//$data = array();
		foreach ($items as $item) {
			//$data[] = $item['BomItem']['quantity'];
			return $item[0]['total'];
		}
		//return $data;
	}

	private function get_currency($id) {
		$this->loadModel('SaleQuotationCurrency');
		$currency = $this->SaleQuotationCurrency->find('first', array(
			'conditions' => array(
				'SaleQuotationCurrency.general_currency_id' => $id
				),
			'recursive' => -1  
			));
		return $currency['SaleQuotationCurrency']['rate'];
	}

	private function get_default_supplier_currency($inv_item_id) {
		$this->loadModel('InventorySupplierItem');
		$item = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $inv_item_id,
				//'InventorySupplierItem.general_currency_id !=' => 1, // ! MYR
				'InventorySupplierItem.is_default' => 1
				),
			'recursive' => 0 
			));
		if($item) {
			$price = $item['InventorySupplierItem']['price_per_unit'];
			$total = array(
				'rate' => $this->get_currency($item['InventorySupplierItem']['general_currency_id']), 
				'currency' => $item['GeneralCurrency']['iso_code']
				);
			//$total = $price * $rate;	
		} else {
			$total = array('rate' => 1, 'currency' => 'MYR');
		}
		
		return $total;
	}

	private function get_default_supplier($inv_item_id) {
		$this->loadModel('InventorySupplierItem');
		$supplier = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $inv_item_id,
				//'InventorySupplierItem.general_currency_id !=' => 1, // ! MYR
				'InventorySupplierItem.is_default' => 1
				)
			)); 
		if($supplier) {
			return $supplier;
		} else {
			return array();
		}
		
	}

	private function get_other_supplier($item_id, $except_id) {
		$this->loadModel('InventorySupplierItem');
		$suppliers = $this->InventorySupplierItem->find('all', array(
			'conditions' => array( 
				'InventorySupplierItem.inventory_item_id' => $item_id, 
				'InventorySupplierItem.expiry_date >=' => $this->date,
				'InventorySupplierItem.inventory_supplier_id !=' => $except_id,
				),
			'reqursive' => 0,
			'limit' => 5
			)); 
		return $suppliers;
	}

	private function get_all_supplier() {
		$this->loadModel('InventorySupplier');
		$suppliers = $this->InventorySupplier->find('all', array(
			'conditions' => array( 
				'InventorySupplier.status' => 'Active'
				)
			)); 
		return $suppliers;
	}

	private function get_supplier_json() {
		$this->loadModel('InventorySupplier');
		$suppliers = $this->InventorySupplier->find('all', array(
			'conditions' => array( 
				'InventorySupplier.status' => 'Active'
				),
			'reqursive' => -1,
			'order' => array('InventorySupplier.name' => 'ASC')
			)); 
		$json = array();
		foreach ($suppliers as $supplier) {
			$json[] = array(
				'id' => $supplier['InventorySupplier']['id'],
				'name' =>  $supplier['InventorySupplier']['name']
				);
		}
		return $json;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function purchase($id = null) {
		if (!$this->Planning->exists($id)) {
			throw new NotFoundException(__('Invalid planning'));
		}
		$options = array('conditions' => array('Planning.' . $this->Planning->primaryKey => $id));
		$planning = $this->Planning->find('first', $options);

		$job = explode(',', $planning['Planning']['jobs']); 
 

		$this->loadModel('SaleJob');
		$jobs = $this->SaleJob->find('all', array(
			'conditions' => array(
				'SaleJob.id' => $job
				)
			));

		$this->set('jobs', $jobs);

		$this->loadModel('SaleJobItem');
		$jobitems = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_id' => $job
				),
			'recursive' => 1
			)); 

		$this->set('jobitems', $jobitems);

		if ($this->request->is('post')) {
			 
		} else {
			$this->request->data = $planning;
		}
		// Item 

		$cond['PlanningItem.planning_id'] = $id;
		$cond['OR']['PlanningItem.planning_qty !='] = 0;
		$cond['OR']['PlanningItem.buffer !='] = 0;

		$suppliers = $this->Planning->PlanningItem->find('all', array(
			'conditions' => $cond,
			'order' => array(
				'InventorySupplier.name' => 'ASC'
				),
			'group' => array(
				'PlanningItem.inventory_supplier_id' 
				), 
			'recursive' => -1
			));

		$items = array();
		foreach ($suppliers as $supplier) {
			$items[] = array(
				'InventorySupplier' => $supplier['InventorySupplier'],
				'PlanningItem' => $this->get_planning_item_by_supplier($supplier['PlanningItem']['inventory_supplier_id'], $id)
				);
		}

		$this->set('planning', $planning);
		$this->set('planning', $planning);
		$this->set('items', $items);
	} 

	public function planorderverification($id) {
		if (!$this->Planning->exists($id)) {
			throw new NotFoundException(__('Invalid planning'));
		}


		$options = array('conditions' => array('Planning.' . $this->Planning->primaryKey => $id));
		$planning = $this->Planning->find('first', $options);

		$job = explode(',', $planning['Planning']['jobs']); 
  		
  		$group_id = $this->Session->read('Auth.User.group_id');

		$this->loadModel('SaleJobChild');
		$jobs = $this->SaleJobChild->find('all', array(
			'conditions' => array(
				'SaleJobChild.id' => $job
				)
			));

		$this->set('jobs', $jobs);

		$this->loadModel('SaleJobItem');
		$jobitems = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $job
				),
			'recursive' => 1
			)); 

		$this->set('jobitems', $jobitems);

		// Submit approval
		if(isset($_GET['status'])) {
			// Find Approval 
			$this->loadModel('Approval');
			$approvals = $this->Approval->find('all', array(
				'conditions' => array(
					'Approval.name' => 'Plan Order' 
					)
				));
			if($approvals) {
				$this->Planning->updateAll(array(
					'Planning.status' => "1"
					), array(
					'Planning.id' => $id
					));
				foreach ($approvals as $approval) {
					$data = array(
						'to' => $approval['User']['email'],
						'template' => 'plan_order_verification',
						'subject' => 'Plan Order Require Your Approval ' . $planning['Planning']['name'],
						'content' => array(
							'plan_order_no' => $planning['Planning']['name'],
							'from' => $this->Session->read('Auth.User.username'),
							'username' => $approval['User']['username'],  
							'link' => 'plannings/planorderverification/'.$id
							)
						);
					$this->send_email($data);
				}	
				$this->Session->setFlash(__('Plan Order has been sent for approval.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('User approval not found. Please ask Admin for approval.'), 'error');
				return $this->redirect(array('action' => 'index'));
			}
		}

		if ($this->request->is(array('post', 'put'))) {
			$role = $this->Session->read('Auth.User.id');
			$group_id = $this->Session->read('Auth.User.group_id');
			$action = $this->request->data['Planning']['status'];
			$this->request->data['Planning']['modified'] = $this->date;
			if($group_id == 1 || $group_id == 12 || $group_id == 6 && $role == 'HOD' || $role == 'HOS') {

				if($action == 2) {
					$act = 2;
					$name = 'Verify Plan Order: ' . $planning['Planning']['name'];
					$type = 'Verify Plan Order';

					// Copy to purchase requisition

				}
				if($action == 3) {
					$act = 3;
					$name = 'Reject Plan Order: ' . $planning['Planning']['name'];
					$type = 'Reject Plan Order';
				} 

				// Log
				if($this->Planning->save($this->request->data)) {
					$link = 'plannings/view/' . $id; 
					$this->insert_log($this->user_id, $name, $link, $type);
					$this->Session->setFlash(__('The Plan Order has been saved.'), 'success');
					return $this->redirect(array('action' => 'verification'));
				} else {
					$this->Session->setFlash(__('Planning could not be save.'), 'error');
				}
			} else {
				$this->Session->setFlash(__('You are not authorize to edit this section.'), 'error');
			}
		} else {
			$this->request->data = $planning;
		}

		// Job for budget
		$this->loadModel('PlanningItemJob');
		$jobs = $this->PlanningItemJob->find('all', array(
			'fields' => array(
				'PlanningItemJob.*',
				'SaleJobChild.*',
				'SUM(PlanningItemJob.total_price_rm) AS total_price_rm' 
				),
			'conditions' => array(
				'PlanningItemJob.planning_id' => $id
				),
			'group' => 'PlanningItemJob.sale_job_child_id'
			));
		$job_budgets = array();
		foreach ($jobs as $job) {
			$job_budgets[] = array(
				'PlanningItemJob' => $job['PlanningItemJob'],
				'RequireBudget' => $job[0]['total_price_rm'],
				'SaleJobChild' => $job['SaleJobChild'],
				'ProjectBudget' => $this->get_job_budget($job['PlanningItemJob']['sale_job_child_id']),
				);
		}
		//Budget compare $job['PlanningItemJob']['sale_job_child_id']
		$this->set('job_budgets', $job_budgets); 

		// Item 

		$this->loadModel('PlanningItem');
		$cond['PlanningItem.planning_id'] = $id; 




		$con = array( 
			'conditions' => $cond,
			'order' => 'InventorySupplier.name ASC', 
			'limit' => 1000,
			'recursive' => 0,
			'group' => 'PlanningItem.inventory_supplier_id'
			); 
		$suppliers = $this->PlanningItem->find('all', $con);
		$items = array();
		foreach ($suppliers as $supplier) {
			$items[] = array(
				'InventorySupplier' => $supplier['InventorySupplier'], 
				'PlanningItem' => $this->plan_item_by_supplier($supplier['PlanningItem']['inventory_supplier_id'], $id)
				);
		}

		//var_dump($items);
		$this->set('suppliers', $items); 
		$this->set('planning', $planning); 
	}

	private function _planorderreview($id) {
		if (!$this->Planning->exists($id)) {
			throw new NotFoundException(__('Invalid planning'));
		}


		$options = array('conditions' => array('Planning.' . $this->Planning->primaryKey => $id));
		$planning = $this->Planning->find('first', $options);

		$job = explode(',', $planning['Planning']['jobs']); 
  		
  		$group_id = $this->Session->read('Auth.User.group_id');

		$this->loadModel('SaleJobChild');
		$jobs = $this->SaleJobChild->find('all', array(
			'conditions' => array(
				'SaleJobChild.id' => $job
				)
			));

		$this->set('jobs', $jobs);

		$this->loadModel('SaleJobItem');
		$jobitems = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $job
				),
			'recursive' => 1
			)); 

		$this->set('jobitems', $jobitems);

		// Submit approval
		if(isset($_GET['status'])) {
			// Find Approval 
			$this->loadModel('Approval');
			$approvals = $this->Approval->find('all', array(
				'conditions' => array(
					'Approval.name' => 'Plan Order' 
					)
				));
			if($approvals) {
				$this->Planning->updateAll(array(
					'Planning.status' => "1"
					), array(
					'Planning.id' => $id
					));
				foreach ($approvals as $approval) {
					$data = array(
						'to' => $approval['User']['email'],
						'template' => 'plan_order_verification',
						'subject' => 'Plan Order Require Your Approval ' . $planning['Planning']['name'],
						'content' => array(
							'plan_order_no' => $planning['Planning']['name'],
							'from' => $this->Session->read('Auth.User.username'),
							'username' => $approval['User']['username'],  
							'link' => 'plannings/planorderverification/'.$id
							)
						);
					$this->send_email($data);
				}	
				$this->Session->setFlash(__('Plan Order has been sent for approval.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('User approval not found. Please ask Admin for approval.'), 'error');
				return $this->redirect(array('action' => 'index'));
			}
		}

		if ($this->request->is('post')) {
			// print_r($this->request->data); 
			$this->loadModel('InventoryPurchaseRequisition');
			$this->loadModel('InventoryPurchaseRequisitionItem');
			$pr_suppliers = $this->request->data['inventory_supplier_id'];

			// Items
			$require_date = $this->request->data['default_delivery'];
			$supplier_item_id = $this->request->data['inventory_supplier_item_id'];
			$item_id = $this->request->data['inventory_item_id'];
			$general_unit_id = $this->request->data['general_unit_id'];
			$quantity = $this->request->data['quantity'];
			$amount = $this->request->data['amount'];
			$unit_price = $this->request->data['unit_price'];

			$dateline = $this->request->data['dateline'];

			$planning_item_jobs = $this->request->data['planning_item_jobs'];

			$jobs = $this->request->data['jobs'];

			for($i = 0; $i < count($pr_suppliers); $i++) {

				$req_date = explode(',', $dateline[$i]);
				$job = $jobs[$i];
				$count = $this->InventoryPurchaseRequisition->find('all', array( 
					'recursive' => -1
				)); 
				$pr_no = $this->generate_code('PR', count($count) + 1);
				$this->InventoryPurchaseRequisition->create();
				$header = array(
					'user_id' => $this->user_id,
					'group_id' => $group_id,
					'general_purchase_requisition_type_id' => 2,
					'created' => $this->date,
					'modified' => $this->date,
					'dateline' => $req_date[0],
					'status' => 1,
					'pr_no' => $pr_no,
					'account_department_budget_id' => 1,
					'term_of_payment_id' => 1,
					'remark_id' => 0,
					'warranty_id' => 0,
					'sale_job_id' => 0,
					'sale_job_child_id' => 0,
					'po_type' => 0,
					'planning_item_jobs' => $job, 
					'pr_type' => 4, // 1 Capex, 2 Opex, 3 Serving client or customer, MRP
					'inventory_supplier_id' => $pr_suppliers[$i]
					);
				$save_head = $this->InventoryPurchaseRequisition->save($header);
				if($save_head) {
					$last_id = $this->InventoryPurchaseRequisition->getLastInsertId();

					for($x = 0; $x < count($supplier_item_id[$pr_suppliers[$i]]); $x++) { 
						$this->InventoryPurchaseRequisitionItem->create();
						$insert_item = array(
							'inventory_purchase_requisition_id' => $last_id,
							'quantity' => $quantity[$pr_suppliers[$i]][$x],
							'general_unit_id' => $general_unit_id[$pr_suppliers[$i]][$x],
							'inventory_item_id' => $item_id[$pr_suppliers[$i]][$x],
							'inventory_supplier_item_id' => $supplier_item_id[$pr_suppliers[$i]][$x],
							'inventory_item_category_id' => 0,
							'inventory_supplier_id' => $pr_suppliers[$i],
							'price_per_unit' => $unit_price[$pr_suppliers[$i]][$x],
							'amount' => $amount[$pr_suppliers[$i]][$x],
							'discount' => 0,
							'discount_type' => 0,
							'tax'=> $this->check_supplier_tax($pr_suppliers[$i]),
							'planning_item_jobs' => $planning_item_jobs[$pr_suppliers[$i]][$x],
							);
						$this->InventoryPurchaseRequisitionItem->save($insert_item);
					}
				}
			}
		} else {
			$this->request->data = $planning;
		}

		// Job for budget
		$this->loadModel('PlanningItemJob');
		$jobs = $this->PlanningItemJob->find('all', array(
			'fields' => array(
				'PlanningItemJob.*',
				'SaleJobChild.*',
				'SUM(PlanningItemJob.total_price_rm) AS total_price_rm' 
				),
			'conditions' => array(
				'PlanningItemJob.planning_id' => $id
				),
			'group' => 'PlanningItemJob.sale_job_child_id'
			));
		$job_budgets = array();
		foreach ($jobs as $job) {
			$job_budgets[] = array(
				'PlanningItemJob' => $job['PlanningItemJob'],
				'RequireBudget' => $job[0]['total_price_rm'],
				'SaleJobChild' => $job['SaleJobChild'],
				'ProjectBudget' => $this->get_job_budget($job['PlanningItemJob']['sale_job_child_id']),
				);
		}
		//Budget compare $job['PlanningItemJob']['sale_job_child_id']
		$this->set('job_budgets', $job_budgets); 

		// Item 

		$this->loadModel('PlanningItem');
		$cond['PlanningItem.planning_id'] = $id; 




		$con = array( 
			'conditions' => $cond,
			'order' => 'InventorySupplier.name ASC', 
			'limit' => 1000,
			'recursive' => 0,
			'group' => 'PlanningItem.inventory_supplier_id'
			); 
		$suppliers = $this->PlanningItem->find('all', $con);
		$items = array();
		foreach ($suppliers as $supplier) {
			$items[] = array(
				'InventorySupplier' => $supplier['InventorySupplier'], 
				'PlanningItem' => $this->plan_item_by_supplier($supplier['PlanningItem']['inventory_supplier_id'], $id),
				'PlanningItemRemarks' => $this->get_verifier_remark($supplier['PlanningItem']['inventory_supplier_id'], $id)
				);
		}

		//var_dump($items);
		$this->set('suppliers', $items); 
		$this->set('planning', $planning); 
	}

	public function planorderreview($id) {
		if (!$this->Planning->exists($id)) {
			throw new NotFoundException(__('Invalid planning'));
		}


		$options = array('conditions' => array('Planning.' . $this->Planning->primaryKey => $id));
		$planning = $this->Planning->find('first', $options);

		$job = explode(',', $planning['Planning']['jobs']); 
  		
  		$group_id = $this->Session->read('Auth.User.group_id');

		$this->loadModel('SaleJobChild');
		$jobs = $this->SaleJobChild->find('all', array(
			'conditions' => array(
				'SaleJobChild.id' => $job
				)
			));


		$this->set('jobs', $jobs);

		$this->loadModel('SaleJobItem');
		$jobitems = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $job
				),
			'recursive' => 1
			)); 

		$this->set('jobitems', $jobitems);

		// Submit approval
		if(isset($_GET['status'])) {
			// Find Approval 
			$this->loadModel('Approval');
			$approvals = $this->Approval->find('all', array(
				'conditions' => array(
					'Approval.name' => 'Plan Order' 
					)
				));
			if($approvals) {
				$this->Planning->updateAll(array(
					'Planning.status' => "1"
					), array(
					'Planning.id' => $id
					));
				foreach ($approvals as $approval) {
					$data = array(
						'to' => $approval['User']['email'],
						'template' => 'plan_order_verification',
						'subject' => 'Plan Order Require Your Approval ' . $planning['Planning']['name'],
						'content' => array(
							'plan_order_no' => $planning['Planning']['name'],
							'from' => $this->Session->read('Auth.User.username'),
							'username' => $approval['User']['username'],  
							'link' => 'plannings/planorderverification/'.$id
							)
						);
					$this->send_email($data);
				}	
				$this->Session->setFlash(__('Plan Order has been sent for approval.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('User approval not found. Please ask Admin for approval.'), 'error');
				return $this->redirect(array('action' => 'index'));
			}
		}

		if ($this->request->is('post')) {
			// print_r($this->request->data); 
			$this->loadModel('InventoryPurchaseRequisition');
			$this->loadModel('InventoryPurchaseRequisitionItem');
			$pr_suppliers = $this->request->data['inventory_supplier_id'];

			// Items
			$require_date = $this->request->data['default_delivery'];
			$supplier_item_id = $this->request->data['inventory_supplier_item_id'];
			$item_id = $this->request->data['inventory_item_id'];
			$general_unit_id = $this->request->data['general_unit_id'];
			$quantity = $this->request->data['quantity'];
			$amount = $this->request->data['amount'];
			$unit_price = $this->request->data['unit_price'];

			$total_rm = $this->request->data['total_rm'];

			$dateline = $this->request->data['dateline'];

			$planning_item_jobs = $this->request->data['planning_item_jobs'];

			$delivery_remark = $this->request->data['delivery_remark'];

			$jobs = $this->request->data['jobs'];

			$general_currency_id = $this->request->data['general_currency_id']; 
 
			$count = $this->InventoryPurchaseRequisition->find('first', array( 
				'recursive' => -1,
				'order' => 'InventoryPurchaseRequisition.id DESC'
			)); 
			$pr_no = $this->generate_code('PR', $count['InventoryPurchaseRequisition']['id'] + 1);
			$this->InventoryPurchaseRequisition->create();
			$budget = $this->find_material_budget_by_job($job[0]);
			$header = array(
				'user_id' => $this->user_id,
				'group_id' => $group_id,
				'general_purchase_requisition_type_id' => 2, // Auto
				'created' => $this->date, 
				'dateline' => $this->date, 
				'status' => 0,
				'pr_no' => $pr_no,
				'account_department_budget_id' => empty($budget['AccountDepartmentBudget']['id']) ? 0 : $budget['AccountDepartmentBudget']['id'],
				'term_of_payment_id' => 1,
				'remark_id' => 0,
				'warranty_id' => 0,
				'sale_job_id' => 0,
				'sale_job_child_id' => $job[0],
				'po_type' => 1,
				'planning_item_jobs' => 0, 
				'pr_type' => 3, 
				); 
			$save_head = $this->InventoryPurchaseRequisition->save($header);
			$last_id = $this->InventoryPurchaseRequisition->getLastInsertId();
			for($i = 0; $i < count($item_id); $i++) {   
				$this->InventoryPurchaseRequisitionItem->create();
				$insert_item = array(
					'inventory_purchase_requisition_id' => $last_id,
					'quantity' => $quantity[$i],
					'general_unit_id' => $general_unit_id[$i],
					'general_currency_id' => $general_currency_id[$i],
					'inventory_item_id' => $item_id[$i],
					'inventory_supplier_item_id' => $supplier_item_id[$i],
					'inventory_item_category_id' => 0,
					'inventory_supplier_id' => $pr_suppliers[$i],
					'price_per_unit' => $unit_price[$i],
					'amount' => $amount[$i],
					'discount' => 0,
					'discount_type' => 0,
					'tax'=> $this->check_supplier_tax($pr_suppliers[$i]),
					'planning_item_jobs' => $planning_item_jobs[$i],
					'delivery_remark' => $delivery_remark[$i],
					'total_rm' => $total_rm[$i],
					);
				$this->InventoryPurchaseRequisitionItem->save($insert_item); 
			}
			$this->Session->setFlash(__('Your Plan Order has been saved to Purchase Requisition. PR number: '. $pr_no), 'success');
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->request->data = $planning;
		}

		// Job for budget
		$this->loadModel('PlanningItemJob');
		$jobs = $this->PlanningItemJob->find('all', array(
			'fields' => array(
				'PlanningItemJob.*',
				'SaleJobChild.*',
				'SUM(PlanningItemJob.total_price_rm) AS total_price_rm' 
				),
			'conditions' => array(
				'PlanningItemJob.planning_id' => $id
				),
			'group' => 'PlanningItemJob.sale_job_child_id'
			));
		$job_budgets = array();
		foreach ($jobs as $job) {
			$job_budgets[] = array(
				'PlanningItemJob' => $job['PlanningItemJob'],
				'RequireBudget' => $job[0]['total_price_rm'],
				'SaleJobChild' => $job['SaleJobChild'],
				'ProjectBudget' => $this->get_job_budget($job['PlanningItemJob']['sale_job_child_id']),
				);
		}
		//Budget compare $job['PlanningItemJob']['sale_job_child_id']
		$this->set('job_budgets', $job_budgets); 

		// Item 

		$this->loadModel('PlanningItem');
		$cond['PlanningItem.planning_id'] = $id; 




		$con = array( 
			'conditions' => $cond,
			'order' => 'InventorySupplier.name ASC', 
			'limit' => 1000,
			'recursive' => 0,
			'group' => 'PlanningItem.inventory_supplier_id'
			); 
		$suppliers = $this->PlanningItem->find('all', $con);
		$items = array();
		foreach ($suppliers as $supplier) {
			$items[] = array(
				'InventorySupplier' => $supplier['InventorySupplier'], 
				'PlanningItem' => $this->plan_item_by_supplier($supplier['PlanningItem']['inventory_supplier_id'], $id),
				'PlanningItemRemarks' => $this->get_verifier_remark($supplier['PlanningItem']['inventory_supplier_id'], $id)
				);
		}

		//var_dump($items);
		$this->set('suppliers', $items); 
		$this->set('planning', $planning); 
	}

	private function find_material_budget_by_job($sale_job_child_id) {
		$this->loadModel('AccountDepartmentBudget');
		$budget = $this->AccountDepartmentBudget->find('first', array(
			'conditions' => array(
				'AccountDepartmentBudget.sale_job_child_id' => $sale_job_child_id,
				'AccountDepartmentBudget.is_material' => 1
				)
			));
		return $budget;
	}

	private function check_supplier_tax($supplier_id) {
		$this->loadModel('InventorySupplier');
		$tax = $this->InventorySupplier->find('first', array(
			'conditions' => array(
				'InventorySupplier.id' => $supplier_id
				),
			'recursive' => -1
			));
		if(!empty($tax['InventorySupplier']['gst'])) {
			return Configure::read('Site.gst_amount');
		} else {
			return 0;
		}
	}

	private function get_verifier_remark($supplier_id, $plan_id) {
		$this->loadModel('PlanningItemRemark');
		$remarks = $this->PlanningItemRemark->find('all', array(
			'conditions' => array(
				'PlanningItemRemark.planning_id' => $plan_id,
				'PlanningItemRemark.inventory_supplier_id' => $supplier_id,
				),
			'limit' => 5,
			'order' => 'PlanningItemRemark.id DESC'
			));
		return $remarks;
	}

	private function get_job_budget($sale_job_child_id) {
		$this->loadModel('ProjectBudget');
		$budgets = $this->ProjectBudget->find('all', array(
			'fields' => array(
				'SUM(ProjectBudget.material_cost) AS material_cost', 
				'SUM(ProjectBudget.used) AS used', 
				'SUM(ProjectBudget.balance) AS balance',
				'SUM(ProjectBudget.indirect_cost) AS indirect_cost',
				'SUM(ProjectBudget.soft_cost) AS soft_cost',
				'SUM(ProjectBudget.margin_value) AS margin_value',
				'SUM(ProjectBudget.planning_price) AS planning_price', 
				'SUM(ProjectBudget.amount) AS amount' 
				),
			'conditions' => array(
				'ProjectBudget.sale_job_child_id' => $sale_job_child_id,
				'ProjectBudget.type' => 'Job Budget'
				)
			));
		foreach ($budgets as $budget) {
			return $budget[0];
		}
	}

	private function get_planning_item_by_supplier($supplier_id, $planning_id) {
		$cond['PlanningItem.planning_id'] = $planning_id;
		$cond['PlanningItem.inventory_supplier_id'] = $supplier_id;
		$cond['OR']['PlanningItem.planning_qty !='] = 0;
		$cond['OR']['PlanningItem.buffer !='] = 0; 
		$items = $this->Planning->PlanningItem->find('all', array(
			'conditions' => $cond,  
			'recursive' => 0
			));

		return $items;
	} 

	private function plan_item_by_supplier($supplier_id, $planning_id) {
		$cond['PlanningItem.planning_id'] = $planning_id;
		$cond['PlanningItem.inventory_supplier_id'] = $supplier_id;  
		$items = $this->Planning->PlanningItem->find('all', array(
			'conditions' => $cond,
			'recursive' => 0
			));
		
		$data = array();
		foreach ($items as $item) {
			$data[] = array(
				'PlanningItem' => $item['PlanningItem'], 
				'InventoryItem' => $item['InventoryItem'], 
				'InventorySupplierItem' => $item['InventorySupplierItem'],
				'GeneralUnit' => $item['GeneralUnit'], 
				'PlanningUnit' => $item['PlanningUnit'], 
				'GeneralCurrency' => $item['GeneralCurrency'], 
				'Jobs' => $this->get_plan_item_job($item['PlanningItem']['planning_id'], $item['PlanningItem']['id'])
				);
		}
		return $data;
	} 

	private function get_plan_item_job($planning_id, $planning_item_id) {
		$this->loadModel('PlanningItemJob');
		$jobs = $this->PlanningItemJob->find('all', array(
			'conditions' => array(
				'PlanningItemJob.planning_id' => $planning_id,
				'PlanningItemJob.planning_item_id' => $planning_item_id 
				),
			'recursive' => 0,
			'limit' => 10
			));
		return $jobs;
	}

	private function count_partial($planning_item_id, $planning_id) {
		$this->loadModel('PlanningPartialDelivery');
		$count = $this->PlanningPartialDelivery->find('all', array(
				'conditions' => array(
					'PlanningPartialDelivery.planning_id' => $planning_id,
					'PlanningPartialDelivery.planning_item_id' => $planning_item_id
					),
				'recursive' => -1
				));
		return count($count);
	}

	public function view($id = null) {
		if (!$this->Planning->exists($id)) {
			throw new NotFoundException(__('Invalid planning'));
		}
		$options = array('conditions' => array('Planning.' . $this->Planning->primaryKey => $id));
		$planning = $this->Planning->find('first', $options);

		$job = explode(',', $planning['Planning']['jobs']); 
 

		$this->loadModel('SaleJobChild');
		$jobs = $this->SaleJobChild->find('all', array(
			'conditions' => array(
				'SaleJobChild.id' => $job
				)
			));

		$this->set('jobs', $jobs);

		$this->loadModel('SaleJobItem');
		$jobitems = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $job
				),
			'recursive' => 0
			)); 

		$this->set('jobitems', $jobitems);

		if ($this->request->is(array('post', 'put'))) {
			$role = $this->Session->read('Auth.User.id');
			$group_id = $this->Session->read('Auth.User.group_id');
			$action = $this->request->data['Planning']['status'];
			$this->request->data['Planning']['modified'] = $this->date;
			if($group_id == 1 || $group_id == 12 || $group_id == 6 && $role == 'HOD' || $role == 'HOS') {

				if($action == 2) {
					$act = 2;
					$name = 'Verify Plan Order: ' . $planning['Planning']['name'];
					$type = 'Verify Plan Order';

					// Copy to purchase requisition

				}
				if($action == 3) {
					$act = 3;
					$name = 'Reject Plan Order: ' . $planning['Planning']['name'];
					$type = 'Reject Plan Order';
				} 

				// Log
				if($this->Planning->save($this->request->data)) {
					$link = 'plannings/view/' . $id; 
					$this->insert_log($this->user_id, $name, $link, $type);
					$this->Session->setFlash(__('The Plan Order has been saved.'), 'success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Planning could not be save.'), 'error');
				}
			} else {
				$this->Session->setFlash(__('You are not authorize to edit this section.'), 'error');
			}
		} else {
			$this->request->data = $planning;
		}
		// Item 

		$this->loadModel('PlanningItem');
		$cond['PlanningItem.planning_id'] = $id;
		/*$cond['OR']['PlanningItem.planning_qty !='] = 0;
		$cond['OR']['PlanningItem.buffer !='] = 0;*/


		/*$this->Paginator->settings = array( 
			'conditions' => $cond,
			'order' => 'InventorySupplier.name ASC', 
			'limit' => 600,
			'recursive' => 0,
			'contain' => array('InventorySupplier')
			); */

		$items = $this->PlanningItem->query("SELECT 
			PlanningItem.*,
			GeneralUnit.*,
			InventoryItem.*,
			PlanningUnit.*,
			InventorySupplierItem.*,
			InventorySupplier.*,
			GeneralCurrency.*
			FROM planning_items AS PlanningItem
			LEFT JOIN inventory_items AS InventoryItem ON(InventoryItem.id = PlanningItem.inventory_item_id)
			LEFT JOIN inventory_suppliers AS InventorySupplier ON(InventorySupplier.id = PlanningItem.inventory_supplier_id)
			LEFT JOIN general_units AS GeneralUnit ON(GeneralUnit.id = PlanningItem.general_unit_id)
			LEFT JOIN general_units AS PlanningUnit ON(PlanningUnit.id = PlanningItem.planning_general_unit)
			LEFT OUTER JOIN inventory_supplier_items AS InventorySupplierItem ON(InventorySupplierItem.id = PlanningItem.inventory_supplier_item_id)
			LEFT JOIN general_currencies AS GeneralCurrency ON(GeneralCurrency.id = PlanningItem.general_currency_id)
			WHERE PlanningItem.planning_id = '$id' 
			AND PlanningItem.planning_qty != '0' OR PlanningItem.buffer != '0'
			LIMIT 800
			");
		
		$this->set('items', $items);

		/*
		$items = $this->PlanningItem->find('all', array(
			'conditions' => $cond,
			'order' => array(
				'InventorySupplier.name' => 'ASC'
				),
			'recursive' => 0
			));
		$this->set('items', $items);
		*/
		$this->set('planning', $planning); 
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Planning->create();
			if ($this->Planning->save($this->request->data)) {
				$this->Session->setFlash(__('The planning has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The planning could not be saved. Please, try again.'));
			}
		}
		 
		 
		$users = $this->Planning->User->find('list');
		$this->set(compact('saleJobItems', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Planning->exists($id)) {
			throw new NotFoundException(__('Invalid planning'));
		}

		$options = array('conditions' => array('Planning.' . $this->Planning->primaryKey => $id));
		$planning = $this->Planning->find('first', $options);

		$user_id = $this->Session->read('Auth.User.id');

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Planning->save($this->request->data)) {
				// verification
				$code = $this->request->data['Planning']['name'];
				$status = $this->request->data['Planning']['status'];
				if($status == 1) {
					$this->send_approval($id, $code);
				} 
				$name = 'Edit Plan Order: ' . $code;
				$link = 'plannings/view/' . $id;
				$type = 'Edit Plan Order';
				$this->insert_log($user_id, $name, $link, $type);

				$this->Session->setFlash(__('The planning has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The planning could not be saved. Please, try again.'));
			}
		} else {
			
			$this->request->data = $planning;
		}
		 

		$this->set('planning', $planning);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Planning->id = $id;
		if (!$this->Planning->exists()) {
			throw new NotFoundException(__('Invalid planning'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Planning->delete()) {
			$this->Session->setFlash(__('The planning has been deleted.'));
		} else {
			$this->Session->setFlash(__('The planning could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function review_old() {

		$this->loadModel('SaleJob');
		
		if ($this->request->is('post')) {
			$id = $_POST['jobs'];
		}
		$options = array('conditions' => array('SaleJob.id' => $id));

		$this->loadModel('SaleJobItem');
		$job_items = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_id' => $id
				)
			));

		$this->set('saleJob', $this->SaleJob->find('first', $options));
		$this->set('items', $job_items);

		$bom = array();
		foreach ($job_items as $item) {
			$bom[] = $item['SaleJobItem']['bom_id'];
		}

		// Combine all items
		$this->loadModel('BomChild');
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $bom
				)
			));
		
		$child_id = array();
		foreach ($childs as $child) {
			$child_id[] = $child['BomChild']['id'];
		}

		$this->loadModel('BomItem');
		$bomitems = $this->BomItem->find('all', array( 
			'conditions' => array(
				'BomItem.bom_child_id' => $child_id
				),
			// 'group' => 'BomItem.inventory_item_id' 
			));
   
		//echo '<pre>';
		//var_dump($bomitems);
		$items = array();
		foreach ($bomitems as $bomitem) {
			$items[] = array(
				'item_id' => $bomitem['BomItem']['id'],
				'total' => $this->sum_item_quantity($child_id, $bomitem['BomItem']['inventory_item_id']),
				'name' => $bomitem['InventoryItem']['name'],
				'unit' => $bomitem['GeneralUnit']['name'],
				'code' => $bomitem['InventoryItem']['code'],
				'supplier' => $this->get_default_supplier($bomitem['BomItem']['inventory_item_id']),
				'suppliers' => $this->get_all_supplier($bomitem['BomItem']['inventory_item_id']) 
				);
			
		}
		//var_dump($items);
		$this->set('bomitems', $items);
	} 
}
