<?php
App::uses('AppController', 'Controller');
/**
 * SaleTenders Controller
 *
 * @property SaleTender $SaleTender
 * @property PaginatorComponent $Paginator
 */
class SaleTendersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxfindtender');
	}

/**
 * index method
 * 
 * @return void
 */
	public function index() {


		$record_per_page = Configure::read('Reading.nodes_per_page');
		 
		$conditions['SaleTender.id >'] = 0;
		if(isset($_GET['search'])) {
			if($_GET['customer'] != '') {
				$conditions['Customer.name LIKE'] = '%'.trim($_GET['customer']).'%';
			}
			if($_GET['tender_no'] != '') {
				$conditions['SaleTender.tender_no LIKE'] = '%'.trim($_GET['tender_no']).'%';
			} 

			if($_GET['title'] != '') {
				$conditions['SaleTender.title LIKE'] = '%'.trim($_GET['title']).'%';
			} 

			$this->request->data = $_GET;
		}

		 
		$this->Paginator->settings = array(
			'conditions' => $conditions,   
			'order'=>'SaleTender.id DESC',
			'group'=>'SaleTender.id',
			'limit'=>$record_per_page
			);  
 
		$this->set('saleTenders', $this->Paginator->paginate());
	}

	// Autocomplete find tender name
	// return Tender Id
	public function ajaxfindtender() {
		
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$conditions['OR']['SaleTender.title LIKE'] = '%'.$term.'%';
			$conditions['OR']['SaleTender.tender_no LIKE'] = '%'.$term.'%';

			$items = $this->SaleTender->find('all', array('conditions' => $conditions));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['SaleTender']['id'], 
					'title' => $item['SaleTender']['title'],
					'tender_no' => $item['SaleTender']['tender_no'],
					'customer' => $item['Customer']['name'],
					'customer_id' => $item['Customer']['id'],
					'closing_date' => $item['SaleTender']['closing_date']
					); 
			}
			echo json_encode($json);	
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleTender->exists($id)) {
			throw new NotFoundException(__('Invalid sale tender'));
		}
		$options = array('conditions' => array('SaleTender.' . $this->SaleTender->primaryKey => $id));
		$this->set('saleTender', $this->SaleTender->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleTender->create();
			//print_r($_POST);
			//exit;
			$closing_date = date('Y-m-d H:i:s', strtotime($_POST['data']['SaleTender']['closing_date']));
			$extend_date = date('Y-m-d H:i:s', strtotime($_POST['data']['SaleTender']['extend_date']));
			
			if($this->request->data['SaleTender']['price'] == '') {
				$this->request->data['SaleTender']['price'] = 0;
			}
			 
			$this->request->data['SaleTender']['estimate_price'] = 0;
			 
			$this->request->data['SaleTender']['status'] = '1'; 
			$this->request->data['SaleTender']['created'] = date('Y-m-d H:i:s'); 
			$this->request->data['SaleTender']['closing_date'] = $closing_date;  

			$this->request->data['SaleTender']['user_id'] = $this->user_id; 

			if ($this->SaleTender->save($this->request->data)) {

				$id = $this->SaleTender->getLastInsertId();
				$name = 'Add New Tender / Enquiry';
				$link = 'sale_tenders/view/'.$id;
				$type = 'Add Tender';
				$this->insert_log($this->user_id, $name, $link, $type);

				$this->Session->setFlash(__('The sale tender has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale tender could not be saved. Please, try again.'), 'error');
			}
		}
		$customers = $this->SaleTender->Customer->find('list', array(
            'group' => 'Customer.name'
        ));
		$saleTenderTypes = $this->SaleTender->SaleTenderType->find('list');
		$users = $this->SaleTender->User->find('list');
		$this->set(compact('customers', 'saleTenderTypes', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		//print_r($_POST);
		//exit;
		

		if (!$this->SaleTender->exists($id)) {
			throw new NotFoundException(__('Invalid sale tender'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$closing_date = date('Y-m-d H:i:s', strtotime($this->request->data['SaleTender']['closing_date']));
			$extend_date = date('Y-m-d H:i:s', strtotime($this->request->data['SaleTender']['extend_date']));
			
			$this->request->data['SaleTender']['status'] = '1'; 
			$this->request->data['SaleTender']['created'] = date('Y-m-d H:i:s'); 
			$this->request->data['SaleTender']['closing_date'] = $closing_date; 
			$this->request->data['SaleTender']['extend_date'] = $extend_date;

			$this->request->data['SaleTender']['user_id'] = $_SESSION['Auth']['User']['id'];
			
			if ($this->SaleTender->save($this->request->data)) {

				$name = 'Edit Tender';
				$link = 'sale_tenders/view/'.$id;
				$type = 'Edit Tender';
				$this->insert_log($this->user_id, $name, $link, $type);

				$this->Session->setFlash(__('The sale tender has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale tender could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('SaleTender.' . $this->SaleTender->primaryKey => $id));
			$this->request->data = $this->SaleTender->find('first', $options);
		}
		$customers = $this->SaleTender->Customer->find('list');
		$saleTenderTypes = $this->SaleTender->SaleTenderType->find('list');
		$users = $this->SaleTender->User->find('list');
		$this->set(compact('customers', 'saleTenderTypes', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleTender->id = $id;
		if (!$this->SaleTender->exists()) {
			throw new NotFoundException(__('Invalid sale tender'));
		}
		$this->request->is('post', 'delete');

		if ($this->SaleTender->delete()) {

			$name = 'Tender Deleted';
			$link = 'sale_tenders/view/'.$id;
			$type = 'Delete Tender';
			$this->insert_log($this->user_id, $name, $link, $type);

			$this->Session->setFlash(__('The sale tender has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The sale tender could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
