<?php
App::uses('AppController', 'Controller'); 
 
class PagesController extends AppController {  

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index', 'view');
	}

	public function index() {  
		$this->layout = 'public'; 
	}

	public function view($slug) {
		$this->loadModel('Content.Content');
		
		$options = array('conditions' => array('Content.slug' => $slug),
			'contain' => array(
				'ContentMeta',
				'ContentSeo',
				'FeatureImage'
			)
		);
		$content = $this->Content->find('first', $options);
		$this->set('content', $content);
	}
}