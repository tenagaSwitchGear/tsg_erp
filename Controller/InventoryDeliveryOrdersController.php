<?php
App::uses('AppController', 'Controller');
App::uses('BarcodeHelper','Vendor');
/**
 * InventoryDeliveryOrders Controller
 *
 * @property InventoryDeliveryOrder $InventoryDeliveryOrder
 * @property PaginatorComponent $Paginator
 */
class InventoryDeliveryOrdersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

    public function grn($status = null) { 
    	if($status != null) {
			$do_status = $status;
		} else {
			$do_status = 0;
		}

		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array(
			'order' => 'InventoryDeliveryOrder.id DESC',
			'limit' => $record_per_page
		);
        $deliveryorders = $this->Paginator->paginate();
        $this->set('deliveryorders', $deliveryorders);
    }

/**
 * index method
 *
 * @return void
 */
	public function index($status = null) {
		//$this->InventoryDeliveryOrder->recursive = 1;
		//$this->set('inventoryDeliveryOrders', $this->Paginator->paginate());
		//$this->loadModel('InventoryPurchaseOrder');
		//$ipo = $this->InventoryPurchaseOrder->find('all');
		//$this->set('inventoryPurchaseOrders', $ipo);
		$conditions = array();
		if(isset($_GET['search'])) {
			if($_GET['supplier'] != '') {
				$conditions['InventorySupplier.name LIKE'] = '%'.$_GET['supplier'].'%';
			}
			if($_GET['po_no'] != '') {
				$conditions['InventoryPurchaseOrder.po_no'] = $_GET['po_no'];
			}
			$this->request->data['InventoryDeliveryOrder'] = $_GET;
		}

		
		if($status != null) {
			$grn_status = $status;
		} else {
			$grn_status = 0;
		}

		$conditions['InventoryPurchaseOrder.status'] = $grn_status;
		$conditions['InventoryPurchaseOrder.purchase_status'] = 1;

		$this->loadModel('InventoryPurchaseOrder');
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order' => 'InventoryPurchaseOrder.id DESC',
			'limit' => $record_per_page
		);
        $nfg = $this->Paginator->paginate('InventoryPurchaseOrder');
        
        $arr = array();
        foreach ($nfg as $ipo) {
        	$arr[] = array(
        		'InventoryPurchaseOrder' 		=> $ipo['InventoryPurchaseOrder'],
        		'User'							=> $ipo['User'],
        		'Group'							=> $ipo['Group'],
        		'GeneralPurchaseRequisitionType'=> $ipo['GeneralPurchaseRequisitionType'],
        		'InventorySupplier'				=> $ipo['InventorySupplier'],
        		'TermOfPayment'					=> $ipo['TermOfPayment'],
        		'InventoryDeliveryLocation'		=> $ipo['InventoryDeliveryLocation'],
        		'InventoryPurchaseRequisitions'	=> $ipo['InventoryPurchaseRequisition'],
        		'InventoryPurchaseOrderItem'	=> $ipo['InventoryPurchaseOrderItem'],
        		'GRN' 							=> $this->get_grn($ipo['InventoryPurchaseOrder']['id']),
        	);
        }
      
		$this->set('inventoryPurchaseOrders', $arr);
	}

	private function get_grn($id){
		$this->loadModel('InventoryDeliveryOrder');
		$grn = $this->InventoryDeliveryOrder->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrder.inventory_purchase_order_id' => $id
			),
			'recursive' => -1
		));

		return $grn;

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function view_barcode($id = null){
		ini_set('memory_limit', '512M');
    	$this->layout = 'printBARCODE';

    	$this->loadModel('InventoryDeliveryOrderItem'); 

    	$inventory_do = $this->InventoryDeliveryOrderItem->find('first', array(
    		'conditions' => array(
    			'InventoryDeliveryOrderItem.id' => $id
    			)
    		));

    	$data_to_encode = $inventory_do['InventoryDeliveryOrderItem']['barcode'];
    
		$barcode=new BarcodeHelper();
		        
		// Generate Barcode data
		$barcode->barcode();
		$barcode->setType('C128');
		$barcode->setCode($data_to_encode);
		$barcode->setSize(80,200);
		    
		// Generate filename            
		$random = rand(0,1000000);
		$file = 'img/Barcodes/'.$data_to_encode.'.png';
		    
		// Generates image file on server            
		$barcode->writeBarcodeFile($file);
    	
    	$this->set(compact('inventory_do')); 
	}

	public function view($id = null) {
		if ($this->request->is('post')) {
			 
			$this->loadModel('InventoryItems');
			$this->loadModel('InventoryDeliveryOrderItems');
			$this->loadModel('InventorySupplierItem');

			if(!empty($this->request->data['InventoryDeliveryOrderFile'])){
			 
				$id = $this->request->data['InventoryDeliveryOrderFile']['id'];
				$this->InventoryDeliveryOrder->create();
				
				$data_do_attach = array(
					'id' => $id,
					'filename' => $this->request->data['InventoryDeliveryOrderFile']['filename'],
					'dir' => $this->request->data['InventoryDeliveryOrderFile']['dir'],
				);

				if($this->InventoryDeliveryOrder->save($data_do_attach)){
					 
					$this->Session->setFlash(__('The inventory delivery order has been update.'), 'success');
					return $this->redirect(array('action' => 'view', $id));
				}else{
					$this->Session->setFlash(__('The inventory delivery could not be update.'), 'error');
				}
			}


			if(!empty($this->request->data['InventoryDeliveryOrder'])){
				$this->InventoryDeliveryOrder->create();

				$data_do = array(
					'id' => $this->request->data['InventoryDeliveryOrder']['id'],
					'do_no' => $this->request->data['InventoryDeliveryOrder']['do_no']
				); 

				if($this->InventoryDeliveryOrder->save($data_do)){
					$this->Session->setFlash(__('The inventory delivery order has been update.'), 'success');
					return $this->redirect(array('action' => 'view', $this->request->data['InventoryDeliveryOrder']['id']));
				}else{
					$this->Session->setFlash(__('The inventory delivery order could not be update. Please, try again.'), 'error');
				}
			}

			if(!empty($this->request->data['InventoryDeliveryOrderItems'])){
				//print_r($_POST);
				//exit;
				if($this->request->data['InventoryDeliveryOrderItems']['rejected_quantity']=='0'){
					
					$this->InventoryDeliveryOrderItems->create();

					$get_po = $this->InventoryDeliveryOrder->query("SELECT * FROM inventory_delivery_orders WHERE id=".$this->request->data['InventoryDeliveryOrderItems']['id']);

					$po_no = $get_po[0]['inventory_delivery_orders']['po_no'];

					$get_quantity = $this->InventoryDeliveryOrderItems->query("SELECT * FROM inventory_delivery_order_items WHERE id=".$this->request->data['InventoryDeliveryOrderItems']['id_item']);

					$get_item = $this->InventoryItems->query("SELECT * FROM inventory_items WHERE id=".$get_quantity[0]['inventory_delivery_order_items']['inventory_item_id']);

					$barcode = $get_item[0]['inventory_items']['code'].'-'.$get_po[0]['inventory_delivery_orders']['po_no'];
				
					$quantity = $get_quantity[0]['inventory_delivery_order_items']['quantity'];
					$quantity_remaining = $get_quantity[0]['inventory_delivery_order_items']['quantity_remaining'];
					$quantity_delivered = $get_quantity[0]['inventory_delivery_order_items']['quantity_delivered'];
 
					$inventoryDeliveryOrderItemId = $get_quantity[0]['inventory_delivery_order_items']['id'];

					$remaining = $quantity_remaining - $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'];
					$delivered = $quantity_delivered + $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'];

					$data_do_item = array(
						'id' => $this->request->data['InventoryDeliveryOrderItems']['id_item'],
						'quantity_delivered' => $delivered,
						'quantity_remaining' => $remaining,
						'barcode'	=> $barcode,
						'location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_store_id'],
						'rack_id'	=> $this->request->data['InventoryDeliveryOrderItems']['inventory_rack_id'],
						'quantity_rejected' => $this->request->data['InventoryDeliveryOrderItems']['rejected_quantity']
					);
					
					$item_id = $get_quantity[0]['inventory_delivery_order_items']['inventory_item_id'];

					$item = $this->InventoryItems->query("SELECT * FROM inventory_items WHERE id=".$item_id);

					$supplier_id = $get_quantity[0]['inventory_delivery_order_items']['inventory_supplier_id'];
				
					$quantity_stored = $item[0]['inventory_items']['quantity'];

					$qc_inspect = $item[0]['inventory_items']['qc_inspect'];
					$name = $item[0]['inventory_items']['name'];
					$generalUnitId = $item[0]['inventory_items']['general_unit_id'];
				
					if($qc_inspect == '1'){
						$this->loadModel('InventoryQcItems'); 

						$this->InventoryQcItems->create();

						$data_qc = array(
							'po_no'								=> $po_no,
							'inventory_delivery_order_item_id'	=> $inventoryDeliveryOrderItemId,
							'name' 								=> $name,
							'created'							=> date('Y-m-d H:i:s'),
							'quantity'							=> $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'],
							'inventory_item_id'					=> $item_id,
							'general_unit_id'					=> $generalUnitId,
							'inventory_delivery_location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_delivery_location_id'],
							'location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_store_id'],
							'rack_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_rack_id'],
							'inventory_supplier_id' 			=> $supplier_id,
							'barcode' 							=> $barcode
						);

						$this->InventoryQcItems->save($data_qc);
					}else{
						$this->loadModel('InventoryStock');
				
						$this->InventoryStock->create();
				
						$data_stocks = array(
							'general_unit_id' => $generalUnitId,
							'inventory_item_id' => $item_id,
							'inventory_delivery_order_item_id' => $inventoryDeliveryOrderItemId,
							'quantity' => $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'],
							'qc_status' => '1',
							'created'	=> date('Y-m-d H:i:s'),
							'inventory_delivery_location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_delivery_location_id'],
							'location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_store_id'],
							'inventory_rack_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_rack_id'],
							'inventory_supplier_id' 			=> $supplier_id,
							'barcode' => $barcode
						);
					
						$this->InventoryStock->save($data_stocks);

						$new_quantity = $quantity_stored + $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'];

						$this->InventoryItems->create();

						$data_items = array(
							'id'		=> $item_id,
							'quantity' 	=> $new_quantity
						);

						$this->InventoryItems->save($data_items);
					}

					if($this->InventoryDeliveryOrderItems->save($data_do_item)){
						$this->Session->setFlash(__('The inventory delivery order has been update.'), 'success');
						return $this->redirect(array('action' => 'view', $this->request->data['InventoryDeliveryOrderItems']['id']));
					}else{
						$this->Session->setFlash(__('The inventory delivery order could not be update. Please, try again.'), 'error');
					}
				}else{
					//print_r($_POST);
					$quantity_stock = $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'] - $this->request->data['InventoryDeliveryOrderItems']['rejected_quantity'];

					$this->InventoryDeliveryOrderItems->create();

					$get_po = $this->InventoryDeliveryOrder->query("SELECT * FROM inventory_delivery_orders WHERE id=".$this->request->data['InventoryDeliveryOrderItems']['id']);

					$po_no = $get_po[0]['inventory_delivery_orders']['po_no'];

					$get_quantity = $this->InventoryDeliveryOrderItems->query("SELECT * FROM inventory_delivery_order_items WHERE id=".$this->request->data['InventoryDeliveryOrderItems']['id_item']);

					$get_item = $this->InventoryItems->query("SELECT * FROM inventory_items WHERE id=".$get_quantity[0]['inventory_delivery_order_items']['inventory_item_id']);

					$barcode = $get_item[0]['inventory_items']['code'].'-'.$get_po[0]['inventory_delivery_orders']['po_no'];
				
					$quantity = $get_quantity[0]['inventory_delivery_order_items']['quantity'];
					$quantity_remaining = $get_quantity[0]['inventory_delivery_order_items']['quantity_remaining'];
					$quantity_delivered = $get_quantity[0]['inventory_delivery_order_items']['quantity_delivered'];
 
					$inventoryDeliveryOrderItemId = $get_quantity[0]['inventory_delivery_order_items']['id'];

					$remaining = $quantity_remaining - $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'];
					$delivered = $quantity_delivered + $this->request->data['InventoryDeliveryOrderItems']['quantity_delivered'];

					$data_do_item = array(
						'id' => $this->request->data['InventoryDeliveryOrderItems']['id_item'],
						'quantity_delivered' => $delivered,
						'quantity_remaining' => $remaining,
						'barcode'	=> $barcode,
						'location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_store_id'],
						'rack_id'	=> $this->request->data['InventoryDeliveryOrderItems']['inventory_rack_id'],
						'quantity_rejected' => $this->request->data['InventoryDeliveryOrderItems']['rejected_quantity']
					);

					$item_id = $get_quantity[0]['inventory_delivery_order_items']['inventory_item_id'];

					$item = $this->InventoryItems->query("SELECT * FROM inventory_items WHERE id=".$item_id);

					$supplier_id = $get_quantity[0]['inventory_delivery_order_items']['inventory_supplier_id'];
				
					$quantity_stored = $item[0]['inventory_items']['quantity'];

					$qc_inspect = $item[0]['inventory_items']['qc_inspect'];
					$name = $item[0]['inventory_items']['name'];
					$generalUnitId = $item[0]['inventory_items']['general_unit_id'];

					if($qc_inspect == '1'){
						$this->loadModel('InventoryQcItems');
						$this->InventoryQcItems->create();

						$data_qc = array(
							'po_no'								=> $po_no,
							'inventory_delivery_order_item_id'	=> $inventoryDeliveryOrderItemId,
							'name' 								=> $name,
							'created'							=> date('Y-m-d H:i:s'),
							'quantity'							=> $quantity_stock,
							'inventory_item_id'					=> $item_id,
							'general_unit_id'					=> $generalUnitId,
							'inventory_delivery_location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_delivery_location_id'],
							'location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_store_id'],
							'rack_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_rack_id'],
							'inventory_supplier_id' 			=> $supplier_id,
							'barcode' 							=> $barcode
						);

						$this->InventoryQcItems->save($data_qc);
					}else{
						$this->loadModel('InventoryStock');
				
						$this->InventoryStock->create();
				
						$data_stocks = array(
							'general_unit_id' => $generalUnitId,
							'inventory_item_id' => $item_id,
							'inventory_delivery_order_item_id' => $inventoryDeliveryOrderItemId,
							'quantity' => $quantity_stock,
							'qc_status' => '1',
							'created'	=> date('Y-m-d H:i:s'),
							'inventory_delivery_location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_delivery_location_id'],
							'location_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_store_id'],
							'inventory_rack_id' => $this->request->data['InventoryDeliveryOrderItems']['inventory_rack_id'],
							'inventory_supplier_id' 			=> $supplier_id,
							'barcode' => $barcode
						);
					
						$this->InventoryStock->save($data_stocks);

						$new_quantity = $quantity_stored + $quantity_stock;

						$this->InventoryItems->create();

						$data_items = array(
							'id'		=> $item_id,
							'quantity' 	=> $new_quantity
						);

						$this->InventoryItems->save($data_items);
					}

					$this->loadModel('InventoryStockRejected');
					$this->InventoryStockRejected->create();

					$data_reject = array(
						'general_unit_id' => $generalUnitId,
						'inventory_item_id' => $item_id,
						'inventory_delivery_order_item_id' => $inventoryDeliveryOrderItemId,
						'quantity' => $this->request->data['InventoryDeliveryOrderItems']['rejected_quantity'],
						'finished_good_comment_id' => $this->request->data['InventoryDeliveryOrderItems']['rejected_remark'],
						'inventory_supplier_id' => $supplier_id,
						'type' => '2'
					);

					if($this->InventoryStockRejected->save($data_reject)){
						if($this->InventoryDeliveryOrderItems->save($data_do_item)){
							$this->Session->setFlash(__('The inventory delivery order has been update.'), 'success');
							return $this->redirect(array('action' => 'view', $this->request->data['InventoryDeliveryOrderItems']['id']));
						}else{
							$this->Session->setFlash(__('The inventory delivery order could not be update. Please, try again.'), 'error');
						}
					}else{
						$this->Session->setFlash(__('The inventory delivery order could not be update. Please, try again.'), 'error');
					}
					
				}
			}

		}

		$this->loadModel('InventorySupplierItems');
		
        $inventory_item = $this->InventorySupplierItems->find('all', array(
            'joins'     =>  array(
                                array(
                                    'table' => 'inventory_items',
                                    'alias' => 'invitem',
                                    'type' => 'INNER',
                                    'conditions' => array(
                                                        'invitem.id = InventorySupplierItems.inventory_item_id'
                                                    )
                                    )
                                ),
            //'conditions' => array('InventorySupplierItems.inventory_supplier_id' => $id_supplier),
            'fields' => array('invitem.*', 'InventorySupplierItems.*')  
        ));

		$this->loadModel('GeneralUnit');
		$generalUnits = $this->GeneralUnit->find('list');

		$this->loadModel('InventorySupplier');
		$this->loadModel('State');
		$this->loadModel('Country');
		$states = $this->State->find('list');
		$countries = $this->Country->find('list');

		$do_array = array();
		$deliveryorders = $this->InventoryDeliveryOrder->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrder.inventory_purchase_order_id' => $id
			)
		));
		//print_r($deliveryorders);
		//exit;
		foreach ($deliveryorders as $deliveryorder) {
			$do_array[] = array(
				'InventoryDeliveryOrder' => $deliveryorder['InventoryDeliveryOrder'],
				'InventoryPurchaseOrder' => $this->get_porder($deliveryorder['InventoryDeliveryOrder']['inventory_purchase_order_id']),
				'User' => $deliveryorder['User'],
				'Group' => $deliveryorder['Group'],
				'InventorySupplier' => $deliveryorder['InventorySupplier'],
				'InventoryDeliveryOrderItem' => $this->getdeliveryorderitem($deliveryorder['InventoryDeliveryOrder']['id']),
				'InventoryPayment' => $deliveryorder['InventoryPayment'],
				'StockRejected' => $this->get_reject($deliveryorder['InventoryDeliveryOrder']['po_no']),
			);
		}

		//$options = array('conditions' => array('InventoryDeliveryOrder.' . $this->InventoryDeliveryOrder->primaryKey => $id));
		$this->set('inventoryDeliveryOrder', $do_array);

		//$dLocation = $this->InventoryDeliveryOrder->find('first', array('conditions'=> array('InventoryDeliveryOrder.id' => $id)));
		//print_r($dLocation);
		//exit;
		
		//$this->loadModel('InventoryDeliveryLocation');
        //$inventory_delivery_location = $this->InventoryDeliveryLocation->query("SELECT * FROM inventory_delivery_locations WHERE id=".$dLocation['InventoryDeliveryOrder']['inventory_delivery_location_id']);

        //$this->loadModel('InventoryStore');
        //$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE inventory_delivery_location_id=".$inventory_delivery_location[0]['inventory_delivery_locations']['id']);

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('FinishedGoodComment');
        $finishedgoodcomment = $this->FinishedGoodComment->find('list');

        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

		$sts = array('1'=>'Approved', '2'=>'Rejected');
		$this->set(compact('inventory_item', 'generalUnits', 'sts', 'supplier', 'states', 'countries', 'inventory_delivery_location', 'store', 'remark', 'warranty', 'terms', 'finishedgoodcomment'));
	}

	private function get_porder($id){
		$this->loadModel('InventoryPurchaseOrder');
		$po = $this->InventoryPurchaseOrder->find('all', array(
			'conditions' => array(
				'InventoryPurchaseOrder.id' => $id
			),
			'recursive' => -1
		));

		if(empty($po)){
			return $po;
		}else{
			return $po[0]['InventoryPurchaseOrder'];
		}
	}

	public function ajaxupdatestatus(){
		$po_id = $_POST['po_id'];
		$this->loadModel('InventoryPurchaseOrder');

		$update_po = array(
			'id' => $po_id,
			'purchase_status' => 2
		);

		$json = array();
		if($this->InventoryPurchaseOrder->save($update_po)){
			$json[] = array(
				'Mesej' => 'Closed PO'
			);
		}else{
			$json[] = array(
				'Mesej' => 'Error'
			);
		}

		echo json_encode($json);
		exit;

	}

	public function print_grn($id = null) {
		$this->layout = 'print'; 
	 
		$this->loadModel('InventoryDeliveryOrder');
		$inventoryDeliveryOrder = $this->InventoryDeliveryOrder->find('first', array(
			'conditions' => array(
				'InventoryDeliveryOrder.id' => $id
			)
		));
		
		$items = $this->InventoryDeliveryOrder->InventoryDeliveryOrderItem->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrderItem.inventory_delivery_order_id' => $id
				)
			));
		
		$this->set('items', $items); 
		$this->set('inventoryDeliveryOrder', $inventoryDeliveryOrder); 
	}

	public function view_grn($id = null){
		if ($this->request->is('post')) {
			$this->loadModel('InventoryStockRejected');
			$this->InventoryStockRejected->create();

			$data_st = array(
				'id' => $this->request->data['DeliveryNotes']['stock_rejected_id'],
				'actions' => $this->request->data['DeliveryNotes']['rejected_action']
			);

			if($this->InventoryStockRejected->save($data_st)){
				$this->Session->setFlash(__('The inventory delivery order has been updated.'), 'success');
				return $this->redirect(array('action' => 'view_grn', $id));
			}else{
				$this->Session->setFlash(__('The inventory delivery order could not update.'), 'error');
			}
		}
		$this->loadModel('InventoryDeliveryOrder');
		$inventoryDeliveryOrder = $this->InventoryDeliveryOrder->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrder.id' => $id
			)
		));
		$arr[] = array(

			'InventoryDeliveryOrder' => $inventoryDeliveryOrder[0]['InventoryDeliveryOrder'],
			'InventoryDeliveryOrderItem' => $this->get_do_item($inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['id']),
			'InventorySupplier'	=> $inventoryDeliveryOrder[0]['InventorySupplier'],
		);

		$this->loadModel('InventoryPurchaseOrder');
		$inventoryPurchaseOrder = $this->InventoryPurchaseOrder->find('all', array(
			'conditions' => array(
				'InventoryPurchaseOrder.id' => $inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['inventory_purchase_order_id'],
			)
		));

		$this->set('inventoryDeliveryOrder', $arr);
		$this->set('inventoryPurchaseOrder', $inventoryPurchaseOrder);
	}
	private function get_do_item($id){
		$this->loadModel("InventoryDeliveryOrderItem");
		$items = $this->InventoryDeliveryOrderItem->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrderItem.inventory_delivery_order_id' => $id
			)
		));
		$arr = array();
		foreach ($items as $item) {
			$arr[] = array(
				'InventoryDeliveryOrderItem' => $item['InventoryDeliveryOrderItem'],
				'InventoryItem'	=> $item['InventoryItem'],
				'FinishedGoodComment' => $item['FinishedGoodComment'],
				'GeneralUnit' => $item['GeneralUnit'],
				'Store' => $this->get_store($item['InventoryDeliveryOrderItem']['store_id']),
				'Rack' => $this->get_rack($item['InventoryDeliveryOrderItem']['rack_id']),
				'InventoryQcItem' => $this->get_qc_item($item['InventoryDeliveryOrderItem']['id'])
			);
		}

		return $arr;
	}

	private function get_store($id){
		$this->loadModel('InventoryStore');
		$store = $this->InventoryStore->find('first', array(
			'conditions' => array(
				'InventoryStore.id' => $id
			),
			'recursive' => -1
		));
		if(!empty($store)) {
			return $store['InventoryStore'];
		} 
	}

	private function get_rack($id){
		$this->loadModel('InventoryRack');
		$rack = $this->InventoryRack->find('first', array(
			'conditions' => array(
				'InventoryRack.id' => $id
			),
			'recursive' => -1
		));
		if(!empty($rack)) {
			return $rack['InventoryRack'];
		}
	}

	private function get_qc_item($id){
		$this->loadModel('InventoryQcItem');
		$qcitem = $this->InventoryQcItem->find('all', array(
			'conditions' => array(
				'InventoryQcItem.inventory_delivery_order_item_id' => $id
			),
			'recursive' => -1
		));

		if(!$qcitem){
			$arr = '';
		}else{

			$arr[] = array(
				'InventoryQcItem' => $qcitem[0]['InventoryQcItem'],
				'InventoryQcRemark' => $this->getqcremark($qcitem[0]['InventoryQcItem']['id']),
				'InventoryStockRejected' => $this->getstockrejected($qcitem[0]['InventoryQcItem']['id']),
			);
		}

		return $arr;
	}

	private function getqcremark($id){
		$this->loadModel('InventoryQcRemark');
		$qcremark = $this->InventoryQcRemark->find('all', array(
			'conditions' => array(
				'InventoryQcRemark.inventory_qc_item_id' => $id
			),
			'recursive' => -1
		));
		if(empty($qcremark)){
			return $qcremark;
		}else{
			return $qcremark[0]['InventoryQcRemark'];
		}
	}

	private function getstockrejected($id){
		$this->loadModel('InventoryStockRejected');
		$stockrejected = $this->InventoryStockRejected->find('all', array(
			'conditions' => array(
				'InventoryStockRejected.inventory_qc_item_id' => $id
			),
			'recursive' => -1
		));

		if(empty($stockrejected)){
			return $stockrejected;
		}else{
			return $stockrejected[0]['InventoryStockRejected'];
		}
	}

	private function get_reject($po){
		$this->loadModel('InventoryStockRejected');
		$this->loadModel('InventoryQcItems');

		$reject_array = array();
		$rejects = $this->InventoryStockRejected->find('all', array(
			'joins' => array(
	            array(
	                'table' => 'inventory_qc_items',
	                'alias' => 'inventoryqcitem',
	                'type' => 'INNER',
	                'conditions' => array(
	                                    'inventoryqcitem.id = InventoryStockRejected.inventory_qc_item_id'
	                                )
	                )
	            ),
			'conditions' => array(
				'inventoryqcitem.po_no' => $po
			),
			'recursive' => -1
		));

		foreach ($rejects as $reject) {
			$reject_array[] = array(
				'InventoryStockRejected' => $reject['InventoryStockRejected'],
				'Remark' => $this->get_reject_remark($reject['InventoryStockRejected']['inventory_qc_item_id'])
			);
		}

		return $reject_array;

	}

	private function get_reject_remark($qc_id){
		$this->loadModel('InventoryQcRemark');
		$remark = $this->InventoryQcRemark->find('all', array(
			'conditions' => array(
				'InventoryQcRemark.inventory_qc_item_id' => $qc_id
			),
			'recursive' => -1
		));

		return $remark;
	}

	private function getdeliveryorderitem($id){
		$this->loadModel('InventoryDeliveryOrderItems');

		$item_array = array();
		$items = $this->InventoryDeliveryOrderItems->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrderItems.inventory_delivery_order_id' => $id
			)
		));
		foreach ($items as $item) {
			$item_array[] = array(
				'InventoryDeliveryOrderItems' => $item['InventoryDeliveryOrderItems'],
				'InventoryItem' => $this->getinvitem($item['InventoryDeliveryOrderItems']['inventory_item_id'])
			);
		}

		return $item_array;
	}

	private function getinvitem($id){
		$this->loadModel('InventoryItem');
		$invitem_array = array();
		$invitem = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			)
		));

		$invitem_array[] = array(
			'InventoryItem' => $invitem[0]['InventoryItem'],
			'InventoryItemCategory' => $invitem[0]['InventoryItemCategory'],
			'GeneralUnit' => $invitem[0]['GeneralUnit']
		);

		return $invitem_array;

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('InventorySupplier');
		$this->loadModel('InventoryPurchaseOrder');
		$this->loadModel('User');
		$this->loadModel('GeneralPaymentTerm');
		if ($this->request->is('post')) {
			//print_r($_POST);
			//print_r($_FILES);
			//exit; 
			$this->loadModel('InventoryPurchaseOrderItem');
			$this->loadModel('InventoryItem');

			$po_id = $this->request->data['InventoryDeliveryOrder']['puchase_order_id'];

			$get_po = $this->InventoryPurchaseOrder->find('all', array(
				'conditions' => array(
					'InventoryPurchaseOrder.id' => $this->request->data['InventoryDeliveryOrder']['puchase_order_id'],
				),
				'recursive' => -1
			));

			$this->InventoryDeliveryOrder->create();

			$bil = $this->InventoryDeliveryOrder->find('first', array(
				'recursive' => -1,
				'order' => 'InventoryDeliveryOrder.id DESC'
			));

			if(empty($bil)){
				$bil = '1';
			}else{
				$bil = $bil['InventoryDeliveryOrder']['id'] + 1;
			}

			$grn_no = $this->generate_code('GRN', $bil);

			$this->request->data['InventoryDeliveryOrder'] = array(
				'user_id'							=> $_SESSION['Auth']['User']['id'],
				'group_id'							=> $_SESSION['Auth']['User']['group_id'],
				'created'							=> date('Y-m-d H:i:s'),
				'received_date'						=> $this->request->data['InventoryDeliveryOrder']['received_date'],
				'po_no'								=> $get_po[0]['InventoryPurchaseOrder']['po_no'],
				'inventory_purchase_order_id'		=> $get_po[0]['InventoryPurchaseOrder']['id'],
				'inventory_supplier_id'				=> $get_po[0]['InventoryPurchaseOrder']['inventory_supplier_id'],
				'inventory_delivery_location_id'	=> $get_po[0]['InventoryPurchaseOrder']['inventory_delivery_location_id'],
				'do_no'								=> $this->request->data['InventoryDeliveryOrder']['delivery_order_no'],
				'grn_no' 							=> $grn_no,
				'filename'							=> $this->request->data['InventoryDeliveryOrder']['filename'],
				'dir'								=> $this->request->data['InventoryDeliveryOrder']['dir'],
				'status'						    => 1
			);

			if ($this->InventoryDeliveryOrder->save($this->request->data)) {
				$lastID = $this->InventoryDeliveryOrder->getLastInsertID();
				$this->loadModel('InventoryDeliveryOrderItem');

				$poitemid = $_POST['purchase_order_item_id'];
				for($i=0; $i < count($poitemid); $i++) {
					$this->InventoryDeliveryOrderItem->create();
					$get_po_item = $this->InventoryPurchaseOrderItem->find('all', array(
						'conditions' => array(
							'InventoryPurchaseOrderItem.id' => $poitemid[$i],
						),
						'recursive' => -1
					));

					$codes = $this->InventoryItem->find('all', array(
						'conditions' => array(
							'InventoryItem.id' => $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_item_id']
						),
						'recursive' => -1
					));
					
					$code = $codes[0]['InventoryItem']['code'];
					$qc_inspect = $codes[0]['InventoryItem']['qc_inspect'];
					$name = $codes[0]['InventoryItem']['name'];

					if($qc_inspect == '1') {
						$data_do_item = array(
							'inventory_delivery_order_id' 	=> $lastID,
							'quantity'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['quantity'],
							'general_unit_id'				=> $get_po_item[0]['InventoryPurchaseOrderItem']['general_unit_id'],
							'inventory_item_id' 			=> $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_item_id'],
							'inventory_supplier_item_id'	=> $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_supplier_item_id'],
							'price_per_unit'				=> $get_po_item[0]['InventoryPurchaseOrderItem']['price_per_unit'],
							'amount'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['amount'],
							'discount'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['discount'],
							'discount_type'					=> $get_po_item[0]['InventoryPurchaseOrderItem']['discount_type'],
							'tax'							=> $get_po_item[0]['InventoryPurchaseOrderItem']['tax'],
							'quantity_delivered'			=> $_POST['q_delivered'][$i],
							'barcode'						=> $code.'-'.$grn_no,
							'store_id'						=> $_POST['store_id'][$i],
							'rack_id'						=> $_POST['rack_id'][$i],
							'quantity_rejected'				=> $_POST['q_rejected'][$i],
							'finished_good_comment_id'		=> $_POST['rejected_remark'][$i],
							'quantity_accepted'				=> $_POST['q_accepted'][$i],
							'inventory_purchase_order_item_id' => $_POST['inventory_purchase_order_item_id'][$i],
						);
					}else{
						$data_do_item = array(
							'inventory_delivery_order_id' 	=> $lastID,
							'quantity'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['quantity'],
							'general_unit_id'				=> $get_po_item[0]['InventoryPurchaseOrderItem']['general_unit_id'],
							'inventory_item_id' 			=> $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_item_id'],
							'inventory_supplier_item_id'	=> $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_supplier_item_id'],
							'price_per_unit'				=> $get_po_item[0]['InventoryPurchaseOrderItem']['price_per_unit'],
							'amount'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['amount'],
							'discount'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['discount'],
							'discount_type'					=> $get_po_item[0]['InventoryPurchaseOrderItem']['discount_type'],
							'tax'							=> $get_po_item[0]['InventoryPurchaseOrderItem']['tax'],
							'quantity_delivered'			=> $_POST['q_delivered'][$i],
							'barcode'						=> $code.'-'.$grn_no,
							'store_id'						=> $_POST['store_id'][$i],
							'rack_id'						=> $_POST['rack_id'][$i],
							'quantity_rejected'				=> $_POST['q_rejected'][$i],
							'finished_good_comment_id'		=> $_POST['rejected_remark'][$i],
							'quantity_accepted'				=> $_POST['q_accepted'][$i],
							'inventory_purchase_order_item_id' => $_POST['inventory_purchase_order_item_id'][$i],
						);
					}

					$this->InventoryDeliveryOrderItem->save($data_do_item);

					$lastID_item = $this->InventoryDeliveryOrderItem->getLastInsertID();

					if($qc_inspect == '1') {
						$this->loadModel('InventoryQcItems');
 

						$data_update_do = array(
							'id' => $lastID,
							'created' => date('Y-m-d H:i:s'),
							'status' => '0'
						);

						$this->InventoryDeliveryOrder->save($data_update_do);

						//iF QTY > 0
						if($_POST['q_accepted'][$i] > 0) {
							$this->InventoryQcItems->create(); 
							$data_qc = array(
								'po_no'								=> $get_po[0]['InventoryPurchaseOrder']['po_no'],
								'inventory_delivery_order_item_id'	=> $lastID_item,
								'name' 								=> $name,
								'created'							=> date('Y-m-d H:i:s'),
								'quantity'							=> $_POST['q_accepted'][$i],
								'inventory_item_id'					=> $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_item_id'],
								'general_unit_id'					=> $get_po_item[0]['InventoryPurchaseOrderItem']['general_unit_id'],
								'inventory_delivery_location_id' 	=> $get_po[0]['InventoryPurchaseOrder']['inventory_delivery_location_id'],
								'location_id' 						=> $_POST['store_id'][$i],
								'rack_id' 							=> $_POST['rack_id'][$i],
								'inventory_supplier_id' 			=> $get_po[0]['InventoryPurchaseOrder']['inventory_supplier_id'],
								'barcode' 							=> $code.'-'.$grn_no,
								'status' 							=> 0
							);

							$this->InventoryQcItems->save($data_qc); 
							$deliver = $_POST['q_delivered'][$i]+$get_po_item[0]['InventoryPurchaseOrderItem']['received_quantity'];
							$accept = $_POST['q_delivered'][$i]+$get_po_item[0]['InventoryPurchaseOrderItem']['accepted_quantity'];
							$reject = $_POST['q_rejected'][$i]+$get_po_item[0]['InventoryPurchaseOrderItem']['rejected_quantity'];

							if($get_po_item[0]['InventoryPurchaseOrderItem']['quantity'] == $accept){
								$data_po_item = array(
									'id' 				=> $poitemid[$i],
									'received_quantity' => $deliver,
									'accepted_quantity' => $accept,
									'status' 			=> 1,  
								);
							} else {
								$data_po_item = array(
									'id' 				=> $poitemid[$i],
									'received_quantity' => $deliver,
									'accepted_quantity' => $accept,
									'status' 			=> 0,
								);
							}

							$this->InventoryPurchaseOrderItem->save($data_po_item);	
						} 
					} else {

						if($_POST['store_id'][$i] != 0 && $_POST['rack_id'][$i] != 0) {


							$this->loadModel('InventoryStock');

							$this->InventoryStock->create();
					
							$data_stocks = array(
								'general_unit_id' 					=> $get_po_item[0]['InventoryPurchaseOrderItem']['general_unit_id'],
								'inventory_item_id' 				=> $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_item_id'],
								'inventory_delivery_order_item_id' 	=> $lastID_item,
								'price_per_unit'					=> $get_po_item[0]['InventoryPurchaseOrderItem']['price_per_unit'],
								'qty_in' 							=> $_POST['q_delivered'][$i],
								'quantity' 							=> $_POST['q_delivered'][$i],
								'qc_status' 						=> '1',
								'created'							=> date('Y-m-d H:i:s'),
								'inventory_delivery_location_id'	=> $get_po[0]['InventoryPurchaseOrder']['inventory_delivery_location_id'],
								'inventory_store_id' 	            => $_POST['store_id'][$i],
	 
								'inventory_rack_id'                 => $_POST['rack_id'][$i],

								'inventory_supplier_id' 			=> $get_po[0]['InventoryPurchaseOrder']['inventory_supplier_id'],
								'barcode' 							=> $code.'-'.$grn_no
							);
 

							if($this->InventoryStock->save($data_stocks)) {
								$lastID_stock = $this->InventoryStock->getLastInsertID();

								$this->loadModel('InventoryMaterialHistory');

								$sum = $this->InventoryMaterialHistory->query("SELECT SUM(quantity) AS total FROM inventory_material_histories WHERE inventory_item_id=".$get_po_item[0]['InventoryPurchaseOrderItem']['inventory_item_id']);
								$this->loadModel('InventoryMaterialHistory');

								$sum = $sum[0][0]['total'];
								$balance = $sum + $_POST['q_delivered'][$i];

								$this->InventoryMaterialHistory->create();

								$data_material_history = array(
									'reference'								=> $grn_no, 
									'user_id'								=> $_SESSION['Auth']['User']['id'],
									'store_pic'								=> $_SESSION['Auth']['User']['id'],
									'inventory_item_id'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['inventory_item_id'],
									'inventory_stock_id'					=> $lastID_stock,
									'quantity'								=> $_POST['q_delivered'][$i],
									'general_unit_id'						=> $get_po_item[0]['InventoryPurchaseOrderItem']['general_unit_id'],
									'issued_quantity'						=> 0,
									'quantity_balance'						=> $balance,
									'inventory_material_request_id'			=> 0,
									'inventory_material_request_item_id'	=> 0,
									'created'								=> date('Y-m-d H:i:s'),
									'status'								=> 1,
									'type'									=> 6,
									'transfer_type'							=> 'In',
								);

								$this->InventoryMaterialHistory->save($data_material_history);
							} 
						}

						$this->loadModel('InventoryPurchaseOrderItem');

						$deliver = $_POST['q_delivered'][$i]+$get_po_item[0]['InventoryPurchaseOrderItem']['received_quantity'];
						$accept = $_POST['q_delivered'][$i]+$get_po_item[0]['InventoryPurchaseOrderItem']['accepted_quantity'];
						$reject = $_POST['q_rejected'][$i]+$get_po_item[0]['InventoryPurchaseOrderItem']['rejected_quantity'];

						if($get_po_item[0]['InventoryPurchaseOrderItem']['quantity'] == $accept) {
							$data_po_item = array(
								'id' 				=> $poitemid[$i],
								'received_quantity' => $deliver,
								'accepted_quantity' => $accept,
								'status' 			=> 1,
							);
						} else {
							$data_po_item = array(
								'id' 				=> $poitemid[$i],
								'received_quantity' => $deliver,
								'accepted_quantity' => $accept,
								'status' 			=> 0,
							);
						}

						$this->InventoryPurchaseOrderItem->save($data_po_item);
					}

				}

				// Closed PO when complete
				/*
				UPDATE PO Status 
				Compare status 0 - 1
				$po_id
				*/ 
				$delivered = $this->delivered_qty($po_id);

				$all = $this->purchased_qty($po_id);

				// delivered qty = purchased qty then mark PO as delivered
				if($delivered > 0) {
					// if complete receive
					if($delivered >= $all) {
						$po_status = 2;
					} else {
						$po_status = 1;
					}
					$update_status = $this->InventoryPurchaseOrder->updateAll(
						array(
							'InventoryPurchaseOrder.status' => "'".$po_status."'"
							),
						array(
							'InventoryPurchaseOrder.id' => $po_id
							) 
						);
					if(!$update_status) {
						die('1008');
					}
				}
				

				$this->Session->setFlash(__('The Good Receive Note has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Good Receive Note could not be saved. Please, try again.'), 'error');
			}

		}
		$inventorySuppliers = $this->InventoryDeliveryOrder->InventorySupplier->find('list', array('group'=>'InventorySupplier.name', 'order'=>'InventorySupplier.name ASC'));
		$inventoryPurchaseOrders = $this->InventoryPurchaseOrder->find('all', array(
			'conditions' => array(
				'InventoryPurchaseOrder.purchase_status' => 1
				)
			));
		$arr = array();
		foreach($inventoryPurchaseOrders as $po){
			$arr[] = array(
				'InventoryPurchaseOrder' => $po['InventoryPurchaseOrder'],
				'InventoryPurchaseOrderItem' => $this->get_po_item($po['InventoryPurchaseOrder']['id'])
			);
		}
		$inventoryPurchaseOrders = $arr;
		$arr_2 = array();
		//print_r($inventoryPurchaseOrders);
		//exit;
		
		foreach ($inventoryPurchaseOrders as $item) {
			$q_order = 0;
			$q_accept = 0;
			foreach ($item['InventoryPurchaseOrderItem'] as $item_2) {
				$q_order = $q_order + $item_2['InventoryPurchaseOrderItem']['quantity'];
				$q_accept = $q_accept + $item_2['InventoryPurchaseOrderItem']['accepted_quantity'];

				if($q_order == $q_accept){
					$status = 'Closed';
				}else{
					$status = 'Open';
				}
			}

			$arr_2[] = array(
				'InventoryPurchaseOrder' => $item['InventoryPurchaseOrder'],
				'InventorySupplier' => $this->get_supplier($item['InventoryPurchaseOrder']['inventory_supplier_id']),
				'Status' => $status,
			); 
		}

		$inventoryPurchaseOrders = $arr_2;
		//print_r($arr_2);
		//exit;

		$this->loadModel('InventoryLocation');
		$locations = $this->InventoryLocation->find('list', array('order' => 'InventoryLocation.name ASC'));


		$this->loadModel('FinishedGoodComment');
		$remark = $this->FinishedGoodComment->find('list', array('order'=>'FinishedGoodComment.name ASC'));

		$users = $this->User->find('list');
		$generalPaymentTerms = $this->GeneralPaymentTerm->find('list');
		$this->set(compact('inventorySuppliers', 'inventoryPurchaseOrders', 'users', 'generalPaymentTerms', 'remark', 'locations'));
	}

	private function delivered_qty($po_id) {
		$this->loadModel('InventoryDeliveryOrderItem');
		$items = $this->InventoryDeliveryOrderItem->find('all', array(
			'fields' => array('SUM(InventoryDeliveryOrderItem.quantity_accepted) AS order_qty'),
			'conditions' => array(
				'InventoryDeliveryOrder.inventory_purchase_order_id' => $po_id 
				) 
			));
		if(!empty($items)) {
			foreach ($items as $item) {
				return $item[0]['order_qty'];
			}
		} else {
			return 0;
		}
	}

	private function purchased_qty($po_id) {
		$this->loadModel('InventoryPurchaseOrderItem');
		$items = $this->InventoryPurchaseOrderItem->find('all', array(
			'fields' => array('SUM(InventoryPurchaseOrderItem.quantity) AS order_qty'),
			'conditions' => array(
				'InventoryPurchaseOrderItem.inventory_purchase_order_id' => $po_id 
				),
			'recursive' => -1
			));
		if(!empty($items)) {
			foreach ($items as $item) {
				return $item[0]['order_qty'];
			}
		} else {
			return 0;
		}
	}

	private function get_supplier($id){
		$this->loadModel('InventorySupplier');
		$supplier = $this->InventorySupplier->find('all', array(
			'conditions' => array(
				'InventorySupplier.id' => $id
			),
			'recursive' => -1
		));

		if(empty($supplier)){
			return $supplier;
		}else{
			return $supplier[0]['InventorySupplier'];
		}
	}

	private function get_po_item($id){
		$this->loadModel('InventoryPurchaseOrderItem');
		$item = $this->InventoryPurchaseOrderItem->find('all', array(
			'conditions' => array(
				'InventoryPurchaseOrderItem.inventory_purchase_order_id' => $id
			),
			'recursive' => -1
		)); 
		return $item; 
	}

	public function closed($id = null) {
		if($id != ''){
			$this->InventoryDeliveryOrder->create();

			$do_update = array(
				'id'				=> $id,
				'completed_status'  => '1'
			);
		}

		if($this->InventoryDeliveryOrder->save($do_update)){
			$this->Session->setFlash(__('The inventory delivery order has been closed.'), 'success');
			return $this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash(__('The inventory delivery order could not be closed. Please, try again.'), 'error');
		}
	}

	public function ajaxrack() {
		$id = $_POST['id'];
		$this->loadModel('InventoryRack');

		$rack = $this->InventoryRack->query("SELECT * FROM inventory_racks WHERE inventory_store_id=".$id);

		$json = array();

		for($i=0; $i<count($rack); $i++){
			$json[] = array('id' => $rack[$i]['inventory_racks']['id'], 'name' => $rack[$i]['inventory_racks']['name']);
		}

		echo json_encode($json);
		exit;
	}

	public function ajaxquantity() {
		$qty = $_POST['qty'];
		$id = $_POST['id'];
		$nqty = $_POST['nqty'];
		$do_no = $_POST['dono'];
		$s_id = $_POST['sid'];
		$n_qc = $_POST['n_qc'];
		$item_id = $_POST['i_id'];
		$g_unit = $_POST['gu_id'];
		$rquantity = $nqty - $qty;

		$this->loadModel('InventorySupplierItems');

		$item = $this->InventorySupplierItems->query("SELECT * FROM inventory_supplier_items WHERE id=".$s_id);

		if($n_qc == '1'){
			$this->loadModel('InventoryQcItems');
			$this->InventoryQcItems->create();

			$data_qc = array(
				'do_no'								=> $do_no,
				'inventory_delivery_order_item_id'	=> $id,
				'name' 								=> $item[0]['inventory_supplier_items']['name'],
				'created'							=> date('Y-m-d H:i:s'),
				'quantity'							=> $qty,
				'inventory_item_id'					=> $item_id,
				'general_unit_id'					=> $g_unit
			);

			$this->InventoryQcItems->save($data_qc);
		}else{
			$this->loadModel('InventoryStock');
			
			$this->InventoryStock->create();
			
				$data_stocks = array(
					'general_unit_id' => $g_unit,
					'inventory_item_id' => $item_id,
					'inventory_delivery_order_item_id' => $id,
					'quantity' => $qty,
					'qc_status' => '1'
				);
			
			$this->InventoryStock->save($data_stocks);

			$this->loadModel('InventoryItems');
			$q_item = $this->InventoryItems->query("SELECT * FROM inventory_items WHERE id=".$item_id);
			$this->InventoryItems->create();

			$new_quantity = $q_item[0]['inventory_items']['quantity'] + $qty;

			$data_items = array(
				'id'		=> $item_id,
				'quantity' 	=> $new_quantity
			);
			$this->InventoryItems->save($data_items);
		}

		$this->loadModel('InventoryDeliveryOrderItems');

		$delivered = $this->InventoryDeliveryOrderItems->query("SELECT quantity_delivered FROM inventory_delivery_order_items WHERE id=".$id);

		$q_delivered = $delivered[0]['inventory_delivery_order_items']['quantity_delivered'];

		$new_q = $qty + $q_delivered;

		$this->InventoryDeliveryOrderItems->create();

		$data_update_do = array(
			'id' => $id,
			'quantity_delivered' => $new_q,
			'quantity_remaining' => $rquantity
		);

		if($this->InventoryDeliveryOrderItems->save($data_update_do)){
			$status = 1;
		}else{
			$status = 0;
		}

		echo json_encode($status);
		exit;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryDeliveryOrder->exists($id)) {
			throw new NotFoundException(__('Invalid inventory delivery order'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryDeliveryOrder->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory delivery order has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory delivery order could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryDeliveryOrder.' . $this->InventoryDeliveryOrder->primaryKey => $id));
			$this->request->data = $this->InventoryDeliveryOrder->find('first', $options);
		}
		$inventorySuppliers = $this->InventoryDeliveryOrder->InventorySupplier->find('list');
		$inventoryPurchaseOrders = $this->InventoryDeliveryOrder->InventoryPurchaseOrder->find('list');
		$users = $this->InventoryDeliveryOrder->User->find('list');
		$generalPaymentTerms = $this->InventoryDeliveryOrder->GeneralPaymentTerm->find('list');
		$this->set(compact('inventorySuppliers', 'inventoryPurchaseOrders', 'users', 'generalPaymentTerms'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryDeliveryOrder->id = $id;
		if (!$this->InventoryDeliveryOrder->exists()) {
			throw new NotFoundException(__('Invalid inventory delivery order'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryDeliveryOrder->delete()) {
			$this->Session->setFlash(__('The inventory delivery order has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory delivery order could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
