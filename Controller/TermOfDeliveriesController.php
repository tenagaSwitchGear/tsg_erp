<?php
App::uses('AppController', 'Controller');
/**
 * TermOfDeliveries Controller
 *
 * @property TermOfDelivery $TermOfDelivery
 * @property PaginatorComponent $Paginator
 */
class TermOfDeliveriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TermOfDelivery->recursive = 0;
		$this->set('termOfDeliveries', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TermOfDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid term of delivery'));
		}
		$options = array('conditions' => array('TermOfDelivery.' . $this->TermOfDelivery->primaryKey => $id));
		$this->set('termOfDelivery', $this->TermOfDelivery->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TermOfDelivery->create();
			if ($this->TermOfDelivery->save($this->request->data)) {
				$this->Session->setFlash(__('The term of delivery has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The term of delivery could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TermOfDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid term of delivery'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TermOfDelivery->save($this->request->data)) {
				$this->Session->setFlash(__('The term of delivery has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The term of delivery could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TermOfDelivery.' . $this->TermOfDelivery->primaryKey => $id));
			$this->request->data = $this->TermOfDelivery->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TermOfDelivery->id = $id;
		if (!$this->TermOfDelivery->exists()) {
			throw new NotFoundException(__('Invalid term of delivery'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TermOfDelivery->delete()) {
			$this->Session->setFlash(__('The term of delivery has been deleted.'));
		} else {
			$this->Session->setFlash(__('The term of delivery could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
