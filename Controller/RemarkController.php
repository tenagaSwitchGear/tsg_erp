<?php
App::uses('AppController', 'Controller');
/**
 * Cities Controller
 *
 * @property City $City
 * @property PaginatorComponent $Paginator 
 */
class RemarkController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
/**
 * index method
 *
 * @return void
 */
 	public function beforeFilter() {
        parent::beforeFilter();
    }
 	
	public function index() {
		
		/* Filter With Paging */
        if(!empty($this->data)) {         
            $this->Session->write('Filter.City',$this->data);
        }
        elseif((!empty($this->passedArgs['page']) || strpos($this->referer(), 'cities')) && $this->Session->check('Filter.City')) {
            $this->request->data = $this->Session->check('Filter.City')? $this->Session->read('Filter.City') : '';
        }
        else
        {
            $this->Session->delete('Filter.City');
        }
        /* End Filter With Paging */
		
		
	
		$conditions = array();
		if(!empty($this->data)) {
			if(!empty($this->data['City']['name'])) {
				$conditions['City.name LIKE']='%'.$this->data['City']['name'].'%';
			}
			
		}
		
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'City.created DESC','limit'=>$record_per_page);
		$this->set('cities', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->City->exists($id)) {
			throw new NotFoundException(__('Invalid city'));
		}
		$options = array('conditions' => array('City.' . $this->City->primaryKey => $id));
		$this->set('city', $this->City->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->City->create();
			if ($this->City->save($this->request->data)) {
				$this->Session->setFlash(__('The city has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The city could not be saved. Please, try again.'),'error');
			}
		}
		
		$countries = $this->City->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->City->exists($id)) {
			throw new NotFoundException(__('Invalid city'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->City->save($this->request->data)) {
				$this->Session->setFlash(__('The city has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The city could not be saved. Please, try again.'),'error');
			}
		} else {
			$options = array('conditions' => array('City.' . $this->City->primaryKey => $id));
			$this->request->data = $this->City->find('first', $options);
		}
		if(!empty($this->data['City']['country_id']))
		{
			$states = $this->City->State->find('list',array('conditions'=>array('State.country_id'=>$this->data['City']['country_id'])));
			$this->set(compact('states'));
		}
		
		$countries = $this->City->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->City->id = $id;
		if (!$this->City->exists()) {
			throw new NotFoundException(__('Invalid city'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->City->delete()) {
			$this->Session->setFlash(__('The city has been deleted.'),'success');
		} else {
			$this->Session->setFlash(__('The city could not be deleted. Please, try again.'),'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
