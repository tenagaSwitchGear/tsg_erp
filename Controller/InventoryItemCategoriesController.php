<?php
App::uses('AppController', 'Controller');
/**
 * InventoryItemCategories Controller
 *
 * @property InventoryItemCategory $InventoryItemCategory
 * @property PaginatorComponent $Paginator
 */
class InventoryItemCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryItemCategory->recursive = 0;
		$this->set('inventoryItemCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryItemCategory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item category'));
		}
		$options = array('conditions' => array('InventoryItemCategory.' . $this->InventoryItemCategory->primaryKey => $id));
		$this->set('inventoryItemCategory', $this->InventoryItemCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryItemCategory->create();
			if ($this->InventoryItemCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory item category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory item category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryItemCategory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryItemCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory item category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory item category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryItemCategory.' . $this->InventoryItemCategory->primaryKey => $id));
			$this->request->data = $this->InventoryItemCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryItemCategory->id = $id;
		if (!$this->InventoryItemCategory->exists()) {
			throw new NotFoundException(__('Invalid inventory item category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryItemCategory->delete()) {
			$this->Session->setFlash(__('The inventory item category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory item category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
