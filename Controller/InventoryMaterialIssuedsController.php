<?php
App::uses('AppController', 'Controller');
/**
 * InventoryMaterialIssueds Controller
 *
 * @property InventoryMaterialIssued $InventoryMaterialIssued
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryMaterialIssuedsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryMaterialIssued->recursive = 0;
		$this->set('inventoryMaterialIssueds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryMaterialIssued->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material issued'));
		}
		$options = array('conditions' => array('InventoryMaterialIssued.' . $this->InventoryMaterialIssued->primaryKey => $id));
		$this->set('inventoryMaterialIssued', $this->InventoryMaterialIssued->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryMaterialIssued->create();
			if ($this->InventoryMaterialIssued->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material issued has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material issued could not be saved. Please, try again.'));
			}
		}
		$inventoryMaterialRequestItems = $this->InventoryMaterialIssued->InventoryMaterialRequestItem->find('list');
		$inventoryStocks = $this->InventoryMaterialIssued->InventoryStock->find('list');
		$inventoryItems = $this->InventoryMaterialIssued->InventoryItem->find('list');
		$productionOrders = $this->InventoryMaterialIssued->ProductionOrder->find('list');
		$generalUnits = $this->InventoryMaterialIssued->GeneralUnit->find('list');
		$users = $this->InventoryMaterialIssued->User->find('list');
		$this->set(compact('inventoryMaterialRequestItems', 'inventoryStocks', 'inventoryItems', 'productionOrders', 'generalUnits', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryMaterialIssued->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material issued'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryMaterialIssued->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material issued has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material issued could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryMaterialIssued.' . $this->InventoryMaterialIssued->primaryKey => $id));
			$this->request->data = $this->InventoryMaterialIssued->find('first', $options);
		}
		$inventoryMaterialRequestItems = $this->InventoryMaterialIssued->InventoryMaterialRequestItem->find('list');
		$inventoryStocks = $this->InventoryMaterialIssued->InventoryStock->find('list');
		$inventoryItems = $this->InventoryMaterialIssued->InventoryItem->find('list');
		$productionOrders = $this->InventoryMaterialIssued->ProductionOrder->find('list');
		$generalUnits = $this->InventoryMaterialIssued->GeneralUnit->find('list');
		$users = $this->InventoryMaterialIssued->User->find('list');
		$this->set(compact('inventoryMaterialRequestItems', 'inventoryStocks', 'inventoryItems', 'productionOrders', 'generalUnits', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryMaterialIssued->id = $id;
		if (!$this->InventoryMaterialIssued->exists()) {
			throw new NotFoundException(__('Invalid inventory material issued'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryMaterialIssued->delete()) {
			$this->Session->setFlash(__('The inventory material issued has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory material issued could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
