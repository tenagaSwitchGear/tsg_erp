<?php
App::uses('AppController', 'Controller');
/**
 * SaleOrderChildren Controller
 *
 * @property SaleOrderChild $SaleOrderChild
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleOrderChildsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleOrderChild->recursive = 0;
		$this->set('saleOrderChildren', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleOrderChild->exists($id)) {
			throw new NotFoundException(__('Invalid sale order child'));
		}
		$options = array('conditions' => array('SaleOrderChild.' . $this->SaleOrderChild->primaryKey => $id));
 		
 		$projectboms = $this->SaleOrderChild->ProjectBom->find('all', array(
 			'conditions' => array(
 				'ProjectBom.sale_order_child_id' => $id
 				) 
 			));
 		$boms = array();
 		foreach ($projectboms as $bom) {
 			$boms[] = array(
 				'ProjectBom' => $bom['ProjectBom'],
 				'TotalPrice' => $this->sum_bom_items($bom['ProjectBom']['id'])
 				);
 		}
 		$this->set('boms', $boms);

		$this->set('saleOrderChild', $this->SaleOrderChild->find('first', $options));
	}

	private function sum_bom_items($bom_id) {
		$this->loadModel('ProjectBomItem');
		$items = $this->ProjectBomItem->find('all', array(
			'fields' => array(
				'SUM(price_total) AS total'
				),
			'conditions' => array(
				'ProjectBomItem.project_bom_id' => $bom_id
				)
			));
		foreach ($items as $item) {
			return $item[0]['total'];
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleOrderChild->create();
			if ($this->SaleOrderChild->save($this->request->data)) {
				$this->Session->setFlash(__('The sale order child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale order child could not be saved. Please, try again.'));
			}
		}
		$saleJobs = $this->SaleOrderChild->SaleJob->find('list');
		$this->set(compact('saleJobs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleOrderChild->exists($id)) {
			throw new NotFoundException(__('Invalid sale order child'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleOrderChild->save($this->request->data)) {
				$this->Session->setFlash(__('The sale order child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale order child could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleOrderChild.' . $this->SaleOrderChild->primaryKey => $id));
			$this->request->data = $this->SaleOrderChild->find('first', $options);
		}
		$saleJobs = $this->SaleOrderChild->SaleJob->find('list');
		$this->set(compact('saleJobs'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleOrderChild->id = $id;
		if (!$this->SaleOrderChild->exists()) {
			throw new NotFoundException(__('Invalid sale order child'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleOrderChild->delete()) {
			$this->Session->setFlash(__('The sale order child has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale order child could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
