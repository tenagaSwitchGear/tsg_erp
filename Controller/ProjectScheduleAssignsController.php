<?php
App::uses('AppController', 'Controller');
/**
 * ProjectScheduleAssigns Controller
 *
 * @property ProjectScheduleAssign $ProjectScheduleAssign
 * @property PaginatorComponent $Paginator
 */
class ProjectScheduleAssignsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectScheduleAssign->recursive = 0;
		$this->set('projectScheduleAssigns', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectScheduleAssign->exists($id)) {
			throw new NotFoundException(__('Invalid project schedule assign'));
		}
		$options = array('conditions' => array('ProjectScheduleAssign.' . $this->ProjectScheduleAssign->primaryKey => $id));
		$this->set('projectScheduleAssign', $this->ProjectScheduleAssign->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectScheduleAssign->create();
			if ($this->ProjectScheduleAssign->save($this->request->data)) {
				$this->Session->setFlash(__('The project schedule assign has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project schedule assign could not be saved. Please, try again.'));
			}
		}
		$projectScheduleChildren = $this->ProjectScheduleAssign->ProjectScheduleChild->find('list');
		$projectManpowers = $this->ProjectScheduleAssign->ProjectManpower->find('list');
		$this->set(compact('projectScheduleChildren', 'projectManpowers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectScheduleAssign->exists($id)) {
			throw new NotFoundException(__('Invalid project schedule assign'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectScheduleAssign->save($this->request->data)) {
				$this->Session->setFlash(__('The project schedule assign has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project schedule assign could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectScheduleAssign.' . $this->ProjectScheduleAssign->primaryKey => $id));
			$this->request->data = $this->ProjectScheduleAssign->find('first', $options);
		}
		$projectScheduleChildren = $this->ProjectScheduleAssign->ProjectScheduleChild->find('list');
		$projectManpowers = $this->ProjectScheduleAssign->ProjectManpower->find('list');
		$this->set(compact('projectScheduleChildren', 'projectManpowers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectScheduleAssign->id = $id;
		if (!$this->ProjectScheduleAssign->exists()) {
			throw new NotFoundException(__('Invalid project schedule assign'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectScheduleAssign->delete()) {
			$this->Session->setFlash(__('The project schedule assign has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project schedule assign could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
