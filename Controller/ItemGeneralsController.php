<?php
App::uses('AppController', 'Controller');
/**
 * ItemGenerals Controller
 *
 * @property ItemGeneral $ItemGeneral
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ItemGeneralsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() { 
		$this->Paginator->settings = array('limit' => 500);
		$datas = $this->Paginator->paginate();

		$this->loadModel('InventoryItem');

		foreach ($datas as $row) {
			$this->InventoryItem->create();
			$insert = array(
				'name' => $row['ItemGeneral']['name'],
				'quantity' => 100,
				'unit_price' => 100,
				'inventory_item_category_id' => $row['InventoryItemCategory']['id'],
				'general_unit_id' => $row['GeneralUnit']['id'], 
				'code' => $row['ItemGeneral']['code'],
				'qc_inspect' => 1,
				'quantity_reserved' => 0,
				'quantity_available' => 100,
				'quantity_shortage' => 0,
				'status' => 1,
				'general_currency_id' => 1,
				'note' => '',
				'inventory_location_id' => 2,
				'inventory_store_id' => rand(1, 3),
				'inventory_rack_id' => rand(1, 6),
				'search_1' => $row['ItemGeneral']['search_1'],
				'search_2' => $row['ItemGeneral']['search_2'],
				'search_3' => $row['ItemGeneral']['search_3'],
				'search_4' => $row['ItemGeneral']['search_4'],
				'search_5' => '',
				'is_bom' => 0  
				);
			//$this->InventoryItem->save($insert);
		}

		$this->set('itemGenerals', $datas);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ItemGeneral->exists($id)) {
			throw new NotFoundException(__('Invalid item general'));
		}
		$options = array('conditions' => array('ItemGeneral.' . $this->ItemGeneral->primaryKey => $id));
		$this->set('itemGeneral', $this->ItemGeneral->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ItemGeneral->create();
			if ($this->ItemGeneral->save($this->request->data)) {
				$this->Session->setFlash(__('The item general has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The item general could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ItemGeneral->exists($id)) {
			throw new NotFoundException(__('Invalid item general'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ItemGeneral->save($this->request->data)) {
				$this->Session->setFlash(__('The item general has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The item general could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ItemGeneral.' . $this->ItemGeneral->primaryKey => $id));
			$this->request->data = $this->ItemGeneral->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ItemGeneral->id = $id;
		if (!$this->ItemGeneral->exists()) {
			throw new NotFoundException(__('Invalid item general'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ItemGeneral->delete()) {
			$this->Session->setFlash(__('The item general has been deleted.'));
		} else {
			$this->Session->setFlash(__('The item general could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
