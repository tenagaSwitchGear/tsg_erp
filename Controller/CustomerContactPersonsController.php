<?php
App::uses('AppController', 'Controller');
/**
 * CustomerContactPeople Controller
 *
 * @property CustomerContactPerson $CustomerContactPerson
 * @property PaginatorComponent $Paginator
 */
class CustomerContactPersonsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator'); 

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$conditions = array();
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['CustomerContactPerson.name LIKE'] = '%'.$_GET['name'].'%';
			}
			if($_GET['company_name'] != '') {
				$conditions['Customer.name LIKE'] = '%'.$_GET['company_name'].'%';
			}
			if($_GET['office_phone'] != '') {
				$conditions['CustomerContactPerson.office_phone'] = $_GET['office_phone'];
			}
			if($_GET['mobile_phone'] != '') {
				$conditions['CustomerContactPerson.mobile_phone'] = $_GET['mobile_phone'];
			}
			$this->request->data['CustomerContactPerson'] = $_GET;
		}
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'CustomerContactPerson.name ASC','limit'=>$record_per_page);
		 
		$this->set('customerContactPeople', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CustomerContactPerson->exists($id)) {
			throw new NotFoundException(__('Invalid customer contact person'));
		}
		$options = array('conditions' => array('CustomerContactPerson.' . $this->CustomerContactPerson->primaryKey => $id));
		$this->set('customerContactPerson', $this->CustomerContactPerson->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CustomerContactPerson->create();
			if ($this->CustomerContactPerson->save($this->request->data)) {
				$this->Session->setFlash(__('The customer contact person has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer contact person could not be saved. Please, try again.'), 'error');
			}
		}
		$customers = $this->CustomerContactPerson->Customer->find('list', array('order' => 'Customer.name ASC'));
		$this->set(compact('customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CustomerContactPerson->exists($id)) {
			throw new NotFoundException(__('Invalid customer contact person'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomerContactPerson->save($this->request->data)) {
				$this->Session->setFlash(__('The customer contact person has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer contact person could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('CustomerContactPerson.' . $this->CustomerContactPerson->primaryKey => $id));
			$this->request->data = $this->CustomerContactPerson->find('first', $options);
		}
		$customers = $this->CustomerContactPerson->Customer->find('list');
		$this->set(compact('customers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CustomerContactPerson->id = $id;
		if (!$this->CustomerContactPerson->exists()) {
			throw new NotFoundException(__('Invalid customer contact person'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomerContactPerson->delete()) {
			$this->Session->setFlash(__('The customer contact person has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The customer contact person could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function ajaxfindcontact() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = (int)$this->request->query['term']; 
			$json = $this->CustomerContactPerson->find('all', array(
				'conditions' => array(
					'CustomerContactPerson.customer_id' => $term
					) 
				));
			  
			echo json_encode($json);	
		}
	}
}
