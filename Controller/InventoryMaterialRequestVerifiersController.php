<?php
App::uses('AppController', 'Controller');
/**
 * InventoryMaterialRequestVerifiers Controller
 *
 * @property InventoryMaterialRequestVerifier $InventoryMaterialRequestVerifier
 * @property PaginatorComponent $Paginator
 */
class InventoryMaterialRequestVerifiersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryMaterialRequestVerifier->recursive = 0;
		$this->set('inventoryMaterialRequestVerifiers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryMaterialRequestVerifier->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request verifier'));
		}
		$options = array('conditions' => array('InventoryMaterialRequestVerifier.' . $this->InventoryMaterialRequestVerifier->primaryKey => $id));
		$this->set('inventoryMaterialRequestVerifier', $this->InventoryMaterialRequestVerifier->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryMaterialRequestVerifier->create();
			if ($this->InventoryMaterialRequestVerifier->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material request verifier has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material request verifier could not be saved. Please, try again.'));
			}
		}
		$inventoryMaterialRequests = $this->InventoryMaterialRequestVerifier->InventoryMaterialRequest->find('list');
		$users = $this->InventoryMaterialRequestVerifier->User->find('list');
		$this->set(compact('inventoryMaterialRequests', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryMaterialRequestVerifier->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request verifier'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryMaterialRequestVerifier->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material request verifier has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material request verifier could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryMaterialRequestVerifier.' . $this->InventoryMaterialRequestVerifier->primaryKey => $id));
			$this->request->data = $this->InventoryMaterialRequestVerifier->find('first', $options);
		}
		$inventoryMaterialRequests = $this->InventoryMaterialRequestVerifier->InventoryMaterialRequest->find('list');
		$users = $this->InventoryMaterialRequestVerifier->User->find('list');
		$this->set(compact('inventoryMaterialRequests', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryMaterialRequestVerifier->id = $id;
		if (!$this->InventoryMaterialRequestVerifier->exists()) {
			throw new NotFoundException(__('Invalid inventory material request verifier'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryMaterialRequestVerifier->delete()) {
			$this->Session->setFlash(__('The inventory material request verifier has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory material request verifier could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
