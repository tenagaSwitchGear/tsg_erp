<?php
App::uses('AppController', 'Controller');
/**
 * SaleBomItems Controller
 *
 * @property SaleBomItem $SaleBomItem
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleBomItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleBomItem->recursive = 0;
		$this->set('saleBomItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleBomItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale bom item'));
		}
		$options = array('conditions' => array('SaleBomItem.' . $this->SaleBomItem->primaryKey => $id));
		$this->set('saleBomItem', $this->SaleBomItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleBomItem->create();
			if ($this->SaleBomItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale bom item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale bom item could not be saved. Please, try again.'));
			}
		}
		$saleBoms = $this->SaleBomItem->SaleBom->find('list');
		$inventoryItems = $this->SaleBomItem->InventoryItem->find('list');
		$saleBomChildren = $this->SaleBomItem->SaleBomChild->find('list');
		$generalUnits = $this->SaleBomItem->GeneralUnit->find('list');
		$saleQuotations = $this->SaleBomItem->SaleQuotation->find('list');
		$this->set(compact('saleBoms', 'inventoryItems', 'saleBomChildren', 'generalUnits', 'saleQuotations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleBomItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale bom item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleBomItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale bom item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale bom item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleBomItem.' . $this->SaleBomItem->primaryKey => $id));
			$this->request->data = $this->SaleBomItem->find('first', $options);
		}
		$saleBoms = $this->SaleBomItem->SaleBom->find('list');
		$inventoryItems = $this->SaleBomItem->InventoryItem->find('list');
		$saleBomChildren = $this->SaleBomItem->SaleBomChild->find('list');
		$generalUnits = $this->SaleBomItem->GeneralUnit->find('list');
		$saleQuotations = $this->SaleBomItem->SaleQuotation->find('list');
		$this->set(compact('saleBoms', 'inventoryItems', 'saleBomChildren', 'generalUnits', 'saleQuotations'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleBomItem->id = $id;
		if (!$this->SaleBomItem->exists()) {
			throw new NotFoundException(__('Invalid sale bom item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleBomItem->delete()) {
			$this->Session->setFlash(__('The sale bom item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale bom item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
