<?php
App::uses('AppController', 'Controller');
/**
 * ProjectBoms Controller
 *
 * @property ProjectBom $ProjectBom
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectBomsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

	// Used by Planning Department
	public function costing($id = null) {
		
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectBom->recursive = 0;
		$this->set('projectBoms', $this->Paginator->paginate());
	}

	public function ajaxbomitembyjob() {
		$this->autoRender = false; 
		if(isset($this->request->query['item'])) {
			$item = $this->request->query['item'];

			$item = str_replace('bomItem-', '', $item);
			$arr = explode('-', $item);
			$job_child = $arr[0];
			$inv_item = $arr[1];
			$is_sub = $arr[2]; // 0 item, 1 sub
			$json['qty'] = 0;
			$json['unit'] =0;

			if($is_sub == 1) {
				// Find project bom child
				$this->loadModel('ProjectBomChild');
				$items = $this->ProjectBomChild->find('all', array(
					'fields' => array(
						'SUM(ProjectBomChild.quantity) AS total',
						'GeneralUnit.*'
						),
					'conditions' => array(
						'ProjectBomChild.sale_job_child_id' => $job_child,
						'ProjectBomChild.inventory_item_id' => $inv_item
						),
					'recursive' => 0
					));
				foreach ($items as $item) {
					$json['qty'] = $item[0]['total'];
					$json['unit'] = 'Unit';
				}
			} else {
				// Find project bom child
				$this->loadModel('ProjectBomItem');
				$items = $this->ProjectBomItem->find('all', array(
					'fields' => array(
						'SUM(ProjectBomItem.quantity_total) AS total',
						'GeneralUnit.*'
						),
					'conditions' => array(
						'ProjectBomItem.sale_job_child_id' => $job_child,
						'ProjectBomItem.inventory_item_id' => $inv_item
						),
					'recursive' => 0
					));
				foreach ($items as $item) {
					$json['qty'] = $item[0]['total'];
					$json['unit'] = $item['GeneralUnit']['name'];
				}
			}
			 
			echo json_encode($json);
		}
	}

	public function ajaxloadchild() {
		$this->autoRender = false; 
		$json = array();
		if(isset($this->request->query['parent_id'])) {
			$parent_id = (int)$this->request->query['parent_id'];

			$project_bom_id = (int)$this->request->query['project_bom_id']; 
			$this->loadModel('ProjectBomChild');
			$rows = $this->ProjectBomChild->find('all', array(
				'conditions' => array( 
					'ProjectBomChild.parent_id' => $parent_id,
					'ProjectBomChild.project_bom_id' => $project_bom_id
					),
				'order' => array(
					'ProjectBomChild.id' => 'ASC'
					),
				'recursive' => 0
				));
			// If no rows found, then try to show items
			if($rows) {
				foreach ($rows as $row) {
					$json['data'][] = $row; 
				} 
			}

			$this->loadModel('ProjectBomItem');
			$items = $this->ProjectBomItem->find('all', array(
				'conditions' => array( 
					'ProjectBomItem.project_bom_child_id' => $parent_id,
					'ProjectBomItem.project_bom_id' => $project_bom_id
					),
				'order' => array(
					'ProjectBomItem.id' => 'ASC'
					)
				));

			if($items) {
				foreach ($items as $item) {
					$json['items'][] = $item;
				} 
			}
		}
		echo json_encode($json);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectBom->exists($id)) {
			throw new NotFoundException(__('Invalid project bom'));
		}
		$options = array('conditions' => array('ProjectBom.' . $this->ProjectBom->primaryKey => $id));
		$this->set('projectBom', $this->ProjectBom->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectBom->create();
			if ($this->ProjectBom->save($this->request->data)) {
				$this->Session->setFlash(__('The project bom has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project bom could not be saved. Please, try again.'));
			}
		}
		$boms = $this->ProjectBom->Bom->find('list');
		$saleTenders = $this->ProjectBom->SaleTender->find('list');
		$saleOrders = $this->ProjectBom->SaleOrder->find('list');
		$saleOrderChildren = $this->ProjectBom->SaleOrderChild->find('list');
		$users = $this->ProjectBom->User->find('list');
		$this->set(compact('boms', 'saleTenders', 'saleOrders', 'saleOrderChildren', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectBom->exists($id)) {
			throw new NotFoundException(__('Invalid project bom'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectBom->save($this->request->data)) {
				$this->Session->setFlash(__('The project bom has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project bom could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectBom.' . $this->ProjectBom->primaryKey => $id));
			$this->request->data = $this->ProjectBom->find('first', $options);
		}
		$boms = $this->ProjectBom->Bom->find('list');
		$saleTenders = $this->ProjectBom->SaleTender->find('list');
		$saleOrders = $this->ProjectBom->SaleOrder->find('list');
		$saleOrderChildren = $this->ProjectBom->SaleOrderChild->find('list');
		$users = $this->ProjectBom->User->find('list');
		$this->set(compact('boms', 'saleTenders', 'saleOrders', 'saleOrderChildren', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectBom->id = $id;
		if (!$this->ProjectBom->exists()) {
			throw new NotFoundException(__('Invalid project bom'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectBom->delete()) {
			$this->Session->setFlash(__('The project bom has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project bom could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
