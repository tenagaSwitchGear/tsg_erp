<?php
App::uses('AppController', 'Controller');
/**
 * ProjectBomChildren Controller
 *
 * @property ProjectBomChild $ProjectBomChild
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectBomChildsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectBomChild->recursive = 0;
		$this->set('projectBomChildren', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectBomChild->exists($id)) {
			throw new NotFoundException(__('Invalid project bom child'));
		}
		$options = array('conditions' => array('ProjectBomChild.' . $this->ProjectBomChild->primaryKey => $id));
		$this->set('projectBomChild', $this->ProjectBomChild->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectBomChild->create();
			if ($this->ProjectBomChild->save($this->request->data)) {
				$this->Session->setFlash(__('The project bom child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project bom child could not be saved. Please, try again.'));
			}
		}
		$saleOrders = $this->ProjectBomChild->SaleOrder->find('list');
		$projectBoms = $this->ProjectBomChild->ProjectBom->find('list');
		$this->set(compact('saleOrders', 'projectBoms'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectBomChild->exists($id)) {
			throw new NotFoundException(__('Invalid project bom child'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectBomChild->save($this->request->data)) {
				$this->Session->setFlash(__('The project bom child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project bom child could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectBomChild.' . $this->ProjectBomChild->primaryKey => $id));
			$this->request->data = $this->ProjectBomChild->find('first', $options);
		}
		$saleOrders = $this->ProjectBomChild->SaleOrder->find('list');
		$projectBoms = $this->ProjectBomChild->ProjectBom->find('list');
		$this->set(compact('saleOrders', 'projectBoms'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectBomChild->id = $id;
		if (!$this->ProjectBomChild->exists()) {
			throw new NotFoundException(__('Invalid project bom child'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectBomChild->delete()) {
			$this->Session->setFlash(__('The project bom child has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project bom child could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
