<?php
App::uses('AppController', 'Controller');
/**
 * SaleQuotationItems Controller
 *
 * @property SaleQuotationItem $SaleQuotationItem
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleQuotationItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleQuotationItem->recursive = 0;
		$this->set('saleQuotationItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleQuotationItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation item'));
		}
		$options = array('conditions' => array('SaleQuotationItem.' . $this->SaleQuotationItem->primaryKey => $id));
		$this->set('saleQuotationItem', $this->SaleQuotationItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleQuotationItem->create();
			if ($this->SaleQuotationItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale quotation item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation item could not be saved. Please, try again.'));
			}
		}
		$generalUnits = $this->SaleQuotationItem->GeneralUnit->find('list');
		$products = $this->SaleQuotationItem->Product->find('list');
		$saleQuotations = $this->SaleQuotationItem->SaleQuotation->find('list');
		$this->set(compact('generalUnits', 'products', 'saleQuotations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleQuotationItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleQuotationItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale quotation item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleQuotationItem.' . $this->SaleQuotationItem->primaryKey => $id));
			$this->request->data = $this->SaleQuotationItem->find('first', $options);
		}
		$generalUnits = $this->SaleQuotationItem->GeneralUnit->find('list');
		$products = $this->SaleQuotationItem->Product->find('list');
		$saleQuotations = $this->SaleQuotationItem->SaleQuotation->find('list');
		$this->set(compact('generalUnits', 'products', 'saleQuotations'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleQuotationItem->id = $id;
		if (!$this->SaleQuotationItem->exists()) {
			throw new NotFoundException(__('Invalid sale quotation item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleQuotationItem->delete()) {
			$this->Session->setFlash(__('The sale quotation item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale quotation item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
