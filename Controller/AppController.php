<?php
/** 
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9 
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $date;

	public $user_id;

	public $group_id;

	public $designation;

	public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            ),
			'authenticate' => array(
				'Form' => array(
					'scope' => array('User.status' => '1')
				),
			),
        ),
        'Session',
        'RequestHandler' => array(
			'viewClassMap' => array(
				'xls' => 'CakeExcel.Excel',
				'xlsx' => 'CakeExcel.Excel'
			)
		), 
    	'Cookie'
		//'DebugKit.Toolbar',
    );
	
    public $helpers = array('Html', 'Form', 'Session');
	
	public $uses = array(
		'Configuration.Configuration',
		'EmailTemplate.EmailTemplate',
	);


	public $theme = "";


    public function beforeFilter() { 

    	// set cookie options
	    $this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
	    $this->Cookie->httpOnly = true;

	    if (!$this->Auth->loggedIn() && $this->Cookie->read('remember_me_cookie')) {
	        $cookie = $this->Cookie->read('remember_me_cookie');

	        $this->loadModel('User');

	        $user = $this->User->find('first', array(
	            'conditions' => array(
	                'User.username' => $cookie['username'],
	                'User.password' => $cookie['password']
	            )
	        ));
 
	    }   

    	$this->date = date('Y-m-d H:i:s');

    	// Load notification
    	$user_id = $this->Session->read('Auth.User.id');
    	$notifications = $this->__get_notification($user_id, 10);
    	$notification_counter = count($this->__get_notification($user_id));
    	$this->set('notifications', $notifications);
    	$this->set('notification_counter', $notification_counter);
    
		//Configure AuthComponent
		$this->Configuration->load();
		$this->Auth->loginAction = array(
          'controller' => 'users',
          'action' => 'login',
          'admin' => false,
		  'plugin' => false
        );
        $this->Auth->logoutRedirect = array(
          'controller' => 'users',
          'action' => 'login',
          'admin' => false,
		  'plugin' => false
        );
        $this->Auth->loginRedirect = array(
          'controller' => 'users',
          'action' => 'dashboard', 
		  'plugin' => false
        );
		
		$this->check_group();
		 
		if($this->params['plugin']=='content') {
			$this->layout = 'content';
		}
		
		if($this->params['plugin']=='blog') {
			$this->layout = 'blog';
		} 
 		
		$user_id = $this->Session->read('Auth.User.id');
		$group_id = $this->Session->read('Auth.User.group_id');
 		
		$controller = $this->params['controller']; 

		$designation = $this->check_designation();
		// Inject role to view
		$this->set('designations', $designation);
		
		$this->user_id = $user_id;
		$this->group_id = $group_id;
	}

	protected function spell_number($input) {
		$f = new \NumberFormatter("en", NumberFormatter::SPELLOUT); 
        $number = explode('.', $input);
        $output = $f->format($number[0]) . ' and ' . $f->format($number[1]) . ' cent'; 
        return ucwords($output);
	}

	protected function update_sale_order_progress($sale_job_child_id, $status) {
		// Update sale order progress
 		$this->loadModel('Saleorder');
 		$progress = $this->Saleorder->updateAll(
 			array(
 				'SaleOrder.progress' => "'".$status."'"
 				),
 			array(
 				'SaleOrder.sale_job_child_id' => $sale_job_child_id
 				)
 			);
 		if($progress) {
 			return true;
 		}
 		return false;
	}

	public function afterFilter() {
		//$this->Session->renew();
	}

	private function check_designation() {
		$user_id = $this->Session->read('Auth.User.id');
		$group_id = $this->Session->read('Auth.User.group_id');
		$this->loadModel('InternalDepartment');
		$departments = $this->InternalDepartment->find('all', array('InternalDepartment.group_id' => $group_id));
		$data = array();
		foreach ($departments as $department) {
			$data[] = $department['InternalDepartment'];
		}
		return $data;
	}

	
	private function check_group() {
		$this->theme = 'Default';  
		if (!isset($this->request->params['admin']) && Configure::read('Site.status') == 0){
			$this->theme = 'Default';
			$this->layout = 'maintenance';
			$this->set('title_for_layout', __('Site down for maintenance'));
		} else {
			$this->theme = 'Default';
			$this->layout = 'admin';
			$this->set('title_for_layout', __('Admin'));
		} 

		if ($this->params['plugin'] == 'acl') {
			$this->theme = 'Default';
			$this->layout = 'acl';
		}

		if ($this->request->is('ajax')) {
			$this->theme = 'Default';
			$this->layout = 'ajax';
		}   
	}

	private function __get_notification($user_id, $limit = NULL) {
		$this->loadModel('Notification');
		$condition = array();
		
		$condition['conditions']['Notification.user_id'] = $user_id;
		$condition['conditions']['Notification.status'] = 0;
		$condition['order']['Notification.id'] = 'DESC';

		if($limit != NULL) {
			$condition['limit'] = 10; 
		} 
		$Notification = $this->Notification->find('all', $condition);
		return $Notification;
	}

	protected function _insert_notification($data = array()) {
		$this->loadModel('Notification'); 
		$this->Notification->Create();
		$data = array(
			'user_id' => $data['user_id'],
			'body' => $data['body'],
			'created' => date('Y-m-d H:i:s'),
			'status' => 0,
			'type' => $data['type'],
			'link' => $data['link']
			);
		return $this->Notification->save($data);
	}

	protected function generate_code($prefix, $number) {
		$str_pad = Configure::read('Site.str_pad');
		$num = str_pad($number, $str_pad, 0, STR_PAD_LEFT);
		return $prefix . $num;
	}

	protected function send_email($data = array()) {
		$Email = new CakeEmail('default'); 
		$Email->from(array(Configure::read('Site.email') => 'RPS Auto Reply'));
		$Email->to(array($data['to'])); 
		$Email->subject($data['subject']); 
		$Email->emailFormat('html');
		$Email->template($data['template'])->viewVars($data['content']); 
		return $Email->send(); 
	}

	protected function insert_log($user_id, $name, $link, $type) {
		$this->loadModel('UserLog');
		$this->UserLog->create();
		$log = array(
			'name' => $name,
			'link' => $link,
			'created' => $this->date,
			'user_id' => $user_id,
			'type' => $type,
			'ip' => $this->request->clientIp(),
			'user_agent' => $this->request->header('User-Agent')
			);
		if($this->UserLog->save($log)) {
			return true;
		} else {
			return false;
		}
	}
 
}
