<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodFiles Controller
 *
 * @property FinishedGoodFile $FinishedGoodFile
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FinishedGoodFilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodFile->recursive = 0;
		$this->set('finishedGoodFiles', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodFile->exists($id)) {
			throw new NotFoundException(__('Invalid finished good file'));
		}
		$options = array('conditions' => array('FinishedGoodFile.' . $this->FinishedGoodFile->primaryKey => $id));
		$this->set('finishedGoodFile', $this->FinishedGoodFile->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodFile->create();
			if ($this->FinishedGoodFile->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good file has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good file could not be saved. Please, try again.'));
			}
		}
		$finishedGoods = $this->FinishedGoodFile->FinishedGood->find('list');
		$this->set(compact('finishedGoods'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodFile->exists($id)) {
			throw new NotFoundException(__('Invalid finished good file'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodFile->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good file has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good file could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodFile.' . $this->FinishedGoodFile->primaryKey => $id));
			$this->request->data = $this->FinishedGoodFile->find('first', $options);
		}
		$finishedGoods = $this->FinishedGoodFile->FinishedGood->find('list');
		$this->set(compact('finishedGoods'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodFile->id = $id;
		if (!$this->FinishedGoodFile->exists()) {
			throw new NotFoundException(__('Invalid finished good file'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodFile->delete()) {
			$this->Session->setFlash(__('The finished good file has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good file could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
