<?php
App::uses('AppController', 'Controller');
/**
 * SaleOrderCategories Controller
 *
 * @property SaleOrderCategory $SaleOrderCategory
 * @property PaginatorComponent $Paginator
 */
class SaleOrderCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleOrderCategory->recursive = 0;
		$this->set('saleOrderCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleOrderCategory->exists($id)) {
			throw new NotFoundException(__('Invalid sale order category'));
		}
		$options = array('conditions' => array('SaleOrderCategory.' . $this->SaleOrderCategory->primaryKey => $id));
		$this->set('saleOrderCategory', $this->SaleOrderCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleOrderCategory->create();
			if ($this->SaleOrderCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The sale order category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale order category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleOrderCategory->exists($id)) {
			throw new NotFoundException(__('Invalid sale order category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleOrderCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The sale order category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale order category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleOrderCategory.' . $this->SaleOrderCategory->primaryKey => $id));
			$this->request->data = $this->SaleOrderCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleOrderCategory->id = $id;
		if (!$this->SaleOrderCategory->exists()) {
			throw new NotFoundException(__('Invalid sale order category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleOrderCategory->delete()) {
			$this->Session->setFlash(__('The sale order category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale order category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
