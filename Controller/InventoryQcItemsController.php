<?php
App::uses('AppController', 'Controller');
/**
 * InventoryDeliveryOrderItems Controller
 *
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property PaginatorComponent $Paginator
 */
class InventoryQcItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index($status = null) {	

		if(isset($_GET['search'])) {
            //print_r($_GET);
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryQcItem.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryQcItem.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == ''){
                $conditions = '';
            }            
        } else {
            $conditions = '';
        }	
        if($status != null){
        	$conditions['InventoryQcItem.status'] = (int)$status;
        } else {
        	$conditions['InventoryQcItem.status'] = 0;
        }
		//$this->InventoryQcItem->recursive = 0;
		//$this->set('inventoryQcItems', $this->Paginator->paginate());

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$qc_array = array();
		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'InventoryQcItem.id DESC',
			'limit'=>$record_per_page,
			'recursive' => 0
			); 
		$qc = $this->Paginator->paginate('InventoryQcItem');
		//exit;
		foreach ($qc as $item) {
			$qc_array[] = array(
				'InventoryQcItem' => $item['InventoryQcItem'],
				'InventoryDeliveryOrderItem' => $item['InventoryDeliveryOrderItem'],
				'InventoryItem' => $this->get_inv_item($item['InventoryQcItem']['inventory_item_id'])
			);
		}

		//print_r($qc_array);
		//exit;


		$this->set('inventoryQcItems', $qc_array);

	}

	private function get_inv_item($id){
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			),
			'recursive' => -1
		));

		return $item[0]['InventoryItem'];
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		if (!$this->InventoryQcItem->exists($id)) {
			throw new NotFoundException(__('Invalid qc item'));
		}

		if ($this->request->is('post')) {
			
			//print_r($this->request->data['InventoryQcItem']);
			if(isset($this->request->data['InventoryQcItem']['attachment'])){
				$this->request->data['InventoryQcRemark']['attachment'] = $this->request->data['InventoryQcItem']['attachment'];
			}
			//print_r($this->request->data['InventoryQcRemark']);
			//exit;
			$this->loadModel('InventorySupplierItem');
			$this->InventoryQcItem->create();
			$id = $this->request->data['InventoryQcItem']['id'];
			$inventory_item_id = $this->request->data['InventoryQcItem']['inventory_item_id'];
			$general_unit_id = $this->request->data['InventoryQcItem']['general_unit_id'];

			$details = $this->InventoryQcItem->find('all', array(
				'conditions' => array(
					'InventoryQcItem.id' => $id
				),
				'recursive' => -1
			));

			$this->loadModel('InventoryDeliveryOrderItem');
			$do = $this->InventoryDeliveryOrderItem->find('all', array(
				'conditions' => array(
					'InventoryDeliveryOrderItem.id' => $this->request->data['InventoryQcItem']['inventory_delivery_order_item']
				)
			));

			$old_q_pass = $details[0]['InventoryQcItem']['quantity_pass'];
			$old_q_rej  = $details[0]['InventoryQcItem']['quantity_rejected'];

			if(isset($this->request->data['InventoryQcItem']['quantity_pass'])){
				if(!empty($this->request->data['InventoryQcItem']['quantity_pass'])){
					$qty = $this->request->data['InventoryQcItem']['quantity_pass'] + $old_q_pass;

					$this->InventoryQcItem->create();
					$data_qc_update = array(
						'id' 			=> $id,
						'created'		=> $details[0]['InventoryQcItem']['created'],
						'modified'		=> date('Y-m-d H:i:s'),
						'quantity_pass' => $qty
					);

					$this->InventoryQcItem->save($data_qc_update);

					//print_r($data_qc_update);
					$this->loadModel('InventoryDeliveryOrderItem');
					$get_q_accepted = $this->InventoryDeliveryOrderItem->find('first', array(
						'conditions' => array(
							'InventoryDeliveryOrderItem.id' => $this->request->data['InventoryQcItem']['inventory_delivery_order_item']
						)
					));

					$q_accepted = $get_q_accepted['InventoryDeliveryOrderItem']['quantity_accepted'];
					$new_q_accepted = $q_accepted + $this->request->data['InventoryQcItem']['quantity_pass'];

					$data_do_item = array(
						'id' => $this->request->data['InventoryQcItem']['inventory_delivery_order_item'],
						'quantity_accepted' => $new_q_accepted,
					);

					//update po item
					$this->loadModel('InventoryPurchaseOrderItem');
					$po_item_id = $do[0]['InventoryDeliveryOrderItem']['inventory_purchase_order_item_id'];
					$current_po_item = $this->InventoryPurchaseOrderItem->find('first', array(
						'conditions' => array(
							'InventoryPurchaseOrderItem.id' => $po_item_id
						)
					));

					$receive_q = $current_po_item['InventoryPurchaseOrderItem']['received_quantity'];
					$new_receive_q = $receive_q + $this->request->data['InventoryQcItem']['quantity_pass'];

					$accept_q = $current_po_item['InventoryPurchaseOrderItem']['accepted_quantity'];
					$new_accept_q = $accept_q + $this->request->data['InventoryQcItem']['quantity_pass'];

					if($new_receive_q == $new_accept_q){
						$status = '1';
					}else{
						$status = '0';
					}

					$this->InventoryPurchaseOrderItem->create();

					$data_update_po_item = array(
						'id' => $po_item_id,
						'received_quantity' => $new_receive_q,
						'accepted_quantity' => $new_accept_q,
						'status' => $status
					);

					$this->InventoryPurchaseOrderItem->save($data_update_po_item);

					$this->InventoryDeliveryOrderItem->save($data_do_item);

					$this->loadModel('InventoryStock');

					$this->InventoryStock->create();

						$data_stocks = array(
							'general_unit_id' => $general_unit_id,
							'inventory_item_id'	=> $inventory_item_id,
							'inventory_delivery_order_item_id'	=> $this->request->data['InventoryQcItem']['inventory_delivery_order_item'],
							'price_per_unit'					=> $do[0]['InventoryDeliveryOrderItem']['price_per_unit'],
							'quantity'	=> $this->request->data['InventoryQcItem']['quantity_pass'],
							'qc_status' => '1',
							'inventory_delivery_location_id' => $do[0]['InventoryDeliveryOrder']['inventory_delivery_location_id'],
							'location_id' => $do[0]['InventoryDeliveryOrderItem']['store_id'],
							'rack_id' => $do[0]['InventoryDeliveryOrderItem']['rack_id'],
							'inventory_supplier_id' => $do[0]['InventoryDeliveryOrder']['inventory_supplier_id'],
							'created' => date('Y-m-d H:i:s')
						);

					$this->InventoryStock->save($data_stocks);
					$lastID_stock = $this->InventoryStock->getLastInsertID();

					$this->loadModel('InventoryMaterialHistory');

					$sum = $this->InventoryMaterialHistory->query("SELECT SUM(quantity) AS total FROM inventory_material_histories WHERE inventory_item_id=".$inventory_item_id);
					$this->loadModel('InventoryMaterialHistory');

					$sum = $sum[0][0]['total'];
					$balance = $sum + $this->request->data['InventoryQcItem']['quantity_pass'];

					$this->InventoryMaterialHistory->create();

					$data_material_history = array(
						'reference'								=> $do[0]['InventoryDeliveryOrder']['grn_no'],
						'user_id'								=> $_SESSION['Auth']['User']['id'],
						'store_pic'								=> 0,
						'inventory_item_id'						=> $inventory_item_id,
						'inventory_stock_id'					=> $lastID_stock,
						'quantity'								=> $this->request->data['InventoryQcItem']['quantity_pass'],
						'general_unit_id'						=> $general_unit_id,
						'issued_quantity'						=> 0,
						'quantity_balance'						=> $balance,
						'inventory_material_request_id'			=> 0,
						'inventory_material_request_item_id'	=> 0,
						'created'								=> date('Y-m-d H:i:s'),
						'status'								=> 1,
						'type'									=> 6,
						'transfer_type'							=> 'In',
					);

					$this->InventoryMaterialHistory->save($data_material_history);

				}else{
					$this->Session->setFlash(__('The item could not be update.'), 'error');
				}

			}else{
				//print_r($_POST);
				//exit;
				if(!empty($this->request->data['InventoryQcItem']['quantity_rejected'])){
					$remarkQc = $this->request->data['InventoryQcItem']['remark'];
					$commentQc = $this->request->data['InventoryQcItem']['comment_id'];
					$attachment = $this->request->data['InventoryQcItem']['attachment'];

					$this->loadModel('InventoryQcRemark');
					$this->InventoryQcRemark->create();

					for($i=0; $i<count($remarkQc); $i++){
						$this->InventoryQcRemark->create();
						$this->request->data['InventoryQcRemark']['inventory_qc_item_id'] = $id;
						$this->request->data['InventoryQcRemark']['remark'] = $remarkQc[$i];
						$this->request->data['InventoryQcRemark']['status'] = 0;
						$this->request->data['InventoryQcRemark']['user_id'] = $_SESSION['Auth']['User']['id'];
						$this->request->data['InventoryQcRemark']['attachment'] = $attachment[$i];

						//print_r($this->request->data['InventoryQcRemark']);
						$this->InventoryQcRemark->save($this->request->data['InventoryQcRemark']);
						//exit

					}

					$this->loadModel('InventoryDeliveryOrderItem');
					$this->InventoryDeliveryOrderItem->create();
					$item_do = $this->InventoryDeliveryOrderItem->find('all', array(
						'conditions' => array(
							'InventoryDeliveryOrderItem.id' => $this->request->data['InventoryQcItem']['inventory_delivery_order_item']
						), 
						'recursive' => -1
					));

					$old_do_item_quantity_delivered = $item_do[0]['InventoryDeliveryOrderItem']['quantity_delivered'];
					$new_do_item_quantity_delivered = $old_do_item_quantity_delivered - $this->request->data['InventoryQcItem']['quantity_rejected'];

					$data_do_item_update = array(
						'id' => $details[0]['InventoryQcItem']['inventory_delivery_order_item_id'],
						'quantity_delivered' => $new_do_item_quantity_delivered,
						'quantity_rejected' => $this->request->data['InventoryQcItem']['quantity_rejected'],
						'quantity_remaining' => $this->request->data['InventoryQcItem']['quantity_rejected']
					);

					$this->InventoryDeliveryOrderItem->save($data_do_item_update);

					$qty = $this->request->data['InventoryQcItem']['quantity_rejected'] + $old_q_rej;

					$data_qc_update = array(
						'id' 				=> $id,
						'quantity_rejected' => $qty,
					);

					$this->loadModel('InventoryStockRejected');
					$this->InventoryStockRejected->create();

					$data_stocks_rejected = array(
						'general_unit_id' 		=> $general_unit_id,
						'inventory_item_id'		=> $inventory_item_id,
						'inventory_delivery_order_item_id'	=> $this->request->data['InventoryQcItem']['inventory_delivery_order_item'],
						'quantity'				=> $this->request->data['InventoryQcItem']['quantity_rejected'],
						'status' 				=> '0',
						'inventory_qc_item_id' 	=> $id,
						'finished_good_comment_id'	=> 0,
						'type' => '1',
						'inventory_supplier_id' => $do[0]['InventoryDeliveryOrder']['inventory_supplier_id'],
						'created' 				=> date('Y-m-d H:i:s')
					);

					$this->InventoryStockRejected->save($data_stocks_rejected);
				}else{
					$this->Session->setFlash(__('The item could not be update.'), 'error');
				}
			}

			if(!isset($data_qc_update)){
				$this->Session->setFlash(__('The item could not be update.'), 'error');
			}else{
				if($this->InventoryQcItem->save($data_qc_update)){

					$this->Session->setFlash(__('The item has been update.'), 'success');
					return $this->redirect(array('action' => 'view', $id));
				}else{
					$this->Session->setFlash(__('The item could not be update.'), 'error');
				}
			}
		}

		$this->loadModel('FinishedGoodComment');
		$finishedgoodcomment = $this->FinishedGoodComment->find('list');


		$options = array('conditions' => array('InventoryQcItem.' . $this->InventoryQcItem->primaryKey => $id));
		$this->set('inventoryQcItems', $this->InventoryQcItem->find('first', $options));
		$this->set(compact('finishedgoodcomment'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryDeliveryOrderItem->create();
			if ($this->InventoryDeliveryOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory delivery order item has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory delivery order item could not be saved. Please, try again.'), 'error');
			}
		}
		$inventoryDeliveryOrders = $this->InventoryDeliveryOrderItem->InventoryDeliveryOrder->find('list');
		$inventoryItems = $this->InventoryDeliveryOrderItem->InventoryItem->find('list');
		$generalUnits = $this->InventoryDeliveryOrderItem->GeneralUnit->find('list');
		$generalTaxes = $this->InventoryDeliveryOrderItem->GeneralTax->find('list');
		$this->set(compact('inventoryDeliveryOrders', 'inventoryItems', 'generalUnits', 'generalTaxes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryDeliveryOrderItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory delivery order item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryDeliveryOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory delivery order item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory delivery order item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryDeliveryOrderItem.' . $this->InventoryDeliveryOrderItem->primaryKey => $id));
			$this->request->data = $this->InventoryDeliveryOrderItem->find('first', $options);
		}
		$inventoryDeliveryOrders = $this->InventoryDeliveryOrderItem->InventoryDeliveryOrder->find('list');
		$inventoryItems = $this->InventoryDeliveryOrderItem->InventoryItem->find('list');
		$generalUnits = $this->InventoryDeliveryOrderItem->GeneralUnit->find('list');
		$generalTaxes = $this->InventoryDeliveryOrderItem->GeneralTax->find('list');
		$this->set(compact('inventoryDeliveryOrders', 'inventoryItems', 'generalUnits', 'generalTaxes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryDeliveryOrderItem->id = $id;
		if (!$this->InventoryDeliveryOrderItem->exists()) {
			throw new NotFoundException(__('Invalid inventory delivery order item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryDeliveryOrderItem->delete()) {
			$this->Session->setFlash(__('The inventory delivery order item has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The inventory delivery order item could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}