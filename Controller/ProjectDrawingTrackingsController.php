<?php
App::uses('AppController', 'Controller');
/**
 * ProjectDrawingTrackings Controller
 *
 * @property ProjectDrawingTracking $ProjectDrawingTracking
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectDrawingTrackingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectDrawingTracking->recursive = 0;
		$this->set('projectDrawingTrackings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectDrawingTracking->exists($id)) {
			throw new NotFoundException(__('Invalid project drawing tracking'));
		}
		$options = array('conditions' => array('ProjectDrawingTracking.' . $this->ProjectDrawingTracking->primaryKey => $id));
		$this->set('projectDrawingTracking', $this->ProjectDrawingTracking->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id = null, $drawing = null) {
		if (!$this->ProjectDrawingTracking->ProjectDrawing->exists($drawing)) {
			throw new NotFoundException(__('Invalid project drawing'));
		}

		$this->loadModel('ProjectDrawingFile');
		$this->loadModel('ProjectDrawing');

		
		if ($this->request->is('post')) {
			$this->ProjectDrawingTracking->create();
			$this->request->data['ProjectDrawingTracking']['project_drawing_id'] = $drawing;
			$this->request->data['ProjectDrawingTracking']['user_id'] = $this->user_id;
			$this->request->data['ProjectDrawingTracking']['project_drawing_file_id'] = $id;
			if ($this->ProjectDrawingTracking->save($this->request->data)) {
				
				$status = $this->request->data['ProjectDrawingTracking']['status'];
				$this->ProjectDrawingFile->updateAll(
					array(
						'ProjectDrawingFile.status' => "'".$status."'"
						),
					array(
						'ProjectDrawingFile.id' => $id
						)
					);

				$total_drawing = $this->count_all_file($drawing);
				$total_approved = $this->count_file_status_approved($drawing, 'Approved', 'Approved (Info only)');
				$total_submitted = $this->count_file_status_approved($drawing, 'Submitted', 'Revise & Resubmitted');
		 

				$drawing_status = 'Draft';
				

				if($total_submitted > 0 && $total_submitted < $total_drawing && $total_approved == 0) {
					// Partially submitted
					$drawing_status = 'Partially Submitted';
				}

				if($total_submitted == $total_drawing) {
					// All submitted
					$drawing_status = 'Submitted';
				}

				if($total_approved > 0 && $total_approved < $total_drawing) {
					// Partially approved
					$drawing_status = 'Partially Approved';
				}
				if($total_approved == $total_drawing) {
					// All approved
					$drawing_status = 'Approved';
				}

				$this->ProjectDrawing->updateAll(
					array(
						'ProjectDrawing.status' => "'".$drawing_status."'"
						),
					array(
						'ProjectDrawing.id' => $drawing
						)
					); 
				

				$this->Session->setFlash(__('The project drawing tracking has been saved.'), 'success');
				return $this->redirect(array('controller' => 'project_drawings', 'action' => 'view/'.$drawing));
			} else {
				$this->Session->setFlash(__('The project drawing tracking could not be saved. Please, try again.'), 'error');
			}
		}

		

		$projectDrawing = $this->ProjectDrawingTracking->ProjectDrawing->find('first', array('conditions' => array(
			'ProjectDrawing.id' => $drawing
			)));

		$drawing = $this->ProjectDrawingFile->find('first', array(
			'conditions' => array(
				'ProjectDrawingFile.id' => $id
				)
			));
		$this->set('drawing', $drawing);

		$trackings = $this->ProjectDrawingTracking->find('all', array(
			'conditions' => array(
				'ProjectDrawingTracking.project_drawing_file_id' => $id
				)
			));
		$this->set('trackings', $trackings);

		
		$projectDrawings = $this->ProjectDrawingTracking->ProjectDrawing->find('list');
		$this->set('projectDrawings', $projectDrawing);

		$this->set('id', $id);
	}

	private function count_file_status_approved($drawing_id, $status_1, $status_2) {
		$this->loadModel('ProjectDrawingFile'); 
		$count = $this->ProjectDrawingFile->find('all', array(
			'conditions' => array(
					'ProjectDrawingFile.project_drawing_id' => $drawing_id,
					'ProjectDrawingFile.status' => array($status_1, $status_2)
				) 
			));
		return count($count);
	}

	private function count_all_file($drawing_id) {
		$this->loadModel('ProjectDrawingFile');
		$count = $this->ProjectDrawingFile->find('all', array(
			'conditions' => array(
				'ProjectDrawingFile.project_drawing_id' => $drawing_id
				)
			));
		return count($count);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectDrawingTracking->exists($id)) {
			throw new NotFoundException(__('Invalid project drawing tracking'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectDrawingTracking->save($this->request->data)) {
				$this->Session->setFlash(__('The project drawing tracking has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project drawing tracking could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectDrawingTracking.' . $this->ProjectDrawingTracking->primaryKey => $id));
			$this->request->data = $this->ProjectDrawingTracking->find('first', $options);
		}
		$projectDrawings = $this->ProjectDrawingTracking->ProjectDrawing->find('list');
		$this->set(compact('projectDrawings'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectDrawingTracking->id = $id;
		if (!$this->ProjectDrawingTracking->exists()) {
			throw new NotFoundException(__('Invalid project drawing tracking'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectDrawingTracking->delete()) {
			$this->Session->setFlash(__('The project drawing tracking has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project drawing tracking could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
