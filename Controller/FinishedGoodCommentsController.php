<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodComments Controller
 *
 * @property FinishedGoodComment $FinishedGoodComment
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FinishedGoodCommentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodComment->recursive = 0;
		$this->set('finishedGoodComments', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodComment->exists($id)) {
			throw new NotFoundException(__('Invalid finished good comment'));
		}
		$options = array('conditions' => array('FinishedGoodComment.' . $this->FinishedGoodComment->primaryKey => $id));
		$this->set('finishedGoodComment', $this->FinishedGoodComment->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodComment->create();
			if ($this->FinishedGoodComment->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good comment has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good comment could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodComment->exists($id)) {
			throw new NotFoundException(__('Invalid finished good comment'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodComment->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good comment has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good comment could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodComment.' . $this->FinishedGoodComment->primaryKey => $id));
			$this->request->data = $this->FinishedGoodComment->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodComment->id = $id;
		if (!$this->FinishedGoodComment->exists()) {
			throw new NotFoundException(__('Invalid finished good comment'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodComment->delete()) {
			$this->Session->setFlash(__('The finished good comment has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good comment could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
