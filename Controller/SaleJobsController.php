<?php
App::uses('AppController', 'Controller');
/**
 * SaleJobs Controller
 *
 * @property SaleJob $SaleJob
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleJobsController extends AppController {

/**
 * Components
 *
 * @var array 
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

	private function get_customer_id_by_tender() {

	}

	public function add() { 

		if ($this->request->is('post')) {
			$this->SaleJob->create();

			if(isset($this->request->data['SaleJobChild']['name'])) {
				$count = count($this->request->data['SaleJobChild']['name']);
			} else {
				$count = 0;
			}  

			$this->request->data['SaleJob']['station'] = $count;
			$this->request->data['SaleJob']['quantity'] = 0;  

			$job = $this->SaleJob->find('all', array( 
				'recursive' => -1
				));

			if($this->group_id == 9) {
				// Sale dept
				$prefix = 'P';
			} elseif($this->group_id == 25) {
				// STS dept
				$prefix = 'M';
			} elseif($this->group_id == 22) {
				// Project dept
				$prefix = 'T';
			} else {
				$prefix = 'JOB';
			}

			$job_no = $this->generate_code($prefix, count($job) + 1);

			$this->request->data['SaleJob']['name'] = $job_no; 
			$this->request->data['SaleJob']['user_id'] = $this->Session->read('Auth.User.id'); 
			
			if($this->request->data['SaleJob']['sale_tender_id'] == '') {
				$this->request->data['SaleJob']['sale_tender_id'] = 0;
			}

			$this->request->data['SaleJob']['group_id'] = $this->group_id;
			if ($this->SaleJob->save($this->request->data)) { 
				$id = $this->SaleJob->getLastInsertId();
				$this->loadModel('SaleJobChild');
				// Insert Child, if not exist, then insert 1 child
				if(isset($this->request->data['SaleJobChild']['name'])) {  
					
					$name = $this->request->data['SaleJobChild']['name'];
					$description = $this->request->data['SaleJobChild']['description'];  

					$number = 1;
					for($i = 0; $i < count($name); $i++) {
						$this->SaleJobChild->create(); 

						if(count($name) == 1) {
							$station_no = $job_no;
						} elseif (count($name) > 1) {
							$station_no = $job_no . '-' . $number;
						} else {
							$station_no = $job_no;
						}
						$child = array(
							'sale_job_id' => $id,
							'customer_station_id' => 0,
							'station_name' => $station_no,
							'name' => $name[$i],
							'description' => $description[$i],
							'status' => 0,
							'number' => $number, 
							'po_created' => 0
							);
						if(!$this->SaleJobChild->save($child)) {
							//var_dump($child);
							//return false;
						} 
						$number += 1;
					}
				} else {
					$child = array(
						'sale_job_id' => $id,
						'customer_station_id' => 0,
						'station_name' => $job_no,
						'name' => $this->request->data['SaleJob']['name'],
						'description' => $this->request->data['SaleJob']['name'], 
						'status' => 0,
						'number' => 0, 
						'po_created' => 0
						);
					if(!$this->SaleJobChild->save($child)) {
						//var_dump($child);
						//return false;
					} 
				}

				//$this->job_approval($id, $job_no, 'HOS');
				$this->Session->setFlash(__('Job has been saved.'), 'success');
				return $this->redirect(array('action' => 'index')); 
			} else {
				$this->Session->setFlash(__('Job could not be saved. It may caused by Invalid Quotation No.'), 'error');
			}
		} 

		$saleTenders = $this->SaleJob->SaleTender->find('list');
		$saleQuotations = $this->SaleJob->SaleQuotation->find('list');   
		$users = $this->SaleJob->User->find('list'); 
 
		$Customer = $this->SaleJob->Customer->find('list', array(
			'order' => array('Customer.name' => 'ASC'), 
			'recursive' => -1
			));

		$this->set(compact('saleTenders', 'saleQuotations', 'users', 'Customer'));

	}

	private function job_approval($id, $job_no, $role) { 
		$this->loadModel('User');

		$users = $this->User->find('all', array(
			'conditions' => array(
				'User.group_id' => 9,
				'User.role' => $role
				),
			'reqursive' => -1
			));

		if($users) {
			foreach ($users as $user) {
				$data['to'] = $user['User']['email'];
				$data['template'] = 'job_waiting_approval';
				$data['subject'] = 'New Job Waiting For Verification';
				$data['content'] = array(
					'username' => $user['User']['username'],
					'subtitle' => 'New Job Waiting For Verification',
					'link' => 'sale_jobs/view/' . $id,
					'job_no' => $job_no
					);
				$this->send_email($data);
			} 
		} 
		return true;
	}

	public function bom($id = null) {
		if (!$this->SaleJob->exists($id)) {
			throw new NotFoundException(__('Invalid sale order'));
		} 
		$SaleJob = $this->SaleJob->find('first', array('conditions' => array('SaleJob.id' => $id)));
		// Find Child to display. Then add bom under it
		$stations = $this->SaleJob->SaleJobChild->find('all', array('conditions' => array('SaleJobChild.sale_job_id' => $id)));

		if ($this->request->is('post')) {
			  
			$item_id = $this->request->data['ProjectBom']['item_id'];
			$quantity = $this->request->data['ProjectBom']['quantity'];
			$bom = $this->request->data['ProjectBom']['bom_id'];

			$is_bom = $this->request->data['ProjectBom']['is_bom'];

			$childs = $this->request->data['ProjectBom']['sale_job_child_id'];

			$general_unit_id = $this->request->data['ProjectBom']['general_unit_id'];
			$unit_price = $this->request->data['ProjectBom']['unit_price'];
			$discount = $this->request->data['ProjectBom']['discount'];
			$tax = $this->request->data['ProjectBom']['tax'];  

			//var_dump($childs);
			$user_id = $this->Session->read('Auth.User.id');  


			$this->loadModel('ProjectBom');
			$this->loadModel('SaleJobItem');
			foreach ($childs as $key => $child) { 
				for($i = 0; $i < count($item_id[$child]); $i++) { 
					// IF BOM
					if($is_bom[$child][$i] == 1) {

						$iteminfo = $this->bom_info($bom[$child][$i]); 
						$total_price = $quantity[$child][$i] * $unit_price[$child][$i]; 

						$this->SaleJobItem->create();  
						$data = array(
							'quantity' => $quantity[$child][$i], 
							'general_unit_id' => $general_unit_id[$child][$i],
							'unit_price' => 0,
							'discount' => 0,
							'tax' => 0,
							'total_price' => 0, 
							'bom_id' => $bom[$child][$i],
							'name' => $iteminfo['Bom']['name'],
							'code' => $iteminfo['Bom']['code'],
							'remark' => 'Remark',
							'sale_tender_id' => $SaleJob['SaleTender']['id'],
							'sale_job_id' => $id,
							'sale_job_child_id' => $child,
							'created' => $this->date,
							'modified' => $this->date,
							'user_id' => $user_id,
							'status' => 0, 
							'inventory_item_id' => $item_id[$child][$i],
							'type' => 1 
							);
						$this->SaleJobItem->save($data);	

						// Copy BOM For Planning Purpose
						$this->ProjectBom->create();  
						$data = array(
							'quantity' => $quantity[$child][$i], 
							'general_unit_id' => $general_unit_id[$child][$i],
							'unit_price' => 0,
							'discount' => 0,
							'tax' => 0,
							'total_price' => 0, 
							'bom_id' => $bom[$child][$i],
							'name' => $iteminfo['Bom']['name'],
							'code' => $iteminfo['Bom']['code'],
							'remark' => 'Remark',
							'sale_tender_id' => $SaleJob['SaleJob']['sale_tender_id'],
							'sale_job_id' => $id,
							'sale_job_child_id' => $child,
							'created' => $this->date,
							'modified' => $this->date,
							'user_id' => $user_id,
							'status' => 0
							);
 
						//print_r($SaleJob['SaleTender']['id']);
						$this->ProjectBom->save($data);
						// After save
						$project_bom_id = $this->ProjectBom->getLastInsertId();
						$bom_id = $bom[$child][$i];
						$no_of_order = $quantity[$child][$i];
						 
						$this->copy_bom_child($id, $bom_id, $project_bom_id, $no_of_order);
						//debug($this->ProjectBom->validationErrors);  
					}

					if($is_bom[$child][$i] == 0) {
						// Insert sale_order_items
						$iteminfo = $this->item_info($item_id[$child][$i]); 
						$total_price = $quantity[$child][$i] * $unit_price[$child][$i]; 
						$this->SaleJobItem->create();  
						$data = array(
							'quantity' => $quantity[$child][$i], 
							'general_unit_id' => $general_unit_id[$child][$i],
							'unit_price' => 0,
							'discount' => 0,
							'tax' => 0,
							'total_price' => 0, 
							'bom_id' => 0, // Bukan bom
							'name' => $iteminfo['InventoryItem']['name'],
							'code' => $iteminfo['InventoryItem']['code'],
							'remark' => 'Remark',
							'sale_tender_id' => $SaleJob['SaleTender']['id'],
							'sale_job_id' => $id,
							'sale_job_child_id' => $child,
							'created' => $this->date,
							'modified' => $this->date,
							'user_id' => $user_id,
							'status' => 0, 
							'inventory_item_id' => $item_id[$child][$i],
							'type' => 2 
							);
						$this->SaleJobItem->save($data);
					} 
				} 
			}  

			$this->Session->setFlash(__('Job item has been saved.'), 'success');
			return $this->redirect(array('action' => 'index')); 
		} 

		$this->set('salejob', $SaleJob);
		$this->set('stations', $stations);

		$this->loadModel('InventoryItemCategory');
		$items = $this->InventoryItemCategory->find('all', array('recursive' => -1));

		// Load BOM For JS
		$this->loadModel('BomCategory');

		$categories = $this->BomCategory->find('all', array('recursive' => -1));
		$this->set('categories', $categories); 

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$saleTenders = $this->SaleJob->SaleTender->find('list');
		$saleQuotations = $this->SaleJob->SaleQuotation->find('list');  
		$users = $this->SaleJob->User->find('list');

		$this->set('items', $items);

		$this->set(compact('saleTenders', 'saleQuotations', 'users', 'units'));
	}

	private function get_bom($id) {
		$this->loadModel('Bom');
		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.inventory_item_id' => $id,
				'Bom.status' => 1
				),
			'order' => array(
				'Bom.id' => 'DESC'
				),
			'recursive' => -1
			));
		if($bom) {
			return $bom;
		} else {
			return false;
		}
	}

	private function total_price($unit_price, $qty, $discount, $tax) {
		$total = $unit_price * $qty;
		if($discount != 0 || $discount != '') {
			$total_discount = ($total / 100) * $discount; 
		} else {
			$total_discount = 0;
		}

		if($tax != 0 || $tax != '') {
			$total_tax = ($total_discount / 100) * $tax;
		} else {
			$total_tax = 0;
		}

		$amount = $total - $total_discount + $total_tax;

		return $amount;
	}

	private function item_info($id) {
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('first', array(
			'conditions' => array(
				'InventoryItem.id' => $id
				)
			));
		return $item;
	}

	private function copy_bom_child($sale_job_id, $bom_id, $project_bom_id, $no_of_order) {
		$this->loadModel('BomChild');
		$this->loadModel('ProjectBomChild');
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $bom_id
				),
			'group' => array('BomChild.id')
			));
		foreach ($childs as $child) {
			$this->ProjectBomChild->create();
			$data = array(
				'sale_job_id' => $sale_job_id,
				'project_bom_id' => $project_bom_id,
				'parent_id' => $child['BomChild']['parent_id'],
				'name' => $child['BomChild']['name'],
				'bom_parent' => $child['BomChild']['id'],
				'quantity' => $no_of_order
				);
			$this->ProjectBomChild->save($data);
			$project_bom_child_id = $this->ProjectBomChild->getLastInsertId();

			// Copy each child item
			$this->copy_bom_item($child['BomChild']['id'], $sale_job_id, $project_bom_child_id, $project_bom_id, $no_of_order);

		}
	}

	private function copy_bom_item($bom_child_id, $sale_job_id, $project_bom_child_id, $project_bom_id, $no_of_order) {
		$this->loadModel('BomItem');
		$this->loadModel('ProjectBomItem');
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $bom_child_id
				),
			'group' => array('BomItem.id')
			));
		$data = array();
		foreach ($items as $item) {
			
			$quantity_total = $item['BomItem']['quantity'] * $no_of_order;
			$price = $item['InventoryItem']['unit_price'] * $item['BomItem']['quantity'];
			$price_total = $price * $no_of_order;
			if($item['InventoryItem']['quantity_available'] >= $quantity_total) {
				$quantity_shortage = 0;
			} else {
				$quantity_shortage = $item['InventoryItem']['quantity_available'] - $quantity_total;
			}  
			$data[] = array(
				'sale_job_id' => $sale_job_id,
				'inventory_item_id' => $item['BomItem']['inventory_item_id'],
				'general_unit_id' => $item['BomItem']['inventory_item_id'],
				'project_bom_child_id' => $project_bom_child_id,
				'project_bom_id' => $project_bom_id,
				'quantity' => $item['BomItem']['quantity'],
				'quantity_total' => $quantity_total,
				'quantity_shortage' => abs($quantity_shortage),
				'price' => $item['InventoryItem']['unit_price'],
				'price_total' => $price_total,
				'no_of_order' => $no_of_order
				); 
		}
		$this->ProjectBomItem->saveAll($data, array('deep' => true));
	}

	private function reserve_item($item_id, $quantity, $shortage) {

	}

	private function bom_info($id) {
		$this->loadModel('Bom');
		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $id
				)
			));
		return $bom;
	}

	public function index($stat = NULL) { 
		if($stat == null) {
			$status = 4;
		} elseif($stat == 'waiting') {
			$status = array(1, 2, 3, 5);
		} else {
			$status = $stat;
		}
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array( 
			'conditions' => array(
				'SaleJob.status' => $status,
				'SaleJob.group_id' => $this->group_id
				),
			'order' => 'SaleJob.id DESC',
			'limit' => $record_per_page
			); 
		$this->set('saleJobs', $this->Paginator->paginate()); 
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleJob->exists($id)) {
			throw new NotFoundException(__('Invalid sale order'));
		}


		$options = array('conditions' => array('SaleJob.' . $this->SaleJob->primaryKey => $id));
		$this->set('saleJob', $this->SaleJob->find('first', $options));

		$this->loadModel('SaleJobChild');
		$childs = $this->SaleJobChild->find('all', array(
			'conditions' => array(
				'SaleJobChild.sale_job_id' => $id
				),
			'recursive' => 2
			));  

		$this->set('childs', $childs);

		if ($this->request->is('post')) {

		}
	}

/**
 * index method
 *
 * @return void

	public function index() {
		$this->SaleJob->recursive = 0;
		$this->set('saleJobs', $this->Paginator->paginate());
	}
 */
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 
	public function view($id = null) {
		if (!$this->SaleJob->exists($id)) {
			throw new NotFoundException(__('Invalid sale job'));
		}
		$options = array('conditions' => array('SaleJob.' . $this->SaleJob->primaryKey => $id));
		$this->set('saleJob', $this->SaleJob->find('first', $options));
	}
*/
/**
 * add method
 *
 * @return void

	public function add() {
		$user_id = $this->Session->read('Auth.User.id'); 

		if ($this->request->is('post')) {
			$this->SaleJob->create(); 

			$quotation = $this->SaleJob->SaleQuotation->find('first', array('conditions' => array(
				'SaleQuotation.id' => $this->request->data['SaleJob']['sale_quotation_id']
				)));   

			$this->request->data['SaleJob']['user_id'] = $user_id;
			$this->request->data['SaleJob']['sale_tender_id'] = $quotation['SaleQuotation']['sale_tender_id'];
			$this->request->data['SaleJob']['customer_id'] = $quotation['SaleQuotation']['customer_id'];

			if ($this->SaleJob->save($this->request->data)) {
				$this->Session->setFlash(__('The sale job has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale job could not be saved. Please, try again.'), 'error');
			}
		}
		$saleQuotations = $this->SaleJob->SaleQuotation->find('list');
		
		$customers = $this->SaleJob->Customer->find('list'); 
		$users = $this->SaleJob->User->find('list');
		$this->set(compact('customers', 'users', 'saleQuotations'));
	}
 */

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleJob->exists($id)) {
			throw new NotFoundException(__('Invalid sale job'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleJob->save($this->request->data)) {

				if($this->SaleJob->SaleJobChild->saveMany($this->request->data['SaleJobChild'])) {
					$this->Session->setFlash(__('The sale job has been saved.'), 'success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Job Station could not be saved. Please try again.'), 'error');
				}
				
			} else {
				$this->Session->setFlash(__('The sale job could not be saved. Please try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('SaleJob.' . $this->SaleJob->primaryKey => $id));
			$this->request->data = $this->SaleJob->find('first', $options);
		}
		$stations = $this->SaleJob->SaleJobChild->find('all', array(
			'conditions' => array(
				'SaleJobChild.sale_job_id' => $id
				),
			'order' => 'SaleJobChild.id ASC'
			));
		$this->set('stations', $stations);
		$saleTenders = $this->SaleJob->SaleTender->find('list');
		$saleQuotations = $this->SaleJob->SaleQuotation->find('list');   
		$users = $this->SaleJob->User->find('list'); 
 
		$Customer = $this->SaleJob->Customer->find('list', array(
			'order' => array('Customer.name' => 'ASC'), 
			'recursive' => -1
			));

		$this->set(compact('saleTenders', 'saleQuotations', 'users', 'Customer'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleJob->id = $id;
		if (!$this->SaleJob->exists()) {
			throw new NotFoundException(__('Invalid sale job'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->SaleJob->delete()) {
			$this->Session->setFlash(__('The sale job has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale job could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function ajaxfindjob() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$conditions['OR']['SaleJobChild.station_name LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['SaleJobChild.name LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['SaleJobChild.old_job_no LIKE'] = '%'.$term.'%';  
			$conditions['OR']['SaleJobChild.description LIKE'] = '%'.$term.'%';

			$items = $this->SaleJob->SaleJobChild->find('all', array(
				'conditions' => $conditions,
				'group' => array('SaleJobChild.id'),
				'order' => array('SaleJobChild.id' => 'ASC')
				));
			 
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['SaleJob']['id'], 
					'sale_job_child_id' => $item['SaleJobChild']['id'],
					'name' => $item['SaleJobChild']['station_name'], 
					'station' => $item['SaleJobChild']['name'], 
					'customer' => $this->get_customer($item['SaleJob']['customer_id']),
					'customer_id' => $item['SaleJob']['customer_id'], 
					'sale_tender_id' => $item['SaleJob']['sale_tender_id'], 
					'sale_quotation_id' => $item['SaleJob']['sale_quotation_id'],
					'old_job_no' => $item['SaleJobChild']['old_job_no']
					//'has_sub' => $this->get_sub_station($item['SaleJob']['id'])
					); 
			}
			echo json_encode($json);	
		}
	}

	public function ajaxfindjobitem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['id'])) {
			$this->loadModel('SaleJobItem');
			$id = $this->request->query['id'];
			$conditions = array();

			$conditions['SaleJobItem.sale_job_child_id'] = $id;
			$items = $this->SaleJobItem->find('all', array(
				'conditions' => $conditions, 
				'order' => array('SaleJobItem.id' => 'ASC')
				));
			 
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['SaleJobItem']['id'], 
					'inventory_item_id' => $item['SaleJobItem']['inventory_item_id'],
					'name' => $item['InventoryItem']['name'], 
					'code' => $item['InventoryItem']['code'] 
					); 
			}
			echo json_encode($json);	
		}
	}

	private function get_customer($id) {
		$this->loadModel('Customer');
		$customer = $this->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $id
				)
			));
		if($customer) {
			return $customer['Customer']['name'];
		}
	}

	public function ajaxfindstation() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$this->loadModel('SaleJobChild');
			$conditions['OR']['SaleJobChild.station_name LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['SaleJobChild.name LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['SaleJobChild.old_job_no LIKE'] = '%'.$term.'%';  
			$conditions['OR']['SaleJobChild.description LIKE'] = '%'.$term.'%';

			$items = $this->SaleJobChild->find('all', array(
				'conditions' => $conditions,
				'group' => array('SaleJobChild.id'),
				'order' => array('SaleJobChild.id' => 'DESC')
				));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['SaleJobChild']['id'], 
					'name' => $item['SaleJobChild']['name'], 
					'code' => $item['SaleJobChild']['station_name'],
					'customer_id' => $item['SaleJob']['customer_id'],
					'old_job_no' => $item['SaleJobChild']['old_job_no']
					); 
			}
			echo json_encode($json);	
		}
	}

	private function get_sub_station($job_id) {

		$conditions = array();
		$conditions['SaleJobChild.sale_job_id'] = $job_id; 
		$items = $this->SaleJob->SaleJobChild->find('all', array(
			'conditions' => $conditions 
			));
		$data = array();

		foreach ($items as $item) {
			$data[] = array(
				'id' => $item['SaleJobChild']['id'],
				'station_name' => $item['SaleJobChild']['station_name'],
				'name' => $item['SaleJobChild']['name']
				);
		}
		return $data;
	}
}
