<?php
App::uses('AppController', 'Controller');
/**
 * InventoryPurchaseOrders Controller
 *
 * @property InventoryPurchaseOrder $InventoryPurchaseOrder
 * @property PaginatorComponent $Paginator
 */
class InventoryPurchaseOrdersController extends AppController {
 
    public $components = array('Paginator', 'RequestHandler');

    public function beforeFilter() {
        parent::beforeFilter();
        // $this->spell_number(19.96);
    }
  

    public function view_pdf($id = null) { 
        $this->layout = 'print'; 
        if (!$this->InventoryPurchaseOrder->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase order'));
        }
        $this->loadModel('InventoryPurchaseOrderItem');
        $poitems =  $this->InventoryPurchaseOrderItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseOrderItem.inventory_purchase_order_id' => $id
                )
            ));  

        $this->set('poitems', $poitems);
        $this->loadModel('User');

        $hos = $this->User->find('first', array(
            'conditions' => array(
                'User.group_id' => 11,
                'User.role' => 'HOS'
                )
            ));

        $this->set('hos', $hos);

        // increase memory limit in PHP 
        ini_set('memory_limit', '512M'); 
        $this->loadModel('InventorySupplierItems'); 
         

        $this->loadModel('InventoryDeliveryLocation');
        $inventory_delivery_location = $this->InventoryDeliveryLocation->query("SELECT id,name FROM inventory_delivery_locations");

        $other = array('inventory_delivery_locations' => array('id'=>'x', 'name'=>'NEW DELIVERY LOCATION'));
        array_push($inventory_delivery_location, $other);
 

        $this->loadModel('InventorySupplier');
        $this->loadModel('State');
        $this->loadModel('Country');
        $states = $this->State->find('list');
        $countries = $this->Country->find('list');

        $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id));
        $this->set('inventoryPurchaseOrder', $this->InventoryPurchaseOrder->find('first', $options));

        $POrder = $this->InventoryPurchaseOrder->find('first', $options);
        //print_r($this->InventoryPurchaseOrder->find('first', $options));
        $invdelivery_location = $this->InventoryDeliveryLocation->query("SELECT * FROM inventory_delivery_locations WHERE id=".$POrder['InventoryPurchaseOrder']['inventory_delivery_location_id']);
        //print_r($invdelivery_location);
        //exit;
        
        /*
        $arr[] = array(
            'InventoryPurchaseOrder' => $POrder['InventoryPurchaseOrder'],
            'InventoryLocation'     => $POrder['InventoryLocation'],
            'User' => $POrder['User'],
            'Group' => $POrder['Group'],
            'GeneralPurchaseRequisitionType' => $POrder['GeneralPurchaseRequisitionType'],
            'InventorySupplier' => $POrder['InventorySupplier'],
            'TermOfPayment' => $POrder['TermOfPayment'],
            'InventoryDeliveryLocation' => $POrder['InventoryDeliveryLocation'],
            'InventoryPurchaseRequisition' => $POrder['InventoryPurchaseRequisition'], 
            'GeneralCurrency' => $this->get_currency($POrder['InventorySupplier']['general_currency_id']),
            'TermOfDelivery' => $POrder['TermOfDelivery'],
        );
        */

        $this->loadModel('InventoryPurchaseRequisition');
        $purchase_requisition = $this->InventoryPurchaseRequisition->find('first', array(
            'conditions' => array(
                'InventoryPurchaseRequisition.id' => $POrder['InventoryPurchaseOrder']['inventory_purchase_requisition_id']
                )
            ));

        $this->set('purchase_requisition', $purchase_requisition);

        //$POrder = $arr[0];

        $currency = $this->get_supplier_currency($POrder['InventorySupplier']['id']);

        $this->set('inventoryPurchaseOrder', $POrder);

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $sts = array('1'=>'Approved', '2'=>'Rejected');
        $this->set(compact('sts', 'states', 'countries', 'inventory_delivery_location', 'invdelivery_location', 'remark', 'warranty', 'terms', 'currency'));
        $this->set('post', $this->InventoryPurchaseOrder->read(null, $id));

        // Supplier
        $supplier = $this->InventoryPurchaseOrder->InventorySupplier->find('first', array(
            'conditions' => array(
                'InventorySupplier.id' => $POrder['InventoryPurchaseOrder']['inventory_supplier_id']
                )
            ));
        $this->set('supplier', $supplier); 

        
    }

    private function get_supplier_currency($id){
        $this->loadModel('InventorySupplier');
        $currency = $this->InventorySupplier->find('first', array(
            'conditions' => array(
                'InventorySupplier.id' => $id,
            ) 
        ));

        return $currency['GeneralCurrency'];
    }

    private function get_currency($id){
        $this->loadModel('GeneralCurrency');
        $currency = $this->GeneralCurrency->find('first', array(
            'conditions' => array(
                'GeneralCurrency.id' => $id,
            ) 
        ));

        return $currency['GeneralCurrency'];
    }
/**
 * index method
 *
 * @return void
 */
    public function view_pr($id = null){
        $this->loadModel('InventoryPurchaseRequisition');
        if (!$this->InventoryPurchaseRequisition->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition'));
        }

        if($this->request->is('post')){
            
            $this->loadModel('InventoryPurchaseOrder');
            $this->loadModel('InventoryPurchaseOrderItem');
            $this->InventoryPurchaseOrder->create();
            
            $id = $this->request->data['InventoryPurchaseRequisition']['id'];
            $purchase_requisition = $this->InventoryPurchaseRequisition->find('all', array(
                'conditions' => array(
                    'InventoryPurchaseRequisition.id' => $id 
                )
            ));

            //print_r($purchase_requisition);
            //exit;
            //print_r($data);
            //exit;

            if(empty($data)){
                $bil = '1';
            }else{
                $bil = $data[0]['inventory_purchase_orders']['id']+1;
            }

            $PO_no = $this->generate_code('PO', $bil);

            $options = array('conditions' => array('InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id));
            $this->request->data = $this->InventoryPurchaseRequisition->find('first', $options);
            //print_r($this->request->data);
            //exit;
            $data_pr = $this->request->data['InventoryPurchaseRequisition'];
            //print_r($data_pr);
            $data_po = array(
                'user_id' => $data_pr['user_id'], 
                'group_id' => $data_pr['group_id'], 
                'general_purchase_requisition_type_id' => $data_pr['general_purchase_requisition_type_id'], 
                'created' => date('Y-m-d H:i:s'), 
                'dateline' => $data_pr['dateline'], 
                'status' => $data_pr['status'], 
                'po_no' => $PO_no, 
                'inventory_supplier_id' => $this->request->data['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id'],
                'account_department_budget_id' => $data_pr['account_department_budget_id'],
                'term_of_payment_id' => $data_pr['term_of_payment_id'],
                'remark_id' => $data_pr['remark_id'],
                'warranty_id' => $data_pr['warranty_id'],
                'inventory_purchased_requisition_id' => $id
            );

            $data = $this->InventoryPurchaseOrder->query("SELECT inventory_purchased_requisition_id FROM inventory_purchase_orders WHERE inventory_purchased_requisition_id=".$id);
            $data_item_pr = $this->request->data['InventoryPurchaseRequisitionItem'];
            
            if(count($data)=='0') {
                if($this->InventoryPurchaseOrder->save($data_po)){
                    $data_item_pr = $this->request->data['InventoryPurchaseRequisitionItem'];

                    //print_r($data_item_pr);
                    //exit;
                    $count = count($this->request->data['InventoryPurchaseRequisitionItem']);
                    
                    $lastID = $this->InventoryPurchaseOrder->getLastInsertID();
                    for($i=0; $i<$count; $i++){
                        $this->InventoryPurchaseOrderItem->create();
                        //print_r($data_item_pr[$i]);
                        $data_po_item = array(
                            'inventory_supplier_id' => $data_item_pr[$i]['inventory_supplier_id'],
                            'inventory_supplier_item_id' => $data_item_pr[$i]['inventory_supplier_item_id'], 
                            'inventory_purchase_order_id' => $lastID,
                            'inventory_item_id' => $data_item_pr[$i]['inventory_item_id'],
                            'quantity' => $data_item_pr[$i]['quantity'],
                            'general_unit_id' => $data_item_pr[$i]['general_unit_id'],
                            'price_per_unit' => $data_item_pr[$i]['price_per_unit'],
                            'amount' => $data_item_pr[$i]['amount'],
                            'discount' => $data_item_pr[$i]['discount'],
                            'discount_type' => $data_item_pr[$i]['discount_type'],
                            'tax'   => $data_item_pr[$i]['tax']
                        );

                        //print_r($data_po_item);
                        //exit;
                        $this->InventoryPurchaseOrderItem->save($data_po_item);
                    
                    }

                    
                    $this->Session->setFlash(__('The Purchase Order has been created.'), 'success');
                    return $this->redirect(array('controller'=>'inventory_purchase_orders', 'action' => 'index'));
                }else{
                    $this->Session->setFlash(__('The Purchase Order could not be created. Please, try again.'), 'error');
                    return $this->redirect(array('controller'=>'inventory_purchase_orders', 'action' => 'index'));
                }
            }else{
                $this->Session->setFlash(__('The Purchase Order already created.'), 'error');
                return $this->redirect(array('controller'=>'inventory_purchase_orders', 'action' => 'index'));
            }

        }

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');

        $options = array('conditions' => array('InventoryPurchaseRequisition.' . $this->InventoryPurchaseRequisition->primaryKey => $id));
        $this->set('inventoryPurchaseRequisition', $this->InventoryPurchaseRequisition->find('first', $options));
        
        $n = $this->InventoryPurchaseRequisition->find('first', $options);
        //print_r($n);

        $this->loadModel('InternalDepartment');
        $internal = $this->InternalDepartment->query("SELECT * FROM internal_departments WHERE user_id=".$_SESSION['Auth']['User']['id']." AND group_id=".$_SESSION['Auth']['User']['group_id']);

        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->loadModel('AccountDepartmentBudgets');
        $budget = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets WHERE id=".$n['InventoryPurchaseRequisition']['account_department_budget_id']);

        $this->loadModel('InventorySupplierItems');
        $inventory_item = $this->InventorySupplierItems->find('all', array(
            'conditions' => array('InventorySupplierItems.inventory_supplier_id' => $n['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id']),
            //'fields' => array('invitem.*', 'InventorySupplierItems.*')  
        ));

        $this->loadModel('InventoryPurchaseRequisitionItem');
        $items = $this->InventoryPurchaseRequisitionItem->find('all', array(
            'conditions' => array('InventoryPurchaseRequisitionItem.inventory_purchase_requisition_id' => $id),
            //'fields' => array('invitem.*', 'InventorySupplierItems.*')  
        ));

        if(!empty($internal)){
            if($internal[0]['internal_departments']['id'] == '2'){
                $role = "HOD";
            }else if($internal[0]['internal_departments']['id'] == '1'){
                $role = "HOS";
            }else{
                $role = "Staff";
            }
        }else{
            $role = "Staff";
        }

        $sts = array('1'=>'Approved', '2'=>'Rejected');
        $this->set(compact('generalUnits', 'sts', 'budget', 'terms', 'warranty', 'remark', 'role'));

        $this->set('items', $items);

    }

    public function create($status = null) {
        if(isset($_GET['search'])) {
            //print_r($_GET);
            if($_GET['status'] == 'x'){
                $_GET['status'] == '';
            }
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryPurchaseOrder.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseOrder.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['status'] != '' && $_GET['status'] != 'x') {
                $conditions['InventoryPurchaseOrder.purchase_status LIKE'] = $_GET['status'];
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions = '';
            }            
        }
        else{
            $conditions = '';
        }

        if($status != null) {
            if($status == 1) {
                $conditions['InventoryPurchaseOrder.purchase_status'] = 1;
                $conditions['InventoryPurchaseOrder.status'] = 0;
            }
            if($status == 2) {
                $conditions['InventoryPurchaseOrder.purchase_status'] = 1;
                $conditions['InventoryPurchaseOrder.status'] = 1;
            }
            if($status == 3) {
                $conditions['InventoryPurchaseOrder.purchase_status'] = 1;
                $conditions['InventoryPurchaseOrder.status'] = 2;
            }
        } else {
            $conditions['InventoryPurchaseOrder.purchase_status'] = 0;
        }
 
        //$conditions = '';
        /*$ipo = $this->InventoryPurchaseOrder->find('all', array(
            'conditions' => array(
                'InventoryPurchaseOrder.id' => $id
            )
        ));
        $po_array = array();
        foreach ($ipo as $po) {
            $po_array[] = array(
                'InventoryPurchaseOrder'
                'User'
                'Group'
                'GeneralPurchaseRequisitionType'
                'InventorySupplier'
                'TermOfPayment'
                'InventoryDeliveryLocation'
                'InventoryPurchaseRequisitionBom'
            );
        }*/

        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array('conditions' => $conditions,'order'=>'InventoryPurchaseOrder.id DESC','limit'=>$record_per_page);

        $po = $this->Paginator->paginate();
 
        $this->set('inventoryPurchaseOrders', $po);

        //$this->update_end_user();
    }

    private function update_end_user() {
        $pos = $this->InventoryPurchaseOrder->find('all', array(
            'recursive' => 1,
            'conditions' => array(
                'InventoryPurchaseOrder.user_id !=' => 0
                )
            ));

        foreach ($pos as $po) {
            $end_user = $po['InventoryPurchaseRequisition']['user_id'];
            $update = $this->InventoryPurchaseOrder->updateAll(
                array(
                    'InventoryPurchaseOrder.end_user' => "$end_user"
                    ),
                array(
                    'InventoryPurchaseOrder.id' => $po['InventoryPurchaseOrder']['id']
                    )
                );
            if(!$update) {
                //debug();
            }
        }
    }

    public function index() {
        /*if(isset($_GET['search'])) {
            //print_r($_GET);
            if($_GET['status'] == 'x'){
                $_GET['status'] == '';
            }
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryPurchaseOrder.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseOrder.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['status'] != '' && $_GET['status'] != 'x') {
                $conditions['InventoryPurchaseOrder.purchase_status LIKE'] = $_GET['status'];
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions = '';
            }            
        }
        else{
            $conditions = '';
        }*/

        $this->loadModel('InventoryPurchaseRequisition');
        $pr_array = array();
    

        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array(
            'conditions' => array(
                'InventoryPurchaseRequisition.status' => 9
            )
        );
        $ipr = $this->Paginator->paginate('InventoryPurchaseRequisition');
        foreach ($ipr as $pr) {
            $pr_array[] = array(
                'InventoryPurchaseRequisition' => $pr,
                'Supplier' =>$this->get_supplier($pr['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id'])
            );$pr;
        }
        $this->set('inventoryPurchaseOrder', $pr_array);

        //$this->InventoryPurchaseOrder->recursive = 0;
        //$this->set('inventoryPurchaseOrder', $this->Paginator->paginate());

    }

    public function pending() {

        if(isset($_GET['search'])) {
            //print_r($_GET);
            if($_GET['status'] == 'x'){
                $_GET['status'] == '';
            }
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryPurchaseOrder.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseOrder.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['status'] != '' && $_GET['status'] != 'x') {
                $conditions['InventoryPurchaseOrder.purchase_status LIKE'] = $_GET['status'];
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions['InventoryPurchaseOrder.status'] = '8';
            }            
        }
        else{
            $conditions['InventoryPurchaseOrder.status'] = '8';
        }

                
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseOrder.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseOrder', $this->Paginator->paginate());
    }

    public function approved() {
        if(isset($_GET['search'])) {
            //print_r($_GET);
            if($_GET['status'] == 'x'){
                $_GET['status'] == '';
            }
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryPurchaseOrder.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseOrder.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['status'] != '' && $_GET['status'] != 'x') {
                $conditions['InventoryPurchaseOrder.purchase_status LIKE'] = $_GET['status'];
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions['InventoryPurchaseOrder.status'] = '9'; 
            }            
        }
        else{
            $conditions['InventoryPurchaseOrder.status'] = '9'; 
        }
               
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseOrder.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseOrder', $this->Paginator->paginate());
    }

    public function rejected() {
        if(isset($_GET['search'])) {
            //print_r($_GET);
            if($_GET['status'] == 'x'){
                $_GET['status'] == '';
            }
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryPurchaseOrder.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseOrder.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['status'] != '' && $_GET['status'] != 'x') {
                $conditions['InventoryPurchaseOrder.purchase_status LIKE'] = $_GET['status'];
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions['InventoryPurchaseOrder.status'] = '9'; 
            }            
        }
        else{
            $conditions['InventoryPurchaseOrder.status'] = '0'; 
        }
               
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseOrder.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseOrder', $this->Paginator->paginate());
    }

    public function pending_2() {
        if(isset($_GET['search'])) {
            //print_r($_GET);
            if($_GET['status'] == 'x'){
                $_GET['status'] == '';
            }
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryPurchaseOrder.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseOrder.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['status'] != '' && $_GET['status'] != 'x') {
                $conditions['InventoryPurchaseOrder.purchase_status LIKE'] = $_GET['status'];
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions['InventoryPurchaseOrder.status'] = '1'; 
                $conditions['InventoryPurchaseOrder.purchase_status'] = '0'; 
            }            
        }
        else{
            $conditions['InventoryPurchaseOrder.status'] = '9'; 
            $conditions['InventoryPurchaseOrder.purchase_status'] = '0'; 
        }
              
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseOrder.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseOrder', $this->Paginator->paginate());
    }

    public function purchased() {
        if(isset($_GET['search'])) {
            //print_r($_GET);
            if($_GET['status'] == 'x'){
                $_GET['status'] == '';
            }
            //exit;
            if($_GET['po_no'] != '') {
                $conditions['InventoryPurchaseOrder.po_no LIKE'] = "%".$_GET['po_no']."%";
            }

            if($_GET['date_start'] != '' && $_GET['date_end'] != '') {
                $conditions['InventoryPurchaseOrder.created BETWEEN ? AND ?'] = array($_GET['date_start'],$_GET['date_end']);
            }

            if($_GET['status'] != '' && $_GET['status'] != 'x') {
                $conditions['InventoryPurchaseOrder.purchase_status LIKE'] = $_GET['status'];
            }

            if($_GET['po_no'] == '' && $_GET['date_start'] == '' && $_GET['date_end'] == '' && $_GET['status'] == 'x'){
                $conditions['InventoryPurchaseOrder.purchase_status'] = '1';
            }            
        }
        else{
            $conditions['InventoryPurchaseOrder.purchase_status'] = '1'; 
        }
        //$conditions['InventoryPurchaseOrder.purchase_status'] = '1';        
        $record_per_page = Configure::read('Reading.nodes_per_page');

        $this->Paginator->settings = array('conditions' => $conditions,   
            'order'=>'InventoryPurchaseOrder.id DESC',
            'limit'=>$record_per_page
            ); 
        $this->set('inventoryPurchaseOrder', $this->Paginator->paginate());
    }

    private function get_supplier($id){
        $this->loadModel('InventorySupplier');
        $supplier = $this->InventorySupplier->find('all', array(
            'conditions' => array(
                'InventorySupplier.id' => $id
            ),
            'recursive' => -1
        ));

        return $supplier;
    }

    public function ajaxfindrack() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('InventoryRack');
        if(isset($this->request->query['id'])) {
            $id = (int)$this->request->query['id'];

            $racks = $this->InventoryRack->find('all', array('conditions' => array('InventoryRack.inventory_store_id' => $id), 'recursive' =>-1));

            echo json_encode($racks);  
        }
    }

    public function ajaxfindpo() {
        $this->autoRender = false;
        $this->layout = 'ajax'; 
        
        if(isset($this->request->query['id'])) {
            $id = (int)$this->request->query['id'];
             
            $items = $this->InventoryPurchaseOrder->find('all', array('conditions' => array('InventoryPurchaseOrder.id' => $id)));

            $json = array();
            foreach ($items as $item) {
                $json[] = array(
                    'InventorySupplier' => $item['InventorySupplier'],
                    'TermOfPayment' => $item['TermOfPayment'],
                    'Remark' => $this->get_remark($item['InventoryPurchaseOrder']['remark_id']),
                    'Warranty' => $this->get_warranty($item['InventoryPurchaseOrder']['warranty_id']),
                    'InventoryDeliveryLocation' => $item['InventoryDeliveryLocation'],
                    'InventoryLocation' => $item['InventoryLocation'],
                    'InventoryPurchaseOrder' => $item['InventoryPurchaseOrder'],
                    'InventoryPurchaseOrderItem' => $this->getpoitem($item['InventoryPurchaseOrder']['id']),
                    'Store' =>$this->get_store(),
                    );
            }
            
            //print_r($json);
            echo json_encode($json);    
        }
    }

    private function get_store($id=null){
        $this->loadModel('InventoryStore');
        $store = $this->InventoryStore->find('all', array(
            'order' => 'InventoryStore.name ASC'
        ));
        $arr = array();
        foreach ($store as $s) {
            $arr[] = array(
                'id' => $s['InventoryStore']['id'],
                'name' => $s['InventoryStore']['name'],
            );
        }

        return $arr; 

    }

    private function get_remark($id){
        $this->loadModel('Remark');
        $remark = $this->Remark->find('first', array(
            'conditions' => array(
                'Remark.id' => $id
            ),
            'recursive' => -1
        ));

        return $remark['Remark'];

    }

    private function get_warranty($id){
        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('first', array(
            'conditions' => array(
                'Warranty.id' => $id
            ),
            'recursive' => -1
        ));
        if(empty($warranty)){
            return $warranty;
        }else{
            return $warranty['Warranty'];
        }
    }

    private function getpoitem($id){
        $this->loadModel('InventoryPurchaseOrderItem');
        $poitem = $this->InventoryPurchaseOrderItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseOrderItem.inventory_purchase_order_id' => $id
            )
        ));

        return $poitem;

    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function verifyview($id = null) {
        if (!$this->InventoryPurchaseOrder->exists($id)) {
            throw new NotFoundException(__('Invalid purchase order request'));
        }

        $group = $this->Session->read('Auth.User.group_id');
        
        if($group != 1) {
            $this->loadModel('Approval');
            $approval = $this->Approval->find('first', array(
                'conditions' => array(
                    'Approval.user_id' => $this->user_id,
                    'Approval.name' => 'Purchase Order'
                    )
                )); 
            if($approval) {
                $conditions['User.group_id'] = $approval['Approval']['group_id'];
                $options = array('conditions' => array(
                    'InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id,
                    'User.group_id' => $approval['Approval']['group_id']
                    ));
            } else {
                $this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
        } else {
            $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id), 'recursive' => 2);
        }

        if ($this->request->is(array('put', 'post'))) {  
            if($this->InventoryPurchaseOrder->save($this->request->data)) {                    
                $this->Session->setFlash(__('The purchase order status has been saved.'), 'success');
                return $this->redirect(array('action' => 'create'));
            } else {
                $this->Session->setFlash(__('The purchase status status cannot be saved.'), 'error');
            }
        }

        $this->loadModel('State');
        $this->loadModel('Country');
        $states = $this->State->find('list');
        $countries = $this->Country->find('list');

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');


        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->set(compact('generalUnits', 'terms', 'warranty', 'remark', 'states', 'countries'));

        //$this->set('items', $items);

        
        $this->set('inventoryPurchaseOrders', $this->InventoryPurchaseOrder->find('first', $options));
    }

    public function view_purchase($id = null) {
        if (!$this->InventoryPurchaseOrder->exists($id)) {
            throw new NotFoundException(__('Invalid purchase order request'));
        }

        $group = $this->Session->read('Auth.User.group_id');
        
        if($group != 1) {
            $this->loadModel('Approval');
            $approval = $this->Approval->find('first', array(
                'conditions' => array(
                    'Approval.user_id' => $this->user_id,
                    'Approval.name' => 'Purchase Order'
                    )
                )); 
            if($approval) {
                $conditions['User.group_id'] = $approval['Approval']['group_id'];
                $options = array('conditions' => array(
                    'InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id,
                    'User.group_id' => $approval['Approval']['group_id']
                    ));
            } else {
                $this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
        } else {
            $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id), 'recursive' => 2);
        }

        if ($this->request->is(array('put', 'post'))) {  
            if($this->InventoryPurchaseOrder->save($this->request->data)) {                    
                $this->Session->setFlash(__('The purchase order status has been saved.'), 'success');
                return $this->redirect(array('action' => 'create'));
            } else {
                $this->Session->setFlash(__('The purchase status status cannot be saved.'), 'error');
            }
        }

        $this->loadModel('State');
        $this->loadModel('Country');
        $states = $this->State->find('list');
        $countries = $this->Country->find('list');

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');


        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->set(compact('generalUnits', 'terms', 'warranty', 'remark', 'states', 'countries'));

        //$this->set('items', $items);

        
        $this->set('inventoryPurchaseOrders', $this->InventoryPurchaseOrder->find('first', $options));
    }

    public function view_verify($id = null) {
        if (!$this->InventoryPurchaseOrder->exists($id)) {
            throw new NotFoundException(__('Invalid purchase order request'));
        } 

        $this->loadModel('AccountDepartmentBudget');
 
        $group = $this->Session->read('Auth.User.group_id');

        $this->loadModel('InventoryDeliveryOrder');
        $this->loadModel('InventoryDeliveryOrderItem');

        $grns = $this->InventoryDeliveryOrder->find('all', array(
            'conditions' => array(
                'InventoryDeliveryOrder.inventory_purchase_order_id' => $id
                )
            ));
        $grn_id = array();
        if($grns) {
            foreach ($grns as $grn) {
                $grn_id[] = $grn['InventoryDeliveryOrder']['id'];
            }    
        }

        $grn_items = $this->InventoryDeliveryOrderItem->find('all', array(
            'conditions' => array(
                'InventoryDeliveryOrderItem.inventory_delivery_order_id' => $grn_id
                )
            ));

        $this->set('grn_items', $grn_items);
        
        
        /*
        if($group != 1) {
            $this->loadModel('Approval');
            $approval = $this->Approval->find('first', array(
                'conditions' => array(
                    'Approval.user_id' => $this->user_id,
                    'Approval.name' => 'Purchase Order'
                    )
                )); 
            if($approval) {
                $conditions['User.group_id'] = $approval['Approval']['group_id'];
                $options = array('conditions' => array(
                    'InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id,
                    'User.group_id' => $approval['Approval']['group_id']
                    ));
            } else {
                $this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
                return $this->redirect(array('action' => 'index')); 
            }
        } else {
          
        } 
        */
        $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id), 'recursive' => 2);

        $po = $this->InventoryPurchaseOrder->find('first', $options);

        if ($this->request->is(array('put', 'post'))) {   
            // deduct budget
            if($po['InventoryPurchaseOrder']['general_purchase_requisition_type_id'] == 1) {
                $total_rm_no_tax = $this->request->data['InventoryPurchaseOrder']['total_rm_no_tax'];
                $budget_id = $po['InventoryPurchaseOrder']['account_department_budget_id']; 
                
                // Manual PPP
                // Deduct budget
                $budget = $this->AccountDepartmentBudget->find('first', array(
                    'conditions' => array(
                        'AccountDepartmentBudget.id' => $budget_id
                        )
                    ));

                if($budget['AccountDepartmentBudget']['balance'] < $total_rm_no_tax) {
                    $this->Session->setFlash(__('Purchase Order cannot be save. Please add Budget.'), 'error');
                } else {
                    // Update budget
                    $used = $budget['AccountDepartmentBudget']['used'] - $total_rm_no_tax;
                    $balance = $budget['AccountDepartmentBudget']['balance'] - $total_rm_no_tax;

                    $updateBudget = $this->AccountDepartmentBudget->updateAll(
                        array(
                            'AccountDepartmentBudget.used' => "$used",
                            'AccountDepartmentBudget.balance' => "$balance",
                            ),
                        array(
                            'AccountDepartmentBudget.id' => $budget_id
                            )
                        );

                    if($updateBudget) {
                        // Insert history
                        $this->loadModel('AccountDepartmentBudgetHistory');

                        $this->AccountDepartmentBudgetHistory->create();
                        $history = array(
                            'account_department_budget_id' => $budget_id,
                            'inventory_purchase_order_id' => $id,
                            'amount_before' => $budget['AccountDepartmentBudget']['balance'],
                            'amount_used' => $total_rm_no_tax,
                            'amount_balance' => $balance,
                            'created' => $this->date,
                            'user_id' => $this->user_id,
                            'end_user' => $po['InventoryPurchaseRequisition']['user_id']
                            );
                        $saveHistory = $this->AccountDepartmentBudgetHistory->save($history);
                        if($saveHistory) {
                            $pr_id = $po['InventoryPurchaseRequisition']['id'];
                            $created = $this->count_po_by_pr_id($pr_id, 0);
                            $purchased = $this->count_po_by_pr_id($pr_id, 1);

                            // Update PR Status
                            $this->loadModel('InventoryPurchaseRequisition');

                            /*
                            $this->InventoryPurchaseRequisition->updateAll(
                                array(
                                    'InventoryPurchaseRequisition.status' => "12"
                                    )
                                );
                            */

                           // If Non PO, Then closed
                            $this->request->data['InventoryPurchaseOrder']['purchase_status'] = 1;
                            $this->request->data['InventoryPurchaseOrder']['id'] = $id;

                            if($po['InventoryPurchaseRequisition']['po_type'] != 1) {
                                $this->request->data['InventoryPurchaseOrder']['status'] = 2;
                            }
                            if($this->InventoryPurchaseOrder->save($this->request->data)) { 

                                // If Non PO, Then closed
                                $this->Session->setFlash(__('Purchase Order has been saved to Purchased successfully.'), 'success');
                                return $this->redirect(array('action' => 'create'));
                            } else {
                                $this->Session->setFlash(__('The purchase status status cannot be saved. Error Code: PO874'), 'error');
                            }
                        } else {
                            $this->Session->setFlash(__('The purchase status status cannot be saved. Error Code: PO877'), 'error');
                        }
                    } else {
                        $this->Session->setFlash(__('There is problem to deduct budget. Error Code: PO882'), 'error');
                    }
                }

                 
            } else {
                // Auto MRP
                // Find budget item by array
                // Find acc budget by budget item job
                // Deduct budget by amount in budget item 
                $post_jobs = $this->request->data['InventoryPurchaseOrder']['planning_item_jobs'];
                $total_rm_no_tax = $this->request->data['InventoryPurchaseOrder']['total_rm_no_tax'];
                $budget_used = array();
                for($i = 0; $i < count($post_jobs); $i++) { 
                    
                    $jobs = explode(',', $post_jobs[$i]);

                    $job_id = array();
                    foreach ($jobs as $job) {
                        if($job != '') {
                            $job_id[] = trim($job);
                        }
                    }

                    $this->loadModel('PlanningItemJob');
                    $planning_item_jobs = $this->PlanningItemJob->find('all', array(
                        'conditions' => array(
                            'PlanningItemJob.id' => $job_id
                            )
                        ));

                    
                    foreach($planning_item_jobs as $item_job) {
                        //var_dump($item_job['PlanningItemJob']['id']);
                        $budget_used[] = $item_job['PlanningItemJob']['total_price_rm'];
                        $deduct = $this->deduct_job_budget($item_job['PlanningItemJob']['sale_job_child_id'], $item_job['PlanningItemJob']['total_price_rm'], $po);
                        if($deduct === false) {
                            die('Error 917');
                        } 
                    } 
                } 

                // If Non PO, Then closed
                $this->request->data['InventoryPurchaseOrder']['purchase_status'] = 1;
                if($po['InventoryPurchaseRequisition']['po_type'] != 1) {
                    $this->request->data['InventoryPurchaseOrder']['status'] = 2;
                }
                if($this->InventoryPurchaseOrder->save($this->request->data)) { 
                    
                    $this->Session->setFlash(__('Purchase Order has been saved successfully.'), 'success');
                    return $this->redirect(array('action' => 'create'));
                } else {
                    $this->Session->setFlash(__('The purchase status status cannot be saved.'), 'error');
                }
                 
            }  
        }

        // Items
        $items = $this->InventoryPurchaseOrder->InventoryPurchaseOrderItem->find('all', array(
            'conditions' => array(
                'InventoryPurchaseOrderItem.inventory_purchase_order_id' => $id
                )
            ));
         

        $this->set('items', $items);

        $this->loadModel('State');
        $this->loadModel('Country');
        $states = $this->State->find('list');
        $countries = $this->Country->find('list');

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');


        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->set(compact('generalUnits', 'terms', 'warranty', 'remark', 'states', 'countries'));

        //$this->set('items', $items);

        $budget = $this->AccountDepartmentBudget->find('first', array(
            'conditions' => array(
                'AccountDepartmentBudget.id' => $po['InventoryPurchaseOrder']['account_department_budget_id']
                )
            ));

        $this->set('budget', $budget);
        $this->set('inventoryPurchaseOrders', $po);

        // Supplier
        $supplier = $this->InventoryPurchaseOrder->InventorySupplier->find('first', array(
            'conditions' => array(
                'InventorySupplier.id' => $po['InventoryPurchaseOrder']['inventory_supplier_id']
                )
            ));
        $this->set('supplier', $supplier);
    }

    private function count_po_by_pr_id($pr_id, $status) {
        $countPo = $this->InventoryPurchaseOrder->find('all', array(
        'conditions' => array(
            'InventoryPurchaseOrder.inventory_purchase_requisition_id' => $pr_id,
            'InventoryPurchaseOrder.purchase_status' => $status
            )
        )); 
        return count($countPo);
    }


    private function deduct_job_budget($sale_job_child_id, $total_rm_no_tax, $po) {
        $this->loadModel('AccountDepartmentBudget');
        $budget = $this->AccountDepartmentBudget->find('first', array(
            'conditions' => array(
                'AccountDepartmentBudget.sale_job_child_id' => $sale_job_child_id,
                'AccountDepartmentBudget.is_material' => 1
                )
            ));
        $used = $budget['AccountDepartmentBudget']['used'] - $total_rm_no_tax;
        $balance = $budget['AccountDepartmentBudget']['balance'] - $total_rm_no_tax;

        $updateBudget = $this->AccountDepartmentBudget->updateAll(
            array(
                'AccountDepartmentBudget.used' => "$used",
                'AccountDepartmentBudget.balance' => "$balance",
                ),
            array(
                'AccountDepartmentBudget.id' => $budget['AccountDepartmentBudget']['id']
                )
            );
        if($updateBudget) {
            $this->loadModel('AccountDepartmentBudgetHistory'); 
            $this->AccountDepartmentBudgetHistory->create();
            $history = array(
                'account_department_budget_id' => $budget['AccountDepartmentBudget']['id'],
                'inventory_purchase_order_id' => $po['InventoryPurchaseOrder']['id'],
                'amount_before' => $budget['AccountDepartmentBudget']['balance'],
                'amount_used' => $total_rm_no_tax,
                'amount_balance' => $balance,
                'created' => $this->date,
                'user_id' => $this->user_id,
                'end_user' => $po['InventoryPurchaseOrder']['user_id']
                );
            $saveHistory = $this->AccountDepartmentBudgetHistory->save($history);
            if($saveHistory) {
                return true;
            }
        }
        return false;
    }

    public function view_rejected($id = null) {
        if (!$this->InventoryPurchaseOrder->exists($id)) {
            throw new NotFoundException(__('Invalid purchase order request'));
        }

        $group = $this->Session->read('Auth.User.group_id');
        
        if($group != 1) {
            $this->loadModel('Approval');
            $approval = $this->Approval->find('first', array(
                'conditions' => array(
                    'Approval.user_id' => $this->user_id,
                    'Approval.name' => 'Purchase Order'
                    )
                )); 
            if($approval) {
                $conditions['User.group_id'] = $approval['Approval']['group_id'];
                $options = array('conditions' => array(
                    'InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id,
                    'User.group_id' => $approval['Approval']['group_id']
                    ));
            } else {
                $this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
        } else {
            $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id), 'recursive' => 2);
        }

        if ($this->request->is(array('put', 'post'))) {  
            $data = array(
                'id' => $this->request->data['InventoryPurchaseOrder']['id'],
                'purchase_status' => 1
            );
            if($this->InventoryPurchaseOrder->save($data)) {                    
                $this->Session->setFlash(__('The purchase order status has been saved.'), 'success');
                return $this->redirect(array('action' => 'create'));
            } else {
                $this->Session->setFlash(__('The purchase status status cannot be saved.'), 'error');
            }
        }

        $this->loadModel('State');
        $this->loadModel('Country');
        $states = $this->State->find('list');
        $countries = $this->Country->find('list');

        $this->loadModel('GeneralUnit');
        $generalUnits = $this->GeneralUnit->find('list');


        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');

        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->set(compact('generalUnits', 'terms', 'warranty', 'remark', 'states', 'countries'));

        //$this->set('items', $items);

        
        $this->set('inventoryPurchaseOrders', $this->InventoryPurchaseOrder->find('first', $options));
    }


    public function view($id = null) {
        if (!$this->InventoryPurchaseOrder->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase order'));
        }

        if ($this->request->is(array('post', 'put'))) {   

            if($this->InventoryPurchaseOrder->save($this->request->data)){
                $this->Session->setFlash(__('The Purchase Order has been updated.'), 'success');
                return $this->redirect(array('action' => 'view_verify', $id));
            } else {
                $this->Session->setFlash(__('The Purchase order could not be update. Please, try again.'), 'error');
            } 
        }

        $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id), 'recursive' => 2);
        $data = $this->InventoryPurchaseOrder->find('first', $options);
        $this->request->data = $data;
        $this->set('inventoryPurchaseOrder', $data);
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {
            
            $this->InventoryPurchaseOrder->create();
            if ($this->InventoryPurchaseOrder->save($this->request->data)) {
                $this->Session->setFlash(__('The inventory purchase order has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The inventory purchase order could not be saved. Please, try again.'));
            }
        }
        $inventorySuppliers = $this->InventoryPurchaseOrder->InventorySupplier->find('list');
        $users = $this->InventoryPurchaseOrder->User->find('list');
        $this->set(compact('inventorySuppliers', 'users'));
    } 

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        if (!$this->InventoryPurchaseOrder->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase order'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->InventoryPurchaseOrder->save($this->request->data)) {
                $this->insert_log($this->user_id, 'Edit PO', '/inventory_purchase_orders/view_verify/'.$id, 'Edit PO');
                $this->Session->setFlash(__('The purchase order has been saved.'), 'success');
                return $this->redirect(array('action' => 'view_verify', $id));
            } else {
                $this->Session->setFlash(__('The inventory purchase order could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id));
            $this->request->data = $this->InventoryPurchaseOrder->find('first', $options);
        }
        $inventorySuppliers = $this->InventoryPurchaseOrder->InventorySupplier->find('list');
        $users = $this->InventoryPurchaseOrder->User->find('list');

        $this->loadModel('InventoryLocation');
        $loc = $this->InventoryLocation->find('list');

        $this->loadModel('TermOfDelivery');
        $tod = $this->TermOfDelivery->find('list');

        $this->loadModel('TermOfPayment');
        $terms = $this->TermOfPayment->find('list');

        $this->loadModel('Warranty');
        $warranty = $this->Warranty->find('list');
        
        $this->loadModel('Remark');
        $remark = $this->Remark->find('list');

        $this->set(compact('terms', 'warranty', 'remark', 'states', 'countries', 'tod', 'loc'));
        
        $this->set(compact('inventorySuppliers', 'users'));
    }

    public function ajaxaddress() {
        $this->autoRender = false;
        $this->layout = 'ajax'; 
        if(isset($this->request->query['add_id'])) {
            $id = (int)$this->request->query['add_id'];
            $this->loadModel('InventoryDeliveryLocation');
            $delivery_location = $this->InventoryDeliveryLocation->query("SELECT * FROM inventory_delivery_locations WHERE id=".$id);
            $json = array();

            foreach ($delivery_location as $location) {
                /*$this->loadModel('Country');
                $country = $this->Country->query("SELECT * FROM countries WHERE id=".$location['inventory_delivery_locations']['country_id']);

                $this->loadModel('State');
                $state = $this->State->query("SELECT * FROM states WHERE id=".$location['inventory_delivery_locations']['state_id']);*/

                $json[] = array('id' => $location['inventory_delivery_locations']['id'], 'address' => $location['inventory_delivery_locations']['address'], 'contact_person' => $location['inventory_delivery_locations']['contact_person'], 'contact_number' => $location['inventory_delivery_locations']['contact_num'], 'postcode' => $location['inventory_delivery_locations']['postcode'], 'country_id' => $location['inventory_delivery_locations']['country_id'], 'state_id' => $location['inventory_delivery_locations']['state_id'], 'name' => $location['inventory_delivery_locations']['name']);
            }
            echo json_encode($json);    
        }
        
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

    public function list_history($id = null){
        $this->loadModel('InventoryDeliveryOrderItem');
        $items = $this->InventoryDeliveryOrderItem->find('all', array(
            'conditions' => array(
                'InventoryDeliveryOrderItem.inventory_item_id' => $id
            ),
           'recursive' => -1
        ));
        $supplier_item_array = array();
        foreach ($items as $item) {
            $supplier_item_array[] = array( 
                'InventoryDeliveryOrder' => $this->get_ipo($item['InventoryDeliveryOrderItem']['inventory_delivery_order_id']),
                'InventoryDeliveryOrderItem' => $item['InventoryDeliveryOrderItem'],
                'InventoryItem' => $this->get_inv_item($item['InventoryDeliveryOrderItem']['inventory_item_id'])
            );
        }

        $this->loadModel('InventoryItem');
        $item = $this->InventoryItem->find('all', array(
            'conditions' => array(
                'InventoryItem.id' => $id
            ),
            'recursive' => -1
        ));

        $item_detail = $item[0];

        
        $this->set('supplier_item', $supplier_item_array);
        $this->set('item', $item_detail);
    }

    private function get_ipo($id){

        $ipo = $this->InventoryPurchaseOrder->find('first', array(
            'conditions' => array(
                'InventoryPurchaseOrder.id' => $id,

            ),
            
        ));
        //print_r($ipo);
        //exit;
        $ipo_array = array();
        $ipo_array[] = array(
            'InventoryPurchaseOrder' => $ipo['InventoryPurchaseOrder'],
            'InventorySupplier' => $ipo['InventorySupplier']
        );

        return $ipo_array[0];

    }

    private function get_inv_item($id){
        $this->loadModel('InventoryItem');
        $item_array = array();
        $item = $this->InventoryItem->find('all', array(
            'conditions' => array(
                'InventoryItem.id' => $id
            )
        ));
        $item_array[] = array(
            'InventoryItem' => $item[0]['InventoryItem'],
            'GeneralUnit' => $item[0]['GeneralUnit']
        );

        return $item[0];
    }

    public function delete($id = null) {
        $this->InventoryPurchaseOrder->id = $id;
        if (!$this->InventoryPurchaseOrder->exists()) {
            throw new NotFoundException(__('Invalid inventory purchase order'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->InventoryPurchaseOrder->delete()) {
            $this->Session->setFlash(__('The inventory purchase order has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The inventory purchase order could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index'));
    }

    private function purchase($id) {
        $this->InventoryPurchaseOrder->id = $id;

        $this->loadModel('InventoryDeliveryOrder');
        $this->loadModel('InventoryDeliveryOrderItem');

        $options = array('conditions' => array('InventoryPurchaseOrder.' . $this->InventoryPurchaseOrder->primaryKey => $id));
        $this->request->data = $this->InventoryPurchaseOrder->find('first', $options);

        $data_po = $this->request->data['InventoryPurchaseOrder'];
        $data_po = array(
            'user_id' => $data_po['user_id'], 
            'group_id' => $data_po['group_id'], 
            'general_purchase_requisition_type_id' => $data_po['general_purchase_requisition_type_id'],
            'dateline' => $data_po['dateline'], 
            'status' => $data_po['status'], 
            'po_no' => $data_po['po_no'], 
            'account_department_budget_id' => $data_po['account_department_budget_id'],
            'inventory_supplier_id' => $data_po['inventory_supplier_id'],
            'term_of_payment_id' => $data_po['term_of_payment_id'],
            'remark_id' => $data_po['remark_id'],
            'notes' => $data_po['notes'],
            'inventory_delivery_location_id' => $data_po['inventory_delivery_location_id'],
            'warranty_id' => $data_po['warranty_id']
        );

        $this->InventoryDeliveryOrder->create();

        if($this->InventoryDeliveryOrder->save($data_po)){
            $data_item_po = $this->request->data['InventoryPurchaseOrderItem'];
            $count = count($this->request->data['InventoryPurchaseOrderItem']);
            
            $lastID = $this->InventoryDeliveryOrder->getLastInsertID();
            for($i=0; $i<$count; $i++){
                $this->InventoryDeliveryOrderItem->create();
                //print_r($data_item_pr[$i]);
                $data_do_item = array(
                    'inventory_supplier_id' => $data_item_po[$i]['inventory_supplier_id'],
                    'inventory_delivery_order_id' => $lastID,
                    'inventory_item_id' => $data_item_po[$i]['inventory_item_id'],
                    'quantity' => $data_item_po[$i]['quantity'],
                    'general_unit_id' => $data_item_po[$i]['general_unit_id'],
                    'price_per_unit' => $data_item_po[$i]['price_per_unit'],
                    'amount' => $data_item_po[$i]['amount'],
                    'discount' => $data_item_po[$i]['discount'],
                    'quantity_remaining' => $data_item_po[$i]['quantity'],
                    'inventory_supplier_item_id' => $data_item_po[$i]['inventory_supplier_item_id'],
                    'tax' => $data_item_po[$i]['tax']
                );

                //print_r($data_po_item);
                
                $this->InventoryDeliveryOrderItem->save($data_do_item);

                $data_status_purchase = array(
                    'id'        => $id,
                    'purchase_status'   => '1'
                );
                $this->InventoryPurchaseOrder->save($data_status_purchase);

            }

            $this->Session->setFlash(__('Purchase has been made.'), 'success');
            //return $this->redirect(array('action' => 'view', $id));
        }else{
            $this->Session->setFlash(__('Purchase could not be made.'), 'error');
            //return $this->redirect(array('action' => 'view', $id));
        }
    }
}