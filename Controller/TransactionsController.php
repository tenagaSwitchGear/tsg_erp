<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
 
class TransactionsController extends AppController { 

	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter(); 
	}

	public function wallet() {
		$this->set('title', 'Wallet Balance');
		$user_id = $this->Session->read('Auth.User.id');
		$wallet = $this->Wallet->find('first', array('conditions' => array('user_id' => $user_id)));
	}

	public function admin_index() {
		$this->set('title', 'Transaction History');
		$this->loadModel('Transaction');
		$user_id = $this->Session->read('Auth.User.id');
		$conditions = array(); 
		 
        if(!empty($this->data)) {         
            $this->Session->write('Filter.Transaction',$this->data);
        } elseif((!empty($this->passedArgs['page']) || strpos($this->referer(), 'transactions/index')) && $this->Session->check('Filter.Transaction')  ) {
            $this->request->data = $this->Session->check('Filter.Transaction')? $this->Session->read('Filter.Transaction') : '';
        } else {
            $this->Session->delete('Filter.Transaction');
        }   

        if(!empty($this->data)) {
			if(!empty($this->data['Transaction']['payment_type'])) {
				$conditions['Transaction.payment_type']= $this->data['Transaction']['payment_type'];
			}
			
			if(isset($this->data['Transaction']['from']) && !empty($this->data['Transaction']['from'])) {
				$conditions['Transaction.created >='] = $this->data['Transaction']['from'];
			}
			if(isset($this->data['Transaction']['to']) && !empty($this->data['Transaction']['to'])) {
				$conditions['Transaction.created <='] = $this->data['Transaction']['to'].' 23:59:59';
			}
		}
		
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'Transaction.id DESC','limit' => $record_per_page);

		$payment_type = array('1' => 'Credit', '2' => 'Debit');

		$this->set(compact('payment_type'));
		$this->set('transactions', $this->Paginator->paginate());
	}

	public function index() {
		$this->set('title', 'Transaction History');
		$this->loadModel('Transaction');
		$user_id = $this->Session->read('Auth.User.id');
		$conditions = array(); 
		

        if(!empty($this->data)) {         
            $this->Session->write('Filter.Transaction',$this->data);
        } elseif((!empty($this->passedArgs['page']) || strpos($this->referer(), 'transactions/index')) && $this->Session->check('Filter.Transaction')  ) {
            $this->request->data = $this->Session->check('Filter.Transaction')? $this->Session->read('Filter.Transaction') : '';
        } else {
            $this->Session->delete('Filter.Transaction');
        }  

		$conditions['Transaction.user_id'] = $user_id;	
		
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'Transaction.id DESC','limit'=>$record_per_page);
		$this->set('transactions', $this->Paginator->paginate());
	}
}