<?php
App::uses('AppController', 'Controller');
/**
 * SaleJobChildren Controller
 *
 * @property SaleJobChild $SaleJobChild
 * @property PaginatorComponent $Paginator
 */
class SaleJobChildsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

    /*
		Planning reports goes here
	*/
	public function report() {
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$cond['OR']['SaleJobChild.station_name LIKE'] = '%'.trim($_GET['name']).'%'; 
				$cond['OR']['SaleJobChild.name LIKE'] = '%'.trim($_GET['name']).'%'; 
			} 
		}
		$cond['SaleJobChild.fat_date !='] = NULL;
		$this->Paginator->settings = array(
			'conditions' => $cond,
			'order' => 'SaleJobChild.id DESC',
			'limit' => 30
			); 
		$jobs = $this->Paginator->paginate();

		$this->set('jobs', $jobs);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleJobChild->recursive = 0;
		$this->set('saleJobChildren', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleJobChild->exists($id)) {
			throw new NotFoundException(__('Invalid sale job child'));
		}
		$options = array('conditions' => array('SaleJobChild.' . $this->SaleJobChild->primaryKey => $id));
		$this->set('saleJobChild', $this->SaleJobChild->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleJobChild->create();
			if ($this->SaleJobChild->save($this->request->data)) {
				$this->Session->setFlash(__('The sale job child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale job child could not be saved. Please, try again.'));
			}
		}
		$saleJobs = $this->SaleJobChild->SaleJob->find('list');
		$this->set(compact('saleJobs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleJobChild->exists($id)) {
			throw new NotFoundException(__('Invalid sale job child'));
		}
		$options = array('conditions' => array('SaleJobChild.' . $this->SaleJobChild->primaryKey => $id));
		$saleJob = $this->SaleJobChild->find('first', $options);

		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleJobChild->save($this->request->data)) {
				$this->Session->setFlash(__('Job station has been saved.'), 'success');
				return $this->redirect(array('controller' => 'sale_jobs', 'action' => 'view', $saleJob['SaleJob']['id']));
			} else {
				$this->Session->setFlash(__('ob station could not be saved. Please, try again.'), 'error');
			}
		} 
		
		$this->request->data = $saleJob;
		 
		$saleJobs = $this->SaleJobChild->SaleJob->find('list');
		$this->set(compact('saleJobs'));

		$this->set('saleJob', $saleJob);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleJobChild->id = $id;
		if (!$this->SaleJobChild->exists()) {
			throw new NotFoundException(__('Invalid sale job child'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleJobChild->delete()) {
			$this->Session->setFlash(__('The sale job child has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale job child could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	
}
