<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * SaleQuotations Controller
 * CakeEmail
 * @property SaleQuotation $SaleQuotation
 * @property PaginatorComponent $Paginator
 */
class StsQuotationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	public $uses = array('SaleQuotation');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxfindquotation');
	}

 /**
 * add method 
 * 
 * @return void
 */
	public function add() {  
		$group_id = $this->Session->read('Auth.User.group_id');
		if ($this->request->is('post')) {

			$quot = $this->SaleQuotation->find('all', array(
				'conditions' => array(
					'YEAR(SaleQuotation.created)' => date('Y'),
					'MONTH(SaleQuotation.created)' => date('m'),
					'SaleQuotation.costing' => 0,
					'SaleQuotation.group_id' => $group_id
					),
				'recursive' => -1
				));  
			$count = count($quot) + 1;
			$quotation_no =  'STS/'.date('Y/m').'/'.str_pad($count, 3, 0, STR_PAD_LEFT);

			$this->loadModel('SaleTender');
 

			$this->SaleQuotation->create();

			$name = $quotation_no;
			$product_id = $this->request->data['SaleQuotationItem']['product_id']; 
			$orders = $this->request->data['SaleQuotationItem']['quantity']; 
 
			$general_unit_id = $this->request->data['SaleQuotationItem']['general_unit_id'];
 			$discount = $this->request->data['SaleQuotationItem']['discount'];
			$tax = $this->request->data['SaleQuotationItem']['tax'];

			$planning_price  = $this->request->data['SaleQuotationItem']['planning_price'];
			$sale_price  = $this->request->data['SaleQuotationItem']['sale_price'];
			$sale_margin  = $this->request->data['SaleQuotationItem']['sale_margin'];
			$sale_margin_value  = $this->request->data['SaleQuotationItem']['sale_margin_value'];
			$inventory_supplier_item_id = $this->request->data['SaleQuotationItem']['inventory_supplier_item_id']; 
			$sub_total  = $this->request->data['SaleQuotationItem']['sub_total']; 
			$total_price = $this->request->data['SaleQuotationItem']['total_price']; 
			$this->request->data['SaleQuotation']['name'] = $quotation_no;
			$this->request->data['SaleQuotation']['costing'] = 0; 
			$this->request->data['SaleQuotation']['bom_id'] = 0;
			$this->request->data['SaleQuotation']['inventory_item_id'] = 0; 
			$this->request->data['SaleQuotation']['user_id'] = $this->user_id; 
			$this->request->data['SaleQuotation']['group_id'] = $group_id; 

			if ($this->SaleQuotation->save($this->request->data)) {
				$quotation_id = $this->SaleQuotation->getLastInsertId(); 
 				
 				$name = 'Add New Quotation';
				$link = 'sale_quotations/view/'.$quotation_id;
				$type = 'Add Quotation';
				$this->insert_log($this->user_id, $name, $link, $type);

				// iTEMS
				$this->loadModel('SaleQuotationItem');
				for($i = 0; $i < count($product_id); $i++) {
					// Insert items
					$this->SaleQuotationItem->create(); 
					$item = array(
						'name' => $name[$i],
						'quantity' => $orders[$i],
						'general_unit_id' => $general_unit_id[$i], 
						'unit_price' => $sale_price[$i], 
						'discount' => $discount[$i], 
						'discount_type' => 0, 
						'tax' => $tax[$i], 
						'total_price' => $sub_total[$i], 
						'inventory_item_id' => $product_id[$i], 
						'sale_quotation_id' => $quotation_id, 
						'type' => 0, 
						'planning_costing' => $planning_price[$i], 
						'planning_margin' => 0, 
						'planning_price' => $planning_price[$i], 
						'sale_margin' => $sale_margin[$i], 
						'sale_margin_value' => $sale_margin_value[$i], 
						'sale_price' => $total_price[$i], 
						'total_cost' => 0, 
						'material_cost' => 0, 
						'indirect_cost' => 0, 
						'soft_cost' => 0, 
						'margin_value' => 0, 
						'costing' => 0, 
						'inventory_supplier_item_id' => $inventory_supplier_item_id[$i]
						); 
					$this->SaleQuotationItem->save($item); 
				}

				// If status == 1, then submit to hod
				if($this->request->data['SaleQuotation']['status'] == 1) {
					//$this->send_to_planning($quotation_id);
				}

				$this->Session->setFlash(__('The Quotation has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation could not be saved. Please, try again.'), 'error'); 
			}
		}

		// Currency

		$this->loadModel('GeneralCurrency');

		$currencies = $this->GeneralCurrency->find('all');
		$this->set('currencies', $currencies);

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');

		$this->loadModel('InventoryItemCategory');
		$item_categories = $this->InventoryItemCategory->find('list');

		$termOfPayments = $this->SaleQuotation->TermOfPayment->find('list');

		$saleTenders = $this->SaleQuotation->SaleTender->find('list');
		$customers = $this->SaleQuotation->Customer->find('list', array('order' => 'Customer.name ASC'));
		$saleBoms = $this->SaleQuotation->SaleBom->find('list');
		$this->loadModel('BomCategory');
		$categories = $this->BomCategory->find('all');
		$bomcategories = $this->BomCategory->find('list');
		$this->set('categories', $categories);
		$this->set(compact('saleTenders', 'customers', 'saleBoms', 'bomcategories', 'units', 'item_categories', 'termOfPayments'));
	}

	private function send_to_planning($id) { 
		$this->loadModel('User');

		$users = $this->User->find('all', array(
			'conditions' => array(
				'User.group_id' => 12
				),
			'reqursive' => -1
			));

		if($users) {
			foreach ($users as $user) {
				$data['to'] = $user['User']['email'];
				$data['template'] = 'planning_waiting_costing';
				$data['subject'] = 'Quotation Waiting For Costing';
				$data['content'] = array(
					'username' => $user['User']['username'],
					'subtitle' => 'new Quotation waiting to add costing',
					'link' => 'costings/view/' . $id 
					);
				$this->send_email($data);
			} 
		} 	
		return true;
	}

	/*protected function send_email($data = array()) {
		$Email = new CakeEmail('smtp');
		$Email->from('mohdazrul.jmsb@gmail.com');
		$Email->to($data['to']); 
		$Email->emailFormat('html');
		$Email->template($data['template'])->viewVars($data['content']);
		$Email->subject($data['subject']); 
		if(!$Email->send()) {
			return false;
		}
		return true;
	}*/

	private function _check_product_bom($product_id) {
		$this->loadModel('Bom');
		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.product_id' => $product_id
				)
			));
		return $bom;
	}

	private function _copy_bom($item_id, $bom_id, $quotation_id, $tender_id, $no_of_order, $margin, $unit) {
		$this->loadModel('Bom'); 
		$this->loadModel('SaleBom'); 

		$user_id = $this->Session->read('Auth.User.id');

		$conditions['Bom.id'] = $bom_id;
		$conditions['Bom.status'] = 1;
		$bom = $this->Bom->find('first', array('conditions' => $conditions));
 		
 		if($bom) {
	 		$this->SaleBom->create();
	 		$salebom['sale_quotation_item_id'] = $item_id;
	 		$salebom['inventory_item_id'] 			= $bom['Bom']['inventory_item_id'];
	 		$salebom['bom_id']              = $bom_id;
			$salebom['sale_tender_id']      = $tender_id;
			$salebom['sale_quotation_id']   = $quotation_id;
			$salebom['name']     		    = $bom['Bom']['name'];
			$salebom['code']          		= $bom['Bom']['code'];
			$salebom['description']         = $bom['Bom']['description'];
			$salebom['created']       		= $this->date; 
			$salebom['user_id']     	    = $user_id;
			$salebom['status']     			= 0;
			$salebom['price']     			= 0; // price already markup
			$salebom['margin']      		= $margin;
			$salebom['margin_unit']      	= $unit;
			$salebom['no_of_order']         = $no_of_order;

			if($this->SaleBom->save($salebom)) {
				// insert Bom
				$sale_bom_id = $this->SaleBom->getLastInsertId();

				// Insert Child
				$this->_copy_bom_child($bom_id, $sale_bom_id, $no_of_order, $quotation_id); 
			}	
 		} 
	}  

	private function _copy_bom_child($bom_id, $sale_bom_id, $no_of_order, $quotation_id) {
		$this->loadModel('BomChild');
		$this->loadModel('SaleBomChild');
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
					'BomChild.bom_id' => $bom_id
				) 
			));
		$data = array();
		foreach ($childs as $child) {
			$this->SaleBomChild->create();
			$data = array(
				'name' => $child['BomChild']['name'],
				'sale_bom_id' => $sale_bom_id,
				'parent_id' => $child['BomChild']['parent_id'],  // parent id will refer to bom_parent to prevent duplicate
				'bom_parent' => $child['BomChild']['id'],
				'no_of_order' => $no_of_order,
				'margin' => 0,
				'margin_unit' => 1,
				'sale_quotation_id' => $quotation_id,
				'inventory_item_id' => $child['BomChild']['inventory_item_id'], 
				);  
			$this->SaleBomChild->save($data);
			$child_id = $this->SaleBomChild->getLastInsertId();
			$this->_copy_bom_item($bom_id, $child['BomChild']['id'], $sale_bom_id, $no_of_order, $child_id, $quotation_id);
		}  
	}

	private function _copy_bom_item($bom_id, $childs, $sale_bom_id, $no_of_order, $child_id, $quotation_id) {
		$this->loadModel('BomItem');
		 
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $childs	
				) 
			)); 
		 
		$this->loadModel('SaleBomItem');
		$insert = array();
		foreach ($items as $item) {
			$quantity_total = $item['BomItem']['quantity'] * $no_of_order;
			$price_total = $item['InventoryItem']['unit_price'] * $quantity_total;
			$insert[] = array( 
				'bom_id' => $bom_id,
				'sale_bom_id' => $sale_bom_id,
				'inventory_item_id' => $item['BomItem']['inventory_item_id'],
				'sale_bom_child_id' => $child_id,
				'quantity' => $item['BomItem']['quantity'],
				'no_of_order' => $no_of_order,
				'quantity_total' => $quantity_total, // jumlah order * kuantiti barang
				'general_unit_id' => $item['BomItem']['general_unit_id'],
				'price' => $item['InventoryItem']['unit_price'],
				'price_total' => $price_total,
				'margin' => 0,  // belum markup
				'margin_unit' => 1, // percent 2 == amount 
				'sale_quotation_id' => $quotation_id
				);
		} 
		$this->SaleBomItem->saveAll($insert, array('deep' => true)); 
	}

	public function quotation($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');    
		$this->set(compact('units')); 

		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options); 
  
		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $id
				)
			));
		$this->set('items', $items); 
		$this->set('saleQuotation', $quotation);  
	}

	public function submitverification($id = null, $pic = null, $act = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		if($act == null) {
			if($pic == 17) {
				// HOD
				$action = 'Verification';
				$status = 3;
				$group = 19;
			}

			if($pic == 19) {
				// gm
				$action = 'Approval';
				$status = 5;
				$group = 9;
			}	
		} else {
			if($pic == 17) {
				// HOD
				$action = 'Verification';
				$status = $act;
				$group = 19; // mean GM will receive msg
			}

			if($pic == 19) {
				// gm
				$action = 'Approval';
				$status = $act;
				$group = 9; // mean Sales will receive msg after GM Approve
			}
		} 

		 

		$quotation = $this->SaleQuotation->find('first', array(
			'conditions' => array(
				'SaleQuotation.id' => $id
				),
			'recursive' => -1
			));
		$this->loadModel('User');
		$user = $this->User->find('first', array('conditions' => array(
			'User.group_id' => $group 
			),
			'recursive' => -1
		));

		$update = $this->SaleQuotation->updateAll(                     
		    array(
		    	'SaleQuotation.status' =>  "'".$status."'"
		    	),
		    array(
		    	'SaleQuotation.id' =>  $id
		    	)
		);	

		$data = array(
			'to' => $user['User']['email'],
			'template' => 'quotationverification',
			'subject' => $quotation['SaleQuotation']['name'] . ' Has Been ' . $action,
			'content' => array(
				'username' => $user['User']['username'],
				'itemtoverified' => 'Quotation ('.$quotation['SaleQuotation']['name'].')',
				'action' => $action,
				'link' => 'sale_quotations/verification/'.$id
				)
			);
		if($update) { 
			$this->send_email($data);
			$this->Session->setFlash(__('Quotation has been sent for Verification.'), 'success');
			return $this->redirect(array('action' => 'view', $id));
		} 
	}

	private function total_price($qty, $price, $discount, $tax) {
		$disc = $price - $discount;
		$after_tax = (($disc / 100) * $tax) + $disc;
		$total = $after_tax * $qty;
		return $total;
	}

	public function status($id = null, $status = null, $current = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 

		$auth = $this->Session->read('Auth.User');
		$quotation = $this->SaleQuotation->find('first', array(
			'conditions' => array(
				'SaleQuotation.id' => $id,
				'SaleQuotation.status' => $current
				)
			));
		$quot_name = $quotation['SaleQuotation']['name'];

		if($status == 9) {
			$quotation_status = 'Submitted To Client';
		}
		if($status == 10) {
			$quotation_status = 'Awarded';
		}
		if($status == 11) {
			$quotation_status = 'Failed';
		}

		if($id) {
			$update = $this->SaleQuotation->updateAll(
				array(
					'SaleQuotation.status' => "'".$status."'"
					),
				array(
					'SaleQuotation.id' => $id
					)
				);
			if($update) {
				$name = 'Quotation ' . $quot_name . ' status changed: ' . $quotation_status; 
				$link = 'sale_quotations/view/'.$id;
				$type = 'Quotation';
				$this->insert_log($auth['id'], $name, $link, $type);
				$this->Session->setFlash(__('Quotation status has been changed.'), 'success');
				return $this->redirect(array('action' => 'index/'.$status));
			}
		} else {
			$this->Session->setFlash(__('Quotation not found.'), 'error');
			return $this->redirect(array('action' => 'index/'.$current));
		}
	}

	public function verify($id = null) {
		$group = $this->Session->read('Auth.User.group_id');
		//var_dump($group);
		if($id == null) { 
			$status = 3; 
		} else {
			$status = $id;
		}

		$record_per_page = Configure::read('Reading.nodes_per_page');
		$conditions['SaleQuotation.status'] = $status;
		
		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'SaleQuotation.id DESC',
			'group'=>'SaleQuotation.id',
			'limit'=>$record_per_page
			); 
		$this->set('saleQuotations', $this->Paginator->paginate());

		$this->set('group', $group);
	}

	public function approval($id = null) {

		if($id == null) {
			$status = 3;
		} else {
			$status = $id;
		}

		$conditions = array();
		if(!empty($this->data)) {
			if(!empty($this->data['SaleQuotation']['sale_tender_id'])) {
				$conditions['SaleQuotation.sale_tender_id'] = $this->data['SaleQuotation']['sale_tender_id'];
			}
			
			if(isset($this->data['SaleQuotation']['sale_customer_id'])) {
				$conditions['SaleQuotation.sale_customer_id'] = $this->data['SaleQuotation']['sale_customer_id'];
			}  
		}	

		/*
		if($status == 3) {
			$conditions['SaleQuotation.status'] = array(3,4,5);
		} else if($status == 7) {
			$conditions['SaleQuotation.status'] = array(7,8);
		} else {
			$conditions['SaleQuotation.status'] = $status;
		}

		$role = $this->Session->read('Auth.User.group_id');

		//var_dump($role);

		if($role != 1 || $role != 17 || $role != 18 || $role != 19) {
			$user_id = $this->Session->read('Auth.User.id');
			//$conditions['SaleQuotation.user_id'] = $user_id;
		}  */

		$record_per_page = Configure::read('Reading.nodes_per_page');

		if(isset($_GET['search'])) {
			if($_GET['customer'] != '') {
				$conditions['Customer.name LIKE'] = '%'.$_GET['customer'].'%';
			}
			if($_GET['name'] != '') {
				$conditions['SaleQuotation.name LIKE'] = '%'.$_GET['name'].'%';
			} 
			$this->request->data = $_GET;
		}
		$conditions['SaleQuotation.status'] = $status;
		 
		$this->Paginator->settings = array(
			'conditions' => $conditions,   
			'order'=>'SaleQuotation.id DESC',
			'group'=>'SaleQuotation.id',
			'limit'=>$record_per_page
			); 
		$this->set('saleQuotations', $this->Paginator->paginate());
	}

/**
 * index method
 *
 * @return void
 */
	public function index($id = null) {

		if($id == null) {
			$status = 0;
		} else {
			$status = $id;
		}

		$group_id = $this->Session->read('Auth.User.group_id');

		$conditions = array();
		if(!empty($this->data)) {
			if(!empty($this->data['SaleQuotation']['sale_tender_id'])) {
				$conditions['SaleQuotation.sale_tender_id'] = $this->data['SaleQuotation']['sale_tender_id'];
			}
			
			if(isset($this->data['SaleQuotation']['sale_customer_id'])) {
				$conditions['SaleQuotation.sale_customer_id'] = $this->data['SaleQuotation']['sale_customer_id'];
			}  
		}	

		if($status == 3) {
			$conditions['SaleQuotation.status'] = array(3,4,5);
		} else if($status == 7) {
			$conditions['SaleQuotation.status'] = array(7,8);
		} else {
			$conditions['SaleQuotation.status'] = $status;
		}

		$role = $this->Session->read('Auth.User.group_id');

		//var_dump($role);

		if($role != 1 || $role != 17 || $role != 18 || $role != 19) {
			$user_id = $this->Session->read('Auth.User.id');
			//$conditions['SaleQuotation.user_id'] = $user_id;
		}  
		$conditions['SaleQuotation.group_id'] = $group_id;
		$record_per_page = Configure::read('Reading.nodes_per_page');

		if(isset($_GET['search'])) {
			if($_GET['customer'] != '') {
				$conditions['Customer.name LIKE'] = '%'.$_GET['customer'].'%';
			}
			if($_GET['name'] != '') {
				$conditions['SaleQuotation.name LIKE'] = '%'.$_GET['name'].'%';
			} 
			$this->request->data = $_GET;
		}

		if($role != 1) {
			$conditions['SaleQuotation.user_id'] = $user_id;
		}

		 
		$this->Paginator->settings = array(
			'conditions' => $conditions,   
			'order'=>'SaleQuotation.id DESC',
			'group'=>'SaleQuotation.id',
			'limit'=>$record_per_page
			); 
		$this->set('saleQuotations', $this->Paginator->paginate());
	}

	public function bom($id = null, $sale_quotation_id = null) { 
		$this->loadModel('SaleBom');
		$options = array('conditions' => array('SaleBom.' . $this->SaleBom->primaryKey => $id, 'SaleBom.sale_quotation_id' => $sale_quotation_id));
		$salebom = $this->SaleBom->find('first', $options);

		$this->loadModel('Bom');
		$options = array('conditions' => array('Bom.' . $this->Bom->primaryKey => $salebom['SaleBom']['bom_id']));
		$bom = $this->Bom->find('first', $options);

		$this->set('bom', $bom); 
		$this->set('salebom', $salebom);
	}

	public function ajaxfindquotation() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$conditions['SaleQuotation.name LIKE'] = '%'.trim($term).'%';
			//$conditions['OR']['SaleQuotation.remark LIKE'] = '%'.trim($term).'%';
			$conditions['SaleQuotation.costing'] = 0;
			$items = $this->SaleQuotation->find('all', array(
				'conditions' => $conditions,
				'group' => array('SaleQuotation.id'),
				'order' => 'SaleQuotation.name ASC'
				));
			
			$json = array(); 
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['SaleQuotation']['id'], 
					'name' => $item['SaleQuotation']['name'],
					'customer' => $item['Customer']['name'],
					'tender_name' => $item['SaleTender']['title'],
					'tender_no' => $item['SaleTender']['tender_no'],
					'tender_id' => $item['SaleTender']['id'],
					'customer_id' => $item['SaleQuotation']['customer_id'] 
					); 
			}
			echo json_encode($json);	
		}
	} 



	private function check_child($id, $parent_id) {
		$this->loadModel('SaleBomChild');
		$rows = $this->SaleBomChild->find('all', array(
			'conditions' => array(
				'SaleBomChild.sale_bom_id' => $id,
				'SaleBomChild.parent_id' => $parent_id
				)
			));
		if(count($rows) > 0) {
			return "closed";
		} else {
			$this->loadModel('SaleBomItem');
			$rows = $this->SaleBomItem->find('all', array(
				'conditions' => array( 
					'SaleBomItem.sale_bom_child_id' => $parent_id
					)
				));
			if(count($rows) > 0) {
				return "closed";
			} else {
				return '';
			}
		}	
	}

	public function ajaxtree($id) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$result = array();  

		$this->loadModel('SaleBomChild');

		$rows = $this->SaleBomChild->find('all', array(
			'conditions' => array(
				'SaleBomChild.sale_bom_id' => $id,
				'SaleBomChild.parent_id' => 0
				)
			));

		foreach ($rows as $row) { 
		    $json['attr']['id'] = $row['SaleBomChild']['id'];  

		    $json['attr']['rel'] = $row['SaleBomChild']['name']; 

		    $json['data']  = $row['SaleBomChild']['name'];

		    $json['state'] = $this->check_child($id, $row['SaleBomChild']['bom_parent']); // if state='closed' this node can be opened

		    $result[] = $json;
		}

		echo json_encode($result);
	}

	public function ajaxtreechild($id = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$result = array();  

		if(isset($this->request->query['id'])) {
			$parent_id = (int)$this->request->query['id']; 
		
			$this->loadModel('SaleBomChild');

			$rows = $this->SaleBomChild->find('all', array(
				'conditions' => array(
					'SaleBomChild.sale_bom_id' => $id,
					'SaleBomChild.bom_parent' => $parent_id
					)
				));
			// If no rows found, then try to show items
			$this->loadModel('SaleBomItem'); 
			$items = $this->SaleBomItem->find('all', array(
				'conditions' => array( 
					'SaleBomItem.sale_bom_child_id' => $parent_id
					)
				));
			if($rows || $items) {
				foreach ($rows as $row) { 
				    $json['attr']['id'] = $row['SaleBomItem']['id']; 

				    $json['attr']['rel'] = $row['SaleBomItem']['name']; 

				    $json['data']  = $row['SaleBomItem']['name'];

				    $json['state'] = $this->check_child($id, $row['SaleBomItem']['id']); // if state='closed' this node can be opened

				    $result[] = $json;
				}	
			//} else {
				// try to find items
				
				foreach ($items as $item) { 
				    $json['attr']['id'] = $item['SaleBomItem']['id']; 

				    $json['attr']['rel'] = $item['InventoryItem']['name']; 

				    $json['data']  = $item['InventoryItem']['name'] . " Qty: " . $item['SaleBomItem']['quantity'] . " " . $item['GeneralUnit']['name']; 

				    $json['state'] = ''; // if state='closed' this node can be opened

				    $result[] = $json;
				}
			}
			
		}
		echo json_encode($result);
	}

	public function award($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}

		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options); 

		$bom_id = array();
		$boms = array();
		$test = array();
		foreach ($quotation['SaleBom'] as $bom) {
			$bom_id[] = $bom['id']; 
			$boms[] = array(
				'id' => $bom['id'],
				'bom_id' => $bom['bom_id'],
				'name' => $bom['name'],
				'code' => $bom['code'],
				'description' => $bom['description'],
				'price' => $this->get_sale_bom_item($this->get_sale_bom_child($bom['id'])),
				'margin' => $bom['margin'],
				'margin_unit' => $bom['margin_unit'],
				'no_of_order' => $bom['no_of_order']
				); 
		}
 

		$this->set('boms', $boms); 
		$this->set('saleQuotation', $quotation);

		if ($this->request->is('post')) {
			 
		}

		$this->set('quotation', $quotation);
	}
 
	public function view($id = null) {
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options);  

		$bom_id = array();
		$boms = array();
		$test = array();
		foreach ($quotation['SaleBom'] as $bom) {
			$bom_id[] = $bom['id']; 
			$boms[] = array(
				'id' => $bom['id'],
				'bom_id' => $bom['bom_id'],
				'name' => $bom['name'],
				'code' => $bom['code'],
				'description' => $bom['description'],
				'price' => $this->get_sale_bom_item($this->get_sale_bom_child($bom['id'])),
				'margin' => $bom['margin'],
				'margin_unit' => $bom['margin_unit'],
				'no_of_order' => $bom['no_of_order']
				); 
		} 

		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $id
				)
			));
		$this->set('items', $items);

		$this->set('boms', $boms);

		$customer = $this->SaleQuotation->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $quotation['SaleQuotation']['customer_id']
				)
			));

		$this->set('customer', $customer); 

		$this->set('saleQuotation', $quotation);

		if ($this->request->is(array('post', 'put'))) {

			$group_id =$this->Session->read('Auth.User.group_id');
			$this->loadModel('Approval');
			$cond['Approval.name'] = 'Quotation';
			$cond['Approval.type'] = 'Approval';
			if($group_id != 1) {
				$cond['Approval.group_id'] = $group_id;
			}
			$approvals = $this->Approval->find('all', array(
				'conditions' => $cond
				));
			if($approvals) {
				foreach ($approvals as $approval) {
					$data = array(
						'to' => $approval['User']['email'],
						'template' => 'quotationverification',
						'subject' => 'Quotation Require Your Approval ' . $quotation['SaleQuotation']['name'],
						'content' => array(
							'username' => $approval['User']['username'],
							'itemtoverified' => 'Quotation ('.$quotation['SaleQuotation']['name'].')',
							'action' => 'Approval',
							'link' => 'sale_quotations/verification/'.$id
							)
						);
					$this->send_email($data);
				}
 
			 	$this->request->data['SaleQuotation']['user_approval'] = $this->user_id;
			 	$this->request->data['SaleQuotation']['status'] = 3;
				if($this->SaleQuotation->save($this->request->data)) {  
					$this->Session->setFlash(__('Quotation has been sent for Approval.'), 'success');
					return $this->redirect(array('action' => 'view', $id));
				}	 
			} else {
				$this->Session->setFlash(__('User Approval not found. Please contact Administrator.'), 'error');
				return $this->redirect(array('action' => 'view', $id));
			}
		} else {
			$this->request->data = $quotation;
		}

		$this->loadModel('SaleQuotationRemark');
		$remarks = $this->SaleQuotationRemark->find('all', array(
			'conditions' => array(
				'SaleQuotationRemark.sale_quotation_id' => $id
				),
			'order' => array('SaleQuotationRemark.id' => 'DESC')
			));
 		$this->set('remarks', $remarks);
	}
 
	public function printview($id = null) {
		$this->layout = 'print'; 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options); 

		$this->loadModel('PrintTemplate');

		$template = $this->PrintTemplate->find('first', array(
			'order' => 'PrintTemplate.id DESC',
			'conditions' => array(
				'PrintTemplate.status' => 1 
				)
			)); 
 
		$this->set('template', $template);

		$bom_id = array();
		$boms = array();
		$test = array();
		foreach ($quotation['SaleBom'] as $bom) {
			$bom_id[] = $bom['id']; 
			$boms[] = array(
				'id' => $bom['id'],
				'bom_id' => $bom['bom_id'],
				'name' => $bom['name'],
				'code' => $bom['code'],
				'description' => $bom['description'],
				'price' => $this->get_sale_bom_item($this->get_sale_bom_child($bom['id'])),
				'margin' => $bom['margin'],
				'margin_unit' => $bom['margin_unit'],
				'no_of_order' => $bom['no_of_order']
				); 
		} 

		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $id
				)
			));
		$this->set('items', $items);

		$this->set('boms', $boms);

		$customer = $this->SaleQuotation->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $quotation['SaleQuotation']['customer_id']
				)
			));

		$this->set('customer', $customer); 

		$this->set('saleQuotation', $quotation);

		if ($this->request->is(array('post', 'put'))) {
 
			 $this->loadModel('User');
		 
 			$this->SaleQuotation->id = $id;
			 $user = $this->User->find('first', array(
			 	'conditions' => array(
			 		'User.id' => $this->request->data['SaleQuotation']['user_approval']
			 		)
			 	));
			$data = array(
				'to' => $user['User']['email'],
				'template' => 'quotationverification',
				'subject' => $quotation['SaleQuotation']['name'] . ' Require Your Approval',
				'content' => array(
					'username' => $user['User']['username'],
					'itemtoverified' => 'Quotation ('.$quotation['SaleQuotation']['name'].')',
					'action' => 'Approval',
					'link' => 'sale_quotations/verification/'.$id
					)
				);
			$this->request->data['SaleQuotation']['status'] = 3;
		 	$this->request->data['SaleQuotation']['user_approval'] = $this->request->data['SaleQuotation']['user_approval'];

			if($this->SaleQuotation->save($this->request->data)) { 
				$this->send_email($data);
				$this->Session->setFlash(__('Quotation has been sent for Approval.'), 'success');
				return $this->redirect(array('action' => 'view', $id));
			} 
		}

		$this->request->data = $quotation;

		$this->loadModel('SaleQuotationRemark');
		$remarks = $this->SaleQuotationRemark->find('all', array(
			'conditions' => array(
				'SaleQuotationRemark.sale_quotation_id' => $id
				),
			'order' => array('SaleQuotationRemark.id' => 'DESC')
			));
 		$this->set('remarks', $remarks);
	}

	public function verification($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}

		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options);  

		$group = $this->Session->read('Auth.User.group_id');

		if($quotation['SaleQuotation']['status'] == 3) {
			$approval_type = 'Verify';
			$status = array(5 => 'Verify', 7 => 'Reject (Please add Remark)');
			$app['Approval.user_id'] = $approval_type;
		} elseif($quotation['SaleQuotation']['status'] == 5) {
			$approval_type = 'Approve';
			$status = array(6 => 'Approve', 8 => 'Reject (Please add Remark)');
			$app['Approval.user_id'] = $approval_type;
		} else {
			$status = array(6 => 'Approve', 8 => 'Reject (Please add Remark)');
		}

		$app['Approval.user_id'] = $this->user_id;
		$app['Approval.name'] = 'Quotation';
		
		$this->loadModel('Approval');

		if($group != 1) {
			
			$approval = $this->Approval->find('first', array(
				'conditions' => $app
				)); 
			if(!$approval) {  
				$this->Session->setFlash(__('You are not authorize to view this area.'), 'error');
				return $this->redirect(array('action' => 'approval'));
			}
		}
 
		$this->set('status', $status);	 

		$this->loadModel('SaleQuotationRemark');
		$remarks = $this->SaleQuotationRemark->find('all', array(
			'conditions' => array(
				'SaleQuotationRemark.sale_quotation_id' => $id
				),
			'order' => array('SaleQuotationRemark.id' => 'DESC')
			));
 		$this->set('remarks', $remarks);

 		if($this->request->is('post')) {
			$this->request->data['SaleQuotationRemark']['sale_quotation_id'] = $id; // Remark share with quotation / item data[SaleQuotationRemark][status]
			$this->request->data['SaleQuotationRemark']['sale_quotation_item_id'] = 0;
			$this->request->data['SaleQuotationRemark']['created'] = $this->date;
			$this->request->data['SaleQuotationRemark']['user_id'] = $this->user_id;
			$this->request->data['SaleQuotationRemark']['group_id'] = $group; 
			$quot_status = $this->request->data['SaleQuotationRemark']['status'];
			if($this->SaleQuotationRemark->save($this->request->data)) {
				$this->SaleQuotation->id = $id; 
				$update_status = $this->SaleQuotation->updateAll(array(
						'SaleQuotation.status' => "$quot_status"
					), array(
						'SaleQuotation.id' => $id
					));

				if($quot_status == 7 || $quot_status == 8) {
					$action = 'Rejected';
					// Email reject
					$data = array(
						'to' => $quotation['User']['email'],
						'template' => 'quotationreview',
						'subject' => 'Your ' . $quotation['SaleQuotation']['name'] . ' was Rejected',
						'content' => array(
							'username' => $quotation['User']['username'],
							'from' => $this->Session->read('Auth.User.username'),
							'itemtoverified' => 'Quotation ('.$quotation['SaleQuotation']['name'].')',
							'action' => $action,
							'link' => 'sale_quotations/verification/'.$id
							)
						); 
					$this->send_email($data);
				}
				if($quot_status == 5) {
					$action = 'Approved'; 

					$approvals = $this->Approval->find('all', array(
						'conditions' => array( 
							'Approval.name' => 'Quotation',
							'Approval.type' => 'Verify'
							)
						)); 
					foreach ($approvals as $approval) {
						$data = array(
							'to' => $approval['User']['email'],
							'template' => 'quotationverification',
							'subject' => 'Quotation ' . $quotation['SaleQuotation']['name'] . ' Require Your Approval',
							'content' => array(
								'username' => $quotation['User']['username'],
								'from' => $this->Session->read('Auth.User.username'),
								'itemtoverified' => 'Quotation ('.$quotation['SaleQuotation']['name'].')',
								'action' => 'Approval',
								'link' => 'sale_quotations/verification/'.$id
								)
							); 
						$this->send_email($data);
					} 
					
				}  
				if($quot_status == 6) {
 
					$action = 'Verified'; 
					// Email Success
					$data = array(
						'to' => $quotation['User']['email'],
						'template' => 'quotationreview',
						'subject' => 'Your ' . $quotation['SaleQuotation']['name'] . ' Has Been Approved',
						'content' => array(
							'username' => $quotation['User']['username'],
							'from' => $this->Session->read('Auth.User.username'),
							'itemtoverified' => 'Quotation ('.$quotation['SaleQuotation']['name'].')',
							'action' => 'Approved',
							'link' => 'sale_quotations/view/'.$id
							)
						); 
					$this->send_email($data);
				} 
 
				if($update_status) {
					$this->Session->setFlash(__('Quotation status has been changed ' . $action), 'success');
					return $this->redirect(array('action' => 'approval'));
				} 
			}	
		} 

		$bom_id = array();
		$boms = array();
		$test = array();
		foreach ($quotation['SaleBom'] as $bom) {
			$bom_id[] = $bom['id']; 
			$boms[] = array(
				'id' => $bom['id'],
				'bom_id' => $bom['bom_id'],
				'name' => $bom['name'],
				'code' => $bom['code'],
				'description' => $bom['description'],
				'price' => $this->get_sale_bom_item($this->get_sale_bom_child($bom['id'])),
				'margin' => $bom['margin'],
				'margin_unit' => $bom['margin_unit'],
				'no_of_order' => $bom['no_of_order']
				); 
		} 

		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $id
				)
			));
		$this->set('items', $items);

		$this->set('boms', $boms);

		$customer = $this->SaleQuotation->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $quotation['SaleQuotation']['customer_id']
				)
			));

		$this->set('customer', $customer); 
		$this->set('saleQuotation', $quotation);
	} 

	private function calculate_quotation($bom_id) {
		$child = $this->get_sale_bom_child($bom_id); 

		$item = $this->get_sale_bom_item($child);

		$cost = $this->get_sum_item_price($item);

		return $cost;
	}

	private function get_sale_bom_child($bom_id) {
		$this->loadModel('SaleBomChild');
		$childs = $this->SaleBomChild->find('all', array(
			'fields' => array('id'),
			'conditions' => array(
				'SaleBomChild.sale_bom_id' => $bom_id
				),
			'contain' => array()
			));
		$child_id = array();
		foreach ($childs as $child) {
			$child_id[] = $child['SaleBomChild']['id'];
		}
		return $child_id; 
	}

	private function get_sale_bom_item($child_id) {
		$this->loadModel('SaleBomItem'); 
		$items = $this->SaleBomItem->find('all', array(
			'fields' => array(
				'SUM(price_total) AS price'
				),
			'conditions' => array(
				'SaleBomItem.sale_bom_child_id' => $child_id
				),
			'contain' => array()
			)); 
		foreach ($items as $item) { 
			return $item[0]['price'];
		}  
	}

	private function get_sum_item_price($item_id) {
		$this->loadModel('SaleBomItem');
		$items = $this->SaleBomItem->find('all', array(
			'fields' => array(
				'SUM(price_total) AS price'
				),
			'conditions' => array(
				'SaleBomItem.id' => $item_id
				) 
			));
		foreach ($items as $item) {
			return $item[0]['price'];
		} 
	}

	private function get_item_by_child($child_id) {
		$this->loadModel('BomItem');
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $child_id
				)
			));
		$data = array();
		foreach ($items as $item) {
			$total = $item['InventoryItem']['unit_price'] * $item['BomItem']['quantity'];
			$data[] = array(
				'item_price' => $item['InventoryItem']['unit_price'],
				'quantity' => $item['BomItem']['quantity'],
				'total' => $total
				);
		}
		return $data;
	}   

	private function markup($unit, $margin, $price) {
		// 1 = %, 2 = amt
		if($unit == 1) {
			$total = ($price / 100) * $margin;
		} else {
			$total = $price + $margin;
		}
		return $total;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleQuotation->save($this->request->data)) {

				// Delete item 
				$this->loadModel('SaleQuotationItem');
				$this->SaleQuotationItem->deleteAll(array(
					'SaleQuotationItem.sale_quotation_id' => $id
					), false);

				$name = $this->request->data['SaleQuotationItem']['name'];
				$product_id = $this->request->data['SaleQuotationItem']['product_id']; 
				$orders = $this->request->data['SaleQuotationItem']['quantity']; 
	 
				$general_unit_id = $this->request->data['SaleQuotationItem']['general_unit_id'];
	 			$discount = $this->request->data['SaleQuotationItem']['discount'];
				$tax = $this->request->data['SaleQuotationItem']['tax'];

				$planning_price  = $this->request->data['SaleQuotationItem']['planning_price'];
				$sale_price  = $this->request->data['SaleQuotationItem']['sale_price'];
				$sale_margin  = $this->request->data['SaleQuotationItem']['sale_margin'];
				$sale_margin_value  = $this->request->data['SaleQuotationItem']['sale_margin_value'];
				$inventory_supplier_item_id = $this->request->data['SaleQuotationItem']['inventory_supplier_item_id']; 
				$sub_total  = $this->request->data['SaleQuotationItem']['sub_total']; 
				$total_price = $this->request->data['SaleQuotationItem']['total_price'];  



				for($i = 0; $i < count($product_id); $i++) {
					// Insert items
					// Insert items
					$this->SaleQuotationItem->create();  
					$item = array(
						'name' => $name[$i],
						'quantity' => $orders[$i],
						'general_unit_id' => $general_unit_id[$i], 
						'unit_price' => $sale_price[$i], 
						'discount' => $discount[$i], 
						'discount_type' => 0, 
						'tax' => $tax[$i], 
						'total_price' => $sub_total[$i], 
						'inventory_item_id' => $product_id[$i], 
						'sale_quotation_id' => $quotation_id, 
						'type' => 0, 
						'planning_costing' => $planning_price[$i], 
						'planning_margin' => 0, 
						'planning_price' => $planning_price[$i], 
						'sale_margin' => $sale_margin[$i], 
						'sale_margin_value' => $sale_margin_value[$i], 
						'sale_price' => $total_price[$i], 
						'total_cost' => 0, 
						'material_cost' => 0, 
						'indirect_cost' => 0, 
						'soft_cost' => 0, 
						'margin_value' => 0, 
						'costing' => 0, 
						'inventory_supplier_item_id' => $inventory_supplier_item_id[$i]
						); 
					$this->SaleQuotationItem->save($item); 
				}

				// If status == 1, then submit to hod
				if($this->request->data['SaleQuotation']['status'] == 1) {
					//$this->submit_verification('salequotations/view/'.$id.'?ref=email', 'Verification For Tender Quotation', 'Tender Quotation', 'HOD', 'Verification');
				}

				$name = 'Edit Quotation : ' . $bom['Bom']['code'];
				$link = 'sale_quotations/view/'.$id;
				$type = 'Edit Quotation';

				$this->insert_log($this->user_id, $name, $link, $type);

				$this->Session->setFlash(__('The sale quotation has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation could not be saved. Please, try again.'));
			}
		} 
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));
		$data = $this->SaleQuotation->find('first', $options);




		$this->request->data = $data;

		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array('conditions' => array('SaleQuotationItem.sale_quotation_id' => $id)));
		$this->set('items', $items);
 
		$saleBoms = $this->SaleQuotation->SaleBom->find('all', array('conditions' => array('SaleBom.sale_quotation_id' => $id)));

		$this->set('saleBoms', $saleBoms);
		$this->set('data', $data);
 

		$this->loadModel('GeneralCurrency');

		$currencies = $this->GeneralCurrency->find('all');
		$this->set('currencies', $currencies);

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');

		$this->loadModel('InventoryItemCategory');
		$item_categories = $this->InventoryItemCategory->find('list');

		$saleTenders = $this->SaleQuotation->SaleTender->find('list');
		$customers = $this->SaleQuotation->Customer->find('list');
		$saleBoms = $this->SaleQuotation->SaleBom->find('list');
		$this->loadModel('BomCategory');
		$categories = $this->BomCategory->find('all');
		$bomcategories = $this->BomCategory->find('list');
		$this->set('categories', $categories);
		$this->set(compact('saleTenders', 'customers', 'saleBoms', 'bomcategories', 'units', 'item_categories'));
	}

	public function duplicate($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}

		$quot = $this->SaleQuotation->find('all', array(
				'conditions' => array(
					'YEAR(SaleQuotation.created)' => date('Y'),
					'MONTH(SaleQuotation.created)' => date('m'),
					'SaleQuotation.costing' => 0
					),
				'recursive' => -1
				));  
			$count = count($quot) + 1; 
			
		if ($this->request->is(array('post', 'put'))) {

			$quot = $this->SaleQuotation->find('all', array(
				'conditions' => array(
					'YEAR(SaleQuotation.created)' => date('Y'),
					'MONTH(SaleQuotation.created)' => date('m'),
					'SaleQuotation.costing' => 0
					),
				'recursive' => -1
				));  
			$count = count($quot) + 1;
			$quotation_no =  'SALES/'.date('Y/m').'/'.str_pad($count, 3, 0, STR_PAD_LEFT);
			$this->request->data['SaleQuotation']['name'] = $quotation_no;
			$this->request->data['SaleQuotation']['user_id'] = $this->user_id;
			$this->request->data['SaleQuotation']['created'] = $this->date;
			if ($this->SaleQuotation->save($this->request->data)) {
				$quotation_id = $this->SaleQuotation->getLastInsertId();
				// Delete item 
				$this->loadModel('SaleQuotationItem'); 

				$name = $quotation_no;
				$product_id = $this->request->data['SaleQuotationItem']['product_id']; 
				$orders = $this->request->data['SaleQuotationItem']['quantity']; 
	 
				$general_unit_id = $this->request->data['SaleQuotationItem']['general_unit_id'];
	 			$discount = $this->request->data['SaleQuotationItem']['discount'];
				$tax = $this->request->data['SaleQuotationItem']['tax'];

				$planning_price  = $this->request->data['SaleQuotationItem']['planning_price'];
				$sale_price  = $this->request->data['SaleQuotationItem']['sale_price'];
				$sale_margin  = $this->request->data['SaleQuotationItem']['sale_margin'];
				$sale_margin_value  = $this->request->data['SaleQuotationItem']['sale_margin_value'];
				$inventory_supplier_item_id = $this->request->data['SaleQuotationItem']['inventory_supplier_item_id']; 
				$sub_total  = $this->request->data['SaleQuotationItem']['sub_total']; 
				$total_price = $this->request->data['SaleQuotationItem']['total_price']; 
				$this->request->data['SaleQuotation']['name'] = $quotation_no;
				$this->request->data['SaleQuotation']['costing'] = 0; 
				$this->request->data['SaleQuotation']['bom_id'] = 0;
				$this->request->data['SaleQuotation']['inventory_item_id'] = 0; 
				$this->request->data['SaleQuotation']['user_id'] = $this->user_id;

				for($i = 0; $i < count($product_id); $i++) {
					// Insert items
					$this->SaleQuotationItem->create();  
					$item = array(
						'name' => $name[$i],
						'quantity' => $orders[$i],
						'general_unit_id' => $general_unit_id[$i], 
						'unit_price' => $sale_price[$i], 
						'discount' => $discount[$i], 
						'discount_type' => 0, 
						'tax' => $tax[$i], 
						'total_price' => $sub_total[$i], 
						'inventory_item_id' => $product_id[$i], 
						'sale_quotation_id' => $quotation_id, 
						'type' => 0, 
						'planning_costing' => $planning_price[$i], 
						'planning_margin' => 0, 
						'planning_price' => $planning_price[$i], 
						'sale_margin' => $sale_margin[$i], 
						'sale_margin_value' => $sale_margin_value[$i], 
						'sale_price' => $total_price[$i], 
						'total_cost' => 0, 
						'material_cost' => 0, 
						'indirect_cost' => 0, 
						'soft_cost' => 0, 
						'margin_value' => 0, 
						'costing' => 0, 
						'inventory_supplier_item_id' => $inventory_supplier_item_id[$i]
						); 
					$this->SaleQuotationItem->save($item); 
				}

				// If status == 1, then submit to hod
				if($this->request->data['SaleQuotation']['status'] == 1) {
					//$this->submit_verification('salequotations/view/'.$id.'?ref=email', 'Verification For Tender Quotation', 'Tender Quotation', 'HOD', 'Verification');
				}

				$this->Session->setFlash(__('The sale quotation has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation could not be saved. Please, try again.'));
			}
		} 
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));
		$data = $this->SaleQuotation->find('first', $options);




		$this->request->data = $data;

		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array('conditions' => array('SaleQuotationItem.sale_quotation_id' => $id)));
		$this->set('items', $items);
 
		$saleBoms = $this->SaleQuotation->SaleBom->find('all', array('conditions' => array('SaleBom.sale_quotation_id' => $id)));

		$this->set('saleBoms', $saleBoms);
		$this->set('data', $data);
 

		$this->loadModel('GeneralCurrency');

		$currencies = $this->GeneralCurrency->find('all');
		$this->set('currencies', $currencies);

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');

		$this->loadModel('InventoryItemCategory');
		$item_categories = $this->InventoryItemCategory->find('list');

		$saleTenders = $this->SaleQuotation->SaleTender->find('list');
		$customers = $this->SaleQuotation->Customer->find('list', array('order' => 'Customer.name ASC'));
		$saleBoms = $this->SaleQuotation->SaleBom->find('list');
		$this->loadModel('BomCategory');
		$categories = $this->BomCategory->find('all');
		$bomcategories = $this->BomCategory->find('list');
		$this->set('categories', $categories);
		$termOfPayments = $this->SaleQuotation->TermOfPayment->find('list');
		$this->set(compact('saleTenders', 'customers', 'saleBoms', 'bomcategories', 'units', 'item_categories', 'termOfPayments'));
	}

	// Ie: ('salequotations/verification/id', 'Quotation Verification', 'Tender Quotation', 'HOD') if require HOD verification
	private function submit_verification($link, $subject, $itemtoverified, $who, $action) { 
		$group_id = $this->Session->read('Auth.User.group_id'); 
		$department = $this->find_hod($group_id, $who);    
		if($department) {
			$Email = new CakeEmail('smtp');
			$Email->from(Configure::read('Site.email'));
			$Email->to($department['User']['email']);
			$Email->emailFormat('html');
			$Email->template('internalverification')->viewVars(array('firstname' => $department['User']['firstname'], 'itemtoverified' => $itemtoverified, 'link' => $link, 'action' => $action));
			$Email->subject($subject); 
			if($Email->send()) {
				return true;
			}  
		}
		return false;	
	}

	private function find_hod($group_id, $who) {
		$this->loadModel('InternalDepartment');
		$data = $this->InternalDepartment->find('first', array('InternalDepartment.group_id' => $group_id, 'InternalDepartment.name' => $who));
		return $data;
	}

	private function after_verification($link, $subject, $itemtoverified, $user, $action) {     
		$Email = new CakeEmail('smtp');
		$Email->from(Configure::read('Site.email'));
		$Email->to($user['User']['email']);
		$Email->emailFormat('html');
		$Email->template('internalverification')->viewVars(array('firstname' => $user['User']['firstname'], 'itemtoverified' => $itemtoverified, 'link' => $link, 'action' => $action));
		$Email->subject($subject); 
		if($Email->send()) {
			return true;
		} 
		return false;
	}

	private function find_pic($group_id, $pic) {
		$this->loadModel('InternalDepartment');
		$department = $this->InternalDepartment->find('first', array('InternalDepartment.group_id' => $group_id, 'InternalDepartment.name' => $pic));
		return $department;
	}

	private function check_user_role($user_id, $pic) {
		$group_id = $this->Session->read('Auth.User.group_id'); 
		$user_id = $this->Session->read('Auth.User.id'); 
		$department = $this->find_pic($group_id, $pic);

		if($department['InternalDepartment']['user_id'] == $user_id) {
			return true;
		}
		return false;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleQuotation->id = $id;
		if (!$this->SaleQuotation->exists()) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleQuotation->delete()) {
			$this->Session->setFlash(__('The sale quotation has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale quotation could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}