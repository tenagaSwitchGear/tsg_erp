<?php
App::uses('AppController', 'Controller');
/**
 * Countries Controller
 *
 * @property Country $Country
 * @property PaginatorComponent $Paginator 
 */
class CountriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	public function beforeFilter() {
        parent::beforeFilter();
    }


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$conditions = array();
		/* Filter With Paging */
        if(!empty($this->data)) {         
            $this->Session->write('Filter.Country',$this->data);
        }
        elseif((!empty($this->passedArgs['page']) || strpos($this->referer(), 'countries/index')) && $this->Session->check('Filter.Country')  ) {
            $this->request->data = $this->Session->check('Filter.Country')? $this->Session->read('Filter.Country') : '';
        }
        else
        {
            $this->Session->delete('Filter.Country');
        }
        /* End Filter With Paging */
		
		if(!empty($this->data)) {
			if(!empty($this->data['Country']['name'])) {
				$conditions['Country.name LIKE']='%'.$this->data['Country']['name'].'%';
			}
			
		}		
		
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'Country.created DESC','limit'=>$record_per_page);
		$this->set('countries', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
		$this->set('country', $this->Country->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Country->create();
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'),'error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'),'error');
			}
		} else {
			$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id),'contain'=>array());
			$this->request->data = $this->Country->find('first', $options);
			
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid country'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Country->delete()) {
			$this->Session->setFlash(__('The country has been deleted.'),'success');
		} else {
			$this->Session->setFlash(__('The country could not be deleted. Please, try again.'),'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
	
/**
 * admin_status method
 *
 * @return void
 */
 
	public function admin_status($id,$status){
		if (!$this->Country->exists($id)) {

			$this->Session->setFlash(__('Invalid Country Id.'),'error');
			$this->redirect(array('action' => 'index'));

		}
		$this->Country->id = $id;
		if($this->Country->saveField('status',$status))
			$this->Session->setFlash(__('Status successfully updated.'),'success');
		else
			$this->Session->setFlash(__('Status could not be update. Please, try again.'),'error');
			
		$this->redirect(array('action' => 'index'));
	}	

}
