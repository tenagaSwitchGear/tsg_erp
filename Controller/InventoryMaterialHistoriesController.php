<?php
App::uses('AppController', 'Controller');
/**
 * InventoryMaterialHistories Controller
 *
 * @property InventoryMaterialHistory $InventoryMaterialHistory
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryMaterialHistoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

	public function hist0n() {
		$this->autoRender = false;
		$this->loadModel('InventoryStock');
		$stocks = $this->InventoryStock->find('all', array(
			'order' => 'InventoryStock.created ASC'
			));
		$this->loadModel('InventoryMaterialHistory'); 
		$history = array();
		foreach ($stocks as $stock) {
			$this->InventoryMaterialHistory->create();
			$history[] = array(
				'reference' => 'Stock Entry',
				'user_id' => $stock['InventoryStock']['user_id'],
				'store_pic' => $stock['InventoryStock']['user_id'],
				'inventory_item_id' => $stock['InventoryStock']['inventory_item_id'],
				'inventory_stock_id' => $stock['InventoryStock']['id'],
				'quantity' => $stock['InventoryStock']['quantity'],
				'general_unit_id' => $stock['InventoryStock']['general_unit_id'],
				'issued_quantity' => $stock['InventoryStock']['quantity'],
				'quantity_balance' => $this->sum_stock_balance($stock['InventoryStock']['inventory_item_id'], $stock['InventoryStock']['quantity'], $stock['InventoryStock']['created']),
				'inventory_material_request_id' => 0,
				'inventory_material_request_item_id' => 0,
				'created' => $stock['InventoryStock']['created'],
				'status' => 1,
				'type' => 7, // 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 5: RMA, 6: grn, 7: Stock Entry
				'transfer_type' => 'In'
				);
			//$save_history = $this->InventoryMaterialHistory->save($history);
			//if(!$save_history) {
			//	die('830');
			//}
		}
		echo '<pre>';
		print_r($history); 	
	}

	private function sum_stock_balance($inventory_item_id, $qty, $created) { 
		$stocks = $this->InventoryStock->find('all', array(
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $inventory_item_id,
				'InventoryStock.created <' => $created,
				),
			'fields' => array(
				'SUM(InventoryStock.quantity) AS quantity' 
				),
			'recursive' => -1
			));
		foreach ($stocks as $stock) {
			if(!empty($stock[0]['quantity'])) {
				return $stock[0]['quantity'] + $qty;
			} else {
				return $qty;
			}
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() { 

		if(isset($_GET['search'])) {
			if($_GET['from'] != '') {
				$conditions['inventoryMaterialHistory.created >='] = $_GET['from'];
			}

			if($_GET['to'] != '') {
				$conditions['inventoryMaterialHistory.created <='] = $_GET['to'];
			}

			if($_GET['store_pic'] != '') {
				$conditions['inventoryMaterialHistory.store_pic'] = (int)$_GET['store_pic'];
			}

			if($_GET['user_id'] != '') {
				$conditions['inventoryMaterialHistory.user_id'] = (int)$_GET['user_id'];
			}

			if($_GET['item_id'] != '') {
				$conditions['inventoryMaterialHistory.inventory_item_id'] = (int)$_GET['item_id'];
			} 	

			if($_GET['type'] != '') {
				$conditions['inventoryMaterialHistory.type'] = (int)$_GET['type'];
			} 
			$this->request->data['inventoryMaterialHistory'] = $_GET;
		} 

		// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer,  
		$conditions['inventoryMaterialHistory.status'] = 1;

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order'=>'inventoryMaterialHistory.created DESC',
			'limit'=>$record_per_page
			); 

		$this->set('inventoryMaterialHistories', $this->Paginator->paginate());

		$type = array(
			1 => 'Issued Out', 
			2 => 'Warehouse Transfer', 
			3 => 'Location Transfer', 
			4 => 'Code Transfer', 
			5 => 'Restock Finished Goods', 
			6 => 'Restock Item (D.O)');
		$this->set(compact('type'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryMaterialHistory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material history'));
		}
		$options = array('conditions' => array('InventoryMaterialHistory.' . $this->InventoryMaterialHistory->primaryKey => $id));
		$this->set('inventoryMaterialHistory', $this->InventoryMaterialHistory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryMaterialHistory->create();
			if ($this->InventoryMaterialHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material history could not be saved. Please, try again.'));
			}
		}
		$users = $this->InventoryMaterialHistory->User->find('list');
		$inventoryItems = $this->InventoryMaterialHistory->InventoryItem->find('list');
		$inventoryStocks = $this->InventoryMaterialHistory->InventoryStock->find('list');
		$inventoryMaterialRequests = $this->InventoryMaterialHistory->InventoryMaterialRequest->find('list');
		$inventoryMaterialRequestItems = $this->InventoryMaterialHistory->InventoryMaterialRequestItem->find('list');
		$this->set(compact('users', 'inventoryItems', 'inventoryStocks', 'inventoryMaterialRequests', 'inventoryMaterialRequestItems'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryMaterialHistory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryMaterialHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryMaterialHistory.' . $this->InventoryMaterialHistory->primaryKey => $id));
			$this->request->data = $this->InventoryMaterialHistory->find('first', $options);
		}
		$users = $this->InventoryMaterialHistory->User->find('list');
		$inventoryItems = $this->InventoryMaterialHistory->InventoryItem->find('list');
		$inventoryStocks = $this->InventoryMaterialHistory->InventoryStock->find('list');
		$inventoryMaterialRequests = $this->InventoryMaterialHistory->InventoryMaterialRequest->find('list');
		$inventoryMaterialRequestItems = $this->InventoryMaterialHistory->InventoryMaterialRequestItem->find('list');
		$this->set(compact('users', 'inventoryItems', 'inventoryStocks', 'inventoryMaterialRequests', 'inventoryMaterialRequestItems'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryMaterialHistory->id = $id;
		if (!$this->InventoryMaterialHistory->exists()) {
			throw new NotFoundException(__('Invalid inventory material history'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryMaterialHistory->delete()) {
			$this->Session->setFlash(__('The inventory material history has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory material history could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->InventoryMaterialHistory->recursive = 0;
		$this->set('inventoryMaterialHistories', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->InventoryMaterialHistory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material history'));
		}
		$options = array('conditions' => array('InventoryMaterialHistory.' . $this->InventoryMaterialHistory->primaryKey => $id));
		$this->set('inventoryMaterialHistory', $this->InventoryMaterialHistory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->InventoryMaterialHistory->create();
			if ($this->InventoryMaterialHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material history could not be saved. Please, try again.'));
			}
		}
		$users = $this->InventoryMaterialHistory->User->find('list');
		$inventoryItems = $this->InventoryMaterialHistory->InventoryItem->find('list');
		$inventoryStocks = $this->InventoryMaterialHistory->InventoryStock->find('list');
		$inventoryMaterialRequests = $this->InventoryMaterialHistory->InventoryMaterialRequest->find('list');
		$inventoryMaterialRequestItems = $this->InventoryMaterialHistory->InventoryMaterialRequestItem->find('list');
		$this->set(compact('users', 'inventoryItems', 'inventoryStocks', 'inventoryMaterialRequests', 'inventoryMaterialRequestItems'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->InventoryMaterialHistory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryMaterialHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryMaterialHistory.' . $this->InventoryMaterialHistory->primaryKey => $id));
			$this->request->data = $this->InventoryMaterialHistory->find('first', $options);
		}
		$users = $this->InventoryMaterialHistory->User->find('list');
		$inventoryItems = $this->InventoryMaterialHistory->InventoryItem->find('list');
		$inventoryStocks = $this->InventoryMaterialHistory->InventoryStock->find('list');
		$inventoryMaterialRequests = $this->InventoryMaterialHistory->InventoryMaterialRequest->find('list');
		$inventoryMaterialRequestItems = $this->InventoryMaterialHistory->InventoryMaterialRequestItem->find('list');
		$this->set(compact('users', 'inventoryItems', 'inventoryStocks', 'inventoryMaterialRequests', 'inventoryMaterialRequestItems'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->InventoryMaterialHistory->id = $id;
		if (!$this->InventoryMaterialHistory->exists()) {
			throw new NotFoundException(__('Invalid inventory material history'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryMaterialHistory->delete()) {
			$this->Session->setFlash(__('The inventory material history has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory material history could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
