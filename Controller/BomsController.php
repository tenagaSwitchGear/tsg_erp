<?php
App::uses('AppController', 'Controller');
/**
 * Boms Controller
 *
 * @property Bom $Bom
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class BomsController extends AppController {

/**
 * Helpers
 *
 * @var array 
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index($status = null) {
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['Bom.name LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['code'] != '') {
				$conditions['Bom.code LIKE'] = '%'.trim($_GET['code']).'%';
			} 
			$this->reqursive->data = $_GET;
		}

		if($status != null) {
			$conditions['Bom.status'] = $status;
		} else {
			$conditions['Bom.status'] = 1; 
		} 
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'Bom.id DESC', 'limit' => $record_per_page);
		$this->set('boms', $this->Paginator->paginate());
	}

	public function copyitem($bom_id = null, $parent_id = null) {
		$this->loadModel('BomChild');
		if ($this->request->is('post')) { 
			
			$copy_from_id = $this->request->data['BomChild']['copy_from_id'];
			$copy_from_bom_id = $this->request->data['BomChild']['copy_from_bom_id'];
			// 1st level item general
			$this->copy_bom_item($copy_from_id, $parent_id, $copy_from_bom_id, $bom_id); 

			$this->copyitem_bom_child($copy_from_bom_id, $bom_id, $copy_from_id, $parent_id);
			
			$this->Session->setFlash(__('The bom child has been saved.'), 'success');
			return $this->redirect(array('action' => 'bomchild', $bom_id)); 
		}

		$bom = $this->BomChild->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $bom_id
				)
			));
		$this->set('bom', $bom);

		if($parent_id != null) {
			$child = $this->BomChild->find('first', array(
				'conditions' => array(
					'BomChild.id' => $parent_id
					)
				));
		} else {
			$child = null;
		}

		$this->set('child', $child);
	}

	public function copy($id = null) {
		if (!$this->Bom->exists($id)) {
			throw new NotFoundException(__('Invalid bom'));
		}
 
		//$this->request->allowMethod(array('post', 'put'));
		//if ($this->request->is(array('post', 'put'))) {
			// Copy bom

			$bom = $this->Bom->find('first', array(
				'conditions' => array(
					'Bom.id' => $id
					)
				));
			if($bom) {

				$new_code = $bom['Bom']['code'] . ' - Copy ' . $this->date;
 				$new_name = $bom['Bom']['name'] . ' - Copy ' . $this->date;

				$date = date('Y-m-d H:i:s');
				$this->request->data['Bom']['type'] = $bom['Bom']['type'];
				$this->request->data['Bom']['sale_job_id'] = $bom['Bom']['sale_job_id'];
				$this->request->data['Bom']['sale_job_child_id'] = $bom['Bom']['sale_job_child_id'];
				$this->request->data['Bom']['inventory_item_id'] = $bom['Bom']['inventory_item_id'];
				$this->request->data['Bom']['bom_category_id'] = $bom['Bom']['bom_category_id'];
				$this->request->data['Bom']['name'] = $new_name;
				$this->request->data['Bom']['code'] = $new_code;
				$this->request->data['Bom']['description'] = $bom['Bom']['description'];
				$this->request->data['Bom']['user_id'] = $this->Session->read('Auth.User.id');
				$this->request->data['Bom']['created'] = $this->date;
				$this->request->data['Bom']['modified'] = $this->date; 
				$this->request->data['Bom']['status'] = 0;	
				$this->request->data['Bom']['price'] = 0;  

				if($this->Bom->save($this->request->data)) {
					// Copy child & item
					$bom_id = $this->Bom->getLastInsertId();
					$this->copy_bom_child($id, $bom_id, null);
				} else {
					debug($this->Bom->invalidFields());
				}

				$this->Session->setFlash(__('The BOM has been copied successfully.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('BOM Not found.'), 'error');
			}
		//} else {
		//	$this->Session->setFlash(__('Invalid request.'), 'error');
		//}  
	}

	private function item_info($id) {
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('first', array(
			'conditions' => array(
				'InventoryItem.id' => $id
				)
			));
		return $item;
	}

	private function copy_bom_child($from_bom, $to_bom, $parent_id = null) {
		$this->loadModel('BomChild'); 
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $from_bom
				),
			'order' => array('BomChild.id' => 'ASC')
			));

		$items = array();
		foreach ($childs as $child) {
			$this->BomChild->create();

			$data = array(
				'name' => $child['BomChild']['name'],
				'inventory_item_id' => $child['BomChild']['inventory_item_id'],
				'bom_id' => $to_bom,
				'parent_id' => $parent_id == null ? $child['BomChild']['parent_id'] : $parent_id,
				'old_id' => $child['BomChild']['id'],
				'copy_from_id' => $child['BomChild']['id'],
				'type' => $child['BomChild']['type'],
				'created' => $this->date
				);
			$this->BomChild->save($data);
			$bom_child_id = $this->BomChild->getLastInsertId();

			if($child['BomChild']['parent_id'] != 0) {
				$this->update_parent_id($to_bom, $child['BomChild']['parent_id'], $bom_child_id);
			}  

			// Copy each child item
			$this->copy_bom_item($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom); 
		} 
	}

	private function copy_bom_item($child_id, $new_child_id, $old_bom_id, $new_bom_id) {
		$this->loadModel('BomItem'); 
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $child_id 
				),
			'order' => array(
				'BomItem.bom_child_id' => 'ASC'
				),
			'reqursive' => -1
			));
 
		foreach ($items as $item) { 
			$this->BomItem->create();
			$data = array( 
				'inventory_item_id' => $item['BomItem']['inventory_item_id'],
				'bom_child_id' => $new_child_id, 
				'quantity' => $item['BomItem']['quantity'],
				'general_unit_id' => $item['BomItem']['general_unit_id'],
				'bom_id' => $new_bom_id,
				'created' => $this->date 
				); 
			$this->BomItem->save($data);
		}
		 
	}

	private function update_parent_id($bom_id, $old_parent_id, $new_id) {
		$this->loadModel('BomChild');
		$child = $this->BomChild->find('first', array(
			'conditions' => array(
				'BomChild.bom_id' => $bom_id,
				'BomChild.old_id' => $old_parent_id
				)
			));
		if($child) {
			$update = $this->BomChild->updateAll(
				array(
					'BomChild.parent_id' => "'".$child['BomChild']['id']."'"
					),
				array(
					'BomChild.parent_id' => $old_parent_id,
					'BomChild.bom_id' => $bom_id,
					'BomChild.id' => $new_id
					)
				);
			if($update) {
				return true;
			}
		} 
	} 

	public function additem($bom_id, $child_id) { 
		$options = array('conditions' => array('Bom.' . $this->Bom->primaryKey => $bom_id));
		$bom = $this->Bom->find('first', $options);
		
		if ($this->request->is('post')) {
			$item = $this->request->data['BomItem']['inventory_item_id'];
			$qty = $this->request->data['BomItem']['quantity']; 
			$unit = $this->request->data['BomItem']['general_unit_id']; 

			$temp_quantity = $this->request->data['BomItem']['temp_quantity']; 
			// Delete first
			$this->loadModel('BomItem');

			$check = $this->BomItem->find('all', array(
				'conditions' => array(
					'BomItem.bom_child_id' => $child_id
					)
				));
			if($check) {
				$this->BomItem->deleteAll(
					array(
						'BomItem.bom_child_id' => $child_id
						),
					false
					);	
			}
			
			$this->loadModel('BomItem');
			for($i = 0; $i < count($item); $i++) {
				if($item[$i] != '' || $qty[$i] != '') {
					$this->BomItem->create();
					$data = array(
						'inventory_item_id' => $item[$i],
						'bom_child_id' => $child_id,
						'temp_quantity' => $temp_quantity[$i],
						'quantity' => $qty[$i],
						'general_unit_id' => $unit[$i],
						'bom_id' => $bom_id,
						'created' => $this->date
						);
					$this->BomItem->save($data);
				}
			}

			//log 
			$name = 'Add new item general on BOM : ' . $bom['Bom']['code'];
			$link = 'boms/view/'.$bom_id;
			$type = 'Bom Items';

			$this->insert_log($this->user_id, $name, $link, $type);
			//

			$this->Session->setFlash(__('Theitem has been saved.'), 'success');
			return $this->redirect(array('action' => 'bomchild/'.$bom_id));
		}

		$child = $this->Bom->BomChild->find('first', array(
			'conditions' => array(
				'BomChild.id' => $child_id
				)
			));
		$this->loadModel('BomItem');
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $child_id
				)
			)); 

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list', array(
			'recursive' => -1
			));
		$this->set('bom', $bom);
		$this->set('units', $units); 
		$this->set('child', $child); 
		$this->set('items', $items);
	} 

	public function view($id = null) {
		if (!$this->Bom->exists($id)) {
			throw new NotFoundException(__('Invalid bom'));
		}
		$options = array('conditions' => array('Bom.' . $this->Bom->primaryKey => $id));
		$this->set('bom', $this->Bom->find('first', $options));
	}

	public function ajaxfindbom() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$conditions['OR']['Bom.name LIKE'] = '%'.$term.'%';
			$conditions['OR']['Bom.description LIKE'] = '%'.$term.'%';
			$conditions['OR']['Bom.code LIKE'] = '%'.$term.'%';

			$items = $this->Bom->find('all', array('conditions' => $conditions, 'group' => 'Bom.id'));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array('id' => $item['Bom']['id'], 'name' => $item['Bom']['name'], 'code' => $item['Bom']['code'], 'description' => $item['Bom']['description']); 
			}
			echo json_encode($json);	 
		}
	}

	public function ajaxsaveitem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$parent_id = $_POST['parent_id'];
			$item_id = $_POST['item_id'];
			$bom_id = $_POST['bom_id'];
			$quantity = $_POST['quantity'];
			$quantity_unit_id = $_POST['quantity_unit_id'];

			if($item_id == '') {
				$json['message'] = 'Please select Item.';
			} elseif($quantity == '') {
				$json['message'] = 'Please enter Quantity.';
			} elseif($quantity_unit_id == '') {
				$json['message'] = 'Please select Quantity Unit.';
			} else {
				$this->loadModel('BomItem');
				$data = array(
					'inventory_item_id' => $item_id,
					'bom_child_id' => $parent_id,
					'quantity' => $quantity,
					'general_unit_id' => $quantity_unit_id
					);
				if($this->BomItem->save($data)) {
					// Find item to display
					$this->loadModel('InventoryItem');

					$item = $this->InventoryItem->find('first', array(
						'conditions' => array(
							'InventoryItem.id' => $item_id
							),
						'contains' => array()
						)); 

					$this->loadModel('GeneralUnit');

					$unit = $this->GeneralUnit->find('first', array(
						'conditions' => array(
							'GeneralUnit.id' => $quantity_unit_id
							),
						'contains' => array()
						)); 

					$json['status'] = true;
					$json['message'] = 'Item has been save.';
					$json['name'] = $item['InventoryItem']['name'];
					$json['quantity'] = number_format($quantity, 4);
					$json['unit'] = $unit['GeneralUnit']['name'];
					$json['bom_child_id'] = $this->BomItem->getLastInsertId();
				}
			}
		}
		echo json_encode($json);
	}

	public function ajaxremoveitem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$id = $_POST['id'];
			$parent_id = $_POST['parent_id'];
			$this->loadModel('BomItem');

			if($this->BomItem->delete($id)) {
				$json['status'] = true;
				$json['message'] = 'Item id: '.$id.' has been deleted.';
			} else {
				$json['message'] = 'Item id: '.$id.' has been deleted.';
			}
		}
		echo json_encode($json);
	}

	public function ajaxremovechild() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$id = $_POST['id'];
		}
		echo json_encode($json);
	}

	public function ajaxeditchild() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$id = $_POST['id'];
		}
		echo json_encode($json);
	}

	public function ajaxedititem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$id = $_POST['id'];
		}
		echo json_encode($json);
	}

	public function ajaxupdatechild() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$id = $_POST['id'];
			$name = $_POST['name'];
			if($name == '') {
				$json['message'] = 'Please enter Child Name.';
			} else {
				$this->loadModel('BomChild');

				$data['BomChild']['name'] = $name;
				$this->BomChild->id = $id;
				if($this->BomChild->save($data)) {
					$json['status'] = true;
					$json['message'] = 'Data has been saved.';
				} else {
					$json['message'] = 'Error. Please refresh your browser.';
				}
			}
		}
		echo json_encode($json);
	}

	public function ajaxupdateitem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$id = $_POST['id'];
		}
		echo json_encode($json);
	}

	public function ajaxsavechild() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$json['status'] = false;
		if ($this->request->is('post')) {
			$parent_id = $_POST['parent_id'];
			$name = $_POST['name'];
			$bom_id = $_POST['bom_id'];
			if($name == '') {
				$json['message'] = 'Please enter Child Name.';
			} else {
				$this->loadModel('BomChild');
				$data = array(
					'name' => $name,
					'bom_id' => $bom_id,
					'parent_id' => $parent_id
					);
				if($this->BomChild->save($data)) {
					$json['status'] = true;
					$json['message'] = 'Child has been save.';
					$json['parent_name'] = $name;
					$json['bom_child_id'] = $this->BomChild->getLastInsertId();
				}
			}
		}
		echo json_encode($json);
	}

	public function ajaxtree($id) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$result = array(); 
		if (!$this->Bom->exists($id)) {
			$json['status'] = false;
		}

		$this->loadModel('BomChild');

		$rows = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $id,
				'BomChild.parent_id' => 0
				)
			));

		foreach ($rows as $row) { 
		    $json['attr']['id'] = $row['BomChild']['id']; 

		    $json['attr']['rel'] = $row['BomChild']['name']; 

		    $json['data']  = $row['BomChild']['name'];

		    $json['state'] = $this->check_child($id, $row['BomChild']['id']); // if state='closed' this node can be opened

		    $result[] = $json;
		}

		echo json_encode($result);
	}

	public function ajaxtreechild($id = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$result = array(); 
		if (!$this->Bom->exists($id)) {
			$json['status'] = false;
		}
		if(isset($this->request->query['id'])) {
			$parent_id = (int)$this->request->query['id']; 
		
			$this->loadModel('BomChild');

			$rows = $this->BomChild->find('all', array(
				'conditions' => array(
					'BomChild.bom_id' => $id,
					'BomChild.parent_id' => $parent_id
					)
				));
			// If no rows found, then try to show items
			$this->loadModel('BomItem'); 
			$items = $this->BomItem->find('all', array(
				'conditions' => array( 
					'BomItem.bom_child_id' => $parent_id
					)
				));
			if($rows || $items) {
				foreach ($rows as $row) { 
				    $json['attr']['id'] = $row['BomChild']['id']; 

				    $json['attr']['rel'] = $row['BomChild']['name']; 

				    $json['data']  = $row['BomChild']['name'];

				    $json['state'] = $this->check_child($id, $row['BomChild']['id']); // if state='closed' this node can be opened

				    $result[] = $json;
				}	
			//} else {
				// try to find items
				
				foreach ($items as $item) { 
				    $json['attr']['id'] = $item['BomItem']['id']; 

				    $json['attr']['rel'] = $item['InventoryItem']['name']; 

				    $json['data']  = $item['InventoryItem']['code'] . " Qty: " . $item['BomItem']['quantity'] . " " . $item['GeneralUnit']['name']; 

				    $json['state'] = ''; // if state='closed' this node can be opened

				    $result[] = $json;
				}
			}
			
		}
		echo json_encode($result);
	}

	private function check_child($id, $parent_id) {
		$this->loadModel('BomChild');
		$rows = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $id,
				'BomChild.parent_id' => $parent_id
				)
			));
		if(count($rows) > 0) {
			return "closed";
		} else {
			$this->loadModel('BomItem');
			$rows = $this->BomItem->find('all', array(
				'conditions' => array( 
					'BomItem.bom_child_id' => $parent_id
					)
				));
			if(count($rows) > 0) {
				return "closed";
			} else {
				return '';
			}
		}	
	}

	public function ajaxcopybom() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = str_replace('copybom-', '', $this->request->query['term']);
			$conditions = array();

			$conditions['OR']['Bom.name LIKE'] = '%'.$term.'%';
			$conditions['OR']['Bom.description LIKE'] = '%'.$term.'%';
			$conditions['OR']['Bom.code LIKE'] = '%'.$term.'%';

			$items = $this->Bom->find('all', array('conditions' => $conditions, 'group' => 'Bom.id'));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['Bom']['id'], 
					'name' => $item['Bom']['name'], 
					'code' => $item['Bom']['code'],
					'inventory_item_id' => $item['InventoryItem']['inventory_item_id'],
					'station' => $item['SaleJobChild']['name'],
					'station_name' => $item['SaleJobChild']['station_name']
					); 
			}
			echo json_encode($json);	
		}
	}

	public function ajaxcopychild() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = str_replace('copysub-', '', $this->request->query['term']);
			$conditions = array();
			$this->loadModel('BomChild');
			$conditions['BomChild.name LIKE'] = '%'.$term.'%'; 

			$items = $this->BomChild->find('all', array(
				'conditions' => $conditions,  
				'limit' => 5 
				));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['BomChild']['id'], 
					'name' => $item['BomChild']['name'], 
					'bom_name' => $item['Bom']['name'], 
					'code' => $item['Bom']['code'], 
					'bom_id' => $item['Bom']['id'],
					'inventory_item_id' => $item['BomChild']['inventory_item_id']
					); 
			}
			echo json_encode($json);	
		}
	}

	public function ajaxcopyitem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();
			$this->loadModel('BomChild');
			$conditions['BomChild.name LIKE'] = '%'.$term.'%'; 

			$items = $this->BomChild->find('all', array(
				'conditions' => $conditions,  
				'group' => 'BomChild.id',
				'limit' => 50 
				));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['BomChild']['id'], 
					'name' => $item['BomChild']['name'], 
					'bom_name' => $item['Bom']['name'], 
					'bom_code' => $item['Bom']['code'],  
					'bom_id' => $item['Bom']['id'] 
					); 
			}
			echo json_encode($json);	
		}
	}

	public function add() {
		if ($this->request->is('post')) {
			$date = date('Y-m-d H:i:s');
			$this->request->data['Bom']['user_id'] = $this->Session->read('Auth.User.id');
			$this->request->data['Bom']['created'] = $date;
			$this->request->data['Bom']['modified'] = $date;
			$this->request->data['Bom']['price'] = 0;
			$this->request->data['Bom']['bom_category_id'] = 0;
			
			$this->Bom->create();
			if ($this->Bom->save($this->request->data)) {
				$this->loadModel('BomChild');
				$bom_id = $this->Bom->getLastInsertId();
				$child_name = $this->request->data['BomChild']['name'];
				$copy_from_id = $this->request->data['BomChild']['copy_from_id'];
				$type = $this->request->data['BomChild']['type'];

				$old_bom_id = $this->request->data['BomChild']['bom_id'];

				$type = $this->request->data['BomChild']['type'];

				$item_type = $this->request->data['BomChild']['item_type'];

				$inventory_item_id = $this->request->data['BomChild']['inventory_item_id'];

				$quantity = $this->request->data['BomChild']['quantity'];

				$general_unit_id = $this->request->data['BomChild']['general_unit_id'];

				for($i = 0; $i < count($child_name); $i++) {
					// Insert Bom Child if name is filled
					$childs = array();
					if($child_name != '') { 
						$this->BomChild->create(); 
						$childs = array(
							'name' => $child_name[$i],
							'bom_id' => $bom_id,
							'inventory_item_id' => $inventory_item_id[$i],
							'parent_id' => 0,
							'old_id' => 0,
							'copy_from_id' => $copy_from_id[$i],
							'type' => $type[$i],
							'created' => $this->date,
							'quantity' => $quantity[$i],
							'general_unit_id' => $general_unit_id[$i]
							);  
						$this->BomChild->save($childs);
						$child_id = $this->BomChild->getLastInsertId();
						if($type[$i] == 'bom') {
							// Copy all item & child from BOM
							$this->attach_bom_child($copy_from_id[$i], $bom_id, $child_id);
						}
						if($type[$i] == 'child') {
							// Copy item only from child 
							$this->attach_child($copy_from_id[$i], $old_bom_id[$i], $bom_id, $child_id);
						}  
					} 
				}

				$user_id = $this->Session->read('Auth.User.id');
				$name = 'Add new BOM : ' . $this->request->data['Bom']['code'];
				$link = 'boms/view/'.$bom_id;
				$type = 'Add New Bom';
				
				$this->insert_log($user_id, $name, $link, $type);

				$this->Session->setFlash(__('The bom has been saved.'), 'success');
				return $this->redirect(array('action' => 'bomchild/'.$bom_id));
			} else {
				$this->Session->setFlash(__('The bom could not be saved. Please, try again.'));
			}
		}
		$bomCategories = $this->Bom->BomCategory->find('list');

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$users = $this->Bom->User->find('list');
		$this->set(compact('bomCategories', 'users', 'units'));
	}  

	private function findsub($child_id) {
		$this->loadModel('BomChild');
		$child = $this->BomChild->find('first', array(
			'conditions' => array(
				'BomChild.id' => $child_id
				)
			));
		return $child;
	}

	public function addsub($bom_id = null, $parent_id = null) {
		$this->loadModel('BomChild');
		if ($this->request->is('post')) {
			$this->BomChild->create();

			$this->request->data['BomChild']['bom_id'] = $bom_id;
			if($parent_id != null) {
				$this->request->data['BomChild']['parent_id'] = $parent_id;	
			} else {
				$this->request->data['BomChild']['parent_id'] = 0;	
			} 
			$copy_from_id = $this->request->data['BomChild']['copy_from_id'];
			$type = $this->request->data['BomChild']['type'];
			$from_bom_id = $this->request->data['BomChild']['from_bom_id'];

			$item_type = $this->request->data['BomChild']['item_type'];

			if($type == 'item') {
				$this->request->data['BomChild']['inventory_item_id'] = $copy_from_id;
			} else {
				$this->request->data['BomChild']['inventory_item_id'] = 0;
			}

			if ($this->BomChild->save($this->request->data)) {
				
				$insert_id = $this->BomChild->getLastInsertId(); 

				if($type == 'bom') {
					// Copy all item & child from BOM
					$this->attach_bom_child($copy_from_id, $bom_id, $insert_id);
				}
				if($type == 'child') {
					// Copy item only from child 
					$this->attach_child($copy_from_id, $from_bom_id, $bom_id, $insert_id);
				}  
				if($type == 'copyitem') {
					// Copy item only from child 
					$this->attach_child($copy_from_id, $from_bom_id, $bom_id, $insert_id);
				} 

				$this->Session->setFlash(__('The bom child has been saved.'), 'success');
				return $this->redirect(array('action' => 'bomchild', $bom_id));
			} else {
				$this->Session->setFlash(__('The bom child could not be saved. Please, try again.'));
			}
		}
		$bom = $this->BomChild->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $bom_id
				)
			));
		$this->set('bom', $bom);

		if($parent_id != null) {
			$child = $this->BomChild->find('first', array(
				'conditions' => array(
					'BomChild.id' => $parent_id
					)
				));
		} else {
			$child = null;
		}

		$this->loadModel('GeneralUnit');

		$unit = $this->GeneralUnit->find('list');

		$this->set(compact('unit'));

		$this->set('child', $child);
	}

	private function attach_items($from_child_id, $to_child_id, $to_bom_id) {

	}

	private function copyitem_bom_child($from_bom, $to_bom, $parent_id, $new_parent_id) {
		$this->loadModel('BomChild'); 
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $from_bom,
				'BomChild.parent_id' => $parent_id
				),
			'order' => array('BomChild.id' => 'ASC')
			));

		$items = array();
		foreach ($childs as $child) {
			$this->BomChild->create();

			$data = array(
				'name' => $child['BomChild']['name'],
				'inventory_item_id' => $child['BomChild']['inventory_item_id'],
				'bom_id' => $to_bom,
				'parent_id' => $new_parent_id,
				'old_id' => $child['BomChild']['id'],
				'copy_from_id' => 0,
				'type' => $child['BomChild']['type']
				);
			$this->BomChild->save($data);
			$bom_child_id = $this->BomChild->getLastInsertId(); 

			// Copy each child item
			$this->copy_bom_item($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom); 

			$this->attach_looping($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom);
		} 
	} 

	private function attach_bom_child($from_bom, $to_bom, $parent_id) {
		$this->loadModel('BomChild'); 
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $from_bom,
				'BomChild.parent_id' => 0
				),
			'order' => array('BomChild.id' => 'ASC')
			));

		$items = array();
		foreach ($childs as $child) {
			$this->BomChild->create();

			$data = array(
				'name' => $child['BomChild']['name'],
				'inventory_item_id' => $child['BomChild']['inventory_item_id'],
				'bom_id' => $to_bom,
				'parent_id' => $parent_id,
				'old_id' => $child['BomChild']['id'],
				'copy_from_id' => 0,
				'type' => $child['BomChild']['type']
				);
			$this->BomChild->save($data);
			$bom_child_id = $this->BomChild->getLastInsertId(); 

			// Copy each child item
			$this->copy_bom_item($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom); 

			$this->attach_looping($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom);
		} 
	} 

	private function attach_looping($old_parent_id, $new_parent_id, $from_bom, $to_bom) {
		// Check if has child
		$this->loadModel('BomChild'); 
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $from_bom,
				'BomChild.parent_id' => $old_parent_id
				),
			'recursive' => -1,
			'order' => array('BomChild.id' => 'ASC')
			));  
		foreach ($childs as $child) {
			$this->BomChild->create();

			$data = array(
				'name' => $child['BomChild']['name'],
				'inventory_item_id' => $child['BomChild']['inventory_item_id'],
				'bom_id' => $to_bom,
				'parent_id' => $new_parent_id,
				'old_id' => $child['BomChild']['id'],
				'copy_from_id' => 0,
				'type' => $child['BomChild']['type']
				);
			$this->BomChild->save($data);
			$bom_child_id = $this->BomChild->getLastInsertId();
 
			$this->update_parent_id($to_bom, $child['BomChild']['parent_id'], $bom_child_id);
			 
			// Copy each child item
			$this->copy_bom_item($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom);   

			$this->attach_looping($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom); 
		}  	 
		//$this->copy_bom_item($old_parent_id, $new_parent_id, $from_bom, $to_bom);
	}

	private function attach_child($old_parent_id, $from_bom, $to_bom, $new_parent_id) {
		$this->loadModel('BomChild'); 
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $from_bom,
				'BomChild.parent_id' => $old_parent_id
				),
			'order' => array('BomChild.id' => 'ASC')
			));

		$items = array();
		foreach ($childs as $child) {
			$this->BomChild->create();

			$data = array(
				'name' => $child['BomChild']['name'],
				'inventory_item_id' => $child['BomChild']['inventory_item_id'],
				'bom_id' => $to_bom,
				'parent_id' => $new_parent_id,
				'old_id' => $child['BomChild']['id'],
				'copy_from_id' => 0,
				'type' => $child['BomChild']['type'],
				'created' => $this->date
				);
			$this->BomChild->save($data);
			$bom_child_id = $this->BomChild->getLastInsertId();  
			// Copy each child item
			$this->copy_bom_item($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom); 

			$this->attach_looping($child['BomChild']['id'], $bom_child_id, $from_bom, $to_bom);
		}  
		// Copy item same level as child
		$this->copy_bom_item($old_parent_id, $new_parent_id, $from_bom, $to_bom); 
	}   

	private function check_has_child($id) {
		$this->loadModel('BomChild'); 
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $from_bom,
				'BomChild.parent_id' => $old_parent_id
				),
			'recursive' => -1 
			));  
	}

	public function tableview($id = null) { 

		$this->loadModel('InventoryItemCategory');
		$categories = $this->InventoryItemCategory->find('all', array(
			'recursive' => -1
			));

		$options = array(
			'conditions' => array(
				'Bom.' . $this->Bom->primaryKey => $id
				),
			'recursive' => 2
			);
		$this->set('bom', $this->Bom->find('first', $options));

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('all', array(
			'recursive' => -1
			));

		$this->set('units', $units);
		$this->set('categories', $categories);
	}

	public function downloadexcel() { 
		 
		$this->excelConfig =  array(
			'filename' => 'downloadexcel.xlsx'
		);
 
	}

	public function printview($id = null) { 
		$this->layout = 'printing'; 
		$this->loadModel('InventoryItemCategory');
		$categories = $this->InventoryItemCategory->find('all', array(
			'recursive' => -1
			));

		$options = array(
			'conditions' => array(
				'Bom.' . $this->Bom->primaryKey => $id
				),
			'recursive' => 2
			);
		$this->set('bom', $this->Bom->find('first', $options));
		$this->loadModel('BomChild');
		$options = array(
			'conditions' => array(
				'BomChild.bom_id' => $id,
				'BomChild.parent_id' => 0
				),
			'recursive' => 2
			);
		$this->set('childs', $this->BomChild->find('all', $options));

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('all', array(
			'recursive' => -1
			));

		$this->set('units', $units);
		$this->set('categories', $categories);
 
	}

	public function bomchild($id = null) { 

		$this->loadModel('InventoryItemCategory');
		$categories = $this->InventoryItemCategory->find('all', array(
			'recursive' => -1
			));

		$options = array(
			'conditions' => array(
				'Bom.' . $this->Bom->primaryKey => $id
				),
			'recursive' => 2
			);
		$this->set('bom', $this->Bom->find('first', $options));

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('all', array(
			'recursive' => -1
			));

		$this->set('units', $units);
		$this->set('categories', $categories);
	}  

	public function ajaxloadchild() {
		$this->autoRender = false; 
		$json = array();
		if(isset($this->request->query['parent_id'])) {
			$parent_id = (int)$this->request->query['parent_id'];

			$cond['BomChild.parent_id']  = $parent_id;
 
			$this->loadModel('BomChild');
			$rows = $this->BomChild->find('all', array(
				'conditions' => array( 
					'BomChild.parent_id' => $parent_id
					),
				'order' => array(
					'BomChild.id' => 'ASC'
					),
				'recursive' => 0
				));
			// If no rows found, then try to show items
			if($rows) {
				foreach ($rows as $row) {
					$json['data'][] = $row; 
				} 
			}

			$this->loadModel('BomItem');
			$items = $this->BomItem->find('all', array(
				'conditions' => array( 
					'BomItem.bom_child_id' => $parent_id
					),
				'order' => array(
					'BomItem.id' => 'ASC'
					)
				));

			if($items) {
				foreach ($items as $item) {
					$json['items'][] = $item;
				} 
			}
		}
		echo json_encode($json);
	}

	public function ajaxfindbombycategoryid() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['category_id'])) {
			$id = $this->request->query['category_id'];
			if(is_numeric($id)) {
				$this->loadModel('Bom');
				$items = $this->Bom->find('all', array('conditions' => array('Bom.bom_category_id' => $id)));
				$json = array();
				foreach ($items as $item) {
					$json[] = array('id' => $item['Bom']['id'], 'name' => $item['Bom']['name'], 'status' => 'bom');
				}	
			} else { 
				$prefix = explode('-', $id); 
				$id = $prefix[1];

				if($prefix[0] == 'item') {
					$this->loadModel('InventoryItem');
					$items = $this->InventoryItem->find('all', array('conditions' => array('InventoryItem.inventory_item_category_id' => $id)));
					$json = array();
					foreach ($items as $item) {
						$json[] = array('id' => $item['InventoryItem']['id'], 'name' => $item['InventoryItem']['name'], 'status' => 'item');
					}	
				} elseif($prefix[0] == 'servis') {

				}
				
			}
			
			echo json_encode($json);	
		} 
	}

	public function ajaxitemcategory() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['category_id'])) {
			$id = (int)$this->request->query['category_id'];
			$this->loadModel('InventoryItem');
			$items = $this->InventoryItem->find('all', array('conditions' => array('InventoryItem.inventory_item_category_id' => $id)));
			$json = array();
			foreach ($items as $item) {
				$json[] = array('id' => $item['InventoryItem']['id'], 'name' => $item['InventoryItem']['name']);
			}
			echo json_encode($json);	
		}
		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Bom->exists($id)) {
			throw new NotFoundException(__('Invalid bom'));
		}

		$options = array('conditions' => array('Bom.id'=> $id));
		$bom = $this->Bom->find('first', $options);

		 

		if ($this->request->is(array('post', 'put'))) {

			$this->request->data['Bom']['modified'] = $this->date;

			if ($this->Bom->save($this->request->data)) {

				$user_id = $this->Session->read('Auth.User.id');
				$name = 'Edit BOM Information : ' . $bom['Bom']['code'];
				$link = 'boms/view/'.$bom['Bom']['id'];
				$type = 'Edit Bom';
				
				$this->insert_log($user_id, $name, $link, $type);

				$this->Session->setFlash(__('The bom has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bom could not be saved. Please, try again.'));
			}
		}  
		$this->request->data = $bom; 
		 
		// View Bom
		$this->set('bom', $bom);
		$bomCategories = $this->Bom->BomCategory->find('list', array('reqursive' => -1));
		 
		$this->set(compact('bomCategories'));
 


		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('all', array('reqursive' => -1));

		$this->set('units', $units); 
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Bom->id = $id;
		if (!$this->Bom->exists()) {
			throw new NotFoundException(__('Invalid bom'));
		}

		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $id
				)
			));

		$this->request->allowMethod('post', 'delete');
		if ($this->Bom->delete()) {

			$this->loadModel('BomChild');
			$this->loadModel('BomItem');

			$this->BomChild->deleteAll(array('BomChild.bom_id' => $id), false);
			$this->BomItem->deleteAll(array('BomItem.bom_id' => $id), false); 

				$user_id = $this->Session->read('Auth.User.id');
				$name = 'BOM Deleted : ' . $bom['Bom']['code'];
				$link = 'boms/view/'.$id;
				$type = 'BOM Deleted';
				
				$this->insert_log($user_id, $name, $link, $type);

			$this->Session->setFlash(__('The bom has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The bom could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
