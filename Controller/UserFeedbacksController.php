<?php
App::uses('AppController', 'Controller');
/**
 * UserFeedbacks Controller
 *
 * @property UserFeedback $UserFeedback
 * @property PaginatorComponent $Paginator
 */
class UserFeedbacksController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$group_id = $this->Session->read('Auth.User.group_id');
		if($group_id != 1) {
			$conditions['UserFeedback.user_id'] = $this->user_id; 
		} 
		$conditions['UserFeedback.id !='] = array(0);

		$record_per_page = Configure::read('Reading.nodes_per_page'); 
		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'UserFeedback.id DESC',
			'limit' => $record_per_page
			);   
		$data = $this->Paginator->paginate();

		$this->set('userFeedbacks', $data);
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this->UserFeedback->exists($id)) {
			throw new NotFoundException(__('Invalid user feedback'));
		}
		$options = array('conditions' => array('UserFeedback.' . $this->UserFeedback->primaryKey => $id));
		$this->set('userFeedback', $this->UserFeedback->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserFeedback->create();
			$this->request->data['UserFeedback']['created'] = $this->date;
			$this->request->data['UserFeedback']['user_id'] = $this->user_id;
			if ($this->UserFeedback->save($this->request->data)) {

				

				$this->Session->setFlash(__('The user feedback has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user feedback could not be saved. Please, try again.'), 'error');
			}
		}
		$users = $this->UserFeedback->User->find('list');
		$this->set(compact('users'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		if (!$this->UserFeedback->exists($id)) {
			throw new NotFoundException(__('Invalid user feedback'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UserFeedback->save($this->request->data)) {
				$this->Session->setFlash(__('The user feedback has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user feedback could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('UserFeedback.' . $this->UserFeedback->primaryKey => $id));
			$this->request->data = $this->UserFeedback->find('first', $options);
		}
		$users = $this->UserFeedback->User->find('list');
		$this->set(compact('users'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->UserFeedback->id = $id;
		if (!$this->UserFeedback->exists()) {
			throw new NotFoundException(__('Invalid user feedback'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserFeedback->delete()) {
			$this->Session->setFlash(__('The user feedback has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The user feedback could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
