<?php
App::uses('AppController', 'Controller');
/**
 * InventorySupplierAddresses Controller
 *
 * @property InventorySupplierAddress $InventorySupplierAddress
 * @property PaginatorComponent $Paginator 
 * @property SessionComponent $Session
 */
class InventorySupplierAddressesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventorySupplierAddress->recursive = 0;
		$this->set('inventorySupplierAddresses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventorySupplierAddress->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier address'));
		}
		$options = array('conditions' => array('InventorySupplierAddress.' . $this->InventorySupplierAddress->primaryKey => $id));
		$this->set('inventorySupplierAddress', $this->InventorySupplierAddress->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			//print_r($_POST);
			//exit;
			$this->InventorySupplierAddress->create();
			if ($this->InventorySupplierAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier address has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier address could not be saved. Please, try again.'), 'error');
			}
		}
		$inventorySuppliers = $this->InventorySupplierAddress->InventorySupplier->find('list');
		$states = $this->InventorySupplierAddress->State->find('list');
		$countries = $this->InventorySupplierAddress->Country->find('list');
		$users = $this->InventorySupplierAddress->User->find('list');
		$this->set(compact('inventorySuppliers', 'states', 'countries', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventorySupplierAddress->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventorySupplierAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier address has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier address could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('InventorySupplierAddress.' . $this->InventorySupplierAddress->primaryKey => $id));
			$this->request->data = $this->InventorySupplierAddress->find('first', $options);
		}
		$inventorySuppliers = $this->InventorySupplierAddress->InventorySupplier->find('list');
		$states = $this->InventorySupplierAddress->State->find('list');
		$countries = $this->InventorySupplierAddress->Country->find('list');
		$users = $this->InventorySupplierAddress->User->find('list');
		$this->set(compact('inventorySuppliers', 'states', 'countries', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventorySupplierAddress->id = $id;
		if (!$this->InventorySupplierAddress->exists()) {
			throw new NotFoundException(__('Invalid inventory supplier address'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventorySupplierAddress->delete()) {
			$this->Session->setFlash(__('The inventory supplier address has been deleted.'), 'error');
		} else {
			$this->Session->setFlash(__('The inventory supplier address could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
