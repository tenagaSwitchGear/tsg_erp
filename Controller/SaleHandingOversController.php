<?php
App::uses('AppController', 'Controller');
/**
 * SaleHandingOvers Controller
 *
 * @property SaleHandingOver $SaleHandingOver
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleHandingOversController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator', 'Session');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function receiveindex($status = null) {
        if($status == null) {
            $hod_status = 'Approved';
        } else {
            $hod_status = (string)$status;
        }
        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array( 
            'conditions' => array(
                'SaleHandingOver.status' => $hod_status
                ),
            'order' => 'SaleHandingOver.id DESC',
            'limit' => $record_per_page
            );  
        $this->set('saleHandingOvers', $this->Paginator->paginate());
    }

/**
 * index method
 *
 * @return void
 */
    public function index($status = null) {
        if($status == null) {
            $hod_status = 'Sent';
        } else {
            $hod_status = (string)$status;
        }

        if($this->group_id != 1) {
            $cond['SaleHandingOver.group_id'] = $this->group_id;
        }
        $cond['SaleHandingOver.status'] = $hod_status;

        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array( 
            'conditions' => $cond,
            'order' => 'SaleHandingOver.id DESC',
            'limit' => $record_per_page
            );  
        $this->set('saleHandingOvers', $this->Paginator->paginate());
    }
 
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null, $status = null, $user = null) {
        if (!$this->SaleHandingOver->exists($id)) {
            throw new NotFoundException(__('Invalid sale handing over'));
        }

        $user_id = $this->Session->read('Auth.User.id');

        $options = array('conditions' => array('SaleHandingOver.' . $this->SaleHandingOver->primaryKey => $id));
        $hod = $this->SaleHandingOver->find('first', $options);

        $this->loadModel('SaleOrder');

        $so = $this->SaleOrder->find('first', array(
            'conditions' => array(
                'SaleOrder.sale_job_child_id' => $hod['SaleHandingOver']['sale_job_child_id']
                )
            ));

        $this->set('saleHandingOver', $hod);

        $this->loadModel('SaleHandingOverUser');

        $users = $this->SaleHandingOverUser->find('all', array(
            'conditions' => array(
                'SaleHandingOverUser.sale_handing_over_id' => $id
                )
            ));

        

        $recipient = array();
        foreach ($users as $user) {
            $recipient[] = $user['SaleHandingOverUser']['user_id'];
        } 

        if($status != null && $user != null) { 
            if(in_array($user_id, $recipient)) {
                $update = $this->SaleHandingOverUser->updateAll(
                    array(
                        'SaleHandingOverUser.status' => "'Received'",
                        'SaleHandingOverUser.receive_date' => "'".$this->date."'"
                        ),
                    array(
                        'SaleHandingOverUser.user_id' => $user_id,
                        'SaleHandingOverUser.sale_handing_over_id' => $id,
                        )
                    );

                if($update) {
                    $received = $this->SaleHandingOverUser->find('all', array(
                    'conditions' => array(
                        'SaleHandingOverUser.sale_handing_over_id' => $id,
                        'SaleHandingOverUser.status' => 'Received'
                        ),
                    'recursive' => -1
                    )); 
                    $waiting = count($users);
                    $receive = count($received);

                    if($waiting == $receive) {
                        // Update HOD status
                        $this->SaleHandingOver->updateAll(
                        array(
                            'SaleHandingOver.status' => "'Received'",
                            'SaleHandingOver.modified' => "'".$this->date."'"
                            ),
                        array(
                            'SaleHandingOver.id' => $id,
                            'SaleHandingOver.status' => 'Sent',
                            )
                        );

                        // Send mail to sender 
                        $data['subject'] = 'Handing Over Document Status';
                        $data['to'] = $hod['User']['email'];
                        $data['template'] = 'handing_over_document_received'; 
                        $data['content'] = array(
                            'job' => $hod['SaleJobChild']['station_name'],
                            'username' => $hod['User']['username'],
                            'link' => 'sale_handing_overs/view/' . $id
                            );

                        $this->send_email($data);
                    }

                    $this->Session->setFlash(__('You have remark Handing Over Document as Received.'), 'success');
                    return $this->redirect(array('action' => 'receiveindex'));
                }
            } else {
                $this->Session->setFlash(__('Invalid request 78.'), 'error');
            } 
        } 

        $this->set('recipient', $recipient);
        $this->set('users', $users);

        $this->loadModel('SaleOrderItem');
        $items = $this->SaleOrderItem->find('all', array(
            'conditions' => array(
                'SaleOrderItem.sale_job_child_id' => $hod['SaleHandingOver']['sale_job_child_id']
                )
            ));
        $this->set('items', $items);
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() { 
        if ($this->request->is('post')) {
            $this->SaleHandingOver->create();

            $this->request->data['SaleHandingOver']['created'] = $this->date;
            $this->request->data['SaleHandingOver']['modified'] = null;
            $this->request->data['SaleHandingOver']['user_id'] = $this->user_id;
            $this->request->data['SaleHandingOver']['status'] = 'Waiting Verification';
            $this->request->data['SaleHandingOver']['sale_order_id'] = 0;
            $job_no = $this->request->data['SaleHandingOver']['find_job'];
            $user = $this->request->data['user_id'];
            $this->request->data['SaleHandingOver']['group_id'] = $this->group_id; 
              
            if(count($user) > 0) {
                if ($this->SaleHandingOver->save($this->request->data)) {
                    //debug($this->SaleHandingOver->getDataSource());
                    $hod_id = $this->SaleHandingOver->getLastInsertId(); 

                    $this->loadModel('User'); 

                    $this->loadModel('SaleHandingOverUser'); 
                    for($i = 0; $i < count($user); $i++) {
                        $this->SaleHandingOverUser->create();
                        $data = array(
                            'user_id' => $user[$i],
                            'receive_date' => null,
                            'status' => 'Waiting',
                            'sale_handing_over_id' => $hod_id
                            );
                        $this->SaleHandingOverUser->save($data); 
                    }

                    $group_id =$this->Session->read('Auth.User.group_id');
                    $this->loadModel('Approval');
                    $cond['Approval.name'] = 'HOD';
                    $cond['Approval.type'] = 'Verify';

                    // Update sale order
                    $progress = $this->update_sale_order_progress($this->request->data['SaleHandingOver']['sale_job_child_id'], 'Handing Over Document');

                    if($progress === false) {
                        $this->Session->setFlash(__('Unable to update progress, Please contact administrator. Error Code: E219.'), 'error');
                        return $this->redirect(array('action' => 'index'));
                    }
                   
                    $approvals = $this->Approval->find('all', array(
                        'conditions' => $cond
                        ));
                    if($approvals) {
                        foreach ($approvals as $approval) {
                            $data = array(
                                'to' => $approval['User']['email'],
                                'template' => 'hodverification',
                                'subject' => 'Handing Over Document Require Your Verification. Job No: ' . $job_no,
                                'content' => array(
                                    'username' => $approval['User']['username'],
                                    'itemtoverified' => 'HOD ('.$job_no.')',
                                    'action' => 'Verification',
                                    'link' => 'sale_handing_overs/verifyview/'.$hod_id
                                    )
                                );
                            $this->send_email($data);
                        }  

                        $this->Session->setFlash(__('HOD has been sent for Approval.'), 'success');
                        return $this->redirect(array('action' => 'view', $hod_id));
                         
                    } else {
                        $this->Session->setFlash(__('User Approval not found. Please contact Administrator.'), 'error');
                        return $this->redirect(array('action' => 'view', $id));
                    }

                    
                } else {

                    $this->Session->setFlash(__('The sale handing over could not be saved.'), 'error');
                }
            } else {
                $this->Session->setFlash(__('Please add atleast 1 user'), 'error');
            } 
        }
        $saleQuotations = $this->SaleHandingOver->SaleQuotation->find('list');
        $customers = $this->SaleHandingOver->Customer->find('list'); 
        $saleJobs = $this->SaleHandingOver->SaleJob->find('list');
        $users = $this->SaleHandingOver->User->find('list');
        $this->set(compact('saleQuotations', 'customers', 'saleJobs', 'users'));
    }

    public function approval($status = null) {
        if($status == null) {
            $hod_status = 'Waiting Approval';
        } else {
            $hod_status = (string)$status;
        }
        $this->loadModel('Approval');
        $cond['Approval.name'] = 'HOD';
        $cond['Approval.user_id'] = $this->user_id;
        $approvals = $this->Approval->find('all', array(
            'conditions' => $cond
            ));
        //var_dump($approvals);
        if(!$approvals) {
            $this->Session->setFlash(__('You are not authorize to view this area.'), 'error');
            return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
        }
        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array( 
            'conditions' => array(
                'SaleHandingOver.status' => $hod_status
                ),
            'order' => 'SaleHandingOver.id DESC',
            'limit' => $record_per_page
            );  
        $this->set('saleHandingOvers', $this->Paginator->paginate());
    }
 
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function verifyview($id = null, $status = null, $user = null) {
        if (!$this->SaleHandingOver->exists($id)) {
            throw new NotFoundException(__('Invalid sale handing over'));
        }

        $this->loadModel('Approval');
        $cond['Approval.name'] = 'HOD';
        $cond['Approval.user_id'] = $this->user_id; 
        $approvals = $this->Approval->find('all', array(
            'conditions' => $cond
            ));
        if(count($approvals) == 0) {
            $this->Session->setFlash(__('You are not authorize to view this area.'), 'error');
            return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
        }
        $options = array('conditions' => array('SaleHandingOver.' . $this->SaleHandingOver->primaryKey => $id));
        $hod = $this->SaleHandingOver->find('first', $options);

        $job_no = $hod['SaleJobChild']['station_name'];

        if ($this->request->is(array('post', 'put'))) {
            $hodstatus = $this->request->data['SaleHandingOver']['status'];

            if($hodstatus == 'Waiting Approval') {
                $appro = 'Verified'; 
            } elseif($hodstatus == 'Approved') {
                $appro = 'Approved';
            } else {
                $appro = $hodstatus;
            }
            $by = $this->request->data['SaleHandingOver']['by'];
            if($this->SaleHandingOver->save($this->request->data)) {
                
                if($hodstatus == 'Waiting Approval') { // mean verified by hod
                    $this->loadModel('Approval'); 
 
                    $group_id =$this->Session->read('Auth.User.group_id');
                    $this->loadModel('Approval');
                    $cond['Approval.name'] = 'HOD';
                    $cond['Approval.type'] = 'Approval';
 
                    $approvals = $this->Approval->find('all', array(
                        'conditions' => $cond
                        ));
                    if($approvals) {
                        foreach ($approvals as $approval) {
                            $data = array(
                                'to' => $approval['User']['email'],
                                'template' => 'hodverification',
                                'subject' => 'Handing Over Document Require Your Approval. Job No: ' . $job_no,
                                'content' => array(
                                    'username' => $approval['User']['username'],
                                    'itemtoverified' => 'HOD ('.$job_no.')',
                                    'action' => 'Approval',
                                    'link' => 'sale_handing_overs/verifyview/'.$hod_id
                                    )
                                );
                            $this->send_email($data);
                        }   
                    } 
                }

                if($hodstatus == 'Approved') {
                    $this->loadModel('User'); 

                    $this->loadModel('SaleHandingOverUser'); 
                    $hodusers = $this->SaleHandingOverUser->find('all', array(
                        'conditions' => array(
                            'SaleHandingOverUser.sale_handing_over_id' => $id
                            )
                        ));

                    foreach($hodusers as $hoduser) {
                          
                        $data['subject'] = 'New Handing Over Document Job No: ' . $job_no;
                        $data['to'] = $hoduser['User']['email'];
                        $data['template'] = 'handing_over_document'; 
                        $data['content'] = array(
                            'username' => $hoduser['User']['username'],
                            'link' => 'sale_handing_overs/view/' . $id
                            );

                        $this->send_email($data);
                    }
                }

                $data = array(
                    'to' => $hod['User']['email'],
                    'template' => 'hodverificationresponse',
                    'subject' => 'Handing Over Document Has Been ' . $appro . ' By ' . $by,
                    'content' => array(
                        'username' => $hod['User']['username'],
                        'itemtoverified' => 'HOD ('.$hod['SaleJobChild']['station_name'].')',
                        'action' => $appro,
                        'by' => $by,
                        'link' => 'sale_handing_overs/view/'.$id
                        )
                    );
                $this->send_email($data);

                $this->Session->setFlash(__('HOD has been ' . $appro), 'success');
                return $this->redirect(array('action' => 'approval'));
            } else {
                 $this->Session->setFlash(__('Error code 341'), 'error');
            }
        } else {
            $this->request->data = $hod;
        }

        $user_id = $this->Session->read('Auth.User.id');

       

        $this->loadModel('SaleOrder');

        $so = $this->SaleOrder->find('first', array(
            'conditions' => array(
                'SaleOrder.sale_job_child_id' => $hod['SaleHandingOver']['sale_job_child_id']
                )
            ));

        $this->set('saleHandingOver', $hod);

        $this->loadModel('SaleHandingOverUser');

        $users = $this->SaleHandingOverUser->find('all', array(
            'conditions' => array(
                'SaleHandingOverUser.sale_handing_over_id' => $id
                )
            ));

        

        $recipient = array();
        foreach ($users as $user) {
            $recipient[] = $user['SaleHandingOverUser']['user_id'];
        }

         

        if($status != null && $user != null) {
             
                if(in_array($user_id, $recipient)) {
                    $update = $this->SaleHandingOverUser->updateAll(
                        array(
                            'SaleHandingOverUser.status' => "'Received'",
                            'SaleHandingOverUser.receive_date' => "'".$this->date."'"
                            ),
                        array(
                            'SaleHandingOverUser.user_id' => $user_id,
                            'SaleHandingOverUser.sale_handing_over_id' => $id,
                            )
                        );

                    if($update) {
                        $received = $this->SaleHandingOverUser->find('all', array(
                        'conditions' => array(
                            'SaleHandingOverUser.sale_handing_over_id' => $id,
                            'SaleHandingOverUser.status' => 'Received'
                            ),
                        'recursive' => -1
                        )); 
                        $waiting = count($users);
                        $receive = count($received);

                        if($waiting == $receive) {
                            // Update HOD status
                            $this->SaleHandingOver->updateAll(
                            array(
                                'SaleHandingOver.status' => "'Approved'",
                                'SaleHandingOver.modified' => "'".$this->date."'"
                                ),
                            array(
                                'SaleHandingOver.id' => $id,
                                'SaleHandingOver.status' => 'Sent',
                                )
                            );

                            // Send mail to sender 
                            $data['subject'] = 'Handing Over Document Status Approved';
                            $data['to'] = $hod['User']['email'];
                            $data['template'] = 'handing_over_document_received'; 
                            $data['content'] = array(
                                'username' => $hod['User']['username'],
                                'link' => 'sale_handing_overs/view/' . $id
                                );

                            $this->send_email($data);
                        }

                        $this->Session->setFlash(__('You have remark Handing Over Document as Received.'), 'success');
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $this->Session->setFlash(__('Invalid request 78.'), 'error');
                }
             
        }
 


        $this->set('recipient', $recipient);
        $this->set('users', $users);

        $this->loadModel('SaleOrderItem');
        $items = $this->SaleOrderItem->find('all', array(
            'conditions' => array(
                'SaleOrderItem.sale_job_child_id' => $hod['SaleHandingOver']['sale_job_child_id']
                )
            ));
        $this->set('items', $items);
    }


/**

    protected function send_email($data = array()) {
        $Email = new CakeEmail('smtp');
        $Email->from('mohdazrul.jmsb@gmail.com');
        $Email->to($data['to']); 
        $Email->emailFormat('html');
        $Email->template($data['template'])->viewVars($data['content']);
        $Email->subject($data['subject']); 
        if(!$Email->send()) {
            return false;
        }
        return true;
    }


 * edit method 
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        if (!$this->SaleHandingOver->exists($id)) {
            throw new NotFoundException(__('Invalid sale handing over'));
        }
        if ($this->request->is(array('post', 'put'))) {
 
            $this->request->data['SaleHandingOver']['modified'] = $this->date;
            $this->request->data['SaleHandingOver']['user_id'] = $this->user_id;

            if ($this->SaleHandingOver->save($this->request->data)) {  
                $this->Session->setFlash(__('The sale handing over has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sale handing over could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SaleHandingOver.' . $this->SaleHandingOver->primaryKey => $id));
            $this->request->data = $this->SaleHandingOver->find('first', $options);
        }
        $saleQuotations = $this->SaleHandingOver->SaleQuotation->find('list');
        $customers = $this->SaleHandingOver->Customer->find('list');
        $saleOrders = $this->SaleHandingOver->SaleOrder->find('list');
        $saleJobs = $this->SaleHandingOver->SaleJob->find('list');
        $users = $this->SaleHandingOver->User->find('list');
        $this->set(compact('saleQuotations', 'customers', 'saleOrders', 'saleJobs', 'users'));
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        $this->SaleHandingOver->id = $id;
        if (!$this->SaleHandingOver->exists()) {
            throw new NotFoundException(__('Invalid sale handing over'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->SaleHandingOver->delete()) {
            $this->Session->setFlash(__('The sale handing over has been deleted.'));
        } else {
            $this->Session->setFlash(__('The sale handing over could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
} 