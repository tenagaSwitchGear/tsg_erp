<?php
App::uses('AppController', 'Controller');
/**
 * InventoryPurchaseRequisitionItems Controller
 *
 * @property InventoryPurchaseRequisitionItem $InventoryPurchaseRequisitionItem
 * @property PaginatorComponent $Paginator
 */
class InventoryPurchaseRequisitionItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
 
    public $components = array('Paginator');

    public function beforeFilter() {
        parent::beforeFilter();
    }
 
    public function index($id = NULL) {
        //$this->InventoryPurchaseRequisitionItem->recursive = 0;
        $conditions['InventoryPurchaseRequisitionItem.inventory_item_id'] = $id;
        $record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array('conditions' => $conditions, 'limit' => $record_per_page);
        $this->set('inventoryPurchaseRequisitionItems', $this->Paginator->paginate());
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function list_history($id = null){
        $this->loadModel('InventoryDeliveryOrderItem');
        $items = $this->InventoryDeliveryOrderItem->find('all', array(
            'conditions' => array(
                'InventoryDeliveryOrderItem.inventory_item_id' => $id
            ),
           'recursive' => -1
        ));
        $supplier_item_array = array();
        foreach ($items as $item) {
            $supplier_item_array[] = array( 
                'InventoryDeliveryOrderItem' => $item['InventoryDeliveryOrderItem'],
                'InventoryItem' => $this->get_inv_item($item['InventoryDeliveryOrderItem']['inventory_item_id'])
            );
        }

        $this->loadModel('InventoryItem');
        $item = $this->InventoryItem->find('all', array(
            'conditions' => array(
                'InventoryItem.id' => $id
            ),
            'recursive' => -1
        ));

        $item_detail = $item[0];

        
        $this->set('supplier_item', $supplier_item_array);
        $this->set('item', $item_detail);
    }

    private function get_inv_item($id){
        $this->loadModel('InventoryItem');
        $item_array = array();
        $item = $this->InventoryItem->find('all', array(
            'conditions' => array(
                'InventoryItem.id' => $id
            )
        ));
        $item_array[] = array(
            'InventoryItem' => $item[0]['InventoryItem'],
            'GeneralUnit' => $item[0]['GeneralUnit']
        );

        return $item;
    }
 
    public function view($id = null) {
        if (!$this->InventoryPurchaseRequisitionItem->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition item'));
        }
        $options = array('conditions' => array('InventoryPurchaseRequisitionItem.' . $this->InventoryPurchaseRequisitionItem->primaryKey => $id));
        $this->set('inventoryPurchaseRequisitionItem', $this->InventoryPurchaseRequisitionItem->find('first', $options));
    }

/**
 * add method
 *
 * @return void
 */
 
    public function add() {
        if ($this->request->is('post')) {
            $this->InventoryPurchaseRequisitionItem->create();
            if ($this->InventoryPurchaseRequisitionItem->save($this->request->data)) {
                $this->Session->setFlash(__('The inventory purchase requisition item has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The inventory purchase requisition item could not be saved. Please, try again.'));
            }
        }
        $inventoryPurchaseRequisitions = $this->InventoryPurchaseRequisitionItem->InventoryPurchaseRequisition->find('list');
        $generalUnits = $this->InventoryPurchaseRequisitionItem->GeneralUnit->find('list');
        $this->set(compact('inventoryPurchaseRequisitions', 'generalUnits'));
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 
    public function edit($id = null) {
        if (!$this->InventoryPurchaseRequisitionItem->exists($id)) {
            throw new NotFoundException(__('Invalid inventory purchase requisition item'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->InventoryPurchaseRequisitionItem->save($this->request->data)) {
                $this->Session->setFlash(__('The inventory purchase requisition item has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The inventory purchase requisition item could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('InventoryPurchaseRequisitionItem.' . $this->InventoryPurchaseRequisitionItem->primaryKey => $id));
            $this->request->data = $this->InventoryPurchaseRequisitionItem->find('first', $options);
        }
        $inventoryPurchaseRequisitions = $this->InventoryPurchaseRequisitionItem->InventoryPurchaseRequisition->find('list');
        $generalUnits = $this->InventoryPurchaseRequisitionItem->GeneralUnit->find('list');
        $this->set(compact('inventoryPurchaseRequisitions', 'generalUnits'));
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 
    public function delete($id = null) {
        $this->InventoryPurchaseRequisitionItem->id = $id;
        if (!$this->InventoryPurchaseRequisitionItem->exists()) {
            throw new NotFoundException(__('Invalid inventory purchase requisition item'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->InventoryPurchaseRequisitionItem->delete()) {
            $this->Session->setFlash(__('The inventory purchase requisition item has been deleted.'));
        } else {
            $this->Session->setFlash(__('The inventory purchase requisition item could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
 
}
 
 
