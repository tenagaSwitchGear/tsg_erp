<?php
App::uses('AppController', 'Controller');
/**
 * PrintTemplates Controller
 *
 * @property PrintTemplate $PrintTemplate
 * @property PaginatorComponent $Paginator
 */
class PrintTemplatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PrintTemplate->recursive = 0;
		$this->set('printTemplates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PrintTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid print template'));
		}
		$options = array('conditions' => array('PrintTemplate.' . $this->PrintTemplate->primaryKey => $id));
		$this->set('printTemplate', $this->PrintTemplate->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PrintTemplate->create();
 

			if ($this->PrintTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The print template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				debug($this->PrintTemplate->invalidFields());
				$this->Session->setFlash(__('The print template could not be saved. Please, try again.'));
			}
		}
	} 

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PrintTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid print template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PrintTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The print template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The print template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PrintTemplate.' . $this->PrintTemplate->primaryKey => $id));
			$this->request->data = $this->PrintTemplate->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PrintTemplate->id = $id;
		if (!$this->PrintTemplate->exists()) {
			throw new NotFoundException(__('Invalid print template'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PrintTemplate->delete()) {
			$this->Session->setFlash(__('The print template has been deleted.'));
		} else {
			$this->Session->setFlash(__('The print template could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
