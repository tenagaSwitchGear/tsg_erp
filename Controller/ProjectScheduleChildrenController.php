<?php
App::uses('AppController', 'Controller');
/**
 * ProjectScheduleChildren Controller
 *
 * @property ProjectScheduleChild $ProjectScheduleChild
 * @property PaginatorComponent $Paginator
 */
class ProjectScheduleChildrenController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectScheduleChild->recursive = 0;
		$this->set('projectScheduleChildren', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectScheduleChild->exists($id)) {
			throw new NotFoundException(__('Invalid project schedule child'));
		}
		$options = array('conditions' => array('ProjectScheduleChild.' . $this->ProjectScheduleChild->primaryKey => $id));
		$this->set('projectScheduleChild', $this->ProjectScheduleChild->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectScheduleChild->create();
			if ($this->ProjectScheduleChild->save($this->request->data)) {
				$this->Session->setFlash(__('The project schedule child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project schedule child could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectScheduleChild->exists($id)) {
			throw new NotFoundException(__('Invalid project schedule child'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectScheduleChild->save($this->request->data)) {
				$this->Session->setFlash(__('The project schedule child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project schedule child could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectScheduleChild.' . $this->ProjectScheduleChild->primaryKey => $id));
			$this->request->data = $this->ProjectScheduleChild->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectScheduleChild->id = $id;
		if (!$this->ProjectScheduleChild->exists()) {
			throw new NotFoundException(__('Invalid project schedule child'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectScheduleChild->delete()) {
			$this->Session->setFlash(__('The project schedule child has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project schedule child could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
