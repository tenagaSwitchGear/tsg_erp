<?php
App::uses('AppController', 'Controller');
/**
 * InventoryStores Controller
 *
 * @property InventoryStore $InventoryStore
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryStoresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryStore->recursive = 0;
		$this->set('inventoryStores', $this->Paginator->paginate());

		
	}

	public function ajaxfindstore() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			
			$conditions = array(); 
			$conditions['OR']['Product.name LIKE'] = '%'.$term.'%';
			$conditions['OR']['Product.code LIKE'] = '%'.$term.'%'; 

			$items = $this->Product->find('all', array(
				'conditions' => $conditions,
				'group' => array('Product.id')
				));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['Product']['id'], 
					'name' => $item['Product']['name'],
					'code' => $item['Product']['code'],
					'price' => $item['Product']['price'],
					'type' => $item['Product']['is_bom'] == 1 ? 'Finished Goods' : 'Raw Material',
					'bom_id' => $item['Product']['is_bom'] == 1 ? $this->find_bom($item['Product']['id']) : 0
					); 
			}
			echo json_encode($json);	
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryStore->exists($id)) {
			throw new NotFoundException(__('Invalid inventory store'));
		}
		$options = array('conditions' => array('InventoryStore.' . $this->InventoryStore->primaryKey => $id));
		$this->set('inventoryStore', $this->InventoryStore->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryStore->create();
			if ($this->InventoryStore->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory store has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory store could not be saved. Please, try again.'));
			}
		}
		$inventoryLocations = $this->InventoryStore->InventoryLocation->find('list');
		$this->set(compact('inventoryLocations'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryStore->exists($id)) {
			throw new NotFoundException(__('Invalid inventory store'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryStore->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory store has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory store could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryStore.' . $this->InventoryStore->primaryKey => $id));
			$this->request->data = $this->InventoryStore->find('first', $options);
		}
		$inventoryLocations = $this->InventoryStore->InventoryLocation->find('list');
		$this->set(compact('inventoryLocations'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryStore->id = $id;
		if (!$this->InventoryStore->exists()) {
			throw new NotFoundException(__('Invalid inventory store'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryStore->delete()) {
			$this->Session->setFlash(__('The inventory store has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory store could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
