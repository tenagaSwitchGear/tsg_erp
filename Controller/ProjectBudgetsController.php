<?php
App::uses('AppController', 'Controller');
/**
 * ProjectBudgets Controller
 *
 * @property ProjectBudget $ProjectBudget
 * @property PaginatorComponent $Paginator
 */
class ProjectBudgetsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function ajaxfindbudgetbyid() {
		$this->autoRender = false;  
		if(isset($this->request->query['data'])) {
			$data = str_replace('budget-', '', $this->request->query['data']); 
			$id = explode('-', $data);
			$budgets = $this->ProjectBudget->find('all', array(
				'fields' => array(
					'SUM(ProjectBudget.balance) AS balance'
					),
				'conditions' => array(
					'ProjectBudget.sale_job_child_id' => $id[0]
					)
				));
			$json = array();
			foreach ($budgets as $budget) {
				$json['total'] = $budget[0]['balance'];
				$json['id'] =  $id[0] . '-' . $id[1];
			}
			echo json_encode($json);
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array(
			'conditions' => array('ProjectBudget.type' => 'Job Budget'), 
			'limit'=>$record_per_page,
			'order' => 'ProjectBudget.id DESC'
			); 
		$this->set('projectBudgets', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectBudget->exists($id)) {
			throw new NotFoundException(__('Invalid project budget'));
		}
		$options = array('conditions' => array('ProjectBudget.' . $this->ProjectBudget->primaryKey => $id));
		$project = $this->ProjectBudget->find('first', $options);
		$this->set('projectBudget', $project);

		$this->loadModel('AccountDepartmentBudget');

		$accounts = $this->AccountDepartmentBudget->find('all', array(
			'conditions' => array(
				'AccountDepartmentBudget.sale_job_child_id' => $project['ProjectBudget']['sale_job_child_id']
				)
			)); 
		$this->set('accounts', $accounts);

		$this->set('issue_cost', $this->get_issued_cost($project['ProjectBudget']['sale_job_child_id']));
	}

	private function get_issued_cost($sale_job_child_id) {
		$this->loadModel('InventoryItemIssueCost');
		$sums = $this->InventoryItemIssueCost->find('all', array(
			'fields' => array(
				'SUM(InventoryItemIssueCost.amount) AS total'
				),
			'conditions' => array(
				'InventoryItemIssueCost.sale_job_child_id' => $sale_job_child_id
				)
			));
		foreach ($sums as $sum) {
			if($sum[0]['total'] == null) {
				return 0;
			} else {
				return $sum[0]['total'];
			}
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectBudget->create();
			if ($this->ProjectBudget->save($this->request->data)) {
				$this->Session->setFlash(__('The project budget has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project budget could not be saved. Please, try again.'));
			}
		}
		$saleJobs = $this->ProjectBudget->SaleJob->find('list');
		$saleJobChildren = $this->ProjectBudget->SaleJobChild->find('list');
		$users = $this->ProjectBudget->User->find('list');
		$this->set(compact('saleJobs', 'saleJobChildren', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectBudget->exists($id)) {
			throw new NotFoundException(__('Invalid project budget'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectBudget->save($this->request->data)) {
				$this->Session->setFlash(__('The project budget has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project budget could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectBudget.' . $this->ProjectBudget->primaryKey => $id));
			$this->request->data = $this->ProjectBudget->find('first', $options);
		}
		$saleJobs = $this->ProjectBudget->SaleJob->find('list');
		$saleJobChildren = $this->ProjectBudget->SaleJobChild->find('list');
		$users = $this->ProjectBudget->User->find('list');
		$this->set(compact('saleJobs', 'saleJobChildren', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectBudget->id = $id;
		if (!$this->ProjectBudget->exists()) {
			throw new NotFoundException(__('Invalid project budget'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectBudget->delete()) {
			$this->Session->setFlash(__('The project budget has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project budget could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
