<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodDeliveryItems Controller
 *
 * @property FinishedGoodDeliveryItem $FinishedGoodDeliveryItem
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FinishedGoodDeliveryItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodDeliveryItem->recursive = 0;
		$this->set('finishedGoodDeliveryItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodDeliveryItem->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery item'));
		}
		$options = array('conditions' => array('FinishedGoodDeliveryItem.' . $this->FinishedGoodDeliveryItem->primaryKey => $id));
		$this->set('finishedGoodDeliveryItem', $this->FinishedGoodDeliveryItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodDeliveryItem->create();
			if ($this->FinishedGoodDeliveryItem->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery item could not be saved. Please, try again.'));
			}
		}
		$finishedGoodDeliveries = $this->FinishedGoodDeliveryItem->FinishedGoodDelivery->find('list');
		$finishedGoods = $this->FinishedGoodDeliveryItem->FinishedGood->find('list');
		$saleOrderItems = $this->FinishedGoodDeliveryItem->SaleOrderItem->find('list');
		$saleQuotationItems = $this->FinishedGoodDeliveryItem->SaleQuotationItem->find('list');
		$inventoryItems = $this->FinishedGoodDeliveryItem->InventoryItem->find('list');
		$users = $this->FinishedGoodDeliveryItem->User->find('list');
		$this->set(compact('finishedGoodDeliveries', 'finishedGoods', 'saleOrderItems', 'saleQuotationItems', 'inventoryItems', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodDeliveryItem->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodDeliveryItem->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodDeliveryItem.' . $this->FinishedGoodDeliveryItem->primaryKey => $id));
			$this->request->data = $this->FinishedGoodDeliveryItem->find('first', $options);
		}
		$finishedGoodDeliveries = $this->FinishedGoodDeliveryItem->FinishedGoodDelivery->find('list');
		$finishedGoods = $this->FinishedGoodDeliveryItem->FinishedGood->find('list');
		$saleOrderItems = $this->FinishedGoodDeliveryItem->SaleOrderItem->find('list');
		$saleQuotationItems = $this->FinishedGoodDeliveryItem->SaleQuotationItem->find('list');
		$inventoryItems = $this->FinishedGoodDeliveryItem->InventoryItem->find('list');
		$users = $this->FinishedGoodDeliveryItem->User->find('list');
		$this->set(compact('finishedGoodDeliveries', 'finishedGoods', 'saleOrderItems', 'saleQuotationItems', 'inventoryItems', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodDeliveryItem->id = $id;
		if (!$this->FinishedGoodDeliveryItem->exists()) {
			throw new NotFoundException(__('Invalid finished good delivery item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodDeliveryItem->delete()) {
			$this->Session->setFlash(__('The finished good delivery item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good delivery item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FinishedGoodDeliveryItem->recursive = 0;
		$this->set('finishedGoodDeliveryItems', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FinishedGoodDeliveryItem->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery item'));
		}
		$options = array('conditions' => array('FinishedGoodDeliveryItem.' . $this->FinishedGoodDeliveryItem->primaryKey => $id));
		$this->set('finishedGoodDeliveryItem', $this->FinishedGoodDeliveryItem->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodDeliveryItem->create();
			if ($this->FinishedGoodDeliveryItem->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery item could not be saved. Please, try again.'));
			}
		}
		$finishedGoodDeliveries = $this->FinishedGoodDeliveryItem->FinishedGoodDelivery->find('list');
		$finishedGoods = $this->FinishedGoodDeliveryItem->FinishedGood->find('list');
		$saleOrderItems = $this->FinishedGoodDeliveryItem->SaleOrderItem->find('list');
		$saleQuotationItems = $this->FinishedGoodDeliveryItem->SaleQuotationItem->find('list');
		$inventoryItems = $this->FinishedGoodDeliveryItem->InventoryItem->find('list');
		$users = $this->FinishedGoodDeliveryItem->User->find('list');
		$this->set(compact('finishedGoodDeliveries', 'finishedGoods', 'saleOrderItems', 'saleQuotationItems', 'inventoryItems', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FinishedGoodDeliveryItem->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodDeliveryItem->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodDeliveryItem.' . $this->FinishedGoodDeliveryItem->primaryKey => $id));
			$this->request->data = $this->FinishedGoodDeliveryItem->find('first', $options);
		}
		$finishedGoodDeliveries = $this->FinishedGoodDeliveryItem->FinishedGoodDelivery->find('list');
		$finishedGoods = $this->FinishedGoodDeliveryItem->FinishedGood->find('list');
		$saleOrderItems = $this->FinishedGoodDeliveryItem->SaleOrderItem->find('list');
		$saleQuotationItems = $this->FinishedGoodDeliveryItem->SaleQuotationItem->find('list');
		$inventoryItems = $this->FinishedGoodDeliveryItem->InventoryItem->find('list');
		$users = $this->FinishedGoodDeliveryItem->User->find('list');
		$this->set(compact('finishedGoodDeliveries', 'finishedGoods', 'saleOrderItems', 'saleQuotationItems', 'inventoryItems', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FinishedGoodDeliveryItem->id = $id;
		if (!$this->FinishedGoodDeliveryItem->exists()) {
			throw new NotFoundException(__('Invalid finished good delivery item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodDeliveryItem->delete()) {
			$this->Session->setFlash(__('The finished good delivery item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good delivery item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
