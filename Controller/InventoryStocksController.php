<?php
App::uses('AppController', 'Controller');
/**
 * InventoryStocks Controller
 *
 * @property InventoryStock $InventoryStock
 * @property PaginatorComponent $Paginator
 */
class InventoryStocksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

    public function ajax_save_stock_take() {
    	$this->autoRender = false;
    }

	public function import() { 
		$this->autoRender = false;
		$this->loadModel('InventoryStock');
		$stocks = $this->InventoryStock->find('all', array('recursive' => -1));
 		
 		$this->loadModel('InventoryMaterialHistory');

		foreach ($stocks as $stock) {
			// Update balance
			$in = $stock['InventoryStock']['quantity'] + $stock['InventoryStock']['issued_quantity'];
			/*$update = $this->InventoryStock->updateAll(
				array( 
					'InventoryStock.qty_in' => "'".  $in ."'" 
					),
				array(
					'InventoryStock.id' => $stock['InventoryStock']['id'] 
					)
				); 
			if(!$update) {
				die('Error');
			}*/
			$this->InventoryMaterialHistory->create();
			$ins = array(
				'reference' => 'Stock Entry',
				'user_id' => 0,
				'store_pic' => 36,
				'inventory_item_id' => $stock['InventoryStock']['inventory_item_id'],
				'inventory_stock_id' => $stock['InventoryStock']['id'],
				'quantity' => $stock['InventoryStock']['qty_in'],
				'issued_quantity' => $stock['InventoryStock']['qty_in'],
				'general_unit_id' => $stock['InventoryStock']['general_unit_id'],
				'quantity_balance' => $this->find_before_id($stock['InventoryStock']['id'], $stock['InventoryStock']['inventory_item_id']) + $stock['InventoryStock']['qty_in'],
				'inventory_material_request_id' => 0,
				'inventory_material_request_item_id' => 0,
				'created' => $stock['InventoryStock']['created'],
				'status' => 1,
				'type' => 7,
				'transfer_type' => 'In'
				);
			//$this->InventoryMaterialHistory->save($ins); 
		}

		$this->set('stocks', $stocks);
	}

	private function find_before_id($id, $item_id) {
		$stocks = $this->InventoryStock->find('all', array(
			'fields' => array(
				'SUM(InventoryStock.qty_in) AS balance'
				),
			'recursive' => -1,
			'conditions' => array(
				'InventoryStock.id <' => $id,
				'InventoryStock.inventory_item_id' => $item_id
				)
			));
		if($stocks) {
			foreach ($stocks as $stock) {
				return $stock[0]['balance'];
			}	
		} else {
			return 0;
		}
		
	}

	private function find_similar_category($code) {
		$exp = explode('.', $code);
		$this->loadModel('InventoryItem');

		$conditions['InventoryItem.code LIKE'] = '%' . $exp[0] . '%'; 

		$item = $this->InventoryItem->find('first', array(
			'conditions' => $conditions
			));

		if($item) {
			return $item['InventoryItem']['inventory_item_category_id'];
		} else {
			return 0;
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index($balance = null) {

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
			}

			if($_GET['code'] != '') {
				$conditions['InventoryItem.code LIKE'] = $_GET['code'];
			}

			if($_GET['inventory_item_category_id'] != '') {
				$conditions['InventoryItem.inventory_item_category_id'] = (int)$_GET['inventory_item_category_id'];
			} 

			if($_GET['inventory_location_id'] != '') {
				$conditions['InventoryStock.inventory_location_id'] = (int)$_GET['inventory_location_id'];
			} 

			if($_GET['inventory_store_id'] != '') {
				$conditions['InventoryStock.inventory_store_id'] = (int)$_GET['inventory_store_id'];
			}  
			$this->request->data['InventoryStock'] = $_GET;
		} 

		$conditions['InventoryItem.status'] = array(0, 1); 

		if($balance == null) {
			$conditions['InventoryStock.quantity >'] = 0; 
		} elseif($balance == 1) {
			$conditions['InventoryStock.quantity'] = 0; 
		}

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order'=>'InventoryItem.id DESC',
			'limit'=>$record_per_page
			); 
		$stocks = $this->Paginator->paginate();

		$this->loadModel('InventoryItem');
		foreach ($stocks as $stock) {
			/*$this->InventoryItem->updateAll(
				array(
					'InventoryItem.quantity' => "'" . $stock['InventoryStock']['quantity'] . "'",
					'InventoryItem.quantity_reserved' => "'" . 0 . "'",
					'InventoryItem.quantity_available' => "'" . $stock['InventoryStock']['quantity'] . "'",
					'InventoryItem.quantity_shortage' => "'" . 0 . "'",
					'InventoryItem.rack' => "'" . $stock['InventoryStock']['rack'] . "'",
					'InventoryItem.inventory_location_id' => "'" . $stock['InventoryStock']['inventory_location_id'] . "'",
					'InventoryItem.inventory_store_id' => "'" . $stock['InventoryStock']['inventory_store_id'] . "'"
					),
				array('InventoryItem.id' => $stock['InventoryStock']['inventory_item_id'])
				);*/
		}
		$this->set('inventoryStocks', $stocks);

		$this->loadModel('InventoryItemCategory');
		$categories = $this->InventoryItemCategory->find('list');
		$locations = $this->InventoryStock->InventoryLocation->find('list');
		$stores = $this->InventoryStock->InventoryStore->find('list');

		$this->set(compact('categories', 'locations', 'stores'));
	}

	public function st_list(){
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');

		$st = $this->StockTakes->find('all', array(
			'recursive' => -1
		));
		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}
		$this->set('period', $p);

		$this->set(compact('st'));
	}

	public function stock_take_verify(){
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');

		$st = $this->StockTakes->find('all', array(
			'recursive' => -1
		));

		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}
		$this->set('period', $p);

		$this->set(compact('st'));
	}

	public function print_st($id = null) {
		$this->layout = 'print'; 
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');
		$this->loadModel('StockTakeReasons');

		$st = $this->StockTakes->find('first', array(
			'conditions' => array(
				'StockTakes.id' => $id
			)
		)); 

		$this->Paginator->settings = array(
			'conditions' => array(
				'StockTakeItems.stock_take_id' => $id
				),
			'order' => 'InventoryItem.code ASC', 
			'limit' => 2000
			);  
		$items = $this->Paginator->paginate('StockTakeItems');

		$arr_item = array();
		foreach ($items as $item) {
			$arr_item[] =  array(
				'StockTakeItems' => $item['StockTakeItems'],
				'StockTakeReasons' => $item['StockTakeReasons'],
				'InventoryItem' => $item['InventoryItem'], 
				'InventoryRack' => $item['InventoryRack'],
			); 
		}

		$stock_take = array(
			'StockTakes' => $st['StockTakes'],
			'InventoryLocations' => $st['InventoryLocations'],
			'InventoryStores' => $st['InventoryStores'],
			'StockTakeItems' => $arr_item,
		);

		$reasons = $this->StockTakeReasons->find('all');
		//print_r($reason);
		//exit;
		$arr = array();

		foreach ($reasons as $reason) {
			$arr[] = array(
				'id' => $reason['StockTakeReasons']['id'],
				'notes' => $reason['StockTakeReasons']['notes'],
			);
		}

		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}
		$this->set('period', $p);


		$this->set('st', $stock_take);
		$this->set('reasons', $arr);
	}

	public function view_st($id = null) {
		

		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');
		$this->loadModel('StockTakeReasons');

		$st = $this->StockTakes->find('first', array(
			'conditions' => array(
				'StockTakes.id' => $id
			)
		));

		/*$items = $this->StockTakeItems->find('all', array(
			'conditions' => array(
				'StockTakeItems.stock_take_id' => $id
				),
			'order' => 'InventoryItem.code ASC'
			));*/
		$this->Paginator->settings = array(
			'conditions' => array(
				'StockTakeItems.stock_take_id' => $id
				),
			'order' => 'InventoryItem.code ASC', 
			'limit' => 100
			);  
		$items = $this->Paginator->paginate('StockTakeItems');

		$arr_item = array();
		foreach ($items as $item) {
			$arr_item[] =  array(
				'StockTakeItems' => $item['StockTakeItems'],
				'StockTakeReasons' => $item['StockTakeReasons'],
				'InventoryItem' => $item['InventoryItem'],
				'InventoryStock' => $this->get_rack_by_id($item['InventoryItem']['id']),
			); 
		}

		$stock_take = array(
			'StockTakes' => $st['StockTakes'],
			'InventoryLocations' => $st['InventoryLocations'],
			'InventoryStores' => $st['InventoryStores'],
			'StockTakeItems' => $arr_item 
		);

		$reasons = $this->StockTakeReasons->find('all');
		//print_r($reason);
		//exit;
		$arr = array();

		foreach ($reasons as $reason) {
			$arr[] = array(
				'id' => $reason['StockTakeReasons']['id'],
				'notes' => $reason['StockTakeReasons']['notes'],
			);
		}

		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}
		$this->set('period', $p);


		$this->set('st', $stock_take);
		$this->set('reasons', $arr);
	}

	public function ajax_stock_take_verification($id = null) {
		$this->autoRender = false;
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');
		$this->loadModel('StockTakeReasons');

		$st = $this->StockTakes->find('first', array(
			'conditions' => array(
				'StockTakes.id' => $id
			)
		));

		/*
		$items = $this->StockTakeItems->find('all', array(
			'conditions' => array(
				'StockTakeItems.stock_take_id' => $id
				)
			));
		*/
		if(isset($_GET['page'])) {
			$draw = (int)$_GET['page'];
		} else {
			$draw = 0;
		}
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => array(
				'StockTakeItems.stock_take_id' => $id
				),
			'order' => 'StockTakeItems.id DESC' 
			); 
		$items = $this->Paginator->paginate('StockTakeItems');
 
		$arr_item = array(); 
		$arr_item['draw'] = 1;
		$arr_item['recordsTotal'] = count($items);
		$arr_item['recordsFiltered'] = count($items);
		foreach ($items as $item) {
			 
				/*$arr_item[] =  array(
					'StockTakeItems' => $item,
					'StockTakeReasons' => $this->get_reason($item['stock_take_reason_id']),
					'InventoryItem' => $this->get_item($item['inventory_item_id']),
				
				);*/
				$rack = $this->get_rack_by_id($item['InventoryItem']['id']);
				$arr_item['data'][] = array(
					$item['InventoryItem']['name'],
					$item['InventoryItem']['code'],
					$item['StockTakeItems']['quantity_onhold'],
					$item['StockTakeItems']['quantity_actual'],
					$item['StockTakeItems']['quantity_variant'],
					$rack['InventoryStock']['rack']
					);
			  
		}

		$stock_take = array(
			'StockTakes' => $st['StockTakes'],
			'InventoryLocations' => $st['InventoryLocations'],
			'InventoryStores' => $st['InventoryStores'],
			'StockTakeItems' => $arr_item,
		);

		$reasons = $this->StockTakeReasons->find('all');
		//print_r($reason);
		//exit;
		$arr = array();

		foreach ($reasons as $reason) {
			$arr[] = array(
				'id' => $reason['StockTakeReasons']['id'],
				'notes' => $reason['StockTakeReasons']['notes'],
			);
		}
		
		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}
		$this->set('period', $p);

		$this->set('st', $stock_take);
		$this->set('reasons', $arr);
		 
		echo json_encode($arr_item);
	}

	public function stock_take_verification($id = null) {
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');
		$this->loadModel('StockTakeReasons');

		$st = $this->StockTakes->find('first', array(
			'conditions' => array(
				'StockTakes.id' => $id
			)
		));

		if($this->request->is(array('put', 'post'))) {
			echo '<pre>';
			var_dump($this->request->data);
		}

		$arr_item = array();
		foreach ($st['StockTakeItems'] as $item) {
			if($item['status'] == 1){
				$arr_item[] =  array(
					'StockTakeItems' => $item,
					'StockTakeReasons' => $this->get_reason($item['stock_take_reason_id']),
					'InventoryItem' => $this->get_item($item['inventory_item_id']),
				
				);
			} 
		}

		$stock_take = array(
			'StockTakes' => $st['StockTakes'],
			'InventoryLocations' => $st['InventoryLocations'],
			'InventoryStores' => $st['InventoryStores'],
			'StockTakeItems' => $arr_item,
		);

		$reasons = $this->StockTakeReasons->find('all');
		//print_r($reason);
		//exit;
		$arr = array();

		foreach ($reasons as $reason) {
			$arr[] = array(
				'id' => $reason['StockTakeReasons']['id'],
				'notes' => $reason['StockTakeReasons']['notes'],
			);
		}
		
		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}
		$this->set('period', $p);

		$this->set('st', $stock_take);
		$this->set('reasons', $arr);
	}

	// Update verification
	private function update_st_verify($stock_take_id, $st_item_id, $item_id, $remark, $status, $quantity_actual, $quantity_variant) {
	 
		$this->loadModel('StockTakeItems'); 

		$data_stock_take_item = array(
			'id' => $st_item_id,
			'status' => $status,
			'approval_remark' => $remark
		); 

		if($this->StockTakeItems->save($data_stock_take_item)) { 
			$this->loadModel('StockTakes');

			$stock_take = $this->StockTakes->find('first', array(
				'conditions' => array(
					'StockTakes.id' => $stock_take_id
					)
				));
			$this->loadModel('InventoryItem');
			$item = $this->InventoryItem->find('first', array(
				'conditions' => array(
					'InventoryItem.id' => $item_id
					)
				));

			$stock = $this->InventoryStock->find('first', array(
				'conditions' => array(
					'InventoryStock.inventory_item_id' => $item_id
					),
				'order' => 'InventoryStock.id DESC'
				));
			$this->loadModel('InventoryMaterialHistory'); 
			// Check varian <0 or >
			if($quantity_variant < 0) {
				$abs = abs($quantity_variant);

				// Deduct stock
				$this->update_stock($item_id, $abs);

				$history = array(
					'reference' => 'Stock Take ' . $stock_take['StockTakes']['code'],
					'user_id' => 0, 
					'store_pic' => $this->user_id,
					'inventory_item_id' => $item_id, 
					'inventory_stock_id' => 0,
					'quantity' => $abs, // Req qty
					'general_unit_id' => $stock['InventoryStock']['general_unit_id'], 
					'issued_quantity' => $abs,
					'quantity_balance' => $this->sum_stock_balance($item_id),
					'inventory_material_request_id' => 0,
					'inventory_material_request_item_id' => 0,
					'created' => $this->date,
					'status' => 1,
					'type' => 9, 
					'transfer_type' => 'Out'
					);
				if(!$this->InventoryMaterialHistory->save($history)) {
					return 4; 
				} else {
					return true;
				}

			} else {
				// Add stock
				$data = array(
					'general_unit_id' => $stock['InventoryStock']['general_unit_id'], 
					'inventory_item_id' => $stock['InventoryStock']['inventory_item_id'], 
					'inventory_delivery_order_item_id' => 0, 
					'general_unit_converter_id' => 0, 
					'price_per_unit' => 0, 
					'qty_in' => $quantity_variant, 
					'quantity' => $quantity_variant, 
					'issued_quantity' => 0, 
					'qc_status' => 0, 
					'receive_date' => date('Y-m-d'),  
					'inventory_location_id' => $stock['InventoryStock']['inventory_location_id'], 
					'inventory_store_id' => $stock['InventoryStock']['inventory_store_id'], 
					'inventory_rack_id' => $stock['InventoryStock']['inventory_rack_id'], 
					'inventory_delivery_location_id' => 0, 
					'type' => 3, // 0 manual add, 1 grn, 2 stock take
					'finished_good_id' => 0, 
					'rack' => $stock['InventoryStock']['rack'], 
					'created' => $this->date, 
					'user_id' => $this->user_id, 
					'inventory_supplier_id' => 0, 
					'barcode' => 0
					);
				$insert_stock = $this->InventoryStock->save($data);
				if($insert_stock) {
					$stock_id = $this->InventoryStock->getLastInsertId();
					// Add history 
					// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 5: 
					// RMA, 6: grn, 7: First Keyin, 8 Stock take
					$history = array(
						'reference' => 'Stock Take ' . $stock_take['StockTakes']['code'],
						'user_id' => 0, 
						'store_pic' => $this->user_id,
						'inventory_item_id' => $item_id, 
						'inventory_stock_id' => $stock_id,
						'quantity' => $quantity_variant, // Req qty
						'general_unit_id' => $stock['InventoryStock']['general_unit_id'], 
						'issued_quantity' => $quantity_variant,
						'quantity_balance' => $this->sum_stock_balance($item_id),
						'inventory_material_request_id' => 0,
						'inventory_material_request_item_id' => 0,
						'created' => $this->date,
						'status' => 1,
						'type' => 8, 
						'transfer_type' => 'In'
						);
					if($this->InventoryMaterialHistory->save($history)) {
						return true;
					} else {
						return 1;
					}
				} else {
					return 2;
				} 
			}  
		}  else {
			return 3;
		} 
		return 67; 
	}

	public function view_st_verify($id = null) {
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');
		$this->loadModel('StockTakeReasons');

		$st = $this->StockTakes->find('first', array(
			'conditions' => array(
				'StockTakes.id' => $id
			)
		));

		if($this->request->is(array('put', 'post'))) {
			//echo '<pre>';
			//print_r($this->request->data['InventoryStock']);

			// Single
			$stock_take_id = $id; 
			$status = $this->request->data['InventoryStock']['status'];
			// Multi
			$stock_take_item_id = $this->request->data['InventoryStock']['stock_take_item_id'];
			$quantity_actual = $this->request->data['InventoryStock']['quantity_actual'];
			$quantity_variant = $this->request->data['InventoryStock']['quantity_variant'];
			$item_id = $this->request->data['InventoryStock']['inventory_item_id']; 
			$remark = $this->request->data['InventoryStock']['approval_remark'];
			$include = $this->request->data['InventoryStock']['include'];
			$count = count($stock_take_item_id);


			for($i = 0; $i < $count; $i++) {
				// If selected
				if($include[$i] == 1) {
					$update = $this->update_st_verify($id, $stock_take_item_id[$i], $item_id[$i], $remark[$i], $status, $quantity_actual[$i], $quantity_variant[$i]);
					var_dump($update);
				}
			} 
			//$this->Session->setFlash(__('Stock take status has been saved'), 'success');
			//return $this->redirect(array('action' => 'view_st_verify', $id)); 
		} 

		$arr_item = array();
		foreach ($st['StockTakeItems'] as $item) {
			if($item['status'] == 1){
				$arr_item[] =  array(
					'StockTakeItems' => $item,
					'StockTakeReasons' => $this->get_reason($item['stock_take_reason_id']),
					'InventoryItem' => $this->get_item($item['inventory_item_id']),
				
				);
			} 
		}

		$stock_take = array(
			'StockTakes' => $st['StockTakes'],
			'InventoryLocations' => $st['InventoryLocations'],
			'InventoryStores' => $st['InventoryStores'],
			'StockTakeItems' => $arr_item,
		);

		$reasons = $this->StockTakeReasons->find('all');
		//print_r($reason);
		//exit;
		$arr = array();

		foreach ($reasons as $reason) {
			$arr[] = array(
				'id' => $reason['StockTakeReasons']['id'],
				'notes' => $reason['StockTakeReasons']['notes'],
			);
		}
		
		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}
		$this->set('period', $p);

		$this->set('st', $stock_take);
		$this->set('reasons', $arr);
	}

	public function start_stock_take(){
		$this->loadModel('StockTakePeriods');
		$this->StockTakePeriods->create();

		$data_period = array(
			'date_start' => date('Y-m-d H:i:s'),
			'status' => 0
		);

		if($this->StockTakePeriods->save($data_period)){
			$this->Session->setFlash(__('The Stock Take Period have been started.'), 'success');
			return $this->redirect(array('action' => 'stock_take'));
		}else{
			$this->Session->setFlash(__('The item could not be saved.'), 'error');
		}
	}

	public function stop_stock_take(){
		$this->loadModel('StockTakePeriods');
		$this->StockTakePeriods->create();

		$st = $this->StockTakePeriods->find('first', array(
			'conditions' => array(
				'StockTakePeriods.date_start' => $_GET['startdate'], 
			)
		));

		$id = $st['StockTakePeriods']['id'];
		
		$data_period = array(
			'id' => $id,
			'date_start' => $st['StockTakePeriods']['date_start'],
			'date_end' => date('Y-m-d H:i:s'),
			'status' => 1
		);

		if($this->StockTakePeriods->save($data_period)){
			$this->Session->setFlash(__('The Stock Take Period have been ended.'), 'success');
			return $this->redirect(array('action' => 'stock_take'));
		}else{
			$this->Session->setFlash(__('The item could not be saved.'), 'error');
		}
	}

	

	private function add_history($item_id) {
		$stock_id = $this->InventoryStock->getLastInsertId(); 
		$this->loadModel('InventoryMaterialHistory'); 
		$this->InventoryMaterialHistory->create();
		$history = array(
			'reference' => 'Stock Entry',
			'user_id' => 0, 
			'store_pic' => $this->user_id,
			'inventory_item_id' => $this->request->data['InventoryStock']['inventory_item_id'], 
			'inventory_stock_id' => $stock_id,
			'quantity' => $this->request->data['InventoryStock']['quantity'], // Req qty
			'general_unit_id' => $this->request->data['InventoryStock']['general_unit_id'], 
			'issued_quantity' => $this->request->data['InventoryStock']['quantity'],
			'quantity_balance' => $this->sum_stock_balance($this->request->data['InventoryStock']['inventory_item_id']),
			'inventory_material_request_id' => 0,
			'inventory_material_request_item_id' => 0,
			'created' => $this->date,
			'status' => 1,
			'type' => 7, // 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 5: RMA, 6: grn, 7: First Keyin
			'transfer_type' => 'In'
			);
		$save_history = $this->InventoryMaterialHistory->save($history);
		if(!$save_history) {
			die('830');
		}
	}

	private function add_stock($item_id, $quantity) { 

	}

	private function update_stock($item_id, $quantity) {
		//Input variables 
	    //Output variables
	    if($quantity > 0) {
	    	$totalcost = 0;
		    $qtyout = 0; 
		    
	 		$this->loadModel('InventoryStock'); 
	 		$stocks = $this->InventoryStock->find('all', array(
	 			'conditions' => array(
	 				'InventoryStock.inventory_item_id' => $item_id,
	 				'InventoryStock.quantity >' => 0
	 				),
	 			'order' => array(
	 				'InventoryStock.id' => 'ASC'
	 				)
	 			));
	 		foreach ($stocks as $stock) {
	 			if($quantity > 0) {
	                $batchout = 0;
	                $rem = max($stock['InventoryStock']['quantity'] - $quantity, 0);
	                if($rem == 0)
	                    $batchout = $stock['InventoryStock']['quantity']; //This means there are no items of this cost remaining
	                else
	                    $batchout = $quantity; //This means there are items remaining and therefore our next loop (within the while) will check for the next expensive item

	                $totalcost += ($batchout * $stock['InventoryStock']['price_per_unit']);
	                $quantity -= $batchout;
	                $qtyout += $batchout;
	                
	                //$mysqli->query($sql);
	                $balance = $stock['InventoryStock']['quantity'] - $batchout;
	                $issued_quantity = $stock['InventoryStock']['issued_quantity'] + $batchout;
	                $this->InventoryStock->updateAll(
	                	array(
	                		'InventoryStock.quantity' => "'$balance'",
	                		'InventoryStock.issued_quantity' => "'$issued_quantity'"
	                		),
	                	array(
	                		'InventoryStock.id' => $stock['InventoryStock']['id']
	                		)
	                	); 
	            }
	 		} 
	    } 
	}

	public function update_st(){
		$this->autoRender = false;
		$json = array();
		$json['status'] = false;
		if ($this->request->is('post')) {

			//print_r($_POST);
			//exit;
			if($this->request->data['InventoryStock']['type'] == 'new'){
				$this->loadModel('StockTakeItems');
				$this->StockTakeItems->create();

				if(!$this->request->data['InventoryStock']['stock_take_reason_id']){
					$stock_reason = '0';
				}else{
					$stock_reason = $this->request->data['InventoryStock']['stock_take_reason_id'];
				}

				$data_update_stock_take = array(
					'id' => $this->request->data['InventoryStock']['stock_take_item_id'],
					'quantity_actual' => $this->request->data['InventoryStock']['quantity_actual'],
					'quantity_variant' => $this->request->data['InventoryStock']['quantity_variant'],
					'stock_take_reason_id' => $stock_reason,
					'status' => 1
				);

				if($this->StockTakeItems->save($data_update_stock_take)){
					//$this->Session->setFlash(__('The item have been submit for verification.'), 'success');
					//return $this->redirect(array('action' => 'view_st', $this->request->data['InventoryStock']['stock_take_id']));
					$json['status'] = true;
				}else{
					//$this->Session->setFlash(__('The item could not be saved.'), 'error');
				}
			}else if($this->request->data['InventoryStock']['type'] == 'edit'){
				$this->loadModel('StockTakeItems');
				$this->StockTakeItems->create();

				if($this->request->data['InventoryStock']['stock_take_reason_id'] == ''){
					$stock_reason = '0';
				}else{
					$stock_reason = $this->request->data['InventoryStock']['stock_take_reason_id'];
				}

				$data_update_stock_take = array(
					'id' => $this->request->data['InventoryStock']['stock_take_item_id'],
					'quantity_actual' => $this->request->data['InventoryStock']['new_quantity_actual'],
					'quantity_variant' => $this->request->data['InventoryStock']['quantity_variant'],
					'stock_take_reason_id' => $stock_reason,
					'status' => 1
				);

				if($this->StockTakeItems->save($data_update_stock_take)){
					//$this->Session->setFlash(__('The item have been submit for verification.'), 'success');
					//return $this->redirect(array('action' => 'view_st', $this->request->data['InventoryStock']['stock_take_id']));
					$json['status'] = true;
				}else{
					//$this->Session->setFlash(__('The item could not be saved.'), 'error');
				}
			}
			$json['post'] = $_POST;
			echo json_encode($json);
		}

	}

	private function get_reason($id){
		$this->loadModel('StockTakeReasons');
		$reason = $this->StockTakeReasons->find('first', array(
			'conditions' => array(
				'StockTakeReasons.id' => $id 
			)
		));
		if(empty($reason)){
			return $reason;
		}else{
			return $reason['StockTakeReasons'];
		}
	}

	private function get_item($id){
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('first', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			),
			'recursive' => -1
		));

		return $item['InventoryItem'];
	}

	public function view_stock_list() {
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printing'; 

	    if(isset($_GET['search'])) {
			//print_r($_GET);
			//exit;
			if($_GET['location_id'] != '') {
				$conditions['InventoryStock.inventory_location_id'] = $_GET['location_id'];
			} 

			if($_GET['store_id'] != '') {
				$conditions['InventoryStock.inventory_store_id'] = $_GET['store_id'];
			}  

			$this->request->data['InventoryStock'] = $_GET;
		}else{
			$conditions = '';
		} 
		$record_per_page = Configure::read('Reading.nodes_per_page');
		
		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'group' => 'InventoryStock.inventory_item_id',
			'recursive' => -2,
			'order'=>'InventoryItem.name ASC',
			'limit'=>$record_per_page
			); 


		$stocks = $this->Paginator->paginate();
		$arr = array();
		foreach ($stocks as $stock) {
			$arr[] = array(
				'InventoryStock' => $stock['InventoryStock'],
				'Sum' => $this->get_sum_unit($stock['InventoryStock']['inventory_item_id']),
				'GeneralUnit' => $stock['GeneralUnit'],
				'InventoryItem' => $stock['InventoryItem'],
				'InventoryLocation' => $this->get_location($stock['InventoryStock']['inventory_location_id']),
				'InventoryStore' => $this->get_store($stock['InventoryStock']['inventory_store_id']),
				'InventoryRack' => $this->get_rack($stock['InventoryStock']['inventory_rack_id'])
			);
		}
		$locations = $this->InventoryStock->InventoryLocation->find('list');
		$stores = $this->InventoryStock->InventoryStore->find('list');

		$this->set(compact('locations'));
		
		$this->set('stocks', $arr);
		
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');

		$code = $this->StockTakes->find('first', array(
			'order' => 'StockTakes.id DESC'
		));

		if(empty($code)){
			$bil = '1';
		}else{
			$bil = $code['StockTakes']['id']+1;
		}

		$st_no = $this->generate_code('ST', $bil);
		
		$this->StockTakes->create();
		$data_st = array(
			'code' => $st_no,
			'created' => date('Y-m-d H:i:s'),
			'status' => 0,
			'inventory_location_id' => $arr[0]['InventoryLocation']['id'],
			'inventory_store_id' => $arr[0]['InventoryRack']['id'],
		);
		if($this->StockTakes->save($data_st)){
			$lastID = $this->StockTakes->getLastInsertID();
			foreach ($arr as $stock) {
				$this->StockTakeItems->create();
				$data_st_item = array(
					'stock_take_id' =>$lastID,
					'inventory_item_id' => $stock['InventoryItem']['id'],
					'quantity_onhold' => $stock['Sum']['total_quantity']
				);
				$this->StockTakeItems->save($data_st_item);
			}
			$this->Session->setFlash(__('The Stock Take has been created.'), 'success');
			return $this->redirect(array('action' => 'stock_take'));
		}else{
			$this->Session->setFlash(__('The Stock Take could not be created.'), 'error');
		}
	}

	public function view_stock_list_2() {
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printing'; 

	    if(isset($_GET['search'])) {
			//print_r($_GET);
			//exit;
			if($_GET['location_id'] != '') {
				$conditions['InventoryStock.inventory_location_id'] = $_GET['location_id'];
			} 

			if($_GET['store_id'] != '') {
				$conditions['InventoryStock.inventory_store_id'] = $_GET['store_id'];
			}  

			$this->request->data['InventoryStock'] = $_GET;
		}else{
			$conditions = '';
		} 
		//$record_per_page = Configure::read('Reading.nodes_per_page');
		
		$stocks = $this->InventoryStock->find('all', array(
			'conditions' => $conditions,
			'group' => 'InventoryStock.inventory_item_id',
			'recursive' => 0,
			'order'=>'InventoryItem.name ASC',
			//'limit'=>$record_per_page
			)); 


		//$stocks = $this->Paginator->paginate();
		//print_r($stocks);
		//exit;
		$arr = array();
		foreach ($stocks as $stock) {
			$arr[] = array(
				'InventoryStock' => $stock['InventoryStock'],
				'Sum' => $this->get_sum_unit($stock['InventoryStock']['inventory_item_id']),
				'GeneralUnit' => $stock['GeneralUnit'],
				'InventoryItem' => $stock['InventoryItem'],
				'InventoryLocation' => $this->get_location($stock['InventoryStock']['inventory_location_id']),
				'InventoryStore' => $this->get_store($stock['InventoryStock']['inventory_store_id']),
				'InventoryRack' => $this->get_rack($stock['InventoryStock']['inventory_rack_id'])
			);
		}
		//print_r($arr);
		//exit;
		$locations = $this->InventoryStock->InventoryLocation->find('list');
		$stores = $this->InventoryStock->InventoryStore->find('list');

		$this->set(compact('locations'));
		
		$this->set('stocks', $arr);
		
		$this->loadModel('StockTakes');
		$this->loadModel('StockTakeItems');
		$this->loadModel('StockTakeReasons');

		$code = $this->StockTakes->find('first', array(
			'order' => 'StockTakes.id DESC'
		));

		if(empty($code)){
			$bil = '1';
		}else{
			$bil = $code['StockTakes']['id']+1;
		}

		$st_no = $this->generate_code('ST', $bil);
		
		$this->StockTakes->create();
		$data_st = array(
			'code' => $st_no,
			'created' => date('Y-m-d H:i:s'),
			'status' => 0,
			'start_date' => $_GET['startdate'],
			'inventory_location_id' => $arr[0]['InventoryLocation']['id'],
			'inventory_store_id' => $arr[0]['InventoryStore']['id'],
		);
		if($this->StockTakes->save($data_st)){
			$lastID = $this->StockTakes->getLastInsertID();
			foreach ($arr as $stock) {
				$this->StockTakeItems->create();
				$data_st_item = array(
					'stock_take_id' =>$lastID,
					'inventory_item_id' => $stock['InventoryItem']['id'],
					'quantity_onhold' => $stock['Sum']['total_quantity']
				);
				$this->StockTakeItems->save($data_st_item);
			}
			$this->Session->setFlash(__('The Stock Take has been created.'), 'success');
			return $this->redirect(array('action' => 'stock_take'));
		}else{
			$this->Session->setFlash(__('The Stock Take could not be created.'), 'error');
		}
	}

	public function stock_take() {
		if(isset($_GET['search'])) {
			//print_r($_GET);
			//exit;
			if($_GET['location_id'] != '') {
				$conditions['InventoryStock.inventory_location_id'] = $_GET['location_id'];
			} 

			if($_GET['store_id'] != '') {
				$conditions['InventoryStock.inventory_store_id'] = $_GET['store_id'];
			}  

			$this->request->data['InventoryStock'] = $_GET;
		}else{
			$conditions = '';
		} 
		$record_per_page = Configure::read('Reading.nodes_per_page');
		
		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'group' => 'InventoryStock.inventory_item_id',
			'recursive' => -2,
			'order'=>'InventoryItem.name ASC',
			'limit'=>$record_per_page
			); 


		$stocks = $this->Paginator->paginate();
		$arr = array();
		foreach ($stocks as $stock) {
			$arr[] = array(
				'InventoryStock' => $stock['InventoryStock'],
				'Sum' => $this->get_sum_unit($stock['InventoryStock']['inventory_item_id']),
				'GeneralUnit' => $stock['GeneralUnit'],
				'InventoryItem' => $stock['InventoryItem'],
				'InventoryLocation' => $this->get_location($stock['InventoryStock']['inventory_location_id']),
				'InventoryStore' => $this->get_store($stock['InventoryStock']['inventory_store_id']),
				'InventoryRack' => $this->get_rack($stock['InventoryStock']['inventory_rack_id'])
			);
		}
		$locations = $this->InventoryStock->InventoryLocation->find('list');
		$stores = $this->InventoryStock->InventoryStore->find('list');

		$this->loadModel('StockTakePeriods');
		$period = $this->StockTakePeriods->find('first', array(
			'order' => 'StockTakePeriods.id DESC'
		));

		if(!$period){
			$tempoh = array(
				'StockTakePeriods' => array(
					'status' => 'NULL'
				)
			);
		}else{
			$tempoh = $period;
		}

		if($tempoh['StockTakePeriods']['status'] == '0'){
			$p = array('Period'=>'Ongoing', 'StartDate'=>$tempoh['StockTakePeriods']['date_start']);
		}else if($tempoh['StockTakePeriods']['status'] == '1'){
			$p = array('Period'=>'Closed');
		}else{
			$p = array('Period'=>'');
		}

		$this->set(compact('locations'));
		
		$this->set('stocks', $arr);

		$this->set('period', $p);
	}

	private function get_sum_unit($id){
		$sum = $this->InventoryStock->find('all', array(
			'fields' => array('SUM(InventoryStock.quantity) as total_quantity'),
			'conditions' => array('InventoryStock.inventory_item_id' => $id)

		));
		
		return $sum[0][0];
	}

	private function get_location($id){
		$this->loadModel('InventoryLocation');
		$location = $this->InventoryLocation->find('all', array(
			'conditions' => array('InventoryLocation.id' => $id)

		));
		
		if(empty($location)){
			return $location;
		}else{
			return $location[0]['InventoryLocation'];
		}
	}

	private function get_rack_by_id($id){
		$this->loadModel('InventoryStock');
		$rack = $this->InventoryStock->find('first', array(
			'conditions' => array('InventoryStock.inventory_item_id' => $id),
			'recursive' => 0 
		));
		
		return $rack;
	}

	private function get_rack($id){
		$this->loadModel('InventoryRack');
		$rack = $this->InventoryRack->find('first', array(
			'conditions' => array('InventoryRack.id' => $id) 
		));
		
		if(empty($rack)){
			return $rack;
		}else{
			return $rack['InventoryRack'];
		}
	}

	private function get_store($id){
		$this->loadModel('InventoryStore');
		$store = $this->InventoryStore->find('all', array(
			'conditions' => array('InventoryStore.id' => $id)

		));
		
		if(empty($store)){
			return $store;
		}else{
			return $store[0]['InventoryStore'];
		}
	}

	public function ajaxfindstore() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		$this->loadModel('InventoryStore');
		if(isset($this->request->query['location_id'])) {
			$location_id = $this->request->query['location_id'];
			$conditions = array();
			$conditions['InventoryStore.inventory_location_id'] = $location_id; 

			$stores = $this->InventoryStore->find('all', array('conditions' => $conditions, 'recursive' => -1));
			
			$json = array();
			foreach ($stores as $store) {
				$json[] = $store; 
			}
			echo json_encode($json);	
		}
	}

	public function aging() {

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
			}

			if($_GET['code'] != '') {
				$conditions['InventoryItem.code LIKE'] = $_GET['code'];
			}

			if($_GET['inventory_item_category_id'] != '') {
				$conditions['InventoryItem.inventory_item_category_id'] = (int)$_GET['inventory_item_category_id'];
			} 

			if($_GET['inventory_location_id'] != '') {
				$conditions['InventoryStock.inventory_location_id'] = (int)$_GET['inventory_location_id'];
			} 

			if($_GET['inventory_store_id'] != '') {
				$conditions['InventoryStock.inventory_store_id'] = (int)$_GET['inventory_store_id'];
			}  
			$this->request->data['InventoryStock'] = $_GET;
		} 

		$conditions['InventoryItem.status'] = array(0, 1); 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order'=>'InventoryItem.id DESC',  
			'limit'=>$record_per_page
			); 
		$stocks = $this->Paginator->paginate(); 
		$this->loadModel('InventoryItem');
		foreach ($stocks as $stock) {
			/*$this->InventoryItem->updateAll(
				array(
					'InventoryItem.quantity' => "'" . $stock['InventoryStock']['quantity'] . "'",
					'InventoryItem.quantity_reserved' => "'" . 0 . "'",
					'InventoryItem.quantity_available' => "'" . $stock['InventoryStock']['quantity'] . "'",
					'InventoryItem.quantity_shortage' => "'" . 0 . "'",
					'InventoryItem.rack' => "'" . $stock['InventoryStock']['rack'] . "'",
					'InventoryItem.inventory_location_id' => "'" . $stock['InventoryStock']['inventory_location_id'] . "'",
					'InventoryItem.inventory_store_id' => "'" . $stock['InventoryStock']['inventory_store_id'] . "'"
					),
				array('InventoryItem.id' => $stock['InventoryStock']['inventory_item_id'])
				);*/
		}
		$this->set('inventoryStocks', $stocks);

		$this->loadModel('InventoryItemCategory');
		$categories = $this->InventoryItemCategory->find('list');
		$locations = $this->InventoryStock->InventoryLocation->find('list');
		$stores = $this->InventoryStock->InventoryStore->find('list');

		$this->set(compact('categories', 'locations', 'stores')); 
		$this->set(compact('stocks')); 
	}

	public function transfer() {
		$this->InventoryStock->recursive = 0;
		$this->set('inventoryStocks', $this->Paginator->paginate());
	}

	public function stock_aging() {
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array( 
			'order' => 'InventoryStock.id ASC',
			'group' => 'InventoryStock.inventory_item_id',
			'limit' => 60,
			); 
		
		$inventory_stocks = $this->Paginator->paginate();
		$_30 = date("Y-m-d", strtotime("-30 days"));
		$_90 = date("Y-m-d", strtotime("-90 days"));
		$_180 = date("Y-m-d", strtotime("-180 days"));
		$_365 = date("Y-m-d", strtotime("-365 days"));
		//$_more = date("Y-m-d", strtotime("-365 days"));

		//var_dump($_30);
		//exit;

		$output = array();
		foreach ($inventory_stocks as $stock) {

			$output[] = array(
					'InventoryItem' => $stock['InventoryItem'],
					'InventoryStock' => $stock['InventoryStock'],
					'GeneralUnit' => $stock['GeneralUnit'],
					'sum_30' => $this->sum_stock($stock['InventoryStock']['inventory_item_id'], $_30, $this->date),
					'sum_90' => $this->sum_stock($stock['InventoryStock']['inventory_item_id'], $_90, $_30),
					'sum_180' => $this->sum_stock($stock['InventoryStock']['inventory_item_id'], $_180, $_90),
					'sum_365' => $this->sum_stock($stock['InventoryStock']['inventory_item_id'], $_365, $_180),
					'sum_more' => $this->sum_stock_2($stock['InventoryStock']['inventory_item_id'], $_365),
			);			
		}
		$this->set('output', $output);		
	}

	private function sum_stock($item_id, $from, $to) {
		$sums= $this->InventoryStock->find('all', array(
			'fields' => array(
				'SUM(InventoryStock.quantity) AS total', 
			),
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $item_id,
				'InventoryStock.receive_date >=' => $from,
				'InventoryStock.receive_date <=' => $to,
			)
		));
		foreach ($sums as $sum) {
			return $sum[0]['total'];
		}
	}

	private function sum_stock_2($item_id, $from) {
		$sums= $this->InventoryStock->find('all', array(
			'fields' => array(
				'SUM(InventoryStock.quantity) AS total', 
			),
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $item_id,
				'InventoryStock.receive_date <=' => $from
			)
		));
		foreach ($sums as $sum) {
			return $sum[0]['total'];
		}
	}

	public function exchange($id = null) {
		if (!$this->InventoryStock->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item'));
		}

		if ($this->request->is('post')) {	
			$id = $_POST['inventory_stocks_id'];
			$get_details_stock = $this->InventoryStock->query("SELECT * FROM inventory_stocks INNER JOIN inventory_items on inventory_stocks.inventory_item_id = inventory_items.id WHERE inventory_stocks.id='".$id."'");
			if($_POST['transfer_type']=='1'){
				$warehouse_id = $_POST['inventory_delivery_location_id'];
				$store_id = $_POST['inventory_store_id'];
				$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];
				$new_quantity = $_POST['quantity'];

				if($new_quantity < $quantity){
					$new_quantity_2 = $quantity - $new_quantity;
					$this->InventoryStock->create();
					$data_item = array(
						'id' 								=> $id,
						'quantity' 							=> $new_quantity_2,
						'inventory_delivery_location_id' 	=> $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id']
					);

					$this->InventoryStock->save($data_item);

					$this->InventoryStock->create();
					$data_item_2 = array(
						'general_unit_id' => $get_details_stock[0]['inventory_stocks']['general_unit_id'],
						'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
						'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
						'quantity' => $new_quantity,
						'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
						'created' => date('Y-m-d H:i:s'),
						'inventory_delivery_location_id' => $warehouse_id,
						'location_id' => $store_id
					);

					if($this->InventoryStock->save($data_item_2)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $new_quantity,
							'status'   => '1',
							'type'     => '1'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}else if($new_quantity == $quantity){
					$this->InventoryStock->create();
					$data_item = array(
						'id'			=> $id,
						'inventory_delivery_location_id' => $warehouse_id,
						'location_id' => $store_id,
						'rack_id' => '0'
					);

					if($this->InventoryStock->save($data_item)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $quantity,
							'status'   => '1',
							'type'     => '1'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}
			}else if($_POST['transfer_type']=='2'){
				$store_id = $_POST['inventory_store_id'];
				$new_quantity = $_POST['quantity'];
				$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];

				if($new_quantity < $quantity){
					$new_quantity_2 = $quantity - $new_quantity;
					$this->InventoryStock->create();
					$data_item = array(
						'id' 								=> $id,
						'quantity' 							=> $new_quantity_2,
						'inventory_delivery_location_id' 	=> $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id']
					);

					$this->InventoryStock->save($data_item);

					$this->InventoryStock->create();
					$data_item_2 = array(
						'general_unit_id' => $get_details_stock[0]['inventory_stocks']['general_unit_id'],
						'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
						'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
						'quantity' => $new_quantity,
						'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
						'created' => date('Y-m-d H:i:s'),
						'inventory_delivery_location_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
						'inventory_store_id' => $store_id
					);

					if($this->InventoryStock->save($data_item_2)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $new_quantity,
							'status'   => '1',
							'type'     => '2'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}else if($new_quantity == $quantity){
					$this->InventoryStock->create();
					$data_item = array(
						'id'			=> $id,
						'inventory_delivery_location_id' 	=> $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
						'location_id' => $store_id,
						'rack_id' => '0'
					);

					if($this->InventoryStock->save($data_item)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $quantity,
							'status'   => '1',
							'type'     => '2'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}
			}else if($_POST['transfer_type'] == '3'){
				$this->loadModel('InventoryItem');
				$code = $_POST['code'];		
				$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];
				$new_quantity = $_POST['quantity'];

				$check_code = $this->InventoryItem->query("SELECT * FROM inventory_items WHERE code ='".$code."'");
				
				if(!$check_code){
					$this->Session->setFlash(__('The item code not exist.'), 'error');
				}else{
					if($new_quantity < $quantity){
						$new_quantity_2 = $quantity - $new_quantity;
						$this->InventoryStock->create();

						$data_item = array(
							'id' 								=> $id,
							'quantity' 							=> $new_quantity_2,
							'inventory_delivery_location_id'  => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id']

						);
						
						$this->InventoryStock->save($data_item);
						$this->InventoryStock->create();
						$data_item_2 = array(
							'general_unit_id' => $check_code[0]['inventory_items']['general_unit_id'],
							'inventory_item_id' => $check_code[0]['inventory_items']['id'],
							'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
							'quantity' => $new_quantity,
							'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
							'created' => date('Y-m-d H:i:s'),
							'inventory_delivery_location_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
							'inventory_store_id' => $get_details_stock[0]['inventory_stocks']['inventory_store_id'],
							'inventory_rack_id' => $get_details_stock[0]['inventory_stocks']['inventory_rack_id']

						);

						if($this->InventoryStock->save($data_item_2)){
							$this->loadModel('InventoryMaterialHistory');
							$this->InventoryMaterialHistory->create();
							$data_history = array(
								'user_id' => $_SESSION['Auth']['User']['id'],
								'inventory_item_id' => $check_code[0]['inventory_items']['id'],
								'quantity' => $new_quantity,
								'status'   => '1',
								'type'     => '3'
							);
							if($this->InventoryMaterialHistory->save($data_history)){
								$new_quantity_inventory_item = $check_code[0]['inventory_items']['quantity'] + $new_quantity;
								$new_quantity_inventory_item_2 = $get_details_stock[0]['inventory_items']['quantity'] - $new_quantity;

								$this->InventoryItem->create();
								$item = array(
									'id' => $check_code[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item
								);

								$this->InventoryItem->save($item);
								$this->InventoryItem->create();

								$item_2 = array(
									'id' => $get_details_stock[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item_2
								);
								if($this->InventoryItem->save($item_2)){
									$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
									return $this->redirect(array('action' => 'transfer'));
								}else{
									$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
								}
							}else{
								$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
							}
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}
					}else if($new_quantity == $quantity){
						$data_item = array(
							'id' => $id,
							'inventory_item_id' => $check_code[0]['inventory_items']['id'],
							'general_unit_id' => $check_code[0]['inventory_items']['general_unit_id']
						);
						if($this->InventoryStock->save($data_item)){
							$this->loadModel('InventoryMaterialHistory');
							$this->InventoryMaterialHistory->create();
							$data_history = array(
								'user_id' => $_SESSION['Auth']['User']['id'],
								'inventory_item_id' => $check_code[0]['inventory_items']['id'],
								'quantity' => $new_quantity,
								'status'   => '1',
								'type'     => '3'
							);
							if($this->InventoryMaterialHistory->save($data_history)){
								$new_quantity_inventory_item = $check_code[0]['inventory_items']['quantity'] + $new_quantity;
								$new_quantity_inventory_item_2 = $get_details_stock[0]['inventory_items']['quantity'] - $new_quantity;

								$this->InventoryItem->create();
								$item = array(
									'id' => $check_code[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item
								);

								$this->InventoryItem->save($item);
								$this->InventoryItem->create();
								$item_2 = array(
									'id' => $get_details_stock[0]['inventory_items']['id'],
									'quantity' => $new_quantity_inventory_item_2
								);
								if($this->InventoryItem->save($item_2)){
									$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
									return $this->redirect(array('action' => 'transfer'));
								}else{
									$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
								}
							}else{
								$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
							}
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}
					}
				}
			}else if($_POST['transfer_type'] == '4'){
				$this->loadModel('InventoryItem');
				$rack = $_POST['inventory_rack_id'];
				$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];
				$new_quantity = $_POST['quantity'];

				if($new_quantity < $quantity){
					$new_quantity_2 = $quantity - $new_quantity;
					$this->InventoryStock->create();
					$data_item = array(
						'id'			=> $id,
						'quantity'		=> $new_quantity_2,
						'inventory_delivery_location_id'  => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id']
					);
					$this->InventoryStock->save($data_item);
					$this->InventoryStock->create();
					$data_item_2 = array(
						'general_unit_id' => $get_details_stock[0]['inventory_stocks']['general_unit_id'],
						'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
						'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
						'quantity' => $new_quantity,
						'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
						'created' => date('Y-m-d H:i:s'),
						'inventory_delivery_location_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
						'inventory_store_id' => $get_details_stock[0]['inventory_stocks']['inventory_store_id'],
						'inventory_rack_id' => $rack
					);
					if($this->InventoryStock->save($data_item_2)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_items']['id'],
							'quantity' => $new_quantity,
							'status'   => '1',
							'type'     => '4'
						);
						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}
					}
				}else if($new_quantity == $quantity){
					$this->InventoryStock->create();
					$data_item = array(
						'id' => $id,
						'inventory_item_id' => $get_details_stock[0]['inventory_items']['id'],
						'inventory_delivery_location_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
						'inventory_store_id' => $get_details_stock[0]['inventory_stocks']['inventory_store_id'],
						'inventory_rack_id' => $rack
					);
					if($this->InventoryStock->save($data_item)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_items']['id'],
							'quantity' => $new_quantity,
							'status'   => '1',
							'type'     => '4'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}
			}else if($_POST['transfer_type']=='4'){
				$rack = $_POST['inventory_rack_id'];
				$new_quantity = $_POST['quantity'];
				$quantity = $get_details_stock[0]['inventory_stocks']['quantity'];

				if($new_quantity < $quantity){
					$new_quantity_2 = $quantity - $new_quantity;
					$this->InventoryStock->create();
					$data_item = array(
						'id' 								=> $id,
						'quantity' 							=> $new_quantity_2,
						'inventory_delivery_location_id' 	=> $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id']
					);

					$this->InventoryStock->save($data_item);

					$this->InventoryStock->create();
					$data_item_2 = array(
						'general_unit_id' => $get_details_stock[0]['inventory_stocks']['general_unit_id'],
						'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
						'inventory_delivery_order_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_order_item_id'],
						'quantity' => $new_quantity,
						'qc_status' => $get_details_stock[0]['inventory_stocks']['qc_status'],
						'created' => date('Y-m-d H:i:s'),
						'inventory_delivery_location_id' => $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
						'inventory_rack_id' => $rack
					);

					if($this->InventoryStock->save($data_item_2)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $new_quantity,
							'status'   => '1',
							'type'     => '4'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}else if($new_quantity == $quantity){
					$this->InventoryStock->create();
					$data_item = array(
						'id'			=> $id,
						'inventory_delivery_location_id' 	=> $get_details_stock[0]['inventory_stocks']['inventory_delivery_location_id'],
						'location_id' => $store_id,
						'rack_id' => '0'
					);

					if($this->InventoryStock->save($data_item)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $get_details_stock[0]['inventory_stocks']['inventory_item_id'],
							'quantity' => $quantity,
							'status'   => '1',
							'type'     => '4'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory stocks has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
						}					
					}else{
						$this->Session->setFlash(__('The inventory stocks could not be save.'), 'error');
					}
				}
			}
		}
			

		$this->loadModel('TransferType');
		$transfer = $this->TransferType->find('list');

		$this->loadModel('InventoryDeliveryLocation');
		$warehouse = $this->InventoryDeliveryLocation->find('list');

		$options = array('conditions' => array('InventoryStock.' . $this->InventoryStock->primaryKey => $id));
		$this->set('inventoryItem', $this->InventoryStock->find('first', $options));

		$ditem = $this->InventoryStock->find('first', array('conditions'=> array('InventoryStock.id' => $id)));

		$this->loadModel('InventoryStore');

		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE id=".$ditem['InventoryItem']['inventory_store_id']);
		
		$store_2 = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE inventory_delivery_location_id=".$ditem['InventoryStock']['inventory_delivery_location_id']);

		$this->loadModel('InventoryDeliveryLocation');
		$this->loadModel('InventoryRack');
		$racks = $this->InventoryRack->find('list');


		$this->loadModel('InventoryRack');
		$racks = $this->InventoryRack->find('list');

		$this->set(compact('transfer', 'warehouse', 'store', 'warehouse_location', 'store_2', 'racks'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->loadModel('TransferType');
		$transfer = $this->TransferType->find('list');

		$this->loadModel('InventoryDeliveryLocation');
		$warehouse = $this->InventoryDeliveryLocation->find('list');

		$options = array('conditions' => array('InventoryStock.' . $this->InventoryStock->primaryKey => $id));
		$this->set('inventoryStock', $this->InventoryStock->find('first', $options));

		$ditem = $this->InventoryStock->find('first', array('conditions'=> array('InventoryStock.id' => $id)));

		$this->loadModel('InventoryStore');

		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE id=".$ditem['InventoryStock']['inventory_store_id']);
		
		$store_2 = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE inventory_delivery_location_id=".$ditem['InventoryStock']['inventory_delivery_location_id']);

		$this->loadModel('InventoryDeliveryLocation');

		$this->set(compact('transfer', 'warehouse', 'store', 'store_2'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryStock->create();

			$this->request->data['InventoryStock']['issued_quantity'] = 0;
			$this->request->data['InventoryStock']['created'] = $this->date;
			$this->request->data['InventoryStock']['user_id'] = $this->user_id;
			$this->request->data['InventoryStock']['qty_in'] = $this->request->data['InventoryStock']['quantity'];

			if ($this->InventoryStock->save($this->request->data)) {

				$stock_id = $this->InventoryStock->getLastInsertId();

				$this->loadModel('InventoryMaterialHistory'); 
				$this->InventoryMaterialHistory->create();
				$history = array(
					'reference' => 'Stock Entry',
					'user_id' => 0, 
					'store_pic' => $this->user_id,
					'inventory_item_id' => $this->request->data['InventoryStock']['inventory_item_id'], 
					'inventory_stock_id' => $stock_id,
					'quantity' => $this->request->data['InventoryStock']['quantity'], // Req qty
					'general_unit_id' => $this->request->data['InventoryStock']['general_unit_id'], 
					'issued_quantity' => $this->request->data['InventoryStock']['quantity'],
					'quantity_balance' => $this->sum_stock_balance($this->request->data['InventoryStock']['inventory_item_id']),
					'inventory_material_request_id' => 0,
					'inventory_material_request_item_id' => 0,
					'created' => $this->date,
					'status' => 1,
					'type' => 7, // 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 5: RMA, 6: grn, 7: First Keyin
					'transfer_type' => 'In'
					);
				$save_history = $this->InventoryMaterialHistory->save($history);
				if(!$save_history) {
					die('830');
				}

				$this->Session->setFlash(__('The inventory stock has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				debug($this->InventoryStock->invalidFields());
				$this->Session->setFlash(__('The inventory stock could not be saved. Please, try again.'), 'error');
			}
		}
		$generalUnits = $this->InventoryStock->GeneralUnit->find('list');
		$inventoryItems = $this->InventoryStock->InventoryItem->find('list');
		$inventoryDeliveryOrderItems = $this->InventoryStock->InventoryDeliveryOrderItem->find('list');
		$generalUnitConverters = $this->InventoryStock->GeneralUnitConverter->find('list');
		$inventoryLocations = $this->InventoryStock->InventoryLocation->find('list');
		
		$inventoryStores = $this->InventoryStock->InventoryStore->find('list');
		
 		$inventoryRacks = $this->InventoryStock->InventoryRack->find('list');
		
		$inventoryDeliveryLocations = $this->InventoryStock->InventoryDeliveryLocation->find('list'); 
		
		$this->set(compact('generalUnits', 'inventoryItems', 'inventoryDeliveryOrderItems', 'generalUnitConverters', 'inventoryLocations', 'inventoryStores', 'inventoryDeliveryLocations', 'inventoryRacks'));
	}

	private function sum_stock_balance($inventory_item_id) { 
		$stocks = $this->InventoryStock->find('all', array(
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $inventory_item_id 
				),
			'fields' => array(
				'SUM(InventoryStock.quantity) AS quantity' 
				),
			'recursive' => -1
			));
		foreach ($stocks as $stock) {
			return $stock[0]['quantity'];
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryStock->exists($id)) {
			throw new NotFoundException(__('Invalid inventory stock'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryStock->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory stock has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory stock could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('InventoryStock.' . $this->InventoryStock->primaryKey => $id));
		$this->request->data = $this->InventoryStock->find('first', $options);
		
		$generalUnits = $this->InventoryStock->GeneralUnit->find('list');
		$inventoryItems = $this->InventoryStock->InventoryItem->find('list');
		$inventoryDeliveryOrderItems = $this->InventoryStock->InventoryDeliveryOrderItem->find('list');
		$generalUnitConverters = $this->InventoryStock->GeneralUnitConverter->find('list');
		$inventoryLocations = $this->InventoryStock->InventoryLocation->find('list');
		$inventoryStores = $this->InventoryStock->InventoryStore->find('list');
		 
		$inventoryDeliveryLocations = $this->InventoryStock->InventoryDeliveryLocation->find('list'); 
		$this->set(compact('generalUnits', 'inventoryItems', 'inventoryDeliveryOrderItems', 'generalUnitConverters', 'inventoryLocations', 'inventoryStores', 'inventoryRacks', 'inventoryDeliveryLocations'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryStock->id = $id;
		if (!$this->InventoryStock->exists()) {
			throw new NotFoundException(__('Invalid inventory stock'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryStock->delete()) {
			$this->Session->setFlash(__('The inventory stock has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory stock could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}