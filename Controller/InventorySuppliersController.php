<?php
App::uses('AppController', 'Controller');


/**
 * InventorySuppliers Controller
 *
 * @property InventorySupplier $InventorySupplier
 * @property PaginatorComponent $Paginator
 */
class InventorySuppliersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator'); 

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('register', 'ajaxfindsuppliercurrency'); 
	}

	public function profile($id = null) {
		if (!$this->InventorySupplier->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier'));
		}
		$options = array('conditions' => array('InventorySupplier.' . $this->InventorySupplier->primaryKey => $id));
		$this->set('inventorySupplier', $this->InventorySupplier->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function register() {
		$this->layout = 'public';
		if ($this->request->is(array('post', 'put'))) { 
			// Update
			

			$ip_add = $this->get_ip();

			$this->request->data['InventorySupplier']['ip_address'] = $ip_add; 
			if($this->request->data['InventorySupplier']['random_code'] != '') {
				$security_code = $this->request->data['InventorySupplier']['random_code'];
				$company_name = $this->request->data['InventorySupplier']['name'];

				// Check for bad code
				if(!ctype_alnum($security_code)) {
					$this->Session->setFlash(__('Invalid security code.'), 'error');
				}

				// Find company 
				$supplier = $this->InventorySupplier->find('first', array(
					'conditions' => array(
						'InventorySupplier.random_code' => $security_code 
						)
					));

				if($supplier) {
					$this->request->data['InventorySupplier']['id'] = $supplier['InventorySupplier']['id'];
					if ($this->InventorySupplier->save($this->request->data)) {
						$supplier_id  = $this->InventorySupplier->getLastInsertId(); // $this->InventorySupplier->getLastInsertId();
					} else {
						$this->Session->setFlash(__('Your application could not be saved. Please, try again.'), 'error');
					}
				} else {
					$this->Session->setFlash(__('Invalid security code or company name.'), 'error');
				}
			} else {
				$this->InventorySupplier->create(); 
				$this->request->data['InventorySupplier']['status'] = 'Waiting Approval';  
				$rand_code = $this->generateRandomString(10); 
				$this->request->data['InventorySupplier']['random_code'] = $rand_code; 
				// Check duplicate name
				$company_name = $this->request->data['InventorySupplier']['name'];
				$supplier = $this->InventorySupplier->find('first', array(
					'conditions' => array( 
						'InventorySupplier.name' => $company_name
						)
					));
				if(!empty($supplier)) {
					$this->Session->setFlash(__('Company name already registered. Please choose Renewal on registration type.'), 'error');
				} else {
					if ($this->InventorySupplier->save($this->request->data)) {
						$supplier_id  = $this->InventorySupplier->getLastInsertId(); // $this->InventorySupplier->getLastInsertId();
					} else {
						$this->Session->setFlash(__('Your application could not be saved. Please, try again.'), 'error');
					}
				}
			} 
			
			
			//var_dump($this->request->data);
			if($supplier_id) {
				$this->loadModel('InventorySupplierCategory');
				$category_id = $this->request->data['InventorySupplier']['category_item'];
			
				$data = array();
				for($i=0; $i<count($category_id); $i++){
					
					$data[] = array( 
						'inventory_supplier_id' => $supplier_id,
						'inventory_item_category_id' => $category_id[$i]
					);
					
				}
				$this->InventorySupplierCategory->saveAll($data, array('deep' => true));

 
				$this->loadModel('InventorySupplierFile');

				$file = $this->request->data['InventorySupplierFile']['file'];
				$subject = $this->request->data['InventorySupplierFile']['subject'];
				$dir = $this->request->data['InventorySupplierFile']['dir'];
				$files = array();
				for($i = 0; $i < count($file); $i++) {
					$this->InventorySupplierFile->create();
					if(!empty($file[$i])) {
						$files = array(
							'file' => $file[$i],
							'inventory_supplier_id' => $supplier_id,
							'subject' => $subject[$i],
							'dir' => $dir[$i]
							);
						$this->InventorySupplierFile->save($files);	
					} 
				}  
			 
				$this->loadModel('User');
				$procs = $this->User->find('all', array(
                    'conditions' => array(
                        'User.group_id' => 11
                        )
                    )); 
                $mail = array();
                foreach ($procs as $user) {
                    $data = array(
                        'to' => $user['User']['email'],
                        'template' => 'supplier_registration',
                        'subject' => 'Supplier Registration Require Your Action: ' . $this->request->data['InventorySupplier']['name'],
                        'content' => array(
                            'company_name' => $this->request->data['InventorySupplier']['name'],
                            'from' => $this->request->data['InventorySupplier']['email'],
                            'username' => $user['User']['username'],  
                            'link' => 'inventory_suppliers/view/'.$supplier_id 
                            )
                        );
                    // $this->send_email($data);
                }   


				$this->Session->setFlash(__('Your application has been saved. Please wait for approval by Administrator.'), 'success');
				//return $this->redirect(array('action' => 'register'));
			}

				
 
			
		}
		$states = $this->InventorySupplier->State->find('list');
		$countries = $this->InventorySupplier->Country->find('list');
		 
		$generalCurrencies = $this->InventorySupplier->GeneralCurrency->find('list');
		$termOfPayments = $this->InventorySupplier->TermOfPayment->find('list');
		$this->set(compact('states', 'countries', 'generalCurrencies', 'termOfPayments'));

		$this->loadModel('InventoryItemCategory');

		$categories = $this->InventoryItemCategory->find('all', array(
			'fields' => array('InventoryItemCategory.id', 'InventoryItemCategory.name'),
			'recursive' => -1, 
			'order' => array(
				'InventoryItemCategory.name' => 'ASC'
			)));
		
		$category = array();
		foreach ($categories as $cat) {
			$category[] = array(
				'id' => $cat['InventoryItemCategory']['id'],
				'name' => $cat['InventoryItemCategory']['name']
				);
		}

		$category_2 = $this->InventoryItemCategory->find('list');
		$this->set('category', $category);

		$this->set('category_2', $category_2);
	}

	private function get_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
 
        return $ipaddress;
    }

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function update($id = null) {
		if (!$this->InventorySupplier->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventorySupplier->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventorySupplier.' . $this->InventorySupplier->primaryKey => $id));
			$this->request->data = $this->InventorySupplier->find('first', $options);
		}
		$states = $this->InventorySupplier->State->find('list');
		$countries = $this->InventorySupplier->Country->find('list'); 
		$generalCurrencies = $this->InventorySupplier->GeneralCurrency->find('list');
		$termOfPayments = $this->InventorySupplier->TermOfPayment->find('list');
		$this->set(compact('states', 'countries', 'generalCurrencies', 'termOfPayments'));
	}

	public function index() {

		$joins = array();

		if(isset($_GET['search'])) {
			if($_GET['from'] != '') {
				$conditions['InventorySupplier.valid_to >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['InventorySupplier.valid_to <='] = trim($_GET['to']);
			}

			if($_GET['name'] != '') {
				$conditions['InventorySupplier.name LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['code'] != '') {
				$conditions['InventorySupplier.code LIKE'] = '%'.trim($_GET['code']) . '%';
			}

			if($_GET['status'] != '') {
				$conditions['InventorySupplier.status'] = trim($_GET['status']);
			} else {
				$conditions['InventorySupplier.status'] = 'Active';
			}

			if($_GET['category'] != '') {
				//$conditions['InventorySupplierCategory.inventory_item_category_id'] = trim($_GET['category']);
				$joins = array(
			        array(
			            'table' => 'inventory_supplier_categories',
			            'alias' => 'Category',
			            'type' => 'INNER',
			            'conditions' => array(
			                'InventorySupplier.id = Category.inventory_supplier_id',
			                'Category.inventory_item_category_id = ' . $_GET['category']
			            ),
			            'group' => 'Category.id'
			        )
			    );
			} 	
 
			$this->request->data['InventorySupplier'] = $_GET;
		} 

		if(isset($_GET['status'])){
			if($_GET['status'] != ''){
				$conditions['InventorySupplier.status LIKE'] = $_GET['status'];
			}
		} else {
			$conditions['InventorySupplier.status'] = 'Active';
		}

		// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer,  
		$conditions['InventorySupplier.id >='] = 1;

		$status = array('Active' => 'Active', 'Inactive' => 'Inactive', 'Waiting Approval');
		$this->set(compact('status'));

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order'=>'InventorySupplier.created DESC',
			//'group'=>'InventorySupplier.name',
			'limit'=>$record_per_page,
			'joins' => $joins
			);

        $suppliers = $this->Paginator->paginate('InventorySupplier'); 

		$this->set('inventorySuppliers', $suppliers);

		$this->loadModel('InventoryItemCategory');
		$categories = $this->InventoryItemCategory->find('list');
		$this->set(compact('categories'));
	}

	private function generateRandomString($length) {
        $characters = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

	public function getstatus() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$status = array();
			
		$status[] = array('value' => 'Waiting Approval', 'text' => 'Waiting for Approval');
		$status[] = array('value' => 'Active', 'text' => 'Active');
		$status[] = array('value' => 'Inactive', 'text' => 'Inactive');
		echo json_encode($status);
	}

	public function setstatus() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if(isset($_POST['value'])) {
			$id = (int)$_POST['pk'];
	        $value = $_POST['value'];
	        $rand_code = $this->generateRandomString(10);
	        $get_running_id = $this->get_running_supplier_code();

	        $s = 'S'.str_pad($get_running_id, 6, 0, STR_PAD_LEFT);

			$json = array();
			//if(is_numeric($value) && is_numeric($id)) {
				$update = $this->InventorySupplier->updateAll(
					array('InventorySupplier.status' => "'".$value."'", 'InventorySupplier.random_code' => "'".$rand_code."'", 'supplier_code' => "'".$s."'"),
					array('InventorySupplier.id' => $id)
				);  

				

				if($update) {
					$json['status'] = true;
				} else {
					$json['status'] = false;
				} 
				echo json_encode($json);

				// Change priority to last
				if($value == 'Active') {
					// Check supplier email
					$supplier = $this->InventorySupplier->find('first', array(
						'conditions' => array(
							'InventorySupplier.id' => $id
							)
						));
					if($supplier) {
						if(filter_var($supplier['InventorySupplier']['email'], FILTER_VALIDATE_EMAIL)) {
							$pic = $supplier['InventorySupplier']['pic'] != NULL ? $supplier['InventorySupplier']['pic'] : $supplier['InventorySupplier']['name']; 
							$data = array(
		                        'to' => $user['User']['email'],
		                        'template' => 'supplier_activation',
		                        'subject' => 'Supplier Registration Status - Tenaga Switchgear Sdn Bhd',
		                        'content' => array(
		                        	'pic' => $pic,
		                        	'supplier_no' => $s,
		                            'company_name' => $supplier['InventorySupplier']['name'] 
		                            )
		                        );
		                    $this->send_email($data);
						}
					}
				} 

			//} else {
			//	header('HTTP 400 Bad Request', true, 400);
			//	echo "This field is required!";		
			//}	
		}
		
	}

	private function get_running_supplier_code(){
		$code = $this->InventorySupplier->find('all', array(
			'conditions' => array('InventorySupplier.status' => 'Active'),
			//'order' => 'InventorySupplier.supplier_code DESC',
			'group' => 'InventorySupplier.name',
			'recursive' => -1
		));
		//print_r($code);
		$running_id = count($code) + 1;

		return $running_id;
		
	}

	/**
	 * view method
	 * 
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this->InventorySupplier->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier'));
		}
		$options = array('conditions' => array('InventorySupplier.' . $this->InventorySupplier->primaryKey => $id));
		$this->set('inventorySupplier', $this->InventorySupplier->find('first', $options));

		$this->loadModel('InventorySupplierCategory');

		$categories = $this->InventorySupplierCategory->find('all', array(
			'conditions' => array(
				'InventorySupplierCategory.inventory_supplier_id' => $id
			)
		));
		$this->set('categories', $categories);

		$this->loadModel('InventorySupplierFile');
		$files = $this->InventorySupplierFile->find('all', array(
			'conditions' => array(
				'InventorySupplierFile.inventory_supplier_id' => $id
			)
		));

		$this->set('files', $files);
	}

	public function item_list($id = null) {
		$this->loadModel('InventorySupplierItem');

		$supplier_item_array = array();
		$supplierItem = $this->InventorySupplierItem->find('all', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_supplier_id' => $id
			),
			'order' => 'InventoryItem.name ASC'
		));
		//print_r($supplierItem);
		if(!$supplierItem){
			$supplier = $this->InventorySupplier->find('first', array(
				'conditions' => array(
					'InventorySupplier.id' => $id
				)
			));
			$supplier_name = $supplier['InventorySupplier']['name'];
		}else{
			$supplier_name = $supplierItem[0]['InventorySupplier']['name'];
		}
		//exit;
		
		foreach ($supplierItem as $s_item) {
			$supplier_item_array[] = array(
				'InventorySupplierItem' => $s_item['InventorySupplierItem'],
				'InventoryItem' => $s_item['InventoryItem'],
				'GeneralUnit' => $s_item['GeneralUnit'],
				//'UnitConversion' => $s_item['UnitConversion'],
				//'MinOrderUnit' => $s_item['MinOrderUnit'],
				//'GeneralCurrency' => $s_item['GeneralCurrency']
			);
		}
		//print_r($supplier_item_array);
		//exit;
		$this->set('supplier_name', $supplier_name);
		$this->set('supplier_item', $supplier_item_array);
		
	}

	public function report($id = null) {
		if (!$this->InventorySupplier->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier'));
		}

		$this->loadModel('InventoryStock');
		$this->loadModel('InventoryStockRejected');
		$this->loadModel('InventorySupplierItem');
		$this->loadModel('InventoryDeliveryOrderItem');
		$this->loadModel('InventoryPurchaseOrderItem');
		$this->loadModel('InventoryItem');
		$supplier_item = $this->InventorySupplierItem->query("SELECT * FROM inventory_supplier_items WHERE inventory_supplier_id=".$id);
		//print_r($supplier_item);
		//exit;
		$items = array();
		foreach ($supplier_item as $item) {
			//print_r($item);
			//$supplier_item[] = $item;
			$id = $item['inventory_supplier_items']['id'];
			$id_item = $item['inventory_supplier_items']['inventory_item_id'];
			$supplier_id = $item['inventory_supplier_items']['inventory_supplier_id'];

			$order_quantity = $this->InventoryPurchaseOrderItem->query("SELECT sum(quantity) as Order_Quantity FROM inventory_purchase_order_items WHERE inventory_supplier_item_id=".$id);
			//print_r($order_quantity);
			$item['inventory_supplier_items']['order'] = $order_quantity[0][0]['Order_Quantity'];

			$get = $this->InventoryStock->query("SELECT sum(quantity) as Stock_Quantity FROM inventory_stocks WHERE inventory_item_id=".$id_item." AND inventory_supplier_id=".$supplier_id);

			$item['inventory_supplier_items']['get'] = $get[0][0]['Stock_Quantity'];

			$rejected = $this->InventoryStockRejected->query("SELECT sum(quantity) as Rejected_Quantity FROM inventory_stock_rejecteds WHERE inventory_item_id=".$id_item." AND inventory_supplier_id=".$supplier_id);

			$item['inventory_supplier_items']['rejected'] = $rejected[0][0]['Rejected_Quantity'];

			
			$get_name = $this->InventoryItem->query("SELECT name FROM inventory_items where id=".$id_item);
			$item['inventory_supplier_items']['name'] = $get_name[0]['inventory_items']['name'];

			//print_r($rejected);
			//$item['inventory_supplier_items']['order'] = $order_quantity[0]['Order_Quantity'];

			//print_r($item);
			$items[] = $item;
			//$supplier_item['inventory_supplier_items']['order_quantity'] = $order_quantity[0]['Order_Quantity'];
		}
		
		//$item[] = $item;
		//$stocks = $this->InventoryStock->query("SELECT * FROM inventory_stocks WHERE inventory_supplier_id=".$id);
		//print_r($stocks);
		//exit;

		$options = array('conditions' => array('InventorySupplier.' . $this->InventorySupplier->primaryKey => $id));
		$inventorySupplier = $this->set('inventorySupplier', $this->InventorySupplier->find('first', $options));
		
		$this->set(compact('items'));

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		
		$this->loadModel('InventoryItemCategory');

		if ($this->request->is('post')) {
			
			$this->InventorySupplier->create();
			if ($this->InventorySupplier->save($this->request->data)) {
				$supplier_id = $this->InventorySupplier->getLastInsertId();

				$this->loadModel('InventorySupplierCategory'); 

				$category_id = $this->request->data['InventorySupplier']['category_item'];
			
				$data = array();
				for($i=0; $i<count($category_id); $i++){
					
					$data[] = array(
						'inventory_supplier_id' => $supplier_id,
						'inventory_item_category_id' => $category_id[$i]
					);
					
				}
				$this->InventorySupplierCategory->saveAll($data, array('deep' => true));

				$this->loadModel('InventorySupplierFile');

				$file = $this->request->data['InventorySupplierFile']['file'];
				$subject = $this->request->data['InventorySupplierFile']['subject'];
				$dir = $this->request->data['InventorySupplierFile']['dir'];
				$files = array();
				for($i = 0; $i < count($file); $i++) {
					$this->InventorySupplierFile->create();
					if(!empty($file[$i])) {
						$files = array(
							'file' => $file[$i],
							'inventory_supplier_id' => $supplier_id,
							'subject' => $subject[$i],
							'dir' => $dir[$i]
							);
						$this->InventorySupplierFile->save($files);	
					} 
				}  


				$this->Session->setFlash(__('The inventory supplier has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));

			} else {
				$this->Session->setFlash(__('The inventory supplier could not be saved. Please, try again.'), 'error');
			}
		}

		$states = $this->InventorySupplier->State->find('list');
		$countries = $this->InventorySupplier->Country->find('list');
		 
		$generalCurrencies = $this->InventorySupplier->GeneralCurrency->find('list');
		$termOfPayments = $this->InventorySupplier->TermOfPayment->find('list');
		$this->set(compact('states', 'countries', 'generalCurrencies', 'termOfPayments'));

		$this->loadModel('InventoryItemCategory');

		$categories = $this->InventoryItemCategory->find('all', array(
			'fields' => array('InventoryItemCategory.id', 'InventoryItemCategory.name'),
			'recursive' => -1, 
			'order' => array(
				'InventoryItemCategory.name' => 'ASC'
			)));
		
		$category = array();
		foreach ($categories as $cat) {
			$category[] = array(
				'id' => $cat['InventoryItemCategory']['id'],
				'name' => $cat['InventoryItemCategory']['name']
				);
		}

		$category_2 = $this->InventoryItemCategory->find('list');
		$this->set('category', $category);

		$this->set('category_2', $category_2);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventorySupplier->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier'));
		}

		$this->loadModel('InventorySupplierFile'); 

		if ($this->request->is(array('post', 'put'))) {
			
			if ($this->InventorySupplier->save($this->request->data)) {
				$this->loadModel('InventorySupplierCategory');

				$this->InventorySupplierCategory->deleteAll(array('InventorySupplierCategory.inventory_supplier_id' => $id), false);

				$category_id = $this->request->data['InventorySupplier']['category_item'];
			
				$data = array();
				for($i=0; $i<count($category_id); $i++){
					
					$data[] = array(
						'inventory_supplier_id' => $id,
						'inventory_item_category_id' => $category_id[$i]
					);
					
				}
				$this->InventorySupplierCategory->saveAll($data, array('deep' => true));

				$this->InventorySupplierFile->saveMany($this->request->data['InventorySupplierFile']);	  

				// $this->InventorySupplierFile->deleteAll( );
 				
 				/*
				$file = $this->request->data['InventorySupplierFile']['file'];
				$subject = $this->request->data['InventorySupplierFile']['subject'];
				$dir = $this->request->data['InventorySupplierFile']['dir'];
				$files = array();
				for($i = 0; $i < count($file); $i++) {
					$this->InventorySupplierFile->create();
					 
						$files = array( 
							'file' => $file[$i],
							'inventory_supplier_id' => $id,
							'subject' => $subject[$i],
							'dir' => $dir[$i]
							);
						$this->InventorySupplierFile->save($files);	  
				}  
				*/

				$this->Session->setFlash(__('The inventory supplier has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('InventorySupplier.' . $this->InventorySupplier->primaryKey => $id));
			$this->request->data = $this->InventorySupplier->find('first', $options);
		}
		$states = $this->InventorySupplier->State->find('list');
		$countries = $this->InventorySupplier->Country->find('list');		 
		$generalCurrencies = $this->InventorySupplier->GeneralCurrency->find('list');
		$termOfPayments = $this->InventorySupplier->TermOfPayment->find('list');
		$this->set(compact('states', 'countries', 'generalCurrencies', 'termOfPayments'));


		$this->loadModel('InventorySupplierCategory');
		$this->loadModel('InventoryItemCategory');
		$cats = $this->InventoryItemCategory->find('list');
		$categories = $this->InventorySupplierCategory->find('all', array(
			'conditions' => array(
				'InventorySupplierCategory.inventory_supplier_id' => $id
			)
		));
		$this->set('categories', $categories);
		$this->set(compact('cats'));

		$files = $this->InventorySupplierFile->find('all', array(
			'conditions' => array(
				'InventorySupplierFile.inventory_supplier_id' => $id
				)
			)); 
		$this->request->data['InventorySupplierFile'] = $files;
		$this->set('files', $files);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventorySupplier->id = $id;
		if (!$this->InventorySupplier->exists()) {
			throw new NotFoundException(__('Invalid inventory supplier'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventorySupplier->delete()) {
			$this->Session->setFlash(__('The inventory supplier has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory supplier could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function ajaxfindsuppliercurrency() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$this->loadModel('InventorySupplier');
		if(isset($this->request->query['term'])) {
			$id = (int)$this->request->query['term']; 
			$item = $this->InventorySupplier->find('first', array('conditions' => array('InventorySupplier.id' => $id))); 
			echo json_encode($item);	
		}
	}

	public function ajaxfindsupplier() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$this->loadModel('InventorySupplierItem');
		if(isset($this->request->query['itemId'])) {
			$id = (int)$this->request->query['itemId'];
			 
			$items = $this->InventorySupplierItem->find('all', array('conditions' => array('InventorySupplierItem.inventory_item_id' => $id)));
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['InventorySupplierItem']['id'], 
					'name' => $item['InventoryItem']['name'],
					'supplier' => $item['InventorySupplier']['name'],
					'default' => $item['InventorySupplierItem']['is_default'],
					'expiry_date' => $item['InventorySupplierItem']['expiry_date'],
					'price' => $item['InventorySupplierItem']['price'],
					'price_per_unit' => $item['InventorySupplierItem']['price_per_unit'],
					'min_order' => $item['InventorySupplierItem']['min_order'],
					'lead_time' => $item['InventorySupplierItem']['lead_time'],
					'unit' => $item['GeneralUnit']['name'],
					);
			}
			echo json_encode($json);	
		}
	}
}
