<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodFatRemarks Controller
 *
 * @property FinishedGoodFatRemark $FinishedGoodFatRemark
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FinishedGoodFatRemarksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodFatRemark->recursive = 0;
		$this->set('finishedGoodFatRemarks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodFatRemark->exists($id)) {
			throw new NotFoundException(__('Invalid finished good fat remark'));
		}
		$options = array('conditions' => array('FinishedGoodFatRemark.' . $this->FinishedGoodFatRemark->primaryKey => $id));
		$this->set('finishedGoodFatRemark', $this->FinishedGoodFatRemark->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodFatRemark->create();
			if ($this->FinishedGoodFatRemark->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good fat remark has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good fat remark could not be saved. Please, try again.'));
			}
		}
		$finishedGoods = $this->FinishedGoodFatRemark->FinishedGood->find('list');
		$users = $this->FinishedGoodFatRemark->User->find('list');
		$finishedGoodComments = $this->FinishedGoodFatRemark->FinishedGoodComment->find('list');
		$this->set(compact('finishedGoods', 'users', 'finishedGoodComments'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodFatRemark->exists($id)) {
			throw new NotFoundException(__('Invalid finished good fat remark'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodFatRemark->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good fat remark has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good fat remark could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodFatRemark.' . $this->FinishedGoodFatRemark->primaryKey => $id));
			$this->request->data = $this->FinishedGoodFatRemark->find('first', $options);
		}
		$finishedGoods = $this->FinishedGoodFatRemark->FinishedGood->find('list');
		$users = $this->FinishedGoodFatRemark->User->find('list');
		$finishedGoodComments = $this->FinishedGoodFatRemark->FinishedGoodComment->find('list');
		$this->set(compact('finishedGoods', 'users', 'finishedGoodComments'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodFatRemark->id = $id;
		if (!$this->FinishedGoodFatRemark->exists()) {
			throw new NotFoundException(__('Invalid finished good fat remark'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodFatRemark->delete()) {
			$this->Session->setFlash(__('The finished good fat remark has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good fat remark could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
