<?php
App::uses('AppController', 'Controller');
/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class AccountDepartmentController extends AppController {

/**
 * Components
 * 
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccountDepartment->recursive = 0;
		$this->set('accountDepartment', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccountDepartment->exists($id)) {
			throw new NotFoundException(__('Invalid customer'));
		}
		$budget_array = array();
		$budget_part = $this->AccountDepartment->find('all', array(
			'conditions' => array(
				'AccountDepartment.id' => $id
			)
		));

		$budget_array[] = array(
			'AccountDepartment' => $budget_part[0]['AccountDepartment'],

			'Group' => $budget_part[0]['Group'],
			'AccountDepartmentBudgets' => $this->get_budget_part($budget_part[0]['AccountDepartment']['id'])
		);
		//$options = array('conditions' => array('accountDepartment.' . $this->AccountDepartment->primaryKey => $id));
		$this->set('accountDepartment', $budget_array[0]);
	}

	private function get_budget_part($id){
		$this->loadModel('AccountDepartmentBudgets');

		$account_array = array();
		$account = $this->AccountDepartmentBudgets->find('all', array(
			'conditions' => array(
				'AccountDepartmentBudgets.account_department_id' => $id,
				'AccountDepartmentBudgets.parent_id' => 0
			),
			'recursive' => 1
		));

		foreach ($account as $acc) {
			$account_array[] = array(
				'Parent' => $acc['AccountDepartmentBudgets'],
				'SaleJobChild' => $acc['SaleJobChild'],
				'Child' => $this->get_child_budget($acc['AccountDepartmentBudgets']['id'])
			);
		}

		return $account_array;
	}

	private function get_child_budget($id){
		$this->loadModel('AccountDepartmentBudgets');

		$child_budget = $this->AccountDepartmentBudgets->find('all', array(
			'conditions' => array(
				'AccountDepartmentBudgets.parent_id' => $id
			),
			'order' => 'AccountDepartmentBudgets.name ASC',
			'recursive' =>-1
		));

		return $child_budget;

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AccountDepartment->create();
			$this->request->data['AccountDepartment']['user_id'] = $_SESSION['Auth']['User']['id']; 
			$this->request->data['AccountDepartment']['year'] = $_POST['year'];
			$this->request->data['AccountDepartment']['used'] = '0'; 
			if ($this->AccountDepartment->save($this->request->data)) {
				$this->Session->setFlash(__('The Department Account has been created.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Department Account could not be created. Please, try again.'), 'error');
			}
		}
		$this->loadModel('Group');
		$groups = $this->Group->find('list');
	
		$this->set(compact('groups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AccountDepartment->exists($id)) {
			throw new NotFoundException(__('Invalid Account Department'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->AccountDepartment->create();
			$this->request->data['AccountDepartment']['user_id'] = $_SESSION['Auth']['User']['id']; 
			$this->request->data['AccountDepartment']['year'] = $_POST['year'];
			$this->request->data['AccountDepartment']['used'] = '0';

			if ($this->AccountDepartment->save($this->request->data)) {
				$this->Session->setFlash(__('The Account Department has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Account Department could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('accountDepartment.' . $this->AccountDepartment->primaryKey => $id));
			$this->request->data = $this->AccountDepartment->find('first', $options);
		}
		$this->loadModel('Group');
		$groups = $this->Group->find('list');
	
		$this->set(compact('groups'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AccountDepartment->id = $id;
		if (!$this->AccountDepartment->exists()) {
			throw new NotFoundException(__('Invalid Department Account Id'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AccountDepartment->delete()) {
			$this->Session->setFlash(__('The Department Account has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The Department Account could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
