<?php
App::uses('AppController', 'Controller');
/**
 * SaleQuotationItemCosts Controller
 *
 * @property SaleQuotationItemCost $SaleQuotationItemCost
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleQuotationItemCostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleQuotationItemCost->recursive = 0;
		$this->set('saleQuotationItemCosts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleQuotationItemCost->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation item cost'));
		}
		$options = array('conditions' => array('SaleQuotationItemCost.' . $this->SaleQuotationItemCost->primaryKey => $id));
		$this->set('saleQuotationItemCost', $this->SaleQuotationItemCost->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleQuotationItemCost->create();
			if ($this->SaleQuotationItemCost->save($this->request->data)) {
				$this->Session->setFlash(__('The sale quotation item cost has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation item cost could not be saved. Please, try again.'));
			}
		}
		$generalUnits = $this->SaleQuotationItemCost->GeneralUnit->find('list');
		$saleQuotationItems = $this->SaleQuotationItemCost->SaleQuotationItem->find('list');
		$this->set(compact('generalUnits', 'saleQuotationItems'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleQuotationItemCost->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation item cost'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleQuotationItemCost->save($this->request->data)) {
				$this->Session->setFlash(__('The sale quotation item cost has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation item cost could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleQuotationItemCost.' . $this->SaleQuotationItemCost->primaryKey => $id));
			$this->request->data = $this->SaleQuotationItemCost->find('first', $options);
		}
		$generalUnits = $this->SaleQuotationItemCost->GeneralUnit->find('list');
		$saleQuotationItems = $this->SaleQuotationItemCost->SaleQuotationItem->find('list');
		$this->set(compact('generalUnits', 'saleQuotationItems'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleQuotationItemCost->id = $id;
		if (!$this->SaleQuotationItemCost->exists()) {
			throw new NotFoundException(__('Invalid sale quotation item cost'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleQuotationItemCost->delete()) {
			$this->Session->setFlash(__('The sale quotation item cost has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale quotation item cost could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
