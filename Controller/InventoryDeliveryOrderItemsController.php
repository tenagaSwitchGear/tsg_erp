<?php
App::uses('AppController', 'Controller');
/**
 * InventoryDeliveryOrderItems Controller
 *
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property PaginatorComponent $Paginator
 */
class InventoryDeliveryOrderItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->loadModel('InventoryItems');
		$this->loadModel('InventorySupplierItems');
		
		$this->InventoryDeliveryOrderItem->recursive = 0;
		$this->set('inventoryDeliveryOrderItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryDeliveryOrderItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory delivery order item'));
		}
		$options = array('conditions' => array('InventoryDeliveryOrderItem.' . $this->InventoryDeliveryOrderItem->primaryKey => $id));
		$this->set('inventoryDeliveryOrderItem', $this->InventoryDeliveryOrderItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryDeliveryOrderItem->create();
			if ($this->InventoryDeliveryOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory delivery order item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory delivery order item could not be saved. Please, try again.'));
			}
		}
		$inventoryDeliveryOrders = $this->InventoryDeliveryOrderItem->InventoryDeliveryOrder->find('list');
		$inventoryItems = $this->InventoryDeliveryOrderItem->InventoryItem->find('list');
		$generalUnits = $this->InventoryDeliveryOrderItem->GeneralUnit->find('list');
		$generalTaxes = $this->InventoryDeliveryOrderItem->GeneralTax->find('list');
		$this->set(compact('inventoryDeliveryOrders', 'inventoryItems', 'generalUnits', 'generalTaxes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryDeliveryOrderItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory delivery order item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryDeliveryOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory delivery order item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory delivery order item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryDeliveryOrderItem.' . $this->InventoryDeliveryOrderItem->primaryKey => $id));
			$this->request->data = $this->InventoryDeliveryOrderItem->find('first', $options);
		}
		$inventoryDeliveryOrders = $this->InventoryDeliveryOrderItem->InventoryDeliveryOrder->find('list');
		$inventoryItems = $this->InventoryDeliveryOrderItem->InventoryItem->find('list');
		$generalUnits = $this->InventoryDeliveryOrderItem->GeneralUnit->find('list');
		$generalTaxes = $this->InventoryDeliveryOrderItem->GeneralTax->find('list');
		$this->set(compact('inventoryDeliveryOrders', 'inventoryItems', 'generalUnits', 'generalTaxes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryDeliveryOrderItem->id = $id;
		if (!$this->InventoryDeliveryOrderItem->exists()) {
			throw new NotFoundException(__('Invalid inventory delivery order item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryDeliveryOrderItem->delete()) {
			$this->Session->setFlash(__('The inventory delivery order item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory delivery order item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
