<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProductsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

	public function ajaxfindproduct() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			
			$conditions = array(); 
			$conditions['OR']['Product.name LIKE'] = '%'.$term.'%';
			$conditions['OR']['Product.code LIKE'] = '%'.$term.'%'; 

			$items = $this->Product->find('all', array(
				'conditions' => $conditions,
				'group' => array('Product.id')
				));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['Product']['id'], 
					'name' => $item['Product']['name'],
					'code' => $item['Product']['code'],
					'price' => $item['Product']['price'],
					'type' => $item['Product']['is_bom'] == 1 ? 'Finished Goods' : 'Raw Material',
					'bom_id' => $item['Product']['is_bom'] == 1 ? $this->find_bom($item['Product']['id']) : 0
					); 
			}
			echo json_encode($json);	
		}
	}

	private function find_bom($id) {
		$this->loadModel('Bom');
		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.product_id' => $id 
				)
			));
		if($bom) {
			return $bom['Bom']['id'];
		}
		return 0;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Product->recursive = 0;
		$this->set('products', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
		$this->set('product', $this->Product->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
			}
		}
		$productCategories = $this->Product->ProductCategory->find('list');
		$this->set(compact('productCategories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
			$this->request->data = $this->Product->find('first', $options);
		}
		$productCategories = $this->Product->ProductCategory->find('list');
		$this->set(compact('productCategories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Product->delete()) {
			$this->Session->setFlash(__('The product has been deleted.'));
		} else {
			$this->Session->setFlash(__('The product could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
