<?php
App::uses('AppController', 'Controller');
/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class CustomersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['Customer.name LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['reg_no'] != '') {
				$conditions['Customer.reg_no LIKE'] = '%'.trim($_GET['reg_no']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['Customer.created >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['Customer.created <='] = trim($_GET['to']);
			}
		}
		$conditions['Customer.id !='] = 0;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'Customer.name ASC', 'limit' => $record_per_page);
		$this->set('customers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Customer->exists($id)) {
			throw new NotFoundException(__('Invalid customer'));
		}
		$options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
		$this->set('customer', $this->Customer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Customer->create();
  
			$this->request->data['Customer']['created'] = date('Y-m-d H:i:s'); 
			$this->request->data['Customer']['user_id'] = $_SESSION['Auth']['User']['id']; 
 			//print_r($_POST);
 			//exit;
			if ($this->Customer->save($this->request->data)) {
				$customer_id = $this->Customer->getLastInsertId();
				$user_id = $this->Session->read('Auth.User.id');
				$name = 'Add new Customer : ' . $this->request->data['Customer']['name'];
				$link = 'customers/view/'.$customer_id;
				$type = 'Add Customer';
				
				$this->insert_log($user_id, $name, $link, $type);

				$this->Session->setFlash(__('The customer has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer could not be saved. Please, try again.'), 'error');
			}
		}
		$customerStations = $this->Customer->CustomerStation->find('list');
		$states = $this->Customer->State->find('list');
		$countries = $this->Customer->Country->find('list');
		$customerTypes = $this->Customer->CustomerType->find('list');
		$customerBusinessCategories = $this->Customer->CustomerBusinessCategory->find('list');
		$users = $this->Customer->User->find('list');
		$customerOwnerships = $this->Customer->CustomerOwnership->find('list');
		$this->set(compact('customerStations', 'states', 'countries', 'customerTypes', 'customerBusinessCategories', 'users', 'customerOwnerships'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Customer->exists($id)) {
			throw new NotFoundException(__('Invalid customer'));
		}
		if ($this->request->is(array('post', 'put'))) {
		
			$this->request->data['Customer']['modified'] = date('Y-m-d H:i:s'); 
			$this->request->data['Customer']['user_id'] = $_SESSION['Auth']['User']['id'];
			if ($this->Customer->save($this->request->data)) {

				$user_id = $this->Session->read('Auth.User.id');
				$name = 'Edit Customer : ' . $this->request->data['Customer']['name'];
				$link = 'customers/view/'.$id;
				$type = 'Edit Customer';
				
				$this->insert_log($user_id, $name, $link, $type);

				$this->Session->setFlash(__('The customer has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
			$this->request->data = $this->Customer->find('first', $options);
		}
		$customerStations = $this->Customer->CustomerStation->find('list');
		$states = $this->Customer->State->find('list');
		$countries = $this->Customer->Country->find('list');
		$customerTypes = $this->Customer->CustomerType->find('list');
		$customerBusinessCategories = $this->Customer->CustomerBusinessCategory->find('list');
		$users = $this->Customer->User->find('list');
		$customerOwnerships = $this->Customer->CustomerOwnership->find('list');
		$this->set(compact('customerStations', 'states', 'countries', 'customerTypes', 'customerBusinessCategories', 'users', 'customerOwnerships'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Customer->id = $id;
		if (!$this->Customer->exists()) {
			throw new NotFoundException(__('Invalid customer'));
		}

		$customer = $this->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $id
				)
			));

		$user_id = $this->Session->read('Auth.User.id');
		$name = 'Delete Customer : ' . $customer['Customer']['name'];
		$link = 'customers/view/'.$id;
		$type = 'Delete Customer';
		
		$this->insert_log($user_id, $name, $link, $type);

		$this->request->allowMethod('post', 'delete');
		if ($this->Customer->delete()) {   
			$this->Session->setFlash(__('The customer has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The customer could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

	
} 
