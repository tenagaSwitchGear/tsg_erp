<?php
App::uses('AppController', 'Controller');
/**
 * ProjectDrawingFiles Controller
 *
 * @property ProjectDrawingFile $ProjectDrawingFile
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectDrawingFilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectDrawingFile->recursive = 0;
		$this->set('projectDrawingFiles', $this->Paginator->paginate());
	}

	public function ajaxupload($id = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if ($this->request->is('post')) { 
			$this->ProjectDrawingFile->create();
			$json = array();
			$this->request->data['ProjectDrawingFile']['project_drawing_id'] = $id; 
			if($this->ProjectDrawingFile->save($this->request->data)) { 
				$json['response'] = 'Success upload';
			} else {
				$json['response'] = '';
			}
			debug($this->ProjectDrawingFile->save($this->request->data)); 
			echo json_encode($json);	
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null, $drawing = null) {
		if (!$this->ProjectDrawingFile->exists($id)) {
			throw new NotFoundException(__('Invalid project drawing file'));
		}
		$options = array('conditions' => array('ProjectDrawingFile.' . $this->ProjectDrawingFile->primaryKey => $id));
		$this->set('projectDrawingFile', $this->ProjectDrawingFile->find('first', $options));

		$this->loadModel('ProjectDrawingTracking');
		$trackings = $this->ProjectDrawingTracking->find('all', array(
			'conditions' => array(
				'ProjectDrawingTracking.project_drawing_file_id' => $id
				)
			));
		$this->set('trackings', $trackings);
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id) {
		if ($this->request->is('post')) { 
			if ($this->ProjectDrawingFile->saveAll($this->request->data['ProjectDrawingFile'])) {
				$this->Session->setFlash(__('The project drawing file has been saved.'));
				return $this->redirect(array('controller' => 'project_drawings', 'action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('The project drawing file could not be saved. Please, try again.'));
			}
		}
		$projectDrawings = $this->ProjectDrawingFile->ProjectDrawing->find('list');
		$this->set(compact('projectDrawings'));

		$this->set('id', $id);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $drawing = null) {
		if (!$this->ProjectDrawingFile->exists($id)) {
			throw new NotFoundException(__('Invalid project drawing file'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectDrawingFile->save($this->request->data)) {
				$this->Session->setFlash(__('The project drawing file has been saved.'));
				return $this->redirect(array('controller' => 'project_drawings', 'action' => 'view', $drawing));
			} else {
				$this->Session->setFlash(__('The project drawing file could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectDrawingFile.' . $this->ProjectDrawingFile->primaryKey => $id));
			$this->request->data = $this->ProjectDrawingFile->find('first', $options);
		}
		$projectDrawings = $this->ProjectDrawingFile->ProjectDrawing->find('list');
		$this->set(compact('projectDrawings'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectDrawingFile->id = $id;
		if (!$this->ProjectDrawingFile->exists()) {
			throw new NotFoundException(__('Invalid project drawing file'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectDrawingFile->delete()) {
			$this->Session->setFlash(__('The project drawing file has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project drawing file could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
