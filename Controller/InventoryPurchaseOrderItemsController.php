<?php
App::uses('AppController', 'Controller');
/**
 * InventoryPurchaseOrderItems Controller
 *
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 * @property PaginatorComponent $Paginator
 */
class InventoryPurchaseOrderItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryPurchaseOrderItem->recursive = 0;
		$this->set('inventoryPurchaseOrderItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryPurchaseOrderItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory purchase order item'));
		}
		$options = array('conditions' => array('InventoryPurchaseOrderItem.' . $this->InventoryPurchaseOrderItem->primaryKey => $id));
		$this->set('inventoryPurchaseOrderItem', $this->InventoryPurchaseOrderItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryPurchaseOrderItem->create();
			if ($this->InventoryPurchaseOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory purchase order item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory purchase order item could not be saved. Please, try again.'));
			}
		}
		$inventoryItems = $this->InventoryPurchaseOrderItem->InventoryItem->find('list');
		$inventoryPurchaseOrders = $this->InventoryPurchaseOrderItem->InventoryPurchaseOrder->find('list');
		$generalUnits = $this->InventoryPurchaseOrderItem->GeneralUnit->find('list');
		$this->set(compact('inventoryItems', 'inventoryPurchaseOrders', 'generalUnits'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryPurchaseOrderItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory purchase order item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryPurchaseOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory purchase order item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory purchase order item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryPurchaseOrderItem.' . $this->InventoryPurchaseOrderItem->primaryKey => $id));
			$this->request->data = $this->InventoryPurchaseOrderItem->find('first', $options);
		}
		$inventoryItems = $this->InventoryPurchaseOrderItem->InventoryItem->find('list');
		$inventoryPurchaseOrders = $this->InventoryPurchaseOrderItem->InventoryPurchaseOrder->find('list');
		$generalUnits = $this->InventoryPurchaseOrderItem->GeneralUnit->find('list');
		$this->set(compact('inventoryItems', 'inventoryPurchaseOrders', 'generalUnits'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryPurchaseOrderItem->id = $id;
		if (!$this->InventoryPurchaseOrderItem->exists()) {
			throw new NotFoundException(__('Invalid inventory purchase order item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryPurchaseOrderItem->delete()) {
			$this->Session->setFlash(__('The inventory purchase order item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory purchase order item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
