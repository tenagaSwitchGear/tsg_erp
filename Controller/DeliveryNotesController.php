<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodDeliveries Controller
 *
 * @property FinishedGoodDelivery $FinishedGoodDelivery
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class DeliveryNotesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodDelivery->recursive = 0;
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => 2
		));
		/*print_r($finishedGoodDelivery);
		exit;
		$item_array = array();
		foreach ($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] as $finished_item) {
			$item_array[] => array(
				'fg_item' => $this->get_fg_item($finished_item['id']),
				'inv_item' => $this->get_inv_item($finished_item['inventory_item_id'])
			);
		}
		print_r($item_array);
		exit;
		$finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] = $item;*/

		//$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
		$this->set('finishedGoodDelivery', $finishedGoodDelivery);
	}
	private function get_fg_item($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$item = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.id' => $id
			),
			'recursive' => -1
		));

		return $item;
	}
	private function get_inv_item($id){
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			),
			'recursive' => -1
		));

		return $item;
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		
		if ($this->request->is('post')) {
			//print_r($_POST);
			//exit;
			$this->request->data['FinishedGoodDelivery']['type'] = 'DR';
			$SO_no = $this->request->data['FinishedGoodDelivery']['findSaleOrder'];
			$this->request->data['FinishedGoodDelivery']['user_id'] = $_SESSION['Auth']['User']['id'];
			$this->request->data['FinishedGoodDelivery']['do_number'] = 0;
			$this->FinishedGoodDelivery->create();
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$id = $this->FinishedGoodDelivery->getInsertID();

				$this->loadModel('FinishedGoodDeliveryItem');

				$sale_order_item_id = $_POST['sale_order_item_id'];
				$inventory_item_id = $_POST['inventory_item_id'];
				$sale_job_id = $_POST['sale_job_id'];
				$sale_job_child_id = $_POST['sale_job_child_id'];
				$quantity = $_POST['quantity_deliver'];
				//$remark = $_POST['remark'];

				for($i=0; $i<count($sale_order_item_id); $i++){
					$this->FinishedGoodDeliveryItem->create();
					$this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'] = $id;
					$this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'] = $sale_order_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'] = $inventory_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_id'] = $sale_job_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'] = $sale_job_child_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['quantity'] = $quantity[$i];
					//$this->request->data['FinishedGoodDeliveryItem']['remark'] = $remark[$i];
					$this->request->data['FinishedGoodDeliveryItem']['user_id'] = $_SESSION['Auth']['User']['id'];

					$this->FinishedGoodDeliveryItem->save($this->request->data);
				}

				$request_id = $id;
	            //echo $request_id;
	            //exit;
	            // Find Approval
	            $group_id =$this->Session->read('Auth.User.group_id');
	            $this->loadModel('Approval');
	            $approvals = $this->Approval->find('all', array(
	                'conditions' => array(
	                    'Approval.name' => 'Delivery Request',
	                    'Approval.group_id' => $group_id
	                    )
	                ));
	            if($approvals) {
	                foreach ($approvals as $approval) {
	                    $data = array(
	                        'to' => $approval['User']['email'],
	                        'template' => 'delivery_request',
	                        'subject' => 'Delivery Request Require Your Approval #' . $SO_no,
	                        'content' => array(
	                            'so_number' => $SO_no,
	                            'from' => $this->Session->read('Auth.User.username'),
	                            'username' => $approval['User']['username'],  
	                            'link' => 'finished_good_deliveries/verifyview/'.$request_id
	                            )
	                        );
	                    $this->send_email($data);
	                }
	            }

				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

	public function addItem($id = null) {
		$this->loadModel('SaleOrderItem');
		$this->loadModel('FinishedGoodDelivery');
		$this->loadModel('FinishedGoodDeliveryItem');

		if($this->request->is('post')){
			$finished_good_id = $this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'];
			$sale_order_item_id = $this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'];
			$inventory_item_id = $this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'];
			$sale_job_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_id'];
			$sale_job_child_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'];
			$quantity = $this->request->data['FinishedGoodDeliveryItem']['quantity'];

			for($i=0; $i<count($finished_good_id); $i++){
				$this->FinishedGoodDeliveryItem->create();
				$this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'] = $finished_good_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'] = $sale_order_item_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'] = $inventory_item_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['sale_job_id'] = $sale_job_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'] = $sale_job_child_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['quantity'] = $quantity[$i];
				$this->request->data['FinishedGoodDeliveryItem']['user_id'] = $_SESSION['Auth']['User']['id'];

				$this->FinishedGoodDeliveryItem->save($this->request->data);
			}

			$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
			return $this->redirect(array('action' => 'index'));
		}

		$finished_good = $this->FinishedGoodDelivery->find('all', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));

		$fgood = array(
			'FinishedGood' => $finished_good[0]['FinishedGoodDelivery'],
			'SaleOrder' => $this->get_sale_order($finished_good[0]['FinishedGoodDelivery']['sale_order_id']),
			'SaleOrderItem' => $this->get_sale_order_item($finished_good[0]['FinishedGoodDelivery']['sale_order_id']),
			'SaleJobs' => $this->get_jobs($finished_good[0]['FinishedGoodDelivery']['sale_job_id']),
		);
		$this->set('fgood', $fgood);
	}

	private function get_sale_order($id_sale_order){
		$this->loadModel('SaleOrder');
		$sale_order = $this->SaleOrder->find('all', array(
			'conditions' => array(
				'SaleOrder.id' => $id_sale_order
			),
			'recursive' => -1
		));
		
		return $sale_order[0]['SaleOrder'];
	}

	private function get_sale_order_item($id){
		$this->loadModel('SaleOrderItem');
		$saleorderitem = $this->SaleOrderItem->find('all', array(
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $id
			),
		));
		
		return $saleorderitem;
	}

	private function get_jobs($id){
		$this->loadModel('SaleJob');
		$salejob = $this->SaleJob->find('all', array(
			'conditions' => array(
				'SaleJob.id' => $id
			),
			'recursive' => -1
		));
		
		if(empty($salejob)){
			return $salejob;
		}else{
			return $salejob[0]['SaleJob'];
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$id = $id;

				$this->loadModel('FinishedGoodDeliveryItem');

				$fgitemid = $this->request->data['FinishedGoodDeliveryItem']['id'];
				$sale_order_item_id = $this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'];
				$inventory_item_id = $this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'];
				$sale_job_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_id'];
				$sale_job_child_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'];
				$quantity = $this->request->data['FinishedGoodDeliveryItem']['quantity'];
				//$remark = $_POST['remark'];

				for($i=0; $i<count($sale_order_item_id); $i++){
					$this->FinishedGoodDeliveryItem->create();
					$this->request->data['FinishedGoodDeliveryItem']['id'] = $fgitemid[$i];
					$this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'] = $id;
					$this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'] = $sale_order_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'] = $inventory_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_id'] = $sale_job_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'] = $sale_job_child_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['quantity'] = $quantity[$i];
					//$this->request->data['FinishedGoodDeliveryItem']['remark'] = $remark[$i];
					$this->request->data['FinishedGoodDeliveryItem']['user_id'] = $_SESSION['Auth']['User']['id'];

					$this->FinishedGoodDeliveryItem->save($this->request->data);
				}
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		} else {
			$fg_array = array();
			$finishedgooddlvry = $this->FinishedGoodDelivery->find('all', array(
				'conditions' => array(
					'FinishedGoodDelivery.id' => $id
				)
			));
			//print_r($finishedgooddlvry);
			//exit;

			$fg_array[] = array(
				'FinishedGoodDelivery' => $finishedgooddlvry[0]['FinishedGoodDelivery'],
				'SaleJob' => $finishedgooddlvry[0]['SaleJob'],
				'SaleOrder' => $finishedgooddlvry[0]['SaleOrder'],
				'SaleOrderItem' => $this->get_soitem($finishedgooddlvry[0]['SaleOrder']['id']),
				'SaleJobChild' => $finishedgooddlvry[0]['SaleJobChild'],
				'Customer' => $finishedgooddlvry[0]['Customer'],
				'SaleQuotation' => $finishedgooddlvry[0]['SaleQuotation'],
				'User' => $finishedgooddlvry[0]['User'],
				'FinishedGoodDeliveryItem' => $this->get_item_fg($finishedgooddlvry[0]['FinishedGoodDelivery']['id'])
			);



			//$this->set('this->request->data', $fg_array);

			//$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
			$this->request->data = $fg_array[0];
		}

		//print_r($fg_array);
		//exit;
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

	private function get_soitem($id){
		$this->loadModel('SaleOrderItem');
		$soitem = $this->SaleOrderItem->find('all', array(
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $id
			),
			'recursive' => -1
		));

		return $soitem;
	}

	private function get_item_fg($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$arr = array();
		$item_fg = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.finished_good_delivery_id' => $id
			)
		));

		foreach ($item_fg as $fitem) {
			$arr[] = array(
				'FinishedGoodDeliveryItem' => $fitem['FinishedGoodDeliveryItem'],
				'InventoryItem' => $this->get_detail($fitem['FinishedGoodDeliveryItem']['inventory_item_id']),
			);
		}
		

		return $arr;
	}

	private function get_detail($id){
		$this->loadModel('InventoryItem');
		$arr_item = array();
		$d_i = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			)
		));

		$arr_item[]=array(
			'Item' => $d_i[0]['InventoryItem'],
			'GeneralUnit' => $d_i[0]['GeneralUnit']
		);

		return $arr_item;

	}

	public function view_approved($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			print_r($_POST);
			exit;
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'store'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}
		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem2($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}
	public function view_rejected($id=null) {
		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}
	public function view_delivered($id=null) {
		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function viewverify($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			if($this->FinishedGoodDelivery->save($this->request->data)){
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'store'));
			}else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function verifyview($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			if($this->FinishedGoodDelivery->save($this->request->data)){
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'dn_index'));
			}else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function verifyview2($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			if($this->FinishedGoodDelivery->save($this->request->data)){
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'approved'));
			}else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			)
		));

		//print_r($finishedGoodDelivery);
		//exit;
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem2($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function approval() {
		$conditions['FinishedGoodDelivery.status'] = 3;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'FinishedGoodDelivery.id DESC', 'limit' => $record_per_page);
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

	public function approved() {
		$conditions['FinishedGoodDelivery.status'] = 6;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'FinishedGoodDelivery.id DESC', 'limit' => $record_per_page);
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

	public function rejected() {
		$conditions['FinishedGoodDelivery.status'] = 5;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'FinishedGoodDelivery.id DESC', 'limit' => $record_per_page);
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

	public function delivered() {
		$conditions['FinishedGoodDelivery.status'] = 8;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'FinishedGoodDelivery.id DESC', 'limit' => $record_per_page);
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

	public function store() {
		$nfg_array = array();

		$record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array(
            'conditions' => array(
				'FinishedGoodDelivery.status' => 2,
				'FinishedGoodDelivery.type' => 'DR'
			)
        );
        $nfg = $this->Paginator->paginate('FinishedGoodDelivery');
        foreach ($nfg as $nfg) {
            $nfg_array[] = array(
				'FinishedGoodDelivery' => $nfg['FinishedGoodDelivery'],
				'SaleJob' => $nfg['SaleJob'],
				'SaleOrder' => $nfg['SaleOrder'],
				'SaleJobChild' => $nfg['SaleJobChild'],
				'Customer' => $nfg['Customer'],
				'SaleQuotation' => $nfg['SaleQuotation'],
				'FinishedGoodDeliveryItem' => $nfg['FinishedGoodDeliveryItem'],
				'DN' => $this->get_dn($nfg['FinishedGoodDelivery']['id']),
			);
        }

        $this->set('finishedGoodDeliveries', $nfg_array);

	}

	private function get_dn($id){
		$dn_array = array();
		$get_dn = $this->FinishedGoodDelivery->find('all', array(
			'conditions' => array(
				'FinishedGoodDelivery.dr_id' => $id
			),
			'recursive' => -1
		));

		foreach ($get_dn as $dn) {
			$dn_array[] = array(
				'FinishedGoodDelivery' => $dn['FinishedGoodDelivery']
			);
		}

		return $dn_array;
	}

	public function dn_index() {
		$conditions = array(
			'FinishedGoodDelivery.type' => 'DN',
			'FinishedGoodDelivery.status >=' => 6,
			'FinishedGoodDelivery.status <' => 8,
			
		);
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'FinishedGoodDelivery.id DESC', 'limit' => $record_per_page);
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
		//$log = $this->FinishedGoodDelivery->getDataSource()->getLog(false, false);
		//debug($log);
	}

	public function view_dr($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$id = $this->request->data['FinishedGoodDelivery']['id'];
			$fg_id = $this->request->data['FinishedGoodDelivery']['fgid'];
			$remark = $this->request->data['FinishedGoodDelivery']['remark'];
			$status = $this->request->data['FinishedGoodDelivery']['status'];
			$lorry_no = $this->request->data['FinishedGoodDelivery']['lorry_no'];
			$driver_name = $this->request->data['FinishedGoodDelivery']['driver_name'];
			$driver_ic = $this->request->data['FinishedGoodDelivery']['driver_ic'];
			$date_delivered = $this->request->data['FinishedGoodDelivery']['date'];

			$data = $this->FinishedGoodDelivery->query("SELECT id, do_number FROM finished_good_deliveries ORDER BY id DESC");
			if(empty($data)){
                $bil = '1';
            }else{
                $bil = $data[0]['finished_good_deliveries']['id']+1;
            }

			

			if($status != 0){
				$this->FinishedGoodDelivery->create();
				if($data[0]['finished_good_deliveries']['do_number']==0){
					$DO_no = $this->generate_code('DO', $bil);
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}else{
					$DO_no = $data[0]['finished_good_deliveries']['do_number'];
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}

				if($this->FinishedGoodDelivery->save($data_fg)){
					if($status = '3'){
						$request_id = $this->FinishedGoodDelivery->getLastInsertId();
	                    //echo $request_id;
	                    //exit;
	                    // Find Approval
	                    $group_id =$this->Session->read('Auth.User.group_id');
	                    $this->loadModel('Approval');
	                    $approvals = $this->Approval->find('all', array(
	                        'conditions' => array(
	                            'Approval.name' => 'Delivery Note',
	                            'Approval.group_id' => $group_id
	                            )
	                        ));
	                    if($approvals) {
	                        foreach ($approvals as $approval) {
	                            $data = array(
	                                'to' => $approval['User']['email'],
	                                'template' => 'delivery_note',
	                                'subject' => 'Delivery Note Require Your Approval ' . $DO_no,
	                                'content' => array(
	                                    'do_number' => $DO_no,
	                                    'from' => $this->Session->read('Auth.User.username'),
	                                    'username' => $approval['User']['username'],  
	                                    'link' => 'finished_good_delivery/verifyview/'.$request_id
	                                    )
	                                );
	                            $this->send_email($data);
	                        }
	                    }
	                }
					$this->loadModel('FinishedGoodDeliveryItem');
					for($i=0; $i<count($fg_id); $i++){
						$this->FinishedGoodDeliveryItem->create();
						$data_fgitem = array(
						'id' => $fg_id[$i],
						'remark' => $remark[$i]
						);

						$this->FinishedGoodDeliveryItem->save($data_fgitem);
					}

					$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
					return $this->redirect(array('action' => 'store'));

				}else{
					$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
				}
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'SaleOrderItem' => $this->get_soitem($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		/*print_r($finishedGoodDelivery);
		exit;
		$item_array = array();
		foreach ($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] as $finished_item) {
			$item_array[] => array(
				'fg_item' => $this->get_fg_item($finished_item['id']),
				'inv_item' => $this->get_inv_item($finished_item['inventory_item_id'])
			);
		}
		print_r($item_array);
		exit;
		$finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] = $item;*/

		//$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function view_dn($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$id = $this->request->data['FinishedGoodDelivery']['id'];
			$fg_id = $this->request->data['FinishedGoodDelivery']['fgid'];
			$remark = $this->request->data['FinishedGoodDelivery']['remark'];
			$status = $this->request->data['FinishedGoodDelivery']['status'];
			$lorry_no = $this->request->data['FinishedGoodDelivery']['lorry_no'];
			$driver_name = $this->request->data['FinishedGoodDelivery']['driver_name'];
			$driver_ic = $this->request->data['FinishedGoodDelivery']['driver_ic'];
			$date_delivered = $this->request->data['FinishedGoodDelivery']['date'];

			$data = $this->FinishedGoodDelivery->query("SELECT id, do_number FROM finished_good_deliveries ORDER BY id DESC");
			if(empty($data)){
                $bil = '1';
            }else{
                $bil = $data[0]['finished_good_deliveries']['id']+1;
            }

			

			if($status != 0){
				$this->FinishedGoodDelivery->create();
				if($data[0]['finished_good_deliveries']['do_number']==0){
					$DO_no = $this->generate_code('DO', $bil);
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}else{
					$DO_no = $data[0]['finished_good_deliveries']['do_number'];
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}

				if($this->FinishedGoodDelivery->save($data_fg)){
					if($status = '3'){
						$request_id = $this->FinishedGoodDelivery->getLastInsertId();
	                    //echo $request_id;
	                    //exit;
	                    // Find Approval
	                    $group_id =$this->Session->read('Auth.User.group_id');
	                    $this->loadModel('Approval');
	                    $approvals = $this->Approval->find('all', array(
	                        'conditions' => array(
	                            'Approval.name' => 'Delivery Note',
	                            'Approval.group_id' => $group_id
	                            )
	                        ));
	                    if($approvals) {
	                        foreach ($approvals as $approval) {
	                            $data = array(
	                                'to' => $approval['User']['email'],
	                                'template' => 'delivery_note',
	                                'subject' => 'Delivery Note Require Your Approval ' . $DO_no,
	                                'content' => array(
	                                    'do_number' => $DO_no,
	                                    'from' => $this->Session->read('Auth.User.username'),
	                                    'username' => $approval['User']['username'],  
	                                    'link' => 'finished_good_delivery/verifyview/'.$request_id
	                                    )
	                                );
	                            $this->send_email($data);
	                        }
	                    }
	                }
					$this->loadModel('FinishedGoodDeliveryItem');
					for($i=0; $i<count($fg_id); $i++){
						$this->FinishedGoodDeliveryItem->create();
						$data_fgitem = array(
						'id' => $fg_id[$i],
						'remark' => $remark[$i]
						);

						$this->FinishedGoodDeliveryItem->save($data_fgitem);
					}

					$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
					return $this->redirect(array('action' => 'store'));

				}else{
					$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
				}
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'SaleOrderItem' => $this->get_soitem($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem2($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function create_do($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$id = $this->request->data['FinishedGoodDelivery']['id'];
			
			$status = $this->request->data['FinishedGoodDelivery']['status'];

			$data = $this->FinishedGoodDelivery->find('all', array(
				'conditions' => array(
					'FinishedGoodDelivery.id' => $id 
				)
			));

			$data_2 = $this->FinishedGoodDelivery->find('all', array(
				'conditions' => array(
					'FinishedGoodDelivery.type' => 'DN' 
				),
				'recursive' => -1
			));

			if(empty($data_2)){
                $bil = '1';
            }else{
                $bil = $data_2[0]['FinishedGoodDelivery']['id']+1;
            }	

			$DO_no = $this->generate_code('DO', $bil);

			$sale_job_id = $data[0]['FinishedGoodDelivery']['sale_job_id'];
			$sale_job_child_id = $data[0]['FinishedGoodDelivery']['sale_job_child_id'];
            $sale_order_id = $data[0]['FinishedGoodDelivery']['sale_order_id'];
            $customer_id = $data[0]['FinishedGoodDelivery']['customer_id'];
			$sale_quotation_id = $data[0]['FinishedGoodDelivery']['sale_quotation_id'];         
			$user_id = $data[0]['FinishedGoodDelivery']['user_id'];
			$do_number = $DO_no;
			$remark = $data[0]['FinishedGoodDelivery']['remark'];
			$status = $status;
			$delivery_date = $data[0]['FinishedGoodDelivery']['delivery_date'];
			$type = 'DN';
			$dr_id = $id;

			$data_fg_new = array(
				'sale_job_id' => $sale_job_id,
				'sale_job_child_id' => $sale_job_child_id,
				'sale_order_id' => $sale_order_id,
				'customer_id' => $customer_id,
				'sale_quotation_id' => $sale_quotation_id,
				'user_id' => $user_id,
				'do_number' => $do_number,
				'remark' => $remark,
				'status' => $status,
				'delivery_date' => $delivery_date,
				'type' => $type,
				'dr_id' => $id
			);
			
            $this->FinishedGoodDelivery->create();

            if($this->FinishedGoodDelivery->save($data_fg_new)){
				$id = $this->FinishedGoodDelivery->getLastInsertId();

				$fg_id = $this->request->data['FinishedGoodDelivery']['fgid'];
				$quantity = $this->request->data['FinishedGoodDelivery']['quantity'];
				$remark = $this->request->data['FinishedGoodDelivery']['remark'];

				$this->loadModel('FinishedGoodDeliveryItem');
				for($i=0; $i<count($fg_id); $i++){
					$this->FinishedGoodDeliveryItem->create();
					$data_fgitem = array(
						'id' => $fg_id[$i],
						'delivery_order_id' => $id,
						'quantity_delivered' => $quantity[$i],
						'remark' => $remark[$i]
					);

					$this->FinishedGoodDeliveryItem->save($data_fgitem);
				}			

				$request_id = $id;
                //echo $request_id;
                //exit;
                // Find Approval
                $group_id =$this->Session->read('Auth.User.group_id');
                $this->loadModel('Approval');
                $approvals = $this->Approval->find('all', array(
                    'conditions' => array(
                        'Approval.name' => 'Delivery Order',
                        'Approval.group_id' => $group_id
                        )
                    ));
                if($approvals) {
                    foreach ($approvals as $approval) {
                        $data = array(
                            'to' => $approval['User']['email'],
                            'template' => 'delivery_order',
                            'subject' => 'Delivery Order Require Your Approval ' . $DO_no,
                            'content' => array(
                                'do_number' => $DO_no,
                                'from' => $this->Session->read('Auth.User.username'),
                                'username' => $approval['User']['username'],  
                                'link' => 'finished_good_deliveries/verifyview2/'.$request_id
                                )
                            );
                        $this->send_email($data);
                    }
                }	

                $this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'dn_index'));	

            }else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'SaleOrderItem' => $this->get_soitem($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	private function get_sj($id){
		$this->loadModel('SaleJob');
		$salejob = $this->SaleJob->find('all', array(
			'conditions' => array(
				'SaleJob.id' => $id
			),
			'recursive' => -1
		));

		return $salejob[0]['SaleJob'];
	}

	private function get_so($id){
		$this->loadModel('SaleOrder');
		$saleorder = $this->SaleOrder->find('all', array(
			'conditions' => array(
				'SaleOrder.id' => $id
			),
			'recursive' => -1
		));

		return $saleorder[0]['SaleOrder'];
	}

	private function get_cust($id){
		$this->loadModel('Customer');
		$customer = $this->Customer->find('all', array(
			'conditions' => array(
				'Customer.id' => $id
			),
			'recursive' => -1
		));

		return $customer[0]['Customer'];
	}

	private function get_sq($id){
		$this->loadModel('SaleQuotation');
		$salequotation = $this->SaleQuotation->find('all', array(
			'conditions' => array(
				'SaleQuotation.id' => $id
			),
			'recursive' => -1
		));

		return $salequotation[0]['SaleQuotation'];
	}

	private function get_fgitem($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$fg_item = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.finished_good_delivery_id' => $id
			)
		));
		$item_array = array();
		foreach ($fg_item as $item) {
			$item_array[] = array(
				'FinishedGoodDeliveryItem' => $item['FinishedGoodDeliveryItem'],
				'InventoryItem' => $this->get_fg_item_detail($item['FinishedGoodDeliveryItem']['inventory_item_id'])
			);
		}

		return $item_array;
	}

	private function get_fgitem2($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$fg_item = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.delivery_order_id' => $id
			)
		));
		$item_array = array();
		foreach ($fg_item as $item) {
			$item_array[] = array(
				'FinishedGoodDeliveryItem' => $item['FinishedGoodDeliveryItem'],
				'InventoryItem' => $this->get_fg_item_detail($item['FinishedGoodDeliveryItem']['inventory_item_id'])
			);
		}

		return $item_array;
	}

	private function get_fg_item_detail($id){
		$this->loadModel('InventoryItem');
		$detail_array = array();
		$inv_items = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			)
		));

		$detail_array[] = array(
			'InventoryItem' => $inv_items[0]['InventoryItem'],
			'GeneralUnit' => $inv_items[0]['GeneralUnit']
		);
		return $detail_array;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodDelivery->id = $id;
		if (!$this->FinishedGoodDelivery->exists()) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodDelivery->delete()) {
			$this->Session->setFlash(__('The finished good delivery has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The finished good delivery could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FinishedGoodDelivery->recursive = 0;
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
		$this->set('finishedGoodDelivery', $this->FinishedGoodDelivery->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodDelivery->create();
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'));
			}
		}
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
			$this->request->data = $this->FinishedGoodDelivery->find('first', $options);
		}
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FinishedGoodDelivery->id = $id;
		if (!$this->FinishedGoodDelivery->exists()) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodDelivery->delete()) {
			$this->Session->setFlash(__('The finished good delivery has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good delivery could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function view_pdf($id = null) {
	    //$this->request->id = $id;
	    if (!$this->FinishedGoodDelivery->exists($id)) {
	        throw new NotFoundException(__('Invalid finished good id'));
	    }
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printDO';  
	        $this->loadModel('FinishedGoodDeliveryItem');
	        
	        $finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
				'conditions' => array(
					'FinishedGoodDelivery.id' => $id
				),
				'recursive' => -1
			));
			$fg_array = array();
			$fg_array[] = array(
				'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
				'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
				'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
				'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
				'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
				'FinishedGoodDeliveryItem' => $this->get_fgitem2($finishedGoodDelivery['FinishedGoodDelivery']['id'])
			);

	        $this->set('finished_good_deliveries', $fg_array);
	}
}
