<?php
App::uses('AppController', 'Controller');
/**
 * InventoryMaterialRequestItems Controller
 *
 * @property InventoryMaterialRequestItem $InventoryMaterialRequestItem
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryMaterialRequestItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryMaterialRequestItem->recursive = 0;
		$this->set('inventoryMaterialRequestItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryMaterialRequestItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request item'));
		}
		$options = array('conditions' => array('InventoryMaterialRequestItem.' . $this->InventoryMaterialRequestItem->primaryKey => $id));
		$this->set('inventoryMaterialRequestItem', $this->InventoryMaterialRequestItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryMaterialRequestItem->create();
			if ($this->InventoryMaterialRequestItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory material request item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory material request item could not be saved. Please, try again.'));
			}
		}
		$inventoryMaterialRequests = $this->InventoryMaterialRequestItem->InventoryMaterialRequest->find('list');
		$inventoryItems = $this->InventoryMaterialRequestItem->InventoryItem->find('list');
		$generalUnits = $this->InventoryMaterialRequestItem->GeneralUnit->find('list');
		$this->set(compact('inventoryMaterialRequests', 'inventoryItems', 'generalUnits'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 Type = MRN / RMA
 */
	public function edit($id = null, $mrn_id = null, $type = null) {
		if (!$this->InventoryMaterialRequestItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory material request item'));
		}
		$item = $this->InventoryMaterialRequestItem->find('first', array(
			'conditions' => array(
				'InventoryMaterialRequestItem.id' => $id
				)
			));

		if ($this->request->is(array('post', 'put'))) {

			$old_code = $this->request['item'];

			if ($this->InventoryMaterialRequestItem->save($this->request->data)) {
				
				$new_code = $this->request->data['InventoryItem']['code'];

				$name = 'Edit '.$type.' Item from ' . $old_code . ' to ' . $new_code;
				$link = 'inventory_material_request_items/edit/'.$id;
				$type = 'Edit '.$type.' Item';

				$this->insert_log($this->user_id, $name, $link, $type);

				$this->Session->setFlash(__('The MRN item has been saved.'));
				return $this->redirect(array('controller' => 'inventory_material_requests', 'action' => 'storeview', $mrn_id));
			} else {
				$this->Session->setFlash(__('The inventory material request item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryMaterialRequestItem.' . $this->InventoryMaterialRequestItem->primaryKey => $id));
			$this->request->data = $this->InventoryMaterialRequestItem->find('first', $options);
		}
		
		$inventoryItems = $this->InventoryMaterialRequestItem->InventoryItem->find('list');
		$generalUnits = $this->InventoryMaterialRequestItem->GeneralUnit->find('list');
		$this->set(compact('inventoryMaterialRequests', 'inventoryItems', 'generalUnits'));

		$this->set('item', $item);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryMaterialRequestItem->id = $id;
		if (!$this->InventoryMaterialRequestItem->exists()) {
			throw new NotFoundException(__('Invalid inventory material request item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryMaterialRequestItem->delete()) {
			$this->Session->setFlash(__('The inventory material request item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory material request item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
