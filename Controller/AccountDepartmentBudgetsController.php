<?php
App::uses('AppController', 'Controller');
/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class AccountDepartmentBudgetsController extends AppController {

 
/**
=======
/**  
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }
    

	public function ajaxfindbudget() {
		$this->autoRender = false; 
		if(isset($this->request->query['type'])) {
			$type = (int)$this->request->query['type'];
			$sale_job_child_id = (int)$this->request->query['sale_job_child_id'];

			if(isset($this->request->query['xmptrc'])) {
				$group_id = (int)$this->request->query['xmptrc'];
			} else {
				$group_id = $this->Session->read('Auth.User.group_id');
			} 

			// Non job budget
			if($type != 3) {
				$conditions['AccountDepartmentBudget.type'] = $type;
				$conditions['AccountDepartmentBudget.year'] = date('Y');
				$conditions['AccountDepartmentBudget.sale_job_child_id'] = 0;
				$conditions['AccountDepartment.group_id'] = $group_id;
				$conditions['AccountDepartmentBudget.parent_id !='] = 0;
			} else {
				$conditions['AccountDepartmentBudget.type'] = 3; 
				$conditions['AccountDepartmentBudget.sale_job_child_id'] = $sale_job_child_id;  
			}
			
			$options = array(
				'conditions' => $conditions
				);
			
			$budgets = $this->AccountDepartmentBudget->find('all', $options);

			echo json_encode($budgets);
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccountDepartmentBudget->recursive = 0;
		$this->set('accountDepartmentBudget', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccountDepartmentBudget->exists($id)) {
			throw new NotFoundException(__('Invalid account department budget'));
		}
		$options = array('conditions' => array('AccountDepartmentBudget.' . $this->AccountDepartmentBudget->primaryKey => $id), 'recursive' => 2);
		$this->set('accountDepartmentBudget', $this->AccountDepartmentBudget->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null) {
		
		if ($this->request->is('post')) {
			//print_r($_POST);
			$subbudget = $_POST['subbudget'];
			$sum = array_sum($subbudget);
			//echo $sum;
			//exit;

			if($sum > $this->request->data['AccountDepartmentBudgets']['maxbudget']){
				$this->Session->setFlash(__('Exceed budget allocated.'), 'error');
				return $this->redirect(array('action' => 'add', $this->request->data['AccountDepartmentBudgets']['account_department_id']));
			}else{
				$this->loadModel('AccountDepartmentBudgets');
				$this->AccountDepartmentBudgets->create();
				$data_accdeptbudget = array(
					'account_department_id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
					'name' => $this->request->data['AccountDepartmentBudgets']['name'],
					'total' => $sum,
					'used' => 0,
					'balance' => $sum,
					'year' => $this->request->data['AccountDepartmentBudgets']['year'],
					'parent_id' => 0,
					'type' => $this->request->data['AccountDepartmentBudgets']['type'],
					'is_material' => 0,
					'sale_job_child_id' => $this->request->data['AccountDepartmentBudgets']['sale_job_child_id'] 
				);

				if($this->AccountDepartmentBudgets->save($data_accdeptbudget)){
					if($_POST['subname']){
						$parent_id = $this->AccountDepartmentBudgets->getLastInsertId();
						$subname = $_POST['subname'];
						$subbudget = $_POST['subbudget'];

						for($i=0; $i<count($subname); $i++){
							$this->AccountDepartmentBudgets->create();
							$data_sub = array(
								'account_department_id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
								'name' => $subname[$i],
								'total' => $subbudget[$i],
								'used' => 0,
								'balance' => $subbudget[$i],
								'year' => $this->request->data['AccountDepartmentBudgets']['year'],
								'parent_id' => $parent_id,
								'type' => $this->request->data['AccountDepartmentBudgets']['type'],
								'is_material' => 0,
								'sale_job_child_id' => $this->request->data['AccountDepartmentBudgets']['sale_job_child_id'] 
							);
							$this->AccountDepartmentBudgets->save($data_sub);
						}
					}
	
					//update allocated
					$this->loadModel('AccountDepartment');
						
					$get_detail = $this->AccountDepartmentBudgets->find('all', array(
						'fields' => array(
							'SUM(AccountDepartmentBudgets.total) AS TOTAL'
						),
						'conditions' => array(
							'AccountDepartmentBudgets.account_department_id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
							'AccountDepartmentBudgets.parent_id' => 0
						),
						'recursive' => -1
							
					));
						
					$curr_value = $get_detail[0][0]['TOTAL'];
					
					$this->AccountDepartment->create();
					$data_budget = array(
						'id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
						'allocated' => $curr_value
					);
					$this->AccountDepartment->save($data_budget);

					$this->Session->setFlash(__('The Department Account Budgets has been created.'), 'success');
					return $this->redirect(array('controller'=> 'account_department', 'action' => 'view', $this->request->data['AccountDepartmentBudgets']['account_department_id']));
				}else{
					$this->Session->setFlash(__('The Department Account Budgets could not be save.'), 'error');
					return $this->redirect(array('action' => 'add', $this->request->data['AccountDepartmentBudgets']['account_department_id']));
				}
			}
		}

		$this->loadModel('AccountDepartmentBudgets');
		$existed_budget = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets WHERE account_department_id=".$id);

		$this->loadModel('AccountDepartment');

		$acc_dept = $this->AccountDepartment->query("SELECT * FROM account_departments WHERE id=".$id);

		$this->loadModel('Group');
		$groups = $this->Group->find('list');
	
		$this->set(compact('groups', 'acc_dept', 'existed_budget'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $id_2=null) {		
		if ($this->request->is(array('post', 'put'))) {
			 
			//exit;
			$this->loadModel('AccountDepartmentBudgets');

			$get_detail = $this->AccountDepartmentBudgets->find('all', array(
				'fields' => array(
					'SUM(AccountDepartmentBudgets.total) AS TOTAL'
				),
				'conditions' => array(
					'AccountDepartmentBudgets.account_department_id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
					'AccountDepartmentBudgets.parent_id' => 0
				),
				'recursive' => -1
			));
			
			$curr_value = $get_detail[0][0]['TOTAL'] - $this->request->data['AccountDepartmentBudgets']['old_total'];
			$curr_value_2 = $curr_value + $this->request->data['AccountDepartmentBudgets']['total'];
			
			if($curr_value_2 > $this->request->data['AccountDepartmentBudgets']['maxbudget']){
				$this->Session->setFlash(__('Exceed budget allocated.'), 'error');
				return $this->redirect(array('action' => 'edit', $this->request->data['AccountDepartmentBudgets']['account_department_id'], $this->request->data['AccountDepartmentBudgets']['id']));
			}else{
				$this->loadModel('AccountDepartmentBudgets');
				$get_current_detail = $this->AccountDepartmentBudgets->find('first', array(
					'conditions' =>array(
						'AccountDepartmentBudgets.id' => $this->request->data['AccountDepartmentBudgets']['id'],
					),
					'recursive' => -1
				));

				//print_r($get_current_detail);
				$parent_id = $get_current_detail['AccountDepartmentBudgets']['parent_id'];

				//update parent total
				$get_sum = $this->AccountDepartmentBudgets->find('all', array(
					'fields' => array(
						'SUM(AccountDepartmentBudgets.total) AS SUM'
					),
					'conditions' => array(
						'AccountDepartmentBudgets.parent_id' => $parent_id
					),
					'recursive' => -1
				));
				
				$sum = $get_sum[0][0]['SUM'] - $this->request->data['AccountDepartmentBudgets']['old_total'];
				$sum_2 = $sum + $this->request->data['AccountDepartmentBudgets']['total'];
				$this->AccountDepartmentBudgets->create();
				//update_parent
				$data_parent = array(
					'id' => $parent_id,
					'total' => $sum_2,
					'used' => 0,
					'balance' => $sum_2,
				);
				$this->AccountDepartmentBudgets->save($data_parent);
				
				$this->AccountDepartmentBudgets->create();
				$data_update = array(
					'id' => $this->request->data['AccountDepartmentBudgets']['id'],
					'name' => $this->request->data['AccountDepartmentBudgets']['name'],
					'total' => $this->request->data['AccountDepartmentBudgets']['total'],
					'used' => 0,
					'balance' => $this->request->data['AccountDepartmentBudgets']['total'],
					'year' => $this->request->data['AccountDepartmentBudgets']['year'],
				);

				if($this->AccountDepartmentBudgets->save($data_update)){
					$this->loadModel('AccountDepartment');
					$this->AccountDepartment->create();
					$data_budget = array(
						'id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
						'allocated' => $curr_value_2
					);
					$this->AccountDepartment->save($data_budget);
					
					$this->Session->setFlash(__('The Department Account Budgets has been created.'), 'success');
					return $this->redirect(array('controller'=>'accountDepartment', 'action' => 'view', $this->request->data['AccountDepartmentBudgets']['account_department_id']));
				}else{
					$this->Session->setFlash(__('The Department Account Budgets could not be created. Please, try again.'), 'error');
				}
			}
		
		}else{
			$this->loadModel('AccountDepartmentBudgets');
			$options = array('conditions' => array('AccountDepartmentBudgets.' . $this->AccountDepartmentBudgets->primaryKey => $id_2));
			$this->request->data = $this->AccountDepartmentBudgets->find('first', $options);
		}
		
		$this->loadModel('AccountDepartmentBudgets');
		$existed_budget = $this->AccountDepartmentBudgets->query("SELECT * FROM account_department_budgets WHERE account_department_id=".$id);

		$this->loadModel('AccountDepartment');

		$acc_dept = $this->AccountDepartment->query("SELECT * FROM account_departments WHERE id=".$id);

		$this->loadModel('Group');
		$groups = $this->Group->find('list');
	
		$this->set(compact('groups', 'acc_dept', 'existed_budget'));
	}

	public function edit_child($id = null, $id_2=null) {
		if ($this->request->is('post')) {
			$this->loadModel('AccountDepartmentBudgets');

			$subbudget = $_POST['subbudget'];
			$sum = array_sum($subbudget);

			if($sum > $this->request->data['AccountDepartmentBudgets']['maxbudget']){
				$this->Session->setFlash(__('Exceed budget allocated.'), 'error');
				return $this->redirect(array('action' => 'add', $this->request->data['AccountDepartmentBudgets']['account_department_id']));
			}else{
				$this->AccountDepartmentBudgets->deleteAll(array('AccountDepartmentBudgets.parent_id'=>$this->request->data['AccountDepartmentBudgets']['account_department_budget_id']));

				$this->AccountDepartmentBudgets->create();
				$data_accdeptbudget = array(
					'account_department_id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
					'id' => $this->request->data['AccountDepartmentBudgets']['account_department_budget_id'],
					'name' => $this->request->data['AccountDepartmentBudgets']['name'],
					'total' => $sum,
					'used' => 0,
					'balance' => $sum,
					'year' => $this->request->data['AccountDepartmentBudgets']['year'],
					'parent_id' => 0,
					'type' => $this->request->data['AccountDepartmentBudgets']['type'],
					'is_material' => 0,
					'sale_job_child_id' => $this->request->data['AccountDepartmentBudgets']['sale_job_child_id'] 
				);

				if($this->AccountDepartmentBudgets->save($data_accdeptbudget)){
					if($_POST['subname']){
						$parent_id = $this->request->data['AccountDepartmentBudgets']['account_department_budget_id'];
						$subname = $_POST['subname'];
						$subbudget = $_POST['subbudget'];

						for($i=0; $i<count($subname); $i++){
							$this->AccountDepartmentBudgets->create();
							$data_sub = array(
								'account_department_id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
								'name' => $subname[$i],
								'total' => $subbudget[$i],
								'used' => 0,
								'balance' => $subbudget[$i],
								'year' => $this->request->data['AccountDepartmentBudgets']['year'],
								'parent_id' => $parent_id,
								'type' => $this->request->data['AccountDepartmentBudgets']['type'],
								'is_material' => 0,
								'sale_job_child_id' => $this->request->data['AccountDepartmentBudgets']['sale_job_child_id']
							);
							$this->AccountDepartmentBudgets->save($data_sub);
						}
					}

					//update allocated
					$this->loadModel('AccountDepartment');
					
					$get_detail = $this->AccountDepartmentBudgets->find('all', array(
						'fields' => array(
							'SUM(AccountDepartmentBudgets.total) AS TOTAL'
						),
						'conditions' => array(
							'AccountDepartmentBudgets.account_department_id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
							'AccountDepartmentBudgets.parent_id' => 0
						),
						'recursive' => -1
						
					));
					
					$curr_value = $get_detail[0][0]['TOTAL'];
				
					$this->AccountDepartment->create();
					$data_budget = array(
						'id' => $this->request->data['AccountDepartmentBudgets']['account_department_id'],
						'allocated' => $curr_value
					);
					$this->AccountDepartment->save($data_budget);

					$this->Session->setFlash(__('The Department Account Budgets has been created.'), 'success');
					return $this->redirect(array('controller'=> 'account_department', 'action' => 'view', $this->request->data['AccountDepartmentBudgets']['account_department_id']));
				}else{
					$this->Session->setFlash(__('The Department Account Budgets could not be save.'), 'error');
					return $this->redirect(array('action' => 'add', $this->request->data['AccountDepartmentBudgets']['account_department_id']));
				}
			}
		}


		$this->loadModel('AccountDepartmentBudgets');
		$parent = $this->AccountDepartmentBudgets->find('first', array(
			'conditions' => array(
				'AccountDepartmentBudgets.id' => $id_2 
			),
			'recursive' => 1
		));

		$arr[] = array(
			'AccountDepartment' => $this->get_acc($parent['AccountDepartmentBudgets']['account_department_id']),
			'Parent' => $parent['AccountDepartmentBudgets'],
			'SaleJobChild' => $parent['SaleJobChild'],
			'Child' => $this->get_child($parent['AccountDepartmentBudgets']['id'])
		);

		$this->set('acc_dept', $arr);
		//print_r($arr);
		//exit;
	}

	private function get_acc($id){
		$this->loadModel('AccountDepartment');
		$acc = $this->AccountDepartment->find('first', array(
			'conditions' => array(
				'AccountDepartment.id' => $id,
			),
			'recursive' => -1
		));

		return $acc['AccountDepartment'];
	}

	private function get_child($id){
		$this->loadModel('AccountDepartmentBudgets');
		$child = $this->AccountDepartmentBudgets->find('all', array(
			'conditions' => array(
				'AccountDepartmentBudgets.parent_id' => $id
			),
			'recursive' => -1
		));

		return $child;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id_2 = null, $id = null, $nilai = null) {
		$this->loadModel('AccountDepartmentBudgets');
		$this->loadModel('AccountDepartment');
		$this->AccountDepartmentBudgets->id = $id;
		if (!$this->AccountDepartmentBudgets->exists()) {
			throw new NotFoundException(__('Invalid Department Account Budget Id'));
		}
		$this->request->allowMethod('post', 'delete');
		
		$budget = $nilai;
		$id_budget = $id;
		$id_parent = $id_2;

		$parent_detail = $this->AccountDepartmentBudgets->find('first', array(
			'conditions' => array(
				'AccountDepartmentBudgets.id' => $id_parent
			),
			'recursive' => -1
		));

		$new_parent_total = $parent_detail['AccountDepartmentBudgets']['total'] - $budget;

		$this->AccountDepartmentBudgets->create();
		$data_parent = array(
			'id' => $id_parent,
			'total' => $new_parent_total,
			'balance' => $new_parent_total,
			'parent_id' => 0
		);
		$this->AccountDepartmentBudgets->save($data_parent);

		$account_detail = $this->AccountDepartment->find('first', array(
			'conditions' => array(
				'AccountDepartment.id' => $parent_detail['AccountDepartmentBudgets']['account_department_id']
			),
			'recursive' => -1
		));

		$new_allocated = $account_detail['AccountDepartment']['allocated'] - $budget;

		$this->AccountDepartment->create();
		$data_account = array(
			'id' => $parent_detail['AccountDepartmentBudgets']['account_department_id'],
			'allocated' => $new_allocated
		);
		$this->AccountDepartment->save($data_account);

		if ($this->AccountDepartmentBudgets->delete($id_budget)) {
			$this->Session->setFlash(__('The Department Account Budget has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The Department Account Budget could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('controller' => 'accountdepartment', 'action' => 'view', $account_detail['AccountDepartment']['id']));
	}

	public function delete_2($id = null) {
		$id_budget = $id;
		$this->loadModel('AccountDepartment');
		$this->loadModel('AccountDepartmentBudgets');
		$detail = $this->AccountDepartmentBudgets->find('first', array(
			'conditions' => array(
				'AccountDepartmentBudgets.id' => $id_budget
			),
			'recursive' => -1
		));

		$curr_value = $detail['AccountDepartmentBudgets']['total']; 

		$department_budget = $this->AccountDepartment->find('first', array(
			'conditions' => array(
				'AccountDepartment.id' => $detail['AccountDepartmentBudgets']['account_department_id']
			),
			'recursive' => -1
		));

		$curr_value_2 = $department_budget['AccountDepartment']['allocated'];

		$new_value = $curr_value_2 - $curr_value;

		//echo $new_value;
		$this->AccountDepartment->create();
		$data_account = array(
			'id' => $department_budget['AccountDepartment']['id'],
			'allocated' => $new_value,
		);
		$this->AccountDepartment->save($data_account);

		if ($this->AccountDepartmentBudgets->delete($id_budget)) {
			$this->Session->setFlash(__('The Department Account Budget has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The Department Account Budget could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('controller' => 'accountdepartment', 'action' => 'view', $department_budget['AccountDepartment']['id']));
	}
}
