<?php
App::uses('AppController', 'Controller');
/**
 * InventoryRacks Controller
 *
 * @property InventoryRack $InventoryRack
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryRacksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

 
	public function index() {
		$this->InventoryRack->recursive = 2;
		$this->set('inventoryRacks', $this->Paginator->paginate());

		$stores = $this->InventoryRack->InventoryStore->find('list');
		$this->set(compact('stores'));
	}

	public function setdefaultrackindex() {

		$this->loadModel('InventoryItem');
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
			}

			if($_GET['code'] != '') {
				$conditions['InventoryItem.code LIKE'] = '%'.$_GET['code'].'%';
			}

			if($_GET['inventory_item_category_id'] != '') {
				$conditions['InventoryItem.inventory_item_category_id'] = (int)$_GET['inventory_item_category_id'];
			} 
 
 
			$this->request->data['InventoryItem'] = $_GET;
		} 

		// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer,  
		$conditions['InventoryItem.id !='] = 0; 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array( 
			'conditions' => $conditions,
			'order'=>'InventoryItem.code ASC', 
			'limit'=>$record_per_page, 
			'recursive' => 0
			); 
		$items = $this->Paginator->paginate('InventoryItem');



		$this->loadModel('InventoryItemCategory');
		$this->loadModel('InventoryLocation');
		$this->loadModel('InventoryStore');
		$this->set('inventoryItems', $items); 
		$categories = $this->InventoryItemCategory->find('list'); 
		$locations = $this->InventoryLocation->find('list');
		$stores = $this->InventoryStore->find('list');

		$this->set(compact('categories', 'locations', 'stores'));
	}

	public function editlocation($id = null) {
		$this->loadModel('InventoryItem');
		if (!$this->InventoryItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item'));
		}

		$options = array('conditions' => array('InventoryItem.' . $this->InventoryItem->primaryKey => $id));
		$inventoryItem = $this->InventoryItem->find('first', $options);
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryItem->save($this->request->data)) {

				$this->insert_log($this->user_id, 'Edit Default Location: ' . $inventoryItem['InventoryItem']['code'], 'inventory_racks/editlocation/'.$id, 'Edit Default Location');

				$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory item could not be saved. Please, try again.'));
			}
		} else {
			
			$this->request->data = $this->InventoryItem->find('first', $options);
		}

		

		$this->set('inventoryItem', $inventoryItem);

		$inventoryItemCategories = $this->InventoryItem->InventoryItemCategory->find('list', array(
			'recursive' => -1
			));
		$generalUnits = $this->InventoryItem->GeneralUnit->find('list', array(
			'recursive' => -1
			));
		$locations = $this->InventoryItem->InventoryLocation->find('list', array(
			'recursive' => -1
			));
		$stores = $this->InventoryItem->InventoryStore->find('list', array(
			'recursive' => -1
			));
		$rack = $this->InventoryItem->InventoryRack->find('list', array(
			'recursive' => -1
			));

		$Qc = array('1' => 'Yes', '0' => 'No');

		$this->set(compact('inventoryItemCategories', 'generalUnits', 'Qc', 'locations', 'stores', 'rack'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryRack->exists($id)) {
			throw new NotFoundException(__('Invalid inventory rack'));
		}
		$options = array('conditions' => array('InventoryRack.' . $this->InventoryRack->primaryKey => $id));
		$this->set('inventoryRack', $this->InventoryRack->find('first', $options));
 
		$this->set('items', $this->InventoryRack->InventoryItem->find('all', array('conditions' => array(
			'InventoryItem.inventory_rack_id' => $id
			))));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryRack->create();
			if ($this->InventoryRack->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory rack has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory rack could not be saved. Please, try again.'));
			}
		}
		$inventoryStores = $this->InventoryRack->InventoryStore->find('list');
		$this->set(compact('inventoryStores'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryRack->exists($id)) {
			throw new NotFoundException(__('Invalid inventory rack'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryRack->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory rack has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory rack could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventoryRack.' . $this->InventoryRack->primaryKey => $id));
			$this->request->data = $this->InventoryRack->find('first', $options);
		}
		$inventoryStores = $this->InventoryRack->InventoryStore->find('list');
		$this->set(compact('inventoryStores'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryRack->id = $id;
		if (!$this->InventoryRack->exists()) {
			throw new NotFoundException(__('Invalid inventory rack'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryRack->delete()) {
			$this->Session->setFlash(__('The inventory rack has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The inventory rack could not be deleted. Please, try again.'), 'error');
		}
		//return $this->redirect(array('action' => 'index'));
	}
 
}
