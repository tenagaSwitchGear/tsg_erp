<?php
App::uses('AppController', 'Controller');
/**
 * BomChildren Controller
 *
 * @property BomChild $BomChild
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BomChildsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->BomChild->recursive = 0;
		$this->set('bomChildren', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->BomChild->exists($id)) {
			throw new NotFoundException(__('Invalid bom child'));
		}
		$options = array('conditions' => array('BomChild.' . $this->BomChild->primaryKey => $id));
		$this->set('bomChild', $this->BomChild->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($bom_id = null, $parent_id = null) {
		if ($this->request->is('post')) {
			$this->BomChild->create();

			$this->request->data['BomChild']['bom_id'] = $bom_id;
			if($parent_id != null) {
				$this->request->data['BomChild']['parent_id'] = $parent_id;	
			} else {
				$this->request->data['BomChild']['parent_id'] = 0;	
			} 
			$copy_from_id = $this->request->data['BomChild']['copy_from_id'];
			$type = $this->request->data['BomChild']['type'];
			$from_bom_id = $this->request->data['BomChild']['from_bom_id'];
			if ($this->BomChild->save($this->request->data)) {
				
				$insert_id = $this->BomChild->getLastInsertId(); 

				if($type == 'bom') {
					// Copy all item & child from BOM
					$this->attach_bom_child($copy_from_id, $bom_id, $insert_id);
				}
				if($type == 'child') {
					// Copy item only from child 
					$this->attach_child($copy_from_id, $from_bom_id, $bom_id, $insert_id);
				} 


				$this->Session->setFlash(__('The bom child has been saved.'), 'success');
				return $this->redirect(array('controller' => 'boms', 'action' => 'bomchild', $bom_id));
			} else {
				$this->Session->setFlash(__('The bom child could not be saved. Please, try again.'));
			}
		}
		$bom = $this->BomChild->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $bom_id
				)
			));
		$this->set('bom', $bom);

		if($parent_id != null) {
			$child = $this->BomChild->find('first', array(
				'conditions' => array(
					'BomChild.id' => $parent_id
					)
				));
		} else {
			$child = null;
		}

		$this->set('child', $child);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $bom_id = null) {
		if (!$this->BomChild->exists($id)) {
			throw new NotFoundException(__('Invalid bom child'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BomChild->save($this->request->data)) {
				$this->Session->setFlash(__('The bom child has been saved.'), 'success');
				return $this->redirect(array('controller' => 'boms', 'action' => 'bomchild', $bom_id));
			} else {
				$this->Session->setFlash(__('The bom child could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BomChild.' . $this->BomChild->primaryKey => $id));
			$this->request->data = $this->BomChild->find('first', $options);
		} 

		$generalUnit = $this->BomChild->GeneralUnit->find('list');

		$boms = $this->BomChild->Bom->find('list');
		$this->set(compact('boms', 'generalUnit'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function delete_first($id = null, $bom_id = null) {
		$this->BomChild->id = $id;
		 
		$this->request->allowMethod('post', 'delete');
		if ($this->BomChild->delete()) {  
		 
			$this->BomItem->deleteAll(
				array(
					'BomItem.bom_child_id' => $id
					),
				false
			);	

			$this->delete_sub_assembly($id, $bom_id);
			$this->Session->setFlash(__('The bom child has been deleted.'), 'success');
			return $this->redirect(array('controller' => 'boms', 'action' => 'bomchild', $bom_id)); 
		} else {
			$this->Session->setFlash(__('The inventory material request could not be deleted. Please, try again.'));
		}
	}

	public function delete($id = null, $bom_id = null) {
		$this->BomChild->id = $id;
		if (!$this->BomChild->exists()) {
			//throw new NotFoundException(__('Invalid bom child'));
			$this->Session->setFlash(__('The bom child could not be deleted. Please, try again.'), 'error');
		}
		// $this->request->allowMethod('post', 'delete');
		if ($id != null && $bom_id != null) {

			$this->loadModel('BomItem');

			$check = $this->BomItem->find('all', array(
				'conditions' => array(
					'BomItem.bom_child_id' => $id,
					'BomItem.bom_id' => $bom_id
					)
				));
			//if($check) { 
				$this->BomChild->deleteAll(
					array(
						'BomChild.id' => $id,
						'BomChild.bom_id' => $bom_id
						),
					false
					); 

				//if($deletechild) {
					$this->BomItem->deleteAll(
					array(
						'BomItem.bom_child_id' => $id
						),
					false
					);	
					$this->delete_sub_assembly($id, $bom_id);

					$this->insert_log($this->user_id, 'Sub Assembly Deleted', 'boms/view/' . $bom_id, 'BOM Deleted (Sub assembly)');

					$this->Session->setFlash(__('The bom child has been deleted.'), 'success');
					return $this->redirect(array('controller' => 'boms', 'action' => 'bomchild', $bom_id));
				//}
			//} 
		} else {
			$this->Session->setFlash(__('The bom child could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('controller' => 'boms', 'action' => 'bomchild', $bom_id));
	}

	private function delete_sub_assembly($parent_id, $bom_id) {
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.parent_id' => $parent_id,
				'BomChild.bom_id' => $bom_id
				)
			));
		if($childs) {
			foreach ($childs as $child) {
				$this->delete_sub_items($child['BomChild']['id'], $bom_id);
				$this->delete_sub_assembly($child['BomChild']['id'], $bom_id);
			}
		}
	}

	private function delete_sub_items($child_id, $bom_id) {
		$this->loadModel('BomItem');
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $child_id,
				'BomItem.bom_id' => $bom_id
				)
			));
		if($items) {
			$deleteitem = $this->BomItem->deleteAll(
				array(
					'BomItem.bom_child_id' => $child_id,
					'BomItem.bom_id' => $bom_id,
					),
				false
			); 
			return $deleteitem;	
		} 
	}
}
