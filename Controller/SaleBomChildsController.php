<?php
App::uses('AppController', 'Controller');
/**
 * SaleBomChildren Controller
 *
 * @property SaleBomChild $SaleBomChild
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleBomChildsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleBomChild->recursive = 0;
		$this->set('saleBomChildren', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleBomChild->exists($id)) {
			throw new NotFoundException(__('Invalid sale bom child'));
		}
		$options = array('conditions' => array('SaleBomChild.' . $this->SaleBomChild->primaryKey => $id));
		$this->set('saleBomChild', $this->SaleBomChild->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleBomChild->create();
			if ($this->SaleBomChild->save($this->request->data)) {
				$this->Session->setFlash(__('The sale bom child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale bom child could not be saved. Please, try again.'));
			}
		}


		$saleBoms = $this->SaleBomChild->SaleBom->find('list');
		$inventoryItems = $this->SaleBomChild->InventoryItem->find('list');
		$this->set(compact('saleBoms', 'inventoryItems'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleBomChild->exists($id)) {
			throw new NotFoundException(__('Invalid sale bom child'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleBomChild->save($this->request->data)) {
				$this->Session->setFlash(__('The sale bom child has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale bom child could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleBomChild.' . $this->SaleBomChild->primaryKey => $id));
			$this->request->data = $this->SaleBomChild->find('first', $options);
		}

		$child = $this->SaleBomChild->find('first', array(
				'conditions' => array(
					'SaleBomChild.id' => $id
					)
			)); 
		$this->set('child', $child);
		$saleBoms = $this->SaleBomChild->SaleBom->find('list');
		$inventoryItems = $this->SaleBomChild->InventoryItem->find('list');
		$this->set(compact('saleBoms', 'inventoryItems'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleBomChild->id = $id;
		if (!$this->SaleBomChild->exists()) {
			throw new NotFoundException(__('Invalid sale bom child'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleBomChild->delete()) {
			$this->Session->setFlash(__('The sale bom child has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale bom child could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
