<?php
App::uses('AppController', 'Controller');
/**
 * InventorySupplierFiles Controller
 *
 * @property InventorySupplierFile $InventorySupplierFile
 * @property PaginatorComponent $Paginator
 */
class InventorySupplierFilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventorySupplierFile->recursive = 0;
		$this->set('inventorySupplierFiles', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventorySupplierFile->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier file'));
		}
		$options = array('conditions' => array('InventorySupplierFile.' . $this->InventorySupplierFile->primaryKey => $id));
		$this->set('inventorySupplierFile', $this->InventorySupplierFile->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventorySupplierFile->create();
			if ($this->InventorySupplierFile->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier file has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier file could not be saved. Please, try again.'));
			}
		}
		$inventorySuppliers = $this->InventorySupplierFile->InventorySupplier->find('list');
		$this->set(compact('inventorySuppliers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventorySupplierFile->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier file'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventorySupplierFile->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier file has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier file could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventorySupplierFile.' . $this->InventorySupplierFile->primaryKey => $id));
			$this->request->data = $this->InventorySupplierFile->find('first', $options);
		}
		$inventorySuppliers = $this->InventorySupplierFile->InventorySupplier->find('list');
		$this->set(compact('inventorySuppliers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventorySupplierFile->id = $id;
		if (!$this->InventorySupplierFile->exists()) {
			throw new NotFoundException(__('Invalid inventory supplier file'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventorySupplierFile->delete()) {
			$this->Session->setFlash(__('The inventory supplier file has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory supplier file could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
