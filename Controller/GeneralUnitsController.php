<?php
App::uses('AppController', 'Controller');
/**
 * GeneralUnits Controller
 *
 * @property GeneralUnit $GeneralUnit
 * @property PaginatorComponent $Paginator
 */
class GeneralUnitsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->GeneralUnit->recursive = 0;
		$this->set('generalUnits', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->GeneralUnit->exists($id)) {
			throw new NotFoundException(__('Invalid general unit'));
		}
		$options = array('conditions' => array('GeneralUnit.' . $this->GeneralUnit->primaryKey => $id));
		$this->set('generalUnit', $this->GeneralUnit->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->GeneralUnit->create();
			if ($this->GeneralUnit->save($this->request->data)) {
				$this->Session->setFlash(__('The general unit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The general unit could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->GeneralUnit->exists($id)) {
			throw new NotFoundException(__('Invalid general unit'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->GeneralUnit->save($this->request->data)) {
				$this->Session->setFlash(__('The general unit has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The general unit could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GeneralUnit.' . $this->GeneralUnit->primaryKey => $id));
			$this->request->data = $this->GeneralUnit->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->GeneralUnit->id = $id;
		if (!$this->GeneralUnit->exists()) {
			throw new NotFoundException(__('Invalid general unit'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->GeneralUnit->delete()) {
			$this->Session->setFlash(__('The general unit has been deleted.'));
		} else {
			$this->Session->setFlash(__('The general unit could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
