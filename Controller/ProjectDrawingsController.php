<?php
App::uses('AppController', 'Controller');
/**
 * ProjectDrawings Controller
 *
 * @property ProjectDrawing $ProjectDrawing
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectDrawingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');


	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index($stat = null) {
		$conditions = array();

		if($stat == null) {
			$status = 0;
		} else {
			$status = $stat;
		}

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['ProjectDrawing.name LIKE'] = '%'.$_GET['name'].'%';
			}
			if($_GET['sale_job_child_id'] != '') {
				$conditions['OR']['SaleJobChild.name LIKE'] = '%'.$_GET['sale_job_child_id'].'%';
				$conditions['OR']['SaleJobChild.station_name LIKE'] = '%'.$_GET['sale_job_child_id'].'%';
			}

			if($_GET['status'] != '') {
				$conditions['ProjectDrawing.status'] = '%'.$_GET['status'].'%'; 
			}
			$this->request->data = $_GET;
		} else {
			$conditions['ProjectDrawing.id >'] = 0;
		} 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'ProjectDrawing.id DESC',
			'limit'=>$record_per_page
			);  

		$this->set('projectDrawings', $this->Paginator->paginate());
	}

	public function upload($id = null) {
		if (!$this->ProjectDrawing->exists($id)) {
			throw new NotFoundException(__('Invalid project drawing'));
		}

		if ($this->request->is('post')) {
			$this->loadModel('ProjectDrawingFile');
			$this->ProjectDrawingFile->create();
			$json = array();
			$this->request->data['ProjectDrawingFile']['project_drawing_id'] = $id; 
			if($this->ProjectDrawingFile->save($this->request->data)) { 
				$json['response'] = 'Success upload';
			} else {
				$json['response'] = '';
			}
			debug($this->ProjectDrawingFile->save($this->request->data)); 
			 
		}

		$options = array('conditions' => array('ProjectDrawing.' . $this->ProjectDrawing->primaryKey => $id));
		$this->set('projectDrawing', $this->ProjectDrawing->find('first', $options));
	}

	public function ajaxupload($id = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if ($this->request->is('post')) {
			$this->loadModel('ProjectDrawingFile');
			$this->ProjectDrawingFile->create();
			$json = array();
			$this->request->data['ProjectDrawingFile']['project_drawing_id'] = $id; 
			if($this->ProjectDrawingFile->save($this->request->data)) { 
				$json['response'] = 'Success upload';
			} else {
				$json['response'] = '';
			}
			debug($this->ProjectDrawingFile->save($this->request->data)); 
			echo json_encode($json);	
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectDrawing->exists($id)) {
			throw new NotFoundException(__('Invalid project drawing'));
		}
		$options = array('conditions' => array('ProjectDrawing.' . $this->ProjectDrawing->primaryKey => $id));
		$this->set('projectDrawing', $this->ProjectDrawing->find('first', $options));

		
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectDrawing->create();

			$user_id = $this->Session->read('Auth.User.id');

			$this->request->data['ProjectDrawing']['created'] = $this->date;
			$this->request->data['ProjectDrawing']['modified'] = $this->date;
			$this->request->data['ProjectDrawing']['user_id'] = $user_id;
			$this->request->data['ProjectDrawing']['status'] = 'Draft';

			$drawings = $this->ProjectDrawing->find('all', array( 
				'recursive' => -1
				));
			$dr_no = count($drawings) + 1;
			$drawing_no = $this->generate_code('PD', $dr_no);

			$this->request->data['ProjectDrawing']['name'] = $drawing_no; 

			//var_dump(count($count));
			$this->loadModel('ProjectDrawingFile');
			if ($this->ProjectDrawing->save($this->request->data)) {
				$id = $this->ProjectDrawing->getLastInsertId(); 

				$this->ProjectDrawingFile->saveAll($this->request->data, array('deep' => true));

				$this->Session->setFlash(__('The project drawing has been saved.'), 'success');
				return $this->redirect(array('controller' => 'project_drawing_files', 'action' => 'add/'. $id));
			} else { 
				//debug($this->ProjectDrawing->invalidFields());
				$this->Session->setFlash(__('The project drawing could not be saved because Job No not found. Please ask Sales Dept to create Job.'), 'error');
			}
		}
		$saleJobs = $this->ProjectDrawing->SaleJob->find('list');
		$users = $this->ProjectDrawing->User->find('list');
		$this->set(compact('saleJobs', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectDrawing->exists($id)) {
			throw new NotFoundException(__('Invalid project drawing'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectDrawing->save($this->request->data)) {
				$this->Session->setFlash(__('The project drawing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project drawing could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectDrawing.' . $this->ProjectDrawing->primaryKey => $id));
			$this->request->data = $this->ProjectDrawing->find('first', $options);
		}
		$saleJobs = $this->ProjectDrawing->SaleJob->find('list');
		$users = $this->ProjectDrawing->User->find('list');
		$this->set(compact('saleJobs', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectDrawing->id = $id;
		if (!$this->ProjectDrawing->exists()) {
			throw new NotFoundException(__('Invalid project drawing'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectDrawing->delete()) {
			$this->Session->setFlash(__('The project drawing has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project drawing could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
