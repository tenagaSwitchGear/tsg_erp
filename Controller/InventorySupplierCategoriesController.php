<?php
App::uses('AppController', 'Controller');
/**
 * InventorySupplierCategories Controller
 *
 * @property InventorySupplierCategory $InventorySupplierCategory
 * @property PaginatorComponent $Paginator
 */
class InventorySupplierCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventorySupplierCategory->recursive = 0;
		$this->set('inventorySupplierCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventorySupplierCategory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier category'));
		}
		$options = array('conditions' => array('InventorySupplierCategory.' . $this->InventorySupplierCategory->primaryKey => $id));
		$this->set('inventorySupplierCategory', $this->InventorySupplierCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventorySupplierCategory->create();
			if ($this->InventorySupplierCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier category could not be saved. Please, try again.'));
			}
		}
		$inventorySuppliers = $this->InventorySupplierCategory->InventorySupplier->find('list');
		$inventoryItemCategories = $this->InventorySupplierCategory->InventoryItemCategory->find('list');
		$this->set(compact('inventorySuppliers', 'inventoryItemCategories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventorySupplierCategory->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventorySupplierCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventorySupplierCategory.' . $this->InventorySupplierCategory->primaryKey => $id));
			$this->request->data = $this->InventorySupplierCategory->find('first', $options);
		}
		$inventorySuppliers = $this->InventorySupplierCategory->InventorySupplier->find('list');
		$inventoryItemCategories = $this->InventorySupplierCategory->InventoryItemCategory->find('list');
		$this->set(compact('inventorySuppliers', 'inventoryItemCategories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventorySupplierCategory->id = $id;
		if (!$this->InventorySupplierCategory->exists()) {
			throw new NotFoundException(__('Invalid inventory supplier category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventorySupplierCategory->delete()) {
			$this->Session->setFlash(__('The inventory supplier category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory supplier category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
