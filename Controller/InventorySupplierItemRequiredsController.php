<?php
App::uses('AppController', 'Controller');
/**
 * InventorySupplierItemRequireds Controller
 *
 * @property InventorySupplierItemRequired $InventorySupplierItemRequired
 * @property PaginatorComponent $Paginator
 */
class InventorySupplierItemRequiredsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index($stat = null) {
 		$conditions = array();

 		if(isset($_GET['search'])) {
 			if($_GET['name'] != '') {
 				$conditions['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
 			}
 			if($_GET['code'] != '') {
 				$conditions['InventoryItem.code LIKE'] = '%'.$_GET['code'].'%';
 			} 

 			if($_GET['inventory_item_category_id'] != '') {
 				$conditions['InventoryItem.inventory_item_category_id'] = $_GET['inventory_item_category_id'];
 			} 
 		}

 		if($stat == null) {
 			$conditions['InventorySupplierItemRequired.status'] = 0;
 		} else {
 			$conditions['InventorySupplierItemRequired.status'] = 1;
 		}

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order' => 'InventoryItem.code ASC',
			'limit' => 60
			); 

		$this->set('inventorySupplierItemRequireds', $this->Paginator->paginate());

		$this->loadModel('InventoryItemCategory');
		$category = $this->InventoryItemCategory->find('list');
		$this->set(compact('category'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventorySupplierItemRequired->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier item required'));
		}
		$options = array('conditions' => array('InventorySupplierItemRequired.' . $this->InventorySupplierItemRequired->primaryKey => $id));
		$this->set('inventorySupplierItemRequired', $this->InventorySupplierItemRequired->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventorySupplierItemRequired->create();
			if ($this->InventorySupplierItemRequired->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier item required has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier item required could not be saved. Please, try again.'));
			}
		}
		$inventoryItems = $this->InventorySupplierItemRequired->InventoryItem->find('list');
		$users = $this->InventorySupplierItemRequired->User->find('list');
		$this->set(compact('inventoryItems', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventorySupplierItemRequired->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier item required'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventorySupplierItemRequired->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier item required has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier item required could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InventorySupplierItemRequired.' . $this->InventorySupplierItemRequired->primaryKey => $id));
			$this->request->data = $this->InventorySupplierItemRequired->find('first', $options);
		}
		$inventoryItems = $this->InventorySupplierItemRequired->InventoryItem->find('list');
		$users = $this->InventorySupplierItemRequired->User->find('list');
		$this->set(compact('inventoryItems', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventorySupplierItemRequired->id = $id;
		if (!$this->InventorySupplierItemRequired->exists()) {
			throw new NotFoundException(__('Invalid inventory supplier item required'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventorySupplierItemRequired->delete()) {
			$this->Session->setFlash(__('The inventory supplier item required has been deleted.'));
		} else {
			$this->Session->setFlash(__('The inventory supplier item required could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
