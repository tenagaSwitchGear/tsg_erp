<?php
App::uses('AppController', 'Controller'); 
 
class NotificationsController extends AppController {

	public $components = array('RequestHandler', 'Paginator', 'Session');
  

	public function beforeFilter() {
		parent::beforeFilter(); 
	}

	public function admin_index() {
		$conditions = array();
		$user_id = $this->Session->read('Auth.User.id');  
        if(!empty($this->data)) {         
            $this->Session->write('Filter.Notification',$this->data);
        } elseif((!empty($this->passedArgs['page']) || strpos($this->referer(), 'notifications/index')) && $this->Session->check('Filter.Notification')  ) {
            $this->request->data = $this->Session->check('Filter.Notification')? $this->Session->read('Filter.Notification') : '';
        } else {
            $this->Session->delete('Filter.Notification');
        }
        /* End Filter With Paging */
		
		if(!empty($this->data)) {
			if(!empty($this->data['Notification']['username'])) {
				$conditions['Notification.username LIKE']='%'.$this->data['Notification']['username'].'%';
			} 
			if(!empty($this->data['Notification']['email'])) {
				$conditions['Notification.email LIKE']='%'.$this->data['Notification']['email'].'%';
			}
		}	 
		$conditions['Notification.user_id'] = $user_id;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order' => 'Notification.id DESC', 'limit' => $record_per_page);
		$this->set('notifications', $this->Paginator->paginate());
		$this->__update_notifications_status($user_id);
	}

	public function index() {
		$conditions = array();
		$user_id = $this->Session->read('Auth.User.id'); 
		 
        if(!empty($this->data)) {         
            $this->Session->write('Filter.Notification',$this->data);
        } elseif((!empty($this->passedArgs['page']) || strpos($this->referer(), 'notifications/index')) && $this->Session->check('Filter.Notification')) {
            $this->request->data = $this->Session->check('Filter.Notification') ? $this->Session->read('Filter.Notification') : '';
        } else {
            $this->Session->delete('Filter.Notification');
        }
         
		$conditions['Notification.user_id'] = $user_id;

		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'Notification.id DESC','limit'=>$record_per_page);
		$this->set('notifications', $this->Paginator->paginate());

		$this->__update_notifications_status($user_id);
	} 

	public function admin_view($id) {
		$user_id = $this->Session->read('Auth.User.id');
		$conditions = array(); 
		$conditions['conditions']['Notification.id'] = $id;
		$conditions['conditions']['Notification.user_id'] = $user_id;
		$notification = $this->Notification->find('first', $conditions);
		if($notification['Notification']['status'] == 0) {
			$this->__update_notification_status($id);
		}
		$this->set('notification', $notification);
	}

	public function view($id) {
		$user_id = $this->Session->read('Auth.User.id'); 
		$conditions = array();
		$conditions['conditions']['Notification.id'] = $id;
		$conditions['conditions']['Notification.user_id'] = $user_id;
		$notification = $this->Notification->find('first', $conditions);
		if($notification['Notification']['status'] == 0) {
			$this->__update_notification_status($id);
		}
		$this->set('notification', $notification);
	}

	private function __update_notification_status($id) {
		$this->Notification->id = $id;
		return $this->Notification->saveField('status', 1);
	}

	private function __update_notifications_status($user_id) { 
		$update = $this->Notification->updateAll(
		    array('Notification.status' => 1),
		    array('Notification.user_id' => $user_id)
		);
		return $update;
	}

}