<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodDeliveries Controller
 *
 * @property FinishedGoodDelivery $FinishedGoodDelivery
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FinishedGoodDeliveriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodDelivery->recursive = 0;
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => 2
		));
		/*print_r($finishedGoodDelivery);
		exit;
		$item_array = array();
		foreach ($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] as $finished_item) {
			$item_array[] => array(
				'fg_item' => $this->get_fg_item($finished_item['id']),
				'inv_item' => $this->get_inv_item($finished_item['inventory_item_id'])
			);
		}
		print_r($item_array);
		exit;
		$finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] = $item;*/

		//$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
		$this->set('finishedGoodDelivery', $finishedGoodDelivery);
	}
	private function get_fg_item($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$item = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.id' => $id
			),
			'recursive' => -1
		));

		return $item;
	}
	private function get_inv_item($id){
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			),
			'recursive' => -1
		));

		return $item;
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		
		if ($this->request->is('post')) {
			//print_r($_POST);
			//exit;
			$this->request->data['FinishedGoodDelivery']['type'] = 'DR';
			$SO_no = $this->request->data['FinishedGoodDelivery']['findSaleOrder'];
			$this->request->data['FinishedGoodDelivery']['user_id'] = $_SESSION['Auth']['User']['id'];
			$this->request->data['FinishedGoodDelivery']['do_number'] = 0;
			$this->FinishedGoodDelivery->create();
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$id = $this->FinishedGoodDelivery->getInsertID();

				$this->loadModel('FinishedGoodDeliveryItem');

				$sale_order_item_id = $_POST['sale_order_item_id'];
				$inventory_item_id = $_POST['inventory_item_id'];
				$sale_job_id = $_POST['sale_job_id'];
				$sale_job_child_id = $_POST['sale_job_child_id'];
				$quantity = $_POST['quantity_deliver'];
				//$remark = $_POST['remark'];

				for($i=0; $i<count($sale_order_item_id); $i++){
					$this->FinishedGoodDeliveryItem->create();
					$this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'] = $id;
					$this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'] = $sale_order_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'] = $inventory_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_id'] = $sale_job_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'] = $sale_job_child_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['quantity'] = $quantity[$i];
					//$this->request->data['FinishedGoodDeliveryItem']['remark'] = $remark[$i];
					$this->request->data['FinishedGoodDeliveryItem']['user_id'] = $_SESSION['Auth']['User']['id'];

					$this->FinishedGoodDeliveryItem->save($this->request->data);
				}

				$request_id = $id;
	            //echo $request_id;
	            //exit;
	            // Find Approval
	            $group_id =$this->Session->read('Auth.User.group_id');
	            $this->loadModel('Approval');
	            $approvals = $this->Approval->find('all', array(
	                'conditions' => array(
	                    'Approval.name' => 'Delivery Request',
	                    'Approval.group_id' => $group_id
	                    )
	                ));
	            if($approvals) {
	                foreach ($approvals as $approval) {
	                    $data = array(
	                        'to' => $approval['User']['email'],
	                        'template' => 'delivery_request',
	                        'subject' => 'Delivery Request Require Your Approval #' . $SO_no,
	                        'content' => array(
	                            'so_number' => $SO_no,
	                            'from' => $this->Session->read('Auth.User.username'),
	                            'username' => $approval['User']['username'],  
	                            'link' => 'finished_good_deliveries/verifyview/'.$request_id
	                            )
	                        );
	                    //$this->send_email($data);
	                }
	            }

				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

	public function addItem($id = null) {
		$this->loadModel('SaleOrderItem');
		$this->loadModel('FinishedGoodDelivery');
		$this->loadModel('FinishedGoodDeliveryItem');

		if($this->request->is('post')){
			$finished_good_id = $this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'];
			$sale_order_item_id = $this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'];
			$inventory_item_id = $this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'];
			$sale_job_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_id'];
			$sale_job_child_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'];
			$quantity = $this->request->data['FinishedGoodDeliveryItem']['quantity'];

			for($i=0; $i<count($finished_good_id); $i++){
				$this->FinishedGoodDeliveryItem->create();
				$this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'] = $finished_good_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'] = $sale_order_item_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'] = $inventory_item_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['sale_job_id'] = $sale_job_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'] = $sale_job_child_id[$i];
				$this->request->data['FinishedGoodDeliveryItem']['quantity'] = $quantity[$i];
				$this->request->data['FinishedGoodDeliveryItem']['user_id'] = $_SESSION['Auth']['User']['id'];

				$this->FinishedGoodDeliveryItem->save($this->request->data);
			}

			$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
			return $this->redirect(array('action' => 'index'));
		}

		$finished_good = $this->FinishedGoodDelivery->find('all', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));

		$fgood = array(
			'FinishedGood' => $finished_good[0]['FinishedGoodDelivery'],
			'SaleOrder' => $this->get_sale_order($finished_good[0]['FinishedGoodDelivery']['sale_order_id']),
			'SaleOrderItem' => $this->get_sale_order_item($finished_good[0]['FinishedGoodDelivery']['sale_order_id']),
			'SaleJobs' => $this->get_jobs($finished_good[0]['FinishedGoodDelivery']['sale_job_id']),
		);
		$this->set('fgood', $fgood);
	}

	private function get_sale_order($id_sale_order){
		$this->loadModel('SaleOrder');
		$sale_order = $this->SaleOrder->find('all', array(
			'conditions' => array(
				'SaleOrder.id' => $id_sale_order
			),
			'recursive' => -1
		));
		
		return $sale_order[0]['SaleOrder'];
	}

	private function get_sale_order_item($id){
		$this->loadModel('SaleOrderItem');
		$saleorderitem = $this->SaleOrderItem->find('all', array(
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $id
			),
		));
		
		return $saleorderitem;
	}

	private function get_jobs($id){
		$this->loadModel('SaleJob');
		$salejob = $this->SaleJob->find('all', array(
			'conditions' => array(
				'SaleJob.id' => $id
			),
			'recursive' => -1
		));
		
		if(empty($salejob)){
			return $salejob;
		}else{
			return $salejob[0]['SaleJob'];
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$id = $id;

				$this->loadModel('FinishedGoodDeliveryItem');

				$fgitemid = $this->request->data['FinishedGoodDeliveryItem']['id'];
				$sale_order_item_id = $this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'];
				$inventory_item_id = $this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'];
				$sale_job_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_id'];
				$sale_job_child_id = $this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'];
				$quantity = $this->request->data['FinishedGoodDeliveryItem']['quantity'];
				//$remark = $_POST['remark'];

				for($i=0; $i<count($sale_order_item_id); $i++){
					$this->FinishedGoodDeliveryItem->create();
					$this->request->data['FinishedGoodDeliveryItem']['id'] = $fgitemid[$i];
					$this->request->data['FinishedGoodDeliveryItem']['finished_good_delivery_id'] = $id;
					$this->request->data['FinishedGoodDeliveryItem']['sale_order_item_id'] = $sale_order_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['inventory_item_id'] = $inventory_item_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_id'] = $sale_job_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['sale_job_child_id'] = $sale_job_child_id[$i];
					$this->request->data['FinishedGoodDeliveryItem']['quantity'] = $quantity[$i];
					//$this->request->data['FinishedGoodDeliveryItem']['remark'] = $remark[$i];
					$this->request->data['FinishedGoodDeliveryItem']['user_id'] = $_SESSION['Auth']['User']['id'];

					$this->FinishedGoodDeliveryItem->save($this->request->data);
				}
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		} else {
			$fg_array = array();
			$finishedgooddlvry = $this->FinishedGoodDelivery->find('all', array(
				'conditions' => array(
					'FinishedGoodDelivery.id' => $id
				)
			));
			//print_r($finishedgooddlvry);
			//exit;

			$fg_array[] = array(
				'FinishedGoodDelivery' => $finishedgooddlvry[0]['FinishedGoodDelivery'],
				'SaleJob' => $finishedgooddlvry[0]['SaleJob'],
				'SaleOrder' => $finishedgooddlvry[0]['SaleOrder'],
				'SaleOrderItem' => $this->get_soitem($finishedgooddlvry[0]['SaleOrder']['id']),
				'SaleJobChild' => $finishedgooddlvry[0]['SaleJobChild'],
				'Customer' => $finishedgooddlvry[0]['Customer'],
				'SaleQuotation' => $finishedgooddlvry[0]['SaleQuotation'],
				'User' => $finishedgooddlvry[0]['User'],
				'FinishedGoodDeliveryItem' => $this->get_item_fg($finishedgooddlvry[0]['FinishedGoodDelivery']['id'])
			);



			//$this->set('this->request->data', $fg_array);

			//$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
			$this->request->data = $fg_array[0];
		}

		//print_r($fg_array);
		//exit;
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

	private function get_soitem($id){
		$this->loadModel('SaleOrderItem');
		$soitem = $this->SaleOrderItem->find('all', array(
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $id
			),
			'recursive' => -1
		));

		return $soitem;
	}

	private function get_item_fg($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$arr = array();
		$item_fg = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.finished_good_delivery_id' => $id
			)
		));

		foreach ($item_fg as $fitem) {
			$arr[] = array(
				'FinishedGoodDeliveryItem' => $fitem['FinishedGoodDeliveryItem'],
				'InventoryItem' => $this->get_detail($fitem['FinishedGoodDeliveryItem']['inventory_item_id']),
			);
		}
		

		return $arr;
	}

	private function get_detail($id){
		$this->loadModel('InventoryItem');
		$arr_item = array();
		$d_i = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			)
		));

		$arr_item[]=array(
			'Item' => $d_i[0]['InventoryItem'],
			'GeneralUnit' => $d_i[0]['GeneralUnit']
		);

		return $arr_item;

	}

	public function view_approved($id=null) {
		$this->loadModel('DeliveryNote');
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			if ($this->DeliveryNote->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'store'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}
		$finishedGoodDelivery = $this->DeliveryNote->find('first', array(
			'conditions' => array(
				'DeliveryNote.id' => $id
			),
			//'recursive' => -1
		));
		//print_r($finishedGoodDelivery);
		//exit;
		$fg_array = array();
		$fg_array[] = array(
			'DeliveryNote' => $finishedGoodDelivery['DeliveryNote'],
			'SaleJob' => $finishedGoodDelivery['SaleJob'],
			'SaleOrder' => $finishedGoodDelivery['SaleOrder'],
			'Customer' => $finishedGoodDelivery['Customer'],
			'SaleQuotation' => $finishedGoodDelivery['SaleQuotation'],
			'DeliveryNoteItem' => $this->get_dnitem2($finishedGoodDelivery['DeliveryNote']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}
	public function view_rejected($id=null) {
		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}
	public function view_delivered($id=null) {
		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function viewverify($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			if($this->FinishedGoodDelivery->save($this->request->data)){
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'store'));
			}else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function verifyview($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			if($this->FinishedGoodDelivery->save($this->request->data)){
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'dn_index'));
			}else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function verifyview2($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$this->loadModel('DeliveryNote');
			if($this->DeliveryNote->save($this->request->data)){
				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'approved'));
			}else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}
		}

		$this->loadModel('DeliveryNote');
		$deliverynote = $this->DeliveryNote->find('first', array(
			'conditions' => array(
				'DeliveryNote.id' => $id
			)
		));

		//print_r($deliverynote);
		//exit;
		$dn_array = array();
		$dn_array[] = array(
			'DeliveryNote' => $deliverynote['DeliveryNote'],
			'SaleJob' =>$this->get_sj($deliverynote['DeliveryNote']['sale_job_id']),
			'SaleOrder' => $this->get_so($deliverynote['DeliveryNote']['sale_order_id']),
			'Customer' => $this->get_cust($deliverynote['DeliveryNote']['customer_id']),
			'SaleQuotation' => $this->get_sq($deliverynote['DeliveryNote']['sale_quotation_id']),
			'DeliveryNoteItem' => $this->get_dnitem2($deliverynote['DeliveryNote']['id'])
		);
		//print_r($dn_array);
		//exit;
		$this->set('deliverynote', $dn_array);
	}

	public function approval() {
		$this->loadModel('DeliveryNote');
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array(
			'conditions' => array(
				'DeliveryNote.status' => 5,
			),
			'limit' => $record_per_page
		);
        $nfg = $this->Paginator->paginate('DeliveryNote');
        //print_r($nfg);
        //exit;
      
		$this->set('finishedGoodDeliveries', $nfg);
	}

	public function approved() {
		$this->loadModel('DeliveryNote');
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array(
			'conditions' => array(
				'DeliveryNote.status' => 6,
			),
			'limit' => $record_per_page
		);
        $nfg = $this->Paginator->paginate('DeliveryNote');
      
		$this->set('finishedGoodDeliveries', $nfg);
	}

	public function rejected() {
		$conditions['FinishedGoodDelivery.status'] = 5;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'FinishedGoodDelivery.id DESC', 'limit' => $record_per_page);
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

	public function delivered() {
		$conditions['FinishedGoodDelivery.status'] = 8;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'FinishedGoodDelivery.id DESC', 'limit' => $record_per_page);
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

	public function store() {
		//$this->loadModel('DeliveryNote');
		$nfg_array = array();

		$record_per_page = Configure::read('Reading.nodes_per_page');
        $this->Paginator->settings = array(
            'order' => 'FinishedGoodDelivery.id DESC'
        );
        $nfg = $this->Paginator->paginate('FinishedGoodDelivery');
        foreach ($nfg as $nfg) {
            $nfg_array[] = array(
				'FinishedGoodDelivery' => $nfg['FinishedGoodDelivery'],
				'SaleJob' => $nfg['SaleJob'],
				'SaleOrder' => $nfg['SaleOrder'],
				'SaleJobChild' => $nfg['SaleJobChild'],
				'Customer' => $nfg['Customer'],
				'SaleQuotation' => $nfg['SaleQuotation'],
				'FinishedGoodDeliveryItem' => $nfg['FinishedGoodDeliveryItem'],
				'DN' => $this->get_dn($nfg['FinishedGoodDelivery']['id']),
			);
        }

        $this->set('finishedGoodDeliveries', $nfg_array);

	}

	private function get_dn($id){
		$this->loadModel('DeliveryNote');
		$dn_array = array();
		$get_dn = $this->DeliveryNote->find('all', array(
			'conditions' => array(
				'DeliveryNote.finished_good_deliveries_id' => $id
			)
		));

		foreach ($get_dn as $dn) {
			$dn_array[] = array(
				'DeliveryNote' => $dn['DeliveryNote'],
				'DeliveryNoteItem' => $dn['DeliveryNoteItem']
			);
		}

		return $dn_array;
	}

	public function dn_index() {
		$this->loadModel('DeliveryNote');
		//$dn = $this->DeliveryNote->find('all');

		/*$conditions = array(
			'FinishedGoodDelivery.type' => 'DN',
			'FinishedGoodDelivery.status >=' => 6,
			'FinishedGoodDelivery.status <' => 8,
			
		);*/
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array(
			'conditions' => array(
				'DeliveryNote.type' => 'DO'
			),
            'order' => 'DeliveryNote.id DESC',
            'limit' => $record_per_page
        );
        $dn = $this->Paginator->paginate('DeliveryNote');
		//$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'DeliveryNote.id DESC', 'limit' => $record_per_page);
		$this->set('delivery_note', $dn);
		//$log = $this->FinishedGoodDelivery->getDataSource()->getLog(false, false);
		//debug($log);
	}

	public function notes_index() {
		$this->loadModel('DeliveryNote');
		//$dn = $this->DeliveryNote->find('all');

		/*$conditions = array(
			'FinishedGoodDelivery.type' => 'DN',
			'FinishedGoodDelivery.status >=' => 6,
			'FinishedGoodDelivery.status <' => 8,
			
		);*/
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array(
			'joins' => array(
            	array(
                	'table' => 'inventory_suppliers',
                	'alias' => 'inventorysupplier',
                	'type' => 'INNER',
                	'conditions' => array(
                        'inventorysupplier.id = DeliveryNote.customer_id'
                    )
                )
            ), 
			'conditions' => array(
				'DeliveryNote.type' => 'DN'
			),
            'order' => 'DeliveryNote.id DESC',
            'limit' => $record_per_page
        );
        $dn = $this->Paginator->paginate('DeliveryNote');

        if(!empty($dn)){
	        $dn_array = array();
	        $dn_array[] = array(
	        	'DeliveryNote' => $dn[0]['DeliveryNote'],
	        	'DeliveryNoteItem' => $dn[0]['DeliveryNoteItem'],
	        	'Supplier' => $this->get_supplier($dn[0]['DeliveryNote']['customer_id']),
	        );
	    }else{
	    	$dn_array = $dn;
	    }
		//$this->Paginator->settings = array('conditions' => $conditions, 'order' => 'DeliveryNote.id DESC', 'limit' => $record_per_page);
		$this->set('delivery_note', $dn_array);
		//$log = $this->FinishedGoodDelivery->getDataSource()->getLog(false, false);
		//debug($log);
	}

	private function get_supplier($id){
		$this->loadModel('InventorySupplier');
		$supplier = $this->InventorySupplier->find('all', array(
			'conditions' => array(
				'InventorySupplier.id' => $id
			),
			'recursive' => -1
		));

		return $supplier[0];
	}

	public function stockrejected(){
		$this->loadModel('InventoryStockRejected');
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$stocks = array();
		$this->Paginator->settings = array(
            'order' => 'InventoryStockRejected.id DESC',
            'limit' => $record_per_page
        );
        $stockrejected = $this->Paginator->paginate('InventoryStockRejected');

        foreach ($stockrejected as $stock) {
        	$stocks[] = array(
        		'InventoryStockRejected' => $stock['InventoryStockRejected'],
        		'GeneralUnit' => $stock['GeneralUnit'],
        		'InventoryItem' => $stock['InventoryItem'],
        		'InventoryDeliveryOrderItem' => $stock['InventoryDeliveryOrderItem'],
        		'InventoryDeliveryOrder' => $this->get_delivery_order($stock['InventoryDeliveryOrderItem']['inventory_delivery_order_id']),
        	);
        }

       
        $this->set('inventoryStockRejecteds', $stocks);

	}

	private function get_delivery_order($id){
		$this->loadModel('InventoryDeliveryOrder');
		$order_arr = array();
		$order = $this->InventoryDeliveryOrder->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrder.id' => $id
			)
		));

		$order_arr[] = array(
			'InventoryDeliveryOrder' => $order[0]['InventoryDeliveryOrder'],
			'InventorySupplier' => $order[0]['InventorySupplier'],
		);

		return $order_arr[0];
	}

	public function new_notes($id=null){
		if($this->request->is('post')){
			//print_r($_POST);
			//exit;
			$note_item = $_POST['item_id'];

			$this->loadModel('DeliveryNote');
			$this->loadModel('DeliveryNoteItem');
			$this->loadModel('InventoryMaterialHistory');

			$bil = $this->DeliveryNote->find('all', array(
				'order' => 'DeliveryNote.id DESC',
				'recursive' => -1
			));

			if(empty($bil)){
				$note_bil = 1;
			}else{
				$note_bil = $bil[0]['DeliveryNote']['id']+1;
			}

			$this->loadModel('InventoryStockRejected');
			$get_reference = $this->InventoryStockRejected->find('all', array(
				'conditions' => array(
					'InventoryStockRejected.id' => $id,
				)
			));
			//print_r($get_reference);
			$ref_no = $this->get_po($get_reference[0]['InventoryDeliveryOrderItem']['inventory_delivery_order_id']);

			$po_no = $ref_no['InventoryDeliveryOrder']['po_no'];

			$sale_job_id = 0;
			$sale_job_child_id = 0;
			$sale_order_id = 0;
			$sale_quotation_id = 0;
			$finished_good_deliveries_id = 0;
			$reference_no = $po_no;
			$customer_id = $this->request->data['Notes']['supplier_id'];
			$user_id = $_SESSION['Auth']['User']['id'];
			$do_number = $this->generate_code('DN', $note_bil);
			$remark = $this->request->data['Notes']['note'];
			$status = $this->request->data['Notes']['status'];
			$delivery_date = $this->request->data['Notes']['require_date'];
			$type = 'DN';

			$this->DeliveryNote->create();

			$data_notes = array(
				'sale_job_id' 					=> $sale_job_id,
				'sale_job_child_id' 			=> $sale_job_child_id,
				'sale_order_id'					=> $sale_order_id,
				'sale_quotation_id'				=> $sale_quotation_id,
				'finished_good_deliveries_id'	=> $finished_good_deliveries_id,
				'reference_no'					=> $reference_no,
				'customer_id'					=> $customer_id,
				'user_id'						=> $user_id,
				'do_number'						=> $do_number,
				'remark'						=> $remark,
				'status'						=> $status,
				'date_delivered'				=> $delivery_date,
				'type'							=> $type
			);

			if($this->DeliveryNote->save($data_notes)){
				$insert_id = $this->DeliveryNote->getLastInsertId();
				$n_item_array = array();
				for($i=0; $i<count($note_item); $i++){
					$n_item_array[] = array(
						'SupplierItem' => $this->get_item($note_item[$i]),
					);

					$this->DeliveryNoteItem->create();
					$data_note_item = array(
						'finished_good_delivery_id' 		=> 0,
						'finished_good_id' 					=> 0,
						'sale_order_item_id' 				=> 0,
						'sale_quotation_item_id' 			=> 0,
						'inventory_item_id' 				=> $n_item_array[0]['SupplierItem'][0]['InventoryItem']['id'],
						'sale_job_id'						=> 0,
						'sale_job_child_id'					=> 0,
						'production_order_id'				=> 0,	
						'sale_job_item_id'					=> 0,
						'created'							=> date('Y-m-d H:i:s'),
						'user_id' 							=>	$_SESSION['Auth']['User']['id'],
						'type' 								=> 0,
						'quantity' 							=> $_POST['quantity'][$i],
						'delivery_note_id' 					=> $insert_id
					);

					$this->DeliveryNoteItem->save($data_note_item);

					$this->InventoryMaterialHistory->create();

					$data_material_history = array(
						'reference' 						=> 'DN',
						'user_id' 							=> $_SESSION['Auth']['User']['id'],
						'store_pic' 						=> 0,
						'inventory_item_id' 				=> $n_item_array[0]['SupplierItem'][0]['InventoryItem']['id'],
						'inventory_stock_id'				=> 0,
						'quantity' 							=> $_POST['quantity'][$i],
						'general_unit_id' 					=> $n_item_array[0]['SupplierItem'][0]['GeneralUnit']['id'],
						'issued_quantity' 					=> $_POST['quantity'][$i],
						'quantity_balance' 					=> 0,
						'inventory_material_request_id' 	=> 0,
						'inventory_material_request_item_id' => 0,
						'created' 							=> date('Y-m-d H:i:s'),
						'status' 							=> 1,
						'type' 								=> 9,
						'transfer_type' 					=> 'Out'
					);

					$this->InventoryMaterialHistory->save($data_material_history);

				}



				/*foreach ($note_item as $n_item) {
					$n_item_array[] = array(
						'SupplierItem' => $this->get_item($n_item),
					);

					$this->DeliveryNoteItem->create();

					$data_note_item = array(
						'finished_good_delivery_id' 		=> 0,
						'finished_good_id' 					=> 0,
						'sale_order_item_id' 				=> 0,
						'sale_quotation_item_id' 			=> 0,
						'inventory_item_id' 				=> $n_item_array[0]['SupplierItem'][0]['InventoryItem']['id'],
						'sale_job_id'						=> 0,
						'sale_job_child_id'					=> 0,
						'production_order_id'				=> 0,	
						'sale_job_item_id'					=> 0,
						'created'							=> date('Y-m-d H:i:s'),
						'user_id' 							=>	$_SESSION['Auth']['User']['id'],
						'type' 								=> 0,
						'quantity' 							=> $n_item['quantity'],
						'delivery_note_id' 					=> $insert_id
					);

					$this->DeliveryNoteItem->save($data_note_item);
				}*/

				$this->Session->setFlash(__('The delivery notes has been saved.'), 'success');
				return $this->redirect(array('action' => 'notes_index'));
			}else{
				$this->Session->setFlash(__('The delivery notes could not be saved.'), 'error');
			}
		}



		$this->loadModel('InventoryStockRejected');
		if($id){
			$stock = $this->InventoryStockRejected->find('all', array(
				'conditions' => array(
					'InventoryStockRejected.id' => $id
				)
			));
			$stocks = array();

			$stocks[] = array(
        		'InventoryStockRejected' => $stock[0]['InventoryStockRejected'],
        		'GeneralUnit' => $stock[0]['GeneralUnit'],
        		'InventoryItem' => $stock[0]['InventoryItem'],
        		'InventoryDeliveryOrderItem' => $stock[0]['InventoryDeliveryOrderItem'],
        		'InventoryDeliveryOrder' => $this->get_delivery_order($stock[0]['InventoryDeliveryOrderItem']['inventory_delivery_order_id']),
        	);
		}else{
			$stocks = '';
		}

		$this->loadModel("ProductionOrder");
		$this->loadModel("SaleJob");
		$this->loadModel("SaleJobItem");
		$this->loadModel("User");
		$this->loadModel('GeneralUnit');
		$this->loadModel('InventorySupplier');
		$productionOrders = $this->ProductionOrder->find('list');
		$saleJobs = $this->SaleJob->find('list');
		$saleJobItems = $this->SaleJobItem->find('list');
		$suppliers = $this->InventorySupplier->find('list', array('group' => 'InventorySupplier.name'));
		$users = $this->User->find('list');
		$units = $this->GeneralUnit->find('list');
		$this->set(compact('productionOrders', 'saleJobs', 'saleJobItems', 'users', 'units', 'suppliers', 'stocks'));
	}

	private function get_po($id){
		$this->loadModel('InventoryDeliveryOrder');
		$d_order = $this->InventoryDeliveryOrder->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrder.id' => $id
			),
			'recursive' => -1
		));

		return $d_order[0];
	}

	private function get_item($id){
		$this->loadModel('InventorySupplierItem');
		$items = $this->InventorySupplierItem->find('all', array(
			'conditions' => array(
				'InventorySupplierItem.id' => $id
			)
		));

		$item_array = array();
		$item_array[] = array(
			'InventorySupplierItem' => $items[0]['InventorySupplierItem'],
			'InventoryItem' => $items[0]['InventoryItem'],
			'GeneralUnit' => $items[0]['GeneralUnit'],
			'GeneralCurrency' => $items[0]['GeneralCurrency'],

		);

		return $item_array;
	}

	public function view_stockreject($id=null) {
		$this->loadModel('InventoryQcItem');
		$qc_array = array();
		$stock = $this->InventoryQcItem->find('all', array(
			'conditions' => array(
				'InventoryQcItem.id' => $id
			)
		));

		$qc_array[] = array(
			'InventoryItem' => $this->get_inventoryitem($stock[0]['InventoryQcItem']['inventory_item_id']),
			'InventoryQcItem' => $stock[0]['InventoryQcItem'],
			'InventoryQcRemark' => $this->get_qcremark($stock[0]['InventoryQcItem']['id'])
		);
		//print_r($qc_array);
		//exit;
		//$options = array('conditions' => array('InventoryStockRejected.' . $this->InventoryStockRejected->primaryKey => $id));
		$this->set('inventoryStockRejected', $qc_array);

	}

	private function get_inventoryitem($id){
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			),
			'recursive' => -1
		));

		return $item[0]['InventoryItem'];

	}

	private function get_qcremark($id){
		$this->loadModel('InventoryQcRemark');

		$remarks = $this->InventoryQcRemark->find('all', array(
			'conditions' => array(
				'InventoryQcRemark.inventory_qc_item_id' => $id
			),
			'recursive' => -1
		));

		$remark_array = array();
		foreach ($remarks as $remark) {
			$remark_array[] = array(
				'InventoryQcRemark' => $remark['InventoryQcRemark'],
				'Comment' => $this->get_comment($remark['InventoryQcRemark']['finished_good_comment_id'])
			);
		}

		return $remark_array;
	}

	private function get_comment($id){
		$this->loadModel('FinishedGoodComment');
		$comment = $this->FinishedGoodComment->find('first', array(
			'conditions' => array(
				'FinishedGoodComment.id' => $id
			),
			'recursive' => -1
		));

		return $comment['FinishedGoodComment'];

	}

	public function view_dr($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$id = $this->request->data['FinishedGoodDelivery']['id'];
			$fg_id = $this->request->data['FinishedGoodDelivery']['fgid'];
			$remark = $this->request->data['FinishedGoodDelivery']['remark'];
			$status = $this->request->data['FinishedGoodDelivery']['status'];
			$lorry_no = $this->request->data['FinishedGoodDelivery']['lorry_no'];
			$driver_name = $this->request->data['FinishedGoodDelivery']['driver_name'];
			$driver_ic = $this->request->data['FinishedGoodDelivery']['driver_ic'];
			$date_delivered = $this->request->data['FinishedGoodDelivery']['date'];

			$data = $this->FinishedGoodDelivery->query("SELECT id, do_number FROM finished_good_deliveries ORDER BY id DESC");
			if(empty($data)){
                $bil = '1';
            }else{
                $bil = $data[0]['finished_good_deliveries']['id']+1;
            }

			

			if($status != 0){
				$this->FinishedGoodDelivery->create();
				if($data[0]['finished_good_deliveries']['do_number']==0){
					$DO_no = $this->generate_code('DO', $bil);
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}else{
					$DO_no = $data[0]['finished_good_deliveries']['do_number'];
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}

				if($this->FinishedGoodDelivery->save($data_fg)){
					if($status = '3'){
						$request_id = $this->FinishedGoodDelivery->getLastInsertId();
	                    //echo $request_id;
	                    //exit;
	                    // Find Approval
	                    $group_id =$this->Session->read('Auth.User.group_id');
	                    $this->loadModel('Approval');
	                    $approvals = $this->Approval->find('all', array(
	                        'conditions' => array(
	                            'Approval.name' => 'Delivery Note',
	                            'Approval.group_id' => $group_id
	                            )
	                        ));
	                    if($approvals) {
	                        foreach ($approvals as $approval) {
	                            $data = array(
	                                'to' => $approval['User']['email'],
	                                'template' => 'delivery_note',
	                                'subject' => 'Delivery Note Require Your Approval ' . $DO_no,
	                                'content' => array(
	                                    'do_number' => $DO_no,
	                                    'from' => $this->Session->read('Auth.User.username'),
	                                    'username' => $approval['User']['username'],  
	                                    'link' => 'finished_good_delivery/verifyview/'.$request_id
	                                    )
	                                );
	                            $this->send_email($data);
	                        }
	                    }
	                }
					$this->loadModel('FinishedGoodDeliveryItem');
					for($i=0; $i<count($fg_id); $i++){
						$this->FinishedGoodDeliveryItem->create();
						$data_fgitem = array(
						'id' => $fg_id[$i],
						'remark' => $remark[$i]
						);

						$this->FinishedGoodDeliveryItem->save($data_fgitem);
					}

					$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
					return $this->redirect(array('action' => 'store'));

				}else{
					$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
				}
			}
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'SaleOrderItem' => $this->get_soitem($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		/*print_r($finishedGoodDelivery);
		exit;
		$item_array = array();
		foreach ($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] as $finished_item) {
			$item_array[] => array(
				'fg_item' => $this->get_fg_item($finished_item['id']),
				'inv_item' => $this->get_inv_item($finished_item['inventory_item_id'])
			);
		}
		print_r($item_array);
		exit;
		$finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] = $item;*/

		//$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function view_dn($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$id = $this->request->data['FinishedGoodDelivery']['id'];
			$fg_id = $this->request->data['FinishedGoodDelivery']['fgid'];
			$remark = $this->request->data['FinishedGoodDelivery']['remark'];
			$status = $this->request->data['FinishedGoodDelivery']['status'];
			$lorry_no = $this->request->data['FinishedGoodDelivery']['lorry_no'];
			$driver_name = $this->request->data['FinishedGoodDelivery']['driver_name'];
			$driver_ic = $this->request->data['FinishedGoodDelivery']['driver_ic'];
			$date_delivered = $this->request->data['FinishedGoodDelivery']['date'];

			$data = $this->FinishedGoodDelivery->query("SELECT id, do_number FROM finished_good_deliveries ORDER BY id DESC");
			if(empty($data)){
                $bil = '1';
            }else{
                $bil = $data[0]['finished_good_deliveries']['id']+1;
            }

			 

			if($status != 0){
				$this->FinishedGoodDelivery->create();
				if($data[0]['finished_good_deliveries']['do_number']==0){
					$DO_no = $this->generate_code('DO', $bil);
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}else{
					$DO_no = $data[0]['finished_good_deliveries']['do_number'];
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}

				if($this->FinishedGoodDelivery->save($data_fg)){
					if($status = '3'){
						$request_id = $this->FinishedGoodDelivery->getLastInsertId();
	                    //echo $request_id;
	                    //exit;
	                    // Find Approval
	                    $group_id =$this->Session->read('Auth.User.group_id');
	                    $this->loadModel('Approval');
	                    $approvals = $this->Approval->find('all', array(
	                        'conditions' => array(
	                            'Approval.name' => 'Delivery Note',
	                            'Approval.group_id' => $group_id
	                            )
	                        ));
	                    if($approvals) {
	                        foreach ($approvals as $approval) {
	                            $data = array(
	                                'to' => $approval['User']['email'],
	                                'template' => 'delivery_note',
	                                'subject' => 'Delivery Note Require Your Approval ' . $DO_no,
	                                'content' => array(
	                                    'do_number' => $DO_no,
	                                    'from' => $this->Session->read('Auth.User.username'),
	                                    'username' => $approval['User']['username'],  
	                                    'link' => 'finished_good_delivery/verifyview/'.$request_id
	                                    )
	                                );
	                            $this->send_email($data);
	                        }
	                    }
	                }
					$this->loadModel('FinishedGoodDeliveryItem');
					for($i=0; $i<count($fg_id); $i++){
						$this->FinishedGoodDeliveryItem->create();
						$data_fgitem = array(
						'id' => $fg_id[$i],
						'remark' => $remark[$i]
						);

						$this->FinishedGoodDeliveryItem->save($data_fgitem);
					}

					$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
					return $this->redirect(array('action' => 'store'));

				}else{
					$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
				}
			}
		}
		$this->loadModel('DeliveryNote');
		$deliverynote = $this->DeliveryNote->find('first', array(
			'conditions' => array(
				'DeliveryNote.id' => $id
			),
			'recursive' => -1
		));
		//print_r($deliverynote);
		//exit;
		$fg_array = array();
		$fg_array[] = array(
			'DeliveryNote' => $deliverynote['DeliveryNote'],
			'SaleJob' =>$this->get_sj($deliverynote['DeliveryNote']['sale_job_id']),
			'SaleOrder' => $this->get_so($deliverynote['DeliveryNote']['sale_order_id']),
			'SaleOrderItem' => $this->get_soitem($deliverynote['DeliveryNote']['sale_order_id']),
			'Customer' => $this->get_cust($deliverynote['DeliveryNote']['customer_id']),
			'SaleQuotation' => $this->get_sq($deliverynote['DeliveryNote']['sale_quotation_id']),
			'DeliveryNoteItem' => $this->get_dnitem2($deliverynote['DeliveryNote']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function view_notes($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$id = $this->request->data['FinishedGoodDelivery']['id'];
			$fg_id = $this->request->data['FinishedGoodDelivery']['fgid'];
			$remark = $this->request->data['FinishedGoodDelivery']['remark'];
			$status = $this->request->data['FinishedGoodDelivery']['status'];
			$lorry_no = $this->request->data['FinishedGoodDelivery']['lorry_no'];
			$driver_name = $this->request->data['FinishedGoodDelivery']['driver_name'];
			$driver_ic = $this->request->data['FinishedGoodDelivery']['driver_ic'];
			$date_delivered = $this->request->data['FinishedGoodDelivery']['date'];

			$data = $this->FinishedGoodDelivery->query("SELECT id, do_number FROM finished_good_deliveries ORDER BY id DESC");
			if(empty($data)){
                $bil = '1';
            }else{
                $bil = $data[0]['finished_good_deliveries']['id']+1;
            }

			

			if($status != 0){
				$this->FinishedGoodDelivery->create();
				if($data[0]['finished_good_deliveries']['do_number']==0){
					$DO_no = $this->generate_code('DO', $bil);
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}else{
					$DO_no = $data[0]['finished_good_deliveries']['do_number'];
					$data_fg = array(
						'id' => $id,
						'status' => $status,
						'do_number' => $DO_no,
						'lorry_no' => $lorry_no,
						'driver_name' => $driver_name,
						'driver_ic' => $driver_ic,
						'date_delivered' => $date_delivered
					);
				}

				if($this->FinishedGoodDelivery->save($data_fg)){
					if($status = '3'){
						$request_id = $this->FinishedGoodDelivery->getLastInsertId();
	                    //echo $request_id;
	                    //exit;
	                    // Find Approval
	                    $group_id =$this->Session->read('Auth.User.group_id');
	                    $this->loadModel('Approval');
	                    $approvals = $this->Approval->find('all', array(
	                        'conditions' => array(
	                            'Approval.name' => 'Delivery Note',
	                            'Approval.group_id' => $group_id
	                            )
	                        ));
	                    if($approvals) {
	                        foreach ($approvals as $approval) {
	                            $data = array(
	                                'to' => $approval['User']['email'],
	                                'template' => 'delivery_note',
	                                'subject' => 'Delivery Note Require Your Approval ' . $DO_no,
	                                'content' => array(
	                                    'do_number' => $DO_no,
	                                    'from' => $this->Session->read('Auth.User.username'),
	                                    'username' => $approval['User']['username'],  
	                                    'link' => 'finished_good_delivery/verifyview/'.$request_id
	                                    )
	                                );
	                            $this->send_email($data);
	                        }
	                    }
	                }
					$this->loadModel('FinishedGoodDeliveryItem');
					for($i=0; $i<count($fg_id); $i++){
						$this->FinishedGoodDeliveryItem->create();
						$data_fgitem = array(
						'id' => $fg_id[$i],
						'remark' => $remark[$i]
						);

						$this->FinishedGoodDeliveryItem->save($data_fgitem);
					}

					$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
					return $this->redirect(array('action' => 'store'));

				}else{
					$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
				}
			}
		}
		$this->loadModel('DeliveryNote');
		$deliverynote = $this->DeliveryNote->find('first', array(
			'conditions' => array(
				'DeliveryNote.id' => $id
			),
			'recursive' => -1
		));
		//print_r($deliverynote);
		//exit;
		$fg_array = array();
		$fg_array[] = array(
			'DeliveryNote' => $deliverynote['DeliveryNote'],
			'SaleJob' =>$this->get_sj($deliverynote['DeliveryNote']['sale_job_id']),
			'SaleOrder' => $this->get_so($deliverynote['DeliveryNote']['sale_order_id']),
			'SaleOrderItem' => $this->get_soitem($deliverynote['DeliveryNote']['sale_order_id']),
			'Supplier' => $this->get_supplier($deliverynote['DeliveryNote']['customer_id']),
			'SaleQuotation' => $this->get_sq($deliverynote['DeliveryNote']['sale_quotation_id']),
			'DeliveryNoteItem' => $this->get_dnitem2($deliverynote['DeliveryNote']['id'])
		);
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	public function create_do($id=null) {
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$get_fg = $this->FinishedGoodDelivery->find('first', array(
				'conditions' => array(
					'FinishedGoodDelivery.id' => $this->request->data['DeliveryNote']['id']
				)
			));
			//print_r($get_fg);
			//exit;
			$this->loadModel('DeliveryNote');
			$data_dn = $this->DeliveryNote->find('all', array(
				'order' => 'DeliveryNote.id DESC'
				)
			);
			
			if(empty($data_dn)){
                $bil = '1';
            }else{
                $bil = $data_dn[0]['DeliveryNote']['id']+1;
            }	

            $DO_no = $this->generate_code('DO', $bil);

			$finished_good_delivery = $get_fg['FinishedGoodDelivery'];

			$finished_good_delivery_item = $get_fg['FinishedGoodDeliveryItem'];
			//print_r($finished_good_delivery_item);
			//exit;
			$this->DeliveryNote->create();
			$data_delivery_note = array(
				'sale_job_id'					=> $finished_good_delivery['sale_job_id'],
				'sale_job_child_id'				=> $finished_good_delivery['sale_job_child_id'],
				'sale_order_id'					=> $finished_good_delivery['sale_order_id'],
				'customer_id'					=> $finished_good_delivery['customer_id'],
				'sale_quotation_id'				=> $finished_good_delivery['sale_quotation_id'],
				'user_id'						=> $_SESSION['Auth']['User']['id'],
				'do_number'						=> $DO_no,
				'remark'						=> $this->request->data['DeliveryNote']['notes'],
				'status'						=> $this->request->data['DeliveryNote']['status'],
				'delivery_date'					=> $finished_good_delivery['delivery_date'],
				'finished_good_deliveries_id'	=> $finished_good_delivery['id'],
				'reference_no'					=> $finished_good_delivery['reference_no'],
				'type' 							=> 'DO'
			);

			if($this->DeliveryNote->save($data_delivery_note)){
				$id = $this->DeliveryNote->getLastInsertId();
				$this->loadModel('DeliveryNoteItem');
				//print_r($finished_good_delivery_item);

				for($i=0; $i<count($this->request->data['DeliveryNote']['fgitemid']); $i++){
					$data_dn_item = $this->get_fgitem_dn($this->request->data['DeliveryNote']['fgitemid'][$i]);
					//print_r($data_dn_item);

					$this->DeliveryNoteItem->create();
					$data_dn_item_fg = array(
			            'finished_good_delivery_id' => $data_dn_item['FinishedGoodDeliveryItem']['finished_good_delivery_id'],
			            'finished_good_id' => $data_dn_item['FinishedGoodDeliveryItem']['finished_good_id'],
			            'sale_order_item_id' => $data_dn_item['FinishedGoodDeliveryItem']['sale_order_item_id'],
			            'sale_quotation_item_id' => $data_dn_item['FinishedGoodDeliveryItem']['sale_quotation_item_id'],
			            'inventory_item_id' => $data_dn_item['FinishedGoodDeliveryItem']['inventory_item_id'],
			            'sale_job_id' => $data_dn_item['FinishedGoodDeliveryItem']['sale_job_id'],
			            'sale_job_child_id' => $data_dn_item['FinishedGoodDeliveryItem']['sale_job_child_id'],
			            'production_order_id' => $data_dn_item['FinishedGoodDeliveryItem']['production_order_id'],
			            'sale_job_item_id' => $data_dn_item['FinishedGoodDeliveryItem']['sale_job_item_id'],
			            'remark' => $this->request->data['DeliveryNote']['remark'][$i],
			            'user_id' => $_SESSION['Auth']['User']['id'],
			            'quantity' => $this->request->data['DeliveryNote']['quantity'][$i],
			            'delivery_note_id' => $id
		            );
		            //print_r($data_dn_item_fg);
		            $this->DeliveryNoteItem->save($data_dn_item_fg);
				}

				if($this->request->data['DeliveryNote']['status'] == '5'){
					$request_id = $id;
	                //echo $request_id;
	                //exit;
	                // Find Approval
	                $group_id =$this->Session->read('Auth.User.group_id');
	                $this->loadModel('Approval');
	                $approvals = $this->Approval->find('all', array(
	                    'conditions' => array(
	                        'Approval.name' => 'Delivery Order',
	                        'Approval.group_id' => $group_id
	                        )
	                    ));
	                if($approvals) {
	                    foreach ($approvals as $approval) {
	                        $data = array(
	                            'to' => $approval['User']['email'],
	                            'template' => 'delivery_order',
	                            'subject' => 'Delivery Order Require Your Approval ' . $DO_no,
	                            'content' => array(
	                                'do_number' => $DO_no,
	                                'from' => $this->Session->read('Auth.User.username'),
	                                'username' => $approval['User']['username'],  
	                                'link' => 'finished_good_deliveries/verifyview2/'.$request_id
	                                )
	                            );
	                        $this->send_email($data);
	                    }
	                }	
				}
				//exit;

				$this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'dn_index'));
			}else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}

			/*print_r($data_dn_item);

			//print_r($get_fg);
			exit;
			$id = $this->request->data['FinishedGoodDelivery']['id'];
			
			$status = $this->request->data['FinishedGoodDelivery']['status'];

			$data = $this->FinishedGoodDelivery->find('all', array(
				'conditions' => array(
					'FinishedGoodDelivery.id' => $id 
				)
			));

			$data_2 = $this->FinishedGoodDelivery->find('all', array(
				'conditions' => array(
					'FinishedGoodDelivery.type' => 'DN' 
				),
				'recursive' => -1
			));

			if(empty($data_2)){
                $bil = '1';
            }else{
                $bil = $data_2[0]['FinishedGoodDelivery']['id']+1;
            }	

			$DO_no = $this->generate_code('DO', $bil);

			$sale_job_id = $data[0]['FinishedGoodDelivery']['sale_job_id'];
			$sale_job_child_id = $data[0]['FinishedGoodDelivery']['sale_job_child_id'];
            $sale_order_id = $data[0]['FinishedGoodDelivery']['sale_order_id'];
            $customer_id = $data[0]['FinishedGoodDelivery']['customer_id'];
			$sale_quotation_id = $data[0]['FinishedGoodDelivery']['sale_quotation_id'];         
			$user_id = $data[0]['FinishedGoodDelivery']['user_id'];
			$do_number = $DO_no;
			$remark = $data[0]['FinishedGoodDelivery']['remark'];
			$status = $status;
			$delivery_date = $data[0]['FinishedGoodDelivery']['delivery_date'];
			$type = 'DN';
			$dr_id = $id;

			$data_fg_new = array(
				'sale_job_id' => $sale_job_id,
				'sale_job_child_id' => $sale_job_child_id,
				'sale_order_id' => $sale_order_id,
				'customer_id' => $customer_id,
				'sale_quotation_id' => $sale_quotation_id,
				'user_id' => $user_id,
				'do_number' => $do_number,
				'remark' => $remark,
				'status' => $status,
				'delivery_date' => $delivery_date,
				'type' => $type,
				'dr_id' => $id
			);
			
            $this->FinishedGoodDelivery->create();

            if($this->FinishedGoodDelivery->save($data_fg_new)){
				$id = $this->FinishedGoodDelivery->getLastInsertId();

				$fg_id = $this->request->data['FinishedGoodDelivery']['fgid'];
				$quantity = $this->request->data['FinishedGoodDelivery']['quantity'];
				$remark = $this->request->data['FinishedGoodDelivery']['remark'];

				$this->loadModel('FinishedGoodDeliveryItem');
				for($i=0; $i<count($fg_id); $i++){
					$this->FinishedGoodDeliveryItem->create();
					$data_fgitem = array(
						'id' => $fg_id[$i],
						'delivery_order_id' => $id,
						'quantity_delivered' => $quantity[$i],
						'remark' => $remark[$i]
					);

					$this->FinishedGoodDeliveryItem->save($data_fgitem);
				}			

				$request_id = $id;
                //echo $request_id;
                //exit;
                // Find Approval
                $group_id =$this->Session->read('Auth.User.group_id');
                $this->loadModel('Approval');
                $approvals = $this->Approval->find('all', array(
                    'conditions' => array(
                        'Approval.name' => 'Delivery Order',
                        'Approval.group_id' => $group_id
                        )
                    ));
                if($approvals) {
                    foreach ($approvals as $approval) {
                        $data = array(
                            'to' => $approval['User']['email'],
                            'template' => 'delivery_order',
                            'subject' => 'Delivery Order Require Your Approval ' . $DO_no,
                            'content' => array(
                                'do_number' => $DO_no,
                                'from' => $this->Session->read('Auth.User.username'),
                                'username' => $approval['User']['username'],  
                                'link' => 'finished_good_deliveries/verifyview2/'.$request_id
                                )
                            );
                        $this->send_email($data);
                    }
                }	

                $this->Session->setFlash(__('The finished good delivery has been saved.'), 'success');
				return $this->redirect(array('action' => 'dn_index'));	

            }else{
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'), 'error');
			}*/
		}

		$finishedGoodDelivery = $this->FinishedGoodDelivery->find('first', array(
			'conditions' => array(
				'FinishedGoodDelivery.id' => $id
			),
			'recursive' => -1
		));
		$fg_array = array();
		$fg_array[] = array(
			'FinishedGoodDelivery' => $finishedGoodDelivery['FinishedGoodDelivery'],
			'SaleJob' =>$this->get_sj($finishedGoodDelivery['FinishedGoodDelivery']['sale_job_id']),
			'SaleOrder' => $this->get_so($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'SaleOrderItem' => $this->get_soitem($finishedGoodDelivery['FinishedGoodDelivery']['sale_order_id']),
			'Customer' => $this->get_cust($finishedGoodDelivery['FinishedGoodDelivery']['customer_id']),
			'SaleQuotation' => $this->get_sq($finishedGoodDelivery['FinishedGoodDelivery']['sale_quotation_id']),
			'FinishedGoodDeliveryItem' => $this->get_fgitem($finishedGoodDelivery['FinishedGoodDelivery']['id']),
			'DN' => $this->get_delivery_note($finishedGoodDelivery['FinishedGoodDelivery']['id'])
		);
		
		
		$this->set('finishedGoodDelivery', $fg_array);
	}

	private function get_delivery_note($id){
		$this->loadModel('DeliveryNote');
		$dn = $this->DeliveryNote->find('all', array(
			'conditions' => array(
				'DeliveryNote.finished_good_deliveries_id' => $id
			)
		));
		if(!$dn){
			return $dn;
		}else{
			return $dn[0];
		}
	}

	private function get_fgitem_dn($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$fgitem_dn = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.id' => $id
			),
			'recursive' => -1
		));

		return $fgitem_dn[0];
	}

	private function get_sj($id){
		$this->loadModel('SaleJob');
		$salejob = $this->SaleJob->find('all', array(
			'conditions' => array(
				'SaleJob.id' => $id
			),
			'recursive' => -1
		));
		if(empty($salejob)){
			return $salejob;
		}else{
			return $salejob[0]['SaleJob'];
		}
		
	}

	private function get_so($id){
		$this->loadModel('SaleOrder');
		$saleorder = $this->SaleOrder->find('all', array(
			'conditions' => array(
				'SaleOrder.id' => $id
			),
			'recursive' => -1
		));

		
		if(empty($saleorder)){
			return $saleorder;
		}else{
			$array[] = array(
				'SaleOrder' => $saleorder[0]['SaleOrder'],
				'Term Of Payment' => $this->get_term_payment($saleorder[0]['SaleOrder']['term_of_payment_id'])
			);
			return $array[0];
		}
	}

	private function get_term_payment($id){
		$this->loadModel('TermOfPayment');
		$term = $this->TermOfPayment->find('all', array(
			'conditions' => array(
				'TermOfPayment.id' => $id
			),
			'recursive' => -1
		));

		if(empty($term)){
			return $term;
		}else{
			return $term[0]['TermOfPayment'];
		}
	}

	private function get_cust($id){
		$this->loadModel('Customer');
		$customer = $this->Customer->find('all', array(
			'conditions' => array(
				'Customer.id' => $id
			),
			'recursive' => -1
		));

		return $customer[0]['Customer'];
	}

	private function get_sq($id){
		$this->loadModel('SaleQuotation');
		$salequotation = $this->SaleQuotation->find('all', array(
			'conditions' => array(
				'SaleQuotation.id' => $id
			),
			'recursive' => -1
		));

		if(empty($salequotation)){
			return $salequotation;
		}else{
			return $salequotation[0]['SaleQuotation'];
		}
	}

	private function get_fgitem($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$fg_item = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.finished_good_delivery_id' => $id
			)
		));
		$item_array = array();
		foreach ($fg_item as $item) {
			$item_array[] = array(
				'FinishedGoodDeliveryItem' => $item['FinishedGoodDeliveryItem'],
				'InventoryItem' => $this->get_fg_item_detail($item['FinishedGoodDeliveryItem']['inventory_item_id'])
			);
		}

		return $item_array;
	}

	private function get_fgitem2($id){
		$this->loadModel('FinishedGoodDeliveryItem');
		$fg_item = $this->FinishedGoodDeliveryItem->find('all', array(
			'conditions' => array(
				'FinishedGoodDeliveryItem.delivery_order_id' => $id
			)
		));
		$item_array = array();
		foreach ($fg_item as $item) {
			$item_array[] = array(
				'FinishedGoodDeliveryItem' => $item['FinishedGoodDeliveryItem'],
				'InventoryItem' => $this->get_fg_item_detail($item['FinishedGoodDeliveryItem']['inventory_item_id'])
			);
		}

		return $item_array;
	}

	private function get_dnitem2($id){
		$this->loadModel('DeliveryNoteItem');
		$dn_item_array = array();
		$dn_item = $this->DeliveryNoteItem->find('all', array(
			'conditions' => array(
				'DeliveryNoteItem.delivery_note_id' => $id
			)
		));

		foreach ($dn_item as $dnitem) {
			$dn_item_array[] = array(
				'DeliveryNoteItem' 	=> $dnitem['DeliveryNoteItem'],
				'DeliveryNote'		=> $dnitem['DeliveryNote'],
				'InventoryItem'		=> $dnitem['InventoryItem'],
				'GeneralUnit' 		=> $this->get_gu($dnitem['InventoryItem']['general_unit_id']),
				'SaleOrderItem'		=> $dnitem['SaleOrderItem']
			);
		}
		/*$item_array = array();
		foreach ($dn_item as $item) {
			$item_array[] = array(
				'DeliveryNote' => $item['FinishedGoodDeliveryItem'],
				'DeliveryNoteItam' => $this->get_fg_item_detail($item['FinishedGoodDeliveryItem']['inventory_item_id'])
			);
		}*/

		return $dn_item_array;
	}

	private function get_gu($id){
		$this->loadModel('GeneralUnit');
		$g_unit = $this->GeneralUnit->find('first', array(
			'conditions' => array(
				'GeneralUnit.id' => $id
			),
			'recursive' => -1
		));

		return $g_unit['GeneralUnit'];
	}

	private function get_fg_item_detail($id){
		$this->loadModel('InventoryItem');
		$detail_array = array();
		$inv_items = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			)
		));

		$detail_array[] = array(
			'InventoryItem' => $inv_items[0]['InventoryItem'],
			'GeneralUnit' => $inv_items[0]['GeneralUnit']
		);
		return $detail_array;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodDelivery->id = $id;
		if (!$this->FinishedGoodDelivery->exists()) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodDelivery->delete()) {
			$this->Session->setFlash(__('The finished good delivery has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The finished good delivery could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->FinishedGoodDelivery->recursive = 0;
		$this->set('finishedGoodDeliveries', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
		$this->set('finishedGoodDelivery', $this->FinishedGoodDelivery->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodDelivery->create();
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'));
			}
		}
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->FinishedGoodDelivery->exists($id)) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodDelivery->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good delivery has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good delivery could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodDelivery.' . $this->FinishedGoodDelivery->primaryKey => $id));
			$this->request->data = $this->FinishedGoodDelivery->find('first', $options);
		}
		$saleJobs = $this->FinishedGoodDelivery->SaleJob->find('list');
		$saleOrders = $this->FinishedGoodDelivery->SaleOrder->find('list');
		$customers = $this->FinishedGoodDelivery->Customer->find('list');
		$saleQuotations = $this->FinishedGoodDelivery->SaleQuotation->find('list');
		$users = $this->FinishedGoodDelivery->User->find('list');
		$this->set(compact('saleJobs', 'saleOrders', 'customers', 'saleQuotations', 'users'));
	}

	public function setStatus() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		$this->loadModel('InventoryStockRejected');
		$pk = mysql_real_escape_string($_POST['pk']);
        $value = mysql_real_escape_string($_POST['value']);
		
		if($value != '') {
			/*$this->FinishedGood->updateAll(
			array('FinishedGood.status' => $value),
			array('FinishedGood.id' => $pk)
			);*/
			
			$this->InventoryStockRejected->query('UPDATE `inventory_stock_rejecteds` SET `status` = "'.$value.'" WHERE `id` = "'.$pk.'"');

					
		} else {
			header('HTTP 400 Bad Request', true, 400);
			echo "This field is required!";		
		}
		
	}

	public function getStatus()
	{
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$statuses = array();
		
		$statuses[] = array('value' => '0', 'text' => 'Submit to Store');
		$statuses[] = array('value' => '1', 'text' => 'Delivery Notes');
		$statuses[] = array('value' => '2', 'text' => 'Scrap');
		echo json_encode($statuses);
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->FinishedGoodDelivery->id = $id;
		if (!$this->FinishedGoodDelivery->exists()) {
			throw new NotFoundException(__('Invalid finished good delivery'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodDelivery->delete()) {
			$this->Session->setFlash(__('The finished good delivery has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good delivery could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function view_pdf($id = null) {
	    //$this->request->id = $id;
	    //if (!$this->FinishedGoodDelivery->exists($id)) {
	    //    throw new NotFoundException(__('Invalid finished good id'));
	    //}
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printDO';  
	        $this->loadModel('DeliveryNote');
	        
	        $finishedGoodDelivery = $this->DeliveryNote->find('first', array(
				'conditions' => array(
					'DeliveryNote.id' => $id
				),
				'recursive' => -1
			));
			$fg_array = array();
			$fg_array[] = array(
				'DeliveryNote' => $finishedGoodDelivery['DeliveryNote'],
				'SaleJob' =>$this->get_sj($finishedGoodDelivery['DeliveryNote']['sale_job_id']),
				'SaleOrder' => $this->get_so($finishedGoodDelivery['DeliveryNote']['sale_order_id']),
				'Customer' => $this->get_cust($finishedGoodDelivery['DeliveryNote']['customer_id']),
				'SaleQuotation' => $this->get_sq($finishedGoodDelivery['DeliveryNote']['sale_quotation_id']),
				'DeliveryNoteItem' => $this->get_dnitem2($finishedGoodDelivery['DeliveryNote']['id'])
			);

	        $this->set('finished_good_deliveries', $fg_array);
	}

	public function view_do($id = null) {
	    //$this->request->id = $id;
	    $this->loadModel('DeliveryNote');
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printDOorder';  
	        
	        
	        $finishedGoodDelivery = $this->DeliveryNote->find('first', array(
				'conditions' => array(
					'DeliveryNote.id' => $id
				),
				'recursive' => -1
			));
			$fg_array = array();
			$fg_array[] = array(
				'DeliveryNote' => $finishedGoodDelivery['DeliveryNote'],
				'SaleJob' =>$this->get_sj($finishedGoodDelivery['DeliveryNote']['sale_job_id']),
				'SaleOrder' => $this->get_so($finishedGoodDelivery['DeliveryNote']['sale_order_id']),
				'Customer' => $this->get_cust($finishedGoodDelivery['DeliveryNote']['customer_id']),
				'SaleQuotation' => $this->get_sq($finishedGoodDelivery['DeliveryNote']['sale_quotation_id']),
				'DeliveryNoteItem' => $this->get_dnitem2($finishedGoodDelivery['DeliveryNote']['id'])
			);

	        $this->set('finished_good_deliveries', $fg_array);
	}

	public function view_invoice($id = null) {
	    //$this->request->id = $id;
	    $this->loadModel('DeliveryNote');
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printInvoice';  
	        
	        
	        $finishedGoodDelivery = $this->DeliveryNote->find('first', array(
				'conditions' => array(
					'DeliveryNote.id' => $id
				),
				'recursive' => -1
			));
			$fg_array = array();
			$fg_array[] = array(
				'DeliveryNote' => $finishedGoodDelivery['DeliveryNote'],
				'SaleJob' =>$this->get_sj($finishedGoodDelivery['DeliveryNote']['sale_job_id']),
				'SaleOrder' => $this->get_so($finishedGoodDelivery['DeliveryNote']['sale_order_id']),
				'Customer' => $this->get_cust($finishedGoodDelivery['DeliveryNote']['customer_id']),
				'SaleQuotation' => $this->get_sq($finishedGoodDelivery['DeliveryNote']['sale_quotation_id']),
				'DeliveryNoteItem' => $this->get_dnitem2($finishedGoodDelivery['DeliveryNote']['id'])
			);

	        $this->set('finished_good_deliveries', $fg_array);
	}
}
