<?php
App::uses('AppController', 'Controller');
/**
 * InternalLofas Controller
 *
 * @property InternalLofa $InternalLofa
 * @property PaginatorComponent $Paginator
 */
class InternalLofasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InternalLofa->recursive = 0;
		$this->set('internalLofas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InternalLofa->exists($id)) {
			throw new NotFoundException(__('Invalid internal lofa'));
		}
		$options = array('conditions' => array('InternalLofa.' . $this->InternalLofa->primaryKey => $id));
		$this->set('internalLofa', $this->InternalLofa->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InternalLofa->create();
			$this->request->data['InternalLofa']['year'] = date('Y');
			if ($this->InternalLofa->save($this->request->data)) {
				$this->Session->setFlash(__('The internal lofa has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The internal lofa could not be saved. Please, try again.'), 'error');
			}
		}
		
		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InternalLofa->exists($id)) {
			throw new NotFoundException(__('Invalid internal lofa'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InternalLofa->save($this->request->data)) {
				$this->Session->setFlash(__('The internal lofa has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The internal lofa could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('InternalLofa.' . $this->InternalLofa->primaryKey => $id));
			$this->request->data = $this->InternalLofa->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InternalLofa->id = $id;
		if (!$this->InternalLofa->exists()) {
			throw new NotFoundException(__('Invalid internal lofa'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InternalLofa->delete()) {
			$this->Session->setFlash(__('The internal lofa has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The internal lofa could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}