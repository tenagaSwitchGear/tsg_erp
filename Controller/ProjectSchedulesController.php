<?php
App::uses('AppController', 'Controller');
/**
 * ProjectSchedules Controller
 *
 * @property ProjectSchedule $ProjectSchedule
 * @property PaginatorComponent $Paginator
 */
class ProjectSchedulesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxdate');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectSchedule->recursive = 0;
		$this->set('projectSchedules', $this->Paginator->paginate());

		$date = $this->get_job_fat_delivery();
		$this->set('date', $date);
	}

	private function get_job_fat_delivery() {
		$conditions = array(); 
		$this->loadModel('SaleJobChild');
		$dates = $this->SaleJobChild->find('all', array(
			'ORDER' => array('SaleJobChild.date_fat' => 'ASC')
			));
		$json = array();
		$i =  0;
	 
		foreach ($dates as $date) {
			$json['success'] = 1;
			$json['result'][] = array(
				'id' => $i,
                'title' => $date['SaleJobChild']['name'] . ' Job: ' . $date['SaleJob']['name'] != null ? $date['SaleJob']['name'] : 'NA',
                'url' => $date['SaleJob']['name'] != null ? $date['SaleJob']['name'] : 'NA',
                'class' => 'event-warning',
                'start' => strtotime($date['SaleJobChild']['fat_date'])  * 1000,
                'end' => strtotime($date['SaleJobChild']['delivery_date'])  * 1000,
				);
			$i += 1;
		}
		return json_encode($json);
	}

	public function ajaxdate() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$conditions = array(); 
		$this->loadModel('SaleJobChild');
		$dates = $this->SaleJobChild->find('all', array(
			'ORDER' => array('SaleJobChild.date_fat' => 'ASC')
			));
		$json = array();
		$i =  0;
		foreach ($dates as $date) {
			$json['success'] = 1;
			$json['result'][] = array(
				'id' => $i,
                'title' => 'FAT - ' . $date['SaleJobChild']['name'],
                'url' => BASE_URL . 'project_schedules/index#',
                'link' => BASE_URL . 'sale_jobs/view/' . $date['SaleJob']['id'],
                'class' => 'event-warning',
                'start' => strtotime($date['SaleJobChild']['fat_date']) * 1000
				);

			$json['result'][] = array(
				'id' => $i,
                'title' => 'DO - ' . $date['SaleJobChild']['name'],
                'url' => BASE_URL . 'project_schedules/index#',
                'link' => BASE_URL . 'sale_jobs/view/' . $date['SaleJob']['id'],
                'class' => 'event-success',
                'start' => strtotime($date['SaleJobChild']['delivery_date']) * 1000
				);

			$i += 1;
		}
		echo json_encode($json);
	}
 
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectSchedule->exists($id)) {
			throw new NotFoundException(__('Invalid project schedule'));
		}
		$options = array('conditions' => array('ProjectSchedule.' . $this->ProjectSchedule->primaryKey => $id));
		$this->set('projectSchedule', $this->ProjectSchedule->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectSchedule->create();
			if ($this->ProjectSchedule->save($this->request->data)) {
				$this->Session->setFlash(__('The project schedule has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project schedule could not be saved. Please, try again.'));
			}
		}
		$projectBoms = $this->ProjectSchedule->ProjectBom->find('list');
		$users = $this->ProjectSchedule->User->find('list');
		$this->set(compact('projectBoms', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectSchedule->exists($id)) {
			throw new NotFoundException(__('Invalid project schedule'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectSchedule->save($this->request->data)) {
				$this->Session->setFlash(__('The project schedule has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project schedule could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectSchedule.' . $this->ProjectSchedule->primaryKey => $id));
			$this->request->data = $this->ProjectSchedule->find('first', $options);
		}
		$projectBoms = $this->ProjectSchedule->ProjectBom->find('list');
		$users = $this->ProjectSchedule->User->find('list');
		$this->set(compact('projectBoms', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectSchedule->id = $id;
		if (!$this->ProjectSchedule->exists()) {
			throw new NotFoundException(__('Invalid project schedule'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectSchedule->delete()) {
			$this->Session->setFlash(__('The project schedule has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project schedule could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
