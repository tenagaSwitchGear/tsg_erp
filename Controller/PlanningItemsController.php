<?php
App::uses('AppController', 'Controller');
 
class PlanningItemsController extends AppController {

 
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }
 
	public function index() {
		$this->PlanningItem->recursive = 0;
		$this->set('planningItems', $this->Paginator->paginate());
	}
 
	public function view($id = null) {
		if (!$this->PlanningItem->exists($id)) {
			throw new NotFoundException(__('Invalid planning item'));
		}
		$options = array('conditions' => array('PlanningItem.' . $this->PlanningItem->primaryKey => $id));
		$this->set('planningItem', $this->PlanningItem->find('first', $options));
	}
 
	public function add() {
		if ($this->request->is('post')) {
			$this->PlanningItem->create();
			if ($this->PlanningItem->save($this->request->data)) {
				$this->Session->setFlash(__('The planning item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The planning item could not be saved. Please, try again.'));
			}
		}
		$plannings = $this->PlanningItem->Planning->find('list');
		$inventoryItems = $this->PlanningItem->InventoryItem->find('list');
		$generalUnits = $this->PlanningItem->GeneralUnit->find('list');
		$inventorySuppliers = $this->PlanningItem->InventorySupplier->find('list');
		$this->set(compact('plannings', 'inventoryItems', 'generalUnits', 'inventorySuppliers'));
	} 

	public function edit($id = null, $planning_id = null) {
		if (!$this->PlanningItem->exists($id)) {
			throw new NotFoundException(__('Invalid planning item'));
		}

		$options = array('conditions' => array('PlanningItem.' . $this->PlanningItem->primaryKey => $id));
		$item = $this->PlanningItem->find('first', $options);

		if ($this->request->is(array('post', 'put'))) {
			if ($this->PlanningItem->save($this->request->data)) {
				$this->Session->setFlash(__('The planning item has been saved.'));
				return $this->redirect(array('controller' => 'plannings', 'action' => 'view', $planning_id));
			} else {
				$this->Session->setFlash(__('The planning item could not be saved. Please, try again.'));
			}
		} else { 
			$this->request->data = $item;
		}
		$plannings = $this->PlanningItem->Planning->find('list');
		$inventoryItems = $this->PlanningItem->InventoryItem->find('list');
		$generalUnits = $this->PlanningItem->GeneralUnit->find('list');
		$inventorySuppliers = $this->PlanningItem->InventorySupplier->find('list');
		$this->set(compact('plannings', 'inventoryItems', 'generalUnits', 'inventorySuppliers')); 
		$this->set('item', $item);
	}

	public function partialdelivery($id = null, $planning_id = null) {
		if (!$this->PlanningItem->exists($id)) {
			throw new NotFoundException(__('Invalid planning item'));
		}

		$options = array('conditions' => array('PlanningItem.' . $this->PlanningItem->primaryKey => $id));
		$item = $this->PlanningItem->find('first', $options);

		if ($this->request->is('post')) {
			$this->loadModel('PlanningPartialDelivery');
			$count = $this->PlanningPartialDelivery->find('all', array(
				'conditions' => array(
					'PlanningPartialDelivery.planning_id' => $planning_id,
					'PlanningPartialDelivery.planning_item_id' => $id
					),
				'recursive' => -1
				));
			if(count($count) > 0) {
				$delete = $this->PlanningPartialDelivery->deleteAll(array( 
						'PlanningPartialDelivery.planning_id' => $planning_id,
						'PlanningPartialDelivery.planning_item_id' => $id 
					), false);
			}
			$qty = $this->request->data['PlanningPartialDelivery']['quantity'];
			$delivery = $this->request->data['PlanningPartialDelivery']['delivery'];
			$remark = $this->request->data['PlanningPartialDelivery']['remark'];

			for($i = 0; $i < count($qty); $i++) {
				$this->PlanningPartialDelivery->create();
				$insert = array(
					'planning_id' => $planning_id,
					'planning_item_id' => $id,
					'delivery' => $delivery[$i],
					'quantity' => $qty[$i],
					'remark' => $remark[$i]
					);
				$this->PlanningPartialDelivery->save($insert); 
			}

			$this->Session->setFlash(__('The Partial Delivery has been saved.'));
			return $this->redirect(array('controller' => 'plannings', 'action' => 'planorderreview', $planning_id));
			 
		}

		$this->request->data = $item;

		$plannings = $this->PlanningItem->Planning->find('list');
		$inventoryItems = $this->PlanningItem->InventoryItem->find('list');
		$generalUnits = $this->PlanningItem->GeneralUnit->find('list');
		$inventorySuppliers = $this->PlanningItem->InventorySupplier->find('list');
		$this->set(compact('plannings', 'inventoryItems', 'generalUnits', 'inventorySuppliers'));

		$this->set('item', $item);

		$this->loadModel('PlanningPartialDelivery');
		$partials = $this->PlanningPartialDelivery->find('all', array(
			'conditions' => array(
				'PlanningPartialDelivery.planning_item_id' => $id
				)
			));
		$this->set('partials', $partials);
	}
 
	public function delete($id = null, $planning_id = null) {
		$this->PlanningItem->id = $id;
		if (!$this->PlanningItem->exists()) {
			throw new NotFoundException(__('Invalid planning item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PlanningItem->delete()) {
			$this->Session->setFlash(__('The planning item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The planning item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('controller' => 'plannings', 'action' => 'view', $planning_id));
	}
}
