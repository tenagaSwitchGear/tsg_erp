<?php
App::uses('AppController', 'Controller');
/**
 * SaleInvoices Controller
 *
 * @property SaleInvoice $SaleInvoice
 * @property PaginatorComponent $Paginator
 */
class SaleInvoicesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		if(isset($_GET['search'])) {
			if($_GET['station_name'] != '') {
				$cond['SaleJobChild.station_name LIKE'] = '%'. trim($_GET['station_name']) . '%';
			}
			if($_GET['so'] != '') {
				$cond['SaleOrder.name LIKE'] = '%'. trim($_GET['so']) . '%';
			}
			if($_GET['customer'] != '') {
				$cond['Customer.name LIKE'] = '%'. trim($_GET['customer']) . '%';
			}
			$this->request->data['SaleInvoice'] = $_GET;
		}
		$cond['SaleInvoice.customer_id !='] = 0;
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array( 
			'conditions' => $cond,
			'order' => 'SaleInvoice.id DESC',
			'limit' => $record_per_page
			);  
		$this->set('saleInvoices', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleInvoice->exists($id)) {
			throw new NotFoundException(__('Invalid sale invoice'));
		}
		$options = array('conditions' => array('SaleInvoice.' . $this->SaleInvoice->primaryKey => $id));
		$this->set('saleInvoice', $this->SaleInvoice->find('first', $options));

		$this->loadModel('SaleInvoiceItem');

		$items = $this->SaleInvoiceItem->find('all', array(
			'conditions' => array(
				'SaleInvoiceItem.sale_invoice_id' => $id
				),
			'recursive' => 2
			));
		$this->set('items', $items);
	}

	/**
 * index method
 *
 * @return void
 */
	public function financeindex($stat = null) {

		if($stat == null) {
			$status = 'Submit To Finance';
		} else {
			$status = $stat;
		}
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array( 
			'conditions' => array(
				'SaleInvoice.status' => $status
				),
			'order' => 'SaleInvoice.id DESC',
			'limit' => $record_per_page
			);  
		$this->set('saleInvoices', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function financeview($id = null) {
		if (!$this->SaleInvoice->exists($id)) {
			throw new NotFoundException(__('Invalid sale invoice'));
		}
		$options = array('conditions' => array('SaleInvoice.' . $this->SaleInvoice->primaryKey => $id));
		$this->set('saleInvoice', $this->SaleInvoice->find('first', $options));

		$this->loadModel('SaleInvoiceItem');

		$items = $this->SaleInvoiceItem->find('all', array(
			'conditions' => array(
				'SaleInvoiceItem.sale_invoice_id' => $id
				),
			'recursive' => 2
			));
		$this->set('items', $items);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleInvoice->create();
			$this->request->data['SaleInvoice']['user_id'] = $this->user_id;
			if ($this->SaleInvoice->save($this->request->data)) {
				$inv_id = $this->SaleInvoice->getLastInsertId();

				$item = $this->request->data['SaleInvoiceItem']['sale_order_item_id'];  
				$quantity = $this->request->data['SaleInvoiceItem']['quantity'];
				$unit = $this->request->data['SaleInvoiceItem']['general_unit_id'];  
				$this->loadModel('SaleInvoiceItem');
				for($i = 0; $i < count($item); $i++) {
					$this->SaleInvoiceItem->create();
					$insert = array(
						'sale_invoice_id' => $inv_id,
						'sale_order_item_id' => $item[$i],
						'quantity' => $quantity[$i],
						'general_unit_id' => $unit[$i],
						'finished_good_id' => 0 
						);
					$this->SaleInvoiceItem->save($insert);
				}

				$this->Session->setFlash(__('The RFI has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale invoice could not be saved. Please, try again.'));
			}
		}
		$customers = $this->SaleInvoice->Customer->find('list'); 
		$saleJobs = $this->SaleInvoice->SaleJob->find('list');
		$saleJobChildren = $this->SaleInvoice->SaleJobChild->find('list');
		$saleOrders = $this->SaleInvoice->SaleOrder->find('list');
		$users = $this->SaleInvoice->User->find('list');
		$termOfPayments = $this->SaleInvoice->TermOfPayment->find('list');
		$this->set(compact('customers', 'saleJobs', 'saleJobChildren', 'saleOrders', 'users', 'termOfPayments'));

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');
		$this->set(compact('units'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleInvoice->exists($id)) {
			throw new NotFoundException(__('Invalid sale invoice'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleInvoice->save($this->request->data)) {
				$this->Session->setFlash(__('The RFI has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The RFI could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleInvoice.' . $this->SaleInvoice->primaryKey => $id));
			$this->request->data = $this->SaleInvoice->find('first', $options);
		}
		$customers = $this->SaleInvoice->Customer->find('list');
		 
		$saleJobs = $this->SaleInvoice->SaleJob->find('list');
		$saleJobChildren = $this->SaleInvoice->SaleJobChild->find('list');
		$saleOrders = $this->SaleInvoice->SaleOrder->find('list');
		$users = $this->SaleInvoice->User->find('list');
		$termOfPayments = $this->SaleInvoice->TermOfPayment->find('list');
		$this->set(compact('customers', 'saleJobs', 'saleJobChildren', 'saleOrders', 'users', 'termOfPayments'));

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');
		$this->set(compact('units'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleInvoice->id = $id;
		if (!$this->SaleInvoice->exists()) {
			throw new NotFoundException(__('Invalid sale invoice'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleInvoice->delete()) {
			$this->Session->setFlash(__('The sale invoice has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale invoice could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
