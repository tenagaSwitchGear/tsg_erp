<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * InventorySupplierItems Controller
 *
 * @property InventorySupplierItem $InventorySupplierItem 
 * @property PaginatorComponent $Paginator
 */


class InventorySupplierItemsController extends AppController { 


	public $components = array('Paginator');
	
	public function beforeFilter() {
		parent::beforeFilter(); 
		$this->Auth->allow('cronexpiry');
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		if(isset($_GET['search'])) {
 
			if($_GET['name'] != '') {
				$conditions['OR']['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
				$conditions['OR']['InventoryItem.code LIKE'] = '%'.$_GET['name'].'%';
			} 
 
			if($_GET['inventory_supplier_id'] != '') {
				$conditions['InventorySupplierItem.inventory_supplier_id'] = (int)$_GET['inventory_supplier_id'];
			}

			if($_GET['effective_date'] != '') {
				$conditions['InventorySupplierItem.expiry_date >='] = $_GET['effective_date'];
			} 

			if($_GET['expiry_date'] != '') {
				$conditions['InventorySupplierItem.expiry_date <='] = $_GET['expiry_date'];
			} 

			if($_GET['part_number'] != '') {
				$conditions['InventorySupplierItem.part_number LIKE'] = '%'.$_GET['part_number'].'%';
			}  
			if($_GET['quotation_number'] != '') {
				$conditions['InventorySupplierItem.quotation_number LIKE'] = '%'.$_GET['quotation_number'].'%';
			}
			$this->request->data['InventorySupplierItem'] = $_GET;
		} 
 
		$conditions['InventorySupplierItem.id !='] = 0; 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order' => 'InventorySupplierItem.id DESC',
			'limit' => $record_per_page
			);
		$this->set('inventorySupplierItems', $this->Paginator->paginate());
		$this->loadModel('InventoryItemCategory');

		$categories = $this->InventoryItemCategory->find('list', array('recursive' => -1)); 

		$suppliers = $this->InventorySupplierItem->InventorySupplier->find('list', array(
		'group' => 'InventorySupplier.name',
		'recursive' => -1)); 

		$this->set(compact('categories', 'suppliers', 'stores'));
	}

	public function sales() {
		if(isset($_GET['search'])) {
			if($_GET['inventory_item_id'] != '') {
				$conditions['InventorySupplierItem.inventory_item_id'] = (int)$_GET['inventory_item_id'];
			}

			if($_GET['inventory_supplier_id'] != '') {
				$conditions['InventorySupplierItem.inventory_supplier_id'] = (int)$_GET['inventory_supplier_id'];
			}

			if($_GET['effective_date'] != '') {
				$conditions['InventorySupplierItem.effective_date >='] = $_GET['effective_date'];
			} 

			if($_GET['expiry_date'] != '') {
				$conditions['InventoryItem.expiry_date <='] = $_GET['expiry_date'];
			} 

			if($_GET['part_number'] != '') {
				$conditions['InventorySupplierItem.part_number LIKE'] = '%'.$_GET['part_number'].'%';
			}  
			if($_GET['quotation_number'] != '') {
				$conditions['InventorySupplierItem.quotation_number LIKE'] = '%'.$_GET['quotation_number'].'%';
			}
			$this->request->data['InventorySupplierItem'] = $_GET;
		} 
 
		$conditions['InventorySupplierItem.sales'] = 1; 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'order' => 'InventorySupplierItem.id DESC',
			'limit' => $record_per_page
			);
		$this->set('inventorySupplierItems', $this->Paginator->paginate());
		$this->loadModel('InventoryItemCategory');

		$categories = $this->InventoryItemCategory->find('list', array('recursive' => -1)); 

		$suppliers = $this->InventorySupplierItem->InventorySupplier->find('list', array('recursive' => -1)); 

		$this->set(compact('categories', 'suppliers', 'stores'));
	}

	public function viewprice($id = null) {
		if (!$this->InventorySupplierItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier item'));
		}
		$options = array('conditions' => array(
			'InventorySupplierItem.' . $this->InventorySupplierItem->primaryKey => $id,
			'InventorySupplierItem.sales' => 1
			));
		$this->set('inventorySupplierItem', $this->InventorySupplierItem->find('first', $options));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventorySupplierItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier item'));
		}
		$options = array('conditions' => array('InventorySupplierItem.' . $this->InventorySupplierItem->primaryKey => $id));
		$this->set('inventorySupplierItem', $this->InventorySupplierItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id = null) {
		if ($this->request->is('post')) {
			$this->loadModel('InventoryItem');
			 
			$inventory_item_name = $this->InventoryItem->query("SELECT name, qc_inspect  FROM inventory_items WHERE id=".$this->request->data['InventorySupplierItem']['inventory_item_id']);
  
			$this->request->data['InventorySupplierItem']['qc_inspect'] = $inventory_item_name[0]['inventory_items']['qc_inspect'];
			//exit;
			$this->request->data['InventorySupplierItem']['created'] = $this->date; 
			 
			$this->InventorySupplierItem->create(); 

			$general_unit = $this->request->data['InventorySupplierItem']['general_unit_id'];
			$conversion_unit = $this->request->data['InventorySupplierItem']['conversion_unit_id'];
			$conversion_qty = $this->request->data['InventorySupplierItem']['conversion_qty'];

			if($general_unit != $conversion_unit && $conversion_qty != '') {
				$price = $this->request->data['InventorySupplierItem']['price'];
				
				$price_per_unit = $price / $conversion_qty;
				$this->request->data['InventorySupplierItem']['price_per_unit'] = $price_per_unit;
			} else {
				$this->request->data['InventorySupplierItem']['price_per_unit'] = $this->request->data['InventorySupplierItem']['price'];
			}

			// Overide default supplier
			$default_supplier = $this->request->data['InventorySupplierItem']['is_default'];
			if($default_supplier == 1) {
				// Update current default supplier
				$this->InventorySupplierItem->updateAll(
					array(
						'InventorySupplierItem.is_default' => "'0'"
					),
					array(
						'InventorySupplierItem.inventory_item_id' => $this->request->data['InventorySupplierItem']['inventory_item_id']
					)); 
			}

			if ($this->InventorySupplierItem->save($this->request->data)) { 
				$this->Session->setFlash(__('The inventory supplier item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier item could not be saved. Please, try again.'));
			}
		}
		if(!empty($id)){
			$id = $id;
		}else{
			$id = 0;
		}
		$inventoryItems = $this->InventorySupplierItem->InventoryItem->find('list', array('recursive' => -1, 'order'=>'InventoryItem.name ASC'));
		
		$generalUnits = $this->InventorySupplierItem->GeneralUnit->find('list', array('recursive' => -1));

		$InventorySuppliers = $this->InventorySupplierItem->InventorySupplier->find('list', array(
			'order' => 'InventorySupplier.name ASC', 
			'conditions' => array(
				'InventorySupplier.status' => 'Active'
				) 
			));
		$currencies = $this->InventorySupplierItem->GeneralCurrency->find('list', array('recursive' => -1)); 

		$this->set(compact('inventoryItems', 'generalUnits', 'InventorySuppliers', 'currencies', 'id'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void 
 */
	public function ajaxfinditem_2() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			//$supplier = $this->request->query['supplier'];
			$term = $this->request->query['term'];
			$this->loadModel('InventorySupplierItem');
			$this->loadModel('InventorySupplier');
			$this->loadModel('InventoryItem');
			$conditions = array(); 

			$conditions['OR']['InventoryItem.name LIKE'] = '%'.$term.'%';
			$conditions['OR']['InventoryItem.code LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['InventoryItem.note LIKE'] = '%'.$term.'%'; 

			$conditions['OR']['InventoryItem.search_1 LIKE'] = '%'.$term.'%';
			$conditions['OR']['InventoryItem.search_2 LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['InventoryItem.search_3 LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['InventoryItem.search_4 LIKE'] = '%'.$term.'%';
			$conditions['OR']['InventoryItem.search_5 LIKE'] = '%'.$term.'%';  
			
			//$conditions['InventorySupplierItem.inventory_supplier_id'] = $supplier; 
			//$conditions['InventorySupplierItem.expiry_date >='] = date('Y-m-d');
			//$conditions['InventorySupplierItem.is_default'] = 1; 
			$items = $this->InventoryItem->find('all', array(
				'conditions' => $conditions,
				'order' => 'InventoryItem.code ASC',
				'limit' => 20
				));

			 

			$json = array(); 
			$json['Suppliers'] = array();
			if($items) {
				foreach ($items as $item) { 
					$pricebook = $this->price_book($item['InventoryItem']['id']);
					$json[] = array(
						'id' => $item['InventoryItem']['id'],
						'name' => $item['InventoryItem']['name'],
						'code' => $item['InventoryItem']['code'],
						'note' => $item['InventoryItem']['note'],
						'general_unit_id' => $item['InventoryItem']['general_unit_id'],
						'unit' => $item['GeneralUnit']['name'], 
						'InventorySupplierItem' => !empty($pricebook['InventorySupplierItem']) ? $pricebook['InventorySupplierItem'] : null,
						'InventorySupplier' => !empty($pricebook['InventorySupplier']) ? $pricebook['InventorySupplier'] : null,
						'GeneralCurrency' => !empty($pricebook['GeneralCurrency']) ? $pricebook['GeneralCurrency'] : null, 
						 
						); 
				}
				/*foreach ($items as $item) { 
					$pricebook = $this->price_book($item['InventoryItem']['id']);
					$json[] = $item;
					if(!empty($pricebook)) {
						$json[] = $pricebook['InventorySupplierItem'];
						$json[] = $pricebook['InventorySupplier'];
						$json[]['notFound'] = 0;
					} else {
						$json[]['InventoryItem']['id'] = 0;
						$json[]['InventorySupplierItem']['id'] = 0;
						$json[]['InventorySupplier']['id'] = 0;
						$json[]['InventoryItem']['code'] = 'Price book not found. Please contact Procurement.';
						$json[]['InventoryItem']['name'] = 'Not found'; 
						$json[]['InventorySupplierItem']['general_currency_id'] = 0;
						$json[]['InventorySupplier']['id'] = 0; 
						$json[]['InventorySupplier']['gst'] = 0;
						$json[]['GeneralUnit']['id'] = 0;
						$json[]['notFound'] = 1;
					}
				}*/	 
			} 
			echo json_encode($json);	
		}
	}

	private function price_book($item_id) {
		$this->loadModel('InventorySupplierItem');
		$item = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $item_id,
				'InventorySupplierItem.is_default' => 1
				)
			));
		return $item;
	}

	public function ajaxfinditemdetail() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['supplierId'])) {
			//$supplier = $this->request->query['supplier'];
			$id = $this->request->query['itemId'];
			$idsupplier = $this->request->query['supplierId'];
			$this->loadModel('InventorySupplierItem');
			$conditions = array(); 

			$conditions['InventorySupplierItem.inventory_item_id'] = $id;
			$conditions['InventorySupplierItem.id'] = $idsupplier; 

			$items = $this->InventorySupplierItem->find('all', array(
				'conditions' => $conditions,
			));
			
			$json = $items;
			//print_r($json);
			echo json_encode($json);	
		}
	}

	public function ajaxfinditemdetail_2() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['supplierId'])) {
			//$supplier = $this->request->query['supplier'];
			$id = $this->request->query['itemId'];
			$idsupplier = $this->request->query['supplierId'];
			$this->loadModel('InventorySupplierItem');
			$conditions = array(); 

			$conditions['InventorySupplierItem.inventory_item_id'] = $id;
			$conditions['InventorySupplierItem.inventory_supplier_id'] = $idsupplier; 

			$items = $this->InventorySupplierItem->find('all', array(
				'conditions' => $conditions,
			));
			
			$json = $items;
			//print_r($json);
			echo json_encode($json);	
		}
	}

	public function export() {

		$this->response->download("Supplier Item.csv");

		//$data = $this->InventorySupplierItem->find('all');
		//$this->set(compact('data'));

		$this->layout = 'ajax';

		$header = array(
			'#',
			'Items',
			'Currency',
			'Price',
			'Price / Pcs',
			'Unit',
			'Effective Date',
			'Expiry Date',
			'Supplier',
			'Default'
		);

		$this->set('header',$header);

		if(isset($_GET['search'])) {
 
			if($_GET['name'] != '') {
				$conditions['OR']['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
				$conditions['OR']['InventoryItem.code LIKE'] = '%'.$_GET['name'].'%';
			} 
 
			if($_GET['inventory_supplier_id'] != '') {
				$conditions['InventorySupplierItem.inventory_supplier_id'] = (int)$_GET['inventory_supplier_id'];
			}

			if($_GET['effective_date'] != '') {
				$conditions['InventorySupplierItem.expiry_date >='] = $_GET['effective_date'];
			} 

			if($_GET['expiry_date'] != '') {
				$conditions['InventorySupplierItem.expiry_date <='] = $_GET['expiry_date'];
			} 

			if($_GET['part_number'] != '') {
				$conditions['InventorySupplierItem.part_number LIKE'] = '%'.$_GET['part_number'].'%';
			}  
			if($_GET['quotation_number'] != '') {
				$conditions['InventorySupplierItem.quotation_number LIKE'] = '%'.$_GET['quotation_number'].'%';
			}
			$this->request->data['InventorySupplierItem'] = $_GET;
		} 
 
		$conditions['InventorySupplierItem.id !='] = 0; 

		//$record_per_page = Configure::read('Reading.nodes_per_page');

		$inventorySupplierItems = $this->InventorySupplierItem->find('all', array(
			'conditions' => $conditions,
			'order' => 'InventorySupplierItem.id DESC',
			//'limit' => $record_per_page
			));
		$this->set('inventorySupplierItems', $inventorySupplierItems);
		$this->loadModel('InventoryItemCategory');

		$categories = $this->InventoryItemCategory->find('list', array('recursive' => -1)); 

		$suppliers = $this->InventorySupplierItem->InventorySupplier->find('list', array(
		'group' => 'InventorySupplier.name',
		'recursive' => -1)); 

		$this->set(compact('categories', 'suppliers', 'stores'));

		return;

	}


	public function view_pdf_item_list() {
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printing'; 

	    if(isset($_GET['search'])) {
 
			if($_GET['name'] != '') {
				$conditions['OR']['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
				$conditions['OR']['InventoryItem.code LIKE'] = '%'.$_GET['name'].'%';
			} 
 
			if($_GET['inventory_supplier_id'] != '') {
				$conditions['InventorySupplierItem.inventory_supplier_id'] = (int)$_GET['inventory_supplier_id'];
			}

			if($_GET['effective_date'] != '') {
				$conditions['InventorySupplierItem.expiry_date >='] = $_GET['effective_date'];
			} 

			if($_GET['expiry_date'] != '') {
				$conditions['InventorySupplierItem.expiry_date <='] = $_GET['expiry_date'];
			} 

			if($_GET['part_number'] != '') {
				$conditions['InventorySupplierItem.part_number LIKE'] = '%'.$_GET['part_number'].'%';
			}  
			if($_GET['quotation_number'] != '') {
				$conditions['InventorySupplierItem.quotation_number LIKE'] = '%'.$_GET['quotation_number'].'%';
			}
			$this->request->data['InventorySupplierItem'] = $_GET;
		} 
 
		$conditions['InventorySupplierItem.id !='] = 0; 

		//$record_per_page = Configure::read('Reading.nodes_per_page');

		$inventorySupplierItems = $this->InventorySupplierItem->find('all', array(
			'conditions' => $conditions,
			'order' => 'InventorySupplierItem.id DESC',
			//'limit' => $record_per_page
			));
		$this->set('inventorySupplierItems', $inventorySupplierItems);
		$this->loadModel('InventoryItemCategory');

		$categories = $this->InventoryItemCategory->find('list', array('recursive' => -1)); 

		$suppliers = $this->InventorySupplierItem->InventorySupplier->find('list', array(
		'group' => 'InventorySupplier.name',
		'recursive' => -1)); 

		$this->set(compact('categories', 'suppliers', 'stores'));

	}

	public function edit($id = null) {
		if (!$this->InventorySupplierItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier item'));
		}
		if ($this->request->is(array('post', 'put'))) {

			$general_unit = $this->request->data['InventorySupplierItem']['general_unit_id'];
			$conversion_unit = $this->request->data['InventorySupplierItem']['conversion_unit_id'];
			$conversion_qty = $this->request->data['InventorySupplierItem']['conversion_qty'];

			if($general_unit != $conversion_unit && $conversion_qty != '') {
				$price = $this->request->data['InventorySupplierItem']['price']; 
				$price_per_unit = $price / $conversion_qty;
				$this->request->data['InventorySupplierItem']['price_per_unit'] = $price_per_unit;
			} else {
				$this->request->data['InventorySupplierItem']['price_per_unit'] = $this->request->data['InventorySupplierItem']['price'];
				$this->request->data['InventorySupplierItem']['conversion_qty'] = 1;
			}

			// Overide default supplier
			$default_supplier = $this->request->data['InventorySupplierItem']['is_default'];
			if($default_supplier == 1) {
				// Update current default supplier
				$update = $this->InventorySupplierItem->updateAll(
					array(
						'InventorySupplierItem.is_default' => "'0'"
					),
					array(
						'InventorySupplierItem.inventory_item_id' => $this->request->data['InventorySupplierItem']['inventory_item_id']
					)); 
				if(!$update) {
					die('Error 318');
				}
			}


			if ($this->InventorySupplierItem->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier item has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier item could not be saved. Please, try again.'), 'error');
			}
		}
		$options = array('conditions' => array('InventorySupplierItem.' . $this->InventorySupplierItem->primaryKey => $id));
		$this->request->data = $this->InventorySupplierItem->find('first', $options);
		
		$inventoryItems = $this->InventorySupplierItem->InventoryItem->find('list', array('recursive' => -1));
		$generalUnits = $this->InventorySupplierItem->GeneralUnit->find('list', array('recursive' => -1));

		$InventorySuppliers = $this->InventorySupplierItem->InventorySupplier->find('list', array('recursive' => -1));
		$currencies = $this->InventorySupplierItem->GeneralCurrency->find('list', array('recursive' => -1));

		//json_encode($InventorySuppliers);

		//if(!empty($this->request->data['aaa'])) {
		//	$conditions['User.username LIKE'] = '%'.$this->data['User']['username'].'%';
		//}

		$this->set(compact('inventoryItems', 'generalUnits', 'InventorySuppliers', 'currencies'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventorySupplierItem->id = $id;
		if (!$this->InventorySupplierItem->exists()) {
			throw new NotFoundException(__('Invalid inventory supplier item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventorySupplierItem->delete()) {
			$this->Session->setFlash(__('The inventory supplier item has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The inventory supplier item could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function ajaxfinddefaultsupplier() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['inventory_item_id'])) {
			$id = (int)$this->request->query['inventory_item_id'];
			 
			$items = $this->InventorySupplierItem->find('all', array('conditions' => array('InventorySupplierItem.inventory_item_id' => $id)));
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['InventorySupplierItem']['id'], 
					'name' => $item['InventoryItem']['name'],
					'supplier' => $item['InventorySupplier']['name'],
					'default' => $item['InventorySupplierItem']['is_default'],
					'expiry_date' => $item['InventorySupplierItem']['expiry_date'],
					'price' => $item['InventorySupplierItem']['price'],
					'price_per_unit' => $item['InventorySupplierItem']['price_per_unit'],
					'min_order' => $item['InventorySupplierItem']['min_order'],
					'lead_time' => $item['InventorySupplierItem']['lead_time'],
					'unit' => $item['GeneralUnit']['name'],
					'conversion_qty' => $item['InventorySupplierItem']['conversion_qty'],
					'label' => $this->label_expiry($item['InventorySupplierItem']['expiry_date'])
					);
			}
			echo json_encode($json);	
		}
	}

	public function ajaxfinditembysupplier() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['inventory_item_id'])) {
			$inventory_item_id = (int)$this->request->query['inventory_item_id'];
			$supplier_id = (int)$this->request->query['supplier_id'];

			$item = $this->InventorySupplierItem->find('first', array(
				'conditions' => array(
					'InventorySupplierItem.inventory_item_id' => $inventory_item_id,
					'InventorySupplierItem.inventory_supplier_id' => $supplier_id 
					)
				));  
			if($item['InventorySupplier']['gst'] == '') {
				$tax = 0;
			} else {
				$tax = Configure::read('Site.gst_amount');
			}
			$json = array(
				'InventorySupplierItem' => $item['InventorySupplierItem'],
				'GeneralCurrency' => $item['GeneralCurrency'],
				'MinOrderUnit' => $item['MinOrderUnit'],
				'Tax' => $tax
				); 
			echo json_encode($json);	
		}
	}

	public function ajaxfinditem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['inventory_item_id'])) {
			$inventory_item_id = (int)$this->request->query['inventory_item_id'];
			$supplier_id = (int)$this->request->query['supplier_id'];

			$items = $this->InventorySupplierItem->find('first', array(
				'conditions' => array(
					'InventorySupplierItem.inventory_item_id' => $inventory_item_id,
					'InventorySupplierItem.inventory_supplier_id' => $supplier_id 
					)
				)); 
			echo json_encode($items);	
		}
	}

	private function label_expiry($expiry) {
		if(date('Y-m-d') > date('Y-m-d', strtotime($expiry)) ){
 			return 'danger';
		} else {
			return 'success';
		}
	}

	public function cronexpiry($token = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if($token != null) {
			$today = date('Y-m-d');
		    $expiry = date('Y-m-d', strtotime('+500 days', strtotime($today)));

		    $items = $this->InventorySupplierItem->find('all', array(
		    	'conditions' => array(
		    		'InventorySupplierItem.expiry_date <=' => $expiry
		    	)));
		    $data = array();
		    //var_dump($items);
		    if($items) {
			    $this->loadModel('User');
			    $users = $this->User->find('all', array(
			    	'conditions' => array(
			    		'User.group_id' => 11 // Prcurement dept
			    		)
			    	));
			    //var_dump($users);
			    foreach ($users as $user) {
			    	$data['to'] = $user['User']['email'];
			    	$data['subject'] = 'Price Books Expiry Reminder';
			    	$data['template'] = 'price_book_expiry_notification';
			    	$data['content']['username'] = $user['User']['username'];
			    	$data['content']['items'] = $items;
			    	$data['content']['link'] = 'inventory_supplier_items/expiry';
			    	debug($this->send_email($data)); 
			    }	
		    } 
		}
	}
}
