<?php
App::uses('AppController', 'Controller');
/**
 * InventoryStockRejecteds Controller
 *
 * @property InventoryStockRejected $InventoryStockRejected
 * @property PaginatorComponent $Paginator
 */
class InventoryStockRejectedsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryStockRejected->recursive = 0;
		$this->set('inventoryStockRejecteds', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->loadModel('InventoryQcItem');
		$qc_array = array();
		$stock = $this->InventoryQcItem->find('all', array(
			'conditions' => array(
				'InventoryQcItem.id' => $id
			)
		));

		$qc_array[] = array(
			'InventoryQcItem' => $stock[0]['InventoryQcItem'],
			'InventoryQcRemark' => $this->get_qcremark($stock[0]['InventoryQcItem']['id'])
		);
		//print_r($qc_array);
		//exit;
		//$options = array('conditions' => array('InventoryStockRejected.' . $this->InventoryStockRejected->primaryKey => $id));
		$this->set('inventoryStockRejected', $qc_array);
	}

	private function get_qcremark($id){
		$this->loadModel('InventoryQcRemark');

		$remarks = $this->InventoryQcRemark->find('all', array(
			'conditions' => array(
				'InventoryQcRemark.inventory_qc_item_id' => $id
			),
			'recursive' => -1
		));

		$remark_array = array();
		foreach ($remarks as $remark) {
			$remark_array[] = array(
				'InventoryQcRemark' => $remark['InventoryQcRemark'],
				'Comment' => $this->get_comment($remark['InventoryQcRemark']['finished_good_comment_id'])
			);
		}

		return $remark_array;
	}

	private function get_comment($id){
		$this->loadModel('FinishedGoodComment');
		$comment = $this->FinishedGoodComment->find('first', array(
			'conditions' => array(
				'FinishedGoodComment.id' => $id
			),
			'recursive' => -1
		));

		return $comment['FinishedGoodComment'];

	}

	public function setStatus() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$pk = mysql_real_escape_string($_POST['pk']);
        $value = mysql_real_escape_string($_POST['value']);
		
		if($value != '') {
			/*$this->FinishedGood->updateAll(
			array('FinishedGood.status' => $value),
			array('FinishedGood.id' => $pk)
			);*/
			
			$this->InventoryStockRejected->query('UPDATE `inventory_stock_rejecteds` SET `status` = "'.$value.'" WHERE `id` = "'.$pk.'"');
					
		} else {
			header('HTTP 400 Bad Request', true, 400);
			echo "This field is required!";		
		}
	}

	public function getStatus()
	{
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$statuses = array();
		
		$statuses[] = array('value' => '0', 'text' => 'Submit to Store');
		$statuses[] = array('value' => '1', 'text' => 'Replace');
		echo json_encode($statuses);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryStockRejected->create();
			if ($this->InventoryStockRejected->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory stock rejected has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory stock rejected could not be saved. Please, try again.'));
			}
		}
		$inventoryStocks = $this->InventoryStockRejected->InventoryStock->find('list');
		$this->set(compact('inventoryStocks'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryStockRejected->exists($id)) {
			throw new NotFoundException(__('Invalid inventory stock rejected'));
		}
		if ($this->request->is(array('post', 'put'))) {
			//print_r($_POST);
			//exit;
			$this->loadModel('InventoryDeliveryOrderItem');
			$status = $_POST['status'];
			$inventory_qc_item_id = $_POST['inventory_qc_item_id'];

			$get_detail = $this->InventoryStockRejected->query("SELECT * FROM inventory_stock_rejecteds WHERE id=".$_POST['id']);
			//print_r($get_detail);
			//exit;
			if($get_detail[0]['inventory_stock_rejecteds']['type']=='2'){
				/*$get_do_item_detail = $this->InventoryDeliveryOrderItem->query("SELECT * FROM inventory_delivery_order_items where id=".$get_detail[0]['inventory_stock_rejecteds']['inventory_delivery_order_item_id']);

				$remain = $get_do_item_detail[0]['inventory_delivery_order_items']['id'] - $_POST['quantity'];
				if($remain<0){
					$new_remaining = 0;
				}else{
					$new_remaining = $remain;
				}

				$data_do_item = array(
					'id' => $get_do_item_detail[0]['inventory_delivery_order_items']['id'],
					'quantity_delivered' => $_POST['quantity'],
					'quantity_remaining' => $new_remaining
				);*/

				//$this->loadModel('InventoryDeliveryOrderItem');
				//$this->InventoryDeliveryOrderItem->create();

				//if ($this->InventoryDeliveryOrderItem->save($data_do_item)) {
					$data_rej = array(
						'id' 	 => $_POST['id'],
						'status' => '1' 
					);
				
					if ($this->InventoryStockRejected->save($data_rej)) {
						$this->Session->setFlash(__('The Stocks has been update.'), 'success');
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash(__('The Stocks could not be update. Please, try again.'), 'error');
					}
				/*} else {
					$this->Session->setFlash(__('The Stocks could not be update. Please, try again.'), 'error');
				}*/
			}else{
				$this->loadModel('InventoryQcItem');
				$item_qc = $this->InventoryQcItem->query("SELECT * FROM inventory_qc_items WHERE id=".$inventory_qc_item_id);

				if($status == '1'){

					$this->loadModel('InventoryQcItem');

					$this->InventoryQcItem->create();
					$data_qc = array(
						'id'				=> $_POST['inventory_qc_item_id'],
						'quantity_rejected' => $item_qc[0]['inventory_qc_items']['quantity_rejected'] - $_POST['quantity']
					);

					$this->InventoryQcItem->save($data_qc);

				}

				$data_rej = array(
					'id' 	 => $_POST['id'],
					'status' => '1' 
				);
			
				if ($this->InventoryStockRejected->save($data_rej)) {
					$this->Session->setFlash(__('The Stocks has been update.'), 'success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The Stocks could not be update. Please, try again.'), 'error');
				}
			}
		} else {
			$options = array('conditions' => array('inventoryStockRejected.' . $this->InventoryStockRejected->primaryKey => $id));
			$this->request->data = $this->InventoryStockRejected->find('first', $options);
			
		}
		//$inventoryStocks = $this->InventoryStockRejected->InventoryStock->find('list');
		//$this->set(compact('inventoryStocks'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryStockRejected->id = $id;
		if (!$this->InventoryStockRejected->exists()) {
			throw new NotFoundException(__('Invalid inventory stock rejected'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryStockRejected->delete()) {
			$this->Session->setFlash(__('The inventory stock rejected has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The inventory stock rejected could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}