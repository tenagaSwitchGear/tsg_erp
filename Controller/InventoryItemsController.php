<?php
App::uses('AppController', 'Controller');
/**
 * InventoryItems Controller
 *
 * @property InventoryItem $InventoryItem
 * @property PaginatorComponent $Paginator
 */
class InventoryItemsController extends AppController { 

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter(); 
	}

	public function ajaxfinditem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$this->loadModel('InventoryItem');
			$conditions = array(); 

			if(isset($_GET['sale_job_child_id'])) {
				$sale_job_child_id = $_GET['sale_job_child_id'];
			} else {
				$sale_job_child_id = '';
			}
  

			$conditions['OR']['InventoryItem.name LIKE'] = '%'.$term.'%';
			$conditions['OR']['InventoryItem.code LIKE'] = '%'.$term.'%'; 
			$conditions['OR']['InventoryItem.note LIKE'] = '%'.$term.'%'; 

			$items = $this->InventoryItem->find('all', array(
				'conditions' => $conditions,
				'group' => array('InventoryItem.id'),
				'order' => 'InventoryItem.code ASC',
				'limit' => 20
				));
			
			$json = array();
			if($items) {
				foreach ($items as $item) { 
					$json[] = array(
						'id' => $item['InventoryItem']['id'], 
						'name' => $item['InventoryItem']['name'],
						'code' => $item['InventoryItem']['code'],
						'note' => $item['InventoryItem']['note'],
						'price' => $this->find_price_book($item['InventoryItem']['id']),
						'unit' => $item['InventoryItem']['general_unit_id'],
						'type' => $item['InventoryItem']['type'],
						'bom' => $this->find_bom($item['InventoryItem']['id'], 1),
						'costings' => $this->find_costing($item['InventoryItem']['id']),
						'pricebooks' => $this->find_price_books($item['InventoryItem']['id']),
						'stocks' => $this->find_stocks($item['InventoryItem']['id']) == null ? 0 : $this->find_stocks($item['InventoryItem']['id']) 
						);  
				}	
			} else {
				$json[] = array(
						'id' => 0, 
						'name' => 'Not found',
						'code' => 'Not found',
						'note' => 'Not found',
						'price' => '0',
						'unit' => '0',
						'type' => 'Not found',
						'bom' => 'Not found',
						'costings' => 'Not found',
						'stocks' => 0
						); 
			}
			
			echo json_encode($json);	
		}
	}

	private function find_stocks($item_id) {
		$this->loadModel('InventoryStock');
		$sums = $this->InventoryStock->find('all', array(
			'fields' => array(
				'SUM(InventoryStock.quantity) AS total'
				),
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $item_id
				)
			)); 
		foreach ($sums as $sum) {
			return $sum[0]['total'];
		} 
	}

	public function ajaxfinditemreport() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$this->loadModel('InventoryItem');
			$conditions = array(); 

			if(isset($_GET['sale_job_child_id'])) {
				$sale_job_child_id = $_GET['sale_job_child_id'];
			} else {
				$sale_job_child_id = '';
			}
  
			$conditions['OR']['InventoryItem.code LIKE'] = '%'.$term.'%'; 

			$items = $this->InventoryItem->find('all', array(
				'conditions' => $conditions,
				'group' => array('InventoryItem.id'),
				'order' => 'InventoryItem.code ASC',
				'limit' => 20
				));
			
			$json = array();
			if($items) {
				foreach ($items as $item) { 
					$json[] = array(
						'id' => $item['InventoryItem']['id'], 
						'name' => $item['InventoryItem']['name'],
						'code' => $item['InventoryItem']['code'],
						'note' => $item['InventoryItem']['note'],
						'price' => $this->find_price_book($item['InventoryItem']['id']),
						'unit' => $item['InventoryItem']['general_unit_id'],
						'type' => $item['InventoryItem']['type'],
						'bom' => $this->find_bom($item['InventoryItem']['id'], 1),
						'costings' => $this->find_costing($item['InventoryItem']['id']),
						'pricebooks' => $this->find_price_books($item['InventoryItem']['id'])  
						);  
				}	
			} else {
				$json[] = array(
						'id' => 0, 
						'name' => 'Not found',
						'code' => 'Not found',
						'note' => 'Not found',
						'price' => '0',
						'unit' => '0',
						'type' => 'Not found',
						'bom' => 'Not found',
						'costings' => 'Not found',
						); 
			}
			
			echo json_encode($json);	
		}
	}

	private function find_price_books($inventory_item_id) {
		$this->loadModel('InventorySupplierItem');
		$price = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $inventory_item_id,
				'InventorySupplierItem.expiry_date >=' => date('Y-m-d') 
				),
			'order' => 'InventorySupplierItem.is_default DESC'
			)); 
		return $price; 
	}

	private function find_price_book($inventory_item_id) {
		$this->loadModel('InventorySupplierItem');
		$price = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $inventory_item_id,
				'InventorySupplierItem.expiry_date >=' => date('Y-m-d'),
				'InventorySupplierItem.is_default' => 1
				)
			));
		if($price) {
			return $price['InventorySupplierItem']['price_per_unit'];
		} else {
			return 0;
		} 
	}

	private function find_costing($id) {
		$this->loadModel('SaleQuotationItem');
		$costs = $this->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.inventory_item_id' => $id,
				'SaleQuotationItem.costing' => 1,
				'SaleQuotation.date >=' => $this->date,
				'SaleQuotation.status' => 4
				),
			'order' => 'SaleQuotationItem.id DESC'
			));
		return $costs;
	}

	private function find_bom($id, $status) {
		$this->loadModel('Bom');
 		 
		$condition['Bom.inventory_item_id'] = $id; 
		$condition['Bom.status'] = $status; 

		$bom = $this->Bom->find('first', array(
			'conditions' => $condition 
			));
		if($bom) {
			return $bom['Bom'];
		} else {
			return array('id' => 0, 'code' => 'BOM Not Found');
		} 
	}

	public function ajaxfindbycategoryid() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['category_id'])) {
			$id = $this->request->query['category_id'];
			if(is_numeric($id)) { 
				$this->loadModel('InventoryItem');
				$items = $this->InventoryItem->find('all', array('conditions' => array('InventoryItem.inventory_item_category_id' => $id)));
				$json = array();
				foreach ($items as $item) {
					$json[] = array('id' => $item['InventoryItem']['id'], 'name' => $item['InventoryItem']['name'], 'status' => $item['InventoryItem']['is_bom']);
				}
				echo json_encode($json);
			} 
		}
	}

	public function ajaxfindbombyitem() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['inventory_item_id'])) {
			$id = $this->request->query['inventory_item_id'];
			if(is_numeric($id)) { 
				$json = array();
				$this->loadModel('InventoryItem');
				$item = $this->InventoryItem->find('first', array('conditions' => array('InventoryItem.id' => $id)));
				if($item['InventoryItem']['is_bom'] == 1) {
					// Check Bom
					$this->loadModel('Bom');
					$bom = $this->Bom->find('first', array(
						'conditions' => array(
							'Bom.inventory_item_id' => $id,
							'Bom.status' => 1
							),
						'order' => array(
							'Bom.id' => 'DESC'
							),
						'recursive' => -1
						));
					if(!$bom) {
						$json['status'] = false;
						$json['message'] = 'BOM Not found. Please add it first before continue (PIC: EAD).';
						$json['is_bom'] = 0;
						$json['bom_id'] = 0;
					} else {
						$json['status'] = true;
						$json['is_bom'] = 1;
						$json['bom_id'] = $bom['Bom']['id'];
					}
				} else {
					$json['status'] = true;
					$json['is_bom'] = 0;
					$json['bom_id'] = 0;
				}
				echo json_encode($json);
			} 
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['InventoryItem.name LIKE'] = '%'.$_GET['name'].'%';
			}

			if($_GET['code'] != '') {
				$conditions['InventoryItem.code LIKE'] = '%'.$_GET['code'].'%';
			}

			if($_GET['inventory_item_category_id'] != '') {
				$conditions['InventoryItem.inventory_item_category_id'] = (int)$_GET['inventory_item_category_id'];
			} 
 
 
			$this->request->data['InventoryItem'] = $_GET;
		} 

		// 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer,  
		$conditions['InventoryItem.status'] = array(0, 1); 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array( 
			'conditions' => $conditions,
			'order'=>'InventoryItem.code ASC', 
			'limit'=>$record_per_page, 
			'recursive' => 0
			); 
		$items = $this->Paginator->paginate();

		$views = array();
		foreach ($items as $item) {
			$views[] = array(
				'InventoryItem' => $item['InventoryItem'],
				'InventoryItemCategory' => $item['InventoryItemCategory'],
				'GeneralUnit' => $item['GeneralUnit'],
				'Stock' => $this->sum_stock($item['InventoryItem']['id']),
				'Mrn' => $this->sum_mrn($item['InventoryItem']['id'])
				);
		}

		$this->set('inventoryItems', $views); 
		$categories = $this->InventoryItem->InventoryItemCategory->find('list');
		/*
		, array(
			'order' => array('InventoryItemCategory.name' => 'ASC', 'InventoryItemCategory.id' => 'ASC')
			)
			*/
		$locations = $this->InventoryItem->InventoryLocation->find('list');
		$stores = $this->InventoryItem->InventoryStore->find('list');

		$this->set(compact('categories', 'locations', 'stores'));
	}

	private function sum_stock($item_id) {
		$this->loadModel('InventoryStock');
		$stocks = $this->InventoryStock->find('all', array(
			'fields' => array(
				'SUM(InventoryStock.quantity) AS stock'
				),
			'conditions' => array(
				'InventoryStock.inventory_item_id' => $item_id
				),
			'recursive' => -1
			));
		if($stocks) {
			foreach ($stocks as $stock) {
				return $stock[0]['stock'];
			}
		} else {
			return 0;
		} 
	}

	private function sum_mrn($item_id) {
		$this->loadModel('InventoryMaterialRequestItem');
		$stocks = $this->InventoryMaterialRequestItem->find('all', array(
			'fields' => array(
				'SUM(InventoryMaterialRequestItem.issued_balance) AS issued_balance'
				),
			'conditions' => array(
				'InventoryMaterialRequestItem.inventory_item_id' => $item_id
				),
			'recursive' => -1
			));
		if($stocks) {
			foreach ($stocks as $stock) {
				return $stock[0]['issued_balance'];
			}
		} else {
			return 0;
		} 
	}

	public function transfer() {
		$this->InventoryItem->recursive = 0;
		$this->set('inventoryItems', $this->Paginator->paginate());
	}
 

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item'));
		}
		$options = array('conditions' => array('InventoryItem.' . $this->InventoryItem->primaryKey => $id));
		$this->set('inventoryItem', $this->InventoryItem->find('first', $options));

 
		$ditem = $this->InventoryItem->find('first', array('conditions' => array('InventoryItem.id' => $id)));
 

		//print_r($ditem);
		$this->loadModel('InventoryStore');
		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE id=".$ditem['InventoryItem']['inventory_store_id']);

		$this->loadModel('InventoryRack');
		$rack = $this->InventoryRack->query("SELECT * FROM inventory_racks WHERE inventory_store_id=".$ditem['InventoryItem']['inventory_store_id']);
		//exit;
		$this->loadModel('InventoryDeliveryLocation');

		$warehouse_location = $this->InventoryStore->query("SELECT * FROM inventory_delivery_locations WHERE id=".$ditem['InventoryItem']['inventory_delivery_location_id']);

		$this->set(compact('store', 'warehouse_location', 'rack'));
 
	}

	public function exchange($id = null) {
		if (!$this->InventoryItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item'));
		}

		if ($this->request->is('post')) {
			$this->InventoryItem->create();
			
			$id = $_POST['inventory_item_id'];

			if($_POST['transfer_type']=='1'){
				$warehouse_id = $_POST['inventory_delivery_location_id'];
				$store_id = $_POST['inventory_store_id'];

				$quantity_item = $this->InventoryItem->query("SELECT * FROM inventory_items WHERE id='".$id."'");
	
				$quantity = $quantity_item[0]['inventory_items']['quantity'];

				$data_item = array(
					'id' 	=> $id,
					'inventory_delivery_location_id' 	=> $warehouse_id,
					'inventory_store_id' 				=> $store_id
				);


				if($this->InventoryItem->save($data_item)){
					$this->loadModel('InventoryMaterialHistory');
					$this->InventoryMaterialHistory->create();
					$data_history = array(
						'user_id' => $_SESSION['Auth']['User']['id'],
						'inventory_item_id' => $id,
						'quantity' => $quantity,
						'status'   => '1',
						'type'     => '1'
					);

					if($this->InventoryMaterialHistory->save($data_history)){
						$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
						return $this->redirect(array('action' => 'transfer'));
					}else{
						$this->Session->setFlash(__('The inventory item could not be save.'), 'error');
					}					
				}else{
					$this->Session->setFlash(__('The inventory item could not be save.'), 'error');
				}
			}else if($_POST['transfer_type']=='2'){
				$store_id = $_POST['inventory_store_id'];
				$quantity_item = $this->InventoryItem->query("SELECT * FROM inventory_items WHERE id='".$id."'");
	
				$quantity = $quantity_item[0]['inventory_items']['quantity'];

				$data_item = array(
					'id' 	=> $id,
					'inventory_store_id' => $store_id
				);

				if($this->InventoryItem->save($data_item)){
					$this->loadModel('InventoryMaterialHistory');
					$this->InventoryMaterialHistory->create();
					$data_history = array(
						'user_id' => $_SESSION['Auth']['User']['id'],
						'inventory_item_id' => $id,
						'quantity' => $quantity,
						'status'   => '1',
						'type'     => '2'
					);

					if($this->InventoryMaterialHistory->save($data_history)){
						$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
						return $this->redirect(array('action' => 'transfer'));
					}else{
						$this->Session->setFlash(__('The inventory item could not be save.'), 'error');
					}
				}else{
					$this->Session->setFlash(__('The inventory item could not be save.'), 'error');
				}
			}else{
				$code = $_POST['code'];
				$cur_item = $this->InventoryItem->query("SELECT * FROM inventory_items WHERE id='".$id."'");
				
				$check_code = $this->InventoryItem->query("SELECT * FROM inventory_items WHERE code='".$code."'");
				
				if(empty($check_code)){
					$this->Session->setFlash(__('The code could not be found saved.'), 'error');
				}else{
					$quantity = $cur_item[0]['inventory_items']['quantity'];
					$new_quantity = $quantity - $_POST['quantity'];

					$data_item = array(
						'id'	=> $id,
						'quantity' => $new_quantity
					);

					$this->InventoryItem->save($data_item);

					$quantity_2 = $check_code[0]['inventory_items']['quantity'];
					$new_quantity_2 = $quantity_2 + $_POST['quantity'];

					$data_item = array(
						'id' 	=> $check_code[0]['inventory_items']['id'],
						'quantity' => $new_quantity_2
					);

					if($this->InventoryItem->save($data_item)){
						$this->loadModel('InventoryMaterialHistory');
						$this->InventoryMaterialHistory->create();
						$data_history = array(
							'user_id' => $_SESSION['Auth']['User']['id'],
							'inventory_item_id' => $id,
							'quantity' => $_POST['quantity'],
							'status'   => '1',
							'type'     => '3'
						);

						if($this->InventoryMaterialHistory->save($data_history)){
							$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
							return $this->redirect(array('action' => 'transfer'));
						}else{
							$this->Session->setFlash(__('The inventory item could not be save.'), 'error');
						}

						$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
						return $this->redirect(array('action' => 'transfer'));
					}else{
						$this->Session->setFlash(__('The inventory item could not be save.'), 'error');
					}
				}
			}
		}

		$this->loadModel('TransferType');
		$transfer = $this->TransferType->find('list');

		$this->loadModel('InventoryDeliveryLocation');
		$warehouse = $this->InventoryDeliveryLocation->find('list');

		$options = array('conditions' => array('InventoryItem.' . $this->InventoryItem->primaryKey => $id));
		$this->set('inventoryItem', $this->InventoryItem->find('first', $options));

		$ditem = $this->InventoryItem->find('first', array('conditions' => array('InventoryItem.id' => $id)));

		$this->loadModel('InventoryStore');

		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE id=".$ditem['InventoryItem']['inventory_store_id']);
		$store_2 = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE inventory_delivery_location_id=".$ditem['InventoryItem']['inventory_delivery_location_id']);

		$this->loadModel('InventoryDeliveryLocation');

		$warehouse_location = $this->InventoryStore->query("SELECT * FROM inventory_delivery_locations WHERE id=".$ditem['InventoryItem']['inventory_delivery_location_id']);

		$this->set(compact('transfer', 'warehouse', 'store', 'warehouse_location', 'store_2'));
 
	}

	public function view_stock($id = null) {
		if (!$this->InventoryItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item'));
		}
		$options = array('conditions' => array('InventoryItem.' . $this->InventoryItem->primaryKey => $id));
		$this->set('inventoryItem', $this->InventoryItem->find('first', $options));

		$ditem = $this->InventoryItem->find();
		
		$this->loadModel('InventoryStore');
		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE id=".$ditem['InventoryItem']['inventory_store_id']);
		
		$this->loadModel('InventoryRack');
		$rack = $this->InventoryRack->query("SELECT * FROM inventory_racks WHERE inventory_store_id=".$ditem['InventoryItem']['inventory_store_id']);
		//exit;
		$this->loadModel('InventoryDeliveryLocation');

		$warehouse_location = $this->InventoryStore->query("SELECT * FROM inventory_delivery_locations WHERE id=".$ditem['InventoryItem']['inventory_delivery_location_id']);

		$this->set(compact('store', 'warehouse_location', 'rack'));
	}
	

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {  
 			$user_id = $this->Session->read('Auth.User.id');
			$this->InventoryItem->create();
			$this->request->data['InventoryItem']['status'] = 1;
			$this->request->data['InventoryItem']['user_id'] = $user_id; 
			$this->request->data['InventoryItem']['created'] = $this->date;
			$this->request->data['InventoryItem']['modified'] = $this->date; 

			if ($this->InventoryItem->save($this->request->data)) {
				$last_id = $this->InventoryItem->getLastInsertID();
 				
 				$this->insert_log($user_id, 'Add new Item General', 'inventory_items/view/'.$last_id, 'Add Item General');
	            //now do the save
	            $this->loadModel('InventoryItemMedia');

	            $this->request->data['InventoryItemMedia']['inventory_item_id'] = $last_id;  

	            if(!$this->InventoryItemMedia->save($this->request->data['InventoryItemMedia'])){
	            	$this->Session->setFlash(__('The media not saved.'), 'error');
	            	return false;
	            } 
	             
				$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
 
			} else {
				$this->Session->setFlash(__('The inventory item could not be saved. Please, try again.'));
			}
		}
		$inventoryItemCategories = $this->InventoryItem->InventoryItemCategory->find('list', array(
			'recursive' => -1
			));
		$generalUnits = $this->InventoryItem->GeneralUnit->find('list', array(
			'recursive' => -1
			));
		$locations = $this->InventoryItem->InventoryLocation->find('list', array(
			'recursive' => -1
			));
		$stores = $this->InventoryItem->InventoryStore->find('list', array(
			'recursive' => -1
			));
		$rack = $this->InventoryItem->InventoryRack->find('list', array(
			'recursive' => -1
			));

		$Qc = array('0' => 'No', '1' => 'Yes');

		$this->set(compact('inventoryItemCategories', 'generalUnits', 'Qc', 'locations', 'stores', 'rack'));
	}

	public function copy($id = null) {
		$item = $this->InventoryItem->find('first', array(
			'conditions' => array(
				'InventoryItem.id' => $id
				)
			));
		
		if ($this->request->is(array('post', 'put'))) {
 			$user_id = $this->Session->read('Auth.User.id'); 
			$this->InventoryItem->create();
			$this->request->data['InventoryItem']['status'] = 1;
			$this->request->data['InventoryItem']['user_id'] = $user_id; 
			$this->request->data['InventoryItem']['created'] = $this->date;
			$this->request->data['InventoryItem']['modified'] = $this->date; 

			if ($this->InventoryItem->save($this->request->data)) {
				$last_id = $this->InventoryItem->getLastInsertID();
 				
 				$this->insert_log($user_id, 'Copy Item General', 'inventory_items/view/'.$last_id, 'Copy Item General');
	            //now do the save
	             
	             
				$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
 
			} else {
				$this->Session->setFlash(__('The inventory item could not be saved. Please, try again.'), 'error');
			} 
		} else {
			$this->request->data = $item;
		}

		$inventoryItemCategories = $this->InventoryItem->InventoryItemCategory->find('list', array(
			'recursive' => -1
			));
		$generalUnits = $this->InventoryItem->GeneralUnit->find('list', array(
			'recursive' => -1
			));
		$locations = $this->InventoryItem->InventoryLocation->find('list', array(
			'recursive' => -1
			));
		$stores = $this->InventoryItem->InventoryStore->find('list', array(
			'recursive' => -1
			));
		 
		$Qc = array('1' => 'Yes', '0' => 'No');

		$this->set(compact('inventoryItemCategories', 'generalUnits', 'Qc', 'locations', 'stores'));

		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryItem->exists($id)) {
			throw new NotFoundException(__('Invalid inventory item'));
		}

		$options = array('conditions' => array('InventoryItem.' . $this->InventoryItem->primaryKey => $id));

		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryItem->save($this->request->data)) {

				$this->insert_log($this->user_id, 'Edit Item General', 'inventory_items/view/'.$id, 'Edit Item General');

				$this->Session->setFlash(__('The inventory item has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory item could not be saved. Please, try again.'));
			}
		} else {
			
			$this->request->data = $this->InventoryItem->find('first', $options);
		}

		$inventoryItem = $this->InventoryItem->find('first', $options);

		$this->set('inventoryItem', $inventoryItem);

		$inventoryItemCategories = $this->InventoryItem->InventoryItemCategory->find('list', array(
			'recursive' => -1
			));
		$generalUnits = $this->InventoryItem->GeneralUnit->find('list', array(
			'recursive' => -1
			));
		$locations = $this->InventoryItem->InventoryLocation->find('list', array(
			'recursive' => -1
			));
		$stores = $this->InventoryItem->InventoryStore->find('list', array(
			'recursive' => -1
			));
		$rack = $this->InventoryItem->InventoryRack->find('list', array(
			'recursive' => -1
			));

		$Qc = array('1' => 'Yes', '0' => 'No');

		$this->set(compact('inventoryItemCategories', 'generalUnits', 'Qc', 'locations', 'stores', 'rack'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryItem->id = $id;
		if (!$this->InventoryItem->exists()) {
			throw new NotFoundException(__('Invalid inventory item'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->InventoryItem->delete()) {
			$this->Session->setFlash(__('The inventory item has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The inventory item could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function ajaxlocation() {
		$id = $_POST['id'];
		$this->loadModel('InventoryStore');

		$store = $this->InventoryStore->query("SELECT * FROM inventory_stores WHERE inventory_delivery_location_id=".$id);

		$json = array();

		for($i=0; $i<count($store); $i++){
			$json[] = array('id' => $store[$i]['inventory_stores']['id'], 'name' => $store[$i]['inventory_stores']['name']);
		}

		echo json_encode($json);
		exit;
	}
}
