<?php
App::uses('AppController', 'Controller');
/**
 * Groups Controller
 *
 * @property Group $Group
 * @property PaginatorComponent $Paginator 
 * Like: www.facebook.com/w3itexperts
 */
class GroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


	public function beforeFilter() {
		parent::beforeFilter();
	} 
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$conditions=array();
		
		/* Filter With Paging */
        if(!empty($this->data)) {         
            $this->Session->write('Filter.Group',$this->data);
        }
        elseif((!empty($this->passedArgs['page']) || strpos($this->referer(),'groups/index')) && $this->Session->check('Filter.Group')) {
            $this->request->data['Group'] = $this->Session->check('Filter.Group')? $this->Session->read('Filter.Group') : '';
        }
        else
        {
            $this->Session->delete('Filter.Group');
        }
        /* End Filter With Paging */
		
		if(!empty($this->data)) {
			if(!empty($this->data['Group']['id'])) {
				$conditions['Group.id']=$this->data['Group']['id'];
			}
			if(!empty($this->data['Group']['name'])) {
				$conditions['Group.name LIKE']='%'.$this->data['Group']['name'].'%';
			}
			if(isset($this->data['Group']['from']) && !empty($this->data['Group']['from'])) {
				$conditions['Group.created >='] = $this->data['Group']['from'];
			}
			if(isset($this->data['Group']['to']) && !empty($this->data['Group']['to'])) {
				$conditions['Group.created <='] = $this->data['Group']['to'].' 23:59:59';
			}
		}
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'Group.created DESC','limit'=>$record_per_page);
		//$this->Group->recursive = 0;
		$this->set('groups', $this->Paginator->paginate());
		
	}


/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Group->create();
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('The group has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The group could not be saved. Please, try again.'),'error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('The group has been saved.'),'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The group could not be saved. Please, try again.'),'error');
			}
		} else {
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			$this->request->data = $this->Group->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			$this->Session->setFlash(__('Invalid group id.'),'error');
		}
		if($this->Group->User->hasAny(array('User.group_id'=>$id))) {
			$this->Session->setFlash(__('User exist in this group.'),'error');
		}
		if($id == $this->Session->read('Auth.User.group_id')){
			$this->Session->setFlash(__('Group can not delete.'),'error');
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Group->delete()) {
			$this->Session->setFlash(__('The group has been deleted.'),'success');
		} else {
			$this->Session->setFlash(__('The group could not be deleted. Please, try again.'),'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

}
