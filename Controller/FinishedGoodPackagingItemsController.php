<?php
App::uses('AppController', 'Controller');
/**
 * FinishedGoodPackagingItems Controller
 *
 * @property FinishedGoodPackagingItem $FinishedGoodPackagingItem
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FinishedGoodPackagingItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->FinishedGoodPackagingItem->recursive = 0;
		$this->set('finishedGoodPackagingItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->FinishedGoodPackagingItem->exists($id)) {
			throw new NotFoundException(__('Invalid finished good packaging item'));
		}
		$options = array('conditions' => array('FinishedGoodPackagingItem.' . $this->FinishedGoodPackagingItem->primaryKey => $id));
		$this->set('finishedGoodPackagingItem', $this->FinishedGoodPackagingItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->FinishedGoodPackagingItem->create();
			if ($this->FinishedGoodPackagingItem->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good packaging item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good packaging item could not be saved. Please, try again.'));
			}
		}
		$finishedGoodPackagings = $this->FinishedGoodPackagingItem->FinishedGoodPackaging->find('list');
		$finishedGoods = $this->FinishedGoodPackagingItem->FinishedGood->find('list');
		$productionOrders = $this->FinishedGoodPackagingItem->ProductionOrder->find('list');
		$this->set(compact('finishedGoodPackagings', 'finishedGoods', 'productionOrders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->FinishedGoodPackagingItem->exists($id)) {
			throw new NotFoundException(__('Invalid finished good packaging item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->FinishedGoodPackagingItem->save($this->request->data)) {
				$this->Session->setFlash(__('The finished good packaging item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The finished good packaging item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('FinishedGoodPackagingItem.' . $this->FinishedGoodPackagingItem->primaryKey => $id));
			$this->request->data = $this->FinishedGoodPackagingItem->find('first', $options);
		}
		$finishedGoodPackagings = $this->FinishedGoodPackagingItem->FinishedGoodPackaging->find('list');
		$finishedGoods = $this->FinishedGoodPackagingItem->FinishedGood->find('list');
		$productionOrders = $this->FinishedGoodPackagingItem->ProductionOrder->find('list');
		$this->set(compact('finishedGoodPackagings', 'finishedGoods', 'productionOrders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->FinishedGoodPackagingItem->id = $id;
		if (!$this->FinishedGoodPackagingItem->exists()) {
			throw new NotFoundException(__('Invalid finished good packaging item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->FinishedGoodPackagingItem->delete()) {
			$this->Session->setFlash(__('The finished good packaging item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The finished good packaging item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
