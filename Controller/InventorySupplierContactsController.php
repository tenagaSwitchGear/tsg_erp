<?php
App::uses('AppController', 'Controller');
/**
 * InventorySupplierContacts Controller
 *
 * @property InventorySupplierContact $InventorySupplierContact
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventorySupplierContactsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventorySupplierContact->recursive = 0;
		$this->set('inventorySupplierContacts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventorySupplierContact->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier contact'));
		}
		$options = array('conditions' => array('InventorySupplierContact.' . $this->InventorySupplierContact->primaryKey => $id));
		$this->set('inventorySupplierContact', $this->InventorySupplierContact->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventorySupplierContact->create();
			if ($this->InventorySupplierContact->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier contact has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier contact could not be saved. Please, try again.'), 'error');
			}
		}
		$inventorySuppliers = $this->InventorySupplierContact->InventorySupplier->find('list');
		$this->set(compact('inventorySuppliers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventorySupplierContact->exists($id)) {
			throw new NotFoundException(__('Invalid inventory supplier contact'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventorySupplierContact->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory supplier contact has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory supplier contact could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('InventorySupplierContact.' . $this->InventorySupplierContact->primaryKey => $id));
			$this->request->data = $this->InventorySupplierContact->find('first', $options);
		}
		$inventorySuppliers = $this->InventorySupplierContact->InventorySupplier->find('list');
		$this->set(compact('inventorySuppliers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventorySupplierContact->id = $id;
		if (!$this->InventorySupplierContact->exists()) {
			throw new NotFoundException(__('Invalid inventory supplier contact'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventorySupplierContact->delete()) {
			$this->Session->setFlash(__('The inventory supplier contact has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The inventory supplier contact could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
