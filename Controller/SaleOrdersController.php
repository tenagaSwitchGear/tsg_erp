<?php
App::uses('AppController', 'Controller');
/**
 * SaleOrders Controller
 *
 * @property SaleOrder $SaleOrder
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleOrdersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxfindsaleorder');
	}

	public function report($id = null) {
		if($id == null) {
			$status = 1;
		} else {
			$status = $id;
		}  

		$conditions = array(); 

		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['SaleOrder.name'] = trim($_GET['name']);
			}
			if($_GET['job'] != '') {
				$conditions['SaleJobChild.station_name'] = trim($_GET['job']);
			}
			if($_GET['from'] != '') {
				$conditions['SaleOrder.date >='] = trim($_GET['from']);
			}
			if($_GET['to'] != '') {
				$conditions['SaleOrder.date <='] = trim($_GET['to']);
			}
			$this->request->data['SaleOrder'] = $_GET;
		}

		$conditions['SaleOrder.status'] = $status;


		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'SaleOrder.id DESC',
			'limit'=>200
			);  
		$reports = $this->Paginator->paginate();
		$saleorders = array();
		foreach ($reports as $report) {
			$saleorders[] = array(
				'SaleOrder' => $report['SaleOrder'],
				'SaleJobChild' => $report['SaleJobChild'],
				'Customer' => $this->get_so_customer($report['SaleJob']['customer_id']),
				'Sum' => $this->sum_items($report['SaleOrder']['id']),
				'SumDelivered' => $this->sum_delivered($report['SaleOrder']['id'])
				);
		}

		$this->set('saleOrders', $saleorders);
	}

	private function sum_delivered($sale_order_item_id) {
		$this->loadModel('DeliveryNoteItem');
		$sums = $this->DeliveryNoteItem->find('all', array(
			'fields' => array(
				'SUM(SaleOrderItem.unit_price) as unit_price', 
				'SUM(SaleOrderItem.discount) as discount',
				'SUM(SaleOrderItem.total_price) as total_price',
				'SUM(SaleOrderItem.quantity) as quantity',
				'SUM(SaleOrderItem.tax) as tax'
				),
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $sale_order_item_id
				)
			));
		foreach ($sums as $sum) { 
			return $sum[0];
		}
	}

	private function get_so_customer($customer_id) {
		$this->loadModel('Customer');
		$customer = $this->Customer->find('first', array( 
			'conditions' => array(
				'Customer.id' => $customer_id
				) 
			));
		return $customer['Customer'];
	}
	
	private function sum_items($sale_order_id) {
		$this->loadModel('SaleOrderItem');
		$sums = $this->SaleOrderItem->find('all', array(
			'fields' => array(
				'SUM(SaleOrderItem.unit_price) as unit_price', 
				'SUM(SaleOrderItem.discount) as discount',
				'SUM(SaleOrderItem.total_price) as total_price',
				'SUM(SaleOrderItem.quantity) as quantity',
				'SUM(SaleOrderItem.tax) as tax'
				),
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $sale_order_id
				),
			'recursive' => -1
			));
		foreach ($sums as $sum) {
			return $sum[0];
		}
	}

	public function index($id = null) {
		if($id == null) {
			$status = 1;
		} else {
			$status = $id;
		}

		$conditions = array(); 
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['SaleOrder.name'] = trim($_GET['name']);
			}
			if($_GET['job'] != '') {
				$conditions['SaleJobChild.station_name'] = trim($_GET['job']);
			}
			if($_GET['from'] != '') {
				$conditions['SaleOrder.date >='] = trim($_GET['from']);
			}
			if($_GET['to'] != '') {
				$conditions['SaleOrder.date <='] = trim($_GET['to']);
			}
			$this->request->data['SaleOrder'] = $_GET;
		}
		$conditions['SaleOrder.status'] = $status; 

		if($this->group_id != 1) {
			$conditions['SaleOrder.group_id'] = $this->group_id; 
		}

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'SaleOrder.id DESC',
			'limit' => $record_per_page
			);  

		$this->set('saleOrders', $this->Paginator->paginate());
	}

	public function ajaxfindsaleorder() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$conditions['OR']['SaleOrder.name LIKE'] = '%'.$term.'%'; 

			$items = $this->SaleOrder->find('all', array('conditions' => $conditions, 'group' => 'SaleOrder.id'));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['SaleOrder']['id'], 
					'name' => $item['SaleOrder']['name'], 
					'sale_job_id' => $item['SaleOrder']['sale_job_id'], 
					'sale_job_child_id' => $item['SaleOrder']['sale_job_child_id'], 
					'sale_quotation_id' => $item['SaleOrder']['sale_quotation_id'], 
					'customer_id' => $item['SaleQuotation']['customer_id'], 
					'sale_tender_id' => $item['SaleQuotation']['sale_tender_id'], 
					'customer_name' => $this->get_customer($item['SaleQuotation']['customer_id'])['name']); 
			}
			echo json_encode($json);	
		}
	}

	public function ajaxfindsaleorderitem() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();
			$this->loadModel('SaleOrderItem');
			$conditions['SaleOrderItem.sale_order_id'] = $term; 

			$items = $this->SaleOrderItem->find('all', array('conditions' => $conditions, 'group' => 'SaleOrderItem.id'));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = $item; 
			}
			echo json_encode($json);	
		}
	}

	private function get_customer($id) {
		$this->loadModel('Customer');
		$customer = $this->Customer->find('first', array('conditions' => array(
			'Customer.id' => $id
			),
			'reqursive' => -1
		));
		if($customer) {
			return $customer['Customer'];
		} 
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleOrder->exists($id)) {
			throw new NotFoundException(__('Invalid sale order'));
		}
		$options = array('conditions' => array('SaleOrder.' . $this->SaleOrder->primaryKey => $id));

		$items = $this->SaleOrder->SaleOrderItem->find('all', array(
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $id
				)
			));

		$this->set('items', $items);

		$saleorder = $this->SaleOrder->find('first', $options);

		$this->loadModel('Customer');
		$customer = $this->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $saleorder['SaleQuotation']['customer_id']
				)
			));

		$this->set('customer', $customer);

		$this->set('saleOrder', $saleorder);
	}

	public function add() { 
		//$item = $this->copy_bom_item(97, 43, 13, 13, 2);

		//var_dump($item);

		$user_id = $this->Session->read('Auth.User.id');

		if ($this->request->is('post')) { 
			//print_r($_POST);
			//exit;
			$this->SaleOrder->create(); 

			$this->request->data['SaleOrder']['date_fat'] = $this->date;
			$this->request->data['SaleOrder']['created'] = $this->date;
			$this->request->data['SaleOrder']['modified'] = $this->date;
			$this->request->data['SaleOrder']['user_id'] = $this->user_id;

			$count = $this->SaleOrder->find('all', array(
				'order' => array('SaleOrder.id' => 'DESC')
				));

			$no = count($count);
			$so_no = $this->generate_code('SO', $no + 1);
			$this->request->data['SaleOrder']['name'] = $so_no;
			$job_id = $this->request->data['SaleOrder']['sale_job_id'];
			$job_child_id = $this->request->data['SaleOrder']['sale_job_child_id'];
			$term_of_payment_id = $this->request->data['SaleOrder']['term_of_payment_id'];

			if ($this->SaleOrder->save($this->request->data)) {
				$id = $this->SaleOrder->getLastInsertId();
				// lOG 
				$name = 'Add Sale Order : ' . $so_no;
				$link = 'sale_orders/view/'.$id;
				$type = 'Add Sale Order';
				
				$this->insert_log($this->user_id, $name, $link, $type);

				$sale_order_id = $this->SaleOrder->getLastInsertId(); 
				
				$this->loadModel('SaleOrderItem');
				$this->loadModel('SaleJobItem');
				$this->loadModel('ProjectBom');

				$this->loadModel('ProjectBudget');
				$this->loadModel('ProjectBudgetItem');
				//if(isset($this->request->data['SaleOrderItem']['product_id'])) {

					$product_id = $this->request->data['SaleOrderItem']['inventory_item_id'];
					$quantity = $this->request->data['SaleOrderItem']['quantity'];
					$general_unit_id = $this->request->data['SaleOrderItem']['general_unit_id'];
					$unit_price = $this->request->data['SaleOrderItem']['unit_price'];
					$discount = $this->request->data['SaleOrderItem']['discount'];
					$tax = $this->request->data['SaleOrderItem']['tax'];
					$discount_type = $this->request->data['SaleOrderItem']['discount_type']; 
					$bom_id = $this->request->data['SaleOrderItem']['bom_id']; 

					$this->loadModel('SaleQuotationItem');
					$this->loadModel('SaleQuotationItemCost'); 
					for($i = 0; $i < count($product_id); $i++) {
						$total_price = $unit_price[$i] * $quantity[$i];
						$this->SaleOrderItem->create(); 
						$data = array(
							'quantity' => $quantity[$i],
							'general_unit_id' => $general_unit_id[$i],
							'unit_price' => $unit_price[$i],
							'discount' => $discount[$i],
							'discount_type' => $discount_type[$i],
							'tax' => $tax[$i],
							'total_price' => $this->calc_total($quantity[$i], $discount[$i], $discount_type[$i], $unit_price[$i], $tax[$i]),
							'inventory_item_id' => $product_id[$i],
							'sale_order_id' => $sale_order_id,
							'type' => 0,
							'sale_job_id' => $job_id,
							'sale_job_child_id' => $job_child_id,
							'bom_id' => $bom_id[$i]  
							);

						if(!$this->SaleOrderItem->save($data)) {
							die('Error');
						}
						$order_item_id = $this->SaleOrderItem->getLastInsertId(); 

						$iteminfo = $this->bom_info($bom_id[$i]); 

						$this->SaleJobItem->create();  
						$data = array(
							'quantity' => $quantity[$i],
							'general_unit_id' => $general_unit_id[$i],
							'unit_price' => 0,
							'discount' => 0,
							'tax' => 0,
							'total_price' => 0, 
							'bom_id' => $bom_id[$i],
							'name' => 0,
							'code' => 0,
							'remark' => 'Remark',
							'sale_tender_id' => 0,
							'sale_job_id' => $job_id,
							'sale_job_child_id' => $job_child_id,
							'created' => $this->date,
							'modified' => $this->date,
							'user_id' => $this->user_id,
							'status' => 0, 
							'inventory_item_id' => $product_id[$i],
							'type' => 1 
							);
						$this->SaleJobItem->save($data);
						$job_item_id = $this->SaleJobItem->getLastInsertId();
						//debug($this->SaleOrderItem->getDataSource()->getLog(false, false));
						// Save Budget 
						// Find costing $product_id[$i]
						
						$cost = $this->SaleQuotationItem->find('first', array(
							'conditions' => array(
								'SaleQuotationItem.inventory_item_id' => $product_id[$i],
								'SaleQuotationItem.costing' => 1,
								'SaleQuotation.status' => 4
								)
							));
						if($cost) {
							$this->ProjectBudget->create(); 
							$budgets = array(
								'inventory_item_id' => $product_id[$i],
								'sale_job_item_id' => $job_item_id,
								'sale_order_item_id' => $order_item_id,
								'sale_quotation_item_id' => $cost['SaleQuotationItem']['id'], 
								'sale_job_id' => $job_id, 
								'sale_job_child_id' => $job_child_id, 
								'indirect_cost' => $cost['SaleQuotationItem']['indirect_cost'], 
								'soft_cost' => $cost['SaleQuotationItem']['soft_cost'], 
								'material_cost' => $cost['SaleQuotationItem']['material_cost'], 
								'margin' => $cost['SaleQuotationItem']['planning_margin'], 
								'margin_value' => $cost['SaleQuotationItem']['margin_value'], 
								'planning_price' => $cost['SaleQuotationItem']['planning_price'], //$quantity[$i]
								'amount' => $cost['SaleQuotationItem']['planning_price'] * $quantity[$i], 
								'used' => 0, 
								'balance' => $cost['SaleQuotationItem']['planning_price'] * $quantity[$i], 
								'created' => $this->date,   
								'user_id' => $this->user_id, 
								'status' => 0,
								'type' => 'Item Budget'
								);
							$this->ProjectBudget->save($budgets);  	
							$project_budget_id = $this->ProjectBudget->getLastInsertId();
							$costs = $this->SaleQuotationItemCost->find('all', array(
								'conditions' => array(
									'SaleQuotationItemCost.sale_quotation_item_id' => $cost['SaleQuotationItem']['id']
									)
								));
							foreach ($costs as $costing) {
								$this->ProjectBudgetItem->create();
								if($costing['SaleQuotationItemCost']['type'] == 'Soft Cost') {
									$cos = $cost['SaleQuotationItem']['indirect_cost'] + $cost['SaleQuotationItem']['material_cost'];
									$price = ($cos / 100) * $costing['SaleQuotationItemCost']['margin'];
									$total_price = $price * $quantity[$i];
								} else {
									$price = $costing['SaleQuotationItemCost']['total_price'];
									$total_price = $price * $quantity[$i];
								}
								$data = array(
									'project_budget_id' => $project_budget_id, 
									'sale_job_child_id' => $job_child_id, 
									'sale_order_item_id' => $job_item_id, 
									'inventory_item_id' => $costing['SaleQuotationItemCost']['inventory_item_id'], 
									'quantity' => $costing['SaleQuotationItemCost']['quantity'], 
									'total_quantity' => $costing['SaleQuotationItemCost']['quantity'] * $quantity[$i], 
									'general_unit_id' => $costing['SaleQuotationItemCost']['general_unit_id'], 
									'margin' => $costing['SaleQuotationItemCost']['margin'], 
									'price' => $price,  
									'used' => 0,
									'balance' => $total_price,
									'total_price' => $total_price, 
									'type' => $costing['SaleQuotationItemCost']['type']
									);
								$this->ProjectBudgetItem->save($data);
							}
						}
						
						// Copy BOM For Planning Purpose
						$this->ProjectBom->create();  
						if($iteminfo['Bom']) {
							$data = array(
								'quantity' => $quantity[$i],
								'general_unit_id' => $general_unit_id[$i],
								'unit_price' => 0,
								'discount' => 0,
								'tax' => 0,
								'total_price' => 0, 
								'bom_id' => $bom_id[$i],
								'name' => $iteminfo['Bom']['name'],
								'code' => $iteminfo['Bom']['code'],
								'remark' => 'Remark',
								'sale_tender_id' => 0,
								'sale_job_id' => $job_id,
								'sale_job_child_id' => $job_child_id,
								'created' => $this->date,
								'modified' => $this->date,
								'user_id' => $user_id,
								'status' => 0
								);	
						} else {
							$data = array(
								'quantity' => $quantity[$i],
								'general_unit_id' => $general_unit_id[$i],
								'unit_price' => 0,
								'discount' => 0,
								'tax' => 0,
								'total_price' => 0, 
								'bom_id' => $bom_id[$i],
								'name' => $iteminfo['InventoryItem']['name'],
								'code' => $iteminfo['InventoryItem']['code'],
								'remark' => 'Remark',
								'sale_tender_id' => 0,
								'sale_job_id' => $job_id,
								'sale_job_child_id' => $job_child_id,
								'created' => $this->date,
								'modified' => $this->date,
								'user_id' => $user_id,
								'status' => 0
								);	
						} 
  
						$this->ProjectBom->save($data);
						// After save
						$project_bom_id = $this->ProjectBom->getLastInsertId(); 
						 
						$this->copy_bom_child($job_id, $bom_id[$i], $project_bom_id, $quantity[$i], $job_child_id);
					}
				//} 

				// Find acc dept budget
				$this->loadModel('AccountDepartment');
				$this->loadModel('AccountDepartmentBudget');
				$dept = $this->AccountDepartment->find('first', array(
					'conditions' => array(
						'AccountDepartment.group_id' => 12, // PPC
						),
					'order' => 'AccountDepartment.year DESC'
					));

				// Sum budget  
				$costs = $this->ProjectBudget->find('all', array(
					'fields' => array(
						'SUM(ProjectBudget.indirect_cost) AS indirect_cost',
						'SUM(ProjectBudget.soft_cost) AS soft_cost',
						'SUM(ProjectBudget.material_cost) AS material_cost',
						'SUM(ProjectBudget.margin_value) AS margin_value',
						'SUM(ProjectBudget.planning_price) AS planning_price',
						'SUM(ProjectBudget.amount) AS amount',
						'SUM(ProjectBudget.balance) AS balance',
						),
					'conditions' => array(
						'ProjectBudget.sale_job_id' => $job_id,
						'ProjectBudget.sale_job_child_id' => $job_child_id,
						'ProjectBudget.type' => 'Item Budget'
						),
					'group' => 'ProjectBudget.sale_job_child_id'
					));
				foreach($costs as $cost) { 
					$this->ProjectBudget->create(); 
					$budgets = array(
						'inventory_item_id' => 0,
						'sale_job_item_id' => $job_item_id,
						'sale_order_item_id' => $order_item_id,
						'sale_quotation_item_id' => 0, 
						'sale_job_id' => $job_id, 
						'sale_job_child_id' => $job_child_id, 
						'indirect_cost' => $cost[0]['indirect_cost'], 
						'soft_cost' => $cost[0]['soft_cost'], 
						'material_cost' => $cost[0]['material_cost'], 
						'margin' => 0, 
						'margin_value' => $cost[0]['margin_value'], 
						'planning_price' => $cost[0]['planning_price'],  
						'amount' => $cost[0]['amount'], 
						'used' => 0, 
						'balance' => $cost[0]['balance'],
						'created' => $this->date,   
						'user_id' => $this->user_id, 
						'status' => 0,
						'type' => 'Job Budget'
						);
					$this->ProjectBudget->save($budgets);  	 

					$this->loadModel('SaleJobChild');

					$job = $this->SaleJobChild->find('first', array(
						'conditions' => array(
							'SaleJobChild.id' => $job_child_id
							)
						));

					// Save budget acount
					$this->AccountDepartmentBudget->create();
					
					$budget[] = array(
						'account_department_id' => $dept['AccountDepartment']['id'],
						'name' => 'Material Cost (' . $job['SaleJobChild']['name'] . ')',
						'total' => $cost[0]['material_cost'],
						'year' => date('Y'),
						'used' => 0,
						'balance' => $cost[0]['material_cost'],
						'parent_id'  => 22,
						'sale_job_child_id' => $job_child_id,
						'type' => 3,
						'is_material' => 1
						);
					$savebudget = $this->AccountDepartmentBudget->saveAll($budget, array('deep' => false));
					if(!$savebudget) { 
						$this->Session->setFlash(__('The Sale Order has been saved but Budget cannot be save. Error code SO459.'), 'error');
					}

					// Save indirect cost and soft cost group by item_id 

					$budgetitems = $this->ProjectBudgetItem->find('all', array(
						'fields' => array(
							'InventoryItem.*',
							'ProjectBudgetItem.*',
							'SUM(ProjectBudgetItem.total_price) AS budget'
							),
						'conditions' => array(
							'ProjectBudgetItem.sale_job_child_id' => $job_child_id 
							),
						'group' => 'ProjectBudgetItem.inventory_item_id'
						));
					$accbudget = array();
					foreach ($budgetitems as $budgetitem) {
						 $accbudget[] = array(
							'account_department_id' => $dept['AccountDepartment']['id'],
							'name' => $budgetitem['InventoryItem']['name'] . ' (' . $job['SaleJobChild']['name'] . ')',
							'total' => $budgetitem[0]['budget'],
							'year' => date('Y'),
							'used' => 0,
							'balance' => $budgetitem[0]['budget'],
							'parent_id'  => 22,
							'sale_job_child_id' => $job_child_id,
							'type' => 3,
							'is_material' => 0
							); 
					}
					$saveaccbudget = $this->AccountDepartmentBudget->saveAll($accbudget, array('deep' => false));
					if(!$saveaccbudget) {
						$this->Session->setFlash(__('Error Code SO489. Please contact Developer.'), 'error');
					}
				}

				$this->Session->setFlash(__('The Sale Order has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
				
			} else {
				$this->Session->setFlash(__('The sale order could not be saved. Please, try again.'), 'error');
				 
			} 
		}   
		$this->loadModel('TermOfPayment');
		$terms = $this->TermOfPayment->find('list');

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list'); 
		$saleQuotations = $this->SaleOrder->SaleQuotation->find('list'); 

		$categories = $this->SaleOrder->SaleOrderCategory->find('list'); 

		$users = $this->SaleOrder->User->find('list'); 
		$this->set(compact('users', 'units', 'terms', 'categories'));
	}

	public function sts_project_add() {  

		$user_id = $this->Session->read('Auth.User.id');

		if ($this->request->is('post')) { 
			//print_r($_POST);
			//exit;
			$this->SaleOrder->create(); 

			$this->request->data['SaleOrder']['date_fat'] = $this->date;
			$this->request->data['SaleOrder']['created'] = $this->date;
			$this->request->data['SaleOrder']['modified'] = $this->date;
			$this->request->data['SaleOrder']['user_id'] = $this->user_id;

			$count = $this->SaleOrder->find('all', array(
				'order' => array('SaleOrder.id' => 'DESC')
				));

			$no = count($count);
			$so_no = $this->generate_code('SO', $no + 1);
			$this->request->data['SaleOrder']['name'] = $so_no;
			$job_id = $this->request->data['SaleOrder']['sale_job_id'];
			$job_child_id = $this->request->data['SaleOrder']['sale_job_child_id'];
			$term_of_payment_id = $this->request->data['SaleOrder']['term_of_payment_id'];
			$sale_quotation_id = $this->request->data['SaleOrder']['sale_quotation_id'];

			if ($this->SaleOrder->save($this->request->data)) {
				$id = $this->SaleOrder->getLastInsertId();
				// lOG 
				$name = 'Add Sale Order : ' . $so_no;
				$link = 'sale_orders/view/'.$id;
				$type = 'Add Sale Order';
				
				$this->insert_log($this->user_id, $name, $link, $type);

				$sale_order_id = $this->SaleOrder->getLastInsertId(); 
				
				$this->loadModel('SaleOrderItem');
				$this->loadModel('SaleJobItem');
				$this->loadModel('ProjectBom');

				$this->loadModel('ProjectBudget');
				$this->loadModel('ProjectBudgetItem'); 

				$product_id = $this->request->data['SaleOrderItem']['inventory_item_id'];
				$quantity = $this->request->data['SaleOrderItem']['quantity'];
				$general_unit_id = $this->request->data['SaleOrderItem']['general_unit_id'];
				$unit_price = $this->request->data['SaleOrderItem']['unit_price'];
				$discount = $this->request->data['SaleOrderItem']['discount'];
				$tax = $this->request->data['SaleOrderItem']['tax'];
				$discount_type = $this->request->data['SaleOrderItem']['discount_type']; 
				$bom_id = $this->request->data['SaleOrderItem']['bom_id']; 


				$this->loadModel('SaleQuotationItem');
				$this->loadModel('SaleQuotationItemCost'); 
				for($i = 0; $i < count($product_id); $i++) {
					$total_price = $unit_price[$i] * $quantity[$i];
					$this->SaleOrderItem->create(); 
					$data = array(
						'quantity' => $quantity[$i],
						'general_unit_id' => $general_unit_id[$i],
						'unit_price' => $unit_price[$i],
						'discount' => $discount[$i],
						'discount_type' => $discount_type[$i],
						'tax' => $tax[$i],
						'total_price' => $this->calc_total($quantity[$i], $discount[$i], $discount_type[$i], $unit_price[$i], $tax[$i]),
						'inventory_item_id' => $product_id[$i],
						'sale_order_id' => $sale_order_id,
						'type' => 0,
						'sale_job_id' => $job_id,
						'sale_job_child_id' => $job_child_id,
						'bom_id' => $bom_id[$i]  
						);

					if(!$this->SaleOrderItem->save($data)) {
						die('Error');
					}
					$order_item_id = $this->SaleOrderItem->getLastInsertId(); 

					$iteminfo = $this->bom_info($bom_id[$i]); 

					$this->SaleJobItem->create();  
					$data = array(
						'quantity' => $quantity[$i],
						'general_unit_id' => $general_unit_id[$i],
						'unit_price' => 0,
						'discount' => 0,
						'tax' => 0,
						'total_price' => 0, 
						'bom_id' => $bom_id[$i],
						'name' => 0,
						'code' => 0,
						'remark' => 'Remark',
						'sale_tender_id' => 0,
						'sale_job_id' => $job_id,
						'sale_job_child_id' => $job_child_id,
						'created' => $this->date,
						'modified' => $this->date,
						'user_id' => $this->user_id,
						'status' => 0, 
						'inventory_item_id' => $product_id[$i],
						'type' => 1 
						);
					$this->SaleJobItem->save($data);
					$job_item_id = $this->SaleJobItem->getLastInsertId();
					//debug($this->SaleOrderItem->getDataSource()->getLog(false, false));
					// Save Budget 
					// Find costing $product_id[$i]
					
					$cost = $this->SaleQuotationItem->find('first', array(
						'conditions' => array(
							'SaleQuotationItem.sale_quotation_id' => $sale_quotation_id,
							'SaleQuotationItem.inventory_item_id' => $product_id[$i],
							'SaleQuotationItem.costing' => 0,
							'SaleQuotation.group_id' => $this->group_id,
							//'SaleQuotation.status' => 10 // Awarded quotation
							)
						));

					if($cost) {
						$this->ProjectBudget->create(); 
						$budgets = array(
							'inventory_item_id' => $product_id[$i],
							'sale_job_item_id' => $job_item_id,
							'sale_order_item_id' => $order_item_id,
							'sale_quotation_item_id' => $cost['SaleQuotationItem']['id'], 
							'sale_job_id' => $job_id, 
							'sale_job_child_id' => $job_child_id, 
							'indirect_cost' => $cost['SaleQuotationItem']['indirect_cost'], 
							'soft_cost' => $cost['SaleQuotationItem']['soft_cost'], 
							'material_cost' => $cost['SaleQuotationItem']['material_cost'], 
							'margin' => $cost['SaleQuotationItem']['planning_margin'], 
							'margin_value' => $cost['SaleQuotationItem']['margin_value'], 
							'planning_price' => $cost['SaleQuotationItem']['planning_price'], //$quantity[$i]
							'amount' => $cost['SaleQuotationItem']['planning_price'] * $quantity[$i], 
							'used' => 0, 
							'balance' => $cost['SaleQuotationItem']['planning_price'] * $quantity[$i], 
							'created' => $this->date,   
							'user_id' => $this->user_id, 
							'status' => 0,
							'type' => 'Item Budget'
							);
						$this->ProjectBudget->save($budgets);  	

						$project_budget_id = $this->ProjectBudget->getLastInsertId();
						 
					}  
				} 

				// Find acc dept budget
				$this->loadModel('AccountDepartment');
				$this->loadModel('AccountDepartmentBudget');
				$dept = $this->AccountDepartment->find('first', array(
					'conditions' => array(
						'AccountDepartment.group_id' => $this->group_id, 
						),
					'order' => 'AccountDepartment.year DESC'
					));

				// Sum budget  
				$costs = $this->ProjectBudget->find('all', array(
					'fields' => array(
						'SUM(ProjectBudget.indirect_cost) AS indirect_cost',
						'SUM(ProjectBudget.soft_cost) AS soft_cost',
						'SUM(ProjectBudget.material_cost) AS material_cost',
						'SUM(ProjectBudget.margin_value) AS margin_value',
						'SUM(ProjectBudget.planning_price) AS planning_price',
						'SUM(ProjectBudget.amount) AS amount',
						'SUM(ProjectBudget.balance) AS balance',
						),
					'conditions' => array(
						'ProjectBudget.sale_job_id' => $job_id,
						'ProjectBudget.sale_job_child_id' => $job_child_id,
						'ProjectBudget.type' => 'Item Budget'
						),
					'group' => 'ProjectBudget.sale_job_child_id'
					));
				foreach($costs as $cost) { 
					$this->ProjectBudget->create(); 
					$budgets = array(
						'inventory_item_id' => 0,
						'sale_job_item_id' => $job_item_id,
						'sale_order_item_id' => $order_item_id,
						'sale_quotation_item_id' => 0, 
						'sale_job_id' => $job_id, 
						'sale_job_child_id' => $job_child_id, 
						'indirect_cost' => $cost[0]['indirect_cost'], 
						'soft_cost' => $cost[0]['soft_cost'], 
						'material_cost' => $cost[0]['material_cost'], 
						'margin' => 0, 
						'margin_value' => $cost[0]['margin_value'], 
						'planning_price' => $cost[0]['planning_price'],  
						'amount' => $cost[0]['amount'], 
						'used' => 0, 
						'balance' => $cost[0]['balance'],
						'created' => $this->date,   
						'user_id' => $this->user_id, 
						'status' => 0,
						'type' => 'Job Budget'
						);
					$this->ProjectBudget->save($budgets);  	  
					$this->loadModel('SaleJobChild'); 
					$job = $this->SaleJobChild->find('first', array(
						'conditions' => array(
							'SaleJobChild.id' => $job_child_id
							)
						));

					// Save budget acount
					$this->AccountDepartmentBudget->create(); 
					$budget[] = array(
						'account_department_id' => $dept['AccountDepartment']['id'],
						'name' => 'Material Cost (' . $job['SaleJobChild']['name'] . ')',
						'total' => $cost[0]['planning_price'],
						'year' => date('Y'),
						'used' => 0,
						'balance' => $cost[0]['planning_price'],
						'parent_id'  => 32,
						'sale_job_child_id' => $job_child_id,
						'type' => 3,
						'is_material' => 1
						);
					$savebudget = $this->AccountDepartmentBudget->saveAll($budget, array('deep' => false));
					if(!$savebudget) { 
						$this->Session->setFlash(__('The Sale Order has been saved but Budget cannot be save. Error code SO459.'), 'error');
					}
 
				}

				$this->Session->setFlash(__('The Sale Order has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
				
			} else {
				$this->Session->setFlash(__('The sale order could not be saved. Please, try again.'), 'error'); 
			} 
		}   
		$this->loadModel('TermOfPayment');
		$terms = $this->TermOfPayment->find('list');

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list'); 
		$saleQuotations = $this->SaleOrder->SaleQuotation->find('list'); 

		$categories = $this->SaleOrder->SaleOrderCategory->find('list'); 

		$users = $this->SaleOrder->User->find('list'); 
		$this->set(compact('users', 'units', 'terms', 'categories'));
	}

	private function bom_info($id) {
		$this->loadModel('Bom');
		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $id
				)
			));
		if($bom) {
			return $bom;
		} 
	}

	private function get_item_budget($inventory_item_id, $type) {

	}

	private function copy_bom_child($sale_job_id, $bom_id, $project_bom_id, $no_of_order, $job_child_id) {
		$this->loadModel('BomChild');
		$this->loadModel('ProjectBomChild');
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
				'BomChild.bom_id' => $bom_id
				),
			'group' => array('BomChild.id')
			));
		foreach ($childs as $child) {
			$this->ProjectBomChild->create();
			$data = array(
				'sale_job_id' => $sale_job_id,
				'sale_job_child_id' => $job_child_id,
				'project_bom_id' => $project_bom_id,
				'parent_id' => $child['BomChild']['parent_id'],
				'name' => $child['BomChild']['name'],
				'bom_parent' => $child['BomChild']['id'],
				'quantity' => $no_of_order,
				'inventory_item_id' => $child['BomChild']['inventory_item_id'],
				'item_type' => $child['InventoryItem']['type']
				);
			$this->ProjectBomChild->save($data);
			$project_bom_child_id = $this->ProjectBomChild->getLastInsertId(); 
			// Copy each child item
			$this->copy_bom_item($child['BomChild']['id'], $sale_job_id, $project_bom_child_id, $project_bom_id, $no_of_order, $job_child_id); 
		}
	}

	private function copy_bom_item($bom_child_id, $sale_job_id, $project_bom_child_id, $project_bom_id, $no_of_order, $job_child_id) {
		$this->loadModel('BomItem');
		$this->loadModel('ProjectBomItem');
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $bom_child_id
				),
			'group' => array('BomItem.id')
			));
		 
		$data = array();
		foreach ($items as $item) {
			
			$quantity_total = $item['BomItem']['quantity'] * $no_of_order;
			$price = $item['InventoryItem']['unit_price'] * $item['BomItem']['quantity'];
			$price_total = $price * $no_of_order;
			if($item['InventoryItem']['quantity_available'] >= $quantity_total) {
				$quantity_shortage = 0;
			} else {
				$quantity_shortage = $item['InventoryItem']['quantity_available'] - $quantity_total;
			}  
			$data[] = array(
				'sale_job_id' => $sale_job_id,
				'sale_job_child_id' => $job_child_id,
				'inventory_item_id' => $item['BomItem']['inventory_item_id'],
				'general_unit_id' => $item['BomItem']['general_unit_id'],
				'project_bom_child_id' => $project_bom_child_id,
				'project_bom_id' => $project_bom_id,
				'quantity' => $item['BomItem']['quantity'],
				'quantity_total' => $quantity_total,
				'quantity_shortage' => abs($quantity_shortage),
				'price' => $item['InventoryItem']['unit_price'],
				'price_total' => $price_total,
				'no_of_order' => $no_of_order
				); 
		}
		$this->ProjectBomItem->saveAll($data, array('deep' => true));
	}

	private function get_tender($job_id) { 

	} 

	private function calc_total($quantity, $discount, $discount_type, $price_unit, $tax) {
		if($discount > 0) {
			if($discount_type == 1) {
				$sub_total = $price_unit * $quantity - $discount;
			} else {
				$sub_total = ($price_unit * $quantity) - ($price_unit / 100) * $discount;
			}	
		} else {
			$sub_total = $price_unit * $quantity;
		} 

		if($tax > 0) {
			$total = ($sub_total / 100) * $tax + $sub_total;
		} else {
			$total = $sub_total;
		}
		return $total;
	}

/**
 * add method
 *
 * @return void

	public function add() {  
		if ($this->request->is('post')) {
			$job_id = $this->request->data['SaleOrder']['sale_job_id'];
			if(!is_numeric($job_id)) {
				$this->flash->error('Please enter Job No', 'error');
			} else {
				$this->loadModel('SaleJob');
				$job = $this->SaleJob->find('first', array(
					'conditions' => array(
						'SaleJob.id' => $job_id
						)
					));
				if($job) {
					return $this->redirect(array('action' => 'generate/'.$job_id));
				} else {
					$this->flash->error('Job No not found', 'error');
				}
			}
		} 
		$saleTenders = $this->SaleOrder->SaleTender->find('list');
		$saleQuotations = $this->SaleOrder->SaleQuotation->find('list');
		$generalCurrencies = $this->SaleOrder->GeneralCurrency->find('list');
		$customerFiles = $this->SaleOrder->CustomerFile->find('list');
		$saleOrderPricings = $this->SaleOrder->SaleOrderPricing->find('list');
		$users = $this->SaleOrder->User->find('list');
		$this->set(compact('saleTenders', 'saleQuotations', 'generalCurrencies', 'customerFiles', 'saleOrderPricings', 'users'));
	}
 */
	public function generate($id = null) {
		$this->loadModel('SaleJob');
		$job = $this->SaleJob->find('first', array(
			'conditions' => array(
				'SaleJob.id' => $id
				)
			));

		$this->loadModel('Customer');
		$customer = $this->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $job['SaleJob']['customer_id']
				)
			));

		$this->loadModel('ProjectBom');
		$boms = $this->ProjectBom->find('all', array(
			'conditions' => array(
				'ProjectBom.sale_job_id' => $job['SaleJob']['id']
				)
			));

		$this->loadModel('SaleJobItem');
		$items = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_id' => $job['SaleJob']['id']
				)
			));

		$this->set('boms', $boms);
		$this->set('items', $items);

		$this->set('customer', $customer);
		$this->set('job', $job);
	}

	public function copybom($job_id = null, $station_id = null, $sale_order_id = null) {
		if (!$this->SaleOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Sale Order'));
		} 

		$saleorder = $this->SaleOrder->find('first', array(
			'conditions' => array(
				'SaleOrder.id' => $sale_order_id
				)
			));
		$this->loadModel('SaleJobItem');
		$jobitems = $this->SaleJobItem->find('first', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $station_id
				)
			));

		$this->set('saleorder', $saleorder); 
		$this->set('jobitems', $jobitems);
	}

	public function bom($id = null) {
		if (!$this->SaleOrder->exists($id)) {
			throw new NotFoundException(__('Invalid sale order'));
		} 
		$saleorder = $this->SaleOrder->find('first', array('conditions' => array('SaleOrder.id' => $id)));
		// Find Child to display. Then add bom under it
		$stations = $this->SaleOrder->SaleOrderChild->find('all', array('conditions' => array('SaleOrderChild.sale_order_id' => $id)));

		if ($this->request->is('post')) {
			//print_r($this->request->data['ProjectBom']['bom_id']);   
			$bom = $this->request->data['ProjectBom']['bom_id'];
			$quantity = $this->request->data['ProjectBom']['quantity'];
			$boms = $this->request->data['ProjectBom']['bom_id'];
			$childs = $this->request->data['ProjectBom']['sale_order_child_id'];

			$general_unit_id = $this->request->data['ProjectBom']['general_unit_id'];
			$unit_price = $this->request->data['ProjectBom']['unit_price'];
			$discount = $this->request->data['ProjectBom']['discount'];
			$tax = $this->request->data['ProjectBom']['tax']; 
			$customer_purchase_order_no = $saleorder['SaleOrder']['customer_purchase_order_no']; 

			//var_dump($childs);
			$user_id = $this->Session->read('Auth.User.id'); 
			$this->loadModel('ProjectBom');
			$this->loadModel('SaleOrderItem');
			foreach ($childs as $key => $child) { 
				for($i = 0; $i < count($bom[$child]); $i++) {
					// type: BOM or ITEM
					$type = explode('-', $bom[$child][$i]);
					// IF BOM
					if($type[0] == 'bom') {
						$bominfo = $this->bominfo($type[1]); 
						$total_price = $quantity[$child][$i] * $unit_price[$child][$i]; 
						$this->ProjectBom->create();  
						$data = array(
							'quantity' => $quantity[$child][$i],

							'general_unit_id' => $general_unit_id[$child][$i],
							'unit_price' => $unit_price[$child][$i],
							'discount' => $discount[$child][$i],
							'tax' => $tax[$child][$i],
							'total_price' => $this->total_price($unit_price[$child][$i], $quantity[$child][$i], $discount[$child][$i], $tax[$child][$i]),
							'customer_purchase_order_no' => $customer_purchase_order_no, 
							'bom_id' => $type[1],
							'name' => $bominfo['Bom']['name'],
							'code' => $bominfo['Bom']['code'],
							'remark' => 'Remark',
							'sale_tender_id' => $saleorder['SaleTender']['id'],
							'sale_order_id' => $id,
							'sale_order_child_id' => $child,
							'created' => $this->date,
							'modified' => $this->date,
							'user_id' => $user_id,
							'status' => 0
							);
						$this->ProjectBom->save($data); 
						$bom_id = $type[1];
						$no_of_order = $quantity[$child][$i];
						// After save
						$project_bom_id = $this->ProjectBom->getLastInsertId();

						$this->copy_bom_child($id, $bom_id, $project_bom_id, $no_of_order);	
					}

					if($type[0] == 'item') {
						// Insert sale_order_items
						$iteminfo = $this->item_info($type[1]); 
						$total_price = $quantity[$child][$i] * $unit_price[$child][$i]; 
						$this->SaleOrderItem->create();  
						$data = array(
							'quantity' => $quantity[$child][$i],

							'general_unit_id' => $general_unit_id[$child][$i],
							'unit_price' => $unit_price[$child][$i],
							'discount' => $discount[$child][$i],
							'tax' => $tax[$child][$i],
							'total_price' => $this->total_price($unit_price[$child][$i], $quantity[$child][$i], $discount[$child][$i], $tax[$child][$i]),
							'customer_purchase_order_no' => $customer_purchase_order_no, 
							'bom_id' => 0, // Bukan bom
							'name' => $iteminfo['InventoryItem']['name'],
							'code' => $iteminfo['InventoryItem']['code'],
							'remark' => 'Remark',
							'sale_tender_id' => $saleorder['SaleTender']['id'],
							'sale_order_id' => $id,
							'sale_order_child_id' => $child,
							'created' => $this->date,
							'modified' => $this->date,
							'user_id' => $user_id,
							'status' => 0, 
							'inventory_item_id' => $type[1],
							'type' => 2

							);
						$this->SaleOrderItem->save($data);  
					}
					

				} 
			}  

			//$this->Session->setFlash(__('The sale quotation has been saved.'), 'success');
			//return $this->redirect(array('action' => 'index')); 
		} 

		$this->set('saleorder', $saleorder);
		$this->set('stations', $stations);

		$this->loadModel('InventoryItemCategory');
		$items = $this->InventoryItemCategory->find('all');

		// Load BOM For JS
		$this->loadModel('BomCategory');

		$categories = $this->BomCategory->find('all');
		$this->set('categories', $categories); 

		$this->loadModel('GeneralUnit');
		$units = $this->GeneralUnit->find('list');

		$saleTenders = $this->SaleOrder->SaleTender->find('list');
		$saleQuotations = $this->SaleOrder->SaleQuotation->find('list');
		$generalCurrencies = $this->SaleOrder->GeneralCurrency->find('list');
		$customerFiles = $this->SaleOrder->CustomerFile->find('list');
		$saleOrderPricings = $this->SaleOrder->SaleOrderPricing->find('list');
		$users = $this->SaleOrder->User->find('list');

		$this->set('items', $items);

		$this->set(compact('saleTenders', 'saleQuotations', 'generalCurrencies', 'customerFiles', 'saleOrderPricings', 'users', 'units'));
	}

	private function total_price($unit_price, $qty, $discount, $tax) {
		$total = $unit_price * $qty;
		if($discount != 0 || $discount != '') {
			$total_discount = ($total / 100) * $discount; 
		} else {
			$total_discount = 0;
		}

		if($tax != 0 || $tax != '') {
			$total_tax = ($total_discount / 100) * $tax;
		} else {
			$total_tax = 0;
		}

		$amount = $total - $total_discount + $total_tax;

		return $amount;
	}

	private function item_info($id) {
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('first', array(
			'conditions' => array(
				'InventoryItem.id' => $id
				)
			));
		return $item;
	}

	

	private function reserve_item($item_id, $quantity, $shortage) {

	}

	private function bominfo($id) {
		$this->loadModel('Bom');
		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $id
				)
			));
		return $bom;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleOrder->exists($id)) {
			throw new NotFoundException(__('Invalid sale order'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['SaleOrder']['modified'] = $this->date;

			if ($this->SaleOrder->save($this->request->data)) {
				$job_child_id = $this->request->data['SaleOrder']['sale_job_child_id'];
				$job_id = $this->request->data['SaleOrder']['sale_job_id'];
				$user_id = $this->user_id;
				// Delete sale order item

				// Delete sale bom item

				// delete budget

				// delete project bom

				// delete project bom child

				// delete project bom item

				// Reinsert

				// lOG 
				$name = 'Edit Sale Order : ' . $this->request->data['SaleOrder']['name'];
				$link = 'sale_orders/view/'.$id;
				$type = 'Edit Sale Order';
				
				$this->insert_log($this->user_id, $name, $link, $type);

				$sale_order_id = $id; 
				
				$this->loadModel('SaleOrderItem');
				$this->loadModel('SaleJobItem');
				$this->loadModel('ProjectBom');

				$this->loadModel('ProjectBudget');
				$this->loadModel('ProjectBudgetItem');
				$this->loadModel('ProjectBomChild');
				$this->loadModel('SaleJobChild');
				$this->loadModel('AccountDepartment');
				$this->loadModel('AccountDepartmentBudget');
				$this->loadModel('SaleQuotationItem');
				$this->loadModel('SaleQuotationItemCost'); 
				$this->loadModel('ProjectBomItem'); 
				//if(isset($this->request->data['SaleOrderItem']['product_id'])) {  

					$product_id = $this->request->data['SaleOrderItem']['inventory_item_id'];
					$quantity = $this->request->data['SaleOrderItem']['quantity'];
					$general_unit_id = $this->request->data['SaleOrderItem']['general_unit_id'];
					$unit_price = $this->request->data['SaleOrderItem']['unit_price'];
					$discount = $this->request->data['SaleOrderItem']['discount'];
					$tax = $this->request->data['SaleOrderItem']['tax'];
					$discount_type = $this->request->data['SaleOrderItem']['discount_type']; 
					$bom_id = $this->request->data['SaleOrderItem']['bom_id']; 

					

					$this->SaleOrderItem->deleteAll(array(
						'SaleOrderItem.sale_order_id' => $id
						), false);

					$this->SaleJobItem->deleteAll(array(
						'SaleJobItem.sale_job_child_id' => $job_child_id
						), false);

					$this->ProjectBudget->deleteAll(array(
						'ProjectBudget.sale_job_child_id' => $job_child_id
						), false);

					$this->ProjectBudgetItem->deleteAll(array(
						'ProjectBudgetItem.sale_job_child_id' => $job_child_id
						), false);

					$this->ProjectBom->deleteAll(array(
						'ProjectBom.sale_job_child_id' => $job_child_id
						), false);

					$this->ProjectBomChild->deleteAll(array(
						'ProjectBomChild.sale_job_child_id' => $job_child_id
						), false);

					$this->ProjectBomItem->deleteAll(array(
						'ProjectBomItem.sale_job_child_id' => $job_child_id
						), false);

					$this->ProjectBudget->deleteAll(array(
						'ProjectBudget.sale_job_child_id' => $job_child_id
						), false);

					$this->AccountDepartmentBudget->deleteAll(array(
						'AccountDepartmentBudget.sale_job_child_id' => $job_child_id  
						), false);
					


					for($i = 0; $i < count($product_id); $i++) {
						$total_price = $unit_price[$i] * $quantity[$i];
						$this->SaleOrderItem->create(); 
						$data = array(
							'quantity' => $quantity[$i],
							'general_unit_id' => $general_unit_id[$i],
							'unit_price' => $unit_price[$i],
							'discount' => $discount[$i],
							'discount_type' => $discount_type[$i],
							'tax' => $tax[$i],
							'total_price' => $this->calc_total($quantity[$i], $discount[$i], $discount_type[$i], $unit_price[$i], $tax[$i]),
							'inventory_item_id' => $product_id[$i],
							'sale_order_id' => $sale_order_id,
							'type' => 0,
							'sale_job_id' => $job_id,
							'sale_job_child_id' => $job_child_id,
							'bom_id' => $bom_id[$i]  
							);

						if(!$this->SaleOrderItem->save($data)) {
							die('Error');
						}

						$order_item_id = $this->SaleOrderItem->getLastInsertId(); 

						$iteminfo = $this->bom_info($bom_id[$i]); 

						$this->SaleJobItem->create();  
						$data = array(
							'quantity' => $quantity[$i],
							'general_unit_id' => $general_unit_id[$i],
							'unit_price' => 0,
							'discount' => 0,
							'tax' => 0,
							'total_price' => 0, 
							'bom_id' => $bom_id[$i],
							'name' => 0,
							'code' => 0,
							'remark' => 'Remark',
							'sale_tender_id' => 0,
							'sale_job_id' => $job_id,
							'sale_job_child_id' => $job_child_id,
							'created' => $this->date,
							'modified' => $this->date,
							'user_id' => $this->user_id,
							'status' => 0, 
							'inventory_item_id' => $product_id[$i],
							'type' => 1 
							);
						$this->SaleJobItem->save($data);
						$job_item_id = $this->SaleJobItem->getLastInsertId();
						//debug($this->SaleOrderItem->getDataSource()->getLog(false, false));
						// Save Budget 
						// Find costing $product_id[$i]
						
						$cost = $this->SaleQuotationItem->find('first', array(
							'conditions' => array(
								'SaleQuotationItem.inventory_item_id' => $product_id[$i],
								'SaleQuotationItem.costing' => 1,
								'SaleQuotation.status' => 4
								)
							));
						if($cost) {
							$this->ProjectBudget->create(); 
							$budgets = array(
								'inventory_item_id' => $product_id[$i],
								'sale_job_item_id' => $job_item_id,
								'sale_order_item_id' => $order_item_id,
								'sale_quotation_item_id' => $cost['SaleQuotationItem']['id'], 
								'sale_job_id' => $job_id, 
								'sale_job_child_id' => $job_child_id, 
								'indirect_cost' => $cost['SaleQuotationItem']['indirect_cost'], 
								'soft_cost' => $cost['SaleQuotationItem']['soft_cost'], 
								'material_cost' => $cost['SaleQuotationItem']['material_cost'], 
								'margin' => $cost['SaleQuotationItem']['planning_margin'], 
								'margin_value' => $cost['SaleQuotationItem']['margin_value'], 
								'planning_price' => $cost['SaleQuotationItem']['planning_price'], //$quantity[$i]
								'amount' => $cost['SaleQuotationItem']['planning_price'] * $quantity[$i], 
								'used' => 0, 
								'balance' => $cost['SaleQuotationItem']['planning_price'] * $quantity[$i], 
								'created' => $this->date,   
								'user_id' => $this->user_id, 
								'status' => 0,
								'type' => 'Item Budget'
								);
							$this->ProjectBudget->save($budgets);  	
							$project_budget_id = $this->ProjectBudget->getLastInsertId();
							$costs = $this->SaleQuotationItemCost->find('all', array(
								'conditions' => array(
									'SaleQuotationItemCost.sale_quotation_item_id' => $cost['SaleQuotationItem']['id']
									)
								));
							foreach ($costs as $costing) {
								$this->ProjectBudgetItem->create();
								if($costing['SaleQuotationItemCost']['type'] == 'Soft Cost') {
									$cos = $cost['SaleQuotationItem']['indirect_cost'] + $cost['SaleQuotationItem']['material_cost'];
									$price = ($cos / 100) * $costing['SaleQuotationItemCost']['margin'];
									$total_price = $price * $quantity[$i];
								} else {
									$price = $costing['SaleQuotationItemCost']['total_price'];
									$total_price = $price * $quantity[$i];
								}
								$data = array(
									'project_budget_id' => $project_budget_id, 
									'sale_job_child_id' => $job_child_id, 
									'sale_order_item_id' => $job_item_id, 
									'inventory_item_id' => $costing['SaleQuotationItemCost']['inventory_item_id'], 
									'quantity' => $costing['SaleQuotationItemCost']['quantity'], 
									'total_quantity' => $costing['SaleQuotationItemCost']['quantity'] * $quantity[$i], 
									'general_unit_id' => $costing['SaleQuotationItemCost']['general_unit_id'], 
									'margin' => $costing['SaleQuotationItemCost']['margin'], 
									'price' => $price,  
									'used' => 0,
									'balance' => $total_price,
									'total_price' => $total_price, 
									'type' => $costing['SaleQuotationItemCost']['type']
									);
								$this->ProjectBudgetItem->save($data);
							}
						}
						
						// Copy BOM For Planning Purpose
						$this->ProjectBom->create();  
						if($iteminfo['Bom']) {
							$data = array(
								'quantity' => $quantity[$i],
								'general_unit_id' => $general_unit_id[$i],
								'unit_price' => 0,
								'discount' => 0,
								'tax' => 0,
								'total_price' => 0, 
								'bom_id' => $bom_id[$i],
								'name' => $iteminfo['Bom']['name'],
								'code' => $iteminfo['Bom']['code'],
								'remark' => 'Remark',
								'sale_tender_id' => 0,
								'sale_job_id' => $job_id,
								'sale_job_child_id' => $job_child_id,
								'created' => $this->date,
								'modified' => $this->date,
								'user_id' => $user_id,
								'status' => 0
								);	
						} else {
							$data = array(
								'quantity' => $quantity[$i],
								'general_unit_id' => $general_unit_id[$i],
								'unit_price' => 0,
								'discount' => 0,
								'tax' => 0,
								'total_price' => 0, 
								'bom_id' => $bom_id[$i],
								'name' => $iteminfo['InventoryItem']['name'],
								'code' => $iteminfo['InventoryItem']['code'],
								'remark' => 'Remark',
								'sale_tender_id' => 0,
								'sale_job_id' => $job_id,
								'sale_job_child_id' => $job_child_id,
								'created' => $this->date,
								'modified' => $this->date,
								'user_id' => $user_id,
								'status' => 0
								);	
						} 
 
						//print_r($SaleJob['SaleTender']['id']);
						$this->ProjectBom->save($data);
						// After save
						$project_bom_id = $this->ProjectBom->getLastInsertId(); 
						 
						$this->copy_bom_child($job_id, $bom_id[$i], $project_bom_id, $quantity[$i], $job_child_id);
					}
				//} 

				// Find acc dept budget
				
				$dept = $this->AccountDepartment->find('first', array(
					'conditions' => array(
						'AccountDepartment.group_id' => 12, // PPC
						),
					'order' => 'AccountDepartment.year DESC'
					));

				// Sum budget  
				$costs = $this->ProjectBudget->find('all', array(
					'fields' => array(
						'SUM(ProjectBudget.indirect_cost) AS indirect_cost',
						'SUM(ProjectBudget.soft_cost) AS soft_cost',
						'SUM(ProjectBudget.material_cost) AS material_cost',
						'SUM(ProjectBudget.margin_value) AS margin_value',
						'SUM(ProjectBudget.planning_price) AS planning_price',
						'SUM(ProjectBudget.amount) AS amount',
						'SUM(ProjectBudget.balance) AS balance',
						),
					'conditions' => array(
						'ProjectBudget.sale_job_id' => $job_id,
						'ProjectBudget.sale_job_child_id' => $job_child_id,
						'ProjectBudget.type' => 'Item Budget'
						),
					'group' => 'ProjectBudget.sale_job_child_id'
					));
				foreach($costs as $cost) { 
					$this->ProjectBudget->create(); 
					$budgets = array(
						'inventory_item_id' => 0,
						'sale_job_item_id' => $job_item_id,
						'sale_order_item_id' => $order_item_id,
						'sale_quotation_item_id' => 0, 
						'sale_job_id' => $job_id, 
						'sale_job_child_id' => $job_child_id, 
						'indirect_cost' => $cost[0]['indirect_cost'], 
						'soft_cost' => $cost[0]['soft_cost'], 
						'material_cost' => $cost[0]['material_cost'], 
						'margin' => 0, 
						'margin_value' => $cost[0]['margin_value'], 
						'planning_price' => $cost[0]['planning_price'], //$quantity[$i]
						'amount' => $cost[0]['amount'], 
						'used' => 0, 
						'balance' => $cost[0]['balance'],
						'created' => $this->date,   
						'user_id' => $this->user_id, 
						'status' => 0,
						'type' => 'Job Budget'
						);
					$this->ProjectBudget->save($budgets);  	  
					
					$job = $this->SaleJobChild->find('first', array(
						'conditions' => array(
							'SaleJobChild.id' => $job_child_id
							)
						));
					// Save budget acount
					$this->AccountDepartmentBudget->create(); 
					
					$budget[] = array(
						'account_department_id' => $dept['AccountDepartment']['id'],
						'name' => 'Material Cost (' . $job['SaleJobChild']['name'] . ')',
						'total' => $cost[0]['material_cost'],
						'year' => date('Y'),
						'used' => 0,
						'balance' => $cost[0]['material_cost'],
						'parent_id'  => 22,
						'sale_job_child_id' => $job_child_id,
						'type' => 3,
						'is_material' => 1
						);
					$savebudget = $this->AccountDepartmentBudget->saveAll($budget, array('deep' => false));
					if(!$savebudget) { 
						$this->Session->setFlash(__('The Sale Order has been saved but Budget cannot be save. Error code SO459.'), 'error');
					}

					// Save indirect cost and soft cost group by item_id  
					$budgetitems = $this->ProjectBudgetItem->find('all', array(
						'fields' => array(
							'InventoryItem.*',
							'ProjectBudgetItem.*',
							'SUM(ProjectBudgetItem.total_price) AS budget'
							),
						'conditions' => array(
							'ProjectBudgetItem.sale_job_child_id' => $job_child_id 
							),
						'group' => 'ProjectBudgetItem.inventory_item_id'
						));
					$accbudget = array();
					foreach ($budgetitems as $budgetitem) {
						 $accbudget[] = array(
							'account_department_id' => $dept['AccountDepartment']['id'],
							'name' => $budgetitem['InventoryItem']['name'] . ' (' . $job['SaleJobChild']['name'] . ')',
							'total' => $budgetitem[0]['budget'],
							'year' => date('Y'),
							'used' => 0,
							'balance' => $budgetitem[0]['budget'],
							'parent_id'  => 22,
							'sale_job_child_id' => $job_child_id,
							'type' => 3,
							'is_material' => 0
							); 
					}
					$saveaccbudget = $this->AccountDepartmentBudget->saveAll($accbudget, array('deep' => false));
					if(!$saveaccbudget) {
						$this->Session->setFlash(__('Error Code SO489. Please contact Developer.'), 'error');
					}
				}

				$this->Session->setFlash(__('The Sale Order has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale order could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleOrder.' . $this->SaleOrder->primaryKey => $id));
			$this->request->data = $this->SaleOrder->find('first', $options);
		}

		// Items
		$items = $this->SaleOrder->SaleOrderItem->find('all', array(
			'conditions' => array(
				'SaleOrderItem.sale_order_id' => $id
				)
			));

		$this->set('items', $items); 

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list'); 
		$saleQuotations = $this->SaleOrder->SaleQuotation->find('list'); 

		$users = $this->SaleOrder->User->find('list'); 
		$this->set(compact('users', 'units'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleOrder->id = $id;
		if (!$this->SaleOrder->exists()) {
			throw new NotFoundException(__('Invalid sale order'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleOrder->delete()) {
			$this->Session->setFlash(__('The sale order has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale order could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}


/*
	
	*/