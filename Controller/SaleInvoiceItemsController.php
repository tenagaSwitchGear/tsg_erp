<?php
App::uses('AppController', 'Controller');
/**
 * SaleInvoiceItems Controller
 *
 * @property SaleInvoiceItem $SaleInvoiceItem
 * @property PaginatorComponent $Paginator
 */
class SaleInvoiceItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleInvoiceItem->recursive = 0;
		$this->set('saleInvoiceItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleInvoiceItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale invoice item'));
		}
		$options = array('conditions' => array('SaleInvoiceItem.' . $this->SaleInvoiceItem->primaryKey => $id));
		$this->set('saleInvoiceItem', $this->SaleInvoiceItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleInvoiceItem->create();
			if ($this->SaleInvoiceItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale invoice item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale invoice item could not be saved. Please, try again.'));
			}
		}
		$saleInvoices = $this->SaleInvoiceItem->SaleInvoice->find('list');
		$saleOrderItems = $this->SaleInvoiceItem->SaleOrderItem->find('list');
		$generalUnits = $this->SaleInvoiceItem->GeneralUnit->find('list');
		$finishedGoods = $this->SaleInvoiceItem->FinishedGood->find('list');
		$this->set(compact('saleInvoices', 'saleOrderItems', 'generalUnits', 'finishedGoods'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleInvoiceItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale invoice item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleInvoiceItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale invoice item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale invoice item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleInvoiceItem.' . $this->SaleInvoiceItem->primaryKey => $id));
			$this->request->data = $this->SaleInvoiceItem->find('first', $options);
		}
		$saleInvoices = $this->SaleInvoiceItem->SaleInvoice->find('list');
		$saleOrderItems = $this->SaleInvoiceItem->SaleOrderItem->find('list');
		$generalUnits = $this->SaleInvoiceItem->GeneralUnit->find('list');
		$finishedGoods = $this->SaleInvoiceItem->FinishedGood->find('list');
		$this->set(compact('saleInvoices', 'saleOrderItems', 'generalUnits', 'finishedGoods'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleInvoiceItem->id = $id;
		if (!$this->SaleInvoiceItem->exists()) {
			throw new NotFoundException(__('Invalid sale invoice item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleInvoiceItem->delete()) {
			$this->Session->setFlash(__('The sale invoice item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale invoice item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
