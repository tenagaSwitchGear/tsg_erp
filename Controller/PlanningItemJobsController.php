<?php
App::uses('AppController', 'Controller');
/**
 * PlanningItemJobs Controller
 *
 * @property PlanningItemJob $PlanningItemJob
 * @property PaginatorComponent $Paginator
 */
class PlanningItemJobsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PlanningItemJob->recursive = 0;
		$this->set('planningItemJobs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->PlanningItemJob->exists($id)) {
			throw new NotFoundException(__('Invalid planning item job'));
		}
		$options = array('conditions' => array('PlanningItemJob.' . $this->PlanningItemJob->primaryKey => $id));
		$this->set('planningItemJob', $this->PlanningItemJob->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PlanningItemJob->create();
			if ($this->PlanningItemJob->save($this->request->data)) {
				$this->Session->setFlash(__('The planning item job has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The planning item job could not be saved. Please, try again.'));
			}
		}
		$saleJobChildren = $this->PlanningItemJob->SaleJobChild->find('list');
		$planningItems = $this->PlanningItemJob->PlanningItem->find('list');
		$plannings = $this->PlanningItemJob->Planning->find('list');
		$generalCurrencies = $this->PlanningItemJob->GeneralCurrency->find('list');
		$this->set(compact('saleJobChildren', 'planningItems', 'plannings', 'generalCurrencies'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->PlanningItemJob->exists($id)) {
			throw new NotFoundException(__('Invalid planning item job'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PlanningItemJob->save($this->request->data)) {
				$this->Session->setFlash(__('The planning item job has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The planning item job could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('PlanningItemJob.' . $this->PlanningItemJob->primaryKey => $id));
			$this->request->data = $this->PlanningItemJob->find('first', $options);
		}
		$saleJobChildren = $this->PlanningItemJob->SaleJobChild->find('list');
		$planningItems = $this->PlanningItemJob->PlanningItem->find('list');
		$plannings = $this->PlanningItemJob->Planning->find('list');
		$generalCurrencies = $this->PlanningItemJob->GeneralCurrency->find('list');
		$this->set(compact('saleJobChildren', 'planningItems', 'plannings', 'generalCurrencies'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->PlanningItemJob->id = $id;
		if (!$this->PlanningItemJob->exists()) {
			throw new NotFoundException(__('Invalid planning item job'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PlanningItemJob->delete()) {
			$this->Session->setFlash(__('The planning item job has been deleted.'));
		} else {
			$this->Session->setFlash(__('The planning item job could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
