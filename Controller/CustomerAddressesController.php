<?php
App::uses('AppController', 'Controller');
/**
 * CustomerAddresses Controller
 *
 * @property CustomerAddress $CustomerAddress
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CustomerAddressesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$conditions = array();
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['Customer.name LIKE'] = '%'.$_GET['name'].'%';
			} 
			$this->request->data['Customer'] = $_GET;
		}
		$record_per_page = Configure::read('Reading.nodes_per_page');
		$this->Paginator->settings = array('conditions' => $conditions,'order'=>'Customer.name ASC','limit'=>$record_per_page);

		$this->set('customerAddresses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CustomerAddress->exists($id)) {
			throw new NotFoundException(__('Invalid customer address'));
		}
		$options = array('conditions' => array('CustomerAddress.' . $this->CustomerAddress->primaryKey => $id));
		$this->set('customerAddress', $this->CustomerAddress->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CustomerAddress->create();
			if ($this->CustomerAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The customer address has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer address could not be saved. Please, try again.'), 'error');
			}
		}
		$customers = $this->CustomerAddress->Customer->find('list');
		$states = $this->CustomerAddress->State->find('list');
		$countries = $this->CustomerAddress->Country->find('list');
		$this->set(compact('customers', 'states', 'countries'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CustomerAddress->exists($id)) {
			throw new NotFoundException(__('Invalid customer address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomerAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The customer address has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer address could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('CustomerAddress.' . $this->CustomerAddress->primaryKey => $id));
			$this->request->data = $this->CustomerAddress->find('first', $options);
		}
		$customers = $this->CustomerAddress->Customer->find('list');
		$states = $this->CustomerAddress->State->find('list');
		$countries = $this->CustomerAddress->Country->find('list');
		$this->set(compact('customers', 'states', 'countries'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CustomerAddress->id = $id;
		if (!$this->CustomerAddress->exists()) {
			throw new NotFoundException(__('Invalid customer address'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomerAddress->delete()) {
			$this->Session->setFlash(__('The customer address has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The customer address could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CustomerAddress->recursive = 0;
		$this->set('customerAddresses', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CustomerAddress->exists($id)) {
			throw new NotFoundException(__('Invalid customer address'));
		}
		$options = array('conditions' => array('CustomerAddress.' . $this->CustomerAddress->primaryKey => $id));
		$this->set('customerAddress', $this->CustomerAddress->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CustomerAddress->create();
			if ($this->CustomerAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The customer address has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer address could not be saved. Please, try again.'), 'error');
			}
		}
		$customers = $this->CustomerAddress->Customer->find('list');
		$states = $this->CustomerAddress->State->find('list');
		$countries = $this->CustomerAddress->Country->find('list');
		$this->set(compact('customers', 'states', 'countries'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CustomerAddress->exists($id)) {
			throw new NotFoundException(__('Invalid customer address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomerAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The customer address has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer address could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('CustomerAddress.' . $this->CustomerAddress->primaryKey => $id));
			$this->request->data = $this->CustomerAddress->find('first', $options);
		}
		$customers = $this->CustomerAddress->Customer->find('list');
		$states = $this->CustomerAddress->State->find('list');
		$countries = $this->CustomerAddress->Country->find('list');
		$this->set(compact('customers', 'states', 'countries'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CustomerAddress->id = $id;
		if (!$this->CustomerAddress->exists()) {
			throw new NotFoundException(__('Invalid customer address'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomerAddress->delete()) {
			$this->Session->setFlash(__('The customer address has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The customer address could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
