<?php
App::uses('AppController', 'Controller');
/**
 * SaleOrderItems Controller
 *
 * @property SaleOrderItem $SaleOrderItem
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SaleOrderItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SaleOrderItem->recursive = 0;
		$this->set('saleOrderItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SaleOrderItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale order item'));
		}
		$options = array('conditions' => array('SaleOrderItem.' . $this->SaleOrderItem->primaryKey => $id));
		$this->set('saleOrderItem', $this->SaleOrderItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SaleOrderItem->create();
			if ($this->SaleOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale order item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale order item could not be saved. Please, try again.'));
			}
		}
		$generalUnits = $this->SaleOrderItem->GeneralUnit->find('list');
		$products = $this->SaleOrderItem->Product->find('list');
		$saleOrders = $this->SaleOrderItem->SaleOrder->find('list');
		$this->set(compact('generalUnits', 'products', 'saleOrders'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SaleOrderItem->exists($id)) {
			throw new NotFoundException(__('Invalid sale order item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleOrderItem->save($this->request->data)) {
				$this->Session->setFlash(__('The sale order item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale order item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SaleOrderItem.' . $this->SaleOrderItem->primaryKey => $id));
			$this->request->data = $this->SaleOrderItem->find('first', $options);
		}
		$generalUnits = $this->SaleOrderItem->GeneralUnit->find('list');
		$products = $this->SaleOrderItem->Product->find('list');
		$saleOrders = $this->SaleOrderItem->SaleOrder->find('list');
		$this->set(compact('generalUnits', 'products', 'saleOrders'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleOrderItem->id = $id;
		if (!$this->SaleOrderItem->exists()) {
			throw new NotFoundException(__('Invalid sale order item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleOrderItem->delete()) {
			$this->Session->setFlash(__('The sale order item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale order item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
