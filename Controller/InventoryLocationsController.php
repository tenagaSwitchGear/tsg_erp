<?php
App::uses('AppController', 'Controller');
/**
 * InventoryLocations Controller
 *
 * @property InventoryLocation $InventoryLocation
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class InventoryLocationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InventoryLocation->recursive = 0;
		$this->set('inventoryLocations', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InventoryLocation->exists($id)) {
			throw new NotFoundException(__('Invalid inventory location'));
		}
		$options = array('conditions' => array('InventoryLocation.' . $this->InventoryLocation->primaryKey => $id));
		$this->set('inventoryLocation', $this->InventoryLocation->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->InventoryLocation->create();
			if ($this->InventoryLocation->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory location has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory location could not be saved. Please, try again.'), 'error');
			}
		}
	}

	 

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InventoryLocation->exists($id)) {
			throw new NotFoundException(__('Invalid inventory location'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->InventoryLocation->save($this->request->data)) {
				$this->Session->setFlash(__('The inventory location has been saved.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The inventory location could not be saved. Please, try again.'), 'error');
			}
		} else {
			$options = array('conditions' => array('InventoryLocation.' . $this->InventoryLocation->primaryKey => $id));
			$this->request->data = $this->InventoryLocation->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->InventoryLocation->id = $id;
		if (!$this->InventoryLocation->exists()) {
			throw new NotFoundException(__('Invalid inventory location'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->InventoryLocation->delete()) {
			$this->Session->setFlash(__('The inventory location has been deleted.'), 'success');
		} else {
			$this->Session->setFlash(__('The inventory location could not be deleted. Please, try again.'), 'error');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
