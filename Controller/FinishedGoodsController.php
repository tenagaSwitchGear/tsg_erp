<?php
App::uses('AppController', 'Controller');
/**
 * ProductionOrders Controller
 *
 * @property ProductionOrder $ProductionOrder
 * @property PaginatorComponent $Paginator
 */
class FinishedGoodsController extends AppController {
/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Js');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$conditions = array('FinishedGood.status' => 'Draft');
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finishedGoods', $this->Paginator->paginate());
	}
	
	public function final_products() { 
		$conditions['FinishedGood.status'] = 'Approved';
		$conditions['FinishedGood.fat_status'] = 'Waiting';
		$conditions['FinishedGood.final_inspect'] = 'Waiting';
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function final_inspection() {
		$conditions = array(
			'FinishedGood.status' => 'Approved', 
			'FinishedGood.fat_status' => 'Approved', 
			'FinishedGood.final_inspect' => 'Waiting',
			'FinishedGood.dismantle_status' => '4',
			);
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function fat_pass() {
		$conditions = array('FinishedGood.status' => 'Approved', 'FinishedGood.fat_status' => 'Approved', 'FinishedGood.dismantle_status' => 'Waiting');
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function dismantle() {
		$conditions = array(
			'FinishedGood.status' => 'Approved', 
			'FinishedGood.fat_status' => 'Approved', 
			'FinishedGood.dismantle_status' => array(1, 2, 3)
			);
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function dismantlerequest() {
		$conditions = array(
			'FinishedGood.status' => 'Approved', 
			'FinishedGood.fat_status' => 'Approved', 
			'FinishedGood.dismantle_status' => array(1, 2, 3)
			);
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function pass() {
		$conditions = array('FinishedGood.fat_status' => 'Approved', 'FinishedGood.final_inspect' => 'Approved');
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function completed() {
		$conditions = array('FinishedGood.fat_status' => 'Approved', 'FinishedGood.final_inspect' => 'Approved');
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function closed() { 

		$conditions = array('FinishedGood.status' => 'Closed');
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'FinishedGood.id DESC', 
			'limit' => $record_per_page
			); 
		$this->set('finalGoods', $this->Paginator->paginate());
	}

	public function qc_submitted() { 
		$conditions['OR']['FinishedGood.status'] = 'Submit To QC'; 
		$conditions['OR']['FinishedGood.final_inspect'] = 'Submit To QC';  
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('submitToQC', $this->Paginator->paginate());
	}
	
	public function submit_qc() { 
		$conditions['OR']['FinishedGood.status'] = 'Submit To QC'; 
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('submitToQC', $this->Paginator->paginate());
	}
	
	public function rejected() {
		$conditions['OR']['FinishedGood.status'] = 'Rejected';
		$conditions['OR']['FinishedGood.fat_status'] = 'Rejected';
		$conditions['OR']['FinishedGood.final_inspect'] = 'Rejected';
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('rejectedGoods', $this->Paginator->paginate());
	}

	public function rework() {
		$conditions['OR']['FinishedGood.status'] = 'Rejected';
		$conditions['OR']['FinishedGood.fat_status'] = 'Rejected';
		$conditions['OR']['FinishedGood.final_inspect'] = 'Rejected';
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('rejectedGoods', $this->Paginator->paginate());
	}
	
	public function wip() {
		$conditions = array('FinishedGood.status' => 'Work In Progress');
		
		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order'=>'FinishedGood.id DESC',
			'limit'=>$record_per_page
			); 
		$this->set('wipGoods', $this->Paginator->paginate());
	}

	public function final_remarks($id = null) {
		if (!$this->FinishedGood->exists($id)) {
			throw new NotFoundException(__('Invalid Product'));
		}
		if($this->request->is('post')) {
			$this->loadModel('FinishedGoodRemark');
			//$this ->FinishedGoodRemark->deleteAll(array('FinishedGoodRemark.finished_good_id' => $id));
			if($this->FinishedGoodRemark->saveAll($this->request->data['FinishedGoodRemark'])) {

			} else {
				die(32);
			} 
		}
		$options = array('conditions' => array('FinishedGood.' . $this->FinishedGood->primaryKey => $id));
		$this->set('FinishedGood', $this->FinishedGood->find('first', $options));
		$remark_status = array('Finished' => 'Finished', 'Rejected' => 'Rejected', 'Closed' => 'Closed');
		
		$this->set('remark_status', $remark_status);

		$this->loadModel('FinishedGoodComment');

		$comments = $this->FinishedGoodComment->find('all', array('conditions' => array('FinishedGoodComment.category' => 'Routine Test & FAT')));
		$this->set('comments', $comments);
	}

	public function fat_remarks($id = null) {
		if (!$this->FinishedGood->exists($id)) {
			throw new NotFoundException(__('Invalid Product'));
		}
		if($this->request->is('post')) {
			$this->loadModel('FinishedGoodFatRemark');
			//$this ->FinishedGoodRemark->deleteAll(array('FinishedGoodRemark.finished_good_id' => $id));
			$this->FinishedGoodFatRemark->saveAll($this->request->data['FinishedGoodFatRemark']);  
		}
		$options = array('conditions' => array('FinishedGood.' . $this->FinishedGood->primaryKey => $id));
		$this->set('FinishedGood', $this->FinishedGood->find('first', $options));
		$remark_status = array('Finished' => 'Finished', 'Rejected' => 'Rejected', 'Closed' => 'Closed');
		
		$this->set('remark_status', $remark_status);

		$this->loadModel('FinishedGoodComment');

		$comments = $this->FinishedGoodComment->find('all', array('conditions' => array('FinishedGoodComment.category' => 'Routine Test & FAT')));
		$this->set('comments', $comments);
	}
	
	/**
 * Remarks method
 *
 * @throws NotFoundException 
 * @param string $id
 * @return void 
 */
	public function remarks($id = null) {
		if (!$this->FinishedGood->exists($id)) {
			throw new NotFoundException(__('Invalid Product'));
		}
		if($this->request->is('post')) {
			$this->loadModel('FinishedGoodRemark');
			//$this ->FinishedGoodRemark->deleteAll(array('FinishedGoodRemark.finished_good_id' => $id));
			$this->FinishedGoodRemark->saveAll($this->request->data['FinishedGoodRemark']); 
			/*$remarks = $this->request->data['FinishedGoodRemark']['remark'];
			$status = $this->request->data['FinishedGoodRemark']['status'];
			$attachment = $this->request->data['FinishedGoodRemark']['attachment'];
			for($i = 0; $i < count($remarks); $i++) {
				if($remarks[$i] != '') {
 

					 if(!empty($this->request->data['attachment'][$i])) { 
						
						$rename = $id . '-' . sha1(date('YmdHis'));
						$allow = array('pdf', 'jpg', 'jpeg', 'png');
						$size['min'] = 2000;
						$size['max'] = 99999999999;
						$upload = $this->upload_multiple_file($i, 'FinishedGoodRemark/', $this->data['FinishedGoodRemark']['attachment'][$i], $rename, $allow, $size);
						if($upload['status'] == true) {
							$att = $upload['name'];
						} else {
							$att = null;
						}
						var_dump($this->data['FinishedGoodRemark']['attachment']);
					} 

					$this->FinishedGoodRemark->create();
					$date = date('Y-m-d H:i:s');
					$this->request->data['FinishedGoodRemark']['user_id'] = $this->Session->read('Auth.User.id');
					$this->request->data['FinishedGoodRemark']['created'] = $date;
					$this->request->data['FinishedGoodRemark']['modified'] = $date;	
					$this->request->data['FinishedGoodRemark']['finished_good_id'] = $id;
					$this->request->data['FinishedGoodRemark']['remark'] = $remarks[$i];	
					$this->request->data['FinishedGoodRemark']['status'] = $status[$i];		 	 
					$this->FinishedGoodRemark->save($this->request->data);
				}
			}*/
			
			 
			/*if ($this->FinishedGoodRemark->save($this->request->data)) {
				$this->Session->setFlash(__('Remarks have been saved'));
			} else {
				$this->Session->setFlash(__('Unable to save remarks. Please, try again.'));
			}*/
		}
		$options = array('conditions' => array('FinishedGood.' . $this->FinishedGood->primaryKey => $id));
		$this->set('FinishedGood', $this->FinishedGood->find('first', $options));
		$remark_status = array('Rejected' => 'Rejected', 'Closed' => 'Closed');
		
		$this->set('remark_status', $remark_status);

		$this->loadModel('FinishedGoodComment');

		$comments = $this->FinishedGoodComment->find('all', array('conditions' => array('FinishedGoodComment.category' => 'Routine Test & FAT')));
		$this->set('comments', $comments);
	}

	public function view_fat_remarks($id = null) {
		if (!$this->FinishedGood->exists($id)) {
			throw new NotFoundException(__('Invalid Product'));
		}
		if($this->request->is('post')) {		
			if($this->request->data['FinishedGoodFatRemark']['finished_good_remark_id'])
			{
				foreach($this->request->data['FinishedGoodFatRemark']['finished_good_remark_id'] as $key=>$finished_good_remark_id)
				{
					$this->loadModel('FinishedGoodFatRemark');
					
					$this->FinishedGoodFatRemark->query('UPDATE `finished_goods_remarks` SET `status` = "'.$this->request->data['FinishedGoodFatRemark']['status'][$key].'" WHERE `id` = "'.$finished_good_remark_id.'"');										
				}
				
				$this->Session->setFlash(__('Remarks status have been updated'), 'success');
			}
		}
		$options = array('conditions' => array('FinishedGood.' . $this->FinishedGood->primaryKey => $id));
		$this->set('FinishedGood', $this->FinishedGood->find('first', $options));
		$remark_status = array('Finished' => 'Finished', 'Rejected' => 'Rejected', 'Closed' => 'Closed');
		$this->loadModel('FinishedGoodFatRemark');
		$remarks = $this->FinishedGoodFatRemark->find('all', array(
			'conditions' => array(
				'FinishedGoodFatRemark.finished_good_id' => $id
				)
			));
		$this->set('remarks', $remarks);
		$this->set('remark_status', $remark_status);
	}
	
	public function view_remarks($id = null, $type = null) {
		if (!$this->FinishedGood->exists($id)) {
			throw new NotFoundException(__('Invalid Product'));
		}
		if($this->request->is('post')) {		
			if($this->request->data['FinishedGoodRemark']['finished_good_remark_id'])
			{
				foreach($this->request->data['FinishedGoodRemark']['finished_good_remark_id'] as $key=>$finished_good_remark_id)
				{
					$this->loadModel('FinishedGoodRemark');
					
					$this->FinishedGoodRemark->query('UPDATE `finished_goods_remarks` SET `status` = "'.$this->request->data['FinishedGoodRemark']['status'][$key].'" WHERE `id` = "'.$finished_good_remark_id.'"');										
				}
				
				$this->Session->setFlash(__('Remarks status have been updated'), 'success');
			}
		}
		$options = array('conditions' => array('FinishedGood.' . $this->FinishedGood->primaryKey => $id));
		$this->set('FinishedGood', $this->FinishedGood->find('first', $options));
		$remark_status = array('Finished' => 'Finished', 'Rejected' => 'Rejected', 'Closed' => 'Closed');
		$this->loadModel('FinishedGoodRemark');

		if($type == null) {
			$remark_type = 0;
		} else {
			$remark_type = 1;
		}
		$remarks = $this->FinishedGoodRemark->find('all', array(
			'conditions' => array(
				'FinishedGoodRemark.finished_good_id' => $id,
				'FinishedGoodRemark.remark_type' => $remark_type
				)
			));
		$this->set('remarks', $remarks);
		$this->set('remark_status', $remark_status);

		$this->set('type', $type);
	}

	public function ajaxsaveremark() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if(isset($_POST['id'])) {
			$json = array();
			$json['status'] = false;
			$this->loadModel('FinishedGoodRemark');
			if($_POST['type'] == 'qc') {
				$save = $this->FinishedGoodRemark->updateAll(
					array(
						'FinishedGoodRemark.remark' => "'$_POST[remark]'",
						'FinishedGoodRemark.status' => "'$_POST[status]'",
						'FinishedGoodRemark.finished_good_comment_id' => "'$_POST[finished_good_comment_id]'",
						'FinishedGoodRemark.modified' => "'date(Y-m-d H:i:s)'",
						),
					array(
						'FinishedGoodRemark.id' => $_POST['id'],
						)
					);	
			} else {
				// Production change status
				$save = $this->FinishedGoodRemark->updateAll(
					array( 
						'FinishedGoodRemark.status' => "'$_POST[status]'", 
						'FinishedGoodRemark.modified' => "'date(Y-m-d H:i:s)'",
						),
					array(
						'FinishedGoodRemark.id' => $_POST['id'],
						)
					);	
			}
			
			if($save) {
				$json['er'] = $_POST;
				$json['status'] = true;
			}
			echo json_encode($json);
		}
	}

	public function ajaxfindfinishedgood() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array(); 
			$conditions['OR']['FinishedGood.serial_no LIKE'] = '%'.$term.'%';  
			$items = $this->FinishedGood->find('all', array('conditions' => $conditions, 'group' => 'FinishedGood.id'));
			
			$json = array();
			foreach ($items as $item) { 
				$json[] = array(
					'id' => $item['FinishedGood']['id'], 
					'name' => $item['FinishedGood']['serial_no'],  
					'sale_job_child_id' => $item['FinishedGood']['sale_job_child_id'],
					'station_name' => $item['SaleJobChild']['name'],
					'sale_job_item_id' => $item['FinishedGood']['sale_job_item_id'], 
					'inventory_item_id' => $item['FinishedGood']['inventory_item_id'],
					'inventory_item_name' => $item['InventoryItem']['name'],
					'inventory_item_code' => $item['InventoryItem']['code'],
					); 
			}
			echo json_encode($json);	
		}
	}

	public function move($id) {
		if (!$this->FinishedGood->exists($id)) {
			throw new NotFoundException(__('Invalid Product'));
		}
		$user_id = $this->Session->read('Auth.User.id');	
		if($this->request->is('post')) {	
			$this->loadModel('InventoryStock');
			$this->InventoryStock->create();
			$this->request->data['InventoryStock']['inventory_delivery_order_item_id'] = 0;
			$this->request->data['InventoryStock']['general_unit_converter_id'] = 0;
			$this->request->data['InventoryStock']['quantity'] = 1;
			$this->request->data['InventoryStock']['issued_quantity'] = 0;
			$this->request->data['InventoryStock']['qc_status'] = 1;
			$this->request->data['InventoryStock']['receive_date'] = $this->date;
			$this->request->data['InventoryStock']['expiry_date'] = $this->date; 
			$this->request->data['InventoryStock']['inventory_delivery_location_id'] = 0; 
			$this->request->data['InventoryStock']['type'] = 2; // 2 = Finished Good, 0 = raw material
			$this->request->data['InventoryStock']['qc_status'] = 1;
			$this->request->data['InventoryStock']['finished_good_id'] = $id;  

			if($this->request->data['InventoryStock']['inventory_store_id'] == '') {
				$this->request->data['InventoryStock']['inventory_store_id'] = 0;
			}
			if($this->request->data['InventoryStock']['inventory_rack_id'] == '') {
				$this->request->data['InventoryStock']['inventory_rack_id'] = 0;
			}

			if($this->InventoryStock->save($this->request->data)) {
				$stock_id = $this->InventoryStock->getLastInsertId();
				// Insert history
				$this->loadModel('InventoryMaterialHistory');
				$this->InventoryMaterialHistory->create();
				$history = array(
					'user_id' => 0, 
					'store_pic' => $user_id,
					'inventory_item_id' => $this->request->data['InventoryStock']['inventory_item_id'],
					'inventory_stock_id' => $stock_id,
					'quantity' => 1, // Req qty
					'issued_quantity' => 0,
					'inventory_material_request_id' => 0,
					'inventory_material_request_item_id' => 0,
					'created' => $this->date,
					'status' => 1,
					'type' => 5 // 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 
					);
				$this->InventoryMaterialHistory->save($history);

				if($this->update_inv_item_qty($this->request->data['InventoryStock']['inventory_item_id'])) {
					// Update finished goods status = closed
					$this->FinishedGood->updateAll(
						array(
							'FinishedGood.status' => "'Closed'", 
							),
						array(
							'FinishedGood.id' => $id
							)
						);	
					
					$this->Session->setFlash(__('Finished goods has been saved to Stock'), 'success');
					return $this->redirect(array('action' => 'final_products'));	
				} 
			} 
		}

		$item = $this->FinishedGood->find('first', array(
			'conditions' => array(
				'FinishedGood.id' => $id
				)
			));

		$this->loadModel('SaleJob');

		$job = $this->SaleJob->find('first', array(
			'conditions' => array(
				'SaleJob.id' => $item['ProductionOrder']['sale_job_id']
				)
			));

		$this->loadModel('SaleJobItem');
		$inventory_item = $this->SaleJobItem->find('first', array(
			'conditions' => array(
				'SaleJobItem.id' => $item['ProductionOrder']['sale_job_items_id']
				)
			));
		

		$this->set('inventory_item', $inventory_item);
		$this->set('job', $job);
		$this->set('item', $item); 

		// Location (Warehouse)
		$this->loadModel('InventoryLocation');
		$warehouse = $this->InventoryLocation->find('list');

		$this->loadModel('InventoryStore');
		$store = $this->InventoryStore->find('list');

		$this->loadModel('InventoryRack');
		$rack = $this->InventoryRack->find('list');

		$this->set(compact('warehouse', 'store', 'rack'));
	}

	private function update_inv_item_qty($id) {
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('first', array(
			'conditions' => array(
				'InventoryItem.id' => $id
				),
			'reqursive' => -1
			));
		if($item) {
			$quantity = $item['InventoryItem']['quantity'] + 1;
			$quantity_available = $item['InventoryItem']['quantity_available'] + 1;

			$update = $this->InventoryItem->updateAll(
				array(
					'InventoryItem.quantity' => "'".$quantity."'",
					'InventoryItem.quantity_available' => "'".$quantity_available."'",
					),
				array(
					'InventoryItem.id' => $id
					)
				);	
			if($update) {
				return true;
			}
		}
		return false; 
	}

	private function find_inventory_item_id($bom_id) {
		$this->loadModel('Bom');
		$bom = $this->Bom->find('first', array(
			'conditions' => array(
				'Bom.id' => $bom_id
				) 
			));
		if($bom) {
			return $bom;
		}
	}
	
	public function updateSerial()
	{
		$this->autoRender = false;
		$this->layout = 'ajax';

		if(isset($_POST['value'])) {
			$pk = $_POST['pk'];
	        $value = $_POST['value'];
			
			$json = array();
			if($value != '') {
				$check = $this->FinishedGood->find('first', array('conditions' => array(
					'FinishedGood.serial_no' => $value
					)));
				if(!$check) {
					$update = $this->FinishedGood->updateAll(
					array('FinishedGood.serial_no' => "'".$value."'"),
					array('FinishedGood.id' => $pk)
					);	
					if($update) {
						$json['status'] = true; 
						$this->insert_log($this->user_id, "Update Finished Good Serial No: {$value}", $pk, "Finished Good Status");
					} else {
						$json['status'] = false;
						$json['message'] = 'Error update';
					}	
				} else {
					$json['status'] = false;
					$json['message'] = 'Serial Number already used';
				}	
			} else { 
				$json['status'] = false;	
				$json['message'] = 'Please insert Serial Number'; 
			}	
			echo json_encode($json);
		}
		
	}
	
	public function getStatus() 
	{
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$statuses = array(); 
		if(isset($_GET['status'])) {
			$status = $_GET['status'];
		} else {
			$status = '';
		}

		if(isset($_GET['mfg'])) {
			$mfg = 1;
		} else {
			$mfg = 0;
		}

		if($status == 'Draft') {
			$statuses[] = array('value' => 'Draft', 'text' => 'Draft');
			$statuses[] = array('value' => 'Submit to QC', 'text' => 'Submit to QC');
		} 
		if($status == 'Submit to QC') {
			$statuses[] = array('value' => 'Rejected', 'text' => 'Rejected');
			$statuses[] = array('value' => 'Approved', 'text' => 'Approved'); 
		} 
		if($status == 'Rejected') {
			$statuses[] = array('value' => 'Rejected', 'text' => 'Rejected');
			if($mfg == 1) {
				$statuses[] = array('value' => 'Submit to QC', 'text' => 'Submit to QC');
			} else {
				$statuses[] = array('value' => 'Approved', 'text' => 'Approved'); 
			} 
		} 

		if($status == 'Approved') { 
			$statuses[] = array('value' => 'Approved', 'text' => 'Approved');
		}  

		echo json_encode($statuses);
	}


	
	public function setStatus() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$pk = mysql_real_escape_string($_POST['pk']);
        $value = mysql_real_escape_string($_POST['value']);
		
		if($value != '') {
			/*$this->FinishedGood->updateAll(
			array('FinishedGood.status' => $value),
			array('FinishedGood.id' => $pk)
			);*/
			$job = $this->FinishedGood->find('first', array(
				'conditions' => array(
					'FinishedGood.id' => $pk
					)
				));

			// Update production order status
			 
			// Update sale order
            $this->update_sale_order_progress($job['FinishedGood']['sale_job_child_id'], 'QAT');

			$this->FinishedGood->query('UPDATE `finished_goods` SET `status` = "'.$value.'" WHERE `id` = "'.$pk.'"');

			$this->insert_log($this->user_id, "Update Finished Good: {$value}", $pk, "Finished Good Status");
					 
		} else {
			header('HTTP 400 Bad Request', true, 400);
			echo "This field is required!";		
		}
	}

	public function getFatStatus()
	{
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$statuses = array();

		if(isset($_GET['status'])) {
			$status = $_GET['status'];
			if($status == 'Approved') {
				$statuses[] = array('value' => 'Approved', 'text' => 'Approved');
			}
			if($status == 'Rejected') {
				$statuses[] = array('value' => 'Approved', 'text' => 'Approved');
				$statuses[] = array('value' => 'Rejected', 'text' => 'Rejected'); 
			}
			if($status == 'Waiting') {
				$statuses[] = array('value' => 'Approved', 'text' => 'Approved');
				$statuses[] = array('value' => 'Rejected', 'text' => 'Rejected'); 
			}
			/* 
			$statuses[] = array('value' => 'Waiting', 'text' => 'Waiting');
			$statuses[] = array('value' => 'Rejected', 'text' => 'Rejected');  
			*/
		} 
		echo json_encode($statuses);
	}
	
	public function setFatStatus() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$pk = mysql_real_escape_string($_POST['pk']);
        $value = mysql_real_escape_string($_POST['value']);
		
		if($value != '') { 
			if($value == 'Approved') {
				$this->FinishedGood->query('UPDATE `finished_goods` SET `fat_status` = "'.$value.'", `final_inspect`="Waiting", `progress` = 100 WHERE `id` = "'.$pk.'"');
				$fat = 'FAT Passed';
				$method = 'rejected';

			} else {
				$this->FinishedGood->query('UPDATE `finished_goods` SET `fat_status` = "'.$value.'" WHERE `id` = "'.$pk.'"');
				$fat = 'FAT Failed';
				$method = 'rework';
			}

			$job = $this->FinishedGood->find('first', array(
				'conditions' => array(
					'FinishedGood.id' => $pk
					)
				)); 

			$group_id = $this->Session->read('Auth.User.group_id');
			if($group_id == 20) { // QAT
				$to_group_id = 21;
				$from = 'QAT Department';
			} else {
				$to_group_id = 20;
				$from = 'Manufacturing Department';
			}
			$link = 'finished_goods/'.$method.'/'.$pk; 

			$this->sendEmailDepartment($to_group_id, $link, 'action', $from);
			// Update sale order
            $this->update_sale_order_progress($job['FinishedGood']['sale_job_child_id'], $fat);

            $this->insert_log($this->user_id, "Update Finished Good: {$fat}", $link, "Finished Good Status");
					
		} else {
			header('HTTP 400 Bad Request', true, 400);
			echo "This field is required!";		
		}
	}

	public function getDismantleStatus()
	{
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$statuses = array();
		
        /*
		$statuses[] = array('value' => 'Waiting', 'text' => 'Waiting');
		$statuses[] = array('value' => 'In Progress', 'text' => 'In Progress'); 
		$statuses[] = array('value' => 'Completed', 'text' => 'Completed');
       */ 
		$statuses[] = array('value' => '1', 'text' => 'Submit to Dismantle');  
		$statuses[] = array('value' => '4', 'text' => 'Submit to QC'); 
		
		echo json_encode($statuses);
	}
	
	public function setDismantleStatus() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$pk = mysql_real_escape_string($_POST['pk']);
        $value = mysql_real_escape_string($_POST['value']);
		
		if($value != '') { 
 
			if($value == '4') {
				$this->FinishedGood->query('UPDATE `finished_goods` SET `dismantle_status` = "'.$value.'", `final_inspect`="Waiting", `progress` = 100 WHERE `id` = "'.$pk.'"');
				$status = 'Final Inspection';
			} else {
				$this->FinishedGood->query('UPDATE `finished_goods` SET `dismantle_status` = "'.$value.'" WHERE `id` = "'.$pk.'"');
				$status = 'Dismantle';
			}
 			$job = $this->FinishedGood->find('first', array(
				'conditions' => array(
					'FinishedGood.id' => $pk
					)
				)); 
			 
			// Update sale order
            $this->update_sale_order_progress($job['FinishedGood']['sale_job_child_id'], $status);
					
		} else {
			header('HTTP 400 Bad Request', true, 400);
			echo "This field is required!";		
		}
	}

	public function getFinalInspect() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$statuses = array();
		
		$statuses[] = array('value' => 'Waiting', 'text' => 'Waiting');
		$statuses[] = array('value' => 'Rejected', 'text' => 'Rejected'); 
		$statuses[] = array('value' => 'Approved', 'text' => 'Approved');
		
		echo json_encode($statuses);
	}
	
	public function setFinalInspect() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$pk = mysql_real_escape_string($_POST['pk']);
        $value = mysql_real_escape_string($_POST['value']);
		
		if($value != '') {
			/*$this->FinishedGood->updateAll(
			array('FinishedGood.status' => $value),
			array('FinishedGood.id' => $pk)
			);*/
			
			$this->FinishedGood->query('UPDATE `finished_goods` SET `final_inspect` = "'.$value.'" WHERE `id` = "'.$pk.'"');
			
					
		} else {
			header('HTTP 400 Bad Request', true, 400);
			echo "This field is required!";		
		}
	}

	public function getProgress()
	{
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$statuses = array();
		
		$statuses[] = array('value' => '0', 'text' => '0');
		$statuses[] = array('value' => '20', 'text' => '20'); 
		$statuses[] = array('value' => '40', 'text' => '40');
		$statuses[] = array('value' => '60', 'text' => '60'); 
		$statuses[] = array('value' => '80', 'text' => '80');
		$statuses[] = array('value' => '100', 'text' => '100');
		echo json_encode($statuses);
	}
	
	public function setProgress() {
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		$pk = mysql_real_escape_string($_POST['pk']);
        $value = mysql_real_escape_string($_POST['value']);
		
		if($value != '') {
			/*$this->FinishedGood->updateAll(
			array('FinishedGood.status' => $value),
			array('FinishedGood.id' => $pk)
			);*/
			
			$this->FinishedGood->query('UPDATE `finished_goods` SET `progress` = "'.$value.'" WHERE `id` = "'.$pk.'"');
					
		} else {
			header('HTTP 400 Bad Request', true, 400);
			echo "This field is required!";		
		}
	}

	private function sendEmailDepartment($group_id, $link, $action, $from) {
		$this->loadModel('User');
		$users = $this->User->find('all', array(
			'conditions' => array(
				'User.group_id' => $group_id
				)
			));
		foreach ($users as $user) {
			$data['to'] = $user['User']['email'];
			$data['template'] = 'finished_good';
			$data['subject'] = 'Item Require Your Action';
			$data['content'] = array(
				'action' => $action,
				'username' => $user['User']['username'], 
				'from' => $from,
				'link' => $link
				);
			$this->send_email($data);
		}
	} 
}