<?php
App::uses('AppController', 'Controller'); 
 
class ReportsController extends AppController {

	public $uses = array('UserLog', 'Customer', 'SaleTender', 'SaleQuotation', 'SaleOrder'); 
 
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

	public function index() {
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['UserLog.name LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['UserLog.created >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['UserLog.created <='] = trim($_GET['to']);
			}  
			 
			$this->request->data['UserLog'] = $_GET;
		} 

		 
		$conditions['UserLog.id !='] = 0; 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array( 
			'conditions' => $conditions,
			'order' => 'UserLog.id DESC', 
			'limit' => $record_per_page
			); 
		
		$this->set('logs', $this->Paginator->paginate());
	}

	public function warehouse() {

		if($this->request->is('post')) {
			print_r($_POST);
			
			if(isset($_POST['warehouse']) && $_POST['warehouse'] == 'Search'){
				if($this->request->data['warehouse']['date_start'] != '') {
					$conditions['InventorySupplier.created >='] = trim($this->request->data['warehouse']['date_start']);
				}

				if($this->request->data['warehouse']['date_end'] != '') {
					$conditions['InventorySupplier.created <='] = trim($this->request->data['warehouse']['date_end']);
				}

				if($this->request->data['warehouse']['code_start'] != '') {
					$conditions['InventoryItem.code LIKE'] = trim($this->request->data['warehouse']['code_start']);
				}

				if($this->request->data['warehouse']['code_end'] != '') {
					$conditions['InventoryItem.code LIKE'] = trim($this->request->data['warehouse']['code_end']);
				}

				if($this->request->data['warehouse']['category_id'] != 0) {
					$conditions['InventoryItem.inventory_item_category_id'] = trim($this->request->data['warehouse']['category_id']);
				}
			}

			print_r($conditions);
			exit;
			if($this->request->data['warehouse']['search_id'] == 1){
				$item_start = $this->request->data['warehouse']['code_start'];
				$item_end = $this->request->data['warehouse']['code_end'];
				if(!$item_start && !$item_end){
					$this->Session->setFlash(__('Insert Item Code.'), 'error');
				}else{
					$this->loadModel('InventoryStock');
					$this->loadModel('InventoryItem');

					$item_array = array();
					$items = $this->InventoryStock->find('all', array(
						'joins' => array(
					        array(
					            'table' => 'inventory_items',
					            'alias' => 'inventoryitems',
					            'type' => 'INNER',
					            'conditions' => array(
					                'InventoryStock.inventory_item_id = inventoryitems.id'
					            )
					        )
					    ),
						'conditions' => array(
							'inventoryitems.code BETWEEN ? and ? ' => array($item_start, $item_end),
							),
						'group' => 'inventoryitems.code',
						'recursive' => -1
					));
					//print_r($items);
					//exit;

					foreach ($items as $item) {
						$item_array[] = array(
							'item' => $this->get_item_detail($item['InventoryStock']['inventory_item_id']),
							'begin_stock' => $this->get_begin_stock_quantity($item['InventoryStock']['inventory_item_id'], '', ''),
							'in_stock' => $this->get_stock_in($item['InventoryStock']['inventory_item_id']),
							'issued_quantity' => $this->get_issued_quantity($item['InventoryStock']['inventory_item_id']),
							'end_stock' => $this->get_stock_quantity($item['InventoryStock']['inventory_item_id']),
							
							
						);
					}

					//print_r($item_array);
					//exit;

					/*$record_per_page = Configure::read('Reading.nodes_per_page');
			        $this->Paginator->settings = array(
			            'order' => 'InventoryStock.code DESC',
			            'limit' => $record_per_page
			            );*/
			        //$this->set('items', $this->paginate($item_array));
					$this->set('items', $item_array);
				}

			}
			//exit;
		}
		$this->loadModel('InventoryItemCategory');
		$category = $this->InventoryItemCategory->find('list', array('recursive' => -1));
		$this->set('category', $category);
	}

	private function get_stock_in($id){
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->find('all', array(
				'fields' => array(
					'sum(InventoryStock.quantity) AS total'
					), 
				'conditions'=>array(
					'InventoryStock.inventory_item_id' => $id
					)
				));
		
		$stock_count = $stock[0][0]['total'];
		return number_format($stock_count, 2);
	}

	public function progress() {
		$this->loadModel('SaleOrder');
		$this->loadModel('SaleJobChild');
		

		if(isset($_GET['search'])) {
			//print_r($_GET);
			//exit;
			
			if($_GET['years'] != '') {
				$conditions['YEAR(SaleJobChild.fat_date)'] = $_GET['years'];
				$date = $_GET['years'];

				$stations = $this->SaleJobChild->find('all', array(
					'conditions' => $conditions, 
					'group' => 'MONTH(SaleJobChild.fat_date)',

					));
				$data = array();
				$data_job = array();
				foreach ($stations as $station) {
					$data[] = array(
						'SaleJobChild' => $station['SaleJobChild'],
						'SaleJobNo' => $this->get_job_no($station['SaleJobChild']['sale_job_id']),
						'Customer' => $this->get_customer($station['SaleJob']['customer_id']),
						'Jobs' => $this->get_jobs_by_month($date, date('m', strtotime($station['SaleJobChild']['fat_date']))),
						);
				}
			}

			if($_GET['job_no'] != '') {
				$this->loadModel('SaleJob');
				$conditions['SaleJob.name LIKE'] = '%'.$_GET['job_no'].'%';
				$date = date('Y');

				$stations = $this->SaleJob->find('all', array(
					'conditions' => $conditions,
					));
				//print_r($stations);
				//exit;
				$stations_2 = $this->SaleJobChild->find('all', array(
					'conditions' => array(
						'SaleJobChild.sale_job_id' => $stations[0]['SaleJob']['id'],
					),
					'order' => 'MONTH(SaleJobChild.fat_date) ASC',
				));
				$data = array();
				$data_job = array();
				foreach ($stations_2 as $station_2) {
					$data[] = array(
						'SaleJobChild' => $station_2['SaleJobChild'],
						'SaleJobNo' => $this->get_job_no($station_2['SaleJobChild']['sale_job_id']),
						'Customer' => $this->get_customer($station_2['SaleJob']['customer_id']),
						'Jobs' => $this->get_jobs_by_month($date, date('m', strtotime($station_2['SaleJobChild']['fat_date']))),
						);
				}
			}
			//$this->request->data['Reports'] = $_GET;
			$this->set('year', $date);
			$this->set('stations', $data);

		}else{		
			if(isset($_GET['year'])) { 
				$date = (int)$_GET['year'];
			} else {
				$date = date('Y');
			}	

			$stations = $this->SaleJobChild->find('all', array(
				'conditions' => array(
					'YEAR(SaleJobChild.fat_date) =' => $date
					), 
				'group' => 'MONTH(SaleJobChild.fat_date)',
				));
			$data = array();
			$data_job = array();
			foreach ($stations as $station) {
				$data[] = array(
					'SaleJobChild' => $station['SaleJobChild'],
					'SaleJobNo' => $this->get_job_no($station['SaleJobChild']['sale_job_id']),
					'Customer' => $this->get_customer($station['SaleJob']['customer_id']),
					'Jobs' => $this->get_jobs_by_month($date, date('m', strtotime($station['SaleJobChild']['fat_date']))),
					);
			}
			$this->set('year', $date);
			$this->set('stations', $data);
			
		}
	}

	private function get_item_detail($id) {
		$this->loadModel('InventoryItem');
		$this->loadModel('GeneralUnit');
		$stock = $this->InventoryItem->find('all', array(
				'joins' => array(
					array(
						'table' => 'general_units',
						'alias' => 'generalunits',
						'type' => 'INNER',
						'conditions' => array(
							'generalunits.id = InventoryItem.general_unit_id'
						)
					)
				),
				'fields' => array(
					'InventoryItem.id',
					'InventoryItem.name',
					'InventoryItem.code',
					'generalunits.name as gunit',
					), 
				'conditions'=>array(
					'InventoryItem.id' => $id
					),
				'recursive' => -1
				));
		
		$item = array(
			'id' => $stock[0]['InventoryItem']['id'],
			'name' => $stock[0]['InventoryItem']['name'],
			'code' => $stock[0]['InventoryItem']['code'],
			'general_unit' => $stock[0]['generalunits']['gunit']
		);
		//print_r($stock);
		//exit;
		return $item;
	}

	private function get_opening_stock($id, $date_start) {
		$this->loadModel('InventoryItem');
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->find('all', array(
				'joins' => array(
					array(
						'table' => 'inventory_items',
						'alias' => 'inventoryItems',
						'type' => 'INNER',
						'conditions' => array(
							'InventoryStock.inventory_item_id = inventoryItems.id'
						)
					)
				),
				'fields' => array(
					'sum(InventoryStock.qty_in) AS total',
					), 
				'conditions'=>array(
					'InventoryStock.inventory_item_id' => $id,
					'InventoryStock.receive_date <' => $date_start
					),
				'recursive' => -1
				));

		$opening_stock = $stock[0][0]['total'];
	
		return $opening_stock;
	}

	private function get_begin_stock_quantity($id, $date_start=null, $date_end=null) {
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->find('all', array(
				'fields' => array(
					'sum(InventoryStock.qty_in) AS total',
					//'sum(InventoryStock.issued_quantity) AS issued',
					), 
				'conditions'=>array(
					'InventoryStock.inventory_item_id' => $id,
					//'InventoryStock.receive_date BETWEEN ? and ? ' => array($date_start, $date_end),
					)
				));
		
		$stock_count = $stock[0][0]['total'];
		return number_format($stock_count, 2);
	}

	private function get_stock_quantity($id) {
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->find('all', array(
				'fields' => array(
					'sum(InventoryStock.quantity) AS total'
					), 
				'conditions'=>array(
					'InventoryStock.inventory_item_id' => $id
					)
				));
		
		$stock_count = $stock[0][0]['total'];
		return number_format($stock_count, 2);
	}

	private function get_issued_quantity($id) {
		$this->loadModel('InventoryStock');
		$stock = $this->InventoryStock->find('all', array(
				'fields' => array(
					'sum(InventoryStock.issued_quantity) AS total'
					), 
				'conditions'=>array(
					'InventoryStock.inventory_item_id' => $id
					)
				));
		
		$stock_count = $stock[0][0]['total'];
		return number_format($stock_count, 2);
	}

	private function get_job_no($sale_job_id) {
		$this->loadModel('SaleJob');

		$sale_job = $this->SaleJob->find('first', array(
			'conditions' => array(
				'SaleJob.id' => $sale_job_id
				),
			'recursive' => -1
			));
		return $sale_job;
	}

	public function grn($id) {
		$this->loadModel('InventoryDeliveryOrderItem');
		$items = $this->InventoryDeliveryOrderItem->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrderItem.inventory_delivery_order_id' => $id
			),
			'recursive' => -1
		));

		$item_array = array();
		foreach ($items as $item) {
			$item_array[] = array(
				'InventoryDeliveryOrder' => $this->get_do($item['InventoryDeliveryOrderItem']['inventory_delivery_order_id']),
				'InventoryDeliveryOrderItem' => $item['InventoryDeliveryOrderItem'],
				'InventorySupplier' => $this->get_supplier($item['InventoryDeliveryOrderItem']['inventory_supplier_id']),
				'InventoryItem' => $this->get_item($item['InventoryDeliveryOrderItem']['inventory_item_id']),
				'InventoryQcItem' => $this->get_qcitem($item['InventoryDeliveryOrderItem']['id']),
				'InventoryRack' => $this->get_rack($item['InventoryDeliveryOrderItem']['rack_id']),
			);
		}

		$this->set('grns', $item_array);
	}

	private function get_rack($id){
		$this->loadModel('InventoryRack');
		$rack = $this->InventoryRack->find('all', array(
			'conditions' => array(
				'InventoryRack.id' => $id 
			),
			'recursive' => -1
		));

		if(empty($rack)){
			return $rack;
		}else{
			return $rack[0]['InventoryRack'];
		}
	}

	private function get_supplier($id){
		$this->loadModel('InventorySupplier');
		$supplier = $this->InventorySupplier->find('all', array(
			'conditions' => array(
				'InventorySupplier.id' => $id 
			),
			'recursive' => -1
		));

		return $supplier[0]['InventorySupplier'];
	}

	private function get_do($id){
		$this->loadModel('InventoryDeliveryOrder');
		$d_order = $this->InventoryDeliveryOrder->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrder.id' => $id
			),
			'recursive' => -1
		));

		$arr[] = array(
			'InventoryDeliveryOrder' => $d_order[0]['InventoryDeliveryOrder'],
			'InventoryLocation' => $this->get_location($d_order[0]['InventoryDeliveryOrder']['inventory_delivery_location_id'])
		);

		return $arr[0];
	}

	private function get_location($id){
		$this->loadModel('InventoryDeliveryLocation');
		$location = $this->InventoryDeliveryLocation->find('all', array(
			'conditions' => array(
				'InventoryDeliveryLocation.id' => $id
			),
			'recursive' => -1
		));

		return $location[0]['InventoryDeliveryLocation'];

	}

	private function get_item($id){
		$this->loadModel('InventoryItem');
		$arr = array();
		$item = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			)
		));

		$arr[] = array(
			'InventoryItem' 	=> $item[0]['InventoryItem'],
			'GeneralUnit' 		=> $item[0]['GeneralUnit'],
			'InventoryLocation' => $item[0]['InventoryLocation'],
			'InventoryStore' 	=> $item[0]['InventoryStore']
		);

		return $arr[0];
	}

	private function get_qcitem($id){
		$this->loadModel('InventoryQcItem');
		$qcitem = $this->InventoryQcItem->find('all', array(
			'conditions' => array(
				'InventoryQcItem.inventory_delivery_order_item_id' => $id
			)
		));

		return $qcitem[0]['InventoryQcItem'];
	}

	public function sales() {
		$this->loadModel('SaleQuotation');
		$this->loadModel('SaleTender');
		if(isset($_GET['year'])){
			$year = $_GET['year'];
		}else{
			$year = date('Y');
		}

		$sale_quotation = $this->SaleQuotation->find('all', array('recursive' => -1));
		$data = array();
		$data[] = array(
					'Draft' => $this->get_count_sale(0),
					'WaitingPlanning' => $this->get_count_sale(1),
					'CostingAdded' => $this->get_count_sale(2),
					'WaitingApproval' => $this->get_count_sale(3),
					'Rejected' => $this->get_count_sale(7),
					'Approved' => $this->get_count_sale(6),
					'QuotationSubmit' => $this->get_count_sale(9),
					'Award' => $this->get_count_sale(10),
					'Failed' => $this->get_count_sale(11),
					'Total' => count($sale_quotation),
					);
		
		$months = array('01','02','03','04','05','06','07','08','09','10','11','12');
		$data_2 = array();
		foreach ($months as $month) {
			$data_2[] = array(
				'Months' => date('M', strtotime("2017/" . $month . "/01")),
				'Submit' => $this->get_quotation($month, $year, 9),
				'Award' => $this->get_quotation($month, $year, 10),
				'Reject' => $this->get_quotation($month, $year, 11),
			);
		}

		$this->set('sale_quotation', $data);
		$this->set('sale_quotation_2', $data_2);

	}

	public function material_transfer() {
		$this->loadModel('InventoryMaterialHistory');
		$sejarah = array();
		$history = $this->InventoryMaterialHistory->find('all', array('group' => 'InventoryMaterialHistory.inventory_item_id' ,'recursive' => -1));

		foreach ($history as $htry) {
			$sejarah[] = array(
				'InventoryMaterialHistory' => $htry,
				'InventoryItem' => $this->get_inv_item($htry['InventoryMaterialHistory']['inventory_item_id'])
			);
		}

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array( 
			'limit' => $record_per_page
			); 
		
		$this->set('material_history', $sejarah, $this->Paginator->paginate());
		//$this->set('material_history', $sejarah);
	}
	public function view_record($id = null) {
		//echo $id;
		$this->loadModel('InventoryMaterialHistory');
		$history = $this->InventoryMaterialHistory->find('all', array(
			'conditions' => array(
				'InventoryMaterialHistory.inventory_item_id' => $id
			),
		'order' => 'InventoryMaterialHistory.id DESC',
		'recursive' => -1));
		//exit;
		$this->loadModel('InventoryItem');
		$item = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $id
			),
		'recursive' => -1));

		$this->loadModel('TransferType');
		$type = $this->TransferType->find('list');

		$this->set('history', $history);
		$this->set('item', $item);
		$this->set('type', $type);
		
	}

	private function get_inv_item($item_id) {
		$this->loadModel('InventoryItem');

		$items = $this->InventoryItem->find('all', array(
			'conditions' => array(
				'InventoryItem.id' => $item_id
				),
			'recursive' => -1
			));
		return $items[0];
	}

	public function sales_month() {
		$this->loadModel('SaleQuotation');
		$this->loadModel('SaleTender');
		if(isset($_GET['year'])){
			$year = $_GET['year'];
		}else{
			$year = date('Y');
		}

		$mth = $_GET['mth'];
		$data_quotation = array();
		$data_quotation[] = array(
			'Months' => date('F', strtotime('2017/'.$mth.'/01')),
			'Submit' => $this->get_quotation_detail($mth, $year, 9),
			'Award' => $this->get_quotation_detail($mth, $year, 10),
			'Reject' => $this->get_quotation_detail($mth, $year, 11),
		);

		$this->set('sale_quotation', $data_quotation);
	}

	private function get_quotation($month, $year, $status) {
		$this->loadModel('SaleQuotation');		
		if($status == 9){
			$submit = $this->SaleQuotation->find('all', array(
			'joins' => array(
		        array(
		            'table' => 'sale_tenders',
		            'alias' => 'SaleTender',
		            'type' => 'INNER',
		            'conditions' => array(
		                'SaleTender.id = SaleQuotation.sale_tender_id'
		            )
		        )
		    ),
			'conditions' => array(
				'MONTH(SaleTender.closing_date) =' => $month,
				'YEAR(SaleTender.closing_date) =' => $year,
				'SaleQuotation.status >='=> $status,
				),
			'recursive' => -1
			));
		}else{
			$submit = $this->SaleQuotation->find('all', array(
			'joins' => array(
		        array(
		            'table' => 'sale_tenders',
		            'alias' => 'SaleTender',
		            'type' => 'INNER',
		            'conditions' => array(
		                'SaleTender.id = SaleQuotation.sale_tender_id'
		            )
		        )
		    ),
			'conditions' => array(
				'MONTH(SaleTender.closing_date) =' => $month,
				'YEAR(SaleTender.closing_date) =' => $year,
				'SaleQuotation.status ='=> $status,
				),
			'recursive' => -1
			)); 
		}
		
		return count($submit);
	}

	public function view_grn($id = null) {
	    // increase memory limit in PHP 
	    ini_set('memory_limit', '512M');
	    $this->layout = 'printGRN';  

	    $this->loadModel('InventoryDeliveryOrderItem');
		$items = $this->InventoryDeliveryOrderItem->find('all', array(
			'conditions' => array(
				'InventoryDeliveryOrderItem.inventory_delivery_order_id' => $id
			),
			'recursive' => -1
		));

		$item_array = array();
		foreach ($items as $item) {
			$item_array[] = array(
				'InventoryDeliveryOrder' => $this->get_do($item['InventoryDeliveryOrderItem']['inventory_delivery_order_id']),
				'InventoryDeliveryOrderItem' => $item['InventoryDeliveryOrderItem'],
				'InventorySupplier' => $this->get_supplier($item['InventoryDeliveryOrderItem']['inventory_supplier_id']),
				'InventoryItem' => $this->get_item($item['InventoryDeliveryOrderItem']['inventory_item_id']),
				'InventoryQcItem' => $this->get_qcitem($item['InventoryDeliveryOrderItem']['id']),
				'InventoryRack' => $this->get_rack($item['InventoryDeliveryOrderItem']['rack_id']),
			);
		}

		$this->set('grns', $item_array);

	}

	private function get_quotation_detail($month, $year, $status) {
		$this->loadModel('SaleQuotation');		
		if($status == 9){
			$submit = $this->SaleQuotation->find('all', array(
			'joins' => array(
		        array(
		            'table' => 'sale_tenders',
		            'alias' => 'SaleTender',
		            'type' => 'INNER',
		            'conditions' => array(
		                'SaleTender.id = SaleQuotation.sale_tender_id'
		            )
		        )
		    ),
			'conditions' => array(
				'MONTH(SaleTender.closing_date) =' => $month,
				'YEAR(SaleTender.closing_date) =' => $year,
				'SaleQuotation.status >='=> $status,
				),
			'recursive' => -1
			));
		}else{
			$submit = $this->SaleQuotation->find('all', array(
			'joins' => array(
		        array(
		            'table' => 'sale_tenders',
		            'alias' => 'SaleTender',
		            'type' => 'INNER',
		            'conditions' => array(
		                'SaleTender.id = SaleQuotation.sale_tender_id'
		            )
		        )
		    ),
			'conditions' => array(
				'MONTH(SaleTender.closing_date) =' => $month,
				'YEAR(SaleTender.closing_date) =' => $year,
				'SaleQuotation.status ='=> $status,
				),
			'recursive' => -1
			)); 
		}
		
		return $submit;
	}

	

	private function get_count_sale($status) {
		$this->loadModel('SaleQuotation');
		if($status == 3){
			$status = array(3,4,5);
			$sQ = $this->SaleQuotation->find('all', array(
			'conditions' => array(
				'SaleQuotation.status' => $status,
				),
			'recursive' => -1
			));
		}else if($status == 7){
			$status = array(7,8);
			$sQ = $this->SaleQuotation->find('all', array(
			'conditions' => array(
				'SaleQuotation.status' => $status,
				),
			'recursive' => -1
			));
		}else if($status == 9){
			$sQ = $this->SaleQuotation->find('all', array(
			'conditions' => array(
				'SaleQuotation.status >=' => $status,
				),
			'recursive' => -1
			));
		}else{
			$status = $status;
			$sQ = $this->SaleQuotation->find('all', array(
			'conditions' => array(
				'SaleQuotation.status' => $status,
				),
			'recursive' => -1
			));
		}
		return count($sQ);
	}

	private function get_customer($customer_id) {
		$this->loadModel('Customer');

		$customer = $this->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $customer_id
				),
			'recursive' => -1
			));
		if($customer){
			return $customer['Customer'];
		}else{
			return array();
		}
	}

	private function get_jobs_by_month($year, $month) {
		$this->loadModel('SaleJobChild');

		$jobs = $this->SaleJobChild->find('all', array(
			'conditions' => array(
				'YEAR(SaleJobChild.fat_date)' => $year,
				'MONTH(SaleJobChild.fat_date)' => $month,
			),
		));
		//print_r($jobs);
		$data = array();

		foreach ($jobs as $job) {
			$data[] = array(
				'SaleJobChild' => $job['SaleJobChild'],
				'SaleJobItem' => $this->get_items($job['SaleJobChild']['id']),
			);
		}

		return $data;

	}

	private function get_items($sale_job_child_id) {
		$this->loadModel('SaleJobItem');
		$items = $this->SaleJobItem->find('all', array(
			'conditions' => array(
				'SaleJobItem.sale_job_child_id' => $sale_job_child_id
				)
			));
		

		$data = array();

		foreach ($items as $item) {
			$data[] = array(
				'SaleJobItem' => $item['SaleJobItem'],
				'InventoryItem' => $item['InventoryItem'],
				'GeneralUnit' => $item['GeneralUnit'],
				'FinishedGood' => $this->get_finish_good($item['SaleJobItem']['id'], '1'),
				'Qac' => $this->get_final('qac', $item['SaleJobItem']['id']),
				'Fat' => $this->get_final('fat', $item['SaleJobItem']['id']),
				'Packing' => $this->get_packings($sale_job_child_id, $item['SaleJobItem']['id']),
				'Delivery' => $this->get_delivery($sale_job_child_id, $item['SaleJobItem']['id']),
				);
		}

		return $data; 
	}

	private function get_packings($sale_job_child_id, $sale_job_item_id){
		$this->loadModel('FinishedGood');
		$packing = $this->FinishedGood->find('all', array(
			'conditions' => array(
				'FinishedGood.packing_status' => 1,
				'FinishedGood.sale_job_child_id' => $sale_job_child_id,
				'FinishedGood.sale_job_item_id' => $sale_job_item_id
			)
		));

		return count($packing);
	}

	private function get_delivery($sale_job_child_id, $sale_job_item_id){
		$this->loadModel('FinishedGood');
		$delivery = $this->FinishedGood->find('all', array(
			'conditions' => array(
				'FinishedGood.delivery_status' => 1,
				'FinishedGood.sale_job_child_id' => $sale_job_child_id,
				'FinishedGood.sale_job_item_id' => $sale_job_item_id
			)
		));

		return count($delivery);
	}

	private function get_final($status, $job_item) {
		 
		$this->loadModel('FinishedGood'); 
		if($status == 'qac') {
			$cond['FinishedGood.status'] = 'Approved'; 
			//$cond['FinishedGood.fat_status'] = 'Waiting';
		}
		if($status == 'fat') {
			$cond['FinishedGood.status'] = 'Approved';
			$cond['FinishedGood.fat_status'] = 'Approved';
		}
		if($status == 'final') {
			$cond['FinishedGood.status'] = 'Approved';
			$cond['FinishedGood.fat_status'] = 'Approved';
			$cond['FinishedGood.final_inspect'] = 'Approved'; 
		}
		$cond['FinishedGood.sale_job_item_id'] = $job_item;
		$prod = $this->FinishedGood->find('all', array(
			'conditions' => $cond
			));
		return count($prod);	 
	}

	private function get_packing($production_order_id = null) {
		if($production_order_id != null) {
			$this->loadModel('FinishedGood');  
			$cond['FinishedGood.production_order_id'] = $production_order_id;
			$prods = $this->FinishedGood->find('all', array(
				'conditions' => $cond
				));
			$id = array();
			foreach ($prods as $prod) {
				$id[] = $prod['FinishedGood']['id'];
			}	
			$this->loadModel('FinishedGoodPackagingItem');

			$packings = $this->FinishedGoodPackagingItem->find('all', array(
				'conditions' => array(
					'FinishedGoodPackagingItem.finished_good_id' => $id // array
					)
				));

			return count($packings);

		} else {
			return 0;
		}
	}

	private function get_finish_good($sale_job_item_id, $production_status) {
		$this->loadModel('FinishedGood');
		$fg = $this->FinishedGood->find('all', array(
			'conditions' => array(
				'FinishedGood.sale_job_item_id' => $sale_job_item_id,
				'FinishedGood.production_status' => $production_status,
				),
			'recursive' => -1
			));  
		return count($fg);
	}

	public function sale($status = null) {
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['SaleTender.title LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['SaleTender.created >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['SaleTender.created <='] = trim($_GET['to']);
			}  
			 
			$this->request->data['SaleTender'] = $_GET;
		}  

		if($status != null) {
			$conditions['SaleTender.status'] = $status; 
		} else {
			$conditions['SaleTender.status !='] = 0; 
		} 

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array( 
			'conditions' => $conditions,
			'order' => 'SaleTender.id DESC', 
			'limit' => $record_per_page
			); 
		
		$this->set('sales', $this->Paginator->paginate());
	} 

	public function ajaxsaletender($status = null) {
		if(isset($_GET['search'])) {
			if($_GET['name'] != '') {
				$conditions['SaleTender.title LIKE'] = '%'.trim($_GET['name']).'%';
			}

			if($_GET['from'] != '') {
				$conditions['SaleTender.created >='] = trim($_GET['from']);
			}

			if($_GET['to'] != '') {
				$conditions['SaleTender.created <='] = trim($_GET['to']);
			}  
			 
			$this->request->data['SaleTender'] = $_GET;
		}  

		if($status != null) {
			$conditions['SaleTender.status'] = $status; 
		} else {
			$conditions['SaleTender.status !='] = 0; 
		}   

		$tenders = $this->SaleTender->find('all', array(
			'conditions' => $conditions 
			));
		$json = array();
		foreach ($tenders as $tender) {
			
		}
	} 

}