<?php
App::uses('AppController', 'Controller');
/**
 * Approvals Controller
 *
 * @property Approval $Approval
 * @property PaginatorComponent $Paginator
 */
class ApprovalsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Approval->recursive = 0;
		$this->Paginator->settings = array('limit' => 50);
		$this->set('approvals', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Approval->exists($id)) {
			throw new NotFoundException(__('Invalid approval'));
		}
		$options = array('conditions' => array('Approval.' . $this->Approval->primaryKey => $id));
		$this->set('approval', $this->Approval->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Approval->create();
			if ($this->Approval->save($this->request->data)) {
				$this->Session->setFlash(__('The approval has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The approval could not be saved. Please, try again.'));
			}
		}
		$users = $this->Approval->User->find('all', array('order' => 'User.username ASC'));
		$group = $this->Approval->Group->find('list', array('order' => 'Group.name ASC'));
		$this->set(compact('users', 'group'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Approval->exists($id)) {
			throw new NotFoundException(__('Invalid approval'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Approval->save($this->request->data)) {
				$this->Session->setFlash(__('The approval has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The approval could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Approval.' . $this->Approval->primaryKey => $id));
			$this->request->data = $this->Approval->find('first', $options);
		}
		$users = $this->Approval->User->find('list');
		$group = $this->Approval->Group->find('list');
		$this->set(compact('users', 'group'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Approval->id = $id;
		if (!$this->Approval->exists()) {
			throw new NotFoundException(__('Invalid approval'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Approval->delete()) {
			$this->Session->setFlash(__('The approval has been deleted.'));
		} else {
			$this->Session->setFlash(__('The approval could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
