<?php
App::uses('AppController', 'Controller');
/**
 * ProjectBudgetItems Controller
 *
 * @property ProjectBudgetItem $ProjectBudgetItem
 * @property PaginatorComponent $Paginator
 */
class ProjectBudgetItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ProjectBudgetItem->recursive = 0;
		$this->set('projectBudgetItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ProjectBudgetItem->exists($id)) {
			throw new NotFoundException(__('Invalid project budget item'));
		}
		$options = array('conditions' => array('ProjectBudgetItem.' . $this->ProjectBudgetItem->primaryKey => $id));
		$this->set('projectBudgetItem', $this->ProjectBudgetItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ProjectBudgetItem->create();
			if ($this->ProjectBudgetItem->save($this->request->data)) {
				$this->Session->setFlash(__('The project budget item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project budget item could not be saved. Please, try again.'));
			}
		}
		$projectBudgets = $this->ProjectBudgetItem->ProjectBudget->find('list');
		$saleQuotationItems = $this->ProjectBudgetItem->SaleQuotationItem->find('list');
		$saleOrderItems = $this->ProjectBudgetItem->SaleOrderItem->find('list');
		$this->set(compact('projectBudgets', 'saleQuotationItems', 'saleOrderItems'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ProjectBudgetItem->exists($id)) {
			throw new NotFoundException(__('Invalid project budget item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectBudgetItem->save($this->request->data)) {
				$this->Session->setFlash(__('The project budget item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project budget item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectBudgetItem.' . $this->ProjectBudgetItem->primaryKey => $id));
			$this->request->data = $this->ProjectBudgetItem->find('first', $options);
		}
		$projectBudgets = $this->ProjectBudgetItem->ProjectBudget->find('list');
		$saleQuotationItems = $this->ProjectBudgetItem->SaleQuotationItem->find('list');
		$saleOrderItems = $this->ProjectBudgetItem->SaleOrderItem->find('list');
		$this->set(compact('projectBudgets', 'saleQuotationItems', 'saleOrderItems'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ProjectBudgetItem->id = $id;
		if (!$this->ProjectBudgetItem->exists()) {
			throw new NotFoundException(__('Invalid project budget item'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectBudgetItem->delete()) {
			$this->Session->setFlash(__('The project budget item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project budget item could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
