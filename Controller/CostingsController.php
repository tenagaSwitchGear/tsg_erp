<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * SaleQuotations Controller
 * CakeEmail
 * @property SaleQuotation $SaleQuotation
 * @property PaginatorComponent $Paginator
 */
class CostingsController extends AppController {

	public $uses = array('SaleQuotation');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxfindchild');
	}
 
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	private function get_currency($id) {
		$this->loadModel('SaleQuotationCurrency');
		$currency = $this->SaleQuotationCurrency->find('first', array(
			'conditions' => array(
				'SaleQuotationCurrency.general_currency_id' => $id
				)
			));
		return $currency['SaleQuotationCurrency']['rate'];
	}

	private function get_default_supplier($inv_item_id) {
		$this->loadModel('InventorySupplierItem');
		$item = $this->InventorySupplierItem->find('first', array(
			'fields' => array(
				'InventorySupplierItem.*',
				'InventorySupplier.*',
				'GeneralCurrency.*',
				),
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $inv_item_id,
				//'InventorySupplierItem.general_currency_id !=' => 1, // ! MYR
				'InventorySupplierItem.is_default' => 1
				),
			'recursive' => 0
			));
		if($item) {
			$price = $item['InventorySupplierItem']['price_per_unit'];
			$total = array(
				'rate' => $this->get_currency($item['InventorySupplierItem']['general_currency_id']), 
				'currency' => $item['GeneralCurrency']['iso_code'],
				'supplier' => $item['InventorySupplier']['name'],
				'inventory_supplier_item_id' => $item['InventorySupplierItem']['id']
				);
			//$total = $price * $rate;	
		} else {
			$total = array('rate' => 1, 'currency' => 'N/A', 'supplier' => 'Not found');
		}
		
		return $total;
	}

	public function currency($id = null) {
		$this->loadModel('GeneralCurrency'); 
		$this->loadModel('SaleQuotationCurrency');
		$currencies = $this->GeneralCurrency->find('all');
		$this->set('currencies', $currencies);

		$rates = $this->SaleQuotationCurrency->find('all', array(
			'conditions' => array(
				'SaleQuotationCurrency.sale_quotation_id' => $id
				),
			'order' => 'SaleQuotationCurrency.general_currency_id ASC'
			)); 

		if ($this->request->is('post')) {
			
			if($rates) {
				$this->SaleQuotationCurrency->deleteAll(array('SaleQuotationCurrency.sale_quotation_id' => $id), false);
			} 

			$currency_id = $this->request->data['SaleQuotationCurrency']['general_currency_id'];
			$rate = $this->request->data['SaleQuotationCurrency']['rate'];
			$actual_rate = $this->request->data['SaleQuotationCurrency']['actual_rate'];
			$margin  = $this->request->data['SaleQuotationCurrency']['margin'];

			for($i = 0; $i < count($currency_id); $i++) {
				$this->SaleQuotationCurrency->create();
				$data = array(
					'general_currency_id' => $currency_id[$i],
					'rate' => $rate[$i],
					'sale_quotation_id' => $id,
					'created' => date('Y-m-d H:i:s'),
					'margin' => $margin[$i],
					'actual_rate' => $actual_rate[$i],
					); 
				$this->SaleQuotationCurrency->save($data);
			}

			// Calculate price by default supplier. * Current rate

			$this->Session->setFlash(__('The Currency Rate has been saved.'), 'success');
			return $this->redirect(array('action' => 'view/' . $id)); 

		}

		$this->set('rates', $rates);

		$sale_bom_id = $this->get_sale_bom($id);

		$items = $this->get_sale_bom_items($sale_bom_id);
	}

	public function itemcost($id = null) {
		$this->loadModel('SaleQuotationCurrency');

		$this->loadModel('SaleBom');
		$this->loadModel('SaleBomChild');
		$this->loadModel('SaleBomItem');
		$bom = $this->SaleBom->find('first', array(
			'conditions' => array(
				'SaleBom.sale_quotation_item_id' => $id
				)
			));
		 
		//exit;

		$this->loadModel('SaleQuotationItem');
		$item = $this->SaleQuotationItem->find('first', array(
			'conditions' => array(
				'SaleQuotationItem.id' => $id
				)
			)); 
		$this->set('detail', $item);


		// Update SaleQuotationItem

		if ($this->request->is(array('post', 'put'))) { 
			$type = $this->request->data['type'];
			$id = $this->request->data['id'];
			$inventory_supplier_item_id = $this->request->data['inventory_supplier_item_id'];
			$price = $this->request->data['price'];
			$total = $this->request->data['total'];
			$buffer = $this->request->data['buffer'];

			for($i = 0; $i < count($id); $i++) {
				if($type[$i] == 0) {
					// Item general
					$this->SaleBomItem->updateAll(
						array(
							'SaleBomItem.price' => "$price[$i]",
							'SaleBomItem.inventory_supplier_item_id' => "$inventory_supplier_item_id[$i]",
							'SaleBomItem.price_total' => "$total[$i]",
							'SaleBomItem.buffer' => "$buffer[$i]"
							),
						array(
							'SaleBomItem.id' => $id[$i]
							)
						);

				} else {
					$this->SaleBomChild->updateAll(
						array(
							'SaleBomChild.unit_price' => "$price[$i]",
							'SaleBomChild.inventory_supplier_item_id' => "$inventory_supplier_item_id[$i]",
							'SaleBomChild.total_price' => "$total[$i]",
							'SaleBomChild.buffer' => "$buffer[$i]" 
							),
						array(
							'SaleBomChild.id' => $id[$i]
							)
						);
				}
			}
			$this->Session->setFlash(__('Price book has been saved to Item Cost.'), 'success');
			return $this->redirect(array('action' => 'view/' . $bom['SaleBom']['sale_quotation_id'])); 
		}

		if($bom) {
			// Check if currency has been added
			$currency = $this->SaleQuotationCurrency->find('first', array('conditions' => array(
				'SaleQuotationCurrency.sale_quotation_id' => $bom['SaleBom']['sale_quotation_id'] 
				)));

			if(!$currency) {
				$this->Session->setFlash(__('Please add Currency first.'), 'error');
				return $this->redirect(array('action' => 'currency/' . $bom['SaleBom']['sale_quotation_id'])); 
			}

			

			/*$items = $this->SaleBomItem->find('all', array(
				'conditions' => array(
					'SaleBomItem.sale_bom_id' => $bom['SaleBom']['id']
					),
				'recursive' => 0
				));

			$childs = $this->SaleBomChild->find('all', array(
				'conditions' => array(
					'SaleBomChild.sale_bom_id' => $bom['SaleBom']['id'],
					'SaleBomChild.inventory_item_id !=' => 0,
					'InventoryItem.type' => 'Purchase'
					),
				'recursive' => 0
				));*/

			$items = $this->SaleBomChild->query("SELECT 
				SaleBomItem.*, 
				InventoryItem.*, 
				GeneralUnit.*  
				FROM sale_bom_items AS SaleBomItem  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomItem.inventory_item_id)
				LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomItem.general_unit_id)
				WHERE SaleBomItem.sale_bom_id=".$bom['SaleBom']['id']);

			$childs = $this->SaleBomChild->query("SELECT 
				SaleBomChild.*, 
				InventoryItem.*  
				FROM sale_bom_childs AS SaleBomChild  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomChild.inventory_item_id)
				WHERE SaleBomChild.sale_bom_id=".$bom['SaleBom']['id']. " 
				AND InventoryItem.type = 'Purchase'
				");

			$data = array();
 
			
			foreach ($childs as $child) {
				$pricebook = $this->price_book($child['InventoryItem']['id']);
				$supplier = $this->get_default_supplier($child['InventoryItem']['id']);
				$data[] = array(
					'id' => $child['SaleBomChild']['id'], 
					'rate' => $supplier['rate'],
					'currency' => $supplier['currency'],
					'supplier' => $supplier['supplier'],
					'inventory_item_id' => $child['InventoryItem']['id'],
					'name' => $child['InventoryItem']['name'],
					'code' => $child['InventoryItem']['code'],
					'quantity' => 1,
					'unit' => 0,
					'quantity_total' => 1,
					'unit_price' => $pricebook,
					'price_total' => $pricebook,
					'inventory_supplier_item_id' => isset($supplier['inventory_supplier_item_id']) ? $supplier['inventory_supplier_item_id'] : 0, 
					'type' => 'S.A',
					'item_type' => $child['InventoryItem']['type'],
					'buffer' => $child['SaleBomChild']['buffer'],
					'total_price' => $child['SaleBomChild']['total_price']
					);
			}
 
			foreach ($items as $item) { 
				$pricebook = $this->price_book($item['InventoryItem']['id']);
				$supplier = $this->get_default_supplier($item['InventoryItem']['id']);
				$data[] = array(
					'id' => $item['SaleBomItem']['id'], 
					'rate' => $supplier['rate'],
					'currency' => $supplier['currency'],
					'supplier' => $supplier['supplier'],
					'inventory_item_id' => $item['InventoryItem']['id'],
					'name' => $item['InventoryItem']['name'],
					'code' => $item['InventoryItem']['code'],
					'quantity' => $item['SaleBomItem']['quantity'],
					'unit' => $item['GeneralUnit']['name'],
					'quantity_total' => $item['SaleBomItem']['quantity_total'],
					'unit_price' => $pricebook,
					'price_total' => $pricebook * $item['SaleBomItem']['quantity'],
					'inventory_supplier_item_id' => !empty($supplier['inventory_supplier_item_id']) ? $supplier['inventory_supplier_item_id'] : 0,  
					'type' => 'I.G',
					'item_type' => $item['InventoryItem']['type'],
					'buffer' => $item['SaleBomItem']['buffer'],
					'total_price' => $item['SaleBomItem']['price_total']
					);
			} 
			
			$this->request->data = $bom;

			$this->set('is_bom', true); 
			$this->set('bom', $bom);
			$this->set('items', $data);	

			if(isset($_GET['price_books'])) {
				$this->loadModel('InventorySupplierItemRequired');
				$count = 0;
				foreach ($data as $required) {
					if($required['unit_price'] == 0) {
						if($this->check_required($required['inventory_item_id']) === true) {
							$this->InventorySupplierItemRequired->create();
							$ins = array(
								'inventory_item_id' => $required['inventory_item_id'],
								'created' => $this->date,
								'user_id' => $this->user_id,
								'status' => 0
								);
							$this->InventorySupplierItemRequired->save($ins);
							$count++;
						}
					}
				}
				if($count > 0) {
					// Email Procurement
					$this->loadModel('User');
					$procs = $this->User->find('all', array('conditions' => array(
						'User.group_id' => 11
						)));
					foreach ($procs as $proc) {
					    $mail = array(
							'template' => 'procurement_price_book_required',
							'subject' => 'Price Book Required',
							'to' => $proc['User']['email'],
							'content' => array(
								'username' => $proc['User']['username'],
								'link' => 'inventory_supplier_item_requireds'
								)
							);
						$this->send_email($mail);
					} 
					$this->Session->setFlash(__('Notification has been sent to Procurement Department.'), 'success');
					return $this->redirect(array('action' => 'itemcost', $id)); 
				}
			}
		} else {
			// Raw material
			
 			$pricebook = $this->price_book($item['InventoryItem']['id']);
			$supplier = $this->get_default_supplier($item['InventoryItem']['id']);
 
			$data = array(
				'id' => $item['SaleQuotationItem']['id'], 
				'rate' => $supplier['rate'],
				'currency' => $supplier['currency'],
				'name' => $item['InventoryItem']['name'],
				'supplier' => $supplier['supplier'],
				'unit' => $item['GeneralUnit']['name'],
				'type' => 'Purchase',
				'item_type' => $item['InventoryItem']['type'],
				'inventory_item_id' => $item['InventoryItem']['id'],
				'code' => $item['InventoryItem']['code'],
				'quantity' => $item['SaleQuotationItem']['quantity'],
				'quantity_total' => $item['SaleQuotationItem']['quantity'],
				'unit_price' => $item['SaleQuotationItem']['unit_price'],
				'planning_price' => $item['SaleQuotationItem']['planning_price'],
				'total_cost' => $item['SaleQuotationItem']['total_cost'],
				'material_cost' => $item['SaleQuotationItem']['material_cost'],
				'indirect_cost' => $item['SaleQuotationItem']['indirect_cost'],
				'soft_cost' => $item['SaleQuotationItem']['soft_cost'],
				'margin_value' => $item['SaleQuotationItem']['margin_value'],
				'unit_price' => $this->price_book($item['InventoryItem']['id']),
				'price_total' => $this->price_book($item['InventoryItem']['id']) * $item['SaleQuotationItem']['quantity'],
				'default_supplier' => $this->default_supplier($item['InventoryItem']['id']),
				'related_suppliers' => $this->non_default_supplier($item['InventoryItem']['id']),
				);
			 
			$this->request->data = $item; 
			$this->set('is_bom', false);
			$this->set('item', $data);

			if(isset($_GET['price_books'])) {
				$this->loadModel('InventorySupplierItemRequired'); 
				if($data['unit_price'] == 0) {
					if($this->check_required($data['inventory_item_id']) === true) {
						$this->InventorySupplierItemRequired->create();
						$ins = array(
							'inventory_item_id' => $data['inventory_item_id'],
							'created' => $this->date,
							'user_id' => $this->user_id,
							'status' => 0
							);
						$this->InventorySupplierItemRequired->save($ins); 
						// Email Procurement
						$this->loadModel('User');
						$procs = $this->User->find('all', array('conditions' => array(
							'User.group_id' => 11
							)));
						foreach ($procs as $proc) {
						    $mail = array(
								'template' => 'procurement_price_book_required',
								'subject' => 'Price Book Required',
								'to' => $proc['User']['email'],
								'content' => array(
									'username' => $proc['User']['username'],
									'link' => 'inventory_supplier_item_requireds'
									)
								);
							$this->send_email($mail);
						} 
						$this->Session->setFlash(__('Notification has been sent to Procurement Department.'), 'success');
						return $this->redirect(array('action' => 'itemcost', $id)); 
					}
				}  
			}

		} 

		$this->set('costing', $item); 
		$this->set('id', $id);
	}

	public function editbom($id = null) {
		$this->loadModel('SaleQuotationCurrency');

		$this->loadModel('SaleBom');
		$this->loadModel('SaleBomChild');
		$this->loadModel('SaleBomItem');
		$bom = $this->SaleBom->find('first', array(
			'conditions' => array(
				'SaleBom.sale_quotation_item_id' => $id
				)
			));
		 
		//exit;

		$this->loadModel('SaleQuotationItem');
		$item = $this->SaleQuotationItem->find('first', array(
			'conditions' => array(
				'SaleQuotationItem.id' => $id
				)
			)); 
		$this->set('detail', $item); 
		// Update SaleQuotationItem

		if ($this->request->is(array('post', 'put'))) { 
			$type = $this->request->data['type'];
			$id = $this->request->data['id'];
			$inventory_supplier_item_id = $this->request->data['inventory_supplier_item_id'];
			$price = $this->request->data['price'];
			$total = $this->request->data['total'];
			$buffer = $this->request->data['buffer'];

			for($i = 0; $i < count($id); $i++) {
				if($type[$i] == 0) {
					// Item general
					$this->SaleBomItem->updateAll(
						array(
							'SaleBomItem.price' => "$price[$i]",
							'SaleBomItem.inventory_supplier_item_id' => "$inventory_supplier_item_id[$i]",
							'SaleBomItem.price_total' => "$total[$i]",
							'SaleBomItem.buffer' => "$buffer[$i]"
							),
						array(
							'SaleBomItem.id' => $id[$i]
							)
						);

				} else {
					$this->SaleBomChild->updateAll(
						array(
							'SaleBomChild.unit_price' => "$price[$i]",
							'SaleBomChild.inventory_supplier_item_id' => "$inventory_supplier_item_id[$i]",
							'SaleBomChild.total_price' => "$total[$i]",
							'SaleBomChild.buffer' => "$buffer[$i]" 
							),
						array(
							'SaleBomChild.id' => $id[$i]
							)
						);
				}
			}
			$this->Session->setFlash(__('Price book has been saved to Item Cost.'), 'success');
			return $this->redirect(array('action' => 'view/' . $bom['SaleBom']['sale_quotation_id'])); 
		}

		if($bom) {
			// Check if currency has been added
			$currency = $this->SaleQuotationCurrency->find('first', array('conditions' => array(
				'SaleQuotationCurrency.sale_quotation_id' => $bom['SaleBom']['sale_quotation_id'] 
				)));

			if(!$currency) {
				$this->Session->setFlash(__('Please add Currency first.'), 'error');
				return $this->redirect(array('action' => 'currency/' . $bom['SaleBom']['sale_quotation_id'])); 
			}

			

			/*$items = $this->SaleBomItem->find('all', array(
				'conditions' => array(
					'SaleBomItem.sale_bom_id' => $bom['SaleBom']['id']
					),
				'recursive' => 0
				));

			$childs = $this->SaleBomChild->find('all', array(
				'conditions' => array(
					'SaleBomChild.sale_bom_id' => $bom['SaleBom']['id'],
					'SaleBomChild.inventory_item_id !=' => 0,
					'InventoryItem.type' => 'Purchase'
					),
				'recursive' => 0
				));*/

			$items = $this->SaleBomChild->query("SELECT 
				SaleBomItem.*, 
				InventoryItem.*, 
				GeneralUnit.*,
				SaleBomChild.*  
				FROM sale_bom_items AS SaleBomItem  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomItem.inventory_item_id)
				LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomItem.general_unit_id)
				LEFT OUTER JOIN sale_bom_childs AS SaleBomChild ON(SaleBomChild.id = SaleBomItem.sale_bom_child_id)
				WHERE SaleBomItem.sale_bom_id=".$bom['SaleBom']['id']." GROUP BY SaleBomItem.id ORDER BY SaleBomItem.sale_bom_child_id ASC");

			$childs = $this->SaleBomChild->query("SELECT 
				SaleBomChild.*,  
				InventoryItem.*, 
				GeneralUnit.*   
				FROM sale_bom_childs AS SaleBomChild  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomChild.inventory_item_id)
				LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomChild.general_unit_id) 
				WHERE SaleBomChild.sale_bom_id=".$bom['SaleBom']['id']. " AND SaleBomChild.parent_id = '0' 
				 GROUP BY SaleBomChild.id ORDER BY SaleBomChild.id ASC
				");

			$data = array();
 
			
			foreach ($childs as $child) { 
				$data[] = array(
					'id' => $child['SaleBomChild']['id'],  
					'inventory_item_id' => $child['InventoryItem']['id'],
					'name' => $child['InventoryItem']['name'],
					'code' => $child['InventoryItem']['code'],
					'quantity' => $child['SaleBomChild']['quantity'],
					'unit' => $child['GeneralUnit']['name'],
					'quantity_total' => 1, 
					'type' => 'S.A', 
					'parent_id' => $child['SaleBomChild']['parent_id'],
					'bom_parent' => $child['SaleBomChild']['bom_parent'] 
					);
			}
 			
 			/*
			foreach ($items as $item) {  
				$data[] = array(
					'id' => $item['SaleBomItem']['id'],  
					'inventory_item_id' => $item['InventoryItem']['id'],
					'name' => $item['InventoryItem']['name'],
					'code' => $item['InventoryItem']['code'],
					'quantity' => $item['SaleBomItem']['quantity'],
					'unit' => $item['GeneralUnit']['name'],
					'quantity_total' => $item['SaleBomItem']['quantity_total'], 
					'type' => 'I.G', 
					'parent_id' => $item['SaleBomItem']['sale_bom_child_id'],
					'bom_parent' => 0
					);
			} */
			
			$this->request->data = $bom;

			$this->set('is_bom', true); 
			$this->set('bom', $bom);
			$this->set('items', $data);	

			if(isset($_GET['price_books'])) {
				$this->loadModel('InventorySupplierItemRequired');
				$count = 0;
				foreach ($data as $required) {
					if($required['unit_price'] == 0) {
						if($this->check_required($required['inventory_item_id']) === true) {
							$this->InventorySupplierItemRequired->create();
							$ins = array(
								'inventory_item_id' => $required['inventory_item_id'],
								'created' => $this->date,
								'user_id' => $this->user_id,
								'status' => 0
								);
							$this->InventorySupplierItemRequired->save($ins);
							$count++;
						}
					}
				}
				if($count > 0) {
					// Email Procurement
					$this->loadModel('User');
					$procs = $this->User->find('all', array('conditions' => array(
						'User.group_id' => 11
						)));
					foreach ($procs as $proc) {
					    $mail = array(
							'template' => 'procurement_price_book_required',
							'subject' => 'Price Book Required',
							'to' => $proc['User']['email'],
							'content' => array(
								'username' => $proc['User']['username'],
								'link' => 'inventory_supplier_item_requireds'
								)
							);
						$this->send_email($mail);
					} 
					$this->Session->setFlash(__('Notification has been sent to Procurement Department.'), 'success');
					return $this->redirect(array('action' => 'itemcost', $id)); 
				}
			}
		} 

		$this->set('costing', $item); 
		$this->set('id', $id);
	}

	public function ajaxfindchild() {
		$this->autoRender = false;
		if(isset($_GET['parent_id'])) {
			$parent_id = (int)$_GET['parent_id'];
			$bom_id = (int)$_GET['bom_id'];	

			$this->loadModel('SaleBomChild');   
			$childs = $this->SaleBomChild->query("SELECT 
				SaleBomChild.*,  
				InventoryItem.*, 
				GeneralUnit.*   
				FROM sale_bom_childs AS SaleBomChild  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomChild.inventory_item_id)
				LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomChild.general_unit_id) 
				WHERE SaleBomChild.sale_bom_id=".$bom_id. " AND SaleBomChild.parent_id = ". $parent_id . " 
				GROUP BY SaleBomChild.id ORDER BY SaleBomChild.id ASC");

			$items = $this->SaleBomChild->query("SELECT 
				SaleBomItem.*, 
				InventoryItem.*, 
				GeneralUnit.*,
				SaleBomChild.*  
				FROM sale_bom_childs AS SaleBomChild   
				LEFT OUTER JOIN sale_bom_items AS SaleBomItem ON(SaleBomChild.id = SaleBomItem.sale_bom_child_id)
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomItem.inventory_item_id)
				LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomItem.general_unit_id)
				WHERE SaleBomItem.sale_bom_id=".$bom_id." AND SaleBomChild.bom_parent = ". $parent_id . "  GROUP BY SaleBomItem.id ORDER BY SaleBomItem.id ASC");


			$data = array(); 
			
			foreach ($childs as $child) { 
				
				$list[] = array(
						'inventory_item_id' => $child['InventoryItem']['id'],
						'name' => $child['InventoryItem']['name'],
						'code' => $child['InventoryItem']['code'],
						'quantity' => $child['SaleBomChild']['quantity'],
						'unit' => $child['GeneralUnit']['name'],
						'quantity_total' => 1, 
						'type' => 'S.A', 
						'parent_id' => $child['SaleBomChild']['parent_id'],
						'bom_parent' => $child['SaleBomChild']['bom_parent'],
						'childNodeType' => 'branch'	 
					);

				$data['nodeID'][$parent_id] = $list; 
			}  

			foreach ($items as $item) {  
				$itm[] = array(  
					'inventory_item_id' => $item['InventoryItem']['id'],
					'name' => $item['InventoryItem']['name'],
					'code' => $item['InventoryItem']['code'],
					'quantity' => $item['SaleBomItem']['quantity'],
					'unit' => $item['GeneralUnit']['name'],
					'quantity_total' => $item['SaleBomItem']['quantity_total'], 
					'type' => 'I.G', 
					'parent_id' => $item['SaleBomChild']['parent_id'],
					'bom_parent' => 0,
					'childNodeType' => 'leaf'
					);
				$data['nodeID'][$parent_id] = $itm; 
			}  
			echo json_encode($data);

		}  
	} 

	private function loop_childs($parent_id, $bom_id) { 
		$this->loadModel('SaleBomChild');  

		$childs = $this->SaleBomChild->query("SELECT 
			SaleBomChild.*, 
			SaleBomChildParent.*,
			InventoryItem.*, 
			GeneralUnit.*   
			FROM sale_bom_childs AS SaleBomChild  
			LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomChild.inventory_item_id)
			LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomChild.general_unit_id)
			LEFT OUTER JOIN sale_bom_childs AS SaleBomChildParent ON(SaleBomChildParent.bom_parent = SaleBomChild.parent_id AND SaleBomChildParent.sale_bom_id = '".$bom_id. "')
			WHERE SaleBomChild.sale_bom_id=".$bom_id. " AND SaleBomChild.parent_id =". $parent_id . " 
			GROUP BY SaleBomChild.id ORDER BY SaleBomChild.id ASC");

		$data = array(); 
		
		foreach ($childs as $child) { 
			$data[] = array(
				'id' => $child['SaleBomChild']['id'],  
				'inventory_item_id' => $child['InventoryItem']['id'],
				'name' => $child['SaleBomChild']['name'],
				'code' => $child['InventoryItem']['code'],
				'quantity' => $child['SaleBomChild']['quantity'],
				'unit' => $child['GeneralUnit']['name'],
				'quantity_total' => 1, 
				'type' => 'S.A', 
				'parent_id' => $child['SaleBomChildParent']['id'] == null ? 0 : $child['SaleBomChildParent']['id'],
				'bom_parent' => $child['SaleBomChild']['bom_parent'] 
				);
		}  
		return $data;
	} 

	private function check_required($item_id) {
		$this->loadModel('InventorySupplierItemRequired');
		$required = $this->InventorySupplierItemRequired->find('first', array(
			'conditions' => array(
				'InventorySupplierItemRequired.inventory_item_id' => $item_id
				),
			'recursive' => -1
			));
		if($required) {
			return false;
		}
		return true;
	}

	private function default_supplier($item_id) {
		$this->loadModel('InventorySupplierItem');
		$default = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $item_id,
				'InventorySupplierItem.is_default' => 1
				)
			));
		return $default;
	}

	private function non_default_supplier($item_id) {
		$this->loadModel('InventorySupplierItem');
		$default = $this->InventorySupplierItem->find('all', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $item_id,
				'InventorySupplierItem.is_default' => 0
				)
			));
		return $default;
	}

	public function viewitem($id = null) {
		$this->loadModel('SaleQuotationCurrency'); 
		$this->loadModel('SaleBom');
		$this->loadModel('SaleBomItem');
		$bom = $this->SaleBom->find('first', array(
			'conditions' => array(
				'SaleBom.sale_quotation_item_id' => $id
				)
			));

		$this->loadModel('SaleQuotationItem');
		$item = $this->SaleQuotationItem->find('first', array(
			'conditions' => array(
				'SaleQuotationItem.id' => $id
				)
			)); 
		// Update SaleQuotationItem
		if ($this->request->is(array('post', 'put'))) { 
			$this->SaleQuotationItem->id = $id;
			if($this->SaleQuotationItem->save($this->request->data)) {
				$this->loadModel('SaleQuotation');
				$price = $this->request->data['SaleQuotationItem']['planning_price'];
				$this->SaleQuotation->updateAll(array(
					'SaleQuotation.planning_price' => "'".$price."'",
					'SaleQuotation.status' => "'2'"
					), array(
						'SaleQuotation.id' => $item['SaleQuotationItem']['sale_quotation_id']
					)
				);
				

				$this->Session->setFlash(__('Margin has been saved.'), 'success');
				return $this->redirect(array('action' => 'view/' . $item['SaleQuotationItem']['sale_quotation_id']));
			} else {
				$this->Session->setFlash(__('Margin could not be saved.'), 'error');
			}
		}

		if($bom) {
			// Check if currency has been added
			$currency = $this->SaleQuotationCurrency->find('first', array('conditions' => array(
				'SaleQuotationCurrency.sale_quotation_id' => $bom['SaleBom']['sale_quotation_id'] 
				)));

			if(!$currency) {
				$this->Session->setFlash(__('Please add Currency first.'), 'error');
				return $this->redirect(array('action' => 'currency/' . $bom['SaleBom']['sale_quotation_id'])); 
			}
 

			$this->loadModel('SaleBomChild'); 

			$items = $this->SaleBomChild->query("SELECT 
				SaleBomItem.*, 
				InventoryItem.*, 
				GeneralUnit.*  
				FROM sale_bom_items AS SaleBomItem  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomItem.inventory_item_id)
				LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomItem.general_unit_id)
				WHERE SaleBomItem.sale_bom_id=".$bom['SaleBom']['id']);

			$childs = $this->SaleBomChild->query("SELECT 
				SaleBomChild.*, 
				InventoryItem.*  
				FROM sale_bom_childs AS SaleBomChild  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomChild.inventory_item_id)
				WHERE SaleBomChild.sale_bom_id=".$bom['SaleBom']['id']. " 
				AND InventoryItem.type = 'Purchase'
				");

			$data = array();
 
			
			foreach ($childs as $child) {
				$pricebook = $this->price_book($child['InventoryItem']['id']);
				$supplier = $this->get_default_supplier($child['InventoryItem']['id']);
				$data[] = array(
					'id' => $child['SaleBomChild']['id'], 
					'rate' => $supplier['rate'],
					'currency' => $supplier['currency'],
					'supplier' => $supplier['supplier'],
					'inventory_item_id' => $child['InventoryItem']['id'],
					'name' => $child['InventoryItem']['name'],
					'code' => $child['InventoryItem']['code'],
					'quantity' => 1,
					'unit' => 0,
					'quantity_total' => 1,
					'unit_price' => $pricebook,
					'price_total' => $pricebook,
					//'default_supplier' => $supplier, 
					'type' => 'S.A',
					'item_type' => $child['InventoryItem']['type'],
					'buffer' => $child['SaleBomChild']['buffer']
					);
			}
 
			foreach ($items as $item) { 
				$pricebook = $this->price_book($item['InventoryItem']['id']);
				$supplier = $this->get_default_supplier($item['InventoryItem']['id']);
				$data[] = array(
					'id' => $item['SaleBomItem']['id'], 
					'rate' => $supplier['rate'],
					'currency' => $supplier['currency'],
					'supplier' => $supplier['supplier'],
					'inventory_item_id' => $item['InventoryItem']['id'],
					'name' => $item['InventoryItem']['name'],
					'code' => $item['InventoryItem']['code'],
					'quantity' => $item['SaleBomItem']['quantity'],
					'unit' => $item['GeneralUnit']['name'],
					'quantity_total' => $item['SaleBomItem']['quantity_total'],
					'unit_price' => $pricebook,
					'price_total' => $pricebook * $item['SaleBomItem']['quantity'],
					//'default_supplier' => $this->default_supplier($item['InventoryItem']['id']), 
					'type' => 'I.G',
					'item_type' => $item['InventoryItem']['type'],
					'buffer' => $item['SaleBomItem']['buffer']
					);
			} 

			// View Costing
			$this->loadModel('SaleQuotationItemCost');

			$indirect_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Indirect Cost',
					)
				)); 

			$soft_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Soft Cost',
					)
				));

			$this->set('costs', $indirect_costs);
			$this->set('soft_costs', $soft_costs);

			$this->request->data = $bom;

			$this->set('is_bom', true);
			
			$this->set('bom', $bom);
			$this->set('items', $data);	
		} else {
			// Raw material
			$this->loadModel('SaleQuotationItemCost');

			$indirect_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Indirect Cost',
					)
				)); 

			$soft_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Soft Cost',
					)
				));
 
			$data = array(
				'id' => $item['SaleQuotationItem']['id'], 
				'rate' => $this->get_default_supplier($item['InventoryItem']['id'])['rate'],
				'currency' => $this->get_default_supplier($item['InventoryItem']['id'])['currency'],
				'name' => $item['InventoryItem']['name'],
				'code' => $item['InventoryItem']['code'],
				'quantity' => $item['SaleQuotationItem']['quantity'],
				'quantity_total' => $item['SaleQuotationItem']['quantity'],
				'unit_price' => $item['SaleQuotationItem']['unit_price'],
				'planning_price' => $item['SaleQuotationItem']['planning_price'],
				'total_cost' => $item['SaleQuotationItem']['total_cost'],
				'material_cost' => $item['SaleQuotationItem']['material_cost'],
				'indirect_cost' => $item['SaleQuotationItem']['indirect_cost'],
				'soft_cost' => $item['SaleQuotationItem']['soft_cost'],
				'margin_value' => $item['SaleQuotationItem']['margin_value'],
				'unit_price' => $this->price_book($item['InventoryItem']['id']),
				'price_total' => $this->price_book($item['InventoryItem']['id']) * $item['SaleQuotationItem']['quantity']
				);
			 
			$this->request->data = $item;
			$this->set('costs', $indirect_costs);
			$this->set('soft_costs', $soft_costs);
			$this->set('is_bom', false);
			$this->set('item', $data);
		} 

		$this->set('costing', $item);
	}

	public function printcosting($id = null) {

		$this->layout = 'print'; 

		$this->loadModel('SaleQuotationCurrency'); 
		$this->loadModel('SaleBom');
		$this->loadModel('SaleBomItem');
		$bom = $this->SaleBom->find('first', array(
			'conditions' => array(
				'SaleBom.sale_quotation_item_id' => $id
				)
			));

		$this->loadModel('SaleQuotationItem');
		$item = $this->SaleQuotationItem->find('first', array(
			'conditions' => array(
				'SaleQuotationItem.id' => $id
				)
			)); 
		// Update SaleQuotationItem
		if ($this->request->is(array('post', 'put'))) { 
			$this->SaleQuotationItem->id = $id;
			if($this->SaleQuotationItem->save($this->request->data)) {
				$this->loadModel('SaleQuotation');
				$price = $this->request->data['SaleQuotationItem']['planning_price'];
				$this->SaleQuotation->updateAll(array(
					'SaleQuotation.planning_price' => "'".$price."'",
					'SaleQuotation.status' => "'2'"
					), array(
						'SaleQuotation.id' => $item['SaleQuotationItem']['sale_quotation_id']
					)
				);
				

				$this->Session->setFlash(__('Margin has been saved.'), 'success');
				return $this->redirect(array('action' => 'view/' . $item['SaleQuotationItem']['sale_quotation_id']));
			} else {
				$this->Session->setFlash(__('Margin could not be saved.'), 'error');
			}
		}

		if($bom) {
			// Check if currency has been added
			$currency = $this->SaleQuotationCurrency->find('first', array('conditions' => array(
				'SaleQuotationCurrency.sale_quotation_id' => $bom['SaleBom']['sale_quotation_id'] 
				)));

			if(!$currency) {
				$this->Session->setFlash(__('Please add Currency first.'), 'error');
				return $this->redirect(array('action' => 'currency/' . $bom['SaleBom']['sale_quotation_id'])); 
			}
 

			$this->loadModel('SaleBomChild'); 

			$items = $this->SaleBomChild->query("SELECT 
				SaleBomItem.*, 
				InventoryItem.*, 
				GeneralUnit.*  
				FROM sale_bom_items AS SaleBomItem  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomItem.inventory_item_id)
				LEFT OUTER JOIN general_units AS GeneralUnit ON(GeneralUnit.id = SaleBomItem.general_unit_id)
				WHERE SaleBomItem.sale_bom_id=".$bom['SaleBom']['id']);

			$childs = $this->SaleBomChild->query("SELECT 
				SaleBomChild.*, 
				InventoryItem.*  
				FROM sale_bom_childs AS SaleBomChild  
				LEFT OUTER JOIN inventory_items AS InventoryItem ON(InventoryItem.id = SaleBomChild.inventory_item_id)
				WHERE SaleBomChild.sale_bom_id=".$bom['SaleBom']['id']. " 
				AND InventoryItem.type = 'Purchase'
				");

			$data = array();
 
			
			foreach ($childs as $child) {
				$pricebook = $this->price_book($child['InventoryItem']['id']);
				$supplier = $this->get_default_supplier($child['InventoryItem']['id']);
				$data[] = array(
					'id' => $child['SaleBomChild']['id'], 
					'rate' => $supplier['rate'],
					'currency' => $supplier['currency'],
					'supplier' => $supplier['supplier'],
					'inventory_item_id' => $child['InventoryItem']['id'],
					'name' => $child['InventoryItem']['name'],
					'code' => $child['InventoryItem']['code'],
					'quantity' => 1,
					'unit' => 0,
					'quantity_total' => 1,
					'unit_price' => $pricebook,
					'price_total' => $pricebook,
					//'default_supplier' => $supplier, 
					'type' => 'S.A',
					'item_type' => $child['InventoryItem']['type'],
					'buffer' => $child['SaleBomChild']['buffer']
					);
			}
 
			foreach ($items as $item) { 
				$pricebook = $this->price_book($item['InventoryItem']['id']);
				$supplier = $this->get_default_supplier($item['InventoryItem']['id']);
				$data[] = array(
					'id' => $item['SaleBomItem']['id'], 
					'rate' => $supplier['rate'],
					'currency' => $supplier['currency'],
					'supplier' => $supplier['supplier'],
					'inventory_item_id' => $item['InventoryItem']['id'],
					'name' => $item['InventoryItem']['name'],
					'code' => $item['InventoryItem']['code'],
					'quantity' => $item['SaleBomItem']['quantity'],
					'unit' => $item['GeneralUnit']['name'],
					'quantity_total' => $item['SaleBomItem']['quantity_total'],
					'unit_price' => $pricebook,
					'price_total' => $pricebook * $item['SaleBomItem']['quantity'],
					//'default_supplier' => $this->default_supplier($item['InventoryItem']['id']), 
					'type' => 'I.G',
					'item_type' => $item['InventoryItem']['type'],
					'buffer' => $item['SaleBomItem']['buffer']
					);
			} 

			// View Costing
			$this->loadModel('SaleQuotationItemCost');

			$indirect_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Indirect Cost',
					)
				)); 

			$soft_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Soft Cost',
					)
				));

			$this->set('costs', $indirect_costs);
			$this->set('soft_costs', $soft_costs);

			$this->request->data = $bom;

			$this->set('is_bom', true);
			
			$this->set('bom', $bom);
			$this->set('items', $data);	
		} else {
			// Raw material
			$this->loadModel('SaleQuotationItemCost');

			$indirect_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Indirect Cost',
					)
				)); 

			$soft_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Soft Cost',
					)
				));
 
			$data = array(
				'id' => $item['SaleQuotationItem']['id'], 
				'rate' => $this->get_default_supplier($item['InventoryItem']['id'])['rate'],
				'currency' => $this->get_default_supplier($item['InventoryItem']['id'])['currency'],
				'name' => $item['InventoryItem']['name'],
				'code' => $item['InventoryItem']['code'],
				'quantity' => $item['SaleQuotationItem']['quantity'],
				'quantity_total' => $item['SaleQuotationItem']['quantity'],
				'unit_price' => $item['SaleQuotationItem']['unit_price'],
				'planning_price' => $item['SaleQuotationItem']['planning_price'],
				'total_cost' => $item['SaleQuotationItem']['total_cost'],
				'material_cost' => $item['SaleQuotationItem']['material_cost'],
				'indirect_cost' => $item['SaleQuotationItem']['indirect_cost'],
				'soft_cost' => $item['SaleQuotationItem']['soft_cost'],
				'margin_value' => $item['SaleQuotationItem']['margin_value'],
				'unit_price' => $this->price_book($item['InventoryItem']['id']),
				'price_total' => $this->price_book($item['InventoryItem']['id']) * $item['SaleQuotationItem']['quantity']
				);
			 
			$this->request->data = $item;
			$this->set('costs', $indirect_costs);
			$this->set('soft_costs', $soft_costs);
			$this->set('is_bom', false);
			$this->set('item', $data);
		} 

		$this->set('costing', $item);
	}

	private function price_book($item_id) {
		$this->loadModel('InventorySupplierItem');
		$item = $this->InventorySupplierItem->find('first', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $item_id,
				'InventorySupplierItem.is_default' => 1
				),
			'recursive' => -1
			));
		if($item) {
			return $item['InventorySupplierItem']['price_per_unit'];
		} else {
			return 0;
		} 
	}

	public function salemargin($id = null) {
		$this->loadModel('SaleQuotationCurrency');

		$this->loadModel('SaleBom');
		$this->loadModel('SaleBomItem');
		$bom = $this->SaleBom->find('first', array(
			'conditions' => array(
				'SaleBom.sale_quotation_item_id' => $id
				)
			));

		$this->loadModel('SaleQuotationItem');
		$item = $this->SaleQuotationItem->find('first', array(
			'conditions' => array(
				'SaleQuotationItem.id' => $id
				)
			)); 

		$this->set('detail', $item);

		// Remark by HOD / GM 
		$remarks = $this->SaleQuotationItem->SaleQuotationRemark->find('all', array(
			'conditions' => array(
				'SaleQuotationRemark.sale_quotation_item_id' => $id
				),
			'order' => array('SaleQuotationRemark.id' => 'DESC')
			));
		
		$this->set('remarks', $remarks);
		$this->set('price', $item);
		$auth = $this->Session->read('Auth.User');
 		
 		$status = array();

		if($auth['group_id'] == 17) { // HOD
			$status = array(4 => 'Verify', 7 => 'Reject (Please add Remark)');
		}
		if($auth['group_id'] == 19) { // GM
			$status = array(6 => 'Approve', 8 => 'Reject (Please add Remark)');
		}
		$this->set('status', $status);	
		// Update SaleQuotationItem
		if ($this->request->is(array('post', 'put'))) { 
			if(isset($_GET['ref'])) {
				// Verifier
				$this->request->data['SaleQuotationRemark']['sale_quotation_id'] = 0; // Remark share with quotation / item
				$this->request->data['SaleQuotationRemark']['sale_quotation_item_id'] = $id;
				$this->request->data['SaleQuotationRemark']['created'] = $this->date;
				$this->request->data['SaleQuotationRemark']['user_id'] = $auth['id'];
				$this->request->data['SaleQuotationRemark']['group_id'] = $auth['group_id']; 
				if($this->SaleQuotationItem->SaleQuotationRemark->save($this->request->data)) {

					$this->Session->setFlash(__('Remark has been saved.'), 'success');
					return $this->redirect(array('action' => 'salemargin/' . $id . '?ref=verify'));
				}

			} else {
				$this->SaleQuotationItem->id = $id;
				if($this->SaleQuotationItem->save($this->request->data)) {
					$this->Session->setFlash(__('Margin has been saved.'), 'success');
					return $this->redirect(array('controller' => 'sale_quotations', 'action' => 'view/' . $item['SaleQuotationItem']['sale_quotation_id']));
				} else {
					$this->Session->setFlash(__('Margin could not be saved.'), 'error');
				}	
			} 
		}

		if($bom) {
			// Check if currency has been added
			$currency = $this->SaleQuotationCurrency->find('first', array('conditions' => array(
				'SaleQuotationCurrency.sale_quotation_id' => $bom['SaleBom']['sale_quotation_id']
				)));

			if(!$currency) {
				$this->Session->setFlash(__('Please add Currency first.'), 'error');
				return $this->redirect(array('action' => 'currency/' . $bom['SaleBom']['sale_quotation_id'])); 
			}

			$items = $this->SaleBomItem->find('all', array(
				'conditions' => array(
					'SaleBomItem.sale_bom_id' => $bom['SaleBom']['id']
					)
				));

			$this->loadModel('SaleBomChild');

			$childs = $this->SaleBomChild->find('all', array(
				'conditions' => array(
					'SaleBomChild.sale_bom_id' => $bom['SaleBom']['id'],
					'SaleBomChild.inventory_item_id !=' => 0,
					'InventoryItem.type' => 'Purchase'
					)
				));

			$data = array();
			
			foreach ($childs as $child) {
				$data[] = array(
					'id' => $child['SaleBomChild']['id'], 
					'rate' => $this->get_default_supplier($child['InventoryItem']['id'])['rate'],
					'currency' => $this->get_default_supplier($child['InventoryItem']['id'])['currency'],
					'inventory_item_id' => $child['InventoryItem']['id'],
					'name' => $child['InventoryItem']['name'],
					'code' => $child['InventoryItem']['code'],
					'quantity' => 1,
					'unit' => 0,
					'quantity_total' => 1 * $bom['SaleBom']['no_of_order'],
					'unit_price' => $this->price_book($child['InventoryItem']['id']),
					'price_total' => $this->price_book($child['InventoryItem']['id']) * 1,
					'default_supplier' => $this->default_supplier($child['InventoryItem']['id']),
					'related_suppliers' => $this->non_default_supplier($child['InventoryItem']['id']),
					'type' => 'Subassembly',
					'unit_name' => 'N/A',
					);
			}

			foreach ($items as $item) {
				$data[] = array(
					'id' => $item['SaleBomItem']['id'], 
					'rate' => $this->get_default_supplier($item['InventoryItem']['id'])['rate'],
					'currency' => $this->get_default_supplier($item['InventoryItem']['id'])['currency'], 
					'name' => $item['InventoryItem']['name'],
					'code' => $item['InventoryItem']['code'],
					'quantity' => $item['SaleBomItem']['quantity'],
					'unit_name' => $item['GeneralUnit']['name'],
					'quantity_total' => $item['SaleBomItem']['quantity_total'],
					'unit_price' => $this->price_book($item['InventoryItem']['id']),
					'price_total' => $this->price_book($item['InventoryItem']['id']) * $item['SaleBomItem']['quantity'] 
					);
			}

			// View Costing
			$this->loadModel('SaleQuotationItemCost');

			$indirect_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Indirect Cost',
					)
				)); 

			$soft_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Soft Cost',
					)
				));

			$this->set('costs', $indirect_costs);
			$this->set('soft_costs', $soft_costs);
			
			$this->request->data = $bom; 
			$this->set('is_bom', true); 
			$this->set('bom', $bom);
			$this->set('items', $data);	
		} else {
			// Raw material
			

			$this->loadModel('SaleQuotationItemCost');

			$indirect_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Indirect Cost',
					)
				)); 

			$soft_costs = $this->SaleQuotationItemCost->find('all', array(
				'conditions' => array(
					'SaleQuotationItemCost.sale_quotation_item_id' => $id,
					'SaleQuotationItemCost.type' => 'Soft Cost',
					)
				));

			$this->set('costs', $indirect_costs);
			$this->set('soft_costs', $soft_costs);
 
			$data = array(
				'id' => $item['SaleQuotationItem']['id'], 
				'rate' => $this->get_default_supplier($item['InventoryItem']['id'])['rate'],
				'currency' => $this->get_default_supplier($item['InventoryItem']['id'])['currency'],
				'name' => $item['InventoryItem']['name'],
				'code' => $item['InventoryItem']['code'],
				'quantity' => $item['SaleQuotationItem']['quantity'],
				'unit_name' => $item['GeneralUnit']['name'],
				'quantity_total' => $item['SaleQuotationItem']['quantity'],
				'unit_price' => $this->price_book($item['InventoryItem']['id']),
				'price_total' => $this->price_book($item['InventoryItem']['id']) * $item['SaleQuotationItem']['quantity']
				);
			 
			$this->request->data = $item; 
			$this->set('is_bom', false);
			$this->set('item', $data);
		} 
	}

	private function get_default_supplier_by_product_id($id) {
		$this->loadModel('Product');
		$product = $this->Product->find('first', array(
			'conditions' => array(
				'Product.id' => $id
				)
			));
	}

	private function get_sale_bom($quotation_id) {
		$this->loadModel('SaleBom');
		$boms = $this->SaleBom->find('all', array(
			'conditions' => array(
				'SaleBom.sale_quotation_id' => $quotation_id
				)
			));
		$sale_bom_id = array();
		foreach($boms as $bom) {
			$sale_bom_id[] = $bom['SaleBom']['id'];
		}
		return $sale_bom_id;
	}

	private function get_sale_bom_items($sale_bom_id) {
		$this->loadModel('SaleBomItem');
		$items = $this->SaleBomItem->find('all', array(
			'conditions' => array(
				'SaleBomItem.sale_bom_id' => $sale_bom_id,
				'InventoryItem.general_currency_id !=' => 1
				),
			'contain' => array(
				'InventoryItem'
				)
			));
		foreach ($items as $item) {
			//echo $item['SaleBomItem']['id'] . ' - ' . $item['InventoryItem']['unit_price'] . ' - ' . $this->get_currency($item['InventoryItem']['general_currency_id']) . '<br/>';

		}
	} 

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {  

		if ($this->request->is('post')) {

			$this->loadModel('SaleTender');
 

			$this->SaleQuotation->create();

			$name = $this->request->data['SaleQuotationItem']['name'];
			$product_id = $this->request->data['SaleQuotationItem']['inventory_item_id'];
			$boms = $this->request->data['SaleQuotationItem']['bom_id'];
			$orders = 1; 

			$unit_price = 0;
			$general_unit_id = 0;
 			$discount = 0;
			$tax = 0;

			$count = $this->SaleQuotation->find('all', array(
				'conditions' => array('SaleQuotation.costing' => 1)
				));

			$no = count($count);
			$cost_name = $this->generate_code('PC', $no + 1);
 

			$this->request->data['SaleQuotation']['user_id'] = $this->user_id;
			$this->request->data['SaleQuotation']['costing'] = 1;
			$this->request->data['SaleQuotation']['sale_tender_id'] = 0;
			$this->request->data['SaleQuotation']['customer_id'] = 0;
			$this->request->data['SaleQuotation']['name'] = $name;
			$this->request->data['SaleQuotation']['bom_id'] = $boms;
			$this->request->data['SaleQuotation']['inventory_item_id'] = $product_id; 
 			
			if ($this->SaleQuotation->save($this->request->data)) {
				$quotation_id = $this->SaleQuotation->getLastInsertId();
 
 
				// iTEMS
				$this->loadModel('SaleQuotationItem');
				 
				// Insert items
				$this->SaleQuotationItem->create(); 
				$item = array(
					'name'              => $name,
					'quantity'          => 1,
					'general_unit_id'   => 0,
					'unit_price'        => 0,
					'discount'          => 0,
					'tax'               => 0,
					'total_price'       => 0,
					'inventory_item_id' => $product_id,
					'sale_quotation_id' => $quotation_id,
					'type'              => $boms == 0 ? 0 : $boms,
					'costing'           => 1
					); 
				$this->SaleQuotationItem->save($item);

				$item_id = $this->SaleQuotationItem->getLastInsertId();
				$this->_copy_bom($item_id, $boms, $quotation_id, 0, 1, 0, 0); 

				// If status == 1, then submit to hod
				if($this->request->data['SaleQuotation']['status'] == 1) {
					//$this->send_to_planning($quotation_id);
				}

				$this->Session->setFlash(__('Costing has been saved. Please add Costings, Currency & Margin.'), 'success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Costing could not be saved. Please, try again.'), 'error');
				debug($this->SaleQuotation->invalidFields());
			}
		}

		// Currency

		$this->loadModel('GeneralCurrency');

		$currencies = $this->GeneralCurrency->find('all');
		$this->set('currencies', $currencies);

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');

		$this->loadModel('InventoryItemCategory');
		$item_categories = $this->InventoryItemCategory->find('list');

		$saleTenders = $this->SaleQuotation->SaleTender->find('list');
		$customers = $this->SaleQuotation->Customer->find('list');
		$saleBoms = $this->SaleQuotation->SaleBom->find('list');
		$this->loadModel('BomCategory');
		$categories = $this->BomCategory->find('all');
		$bomcategories = $this->BomCategory->find('list');
		$this->set('categories', $categories);
		$this->set(compact('saleTenders', 'customers', 'saleBoms', 'bomcategories', 'units', 'item_categories'));
	}

	public function quotation($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}

		$this->loadModel('GeneralUnit');

		$units = $this->GeneralUnit->find('list');    
		$this->set(compact('units')); 

		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options); 
  
		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $id
				)
			));
		$this->set('items', $items); 
		$this->set('saleQuotation', $quotation);  
	}


	private function total_price($qty, $price, $discount, $tax) {
		$disc = $price - $discount;
		$after_tax = (($disc / 100) * $tax) + $disc;
		$total = $after_tax * $qty;
		return $total;
	}

/**
 * index method
 *
 * @return void
 */

	public function approval($id = null) {
		if($id == NULL) {
			$status = 3;
		} else {
			$status = $id;
		}

		$conditions = array();
		if(!empty($this->data)) {
			if(!empty($this->data['SaleQuotation']['sale_tender_id'])) {
				$conditions['SaleQuotation.sale_tender_id'] = $this->data['SaleQuotation']['sale_tender_id'];
			}
			
			if(isset($this->data['SaleQuotation']['sale_customer_id'])) {
				$conditions['SaleQuotation.sale_customer_id'] = $this->data['SaleQuotation']['sale_customer_id'];
			}

			if(isset($this->data['SaleQuotation']['status'])) {
				$conditions['SaleQuotation.status'] = $this->data['SaleQuotation']['status'];
			} 
		}	

		$conditions['SaleQuotation.costing'] = 1;
		$conditions['SaleQuotation.status'] = $status;

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'SaleQuotation.id DESC',
			'limit' => $record_per_page
			); 
		$this->set('saleQuotations', $this->Paginator->paginate());
	}

	public function index($id = null) {
		if($id == NULL) {
			$status = 1;
		} else {
			$status = $id;
		}

		$conditions = array();
		if(!empty($this->data)) {
			if(!empty($this->data['SaleQuotation']['sale_tender_id'])) {
				$conditions['SaleQuotation.sale_tender_id'] = $this->data['SaleQuotation']['sale_tender_id'];
			}
			
			if(isset($this->data['SaleQuotation']['sale_customer_id'])) {
				$conditions['SaleQuotation.sale_customer_id'] = $this->data['SaleQuotation']['sale_customer_id'];
			}

			if(isset($this->data['SaleQuotation']['status'])) {
				$conditions['SaleQuotation.status'] = $this->data['SaleQuotation']['status'];
			} 
		}	

		$conditions['SaleQuotation.costing'] = 1;
		$conditions['SaleQuotation.status'] = $status;

		$record_per_page = Configure::read('Reading.nodes_per_page');

		$this->Paginator->settings = array('conditions' => $conditions,   
			'order' => 'SaleQuotation.id DESC',
			'limit' => $record_per_page
			); 
		$this->set('saleQuotations', $this->Paginator->paginate());
	}

	public function bom($id = null, $sale_quotation_id = null) { 
		$this->loadModel('SaleBom');
		$options = array('conditions' => array('SaleBom.' . $this->SaleBom->primaryKey => $id, 'SaleBom.sale_quotation_id' => $sale_quotation_id));
		$salebom = $this->SaleBom->find('first', $options);

		$this->loadModel('Bom');
		$options = array('conditions' => array('Bom.' . $this->Bom->primaryKey => $salebom['SaleBom']['bom_id']));
		$bom = $this->Bom->find('first', $options);

		$this->set('bom', $bom); 
		$this->set('salebom', $salebom);
	}

	public function ajaxfindquotation() {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		if(isset($this->request->query['term'])) {
			$term = $this->request->query['term'];
			$conditions = array();

			$conditions['OR']['SaleQuotation.name LIKE'] = '%'.$term.'%';
			$conditions['OR']['SaleQuotation.remark LIKE'] = '%'.$term.'%';

			$items = $this->SaleQuotation->find('all', array(
				'conditions' => $conditions,
				'group' => array('SaleQuotation.id')
				));
			
			$json = array();
			foreach ($items as $item) {
				$json[] = array(
					'id' => $item['SaleQuotation']['id'], 
					'name' => $item['SaleQuotation']['name'],
					'customer' => $item['Customer']['name'],
					'tender_name' => $item['SaleTender']['title'],
					'tender_no' => $item['SaleTender']['tender_no'],
					'tender_id' => $item['SaleTender']['id'],
					'customer_id' => $item['SaleTender']['customer_id']
					); 
			}
			echo json_encode($json);	
		}
	}

	private function check_child($id, $parent_id) {
		$this->loadModel('SaleBomChild');
		$rows = $this->SaleBomChild->find('all', array(
			'conditions' => array(
				'SaleBomChild.sale_bom_id' => $id,
				'SaleBomChild.parent_id' => $parent_id
				)
			));
		if(count($rows) > 0) {
			return "closed";
		} else {
			$this->loadModel('SaleBomItem');
			$rows = $this->SaleBomItem->find('all', array(
				'conditions' => array( 
					'SaleBomItem.sale_bom_child_id' => $parent_id
					)
				));
			if(count($rows) > 0) {
				return "closed";
			} else {
				return '';
			}
		}	
	}

	public function ajaxtree($id) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$result = array();  

		$this->loadModel('SaleBomChild');

		$rows = $this->SaleBomChild->find('all', array(
			'conditions' => array(
				'SaleBomChild.sale_bom_id' => $id,
				'SaleBomChild.parent_id' => 0
				)
			));

		foreach ($rows as $row) { 
		    $json['attr']['id'] = $row['SaleBomChild']['id'];  

		    $json['attr']['rel'] = $row['SaleBomChild']['name']; 

		    $json['data']  = $row['SaleBomChild']['name'];

		    $json['state'] = $this->check_child($id, $row['SaleBomChild']['bom_parent']); // if state='closed' this node can be opened

		    $result[] = $json;
		}

		echo json_encode($result);
	}

	public function ajaxtreechild($id = null) {
		$this->autoRender = false;
		$this->layout = 'ajax'; 
		$json = array();  
		$result = array();  

		if(isset($this->request->query['id'])) {
			$parent_id = (int)$this->request->query['id']; 
		
			$this->loadModel('SaleBomChild');

			$rows = $this->SaleBomChild->find('all', array(
				'conditions' => array(
					'SaleBomChild.sale_bom_id' => $id,
					'SaleBomChild.bom_parent' => $parent_id
					)
				));
			// If no rows found, then try to show items
			$this->loadModel('SaleBomItem'); 
			$items = $this->SaleBomItem->find('all', array(
				'conditions' => array( 
					'SaleBomItem.sale_bom_child_id' => $parent_id
					)
				));
			if($rows || $items) {
				foreach ($rows as $row) { 
				    $json['attr']['id'] = $row['SaleBomItem']['id']; 

				    $json['attr']['rel'] = $row['SaleBomItem']['name']; 

				    $json['data']  = $row['SaleBomItem']['name'];

				    $json['state'] = $this->check_child($id, $row['SaleBomItem']['id']); // if state='closed' this node can be opened

				    $result[] = $json;
				}	
			//} else {
				// try to find items
				
				foreach ($items as $item) { 
				    $json['attr']['id'] = $item['SaleBomItem']['id']; 

				    $json['attr']['rel'] = $item['InventoryItem']['name']; 

				    $json['data']  = $item['InventoryItem']['name'] . " Qty: " . $item['SaleBomItem']['quantity'] . " " . $item['GeneralUnit']['name']; 

				    $json['state'] = ''; // if state='closed' this node can be opened

				    $result[] = $json;
				}
			}
			
		}
		echo json_encode($result);
	}

	public function award($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}

		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options); 

		$bom_id = array();
		$boms = array();
		$test = array();
		foreach ($quotation['SaleBom'] as $bom) {
			$bom_id[] = $bom['id']; 
			$boms[] = array(
				'id' => $bom['id'],
				'bom_id' => $bom['bom_id'],
				'name' => $bom['name'],
				'code' => $bom['code'],
				'description' => $bom['description'],
				'price' => $this->get_sale_bom_item($this->get_sale_bom_child($bom['id'])),
				'margin' => $bom['margin'],
				'margin_unit' => $bom['margin_unit'],
				'no_of_order' => $bom['no_of_order']
				); 
		}
 

		$this->set('boms', $boms); 
		$this->set('saleQuotation', $quotation);

		if ($this->request->is('post')) {
			 
		}

		$this->set('quotation', $quotation);
	}

	public function verifyview($id = null) {
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options); 

		$this->loadModel('InventorySupplierItem');

		$books = $this->InventorySupplierItem->find('all', array(
			'conditions' => array(
				'InventorySupplierItem.inventory_item_id' => $quotation['SaleQuotation']['inventory_item_id']
				)
			));

		$this->set('books', $books);

		$group = $this->Session->read('Auth.User.group_id');
		if($group != 1) {
			$this->loadModel('Approval');
			$approval = $this->Approval->find('first', array(
				'conditions' => array(
					'Approval.user_id' => $this->user_id,
					'Approval.name' => 'Costing'
					)
				)); 
			if($approval) { 

			} else {
				$this->Session->setFlash(__('You are not authorize to view this location.'), 'error');
				return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
			}
		} else {
			 
		}

		$bom_id = array();
		$boms = array();
		$test = array();
		foreach ($quotation['SaleBom'] as $bom) {
			$bom_id[] = $bom['id']; 
			$boms[] = array(
				'id' => $bom['id'],
				'bom_id' => $bom['bom_id'],
				'name' => $bom['name'],
				'code' => $bom['code'],
				'description' => $bom['description'],
				'price' => $this->get_sale_bom_item($this->get_sale_bom_child($bom['id'])),
				'margin' => $bom['margin'],
				'margin_unit' => $bom['margin_unit'],
				'no_of_order' => $bom['no_of_order']
				); 
		} 

		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $id
				)
			));
		$this->set('items', $items);

		$this->set('boms', $boms);

		$customer = $this->SaleQuotation->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $quotation['SaleQuotation']['customer_id']
				)
			));

		$this->set('customer', $customer);

		if ($this->request->is('post')) { 

			$auth = $this->Session->read('Auth.User');
			$this->loadModel('SaleQuotationRemark');
			$this->loadModel('SaleQuotation');
			$this->request->data['SaleQuotationRemark']['sale_quotation_id'] = $id;  
			$this->request->data['SaleQuotationRemark']['sale_quotation_item_id'] = 0;
			$this->request->data['SaleQuotationRemark']['created'] = $this->date;
			$this->request->data['SaleQuotationRemark']['user_id'] = $auth['id'];
			$this->request->data['SaleQuotationRemark']['group_id'] = $auth['group_id']; 
			$quot_status = $this->request->data['SaleQuotationRemark']['status'];
			if($this->SaleQuotationRemark->save($this->request->data)) {
				$this->SaleQuotation->id = $id; 
				$update_status = $this->SaleQuotation->updateAll(array(
					'SaleQuotation.status' => "$quot_status"
					), array(
					'SaleQuotation.id' => $id
					));

				if($quot_status == 7) {
					$action = 'Rejected';
				}
				if($quot_status == 4) {
					$action = 'Approved';
				}
 				if($quot_status == 4) {
					$savepricebook = $this->request->data['SaleQuotationRemark']['pricebook'];
					$default = $this->request->data['SaleQuotationRemark']['default'];
					if($savepricebook == 1) {
						if($default == 1) {
							// Override current default
							$this->InventorySupplierItem->updateAll(array(
									'InventorySupplierItem.is_default' => "'0'",
									'InventorySupplierItem.modified' => "'".$this->date."'"
								),
								array(
									'InventorySupplierItem.inventory_item_id' => $quotation['SaleQuotation']['inventory_item_id']
								)
							);
						}
						$this->loadModel('InventorySupplierItem');
						$supply = array(
							'inventory_item_id' => $quotation['SaleQuotation']['inventory_item_id'],
							'price' => $quotation['SaleQuotation']['planning_price'],
							'price_per_unit' => $items[0]['SaleQuotationItem']['planning_price'],
							'general_unit_id' => $items[0]['SaleQuotationItem']['general_unit_id'],
							'conversion_unit_id' => 0,
							'created' => $this->date,
							'modified' => $this->date,
							'effective_date' => $this->date,
							'expiry_date' => $this->request->data['SaleQuotation']['date'],
							'inventory_supplier_id' => 841,
							'general_currency_id' => 1,
							'is_default' => $default,
							'qc_inspect' => 0,
							'min_order' => 1,
							'min_order_unit' => $items[0]['SaleQuotationItem']['general_unit_id'],
							'conversion_qty' => 1,
							'part_number' => $items[0]['InventoryItem']['code'],
							'lead_time' => 1,
							'quotation_number' => 'Auto Generated',
							'sale_quotation_id' => $id
							);
						$this->InventorySupplierItem->save($supply);
					} 
				} 

				$data = array(
					'to' => $quotation['User']['email'],
					'template' => 'costingreview',
					'subject' => $quotation['SaleQuotation']['name'] . ' Has Been ' . $action,
					'content' => array(
						'from' => $this->Session->read('User.username'),
						'username' => $quotation['User']['username'],
						'itemtoverified' => $quotation['SaleQuotation']['name'],
						'action' => $action,
						'link' => 'costings/view/'.$id
						)
					);
				$this->send_email($data);
				if($update_status) {
					$this->Session->setFlash(__('Quotation status has been ' . $action), 'success');
					return $this->redirect(array('action' => 'approval'));
				} 
			}	 
		} 
		$this->set('saleQuotation', $quotation);

		$this->request->data = $quotation;

		$this->loadModel('SaleQuotationRemark');
		$remarks = $this->SaleQuotationRemark->find('all', array(
			'conditions' => array(
				'SaleQuotationRemark.sale_quotation_id' => $id
				),
			'order' => array('SaleQuotationRemark.id' => 'DESC')
			));
 		$this->set('remarks', $remarks);
 		
	}
 
	public function view($id = null) {
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));

		$quotation = $this->SaleQuotation->find('first', $options); 

		$bom_id = array();
		$boms = array();
		$test = array();
		foreach ($quotation['SaleBom'] as $bom) {
			$bom_id[] = $bom['id']; 
			$boms[] = array(
				'id' => $bom['id'],
				'bom_id' => $bom['bom_id'],
				'name' => $bom['name'],
				'code' => $bom['code'],
				'description' => $bom['description'],
				'price' => $this->get_sale_bom_item($this->get_sale_bom_child($bom['id'])),
				'margin' => $bom['margin'],
				'margin_unit' => $bom['margin_unit'],
				'no_of_order' => $bom['no_of_order']
				); 
		} 

		$items = $this->SaleQuotation->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $id
				)
			));
		$this->set('items', $items);

		$this->set('boms', $boms);

		$customer = $this->SaleQuotation->Customer->find('first', array(
			'conditions' => array(
				'Customer.id' => $quotation['SaleQuotation']['customer_id']
				)
			));

		$this->set('customer', $customer);

		if ($this->request->is('post')) { 
			if($this->check_costing($id) === true) {
				$this->SaleQuotation->id = $id;

				// Find Approval
				$group_id =$this->Session->read('Auth.User.group_id');
				$this->loadModel('Approval');
				$approvals = $this->Approval->find('all', array(
					'conditions' => array(
						'Approval.name' => 'Costing' 
						)
					));
				if($approvals) {

		 			if($this->SaleQuotation->save($this->request->data)) {

		 				// Email sales
		 				//$this->send_to_sales($id, $quotation['SaleQuotation']['name']);

		 				
						foreach ($approvals as $approval) {
							$data = array(
								'to' => $approval['User']['email'],
								'template' => 'costing',
								'subject' => 'Costing Require Your Approval ',
								'content' => array( 
									'from' => $this->Session->read('Auth.User.username'),
									'username' => $approval['User']['username'],  
									'link' => 'costings/verifyview/'.$id
									)
								);
							$this->send_email($data);
						}	
						$this->Session->setFlash(__('Costing has been sent for approval.'), 'success');
						return $this->redirect(array('action' => 'index'));
						
		 				
		 				$this->Session->setFlash(__('Costing has been send for Approval.'), 'success');
						return $this->redirect(array('action' => 'index'));
		 			}	

	 			} else {
					$this->Session->setFlash(__('User approval not found. Please ask Admin to add User.'), 'error');
					return $this->redirect(array('action' => 'index'));
				}

			} else{
				$this->Session->setFlash(__('Please complete costing and margin before submit for approval.'), 'error');
			} 
		} 
		$this->set('saleQuotation', $quotation);

		$this->loadModel('SaleQuotationRemark');
		$remarks = $this->SaleQuotationRemark->find('all', array(
			'conditions' => array(
				'SaleQuotationRemark.sale_quotation_id' => $id
				),
			'order' => array('SaleQuotationRemark.id' => 'DESC')
			));
 		$this->set('remarks', $remarks);
 		
	}

	private function send_to_sales($id, $quotation_no) { 
		$this->loadModel('User');

		$users = $this->User->find('all', array(
			'conditions' => array(
				'User.group_id' => 9
				),
			'reqursive' => -1
			));

		if($users) {
			foreach ($users as $user) {
				$data['to'] = $user['User']['email'];
				$data['template'] = 'planning_costing_completed';
				$data['subject'] = 'Quotation No: ' . $quotation_no . ' Costing Completed';
				$data['content'] = array(
					'quotation_no' => $quotation_no,
					'username' => $user['User']['username'], 
					'link' => 'sale_quotations/view/' . $id 
					);
				$this->send_email($data);
			} 
		}
		return false;	
	}

	private function check_costing($quotation_id) {
		$this->loadModel('SaleQuotationItem');
		$items = $this->SaleQuotationItem->find('all', array(
			'conditions' => array(
				'SaleQuotationItem.sale_quotation_id' => $quotation_id,
				'SaleQuotationItem.total_cost' => 0,
				'SaleQuotationItem.planning_price' => 0
				)
			));
		if(count($items) > 0) {
			return false;
		}
		return true;
	}

	public function verification($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}

		$quotation = $this->SeleQuotation->find('first', array('SaleQuotation.id' => $id));

		if ($this->request->is('post')) {
			// If status == 1, then submit to hod
			if($this->request->data['SaleQuotation']['status'] == 2) {
				$this->submit_verification('salequotations/view/'.$id, 'Approval For Tender Quotation', 'Tender Quotation', 'GM', 'Approval');
			} 

			if($this->request->data['SaleQuotation']['status'] == 3) { 
				$this->after_verification('salequotations/view/'.$id, 'Quotation Has Been Approved', 'Quotation Has Been Approved', $user, 'Next Process');
			} 
		}
	} 

	private function calculate_quotation($bom_id) {
		$child = $this->get_sale_bom_child($bom_id); 

		$item = $this->get_sale_bom_item($child);

		$cost = $this->get_sum_item_price($item);

		return $cost;
	}

	private function get_sale_bom_child($bom_id) {
		$this->loadModel('SaleBomChild');
		$childs = $this->SaleBomChild->find('all', array(
			'fields' => array('id'),
			'conditions' => array(
				'SaleBomChild.sale_bom_id' => $bom_id
				),
			'contain' => array()
			));
		$child_id = array();
		foreach ($childs as $child) {
			$child_id[] = $child['SaleBomChild']['id'];
		}
		return $child_id; 
	}

	private function get_sale_bom_item($child_id) {
		$this->loadModel('SaleBomItem'); 
		$items = $this->SaleBomItem->find('all', array(
			'fields' => array(
				'SUM(price_total) AS price'
				),
			'conditions' => array(
				'SaleBomItem.sale_bom_child_id' => $child_id
				),
			'contain' => array()
			)); 
		foreach ($items as $item) { 
			return $item[0]['price'];
		}  
	}

	private function get_sum_item_price($item_id) {
		$this->loadModel('SaleBomItem');
		$items = $this->SaleBomItem->find('all', array(
			'fields' => array(
				'SUM(price_total) AS price'
				),
			'conditions' => array(
				'SaleBomItem.id' => $item_id
				) 
			));
		foreach ($items as $item) {
			return $item[0]['price'];
		} 
	}

	private function get_item_by_child($child_id) {
		$this->loadModel('BomItem');
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $child_id
				)
			));
		$data = array();
		foreach ($items as $item) {
			$total = $item['InventoryItem']['unit_price'] * $item['BomItem']['quantity'];
			$data[] = array(
				'item_price' => $item['InventoryItem']['unit_price'],
				'quantity' => $item['BomItem']['quantity'],
				'total' => $total
				);
		}
		return $data;
	}  

	private function _copy_bom($item_id, $bom_id, $quotation_id, $tender_id, $no_of_order, $margin, $unit) {
		$this->loadModel('Bom'); 
		$this->loadModel('SaleBom'); 

		$user_id = $this->Session->read('Auth.User.id');

		$conditions['Bom.id'] = $bom_id; 
		$bom = $this->Bom->find('first', array('conditions' => $conditions));
 		
 		if($bom) {
	 		$this->SaleBom->create();
	 		$salebom['sale_quotation_item_id'] = $item_id;
	 		$salebom['inventory_item_id']   = $bom['Bom']['inventory_item_id'];
	 		$salebom['bom_id']              = $bom_id;
			$salebom['sale_tender_id']      = 0;
			$salebom['sale_quotation_id']   = $quotation_id;
			$salebom['name']     		    = $bom['Bom']['name'];
			$salebom['code']          		= $bom['Bom']['code'];
			$salebom['description']         = 0;
			$salebom['created']       		= $this->date; 
			$salebom['user_id']     	    = $user_id;
			$salebom['status']     			= 0;
			$salebom['price']     			= 0; // price already markup
			$salebom['margin']      		= 0;
			$salebom['margin_unit']      	= 0;
			$salebom['no_of_order']         = 1;
			$salebom['general_unit_id'] = 0;


			if($this->SaleBom->save($salebom)) {
				// insert Bom
				$sale_bom_id = $this->SaleBom->getLastInsertId();

				// Insert Child
				$this->_copy_bom_child($bom_id, $sale_bom_id, $no_of_order, $quotation_id); 
			} else {
				debug($this->SaleBom->invalidFields());
			}
 		} 
	}  

	private function _copy_bom_child($bom_id, $sale_bom_id, $no_of_order, $quotation_id) {
		$this->loadModel('BomChild');
		$this->loadModel('SaleBomChild');
		$childs = $this->BomChild->find('all', array(
			'conditions' => array(
					'BomChild.bom_id' => $bom_id
				) 
			));
		$data = array();
		foreach ($childs as $child) {
			$this->SaleBomChild->create();
			$data = array(
				'name' => $child['BomChild']['name'],
				'sale_bom_id' => $sale_bom_id,
				'parent_id' => $child['BomChild']['parent_id'],  // parent id will refer to bom_parent to prevent duplicate
				'bom_parent' => $child['BomChild']['id'],
				'no_of_order' => $no_of_order,
				'margin' => 0,
				'margin_unit' => 1,
				'sale_quotation_id' => $quotation_id,
				'inventory_item_id' => $child['BomChild']['inventory_item_id'] 
				);  
			$this->SaleBomChild->save($data);
			$child_id = $this->SaleBomChild->getLastInsertId();
			$this->_copy_bom_item($bom_id, $child['BomChild']['id'], $sale_bom_id, $no_of_order, $child_id, $quotation_id);
		}  
	}

	private function _copy_bom_item($bom_id, $childs, $sale_bom_id, $no_of_order, $child_id, $quotation_id) {
		$this->loadModel('BomItem');
		 
		$items = $this->BomItem->find('all', array(
			'conditions' => array(
				'BomItem.bom_child_id' => $childs	
				) 
			)); 
		 
		$this->loadModel('SaleBomItem');
		$insert = array();
		foreach ($items as $item) {
			$quantity_total = $item['BomItem']['quantity'] * $no_of_order;
			$price_total = $item['InventoryItem']['unit_price'] * $quantity_total;
			$insert[] = array( 
				'bom_id' => $bom_id,
				'sale_bom_id' => $sale_bom_id,
				'inventory_item_id' => $item['BomItem']['inventory_item_id'],
				'sale_bom_child_id' => $child_id,
				'quantity' => $item['BomItem']['quantity'],
				'no_of_order' => $no_of_order,
				'quantity_total' => $quantity_total, // jumlah order * kuantiti barang
				'general_unit_id' => $item['BomItem']['general_unit_id'],
				'price' => $item['InventoryItem']['unit_price'],
				'price_total' => $price_total,
				'margin' => 0,  // belum markup
				'margin_unit' => 1, // percent 2 == amount 
				'sale_quotation_id' => $quotation_id
				);
		} 
		$this->SaleBomItem->saveAll($insert, array('deep' => true)); 
	}

	private function markup($unit, $margin, $price) {
		// 1 = %, 2 = amt
		if($unit == 1) {
			$total = ($price / 100) * $margin;
		} else {
			$total = $price + $margin;
		}
		return $total;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) { 
		if (!$this->SaleQuotation->exists($id)) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SaleQuotation->save($this->request->data)) {

				// If status == 1, then submit to hod
				if($this->request->data['SaleQuotation']['status'] == 1) {
					$this->submit_verification('salequotations/view/'.$id.'?ref=email', 'Verification For Tender Quotation', 'Tender Quotation', 'HOD', 'Verification');
				}

				$this->Session->setFlash(__('The sale quotation has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The sale quotation could not be saved. Please, try again.'));
			}
		} 
		$options = array('conditions' => array('SaleQuotation.' . $this->SaleQuotation->primaryKey => $id));
		$data = $this->SaleQuotation->find('first', $options);
		$this->request->data = $data;

		$saleTenders = $this->SaleQuotation->SaleTender->find('list');
		$customers = $this->SaleQuotation->Customer->find('list');
		$saleBoms = $this->SaleQuotation->SaleBom->find('all', array('conditions' => array('SaleBom.sale_quotation_id' => $id)));

		$this->set('saleBoms', $saleBoms);
		$this->set('data', $data);

		$this->set(compact('saleTenders', 'customers'));
	}

	// Ie: ('salequotations/verification/id', 'Quotation Verification', 'Tender Quotation', 'HOD') if require HOD verification
	private function submit_verification($link, $subject, $itemtoverified, $who, $action) { 
		$group_id = $this->Session->read('Auth.User.group_id'); 
		$department = $this->find_hod($group_id, $who);    
		if($department) {
			$Email = new CakeEmail('smtp');
			$Email->from(Configure::read('Site.email'));
			$Email->to($department['User']['email']);
			$Email->emailFormat('html');
			$Email->template('internalverification')->viewVars(array('firstname' => $department['User']['firstname'], 'itemtoverified' => $itemtoverified, 'link' => $link, 'action' => $action));
			$Email->subject($subject); 
			if($Email->send()) {
				return true;
			}  
		}
		return false;	
	}

	private function find_hod($group_id, $who) {
		$this->loadModel('InternalDepartment');
		$data = $this->InternalDepartment->find('first', array('InternalDepartment.group_id' => $group_id, 'InternalDepartment.name' => $who));
		return $data;
	}

	private function after_verification($link, $subject, $itemtoverified, $user, $action) {     
		$Email = new CakeEmail('smtp');
		$Email->from(Configure::read('Site.email'));
		$Email->to($user['User']['email']);
		$Email->emailFormat('html');
		$Email->template('internalverification')->viewVars(array('firstname' => $user['User']['firstname'], 'itemtoverified' => $itemtoverified, 'link' => $link, 'action' => $action));
		$Email->subject($subject); 
		if($Email->send()) {
			return true;
		} 
		return false;
	}

	private function find_pic($group_id, $pic) {
		$this->loadModel('InternalDepartment');
		$department = $this->InternalDepartment->find('first', array('InternalDepartment.group_id' => $group_id, 'InternalDepartment.name' => $pic));
		return $department;
	}

	private function check_user_role($user_id, $pic) {
		$group_id = $this->Session->read('Auth.User.group_id'); 
		$user_id = $this->Session->read('Auth.User.id'); 
		$department = $this->find_pic($group_id, $pic);

		if($department['InternalDepartment']['user_id'] == $user_id) {
			return true;
		}
		return false;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SaleQuotation->id = $id;
		if (!$this->SaleQuotation->exists()) {
			throw new NotFoundException(__('Invalid sale quotation'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SaleQuotation->delete()) {
			$this->Session->setFlash(__('The sale quotation has been deleted.'));
		} else {
			$this->Session->setFlash(__('The sale quotation could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
