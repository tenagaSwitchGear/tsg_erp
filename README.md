<h1>Login</h1>

<?php echo $this->Form->create('User'); ?>
    <?php echo $this->Form->input('username'); ?>
    <?php echo $this->Form->input('password'); ?>
    <?php echo $this->Form->checkbox('remember_me'); ?> Remember Me
<?php echo $this->Form->end('Login'); ?>

In your user controller:

public function beforeFilter() {
    $this->Auth->allow(array('login', 'register'));
    parent::beforeFilter();
}

public function login() {
    if ($this->request->is('post')) {

        if ($this->Auth->login()) {

            // did they select the remember me checkbox?
            if ($this->request->data['User']['remember_me'] == 1) {
                // remove "remember me checkbox"
                unset($this->request->data['User']['remember_me']);

                // hash the user's password
                $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);

                // write the cookie
                $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
            }

            return $this->redirect($this->Auth->redirect());

        } else {
            $this->Session->setFlash(__('Username or password is incorrect.'));
        }
    }

    $this->set(array(
        'title_for_layout' => 'Login'
    ));
}

public function logout() {
    // clear the cookie (if it exists) when logging out
    $this->Cookie->delete('remember_me_cookie');

    return $this->redirect($this->Auth->logout());
}




public $components = array(
    'Session',
    'Auth',
    'Cookie'
);

public $uses = array('User');

public function beforeFilter() {
    // set cookie options
    $this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
    $this->Cookie->httpOnly = true;

    if (!$this->Auth->loggedIn() && $this->Cookie->read('remember_me_cookie')) {
        $cookie = $this->Cookie->read('remember_me_cookie');

        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.username' => $cookie['username'],
                'User.password' => $cookie['password']
            )
        ));

        if ($user && !$this->Auth->login($user['User'])) {
            $this->redirect('/users/logout'); // destroy session & cookie
        }
    }
}






















23 Dec

Req Item
1. Add MRN Number (MRN0000001)
2. Department 
3. GRN - Set Location (Store A,B,C) Based on Item general. But can change


Friday: 16 Dec

1. What to Update on Production Order.
2. Material requisition manual or auto.


 




1.EAD
Drawing Status
Remark


2. Sales
- Delivery To Address (Sub Address) masa buat JOB


 

===============================================

19-12-2016
Syidi:
PO: Select Payment Term



===============================================

193.9.200.1   3128


WIFI: 0123456789

User: UMP1 / 2

963852



<div class="form-group">
<label class="control-label">Username</label>
    <?php  echo $this->Form->input("username", array("class"=> "form-control", "placeholder" => "username","label"=> false,'div'=>array('class' => 'form-group'))); ?>			
</div>

Before Foreach

$currentPage = empty($this->Paginator->params['paging']['NamaModel']['page']) ? 1 : $this->Paginator->params['paging']['NamaModel']['page'];
$limit = $this->Paginator->params['paging']['NamaModel']['limit'];	
$startSN = (($currentPage * $limit) + 1) - $limit;


Inside Foreach
<td><?php echo $startSN++; ?></td>


# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact