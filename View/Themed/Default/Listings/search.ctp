<!-- Page Content -->
<?php

function housingCategory() {
    $data = array(2,3,4,5);
    if(isset($_GET['property_category_id'])) {
        $category = (int)$_GET['property_category_id'];
        if($category == '' || !in_array($category, $data)) {
            echo 'style="display: none;"';
        } else {
            echo 'style="display: block;"';
        }
    } else {
        echo 'style="display: none;"';
    }
}

function landCategory() {
    $data = array(6,10,13,15);
    if(isset($_GET['property_category_id'])) {
        $category = (int)$_GET['property_category_id'];
        if($category == '' || !in_array($category, $data)) {
            echo 'style="display: none;"';
        } else {
            echo 'style="display: block;"';
        }
    } else {
        echo 'style="display: none;"';
    }
}

function squareCategory() {
    $data = array(2,3,4,5,8,9,12);
    if(isset($_GET['property_category_id'])) {
        $category = (int)$_GET['property_category_id'];
        if($category == '' || !in_array($category, $data)) {
            echo 'style="display: none;"';
        } else {
            echo 'style="display: block;"';
        }
    } else {
        echo 'style="display: none;"';
    }
}

?>
    <div class="container">

        <div class="row"> 
            <div class="col-lg-12">
            <?php echo $this->Form->create('Property', array('url' => '/listings/search', 'type' => 'get', 'class'=> 'form-horizontal form-label-left input_mask', 'novalidate'=>true)); ?>
        <div class="panel panel-default search-form">
            <div class="panel panel-body"> 
            
                <div class="col-md-2">
                    <?php  echo $this->Form->input("state_id", array("options" => $state, "class"=> "form-control select2me", "empty" => "Entire Malaysia","label"=> false, 'div' => array('class' => 'form-group'), "id" => "state")); ?>
                </div>
                <div class="col-md-3">
                    <?php  echo $this->Form->input("city_id", array("options" => $city, "class"=> "form-control select2me", "empty" => "All Area","label"=> false, 'div' => array('class' => 'form-group'), "id" => "cityId")); ?>
                </div>
                <div class="col-md-3">
                    <?php  echo $this->Form->input("property_category_id", array("id" => "category", "options" => $category, "class"=> "form-control select2me", "empty" => "All Category","label"=> false, 'div' => array('class' => 'form-group'))); ?>
                </div>
                <div class="col-md-3">
                    <?php  echo $this->Form->input("title", array("class"=> "form-control", "placeholder" => "Search", "label"=> false,'div'=>array('class' => 'form-group'))); ?>
                </div>
                <div class="col-md-1">
                    <?php echo $this->Form->button('Search',array('class'=>'btn btn-success')); ?>
                </div>

                <div class="col-sm-2">

                <?php echo $this->Form->input("property_post_type_id", array("options" => $type, "class"=> "form-control select2me", "empty" => "All Type","label"=> false, 'div' => array('class' => 'form-group'))); ?>
                </div>
                <div class="col-sm-2">
                     <?php echo $this->Form->input("price_min", array("options" => $price_min, "class"=> "form-control select2me", "empty" => "MIn Price","label"=> false, 'div' => array('class' => 'form-group'))); ?> 
                </div>
                <div class="col-sm-2"> 
                     <?php echo $this->Form->input("price_max", array("options" => $price_max, "class"=> "form-control select2me", "empty" => "Max Price","label"=> false, 'div' => array('class' => 'form-group'))); ?>
                </div>

                <!-- Show if housing -->
                <div id="ajaxHousing" <?php housingCategory(); ?>>
                    <div class="col-sm-2"> 
                        <?php  echo $this->Form->input("bedroom", array("options" => $bedroom, "empty" => "Bedroom", "id" => "bedroom", "class"=> "form-control", "placeholder" => "Bedroom", "label"=> false,'div'=>array('class' => 'form-group'))); ?>
                    </div>
                    <div class="col-sm-2"> 
                        <?php  echo $this->Form->input("bathroom", array("options" => $bathroom, "empty" => "Bathroom", "id" => "bathroom", "class"=> "form-control", "placeholder" => "Bathroom", "label"=> false,'div'=>array('class' => 'form-group'))); ?>
                    </div> 
                </div>

                <div id="ajaxSquare" <?php squareCategory(); ?>>
                    <div class="col-sm-2"> 
                        <?php  echo $this->Form->input("size", array("options" => $size, "empty" => "Size Sq. Ft", "id" => "square", "class"=> "form-control", "placeholder" => "Size", "label"=> false,'div'=>array('class' => 'form-group'))); ?>
                    </div> 
                </div>

                <div id="ajaxLand" <?php landCategory(); ?>> 
                    <div class="col-sm-2"> 
                        <?php  echo $this->Form->input("size_land", array("options" => $size_land, "empty" => "Size Acre", "id" => "acre", "class"=> "form-control", "placeholder" => "Size", "label"=> false,'div'=>array('class' => 'form-group'))); ?>
                    </div> 
                </div> 
            </div>
        </div>
                <ol class="breadcrumb">
                    <li>  
                        <button type="submit" name="sort" value="1" class="btn btn-default"><i class="fa fa-clock-o"></i> Latest</button>  
                    </li>
                    <li>
                        <button type="submit" name="sort" value="2" class="btn btn-primary"><i class="fa fa-usd"></i> Price</button>
                    </li>
                </ol>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
        <!-- /.row -->

        <!-- Blog Post Row -->
        <div class="row"> 
<div class="col-md-9">
<?php echo $this->Session->flash(); ?> 
<?php if($properties) { ?> 
<?php foreach ($properties as $property): ?>
    <div class="row"> 
        <div class="col-lg-12">
            <div class=" listings">
                <div class="col-sm-4 listing-main-image">
                <?php 
                    if($property['PropertyImage'][0]['name']) {
                        echo $this->Html->link($this->Html->image('Users/Original/'.$property['PropertyImage'][0]['name'], array('class' => 'img-responsive')), array('action' => 'view', $property['Property']['id'], $property['Property']['slug']), array('escape' => false)); 
                    } else {
                        echo $this->Html->link($this->Html->image('Users/Original/noimage.jpg', array('class' => 'img-responsive')), array('action' => 'view', $property['Property']['id'], $property['Property']['slug']), array('escape' => false)); 
                    }
                ?>
                </div>
                <div class="col-sm-6 listing-main-info">
                <h3><?php 
                    echo $this->Html->link(__($property['Property']['title']), array('action' => 'view', $property['Property']['id'], $property['Property']['slug'])); 
                ?></h3>
                
                <h4>RM<?php echo __(number_format($property['Property']['price'])); ?></h4> 
                <hr>
                <p><i class="fa <?php echo __($property['PropertyCategory']['icon']); ?>"></i> <?php echo __($property['PropertyCategory']['name']); ?></p>
                <p><i class="fa fa-map-marker"></i> <?php echo __($property['City']['name']); ?>, <?php echo __($property['State']['name']); ?></p>
                <?php if($property['PropertyCategory']['is_land'] == 0) { ?>
                    <p><i class="fa fa-info-circle"></i> Bedrooms: <b><?php echo __(number_format($property['Property']['bedroom'])); ?></b>
                    Bathrooms: <b><?php echo __($property['Property']['bathroom']); ?></b> 
                    Size: <b><?php echo __(number_format($property['Property']['size'])); ?> <?php echo __($property['PropertyCategory']['size']); ?></b></p>
                <?php } ?>    

                </div>
                <div class="col-sm-2 listing-main-info"> 
                <h4><?php echo __($property['PropertyPostType']['name']); ?></h4>
                <p><?php echo __($property['User']['firstname']); ?></p>
                <p><i class="fa fa-clock-o"></i> <?php echo date("d M Y", strtotime($property['Property']['created'])); ?></p>
                <?php
                echo $this->Html->link('View Detail', array('action' => 'view', $property['Property']['id'], $property['Property']['slug']), array('escape' => false, 'class' => 'btn btn-success')); 
                ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<div class="row">
    <div class="col-md-12">
        <nav>
        <ul class="pagination"> 
            <li>
                <?php
                echo $this->Paginator->prev(' Prev ',array('tag'=>'li'),null, array('class' => 'disabled','aria-label'=>'Previous','tag'=>'li'));
                echo $this->Paginator->numbers(array('separator' => '','tag'=>'li'),null, array('class' => 'disabled','tag'=>'li'));
                echo $this->Paginator->next( ' Next ',array('tag'=>'li'),null, array('class' => 'disabled','tag'=>'li','aria-label'=>'Next'));
                ?>
            </li> 
        </ul>
        </nav>
    </div>
</div>
<?php } else { ?>
    <p>Listing not found</p>
<?php } ?>
</div>
            <div class="col-md-3">
                <h3>
                    <a href="blog-post.html">Blog Post Title</a>
                </h3>
                <p>by <a href="#">Start Bootstrap</a>
                </p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <!-- /.row -->

        <hr> 

        <!-- Pager -->
        <div class="row">
            <ul class="pager">
                <li class="previous"><a href="#">&larr; Older</a>
                </li>
                <li class="next"><a href="#">Newer &rarr;</a>
                </li>
            </ul>
        </div>
        <!-- /.row -->

        <hr> 
    </div>
    <!-- /.container -->

    <?php $this->start('script'); ?>

<?php echo $this->Html->script('/vendors/iCheck/icheck.min.js'); ?>  
<script type="text/javascript"> 
    $(document).ready(function() {  
        $('#state').change(function() { 
            $.ajax({ 
                type: "GET", 
                dataType: 'json',
                data: 'state_id=' + $('#state').val(),
                url: baseUrl + 'properties/ajaxstate', 
                success: function(respond) { 
                    var option = '<option value="">All Area</option>'; 
                    $.each(respond, function(i, item) {
                        console.log(90); 
                        option += '<option value="' + item.id + '">' + item.name + '</option>';
                    }); 
                    $('#cityId').html(option);
                }
            }); 
            return false;
        }); 

    $('#category').change(function() { 
      var category = $('#category').val(); 
      var housing = ['2','3','4','5']; // show square & room 2,3,4,5,8,9,12
      var business = ['8','9','12']; // show only square
      var land = ['6','10','13','15']; // show acre
      if(jQuery.inArray(category, housing) !== -1) { 
        $('#ajaxHousing').show(); 
        $('#ajaxSquare').show(); 
        $('#ajaxLand').hide(); 
        $('#acre').val('');
      } else if(jQuery.inArray(category, business) !== -1) {
        $('#ajaxHousing').hide();
        $('#ajaxSquare').show(); 
        $('#ajaxLand').hide(); 
        $('#bathroom').val('');
        $('#bedroom').val(''); 
        $('#acre').val('');
      } else if(jQuery.inArray(category, land) !== -1) {
        $('#ajaxHousing').hide();
        $('#ajaxSquare').hide(); 
        $('#ajaxLand').show(); 
        $('#bathroom').val('');
        $('#bedroom').val(''); 
        $('#square').val('');
      } else { 
        $('#ajaxHousing').hide();
        $('#ajaxSquare').hide(); 
        $('#ajaxLand').hide(); 
        $('#bathroom').val('');
        $('#bedroom').val(''); 
        $('#acre').val('');
        $('#square').val('');
      } 
      console.log(category);
      console.log(isInArray(category, land));
      console.log(isInArray(category, housing));

      return false;
    });  
    });

    function isInArray(value, array) {
      return array.indexOf(value) > -1;
    }
</script>
<?php $this->end(); ?>

