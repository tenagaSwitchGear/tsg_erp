<!-- Page Content -->
    <div class="container listing-view">

        <div class="row">   
	        <div class="col-sm-12 col-xs-12">
	        	<h2><?php echo __($property['Property']['title']); ?></h2>
	        </div> 
	        <div class="col-sm-12">
		        <div class="breadcrumb">
			      	<div class="col-sm-4 col-xs-6">
			      	<p><i class="fa fa-map-marker"></i> <?php echo __($property['City']['name']); ?>, <?php echo __($property['State']['name']); ?></p>
			      	</div>

			      	<div class="col-sm-4 col-xs-6">
			      	<p><i class="fa fa-building"></i> <?php echo __($property['PropertyCategory']['name']); ?> - <?php echo __($property['PropertyPostType']['name']); ?></p>
			      	</div>

					<div class="col-sm-2 col-xs-6">
			      	<p><i class="fa fa-user"></i> <?php echo __($property['User']['firstname']); ?></p>
			      	</div>

			      	<div class="col-sm-2 col-xs-6">
			      	<p><i class="fa fa-clock-o"></i> <?php echo __($property['Property']['created']); ?></p>
			      	</div>
		      	</div>
	      	</div>
	      	

	        <div class="col-md-7 col-sm-7 col-xs-12">
	            <div class="product-image" id="displayImage"> 
		            <?php echo $this->Html->image('Users/Original/'.$property['PropertyImage'][0]['name'], array('class' => 'img-responsive')); ?> 
	            </div>
	            <div class="product_gallery">
	            <?php foreach ($property['PropertyImage'] as $image) { ?> 
	            	<?php $gallery = $image['name']; ?>
		            <a onclick="galleryImage('<?php echo $gallery; ?>');">
		            	<?php echo $this->Html->image('Users/Original/'.$image['name'], array('class' => 'img-responsive')); ?>
		            </a>
                <?php } ?>
	             
	            </div>
	        </div>

	        <div class="col-md-5 col-sm-5 col-xs-12">
	        <div class="product-features" style="border:1px solid #e5e5e5;">
	          <h2 class="prod_title"><?php echo __($property['Property']['title']); ?></h2> 
	          <br />  
	          <div class="product_price">
	              <h3>RM<?php echo __(number_format($property['Property']['price'])); ?></h3> 
	              <br>
	            </div>
	          <p><span>Occupancy:</span> <?php echo __($property['PropertyCurrentOccupancy']['name']); ?></p>
	          <p><span>Land Title:</span> <?php echo __($property['PropertyLandTitle']['name']); ?></p>
	          <?php if(!empty($property['PropertyUnitType']['name'])) { ?> 
	          <p><span>Unit:</span> <?php echo __($property['PropertyUnitType']['name']); ?></p>
	          <?php } ?>
	          <p><span>Title:</span> <?php echo __($property['PropertyTitle']['name']); ?></p>
	          <p><span>Tenure:</span> <?php echo __($property['PropertyTenureType']['name']); ?></p> 
	          <p><span>Furnishing:</span> <?php echo __($property['PropertyFurnishingType']['name']); ?></p>

	          <p><span>Size:</span> <?php echo __(number_format($property['Property']['size'])); ?> <?php echo __($property['PropertyCategory']['size']); ?></p> 

	          <?php if(!empty($property['Property']['bedroom'])) { ?> 
	          <p><span>Bedroom:</span> <?php echo __($property['Property']['bedroom']); ?></p>
	          <p><span>Bathroom:</span> <?php echo __($property['Property']['bathroom']); ?></p>
	          <p><span>Furnishing:</span> <?php echo __($property['PropertyFurnishingType']['name']); ?></p> 
	          <?php } ?>

 
	          
				<br /> 
 
		            <h2 class="prod_title">Contact Agent</h2> 
		            <p><?php echo $this->Html->image('Users/Small/'.$property['User']['image'], array('class' => 'img-responsive', 'style' => 'float: left; margin-right: 5px; width: 80px;')); ?>
			     	<?php echo __($property['User']['firstname']); ?><br/> 
			     	<?php echo __($property['User']['mobile_number']); ?></p>
					
					 
			       	<div class="">
			            <button type="button" class="btn btn-primary">Message Agent</button>
			             
		            </div>
		         
	          </div>
	        </div> 


    <div class="col-md-7 col-sm-7 col-xs-12">  
        <div class="panel">
	         	<?php if($property['PropertyFacilitySelected']) { ?>
				<div class="facility">
				<h2>Facilities</h2>
				<ul class="list-inline prod_size">
					<?php foreach($facilities as $facility) { ?> 
						<li>
							<button type="button" class="btn btn-default btn-xs"><?php echo $facility['PropertyFacility']['name']; ?></button>
						</li>
				    <?php } ?>   
				</ul>
				</div>
	          
	          <?php } ?>
	            <p class="wrapper"><?php echo htmlspecialchars($property['Property']['description']); ?></p>
	       
        </div>
    </div>  

    <div class="col-md-5 col-sm-5 col-xs-12">  
        <div class="panel">
	        <p>Related </p>
        </div>
    </div> 

	</div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
	function galleryImage(name) {
		var image = baseUrl + 'img/Users/Original/' + name;
		$('#displayImage').html('<img src="' + image + '" class="img-responsive">');
		return false;
	}
</script>
<?php $this->end(); ?>