<div class="saleBomChildren form">
<?php echo $this->Form->create('SaleBomChild'); ?>
	<fieldset>
		<legend><?php echo __('Add Sale Bom Child'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('sale_bom_id');
		echo $this->Form->input('parent_id');
		echo $this->Form->input('bom_parent');
		echo $this->Form->input('no_of_order');
		echo $this->Form->input('margin');
		echo $this->Form->input('margin_unit');
		echo $this->Form->input('sale_quotation_id');
		echo $this->Form->input('inventory_item_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sale Bom Children'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Bom Items'), array('controller' => 'sale_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('controller' => 'sale_bom_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
