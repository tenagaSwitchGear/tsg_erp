<?php echo $this->Html->link('Back To Quotation', array('controller' => 'costings', 'action' => 'view', $child['SaleBomChild']['sale_quotation_id']), array('class' => 'btn btn-primary')); ?>

 
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Sub Assembly</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        <?php echo $this->Session->flash(); ?> 

<div class="saleBomChildren form">
<?php echo $this->Form->create('SaleBomChild'); ?> 
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('sale_bom_id');   
		echo $this->Form->input('sale_quotation_id');
		echo $this->Form->input('inventory_item_id');
	?> 
<?php echo $this->Form->end(__('Submit')); ?>
</div>
 
</div>
</div>
</div>
</div>
