<div class="saleBomChildren view">
<h2><?php echo __('Sale Bom Child'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Bom'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleBomChild['SaleBom']['name'], array('controller' => 'sale_boms', 'action' => 'view', $saleBomChild['SaleBom']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Id'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['parent_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bom Parent'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['bom_parent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Order'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['no_of_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Margin'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['margin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Margin Unit'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['margin_unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Quotation Id'); ?></dt>
		<dd>
			<?php echo h($saleBomChild['SaleBomChild']['sale_quotation_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleBomChild['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $saleBomChild['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Bom Child'), array('action' => 'edit', $saleBomChild['SaleBomChild']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Bom Child'), array('action' => 'delete', $saleBomChild['SaleBomChild']['id']), array(), __('Are you sure you want to delete # %s?', $saleBomChild['SaleBomChild']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Bom Children'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom Child'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Bom Items'), array('controller' => 'sale_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('controller' => 'sale_bom_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Bom Items'); ?></h3>
	<?php if (!empty($saleBomChild['SaleBomItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th><?php echo __('Sale Bom Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Sale Bom Child Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Quantity Total'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Price Total'); ?></th>
		<th><?php echo __('Margin'); ?></th>
		<th><?php echo __('No Of Order'); ?></th>
		<th><?php echo __('Margin Unit'); ?></th>
		<th><?php echo __('Sale Quotation Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($saleBomChild['SaleBomItem'] as $saleBomItem): ?>
		<tr>
			<td><?php echo $saleBomItem['id']; ?></td>
			<td><?php echo $saleBomItem['bom_id']; ?></td>
			<td><?php echo $saleBomItem['sale_bom_id']; ?></td>
			<td><?php echo $saleBomItem['inventory_item_id']; ?></td>
			<td><?php echo $saleBomItem['sale_bom_child_id']; ?></td>
			<td><?php echo $saleBomItem['quantity']; ?></td>
			<td><?php echo $saleBomItem['quantity_total']; ?></td>
			<td><?php echo $saleBomItem['general_unit_id']; ?></td>
			<td><?php echo $saleBomItem['price']; ?></td>
			<td><?php echo $saleBomItem['price_total']; ?></td>
			<td><?php echo $saleBomItem['margin']; ?></td>
			<td><?php echo $saleBomItem['no_of_order']; ?></td>
			<td><?php echo $saleBomItem['margin_unit']; ?></td>
			<td><?php echo $saleBomItem['sale_quotation_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_bom_items', 'action' => 'view', $saleBomItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_bom_items', 'action' => 'edit', $saleBomItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_bom_items', 'action' => 'delete', $saleBomItem['id']), array(), __('Are you sure you want to delete # %s?', $saleBomItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('controller' => 'sale_bom_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
