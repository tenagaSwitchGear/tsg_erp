<div class="saleBomChildren index">
	<h2><?php echo __('Sale Bom Children'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_bom_id'); ?></th>
			<th><?php echo $this->Paginator->sort('parent_id'); ?></th>
			<th><?php echo $this->Paginator->sort('bom_parent'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_order'); ?></th>
			<th><?php echo $this->Paginator->sort('margin'); ?></th>
			<th><?php echo $this->Paginator->sort('margin_unit'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_quotation_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleBomChildren as $saleBomChild): ?>
	<tr>
		<td><?php echo h($saleBomChild['SaleBomChild']['id']); ?>&nbsp;</td>
		<td><?php echo h($saleBomChild['SaleBomChild']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleBomChild['SaleBom']['name'], array('controller' => 'sale_boms', 'action' => 'view', $saleBomChild['SaleBom']['id'])); ?>
		</td>
		<td><?php echo h($saleBomChild['SaleBomChild']['parent_id']); ?>&nbsp;</td>
		<td><?php echo h($saleBomChild['SaleBomChild']['bom_parent']); ?>&nbsp;</td>
		<td><?php echo h($saleBomChild['SaleBomChild']['no_of_order']); ?>&nbsp;</td>
		<td><?php echo h($saleBomChild['SaleBomChild']['margin']); ?>&nbsp;</td>
		<td><?php echo h($saleBomChild['SaleBomChild']['margin_unit']); ?>&nbsp;</td>
		<td><?php echo h($saleBomChild['SaleBomChild']['sale_quotation_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleBomChild['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $saleBomChild['InventoryItem']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleBomChild['SaleBomChild']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleBomChild['SaleBomChild']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleBomChild['SaleBomChild']['id']), array(), __('Are you sure you want to delete # %s?', $saleBomChild['SaleBomChild']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sale Bom Child'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Bom Items'), array('controller' => 'sale_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('controller' => 'sale_bom_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
