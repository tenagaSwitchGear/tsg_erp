<div class="generalUnits form">
<?php echo $this->Form->create('GeneralUnit'); ?>
	<fieldset>
		<legend><?php echo __('Add General Unit'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('physical_quantity');
		echo $this->Form->input('rounding_factor');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List General Units'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Bom Items'), array('controller' => 'bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom Item'), array('controller' => 'bom_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Order Items'), array('controller' => 'inventory_delivery_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('controller' => 'inventory_delivery_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Issueds'), array('controller' => 'inventory_material_issueds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Issued'), array('controller' => 'inventory_material_issueds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Order Items'), array('controller' => 'inventory_purchase_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order Item'), array('controller' => 'inventory_purchase_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisition Items'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Item'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Qc Items'), array('controller' => 'inventory_qc_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Qc Item'), array('controller' => 'inventory_qc_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stock Rejecteds'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock Rejected'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Items'), array('controller' => 'inventory_supplier_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Item'), array('controller' => 'inventory_supplier_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('controller' => 'planning_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Order Items'), array('controller' => 'production_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order Item'), array('controller' => 'production_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Items'), array('controller' => 'project_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Boms'), array('controller' => 'project_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Bom Items'), array('controller' => 'sale_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('controller' => 'sale_bom_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Job Items'), array('controller' => 'sale_job_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Item'), array('controller' => 'sale_job_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Item Costs'), array('controller' => 'sale_quotation_item_costs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item Cost'), array('controller' => 'sale_quotation_item_costs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stock Temps'), array('controller' => 'stock_temps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stock Temp'), array('controller' => 'stock_temps', 'action' => 'add')); ?> </li>
	</ul>
</div>
