<div class="generalUnits view">
<h2><?php echo __('General Unit'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($generalUnit['GeneralUnit']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($generalUnit['GeneralUnit']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($generalUnit['GeneralUnit']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Physical Quantity'); ?></dt>
		<dd>
			<?php echo h($generalUnit['GeneralUnit']['physical_quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rounding Factor'); ?></dt>
		<dd>
			<?php echo h($generalUnit['GeneralUnit']['rounding_factor']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit General Unit'), array('action' => 'edit', $generalUnit['GeneralUnit']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete General Unit'), array('action' => 'delete', $generalUnit['GeneralUnit']['id']), array(), __('Are you sure you want to delete # %s?', $generalUnit['GeneralUnit']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bom Items'), array('controller' => 'bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom Item'), array('controller' => 'bom_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Order Items'), array('controller' => 'inventory_delivery_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('controller' => 'inventory_delivery_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Issueds'), array('controller' => 'inventory_material_issueds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Issued'), array('controller' => 'inventory_material_issueds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Order Items'), array('controller' => 'inventory_purchase_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order Item'), array('controller' => 'inventory_purchase_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisition Items'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Item'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Qc Items'), array('controller' => 'inventory_qc_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Qc Item'), array('controller' => 'inventory_qc_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stock Rejecteds'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock Rejected'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Items'), array('controller' => 'inventory_supplier_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Item'), array('controller' => 'inventory_supplier_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('controller' => 'planning_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Order Items'), array('controller' => 'production_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order Item'), array('controller' => 'production_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Items'), array('controller' => 'project_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Boms'), array('controller' => 'project_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Bom Items'), array('controller' => 'sale_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('controller' => 'sale_bom_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Job Items'), array('controller' => 'sale_job_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Item'), array('controller' => 'sale_job_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Item Costs'), array('controller' => 'sale_quotation_item_costs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item Cost'), array('controller' => 'sale_quotation_item_costs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stock Temps'), array('controller' => 'stock_temps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stock Temp'), array('controller' => 'stock_temps', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Bom Items'); ?></h3>
	<?php if (!empty($generalUnit['BomItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Bom Child Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['BomItem'] as $bomItem): ?>
		<tr>
			<td><?php echo $bomItem['id']; ?></td>
			<td><?php echo $bomItem['inventory_item_id']; ?></td>
			<td><?php echo $bomItem['bom_child_id']; ?></td>
			<td><?php echo $bomItem['quantity']; ?></td>
			<td><?php echo $bomItem['general_unit_id']; ?></td>
			<td><?php echo $bomItem['bom_id']; ?></td>
			<td><?php echo $bomItem['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'bom_items', 'action' => 'view', $bomItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'bom_items', 'action' => 'edit', $bomItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'bom_items', 'action' => 'delete', $bomItem['id']), array(), __('Are you sure you want to delete # %s?', $bomItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bom Item'), array('controller' => 'bom_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Delivery Order Items'); ?></h3>
	<?php if (!empty($generalUnit['InventoryDeliveryOrderItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Delivery Order Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Quantity Delivered'); ?></th>
		<th><?php echo __('Quantity Remaining'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryDeliveryOrderItem'] as $inventoryDeliveryOrderItem): ?>
		<tr>
			<td><?php echo $inventoryDeliveryOrderItem['id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_delivery_order_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['quantity']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['remark']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_item_category_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['price_per_unit']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['amount']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['discount']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['quantity_delivered']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['quantity_remaining']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_supplier_item_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_delivery_order_items', 'action' => 'view', $inventoryDeliveryOrderItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_delivery_order_items', 'action' => 'edit', $inventoryDeliveryOrderItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_delivery_order_items', 'action' => 'delete', $inventoryDeliveryOrderItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryDeliveryOrderItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('controller' => 'inventory_delivery_order_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Items'); ?></h3>
	<?php if (!empty($generalUnit['InventoryItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Unit Price'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Qc Inspect'); ?></th>
		<th><?php echo __('Quantity Reserved'); ?></th>
		<th><?php echo __('Quantity Available'); ?></th>
		<th><?php echo __('Quantity Shortage'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('General Currency Id'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Inventory Location Id'); ?></th>
		<th><?php echo __('Inventory Delivery Location Id'); ?></th>
		<th><?php echo __('Inventory Store Id'); ?></th>
		<th><?php echo __('Inventory Rack Id'); ?></th>
		<th><?php echo __('Rack'); ?></th>
		<th><?php echo __('Search 1'); ?></th>
		<th><?php echo __('Search 2'); ?></th>
		<th><?php echo __('Search 3'); ?></th>
		<th><?php echo __('Search 4'); ?></th>
		<th><?php echo __('Search 5'); ?></th>
		<th><?php echo __('Is Bom'); ?></th>
		<th><?php echo __('Attachment'); ?></th>
		<th><?php echo __('Attachment Dir'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Assembly Time'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryItem'] as $inventoryItem): ?>
		<tr>
			<td><?php echo $inventoryItem['id']; ?></td>
			<td><?php echo $inventoryItem['name']; ?></td>
			<td><?php echo $inventoryItem['quantity']; ?></td>
			<td><?php echo $inventoryItem['unit_price']; ?></td>
			<td><?php echo $inventoryItem['inventory_item_category_id']; ?></td>
			<td><?php echo $inventoryItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryItem['code']; ?></td>
			<td><?php echo $inventoryItem['qc_inspect']; ?></td>
			<td><?php echo $inventoryItem['quantity_reserved']; ?></td>
			<td><?php echo $inventoryItem['quantity_available']; ?></td>
			<td><?php echo $inventoryItem['quantity_shortage']; ?></td>
			<td><?php echo $inventoryItem['status']; ?></td>
			<td><?php echo $inventoryItem['general_currency_id']; ?></td>
			<td><?php echo $inventoryItem['note']; ?></td>
			<td><?php echo $inventoryItem['inventory_location_id']; ?></td>
			<td><?php echo $inventoryItem['inventory_delivery_location_id']; ?></td>
			<td><?php echo $inventoryItem['inventory_store_id']; ?></td>
			<td><?php echo $inventoryItem['inventory_rack_id']; ?></td>
			<td><?php echo $inventoryItem['rack']; ?></td>
			<td><?php echo $inventoryItem['search_1']; ?></td>
			<td><?php echo $inventoryItem['search_2']; ?></td>
			<td><?php echo $inventoryItem['search_3']; ?></td>
			<td><?php echo $inventoryItem['search_4']; ?></td>
			<td><?php echo $inventoryItem['search_5']; ?></td>
			<td><?php echo $inventoryItem['is_bom']; ?></td>
			<td><?php echo $inventoryItem['attachment']; ?></td>
			<td><?php echo $inventoryItem['attachment_dir']; ?></td>
			<td><?php echo $inventoryItem['created']; ?></td>
			<td><?php echo $inventoryItem['modified']; ?></td>
			<td><?php echo $inventoryItem['user_id']; ?></td>
			<td><?php echo $inventoryItem['assembly_time']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_items', 'action' => 'view', $inventoryItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_items', 'action' => 'edit', $inventoryItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_items', 'action' => 'delete', $inventoryItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Material Issueds'); ?></h3>
	<?php if (!empty($generalUnit['InventoryMaterialIssued'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Material Request Item Id'); ?></th>
		<th><?php echo __('Inventory Stock Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Production Order Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryMaterialIssued'] as $inventoryMaterialIssued): ?>
		<tr>
			<td><?php echo $inventoryMaterialIssued['id']; ?></td>
			<td><?php echo $inventoryMaterialIssued['inventory_material_request_item_id']; ?></td>
			<td><?php echo $inventoryMaterialIssued['inventory_stock_id']; ?></td>
			<td><?php echo $inventoryMaterialIssued['inventory_item_id']; ?></td>
			<td><?php echo $inventoryMaterialIssued['production_order_id']; ?></td>
			<td><?php echo $inventoryMaterialIssued['quantity']; ?></td>
			<td><?php echo $inventoryMaterialIssued['general_unit_id']; ?></td>
			<td><?php echo $inventoryMaterialIssued['created']; ?></td>
			<td><?php echo $inventoryMaterialIssued['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_material_issueds', 'action' => 'view', $inventoryMaterialIssued['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_material_issueds', 'action' => 'edit', $inventoryMaterialIssued['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_material_issueds', 'action' => 'delete', $inventoryMaterialIssued['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialIssued['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Material Issued'), array('controller' => 'inventory_material_issueds', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Material Request Items'); ?></h3>
	<?php if (!empty($generalUnit['InventoryMaterialRequestItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Material Request Id'); ?></th>
		<th><?php echo __('Production Order Item Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Issued Quantity'); ?></th>
		<th><?php echo __('Issued Balance'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryMaterialRequestItem'] as $inventoryMaterialRequestItem): ?>
		<tr>
			<td><?php echo $inventoryMaterialRequestItem['id']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['inventory_material_request_id']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['production_order_item_id']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['quantity']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['issued_quantity']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['issued_balance']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['created']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['modified']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['note']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['status']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['type']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_material_request_items', 'action' => 'view', $inventoryMaterialRequestItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_material_request_items', 'action' => 'edit', $inventoryMaterialRequestItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_material_request_items', 'action' => 'delete', $inventoryMaterialRequestItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialRequestItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Purchase Order Items'); ?></h3>
	<?php if (!empty($generalUnit['InventoryPurchaseOrderItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Purchase Order Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Discount Type'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryPurchaseOrderItem'] as $inventoryPurchaseOrderItem): ?>
		<tr>
			<td><?php echo $inventoryPurchaseOrderItem['id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_purchase_order_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['quantity']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_supplier_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_item_category_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['price_per_unit']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['amount']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['discount']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['discount_type']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['tax']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_purchase_order_items', 'action' => 'view', $inventoryPurchaseOrderItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_purchase_order_items', 'action' => 'edit', $inventoryPurchaseOrderItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_purchase_order_items', 'action' => 'delete', $inventoryPurchaseOrderItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryPurchaseOrderItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Purchase Order Item'), array('controller' => 'inventory_purchase_order_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Purchase Requisition Items'); ?></h3>
	<?php if (!empty($generalUnit['InventoryPurchaseRequisitionItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Purchase Requisition Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Discount Type'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryPurchaseRequisitionItem'] as $inventoryPurchaseRequisitionItem): ?>
		<tr>
			<td><?php echo $inventoryPurchaseRequisitionItem['id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_purchase_requisition_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['quantity']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_supplier_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_item_category_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['price_per_unit']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['amount']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['discount']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['discount_type']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['tax']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'view', $inventoryPurchaseRequisitionItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'edit', $inventoryPurchaseRequisitionItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'delete', $inventoryPurchaseRequisitionItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryPurchaseRequisitionItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Item'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Qc Items'); ?></h3>
	<?php if (!empty($generalUnit['InventoryQcItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Do No'); ?></th>
		<th><?php echo __('Inventory Delivery Order Item Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Quantity Pass'); ?></th>
		<th><?php echo __('Quantity Rejected'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryQcItem'] as $inventoryQcItem): ?>
		<tr>
			<td><?php echo $inventoryQcItem['id']; ?></td>
			<td><?php echo $inventoryQcItem['do_no']; ?></td>
			<td><?php echo $inventoryQcItem['inventory_delivery_order_item_id']; ?></td>
			<td><?php echo $inventoryQcItem['name']; ?></td>
			<td><?php echo $inventoryQcItem['created']; ?></td>
			<td><?php echo $inventoryQcItem['modified']; ?></td>
			<td><?php echo $inventoryQcItem['quantity']; ?></td>
			<td><?php echo $inventoryQcItem['quantity_pass']; ?></td>
			<td><?php echo $inventoryQcItem['quantity_rejected']; ?></td>
			<td><?php echo $inventoryQcItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryQcItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryQcItem['status']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_qc_items', 'action' => 'view', $inventoryQcItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_qc_items', 'action' => 'edit', $inventoryQcItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_qc_items', 'action' => 'delete', $inventoryQcItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryQcItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Qc Item'), array('controller' => 'inventory_qc_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Stock Rejecteds'); ?></h3>
	<?php if (!empty($generalUnit['InventoryStockRejected'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Delivery Order Item Id'); ?></th>
		<th><?php echo __('General Unit Converter Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Inventory Qc Item Id'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Finished Good Comment Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryStockRejected'] as $inventoryStockRejected): ?>
		<tr>
			<td><?php echo $inventoryStockRejected['id']; ?></td>
			<td><?php echo $inventoryStockRejected['general_unit_id']; ?></td>
			<td><?php echo $inventoryStockRejected['inventory_item_id']; ?></td>
			<td><?php echo $inventoryStockRejected['inventory_delivery_order_item_id']; ?></td>
			<td><?php echo $inventoryStockRejected['general_unit_converter_id']; ?></td>
			<td><?php echo $inventoryStockRejected['quantity']; ?></td>
			<td><?php echo $inventoryStockRejected['status']; ?></td>
			<td><?php echo $inventoryStockRejected['inventory_qc_item_id']; ?></td>
			<td><?php echo $inventoryStockRejected['remark']; ?></td>
			<td><?php echo $inventoryStockRejected['finished_good_comment_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'view', $inventoryStockRejected['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'edit', $inventoryStockRejected['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'delete', $inventoryStockRejected['id']), array(), __('Are you sure you want to delete # %s?', $inventoryStockRejected['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Stock Rejected'), array('controller' => 'inventory_stock_rejecteds', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Stocks'); ?></h3>
	<?php if (!empty($generalUnit['InventoryStock'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Delivery Order Item Id'); ?></th>
		<th><?php echo __('General Unit Converter Id'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Issued Quantity'); ?></th>
		<th><?php echo __('Qc Status'); ?></th>
		<th><?php echo __('Receive Date'); ?></th>
		<th><?php echo __('Expiry Date'); ?></th>
		<th><?php echo __('Inventory Location Id'); ?></th>
		<th><?php echo __('Inventory Store Id'); ?></th>
		<th><?php echo __('Inventory Rack Id'); ?></th>
		<th><?php echo __('Inventory Delivery Location Id'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Finished Good Id'); ?></th>
		<th><?php echo __('Rack'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventoryStock'] as $inventoryStock): ?>
		<tr>
			<td><?php echo $inventoryStock['id']; ?></td>
			<td><?php echo $inventoryStock['general_unit_id']; ?></td>
			<td><?php echo $inventoryStock['inventory_item_id']; ?></td>
			<td><?php echo $inventoryStock['inventory_delivery_order_item_id']; ?></td>
			<td><?php echo $inventoryStock['general_unit_converter_id']; ?></td>
			<td><?php echo $inventoryStock['price_per_unit']; ?></td>
			<td><?php echo $inventoryStock['quantity']; ?></td>
			<td><?php echo $inventoryStock['issued_quantity']; ?></td>
			<td><?php echo $inventoryStock['qc_status']; ?></td>
			<td><?php echo $inventoryStock['receive_date']; ?></td>
			<td><?php echo $inventoryStock['expiry_date']; ?></td>
			<td><?php echo $inventoryStock['inventory_location_id']; ?></td>
			<td><?php echo $inventoryStock['inventory_store_id']; ?></td>
			<td><?php echo $inventoryStock['inventory_rack_id']; ?></td>
			<td><?php echo $inventoryStock['inventory_delivery_location_id']; ?></td>
			<td><?php echo $inventoryStock['type']; ?></td>
			<td><?php echo $inventoryStock['finished_good_id']; ?></td>
			<td><?php echo $inventoryStock['rack']; ?></td>
			<td><?php echo $inventoryStock['created']; ?></td>
			<td><?php echo $inventoryStock['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_stocks', 'action' => 'view', $inventoryStock['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_stocks', 'action' => 'edit', $inventoryStock['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_stocks', 'action' => 'delete', $inventoryStock['id']), array(), __('Are you sure you want to delete # %s?', $inventoryStock['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Supplier Items'); ?></h3>
	<?php if (!empty($generalUnit['InventorySupplierItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Conversion Unit Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Effective Date'); ?></th>
		<th><?php echo __('Expiry Date'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('General Currency Id'); ?></th>
		<th><?php echo __('Is Default'); ?></th>
		<th><?php echo __('Qc Inspect'); ?></th>
		<th><?php echo __('Min Order'); ?></th>
		<th><?php echo __('Min Order Unit'); ?></th>
		<th><?php echo __('Conversion Qty'); ?></th>
		<th><?php echo __('Part Number'); ?></th>
		<th><?php echo __('Lead Time'); ?></th>
		<th><?php echo __('Quotation Number'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['InventorySupplierItem'] as $inventorySupplierItem): ?>
		<tr>
			<td><?php echo $inventorySupplierItem['id']; ?></td>
			<td><?php echo $inventorySupplierItem['inventory_item_id']; ?></td>
			<td><?php echo $inventorySupplierItem['price']; ?></td>
			<td><?php echo $inventorySupplierItem['price_per_unit']; ?></td>
			<td><?php echo $inventorySupplierItem['general_unit_id']; ?></td>
			<td><?php echo $inventorySupplierItem['conversion_unit_id']; ?></td>
			<td><?php echo $inventorySupplierItem['created']; ?></td>
			<td><?php echo $inventorySupplierItem['modified']; ?></td>
			<td><?php echo $inventorySupplierItem['effective_date']; ?></td>
			<td><?php echo $inventorySupplierItem['expiry_date']; ?></td>
			<td><?php echo $inventorySupplierItem['remark']; ?></td>
			<td><?php echo $inventorySupplierItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventorySupplierItem['general_currency_id']; ?></td>
			<td><?php echo $inventorySupplierItem['is_default']; ?></td>
			<td><?php echo $inventorySupplierItem['qc_inspect']; ?></td>
			<td><?php echo $inventorySupplierItem['min_order']; ?></td>
			<td><?php echo $inventorySupplierItem['min_order_unit']; ?></td>
			<td><?php echo $inventorySupplierItem['conversion_qty']; ?></td>
			<td><?php echo $inventorySupplierItem['part_number']; ?></td>
			<td><?php echo $inventorySupplierItem['lead_time']; ?></td>
			<td><?php echo $inventorySupplierItem['quotation_number']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_supplier_items', 'action' => 'view', $inventorySupplierItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_supplier_items', 'action' => 'edit', $inventorySupplierItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_supplier_items', 'action' => 'delete', $inventorySupplierItem['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Supplier Item'), array('controller' => 'inventory_supplier_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Planning Items'); ?></h3>
	<?php if (!empty($generalUnit['PlanningItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Planning Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Buffer'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('Planning Qty'); ?></th>
		<th><?php echo __('Planning General Unit'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th><?php echo __('Purchased Quantity'); ?></th>
		<th><?php echo __('Balance'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['PlanningItem'] as $planningItem): ?>
		<tr>
			<td><?php echo $planningItem['id']; ?></td>
			<td><?php echo $planningItem['planning_id']; ?></td>
			<td><?php echo $planningItem['inventory_item_id']; ?></td>
			<td><?php echo $planningItem['quantity']; ?></td>
			<td><?php echo $planningItem['general_unit_id']; ?></td>
			<td><?php echo $planningItem['buffer']; ?></td>
			<td><?php echo $planningItem['total']; ?></td>
			<td><?php echo $planningItem['planning_qty']; ?></td>
			<td><?php echo $planningItem['planning_general_unit']; ?></td>
			<td><?php echo $planningItem['inventory_supplier_id']; ?></td>
			<td><?php echo $planningItem['created']; ?></td>
			<td><?php echo $planningItem['modified']; ?></td>
			<td><?php echo $planningItem['remark']; ?></td>
			<td><?php echo $planningItem['status']; ?></td>
			<td><?php echo $planningItem['inventory_supplier_item_id']; ?></td>
			<td><?php echo $planningItem['purchased_quantity']; ?></td>
			<td><?php echo $planningItem['balance']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'planning_items', 'action' => 'view', $planningItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'planning_items', 'action' => 'edit', $planningItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'planning_items', 'action' => 'delete', $planningItem['id']), array(), __('Are you sure you want to delete # %s?', $planningItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Production Order Items'); ?></h3>
	<?php if (!empty($generalUnit['ProductionOrderItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Production Order Id'); ?></th>
		<th><?php echo __('Production Order Child Id'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Quantity Total'); ?></th>
		<th><?php echo __('Quantity Issued'); ?></th>
		<th><?php echo __('Quantity Bal'); ?></th>
		<th><?php echo __('Quantity Req'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Price Total'); ?></th>
		<th><?php echo __('Margin'); ?></th>
		<th><?php echo __('No Of Order'); ?></th>
		<th><?php echo __('Margin Unit'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['ProductionOrderItem'] as $productionOrderItem): ?>
		<tr>
			<td><?php echo $productionOrderItem['id']; ?></td>
			<td><?php echo $productionOrderItem['production_order_id']; ?></td>
			<td><?php echo $productionOrderItem['production_order_child_id']; ?></td>
			<td><?php echo $productionOrderItem['bom_id']; ?></td>
			<td><?php echo $productionOrderItem['inventory_item_id']; ?></td>
			<td><?php echo $productionOrderItem['quantity']; ?></td>
			<td><?php echo $productionOrderItem['quantity_total']; ?></td>
			<td><?php echo $productionOrderItem['quantity_issued']; ?></td>
			<td><?php echo $productionOrderItem['quantity_bal']; ?></td>
			<td><?php echo $productionOrderItem['quantity_req']; ?></td>
			<td><?php echo $productionOrderItem['general_unit_id']; ?></td>
			<td><?php echo $productionOrderItem['price']; ?></td>
			<td><?php echo $productionOrderItem['price_total']; ?></td>
			<td><?php echo $productionOrderItem['margin']; ?></td>
			<td><?php echo $productionOrderItem['no_of_order']; ?></td>
			<td><?php echo $productionOrderItem['margin_unit']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'production_order_items', 'action' => 'view', $productionOrderItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'production_order_items', 'action' => 'edit', $productionOrderItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'production_order_items', 'action' => 'delete', $productionOrderItem['id']), array(), __('Are you sure you want to delete # %s?', $productionOrderItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Production Order Item'), array('controller' => 'production_order_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Project Bom Items'); ?></h3>
	<?php if (!empty($generalUnit['ProjectBomItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Job Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Project Bom Child Id'); ?></th>
		<th><?php echo __('Project Bom Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Quantity Total'); ?></th>
		<th><?php echo __('Quantity Shortage'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Price Total'); ?></th>
		<th><?php echo __('No Of Order'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['ProjectBomItem'] as $projectBomItem): ?>
		<tr>
			<td><?php echo $projectBomItem['id']; ?></td>
			<td><?php echo $projectBomItem['sale_job_id']; ?></td>
			<td><?php echo $projectBomItem['inventory_item_id']; ?></td>
			<td><?php echo $projectBomItem['general_unit_id']; ?></td>
			<td><?php echo $projectBomItem['project_bom_child_id']; ?></td>
			<td><?php echo $projectBomItem['project_bom_id']; ?></td>
			<td><?php echo $projectBomItem['quantity']; ?></td>
			<td><?php echo $projectBomItem['quantity_total']; ?></td>
			<td><?php echo $projectBomItem['quantity_shortage']; ?></td>
			<td><?php echo $projectBomItem['price']; ?></td>
			<td><?php echo $projectBomItem['price_total']; ?></td>
			<td><?php echo $projectBomItem['no_of_order']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'project_bom_items', 'action' => 'view', $projectBomItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'project_bom_items', 'action' => 'edit', $projectBomItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'project_bom_items', 'action' => 'delete', $projectBomItem['id']), array(), __('Are you sure you want to delete # %s?', $projectBomItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Project Boms'); ?></h3>
	<?php if (!empty($generalUnit['ProjectBom'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Unit Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th><?php echo __('Total Price'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Sale Tender Id'); ?></th>
		<th><?php echo __('Sale Job Id'); ?></th>
		<th><?php echo __('Sale Job Child Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['ProjectBom'] as $projectBom): ?>
		<tr>
			<td><?php echo $projectBom['id']; ?></td>
			<td><?php echo $projectBom['quantity']; ?></td>
			<td><?php echo $projectBom['general_unit_id']; ?></td>
			<td><?php echo $projectBom['unit_price']; ?></td>
			<td><?php echo $projectBom['discount']; ?></td>
			<td><?php echo $projectBom['tax']; ?></td>
			<td><?php echo $projectBom['total_price']; ?></td>
			<td><?php echo $projectBom['bom_id']; ?></td>
			<td><?php echo $projectBom['name']; ?></td>
			<td><?php echo $projectBom['code']; ?></td>
			<td><?php echo $projectBom['remark']; ?></td>
			<td><?php echo $projectBom['sale_tender_id']; ?></td>
			<td><?php echo $projectBom['sale_job_id']; ?></td>
			<td><?php echo $projectBom['sale_job_child_id']; ?></td>
			<td><?php echo $projectBom['created']; ?></td>
			<td><?php echo $projectBom['modified']; ?></td>
			<td><?php echo $projectBom['user_id']; ?></td>
			<td><?php echo $projectBom['status']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'project_boms', 'action' => 'view', $projectBom['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'project_boms', 'action' => 'edit', $projectBom['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'project_boms', 'action' => 'delete', $projectBom['id']), array(), __('Are you sure you want to delete # %s?', $projectBom['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Bom Items'); ?></h3>
	<?php if (!empty($generalUnit['SaleBomItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th><?php echo __('Sale Bom Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Sale Bom Child Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Quantity Total'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Price Total'); ?></th>
		<th><?php echo __('Margin'); ?></th>
		<th><?php echo __('No Of Order'); ?></th>
		<th><?php echo __('Margin Unit'); ?></th>
		<th><?php echo __('Sale Quotation Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['SaleBomItem'] as $saleBomItem): ?>
		<tr>
			<td><?php echo $saleBomItem['id']; ?></td>
			<td><?php echo $saleBomItem['bom_id']; ?></td>
			<td><?php echo $saleBomItem['sale_bom_id']; ?></td>
			<td><?php echo $saleBomItem['inventory_item_id']; ?></td>
			<td><?php echo $saleBomItem['sale_bom_child_id']; ?></td>
			<td><?php echo $saleBomItem['quantity']; ?></td>
			<td><?php echo $saleBomItem['quantity_total']; ?></td>
			<td><?php echo $saleBomItem['general_unit_id']; ?></td>
			<td><?php echo $saleBomItem['price']; ?></td>
			<td><?php echo $saleBomItem['price_total']; ?></td>
			<td><?php echo $saleBomItem['margin']; ?></td>
			<td><?php echo $saleBomItem['no_of_order']; ?></td>
			<td><?php echo $saleBomItem['margin_unit']; ?></td>
			<td><?php echo $saleBomItem['sale_quotation_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_bom_items', 'action' => 'view', $saleBomItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_bom_items', 'action' => 'edit', $saleBomItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_bom_items', 'action' => 'delete', $saleBomItem['id']), array(), __('Are you sure you want to delete # %s?', $saleBomItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('controller' => 'sale_bom_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Boms'); ?></h3>
	<?php if (!empty($generalUnit['SaleBom'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Sale Quotation Item Id'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th><?php echo __('Sale Tender Id'); ?></th>
		<th><?php echo __('Sale Quotation Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Margin'); ?></th>
		<th><?php echo __('Margin Unit'); ?></th>
		<th><?php echo __('No Of Order'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['SaleBom'] as $saleBom): ?>
		<tr>
			<td><?php echo $saleBom['id']; ?></td>
			<td><?php echo $saleBom['inventory_item_id']; ?></td>
			<td><?php echo $saleBom['sale_quotation_item_id']; ?></td>
			<td><?php echo $saleBom['bom_id']; ?></td>
			<td><?php echo $saleBom['sale_tender_id']; ?></td>
			<td><?php echo $saleBom['sale_quotation_id']; ?></td>
			<td><?php echo $saleBom['name']; ?></td>
			<td><?php echo $saleBom['code']; ?></td>
			<td><?php echo $saleBom['description']; ?></td>
			<td><?php echo $saleBom['created']; ?></td>
			<td><?php echo $saleBom['modified']; ?></td>
			<td><?php echo $saleBom['user_id']; ?></td>
			<td><?php echo $saleBom['status']; ?></td>
			<td><?php echo $saleBom['price']; ?></td>
			<td><?php echo $saleBom['margin']; ?></td>
			<td><?php echo $saleBom['margin_unit']; ?></td>
			<td><?php echo $saleBom['no_of_order']; ?></td>
			<td><?php echo $saleBom['general_unit_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_boms', 'action' => 'view', $saleBom['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_boms', 'action' => 'edit', $saleBom['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_boms', 'action' => 'delete', $saleBom['id']), array(), __('Are you sure you want to delete # %s?', $saleBom['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Job Items'); ?></h3>
	<?php if (!empty($generalUnit['SaleJobItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Unit Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th><?php echo __('Total Price'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Sale Tender Id'); ?></th>
		<th><?php echo __('Sale Job Id'); ?></th>
		<th><?php echo __('Sale Job Child Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['SaleJobItem'] as $saleJobItem): ?>
		<tr>
			<td><?php echo $saleJobItem['id']; ?></td>
			<td><?php echo $saleJobItem['quantity']; ?></td>
			<td><?php echo $saleJobItem['general_unit_id']; ?></td>
			<td><?php echo $saleJobItem['unit_price']; ?></td>
			<td><?php echo $saleJobItem['discount']; ?></td>
			<td><?php echo $saleJobItem['tax']; ?></td>
			<td><?php echo $saleJobItem['total_price']; ?></td>
			<td><?php echo $saleJobItem['bom_id']; ?></td>
			<td><?php echo $saleJobItem['inventory_item_id']; ?></td>
			<td><?php echo $saleJobItem['name']; ?></td>
			<td><?php echo $saleJobItem['code']; ?></td>
			<td><?php echo $saleJobItem['remark']; ?></td>
			<td><?php echo $saleJobItem['sale_tender_id']; ?></td>
			<td><?php echo $saleJobItem['sale_job_id']; ?></td>
			<td><?php echo $saleJobItem['sale_job_child_id']; ?></td>
			<td><?php echo $saleJobItem['created']; ?></td>
			<td><?php echo $saleJobItem['modified']; ?></td>
			<td><?php echo $saleJobItem['user_id']; ?></td>
			<td><?php echo $saleJobItem['status']; ?></td>
			<td><?php echo $saleJobItem['type']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_job_items', 'action' => 'view', $saleJobItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_job_items', 'action' => 'edit', $saleJobItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_job_items', 'action' => 'delete', $saleJobItem['id']), array(), __('Are you sure you want to delete # %s?', $saleJobItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Job Item'), array('controller' => 'sale_job_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Order Items'); ?></h3>
	<?php if (!empty($generalUnit['SaleOrderItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Unit Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Discount Type'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th><?php echo __('Total Price'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Sale Order Id'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Sale Job Id'); ?></th>
		<th><?php echo __('Sale Job Child Id'); ?></th>
		<th><?php echo __('Bom Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['SaleOrderItem'] as $saleOrderItem): ?>
		<tr>
			<td><?php echo $saleOrderItem['id']; ?></td>
			<td><?php echo $saleOrderItem['quantity']; ?></td>
			<td><?php echo $saleOrderItem['general_unit_id']; ?></td>
			<td><?php echo $saleOrderItem['unit_price']; ?></td>
			<td><?php echo $saleOrderItem['discount']; ?></td>
			<td><?php echo $saleOrderItem['discount_type']; ?></td>
			<td><?php echo $saleOrderItem['tax']; ?></td>
			<td><?php echo $saleOrderItem['total_price']; ?></td>
			<td><?php echo $saleOrderItem['inventory_item_id']; ?></td>
			<td><?php echo $saleOrderItem['sale_order_id']; ?></td>
			<td><?php echo $saleOrderItem['type']; ?></td>
			<td><?php echo $saleOrderItem['sale_job_id']; ?></td>
			<td><?php echo $saleOrderItem['sale_job_child_id']; ?></td>
			<td><?php echo $saleOrderItem['bom_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_order_items', 'action' => 'view', $saleOrderItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_order_items', 'action' => 'edit', $saleOrderItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_order_items', 'action' => 'delete', $saleOrderItem['id']), array(), __('Are you sure you want to delete # %s?', $saleOrderItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Quotation Item Costs'); ?></h3>
	<?php if (!empty($generalUnit['SaleQuotationItemCost'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Margin'); ?></th>
		<th><?php echo __('Total Price'); ?></th>
		<th><?php echo __('Sale Quotation Item Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['SaleQuotationItemCost'] as $saleQuotationItemCost): ?>
		<tr>
			<td><?php echo $saleQuotationItemCost['id']; ?></td>
			<td><?php echo $saleQuotationItemCost['name']; ?></td>
			<td><?php echo $saleQuotationItemCost['inventory_item_id']; ?></td>
			<td><?php echo $saleQuotationItemCost['quantity']; ?></td>
			<td><?php echo $saleQuotationItemCost['general_unit_id']; ?></td>
			<td><?php echo $saleQuotationItemCost['price']; ?></td>
			<td><?php echo $saleQuotationItemCost['margin']; ?></td>
			<td><?php echo $saleQuotationItemCost['total_price']; ?></td>
			<td><?php echo $saleQuotationItemCost['sale_quotation_item_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_quotation_item_costs', 'action' => 'view', $saleQuotationItemCost['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_quotation_item_costs', 'action' => 'edit', $saleQuotationItemCost['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_quotation_item_costs', 'action' => 'delete', $saleQuotationItemCost['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotationItemCost['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Quotation Item Cost'), array('controller' => 'sale_quotation_item_costs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Quotation Items'); ?></h3>
	<?php if (!empty($generalUnit['SaleQuotationItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Unit Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Discount Type'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th><?php echo __('Total Price'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Sale Quotation Id'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Planning Costing'); ?></th>
		<th><?php echo __('Planning Margin'); ?></th>
		<th><?php echo __('Planning Price'); ?></th>
		<th><?php echo __('Sale Margin'); ?></th>
		<th><?php echo __('Sale Price'); ?></th>
		<th><?php echo __('Total Cost'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['SaleQuotationItem'] as $saleQuotationItem): ?>
		<tr>
			<td><?php echo $saleQuotationItem['id']; ?></td>
			<td><?php echo $saleQuotationItem['name']; ?></td>
			<td><?php echo $saleQuotationItem['quantity']; ?></td>
			<td><?php echo $saleQuotationItem['general_unit_id']; ?></td>
			<td><?php echo $saleQuotationItem['unit_price']; ?></td>
			<td><?php echo $saleQuotationItem['discount']; ?></td>
			<td><?php echo $saleQuotationItem['discount_type']; ?></td>
			<td><?php echo $saleQuotationItem['tax']; ?></td>
			<td><?php echo $saleQuotationItem['total_price']; ?></td>
			<td><?php echo $saleQuotationItem['inventory_item_id']; ?></td>
			<td><?php echo $saleQuotationItem['sale_quotation_id']; ?></td>
			<td><?php echo $saleQuotationItem['type']; ?></td>
			<td><?php echo $saleQuotationItem['planning_costing']; ?></td>
			<td><?php echo $saleQuotationItem['planning_margin']; ?></td>
			<td><?php echo $saleQuotationItem['planning_price']; ?></td>
			<td><?php echo $saleQuotationItem['sale_margin']; ?></td>
			<td><?php echo $saleQuotationItem['sale_price']; ?></td>
			<td><?php echo $saleQuotationItem['total_cost']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_quotation_items', 'action' => 'view', $saleQuotationItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_quotation_items', 'action' => 'edit', $saleQuotationItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_quotation_items', 'action' => 'delete', $saleQuotationItem['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotationItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Stock Temps'); ?></h3>
	<?php if (!empty($generalUnit['StockTemp'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Store'); ?></th>
		<th><?php echo __('Rack'); ?></th>
		<th><?php echo __('Rack Code'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Unit'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Warehouse'); ?></th>
		<th><?php echo __('Old Code'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($generalUnit['StockTemp'] as $stockTemp): ?>
		<tr>
			<td><?php echo $stockTemp['id']; ?></td>
			<td><?php echo $stockTemp['store']; ?></td>
			<td><?php echo $stockTemp['rack']; ?></td>
			<td><?php echo $stockTemp['rack_code']; ?></td>
			<td><?php echo $stockTemp['code']; ?></td>
			<td><?php echo $stockTemp['description']; ?></td>
			<td><?php echo $stockTemp['general_unit_id']; ?></td>
			<td><?php echo $stockTemp['unit']; ?></td>
			<td><?php echo $stockTemp['quantity']; ?></td>
			<td><?php echo $stockTemp['warehouse']; ?></td>
			<td><?php echo $stockTemp['old_code']; ?></td>
			<td><?php echo $stockTemp['type']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'stock_temps', 'action' => 'view', $stockTemp['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'stock_temps', 'action' => 'edit', $stockTemp['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'stock_temps', 'action' => 'delete', $stockTemp['id']), array(), __('Are you sure you want to delete # %s?', $stockTemp['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Stock Temp'), array('controller' => 'stock_temps', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
