<?php
echo $this->Html->link(__('Add New'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); 
?> 

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Term Of Deliveries</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content">  
	<table class="table table-bordered">
	<thead>
	<tr> 
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('description'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($termOfDeliveries as $termOfDelivery): ?>
	<tr> 
		<td><?php echo h($termOfDelivery['TermOfDelivery']['name']); ?>&nbsp;</td>
		<td><?php echo h($termOfDelivery['TermOfDelivery']['description']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $termOfDelivery['TermOfDelivery']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $termOfDelivery['TermOfDelivery']['id']), array('class' => 'btn btn-warning btn-xs')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $termOfDelivery['TermOfDelivery']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure you want to delete # %s?', $termOfDelivery['TermOfDelivery']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
	</ul>
</div>
 
</div>
</div>
</div>
</div>