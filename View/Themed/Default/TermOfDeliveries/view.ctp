<div class="termOfDeliveries view">
<h2><?php echo __('Term Of Delivery'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($termOfDelivery['TermOfDelivery']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($termOfDelivery['TermOfDelivery']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($termOfDelivery['TermOfDelivery']['description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Term Of Delivery'), array('action' => 'edit', $termOfDelivery['TermOfDelivery']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Term Of Delivery'), array('action' => 'delete', $termOfDelivery['TermOfDelivery']['id']), array(), __('Are you sure you want to delete # %s?', $termOfDelivery['TermOfDelivery']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Term Of Deliveries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Term Of Delivery'), array('action' => 'add')); ?> </li>
	</ul>
</div>
