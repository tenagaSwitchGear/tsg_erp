<div class="termOfDeliveries form">
<?php echo $this->Form->create('TermOfDelivery'); ?>
	<fieldset>
		<legend><?php echo __('Edit Term Of Delivery'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TermOfDelivery.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('TermOfDelivery.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Term Of Deliveries'), array('action' => 'index')); ?></li>
	</ul>
</div>
