<?php echo $this->Html->link('Add New Template', array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Terms &amp; Conditions Templates</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>

<div class="printTemplates index">
	<h2><?php echo __('Print Templates'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($printTemplates as $printTemplate): ?>
	<tr> 
		<td><?php echo h($printTemplate['PrintTemplate']['name']); ?>&nbsp;</td> 
		<td><?php echo status($printTemplate['PrintTemplate']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $printTemplate['PrintTemplate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $printTemplate['PrintTemplate']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $printTemplate['PrintTemplate']['id']), array(), __('Are you sure you want to delete # %s?', $printTemplate['PrintTemplate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
</div>
</div>
</div>
</div>

<?php

function status($status) {
	if($status == 0) {
		return 'Draft';
	} 
	if($status == 1) {
		return 'Active';
	}
	if($status == 2) {
		return 'Disabled';
	}
}

?>