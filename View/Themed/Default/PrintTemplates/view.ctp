<?php echo $this->Html->link('Add New Template', array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Terms &amp; Conditions Templates</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
 
				<h2><?php echo __('Print Template'); ?></h2>

					<div class="row black-color"> 
							<div class="col-md-12"><?php echo __('Name'); ?></div>
							<div class="col-md-12">
								<?php echo h($printTemplate['PrintTemplate']['name']); ?>
								&nbsp;
							</div>
							<div class="col-md-12"><?php echo __('Body'); ?></div>
							<div class="col-md-12">
								<?php echo $printTemplate['PrintTemplate']['body']; ?>
								&nbsp;
							</div>
							<div class="col-md-12"><?php echo __('Status'); ?></div>
							<div class="col-md-12">
								<?php echo status($printTemplate['PrintTemplate']['status']); ?>
								&nbsp;
							</div>
					</div>  
</div>
</div> 
</div>
</div>  

<?php

function status($status) {
	if($status == 0) {
		return 'Draft';
	} 
	if($status == 1) {
		return 'Active';
	}
	if($status == 2) {
		return 'Disabled';
	}
}

?>