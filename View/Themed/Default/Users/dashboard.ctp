 <!-- top tiles -->
  <?php
    if($this->Session->read('Auth.User.id')) {
      //echo $this->element('Pages/topDashboard'); 
    }

    echo $this->Session->flash(); 
 ?> 
 <div class="row tile_count">
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-vcard"></i> Total Customer</span>
  <div class="count"><?php echo count($customers); ?></div>
   
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-bullhorn"></i> Total Tender</span>
  <div class="count"><?php echo count($tenders); ?></div> 
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-file-text-o"></i> Total Quotation</span>
  <div class="count green"><?php echo count($quotations); ?></div> 
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-gears"></i> Total Job</span>
  <div class="count"><?php echo count($jobs); ?></div> 
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-puzzle-piece"></i> Total Production</span>
  <div class="count"><?php echo count($productions); ?></div> 
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-cubes"></i> Total Finished Goods</span>
  <div class="count"><?php echo count($finished_goods); ?></div> 
</div>

</div>

<div class="row"> 
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_390">
                <div class="x_title">
                  <h2>Company News</h2>
                
                  <div class="clearfix"></div>
                </div>
                <div class="x_content"> 
                  <div class="list-group">
                  <?php if($news) {
                    foreach ($news as $new) { ?>
                    <div class="list-group-item">
                      <a href="<?php echo BASE_URL . 'news/view/' . $new['News']['id']; ?>">
                        <h4 class="list-group-item-heading"><?php echo $new['News']['title']; ?></h4>
                        <p class="list-group-item-text"><?php echo substr($new['News']['body'], 0, 180); ?></p>

                      </a> 
                      <p><i class="fa fa-clock-o"></i> <?php echo date("d-m-Y g:i A", strtotime($new['News']['created'])); ?></p>
                      </div>
                  <?php  }
                  } ?>
                    
                  </div> 
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_390 overflow_hidden">
                <div class="x_title">
                  <h2>Internal Link</h2>
                   
                  <div class="clearfix"></div> 
                </div>
                <div class="x_content">
                   <div class="list-group">

                   <div class="list-group-item">
                      <a href="<?php echo BASE_URL . 'users/editpassword'; ?>">
                        <h4 class="list-group-item-heading">Change Password</h4> 
                      </a>  
                      </div>

                      <div class="list-group-item">
                      <a href="<?php echo BASE_URL . 'inventory_purchase_requisitions'; ?>">
                        <h4 class="list-group-item-heading">Purchase Requisition</h4> 
                      </a>  
                      </div>

                      <div class="list-group-item">
                      <a href="<?php echo BASE_URL . 'inventory_purchase_requisitions/verifyindex'; ?>">
                        <h4 class="list-group-item-heading">PR Approval (Authorized area)</h4> 
                      </a>  
                      </div>

                      <div class="list-group-item">
                      <a href="<?php echo BASE_URL . 'inventory_material_requests'; ?>">
                        <h4 class="list-group-item-heading">Material Requisition</h4> 
                      </a>  
                      </div>
                      <div class="list-group-item">
                      <a href="<?php echo BASE_URL . 'inventory_material_requests/verify'; ?>">
                        <h4 class="list-group-item-heading">MRN Approval (Authorized area)</h4> 
                      </a>  
                      </div>

                      <div class="list-group-item">
                      <a href="<?php echo BASE_URL . 'sale_handing_overs/receiveindex'; ?>">
                        <h4 class="list-group-item-heading">Handing Over Document</h4> 
                      </a>  
                      </div>  

                   </div>
                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_390 overflow_hidden">
                <div class="x_title">
                  <h2>External Link</h2>
                   
                  <div class="clearfix"></div> 
                </div>
                <div class="x_content">
                   <div class="list-group">

                      <div class="list-group-item">
                      <a href="http://hq.mytsg.com.my:3391" target="_blank">
                        <h4 class="list-group-item-heading">TSG Cloud</h4> 
                      </a>  
                      </div>
                      <div class="list-group-item">
                      <a href="http://hq.mytsg.com.my:8383/login.aspx" target="_blank">
                        <h4 class="list-group-item-heading">TSG Webmail</h4> 
                      </a>  
                      </div>

                      <div class="list-group-item">
                      <a href="http://www.e-hrms.com/sys/login.php" target="_blank">
                        <h4 class="list-group-item-heading">TSG EHRMS</h4> 
                      </a>  
                      </div>


                      <div class="list-group-item">
                      <a href="http://www.emailmeform.com/builder/form/4vFcTbLb71PYZ" target="_blank">
                        <h4 class="list-group-item-heading">e-Borang Permohonan Kenderaan</h4> 
                      </a>  
                      </div>
                      <div class="list-group-item">
                      <a href="http://www.emailmeform.com/builder/form/KWpdfLeJZXO1f5d1gIh3e" target="_blank">
                        <h4 class="list-group-item-heading">e-Borang Permohonan Penggunaan Peralatan IT</h4> 
                      </a>  
                      </div>

                      <div class="list-group-item">
                      <a href="http://www.emailmeform.com/builder/form/H2cnA2852bF1g1bYv4" target="_blank">
                        <h4 class="list-group-item-heading">e-Borang Aduan kerosakan Admin</h4> 
                      </a>  
                      </div>

                      <div class="list-group-item">
                      <a href="http://smb.cidb.gov.my/personnel/application/" target="_blank">
                        <h4 class="list-group-item-heading">CIDB Renewal</h4> 
                      </a>  
                      </div>


                   </div>
                </div>
              </div>
            </div>
 

          </div> <!-- End 1st row -->
          
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Production</h2>
                    <?php    $new_latest_running = array(); 
                                foreach($latest_production[0]['latest_running'] as $running){
                                    $new_latest_running[] = array(
                                        'name' => $running['ProductionOrder']['name'],
                                        'progress' => $running['ProductionOrder']['progress']
                                    );
                                }     
                                //echo json_encode($new_latest_running);

                                $new_latest_completed = array();
                                foreach($latest_production[0]['latest_complete'] as $complete){
                                    $new_latest_completed[] = array(
                                        'name' => $complete['ProductionOrder']['name'],
                                        'progress' => $complete['ProductionOrder']['progress']
                                    );

                                }      
                        ?>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-6">
                        Latest Running Production
                        <br>
                        <div id="bar-example"></div>
                    </div>
                    <div class="col-md-6">
                        Latest Complete Production
                        <br>
                        <div id="bar-example_2" style="height:160px; width: 100%"></div>
                        <table cellpadding="0" cellspacing="0" class="table table-bordered">
                            <tr>
                                <th>Production</th>
                                <th>Date Completed</th>
                            </tr>
                            <?php foreach($latest_production[0]['latest_complete'] as $complete){ ?>
                            <tr>
                                <td><?php echo $complete['ProductionOrder']['name']; ?></td>
                                <td><?php echo date('d-m-Y', strtotime($complete['ProductionOrder']['modified'])); ?></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Sales</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-12">
                        <?php 
                                $ttl = 0;
                                for($i=0; $i<count($data_submit_tender); $i++){
                                    $jum = $ttl + $data_submit_tender[$i]['Submit'];
                                    $ttl = $jum;
                                }
                        ?>
                        <strong>Tender Submit : <?php echo date('Y'); ?> ( Total :<?php echo $ttl; ?> )</strong>
                        <div id="tender-submit"></div>
                        <br>
                    </div>
                    <div class="col-md-12"> 
                        <strong>Statistic ( Draft / Award / Failed / Extended Date )</strong>
                        <table cellpadding="0" cellspacing="0" class="table table-hovered table-bordered">
                            <thead>
                                <tr>
                                    <th class="col-md-3">Draft</th>
                                    <th class="col-md-3">Award</th>
                                    <th class="col-md-3">Failed</th>
                                </tr>
                                <?php for($i=0; $i<5; $i++){ ?>
                                <?php if(empty($latest_quotation['Draft'][$i]['name'])){ $draft = 0; }else{ $draft = $latest_quotation['Draft'][$i]['name']; } ?>
                                <?php if(empty($latest_quotation['Award'][$i]['name'])){ $award = 0; }else{ $award = $latest_quotation['Award'][$i]['name']; } ?>
                                <?php if(empty($latest_quotation['Failed'][$i]['name'])){ $failed = 0; }else{ $failed = $latest_quotation['Failed'][$i]['name']; } ?>
                                <tr>
                                    <td><?php echo $draft; ?></td>
                                    <td><?php echo $award; ?></td>
                                    <td><?php echo $failed; ?></td>
                                </tr>
                                <?php } ?>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

          


          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Master Planning Schedule</h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content"> 

<div class="page-header">

    <div class="pull-right form-inline">
      <div class="btn-group">
        <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
        <button class="btn" data-calendar-nav="today">Today</button>
        <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
      </div>
      <div class="btn-group">
        <button class="btn btn-warning" data-calendar-view="year">Year</button>
        <button class="btn btn-warning active" data-calendar-view="month">Month</button>
        <button class="btn btn-warning" data-calendar-view="week">Week</button>
        <button class="btn btn-warning" data-calendar-view="day">Day</button>
      </div>
    </div>
 
  </div>

 
    <div id="calendar"></div>
   
 <?php 
      echo $this->Html->css(array(
        '/js/libs/bootstrap-calendar/css/calendar.min.css',
      ));   
    ?>
<?php $this->start('script'); ?>
<?php
      echo $this->Html->script(array(
        '/js/libs/bootstrap-calendar/js/calendar.min.js',
        '/js/libs/bootstrap-calendar/components/underscore/underscore-min.js',
        '/js/libs/bootstrap-calendar/js/moment.js',
        '/js/libs/morris_horizontal/morris.min.js',
        '/js/libs/raphael/raphael.min.js'
      ));
    ?>   
<script type="text/javascript"> 
 (function($) {

  "use strict"; 
  // Get current date by moment.js
  var today = moment().format('YYYY-MM-DD'); 

  var options = {
    // /site/ is root for this file
    events_source: baseUrl + 'project_schedules/ajaxdate',
    //events_source: source,
    view: 'year',
    tmpl_path: baseUrl + 'js/libs/bootstrap-calendar/tmpls/',
    tmpl_cache: false, 
    day: today,
    onAfterEventsLoad: function(events) {
      if(!events) {
        return;
      }
      var list = $('#eventlist');
      list.html(''); 
      $.each(events, function(key, val) {
        $(document.createElement('li'))
          .html('<a href="' + val.url + '">' + val.title + '</a>')
          .appendTo(list);
         
      });
    },
    onAfterViewLoad: function(view) {
      $('.page-header h3').text(this.getTitle());
      $('.btn-group button').removeClass('active');
      $('button[data-calendar-view="' + view + '"]').addClass('active');
    },
    classes: {
      months: {
                inmonth: 'cal-day-inmonth',
                outmonth: 'cal-day-outmonth',
                saturday: 'cal-day-weekend',
                sunday: 'cal-day-weekend',
                holidays: 'cal-day-holiday',
                today: 'cal-day-today'
            },
            week: {
                workday: 'cal-day-workday',
                saturday: 'cal-day-weekend',
                sunday: 'cal-day-weekend',
                holidays: 'cal-day-holiday',
                today: 'cal-day-today'
            }
    }
  };

  var calendar = $('#calendar').calendar(options);

  $('.btn-group button[data-calendar-nav]').each(function() {
    var $this = $(this);
    $this.click(function() {
      calendar.navigate($this.data('calendar-nav')); 

    });
  });

  $('.btn-group button[data-calendar-view]').each(function() {
    var $this = $(this);
    $this.click(function() {
      calendar.view($this.data('calendar-view'));
    });
  });

  $('#first_day').change(function(){
    var value = $(this).val();
    value = value.length ? parseInt(value) : null;
    calendar.setOptions({first_day: value});
    calendar.view();
  });

  $('#language').change(function(){
    calendar.setLanguage($(this).val());
    calendar.view();
  });

  $('#events-in-modal').change(function(){
    var val = $(this).is(':checked') ? $(this).val() : null;
    calendar.setOptions({modal: val});
  });
  $('#format-12-hours').change(function(){
    var val = $(this).is(':checked') ? true : false;
    calendar.setOptions({format12: val});
    calendar.view();
  });
  $('#show_wbn').change(function(){
    var val = $(this).is(':checked') ? true : false;
    calendar.setOptions({display_week_numbers: val});
    calendar.view();
  });
  $('#show_wb').change(function(){
    var val = $(this).is(':checked') ? true : false;
    calendar.setOptions({weekbox: val});
    calendar.view();
  });
  $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
    //e.preventDefault();
    //e.stopPropagation();
  });
}(jQuery));

</script>
<script type="text/javascript">
var data_running = <?php echo json_encode($new_latest_running); ?>;
Morris.Bar({
  element: 'bar-example',
  data: data_running,
  xkey: 'name',
  ykeys: ['progress'],
  labels: ['WIP - %'],
  horizontal: true,
  stacked: false
});
</script>
<script type="text/javascript">
var data_complete = <?php echo json_encode($new_latest_completed); ?>;

Morris.Bar({
    barSizeRatio:0.45,
  element: 'bar-example_2',
  data: data_complete,
  xkey: 'name',
  ykeys: ['progress'],
  labels: ['Production'],
  horizontal: false,
  stacked: false
});
</script>
<script type="text/javascript">
var data_tender_submit = <?php echo json_encode($data_submit_tender); ?>;

Morris.Bar({
    barSizeRatio:0.45,
    element: 'tender-submit',
    data: data_tender_submit,
    xkey: 'Months',
    ykeys: ['Submit'],
    labels: ['Submitted'],
    horizontal: false,
    stacked: false,
    barColors: ["#B21516"],
});
</script>

<?php $this->end(); ?>




    
                </div>
              </div>
            </div>

            </div>


            