<div class="row"> 
  	<div class="col-xs-12">
  		 
    	<div class="x_panel tile">
      		 
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	
<div class="inventorySuppliers form">
<?php echo $this->Form->create('InventorySupplier', array('class' => 'form-horizontal', 'type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('e-Supplier Registration / Renewal Form'); ?></legend>
		<p style="color:red"><b>Note:</b> Please use latest web browser (Google Chrome, Mozilla Firefox) to get full support by this system.</p>
		<div class="form-group">
		<label class="col-sm-12"><h4>Registration Type</h4></label> 
		</div>

		<?php $reg_type = array('1' => 'Register New Company', '2' => 'Renewal'); ?>
		<div class="form-group">
		<label class="col-sm-3">Register Type *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('register_type', array('empty'=>array('0'=>'Select Type'), 'options'=>$reg_type, 'class' => 'form-control', 'label' => false, 'id' => 'registerType')); ?>
		</div>
		</div>
		<div class="form-group" id="supplier_code" <?php if($this->request->data['InventorySupplier']['register_type'] == 2) { ?>style="display: block"<?php } else { ?>display: none<?php } ?>>
		<label class="col-sm-3">Security Code *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('random_code', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Security Code')); ?>
		<small>Please contact Procurement Department to get the Security Code</small>
		</div>
		</div>
		<hr/>
		<div class="form-group">
		<label class="col-sm-12"><h4>Business Information</h4></label> 
		</div>

		<div class="form-group">
		<label class="col-sm-3">Company Name *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Company Name')); ?>
		</div>
		</div>
		
	 
		 
		<div class="form-group">
		<label class="col-sm-3">GST ID (Optional)</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('gst', array('class' => 'form-control', 'label' => false, 'placeholder' => 'GST ID')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Website (Optional)</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('website', array('class' => 'form-control', 'label' => false, 'placeholder' => 'www.example.com')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Company Registration Number *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('company_reg_no', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Company Registration Number')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Currency *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('general_currency_id', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>

		<div class="form-group">
		<label class="col-sm-3">Company Type *</label>
		<div class="col-sm-9">
		<?php 
		$type = array(
			'Sdn Bhd' => 'Sdn Bhd',
			'Enterprise' => 'Enterprise',
			'LLP' => 'LLP',
			'Berhad' => 'Berhad',
			'International' => 'International'
			);
		echo $this->Form->input('company_type', array('options' => $type, 'empty' => 'Select Company Type', 'id' => 'company_type', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>

		<div class="form-group" id="expiry_date" style="display:none">
		<label class="col-sm-3">Company Reg Expiry Date *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('company_expiry_date', array('type' => 'text', 'id' => 'dateonly', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Y-m-d')); ?>
		</div>
		</div>

		<div class="form-group">
		<label class="col-sm-3">Company Registration Date *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('company_reg_date', array('type' => 'text', 'id' => 'dateonly_2', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Company Registration Date')); ?>
		</div>
		</div>

		<div class="form-group">
		<label class="col-sm-3">Business Category *</label>
		<div class="col-sm-9">
		<?php  
		$cat = array(
			'Manufacturing' => 'Manufacturing',
			'Supply' => 'Supply',
			'Services' => 'Services',
			);

		echo $this->Form->input('category', array('options' => $cat, 'class' => 'form-control', 'label' => false, 'empty' => 'Select Category')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Ownership Status *</label>
		<div class="col-sm-9">
		<?php 
		$ownership = array(
			'Local' => 'Local',
			'Joint Venture & Partnership' => 'Joint Venture & Partnership',
			'Foreign' => 'Foreign',
			);
			echo $this->Form->input('ownership_status', array('options' => $ownership, 'class' => 'form-control', 'label' => false, 'empty' => 'Select Ownership Status')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Term of Payment *</label>
		<div class="col-sm-9">
		<?php  
			echo $this->Form->input('term_of_payment_id', array('class' => 'form-control', 'label' => false, 'empty' => 'Select Term of Payment')); ?>
		</div>
		</div> 
		<?php echo $this->Form->input('credit_limit', array('type' => 'hidden', 'value' => 0)); ?>
		 
		<div class="form-group">
		<label class="col-sm-3">No of Employee *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('no_of_employee', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Number of Employee')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Year of Business *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('years_of_business', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Year of Business')); ?>
		</div>
		</div>
		
		
<hr/>
		
		<div class="form-group">
		<label class="col-sm-12"><h4>Contact Information</h4></label> 
		</div>

		<div class="form-group">
		<label class="col-sm-3">Company Address *</label>
		<div class="col-sm-9"> 
		<?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Company Address', 'type' => 'textarea')); ?>
		</div>
		</div>

		<div class="form-group">
		<label class="col-sm-3">City *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('city', array('class' => 'form-control', 'label' => false, 'placeholder' => 'City')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Postcode *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('postcode', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Postcode')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">State *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('state_id', array('id'=> 'state_id', 'empty' => 'Select', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>
		<div class="form-group" style="display: none;" id="state">
		<label class="col-sm-3">Please Enter State *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('state', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'placeholder' => 'State', 'required' => false)); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Country *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('country_id', array('empty' => 'Select', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Phone (Office) *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('phone_office', array('class' => 'form-control', 'label' => false, 'placeholder' => '6032100000')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Person Incharge *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('pic', array('class' => 'form-control', 'label' => false, 'placeholder' => 'PIC Name')); ?>
		</div>
		</div>

		<div class="form-group">
		<label class="col-sm-3">Phone (Mobile) *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('phone_mobile', array('class' => 'form-control', 'label' => false, 'placeholder' => '60123456789')); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">FAX</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('fax', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Fax', 'required' => false)); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Email *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Email')); ?>
		</div>
		</div>

		<hr/>
		

		<div class="form-group">
		<label class="col-sm-12"><h4>Additional Information</h4></label> 
		</div>

		<div class="form-group">
			<div class="col-sm-12">I acknowledge that the supply of equipments/services/works from my company does not give effect/impact on the safety, environmental and health.</div>  
			<div class="col-sm-6">
			<input type="radio" name="data[InventorySupplier][check_supply]" value="0"> No
			</div>
			<div class="col-sm-6">
			<input type="radio" name="data[InventorySupplier][check_supply]" value="1"> Yes
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">Compliance with applicable law.</div>  
			<div class="col-sm-6">
			<input type="radio" name="data[InventorySupplier][check_law]" value="0"> No
			</div>
			<div class="col-sm-6">
			<input type="radio" name="data[InventorySupplier][check_law]" value="1"> Yes
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">Does your company actively participate or associate in programs related to preserving the environment?</div>  
			<div class="col-sm-6">
			<input type="radio" name="data[InventorySupplier][check_program]" value="0"> No
			</div>
			<div class="col-sm-6">
			<input type="radio" name="data[InventorySupplier][check_program]" value="1"> Yes
			</div>
		</div> 

		<div class="form-group">
		<label class="col-sm-3">If yes, please specify</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('if_yes', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
		</div>
		</div>
		<hr/>
		
		<div class="form-group">
			<div class="col-sm-12">Quality Management System</div>  
			<div class="col-sm-2">
			<input type="checkbox" name="data[InventorySupplier][check_iso]" value="1"> ISO
			</div>
			<div class="col-sm-2">
			<input type="checkbox" name="data[InventorySupplier][check_ems]" value="1"> EMS
			</div>
			<div class="col-sm-2">
			<input type="checkbox" name="data[InventorySupplier][check_ohsas]" value="1"> OHSAS
			</div>
			<div class="col-sm-2">
			<input type="checkbox" name="data[InventorySupplier][check_others]" value="1"> Others
			</div>
			<div class="col-sm-2">
			<input type="checkbox" name="data[InventorySupplier][check_none]" value="1"> None
			</div> 
		</div> 

		<div class="form-group">
		<label class="col-sm-3">If Others, please specify</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('others', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
		</div>
		</div>

		<div class="form-group">
		<div class="col-sm-12">
		<h4>Supplier Category</h4>
		<p>Please choose atleast 1 Supplier Category.</p>
		</div> 
		</div>

		<div class="form-group">
			<label class="col-sm-3" style="padding-top: 8px">Category</label>
			<div class="col-sm-8">
			<?php echo $this->Form->input("category_item.", array("empty" => "Select Category", "options"=>$category_2, "class"=> "form-control", "label"=> false, "onchange"=>"showselect(this.value)", "id" =>"cat")); ?>
			</div>
		</div>
		<div id="loadCategory"></div>
		<div class="form-group">
			<label class="col-sm-3" style="padding-top: 8px">&nbsp;</label>
			<div class="col-sm-9">
				<a href="#" id="addCategory" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Category</a>
			</div>
		</div> 

		<hr/>
		<div class="form-group">
		<div class="col-sm-12">
		<h4>Attachment</h4>
		<p>Please upload Supporting Documents (PDF / Ms Words)</p>
		</div>
		</div>

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">ROC Certificate <span class="red">*</span></label>

		<input type="hidden" name="data[InventorySupplierFile][subject][]" class="form-control" value="ROC Certificate" required="required">
		
		<div class="col-sm-9">
		<?php echo $this->Form->input('InventorySupplierFile.file.', array('type' => 'file', 'class' => 'form-control', 'label' => false, 'required' => true)); ?> 
		<input type="hidden" name="data[InventorySupplierFile][dir][]">
		<small>SSM / Form 9 / Form 13 (Malaysian)</small>
		</div>
		 
		</div> 

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">Company Profile <span class="red">*</span></label>
		
		<input type="hidden" name="data[InventorySupplierFile][subject][]" class="form-control" value="Company Profile" required="required">
	
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][file][]" class="form-control" required="required">
		<input type="hidden" name="data[InventorySupplierFile][dir][]">
		</div> 
		</div>

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">Bank Statement <span class="red">*</span></label>
		
		<input type="hidden" name="data[InventorySupplierFile][subject][]" class="form-control" value="Bank Statement" required="required">
	
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][file][]" class="form-control" required="required">
		<input type="hidden" name="data[InventorySupplierFile][dir][]">
		<small>3 month latest bank statement</small>
		</div> 
		</div>

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">3 years P&amp;L</label> 
		<input type="hidden" name="data[InventorySupplierFile][subject][]" class="form-control" value="P & L"> 
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][file][]" class="form-control">
		<input type="hidden" name="data[InventorySupplierFile][dir][]">
		<small>This field required for Sdn Bhd &amp; Bhd status company.</small>
		</div> 
		</div> 

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">Balance Sheet</label> 
		<input type="hidden" name="data[InventorySupplierFile][subject][]" class="form-control" value="Balance Sheet"> 
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][file][]" class="form-control">
		<input type="hidden" name="data[InventorySupplierFile][dir][]">
		<small>This field required for Sdn Bhd &amp; Bhd status company.</small>
		</div> 
		</div> 


		

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">Additional File (Optional)</label>
		
		<input type="hidden" name="data[InventorySupplierFile][subject][]" class="form-control" value="Additional File">
	
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][file][]" class="form-control">
		<input type="hidden" name="data[InventorySupplierFile][dir][]">
		</div> 
		</div>

		<hr/> 


		<div class="form-group">
		<label class="col-sm-8">By click Submit, you are Agree with our <a href="http://www.mytsg.com.my/policy.html" target="_blank">Terms &amp; Conditions</a></label>
		<div class="col-sm-4">
		<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>
		</div>
		</div>
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
 
 
$(function() { 

	$('#registerType').on('change', function() {
		var nilai = $(this).val();
		if(nilai == 2){
			$('#supplier_code').show();
		}else{
			$('#supplier_code').hide();
		}
	});
	 
	var categories = <?php echo json_encode($category); ?>;

	$.each(categories, function (index, data) {
		$('.categories').each(function() {
			var html = '<option value="'+data.id+'">'+data.name+'</option>';
	        $(this).append(html);
	    });
	});


    $('#company_type').on('change', function() {	
    	var type = $(this).val();
    	if(type == 'Enterprise') {
    		$('#expiry_date').show(600);
    	} else {
    		$('#expiry_date').hide(600);
    	}
    });
});	
	
	function showselect(nilai){
		var curr_txt = $("#selectvalue").val();
		var curr_id = $("#id_value").val();

		var txt = $("#cat option:selected").text();
		var id_cat = nilai;

		if(!curr_txt){
			var new_txt = txt;
		}else{
			var new_txt = curr_txt + " | " + txt;
		}
		if(!curr_id){
			var new_id = id_cat;
		}else{
			var new_id = curr_id + " | " + id_cat;
		}

		$("#selectvalue").val(new_txt);
		$("#id_value").val(new_id);
	}

	function clearselect(){
		$("#selectvalue").value = '';
		$("#id_value").value = '';
	}

	var category = 1;
	$('#addCategory').click(function() {
		//alert("SINI");
		var html = '<div id="removeCategory'+category+'">'; 
	    html += '<div class="form-group">';  
	    html += '<label class="col-sm-3" style="padding-top: 8px">Category</label>';
	    html += '<div class="col-sm-8">';
	    html += '<select name="data[InventorySupplier][category_item][]" class="form-control">';
	    html += '<option>Select Category</option>';
	    html += '<?php foreach($category_2 as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
	    html += '</select>';
	    html += '</div>';
	    html += '<a href="#" class="btn btn-danger" onclick="removeCategory('+category+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div></div>';    
	    $("#loadCategory").append(html);  
		category++; 
	});

	var file = 1;
	$('#addFile').click(function() {
		//alert("SINI");
		var html = '<div id="removeFile'+file+'">'; 
	    html += '<div class="form-group">';  
	    html += '<label class="col-sm-2" style="padding-top: 8px">Subject</label>';
	    html += '<div class="col-sm-4">';
	    html += '<input type="text" name="data[InventorySupplierFile]['+file+'][subject]" class="form-control" placeholder="File name (Ie: Company Profile / Bank Statement"required>';  
	    html += '</div>';
	    html += '<div class="col-sm-5">';
	    html += '<input type="file" name="data[InventorySupplierFile]['+file+'][file]" class="form-control"required>'; 
	    html += '<input type="hidden" name="data[InventorySupplierFile]['+file+'][dir]">'; 
	    html += '</div>';
	    html += '<div class="col-sm-1">';
	    html += '<a href="#" class="btn btn-danger" onclick="removeFile('+file+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div></div>';    
	    $("#loadFile").append(html);  
		file++; 
	});

	function removeCategory(row) {
    	$('#removeCategory'+row).html('');
    	return false;
	}
</script>
<?php $this->end(); ?>