<?php echo $this->Html->link(__('List Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		 
<?php echo $this->Html->link(__('New Contact'), array('controller' => 'inventory_supplier_contacts', 'action' => 'add?id='.$inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('New Address'), array('controller' => 'inventory_supplier_addresses', 'action' => 'add?id='.$inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_suppliers', 'action' => 'edit', $inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-warning btn-sm')); ?>


 <div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Inventory Supplier</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="container table-responsive">
 
				<dl> 
					<dt class="col-sm-3"><?php echo __('Security Code'); ?></dt> 
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['random_code']); ?><br/>
						<small>It use for Supplier Renewal. Please make sure you are not send to wrong person.</small>
					</dd>
					<dt class="col-sm-3"><?php echo __('Company Name'); ?></dt> 
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('GST ID'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['gst']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Website'); ?></dt>
					<dd class="col-sm-9">
						: <a href="<?php echo h($inventorySupplier['InventorySupplier']['website']); ?>" target="_blank"><?php echo h($inventorySupplier['InventorySupplier']['website']); ?></a>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Comp. Registration No.'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['company_reg_no']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Currency'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['GeneralCurrency']['name']).' /  '.$inventorySupplier['GeneralCurrency']['iso_code']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Company Type'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['company_type']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Comp. Registration Date'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['company_reg_date']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Business Category'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['category']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Ownership Status'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['ownership_status']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Term of Payment '); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['TermOfPayment']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Credit Limit'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['credit_limit']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('No of Employee'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['no_of_employee']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Year of Business'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['years_of_business']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-12"><h3>Contact Information</h3><hr></dt>
					<dt class="col-sm-3"><?php echo __('Company Address'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['address']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('City'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['city']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Postcode'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['postcode']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('State'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['State']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('State (Non Malaysian)'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['state']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Country'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $this->Html->link($inventorySupplier['Country']['name'], array('controller' => 'countries', 'action' => 'view', $inventorySupplier['Country']['id'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('PIC'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['pic']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Phone (Office)'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['phone_office']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Phone (Mobile)'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['phone_mobile']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Fax'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['fax']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Email'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['email']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['created']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplier['InventorySupplier']['modified']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Status'); ?></dt>
                  	<dd class="col-sm-9">  
 
						: <a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="status_<?php echo $inventorySupplier['InventorySupplier']['status']; ?>" class="editable" data-type="select" data-pk="<?php echo $inventorySupplier['InventorySupplier']['id']; ?>" data-name="#status_<?php echo $inventorySupplier['InventorySupplier']['status']; ?>" data-emptytext="<?php echo $inventorySupplier['InventorySupplier']['status']; ?>" data-url="<?php echo BASE_URL.'inventory_suppliers/setstatus'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $inventorySupplier['InventorySupplier']['status']; ?>" data-source="<?php echo BASE_URL.'inventory_suppliers/getstatus'; ?>"></a>     
 

                      &nbsp;
                  	</dd> 
                  	<dt class="col-sm-12"><h3>Additional Information</h3><hr></dt>
                  	<dt class="col-sm-12">&bull; I acknowledge that the supply of equipments/services/works from my company does not give effect/impact on the safety, environmental and health.</dt>
                  	<dd class="col-sm-2">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_supply'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> Yes</dd>
                  	<dd class="col-sm-9">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_supply'] == 0){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> No</dd>
                  	<dt class="col-sm-12">&bull; Compliance with applicable law.</dt>
                  	<dd class="col-sm-2">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_law'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> Yes</dd>
                  	<dd class="col-sm-9">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_law'] == 0){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> No</dd>
                  	<dt class="col-sm-12">&bull; Does your company actively participate or associate in programs related to preserving the environment?</dt>
                  	<dd class="col-sm-2">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_program'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> Yes</dd>
                  	<dd class="col-sm-9">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_program'] == 0){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> No</dd>
                  	<?php if($inventorySupplier['InventorySupplier']['check_program'] == 1){ ?>
                  	<dt class="col-sm-3">Notes</dt>
                  	<dd class="col-sm-9">: <?php echo $inventorySupplier['InventorySupplier']['if_yes']; ?></dd>
                  	<?php } ?>
                  	<dt class="col-sm-12">Quality Management System</dt>
                  	<dd class="col-sm-2">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_iso'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> ISO</dd>
                  	<dd class="col-sm-2">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_ems'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> EMS</dd>
                  	<dd class="col-sm-2">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_ohsas'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> OHSAS</dd>
                  	<dd class="col-sm-2">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_others'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> Others</dd>
                  	<dd class="col-sm-3">&nbsp; <?php if($inventorySupplier['InventorySupplier']['check_none'] == 1){ ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?> None</dd>
                  	<?php if($inventorySupplier['InventorySupplier']['check_others'] == 1){ ?>
                  	<dt class="col-sm-3">Notes</dt>
                  	<dd class="col-sm-9">: <?php echo $inventorySupplier['InventorySupplier']['others']; ?></dd>
                  	<?php } ?>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="related">
				<h4><?php echo __('Supplier Item Category'); ?></h4>
				<?php if (!empty($categories)): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
				<tr>
					<th><?php echo __('Name'); ?></th>  
				</tr>
				<?php foreach ($categories as $category): ?>
					<tr>
						<td><?php echo $category['InventoryItemCategory']['name']; ?></td> 
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Contact
				<?php endif; ?>
			</div>

			<div class="clearfix">&nbsp;</div>
			<div class="related">
				<h4><?php echo __('Attachment'); ?></h4>
				<?php if (!empty($files)): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
				<tr>
					<th><?php echo __('Name'); ?></th>  
					<th><?php echo __('File'); ?></th>
					<th><?php echo __('Action'); ?></th>  
				</tr>
				<?php foreach ($files as $file): ?>
					<tr>
						<td><?php echo $file['InventorySupplierFile']['subject']; ?></td> 
						<td><?php echo $file['InventorySupplierFile']['file']; ?></td> 
						<td><?php echo $this->Html->link('<i class="fa fa-search"></i> View', '/files/inventory_supplier_file/file/'.$file['InventorySupplierFile']['dir'].'/'.$file['InventorySupplierFile']['file'], array('class' => 'btn btn-primary ', 'escape'=>false, 'target' => '_blank')); ?></td> 
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Contact
				<?php endif; ?>
			</div>
 
			<div class="related">
				<h4><?php echo __('Related Contact'); ?></h4>
				<?php if (!empty($inventorySupplier['InventorySupplierContact'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
				<tr> 
					<th><?php echo __('Name'); ?></th> 
					<th><?php echo __('Mobile Phone'); ?></th> 
					<th><?php echo __('Office'); ?></th> 
					<th><?php echo __('Email'); ?></th> 
					<th><?php echo __('Action'); ?></th>
				</tr>
				<?php foreach ($inventorySupplier['InventorySupplierContact'] as $contact): ?>
					<tr> 
						<td><?php echo $contact['name']; ?></td> 
						<td><?php echo $contact['mobile_phone']; ?></td> 
						<td><?php echo $contact['office_phone']; ?></td> 
						<td><?php echo $contact['email']; ?></td> 
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'inventory_supplier_contacts', 'action' => 'view', $contact['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'inventory_supplier_contacts', 'action' => 'edit', $contact['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'inventory_supplier_contacts', 'action' => 'delete', $contact['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($contact['name']).'"', $contact['id'])); ?>
						</td> 
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Contact
				<?php endif; ?>
			</div> 

			<div class="clearfix">&nbsp;</div>
			<div class="related">
				<h4><?php echo __('Related Address'); ?></h4>
				<?php if (!empty($inventorySupplier['InventorySupplierAddress'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
				<tr> 
					<th><?php echo __('Address'); ?></th> 
					<th><?php echo __('City'); ?></th>  
					<th><?php echo __('Phone'); ?></th> 
					<th><?php echo __('Contact Person'); ?></th> 
					<th><?php echo __('Fax'); ?></th>  
					<th><?php echo __('Email'); ?></th> 
					<th><?php echo __('Action'); ?></th>
				</tr>
				<?php foreach ($inventorySupplier['InventorySupplierAddress'] as $address): ?>
					<tr> 
						<td><?php echo $address['address']; ?></td> 
						<td><?php echo $address['city']; ?></td> 
						<td><?php echo $address['phone']; ?></td> 
						<td><?php echo $address['contact']; ?></td> 
						<td><?php echo $address['fax']; ?></td>  
						<td><?php echo $address['email']; ?></td> 
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'inventory_supplier_addresses', 'action' => 'view', $address['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'inventory_supplier_addresses', 'action' => 'edit', $address['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'inventory_supplier_addresses', 'action' => 'delete', $address['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($address['id']).'"', $address['id'])); ?>
						</td> 
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Address
				<?php endif; ?>
			</div> 

 
      	</div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript"> 
$(function() {  
  
  $('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
        },
        success: function(data) {
          console.log(data);
        }
    });
});
</script>
<?php $this->end(); ?>
