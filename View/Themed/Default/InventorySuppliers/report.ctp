
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Inventory Supplier'), array('controller' => 'inventorySuppliers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Inventory Supplier Item</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
                <div id="piechart" style="width: 900px; height: 500px;"></div>
                <div class="container table-responsive">
				<table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Item</th>
                            <th>Ordered Quantity</th>
                            <th>Rejected Quantity</th>
                            <th>Rejected Rate (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $i = 0;
                        $quantity_order = 0;
                        $quantity_reject = 0;
                        foreach ($items as $item) { $i = $i+1;
                            
                         ?>         
                        <tr>
                            <td class="text-center"><?php echo $i; ?></td>
                            <td><?php echo $item['inventory_supplier_items']['name']; ?></td>
                            <td><?php 
                            if(empty($item['inventory_supplier_items']['order'])){
                                $item['inventory_supplier_items']['order'] = 0;
                            }else{
                                $item['inventory_supplier_items']['order'] = $item['inventory_supplier_items']['order'];
                            }
                            //echo $quantity_order;
                            $order_Q = $quantity_order + $item['inventory_supplier_items']['order'];
                            //echo $order_Q;
                            echo number_format($item['inventory_supplier_items']['order'], 4);
                            
                            $quantity_order = $order_Q;
                            ?></td>
                            <td><?php 
                            if(empty($item['inventory_supplier_items']['rejected'])){
                                $item['inventory_supplier_items']['rejected'] = 0; 
                            }else{
                                $item['inventory_supplier_items']['rejected'] = $item['inventory_supplier_items']['rejected']; 
                            }

                            $reject_Q = $quantity_reject + $item['inventory_supplier_items']['rejected'];
                            echo number_format($item['inventory_supplier_items']['rejected'], 4); 

                            $quantity_reject = $reject_Q;

                            ?></td>
                            <td><?php //if($item['inventory_supplier_items']['order'] == '0' && $item['inventory_supplier_items']['rejected'] =='0') { $rate = 0; }else{ $rate = ($item['inventory_supplier_items']['rejected']/$item['inventory_supplier_items']['order'])*100; }
                            //echo number_format($rate, 2) .'%';
                            echo '0.00%'; ?></td>
                        </tr>
                    <?php   } 
                    //$quantity_order = $order_Q; 
                    $i++;
                    
                     ?>
                        <tr>
                            <td colspan="2">TOTAL</td>
                            <td><?php echo number_format($quantity_order, 4); ?><input type="hidden" id="q_order" value="<?php echo number_format($quantity_order, 4); ?>"></td>
                            <td><?php echo number_format($quantity_reject, 4); ?><input type="hidden" id="q_reject" value="<?php echo number_format($quantity_reject, 4); ?>"></td>
                            <td><?php //if($quantity_order == '0' && $quantity_reject == '0'){ $total_rate = '0'; }else{ $total_rate = ($quantity_reject/$quantity_order)*100; } 

                            //echo number_format($total_rate, 2).' %';
                            echo '0.00%'; ?></td>
                        </tr>
                    </tbody>           
                </table>
			</div>
			<div class="clearfix">&nbsp;</div>
			<?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm')); ?>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php $this->start('script'); ?>
 

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var x = parseInt($('#q_order').val());
        var y = parseInt($('#q_reject').val());
        
        if(x == 0 && y == 0){ 
            var data = google.visualization.arrayToDataTable([
            ['Task', 'Quantity Count'],
            ['Not Available', 100]
       
            ]);
        } else {
            var data = google.visualization.arrayToDataTable([
            ['Task', 'Quantity Count'],
            ['Order Quantity', x],
            ['Rejected Quantity', y],
       
            ]);
        }

        var options = {
          title: 'Rejected Rate'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
</script>
<?php $this->end(); ?>