<?php echo $this->Html->link(__('List Inventory Supplier'), array('controller' => 'inventorySuppliers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Inventory Supplier</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		
        		<div class="inventorySuppliers form">
				<?php echo $this->Form->create('InventorySupplier', array('class'=>'form-horizontal', 'type' => 'file')); ?>
					<fieldset>
						<?php echo $this->Form->input('id'); ?>
						<div class="form-group">
							<label class="col-sm-12"><h4>Business Information</h4></label> 
						</div>

						<div class="form-group">
							<label class="col-sm-3">Valid From Date *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('valid_from', array('type' => 'text', 'id' => 'dateonly', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Valid From Date')); ?>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3">Valid To Date *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('valid_to', array('type' => 'text', 'id' => 'dateonly_2', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Valid To Date')); ?>
							</div> 
						</div>
 

						<div class="form-group">
							<label class="col-sm-3">Company Name *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Company Name')); ?>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3">GST ID (Malaysian Only)</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('gst', array('class' => 'form-control', 'label' => false, 'placeholder' => 'GST ID')); ?>
							</div> 
						</div>

						<div class="form-group">
							<label class="col-sm-3">Website</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input('website', array('class' => 'form-control', 'label' => false, 'placeholder' => 'www.example.com')); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3">Company Registration Number *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('company_reg_no', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Company Registration Number')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Currency *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('general_currency_id', array('class' => 'form-control', 'label' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Company Type *</label>
							<div class="col-sm-9">
								<?php 
									$type = array(
										'Sdn Bhd' => 'Sdn Bhd',
										'Enterprise' => 'Enterprise',
										'LLP' => 'LLP',
										'Berhad' => 'Berhad',
										'International' => 'International'
									);
								echo $this->Form->input('company_type', array('options' => $type, 'empty' => 'Select Company Type', 'id' => 'company_type', 'class' => 'form-control', 'label' => false)); ?>
							</div>
						</div>
						<div class="form-group" id="expiry_date" style="display:none">
							<label class="col-sm-3">Company Reg Expiry Date *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('company_expiry_date', array('type' => 'text', 'id' => 'dateonly', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Y-m-d')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Company Registration Date *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('company_reg_date', array('type' => 'text', 'id' => 'dateonly_2', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Company Registration Date')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Business Category *</label>
							<div class="col-sm-9">
								<?php  
									$cat = array(
										'Manufacturing' => 'Manufacturing',
										'Supply' => 'Supply',
										'Services' => 'Services',
									);

									echo $this->Form->input('category', array('options' => $cat, 'class' => 'form-control', 'label' => false, 'empty' => 'Select Category')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Ownership Status *</label>
							<div class="col-sm-9">
								<?php 
								$ownership = array(
									'Local' => 'Local',
									'Joint Venture & Partnership' => 'Joint Venture & Partnership',
									'Foreign' => 'Foreign',
									);
									echo $this->Form->input('ownership_status', array('options' => $ownership, 'class' => 'form-control', 'label' => false, 'empty' => 'Select Ownership Status')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Term of Payment *</label>
							<div class="col-sm-9">
								<?php  
								echo $this->Form->input('term_of_payment_id', array('class' => 'form-control', 'label' => false, 'empty' => 'Select Term of Payment')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Credit Limit *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('credit_limit', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Credit Limit')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">No of Employee *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('no_of_employee', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Number of Employee')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Year of Business *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('years_of_business', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Year of Business')); ?>
							</div>
						</div>
						<hr/>
						<div class="form-group">
							<label class="col-sm-12"><h4>Contact Information</h4></label> 
						</div>
						<div class="form-group">
							<label class="col-sm-3">Company Address *</label>
							<div class="col-sm-9"> 
								<?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Company Address', 'type' => 'textarea')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">City *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('city', array('class' => 'form-control', 'label' => false, 'placeholder' => 'City')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Postcode *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('postcode', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Postcode')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">State *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('state_id', array('empty' => 'Select', 'class' => 'form-control', 'label' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">State (Non Malaysian)</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('state', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'placeholder' => 'State', 'required' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Country *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('country_id', array('empty' => 'Select', 'class' => 'form-control', 'label' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Phone (Office) *</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input('phone_office', array('class' => 'form-control', 'label' => false, 'placeholder' => '6032100000')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Phone (Mobile) *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('phone_mobile', array('class' => 'form-control', 'label' => false, 'placeholder' => '60123456789')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">FAX</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('fax', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Fax', 'required' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">PIC</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('pic', array('class' => 'form-control', 'label' => false, 'placeholder' => 'PIC', 'required' => true)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Email *</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Email')); ?>
							</div>
						</div>
						<hr/>
						<div class="form-group">
							<label class="col-sm-12"><h4>Additional Information</h4></label> 
						</div>
						<div class="form-group">
							<div class="col-sm-12">I acknowledge that the supply of equipments/services/works from my company does not give effect/impact on the safety, environmental and health.</div>  
							<div class="col-sm-6">
								<input type="radio" name="data[InventorySupplier][check_supply]" value="0" <?php if($this->request->data['InventorySupplier']['check_supply']==0){ ?>checked="checked" <?php } ?>> No
							</div>
							<div class="col-sm-6">
								<input type="radio" name="data[InventorySupplier][check_supply]" value="1" <?php if($this->request->data['InventorySupplier']['check_supply']==1){ ?>checked="checked" <?php } ?>> Yes
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">Compliance with applicable law.</div>  
							<div class="col-sm-6">
								<input type="radio" name="data[InventorySupplier][check_law]" value="0" <?php if($this->request->data['InventorySupplier']['check_law']==0){ ?>checked="checked" <?php } ?>> No
							</div>
							<div class="col-sm-6">
								<input type="radio" name="data[InventorySupplier][check_law]" value="1" <?php if($this->request->data['InventorySupplier']['check_law']==1){ ?>checked="checked" <?php } ?>> Yes
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">Does your company actively participate or associate in programs related to preserving the environment?</div>  
							<div class="col-sm-6">
								<input type="radio" name="data[InventorySupplier][check_program]" value="0" <?php if($this->request->data['InventorySupplier']['check_program']==0){ ?>checked="checked" <?php } ?>> No
							</div>
							<div class="col-sm-6">
								<input type="radio" name="data[InventorySupplier][check_program]" value="1" <?php if($this->request->data['InventorySupplier']['check_program']==1){ ?>checked="checked" <?php } ?>> Yes
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">If yes, please specify</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input('if_yes', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
							</div>
						</div>
						<hr/>
						<div class="form-group">
							<div class="col-sm-12">Quality Management System</div>  
							<div class="col-sm-2">
							<input type="checkbox" name="data[InventorySupplier][check_iso]" value="1" <?php if($this->request->data['InventorySupplier']['check_iso']==1){ ?>checked="checked" <?php } ?>> ISO
							</div>
							<div class="col-sm-2">
							<input type="checkbox" name="data[InventorySupplier][check_ems]" value="1" <?php if($this->request->data['InventorySupplier']['check_ems']==1){ ?>checked="checked" <?php } ?>> EMS
							</div>
							<div class="col-sm-2">
							<input type="checkbox" name="data[InventorySupplier][check_ohsas]" value="1" <?php if($this->request->data['InventorySupplier']['check_ohsas']==1){ ?>checked="checked" <?php } ?>> OHSAS
							</div>
							<div class="col-sm-2">
							<input type="checkbox" name="data[InventorySupplier][check_others]" value="1" <?php if($this->request->data['InventorySupplier']['check_others']==1){ ?>checked="checked" <?php } ?>> Others
							</div>
							<div class="col-sm-2">
							<input type="checkbox" name="data[InventorySupplier][check_none]" value="1" <?php if($this->request->data['InventorySupplier']['check_none']==1){ ?>checked="checked" <?php } ?>> None
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3">If Others, please specify</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input('others', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-12"><h4>Supplier Category</h4></label> 
						</div>
						<?php
						$cat_row = 1; 
						 foreach ($categories as $category) { ?>	
						<div class="form-group" id="removeCategory<?php echo $cat_row; ?>">
							<label class="col-sm-3" style="padding-top: 8px">Category</label>
							<div class="col-sm-8">
							<?php echo $this->Form->input("category_item.", array("empty" => "Select Category", "options"=>$cats, "class"=> "form-control", "label"=> false, "onchange"=>"showselect(this.value)", "id" =>"cat", "value"=>$category['InventorySupplierCategory']['inventory_item_category_id'])); ?>

							</div>
							<a href="#" class="btn btn-danger" onclick="removeCategory('<?php echo $cat_row; ?>'); return false"><i class="fa fa-times"></i></a>
						</div>
						<?php $cat_row++; } ?>
						<div id="loadCategory"></div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">&nbsp;</label>
							<div class="col-sm-9">
								<a href="#" id="addCategory" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Category</a>
							</div>
						</div>
						<hr/>
		<div class="form-group">
		<div class="col-sm-12">
		<h4>Attachment</h4>
		<p>Please upload Supporting Documents (PDF / Ms Words)</p>
		</div>
		</div>

		<?php if($files) { ?> 
		<?php foreach ($files as $file) { ?> 
		<div class="form-group">
			<label class="col-sm-3" style="padding-top: 8px"><?php echo $file['InventorySupplierFile']['subject']; ?></label> 
			<input type="hidden" name="data[InventorySupplierFile][<?php echo $file['InventorySupplierFile']['id']; ?>][subject]" class="form-control" value="<?php echo $file['InventorySupplierFile']['subject']; ?>"> 
			<div class="col-sm-9">
			<?php echo $this->Form->input('InventorySupplierFile.'.$file['InventorySupplierFile']['id'].'.file', array('type' => 'file', 'class' => 'form-control', 'label' => false, 'value' => $file['InventorySupplierFile']['file'] )); ?> 
			<input type="hidden" name="data[InventorySupplierFile][<?php echo $file['InventorySupplierFile']['id']; ?>][dir]" value="<?php echo $file['InventorySupplierFile']['dir']; ?>">
			<input type="hidden" name="data[InventorySupplierFile][<?php echo $file['InventorySupplierFile']['id']; ?>][id]" value="<?php echo $file['InventorySupplierFile']['id']; ?>"> 
			</div> 
		</div> 
		<?php } ?>

		<?php } else { ?>
		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">ROC Certificate</label>

		<input type="hidden" name="data[InventorySupplierFile][0][subject]" class="form-control" value="ROC Certificate">
		
		<div class="col-sm-9">
		<?php echo $this->Form->input('InventorySupplierFile.0.file', array('type' => 'file', 'class' => 'form-control', 'label' => false)); ?> 
		<input type="hidden" name="data[InventorySupplierFile][0][dir]">
		<input type="hidden" name="data[InventorySupplierFile][0][inventory_supplier_id]" value="<?php echo $this->request->data['InventorySupplier']['id']; ?>">
		<small>SSM / Form 9 / Form 13 (Malaysian)</small>
		</div>
		 
		</div> 

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">Company Profile</label>
		
		<input type="hidden" name="data[InventorySupplierFile][1][subject]" class="form-control" value="Company Profile">
	
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][1][file]" class="form-control">
		<input type="hidden" name="data[InventorySupplierFile][1][dir]">
		<input type="hidden" name="data[InventorySupplierFile][1][inventory_supplier_id]" value="<?php echo $this->request->data['InventorySupplier']['id']; ?>">
		</div>
		 
		</div> 

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">Bank Statement</label>
		
		<input type="hidden" name="data[InventorySupplierFile][2][subject]" class="form-control" value="Bank Statement">
	
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][2][file]" class="form-control">
		<input type="hidden" name="data[InventorySupplierFile][2][dir]">
		<input type="hidden" name="data[InventorySupplierFile][2][inventory_supplier_id]" value="<?php echo $this->request->data['InventorySupplier']['id']; ?>">
		<small>3 month latest bank statement</small>
		</div> 
		</div>

		<div class="form-group">
		<label class="col-sm-3" style="padding-top: 8px">Additional File (Optional)</label>
		
		<input type="hidden" name="data[InventorySupplierFile][3][subject]" class="form-control" value="Additional File">
	
		<div class="col-sm-9">
		<input type="file" name="data[InventorySupplierFile][3][file]" class="form-control">
		<input type="hidden" name="data[InventorySupplierFile][3][dir]">
		<input type="hidden" name="data[InventorySupplierFile][3][inventory_supplier_id]" value="<?php echo $this->request->data['InventorySupplier']['id']; ?>">
		</div> 
		</div>

		<hr/> 

		<?php } ?>
		<hr/> 
						<div class="form-group">
							 
							<div class="col-sm-12">
								<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>
							</div>
						</div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<?php //echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
        		<?php echo $this->Form->end(); ?>
      		</div>
    	</div>
  	</div> 
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
	var category = '<?php echo $cat_row; ?>';
	$('#addCategory').click(function() {
		
		var html = '<div id="removeCategory'+category+'">'; 
	    html += '<div class="form-group">';  
	    html += '<label class="col-sm-3" style="padding-top: 8px">Category</label>';
	    html += '<div class="col-sm-8">';
	    html += '<select name="data[InventorySupplier][category_item][]" class="form-control">';
	    html += '<option>Select Category</option>';
	    html += '<?php foreach($cats as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
	    html += '</select>';
	    html += '</div>';
	    html += '<a href="#" class="btn btn-danger" onclick="removeCategory('+category+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div></div>';    
	    $("#loadCategory").append(html);  
		category++; 
	});

	function removeCategory(row) {
    	$('#removeCategory'+row).html('');
    	return false;
    }

</script>
<?php $this->end(); ?>