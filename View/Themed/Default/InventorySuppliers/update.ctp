<div class="inventorySuppliers form">
<?php echo $this->Form->create('InventorySupplier'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Supplier'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('code');
		echo $this->Form->input('name');
		echo $this->Form->input('address');
		echo $this->Form->input('city');
		echo $this->Form->input('postcode');
		echo $this->Form->input('state_id');
		echo $this->Form->input('state');
		echo $this->Form->input('country_id');
		echo $this->Form->input('phone_office');
		echo $this->Form->input('phone_mobile');
		echo $this->Form->input('fax');
		echo $this->Form->input('email');
		echo $this->Form->input('status');
		echo $this->Form->input('valid_from');
		echo $this->Form->input('valid_to');
		echo $this->Form->input('gst');
		echo $this->Form->input('website');
		echo $this->Form->input('company_reg_no');
		echo $this->Form->input('general_currency_id');
		echo $this->Form->input('company_expiry_date');
		echo $this->Form->input('company_reg_date');
		echo $this->Form->input('category');
		echo $this->Form->input('ownership_status');
		echo $this->Form->input('term_of_payment_id');
		echo $this->Form->input('credit_limit');
		echo $this->Form->input('no_of_employee');
		echo $this->Form->input('years_of_business');
		echo $this->Form->input('others');
		echo $this->Form->input('company_type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventorySupplier.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventorySupplier.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Currencies'), array('controller' => 'general_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Currency'), array('controller' => 'general_currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Term Of Payments'), array('controller' => 'term_of_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Term Of Payment'), array('controller' => 'term_of_payments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Order Items'), array('controller' => 'inventory_delivery_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('controller' => 'inventory_delivery_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Orders'), array('controller' => 'inventory_delivery_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order'), array('controller' => 'inventory_delivery_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Order Items'), array('controller' => 'inventory_purchase_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order Item'), array('controller' => 'inventory_purchase_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Orders'), array('controller' => 'inventory_purchase_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order'), array('controller' => 'inventory_purchase_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisition Items'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Item'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Addresses'), array('controller' => 'inventory_supplier_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Address'), array('controller' => 'inventory_supplier_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Categories'), array('controller' => 'inventory_supplier_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Category'), array('controller' => 'inventory_supplier_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Contacts'), array('controller' => 'inventory_supplier_contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Contact'), array('controller' => 'inventory_supplier_contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Files'), array('controller' => 'inventory_supplier_files', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier File'), array('controller' => 'inventory_supplier_files', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Items'), array('controller' => 'inventory_supplier_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Item'), array('controller' => 'inventory_supplier_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('controller' => 'planning_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
