<div class="inventorySuppliers view">
<h2><?php echo __('Inventory Supplier'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Postcode'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['postcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplier['State']['name'], array('controller' => 'states', 'action' => 'view', $inventorySupplier['State']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplier['Country']['name'], array('controller' => 'countries', 'action' => 'view', $inventorySupplier['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone Office'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['phone_office']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone Mobile'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['phone_mobile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['fax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Valid From'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['valid_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Valid To'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['valid_to']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gst'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['gst']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Website'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['website']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Reg No'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['company_reg_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Currency'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplier['GeneralCurrency']['name'], array('controller' => 'general_currencies', 'action' => 'view', $inventorySupplier['GeneralCurrency']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Expiry Date'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['company_expiry_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Reg Date'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['company_reg_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['category']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ownership Status'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['ownership_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Term Of Payment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplier['TermOfPayment']['name'], array('controller' => 'term_of_payments', 'action' => 'view', $inventorySupplier['TermOfPayment']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Credit Limit'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['credit_limit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Employee'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['no_of_employee']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Years Of Business'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['years_of_business']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Others'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['others']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company Type'); ?></dt>
		<dd>
			<?php echo h($inventorySupplier['InventorySupplier']['company_type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Supplier'), array('action' => 'edit', $inventorySupplier['InventorySupplier']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Supplier'), array('action' => 'delete', $inventorySupplier['InventorySupplier']['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplier['InventorySupplier']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Currencies'), array('controller' => 'general_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Currency'), array('controller' => 'general_currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Term Of Payments'), array('controller' => 'term_of_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Term Of Payment'), array('controller' => 'term_of_payments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Order Items'), array('controller' => 'inventory_delivery_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('controller' => 'inventory_delivery_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Orders'), array('controller' => 'inventory_delivery_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order'), array('controller' => 'inventory_delivery_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Order Items'), array('controller' => 'inventory_purchase_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order Item'), array('controller' => 'inventory_purchase_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Orders'), array('controller' => 'inventory_purchase_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order'), array('controller' => 'inventory_purchase_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisition Items'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Item'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Addresses'), array('controller' => 'inventory_supplier_addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Address'), array('controller' => 'inventory_supplier_addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Categories'), array('controller' => 'inventory_supplier_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Category'), array('controller' => 'inventory_supplier_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Contacts'), array('controller' => 'inventory_supplier_contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Contact'), array('controller' => 'inventory_supplier_contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Files'), array('controller' => 'inventory_supplier_files', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier File'), array('controller' => 'inventory_supplier_files', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Items'), array('controller' => 'inventory_supplier_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Item'), array('controller' => 'inventory_supplier_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('controller' => 'planning_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Delivery Order Items'); ?></h3>
	<?php if (!empty($inventorySupplier['InventoryDeliveryOrderItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Delivery Order Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Quantity Delivered'); ?></th>
		<th><?php echo __('Quantity Remaining'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventoryDeliveryOrderItem'] as $inventoryDeliveryOrderItem): ?>
		<tr>
			<td><?php echo $inventoryDeliveryOrderItem['id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_delivery_order_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['quantity']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['remark']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_item_category_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['price_per_unit']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['amount']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['discount']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['quantity_delivered']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['quantity_remaining']; ?></td>
			<td><?php echo $inventoryDeliveryOrderItem['inventory_supplier_item_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_delivery_order_items', 'action' => 'view', $inventoryDeliveryOrderItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_delivery_order_items', 'action' => 'edit', $inventoryDeliveryOrderItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_delivery_order_items', 'action' => 'delete', $inventoryDeliveryOrderItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryDeliveryOrderItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('controller' => 'inventory_delivery_order_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Delivery Orders'); ?></h3>
	<?php if (!empty($inventorySupplier['InventoryDeliveryOrder'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('General Purchase Requisition Type Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Dateline'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Po No'); ?></th>
		<th><?php echo __('Account Department Budget Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Term Of Payment Id'); ?></th>
		<th><?php echo __('Remark Id'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Inventory Delivery Location Id'); ?></th>
		<th><?php echo __('Completed Status'); ?></th>
		<th><?php echo __('Do No'); ?></th>
		<th><?php echo __('Warranty Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventoryDeliveryOrder'] as $inventoryDeliveryOrder): ?>
		<tr>
			<td><?php echo $inventoryDeliveryOrder['id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['user_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['group_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['general_purchase_requisition_type_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['created']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['modified']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['dateline']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['status']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['po_no']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['account_department_budget_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['term_of_payment_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['remark_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['notes']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['inventory_delivery_location_id']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['completed_status']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['do_no']; ?></td>
			<td><?php echo $inventoryDeliveryOrder['warranty_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_delivery_orders', 'action' => 'view', $inventoryDeliveryOrder['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_delivery_orders', 'action' => 'edit', $inventoryDeliveryOrder['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_delivery_orders', 'action' => 'delete', $inventoryDeliveryOrder['id']), array(), __('Are you sure you want to delete # %s?', $inventoryDeliveryOrder['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Delivery Order'), array('controller' => 'inventory_delivery_orders', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Purchase Order Items'); ?></h3>
	<?php if (!empty($inventorySupplier['InventoryPurchaseOrderItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Purchase Order Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Discount Type'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventoryPurchaseOrderItem'] as $inventoryPurchaseOrderItem): ?>
		<tr>
			<td><?php echo $inventoryPurchaseOrderItem['id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_purchase_order_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['quantity']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_supplier_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_item_category_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['price_per_unit']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['amount']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['discount']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['discount_type']; ?></td>
			<td><?php echo $inventoryPurchaseOrderItem['tax']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_purchase_order_items', 'action' => 'view', $inventoryPurchaseOrderItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_purchase_order_items', 'action' => 'edit', $inventoryPurchaseOrderItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_purchase_order_items', 'action' => 'delete', $inventoryPurchaseOrderItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryPurchaseOrderItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Purchase Order Item'), array('controller' => 'inventory_purchase_order_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Purchase Orders'); ?></h3>
	<?php if (!empty($inventorySupplier['InventoryPurchaseOrder'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('General Purchase Requisition Type Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Dateline'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Po No'); ?></th>
		<th><?php echo __('Account Department Budget Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Term Of Payment Id'); ?></th>
		<th><?php echo __('Remark Id'); ?></th>
		<th><?php echo __('Notes'); ?></th>
		<th><?php echo __('Purchase Status'); ?></th>
		<th><?php echo __('Pic'); ?></th>
		<th><?php echo __('Inventory Delivery Location Id'); ?></th>
		<th><?php echo __('Warranty Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventoryPurchaseOrder'] as $inventoryPurchaseOrder): ?>
		<tr>
			<td><?php echo $inventoryPurchaseOrder['id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['user_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['group_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['general_purchase_requisition_type_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['created']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['modified']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['dateline']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['status']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['po_no']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['account_department_budget_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['term_of_payment_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['remark_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['notes']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['purchase_status']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['pic']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['inventory_delivery_location_id']; ?></td>
			<td><?php echo $inventoryPurchaseOrder['warranty_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_purchase_orders', 'action' => 'view', $inventoryPurchaseOrder['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_purchase_orders', 'action' => 'edit', $inventoryPurchaseOrder['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_purchase_orders', 'action' => 'delete', $inventoryPurchaseOrder['id']), array(), __('Are you sure you want to delete # %s?', $inventoryPurchaseOrder['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Purchase Order'), array('controller' => 'inventory_purchase_orders', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Purchase Requisition Items'); ?></h3>
	<?php if (!empty($inventorySupplier['InventoryPurchaseRequisitionItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Purchase Requisition Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Discount Type'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventoryPurchaseRequisitionItem'] as $inventoryPurchaseRequisitionItem): ?>
		<tr>
			<td><?php echo $inventoryPurchaseRequisitionItem['id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_purchase_requisition_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['quantity']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['general_unit_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_supplier_item_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_item_category_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['price_per_unit']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['amount']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['discount']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['discount_type']; ?></td>
			<td><?php echo $inventoryPurchaseRequisitionItem['tax']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'view', $inventoryPurchaseRequisitionItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'edit', $inventoryPurchaseRequisitionItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'delete', $inventoryPurchaseRequisitionItem['id']), array(), __('Are you sure you want to delete # %s?', $inventoryPurchaseRequisitionItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Item'), array('controller' => 'inventory_purchase_requisition_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Supplier Addresses'); ?></h3>
	<?php if (!empty($inventorySupplier['InventorySupplierAddress'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('Postcode'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('State Id'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Fax'); ?></th>
		<th><?php echo __('Contact'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventorySupplierAddress'] as $inventorySupplierAddress): ?>
		<tr>
			<td><?php echo $inventorySupplierAddress['id']; ?></td>
			<td><?php echo $inventorySupplierAddress['inventory_supplier_id']; ?></td>
			<td><?php echo $inventorySupplierAddress['address']; ?></td>
			<td><?php echo $inventorySupplierAddress['postcode']; ?></td>
			<td><?php echo $inventorySupplierAddress['city']; ?></td>
			<td><?php echo $inventorySupplierAddress['state_id']; ?></td>
			<td><?php echo $inventorySupplierAddress['state']; ?></td>
			<td><?php echo $inventorySupplierAddress['code']; ?></td>
			<td><?php echo $inventorySupplierAddress['phone']; ?></td>
			<td><?php echo $inventorySupplierAddress['fax']; ?></td>
			<td><?php echo $inventorySupplierAddress['contact']; ?></td>
			<td><?php echo $inventorySupplierAddress['email']; ?></td>
			<td><?php echo $inventorySupplierAddress['country_id']; ?></td>
			<td><?php echo $inventorySupplierAddress['created']; ?></td>
			<td><?php echo $inventorySupplierAddress['modified']; ?></td>
			<td><?php echo $inventorySupplierAddress['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_supplier_addresses', 'action' => 'view', $inventorySupplierAddress['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_supplier_addresses', 'action' => 'edit', $inventorySupplierAddress['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_supplier_addresses', 'action' => 'delete', $inventorySupplierAddress['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierAddress['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Supplier Address'), array('controller' => 'inventory_supplier_addresses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Supplier Categories'); ?></h3>
	<?php if (!empty($inventorySupplier['InventorySupplierCategory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Inventory Item Category Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventorySupplierCategory'] as $inventorySupplierCategory): ?>
		<tr>
			<td><?php echo $inventorySupplierCategory['id']; ?></td>
			<td><?php echo $inventorySupplierCategory['inventory_supplier_id']; ?></td>
			<td><?php echo $inventorySupplierCategory['inventory_item_category_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_supplier_categories', 'action' => 'view', $inventorySupplierCategory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_supplier_categories', 'action' => 'edit', $inventorySupplierCategory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_supplier_categories', 'action' => 'delete', $inventorySupplierCategory['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierCategory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Supplier Category'), array('controller' => 'inventory_supplier_categories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Supplier Contacts'); ?></h3>
	<?php if (!empty($inventorySupplier['InventorySupplierContact'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Search Key'); ?></th>
		<th><?php echo __('Position'); ?></th>
		<th><?php echo __('Office Phone'); ?></th>
		<th><?php echo __('Mobile Phone'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventorySupplierContact'] as $inventorySupplierContact): ?>
		<tr>
			<td><?php echo $inventorySupplierContact['id']; ?></td>
			<td><?php echo $inventorySupplierContact['code']; ?></td>
			<td><?php echo $inventorySupplierContact['inventory_supplier_id']; ?></td>
			<td><?php echo $inventorySupplierContact['name']; ?></td>
			<td><?php echo $inventorySupplierContact['search_key']; ?></td>
			<td><?php echo $inventorySupplierContact['position']; ?></td>
			<td><?php echo $inventorySupplierContact['office_phone']; ?></td>
			<td><?php echo $inventorySupplierContact['mobile_phone']; ?></td>
			<td><?php echo $inventorySupplierContact['email']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_supplier_contacts', 'action' => 'view', $inventorySupplierContact['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_supplier_contacts', 'action' => 'edit', $inventorySupplierContact['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_supplier_contacts', 'action' => 'delete', $inventorySupplierContact['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierContact['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Supplier Contact'), array('controller' => 'inventory_supplier_contacts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Supplier Files'); ?></h3>
	<?php if (!empty($inventorySupplier['InventorySupplierFile'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Dir'); ?></th>
		<th><?php echo __('File'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventorySupplierFile'] as $inventorySupplierFile): ?>
		<tr>
			<td><?php echo $inventorySupplierFile['id']; ?></td>
			<td><?php echo $inventorySupplierFile['inventory_supplier_id']; ?></td>
			<td><?php echo $inventorySupplierFile['dir']; ?></td>
			<td><?php echo $inventorySupplierFile['file']; ?></td>
			<td><?php echo $inventorySupplierFile['type']; ?></td>
			<td><?php echo $inventorySupplierFile['status']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_supplier_files', 'action' => 'view', $inventorySupplierFile['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_supplier_files', 'action' => 'edit', $inventorySupplierFile['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_supplier_files', 'action' => 'delete', $inventorySupplierFile['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierFile['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Supplier File'), array('controller' => 'inventory_supplier_files', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Supplier Items'); ?></h3>
	<?php if (!empty($inventorySupplier['InventorySupplierItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Price Per Unit'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Conversion Unit Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Effective Date'); ?></th>
		<th><?php echo __('Expiry Date'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('General Currency Id'); ?></th>
		<th><?php echo __('Is Default'); ?></th>
		<th><?php echo __('Qc Inspect'); ?></th>
		<th><?php echo __('Min Order'); ?></th>
		<th><?php echo __('Min Order Unit'); ?></th>
		<th><?php echo __('Conversion Qty'); ?></th>
		<th><?php echo __('Part Number'); ?></th>
		<th><?php echo __('Lead Time'); ?></th>
		<th><?php echo __('Quotation Number'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['InventorySupplierItem'] as $inventorySupplierItem): ?>
		<tr>
			<td><?php echo $inventorySupplierItem['id']; ?></td>
			<td><?php echo $inventorySupplierItem['inventory_item_id']; ?></td>
			<td><?php echo $inventorySupplierItem['price']; ?></td>
			<td><?php echo $inventorySupplierItem['price_per_unit']; ?></td>
			<td><?php echo $inventorySupplierItem['general_unit_id']; ?></td>
			<td><?php echo $inventorySupplierItem['conversion_unit_id']; ?></td>
			<td><?php echo $inventorySupplierItem['created']; ?></td>
			<td><?php echo $inventorySupplierItem['modified']; ?></td>
			<td><?php echo $inventorySupplierItem['effective_date']; ?></td>
			<td><?php echo $inventorySupplierItem['expiry_date']; ?></td>
			<td><?php echo $inventorySupplierItem['remark']; ?></td>
			<td><?php echo $inventorySupplierItem['inventory_supplier_id']; ?></td>
			<td><?php echo $inventorySupplierItem['general_currency_id']; ?></td>
			<td><?php echo $inventorySupplierItem['is_default']; ?></td>
			<td><?php echo $inventorySupplierItem['qc_inspect']; ?></td>
			<td><?php echo $inventorySupplierItem['min_order']; ?></td>
			<td><?php echo $inventorySupplierItem['min_order_unit']; ?></td>
			<td><?php echo $inventorySupplierItem['conversion_qty']; ?></td>
			<td><?php echo $inventorySupplierItem['part_number']; ?></td>
			<td><?php echo $inventorySupplierItem['lead_time']; ?></td>
			<td><?php echo $inventorySupplierItem['quotation_number']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_supplier_items', 'action' => 'view', $inventorySupplierItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_supplier_items', 'action' => 'edit', $inventorySupplierItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_supplier_items', 'action' => 'delete', $inventorySupplierItem['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Supplier Item'), array('controller' => 'inventory_supplier_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Planning Items'); ?></h3>
	<?php if (!empty($inventorySupplier['PlanningItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Planning Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Buffer'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('Planning Qty'); ?></th>
		<th><?php echo __('Planning General Unit'); ?></th>
		<th><?php echo __('Inventory Supplier Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Inventory Supplier Item Id'); ?></th>
		<th><?php echo __('Purchased Quantity'); ?></th>
		<th><?php echo __('Balance'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventorySupplier['PlanningItem'] as $planningItem): ?>
		<tr>
			<td><?php echo $planningItem['id']; ?></td>
			<td><?php echo $planningItem['planning_id']; ?></td>
			<td><?php echo $planningItem['inventory_item_id']; ?></td>
			<td><?php echo $planningItem['quantity']; ?></td>
			<td><?php echo $planningItem['general_unit_id']; ?></td>
			<td><?php echo $planningItem['buffer']; ?></td>
			<td><?php echo $planningItem['total']; ?></td>
			<td><?php echo $planningItem['planning_qty']; ?></td>
			<td><?php echo $planningItem['planning_general_unit']; ?></td>
			<td><?php echo $planningItem['inventory_supplier_id']; ?></td>
			<td><?php echo $planningItem['created']; ?></td>
			<td><?php echo $planningItem['modified']; ?></td>
			<td><?php echo $planningItem['remark']; ?></td>
			<td><?php echo $planningItem['status']; ?></td>
			<td><?php echo $planningItem['inventory_supplier_item_id']; ?></td>
			<td><?php echo $planningItem['purchased_quantity']; ?></td>
			<td><?php echo $planningItem['balance']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'planning_items', 'action' => 'view', $planningItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'planning_items', 'action' => 'edit', $planningItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'planning_items', 'action' => 'delete', $planningItem['id']), array(), __('Are you sure you want to delete # %s?', $planningItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
