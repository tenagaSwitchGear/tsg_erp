<div class="row"> 
  	<div class="col-xs-12">
  		
        <?php echo $this->Html->link(__('Active Supplier'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>

        <?php echo $this->Html->link(__('Inactive Supplier'), array('action' => 'index?status=Inactive'), array('class' => 'btn btn-warning btn-sm')); ?> 

        <?php echo $this->Html->link(__('List Supplier Addresses'), array('controller' => 'inventory_supplier_addresses', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('List Supplier Contact Person'), array('controller' => 'inventory_supplier_contacts', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('Price Books'), array('controller' => 'inventory_supplier_items', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
 
        
        <?php echo $this->Html->link(__('Add Supplier'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
 
        <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'index?status=Waiting Approval'), array('class' => 'btn btn-primary btn-sm')); ?> 
  		<div class="x_panel tile"> 
      		<div class="x_title">
        		<h2><?php echo __('Suppliers'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

        	<?php echo $this->Form->create('InventorySupplier', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Company Name', 'class' => 'form-control', 'label' => false, 'id' => 'findProduct', 'required' => false)); ?>
		</td>
		<td><?php echo $this->Form->input('code', array('type' => 'text', 'placeholder' => 'Code', 'class' => 'form-control', 'label' => false, 'id' => 'username', 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'Expiry From', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly', 'required' => false)); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'Expiry To', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly_2', 'required' => false)); ?></td>

		<td><?php echo $this->Form->input('status', array('options' => $status, 'empty' => '-All Status-', 'class' => 'form-control', 'label' => false, 'required' => false)); ?></td>
<td><?php echo $this->Form->input('category', array('empty' => '-All Category-', 'options' => $categories, 'name' => 'category', 'class' => 'form-control', 'required' => false, 'label' => false)); ?></td> 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 

		</tr>
	</table>
	<?php echo $this->Form->end(); ?> 

        	<!-- content start-->
        	<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
				 
				<tr> 
					<th><?php echo $this->Paginator->sort('code'); ?></th> 
					<th><?php echo $this->Paginator->sort('name'); ?></th> 
					<th><?php echo $this->Paginator->sort('status'); ?></th> 
					<th><?php echo $this->Paginator->sort('valid_from'); ?></th>
					<th><?php echo $this->Paginator->sort('valid_to'); ?></th>
					<th>Contact</th> 
					
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				 
				<?php
					$currentPage = empty($this->Paginator->params['paging']['InventorySupplier']['page']) ? 1 : $this->Paginator->params['paging']['InventorySupplier']['page']; $limit = $this->Paginator->params['paging']['InventorySupplier']['limit'];
					$startSN = (($currentPage * $limit) + 1) - $limit;

					foreach ($inventorySuppliers as $inventorySupplier):
				?>
				<tr> 
					<td><?php echo h($inventorySupplier['InventorySupplier']['code']); ?> </td>
					<td><?php echo h($inventorySupplier['InventorySupplier']['name']); ?> </td>
				 
					<td><?php echo h($inventorySupplier['InventorySupplier']['status']); ?>&nbsp;</td> 

					<td><?php echo h($inventorySupplier['InventorySupplier']['valid_from']); ?>&nbsp;</td> 

					<td><?php echo h($inventorySupplier['InventorySupplier']['valid_to']); ?>&nbsp;</td> 

					<td>Office: <?php echo h($inventorySupplier['InventorySupplier']['phone_office']); ?><br/>
						Mobile: <?php echo h($inventorySupplier['InventorySupplier']['phone_mobile']); ?><br/>
						Fax: <?php echo h($inventorySupplier['InventorySupplier']['fax']); ?><br/>
						Email: <?php echo h($inventorySupplier['InventorySupplier']['email']); ?>
					</td>
					 
					<td>
						<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventorySupplier['InventorySupplier']['name']).'"', $inventorySupplier['InventorySupplier']['id'])); ?>
                        <?php echo $this->Form->postLink('<i class="fa fa-line-chart"></i>', array('action' => 'report', $inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?>
                        <?php echo $this->Form->postLink('<i class="fa fa-list-ol"></i>', array('action' => 'item_list', $inventorySupplier['InventorySupplier']['id']), array('class' => 'btn btn-primary btn-circle-sm', 'escape'=>false)); ?>
					</td>
				</tr>
			<?php endforeach; ?>
				 
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(function() { 
    $('#dateonly2').datepicker({
      dateFormat: 'yy-mm-dd', 
      ampm: true
    }); 
});  
</script>
<?php $this->end(); ?>