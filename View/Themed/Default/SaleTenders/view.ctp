<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php //echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php //echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add', $saleTender['SaleTender']['id']), array('class' => 'btn btn-success btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Sale Tender'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="container table-responsive">
				<dl> 
					<dt class="col-sm-3"><?php echo __('Customer'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['Customer']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Sale Tender Type'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTenderType']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Title'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['title']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Tender No'); ?></dt>
					<dd class="col-sm-9">
						: #<?php echo h($saleTender['SaleTender']['tender_no']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Detail'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['detail']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Specification'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['specification']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Closing Date'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['closing_date']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Price'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['price']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Selling Price'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['estimate_price']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['created']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['modified']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Remark'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['remark']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Extend Date'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['SaleTender']['extend_date']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Contact Person'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['CustomerContactPerson']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Contact Person Phone (Office)'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['CustomerContactPerson']['office_phone']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Contact Person Phone (Mobile)'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['CustomerContactPerson']['mobile_phone']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Contact Person Email'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($saleTender['CustomerContactPerson']['email']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>
			 
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
