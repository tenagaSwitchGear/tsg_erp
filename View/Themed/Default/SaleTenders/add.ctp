<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Customer'), array('controller' => 'customers','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Sale Tender'), array('controller' => 'sale_tenders','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Sale Quotation'), array('controller' => 'sale_quotations','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Sale Tender</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		<div class="saleTenders form">
				<?php echo $this->Form->create('SaleTender', array('class'=>'form-horizontal')); ?>
					<fieldset>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Customer</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("customer_id", array("empty" => "-Select Customer", "class"=> "form-control", "label"=> false, "id" => "customer")); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Contact Person</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("customer_contact_person_id", array("empty" => "-Select Contact Person", "class"=> "form-control", "label"=> false, "id" => "contact", 'required' => false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Sale Tender Type</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("sale_tender_type_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Title</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("title", array("class"=> "form-control", "placeholder"=>"Title", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Tender No</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("tender_no", array("class"=> "form-control", "placeholder"=>"Tender No", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Detail</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("detail", array("type" => "textarea", "class"=> "form-control", "placeholder"=>"Detail", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Specification</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("specification", array("class"=> "form-control", "placeholder"=>"Specification", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Closing Date</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("closing_date", array('type' => 'text', 'id'=>'datepicker', "class" => "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Tender Fees (Optional)</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("price", array("type" => "text", "class"=> "form-control", "placeholder"=>"Price", "label"=> false, 'required' => false)); ?>
							</div> 
						</div>

						 
						 

						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Remark (Optional)</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("remark", array("type" => "textarea", "class"=> "form-control", "placeholder"=>"Remark", "label"=> false, 'required' => false)); ?>
							</div> 
						</div>
 
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-lg')); ?>
				</div>
        		<!-- content end -->
        		<?php $this->Form->end(); ?>
      		</div>
    	</div>
  	</div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(function() {  
	$('#customer').on('change', function() {
		$.ajax({
	        type: "GET",                        
	        url:baseUrl + 'customer_contact_persons/ajaxfindcontact',           
	        contentType: "application/json",
	        dataType: "json",
	        data: "term=" + $('#customer').val(),                                                    
	        success: function (json) { 

	        	var html = '';
	            $.each(json, function (i, data) {
	            	console.log(data);
	  				html += '<option value="'+data.CustomerContactPerson.id+'">'+data.CustomerContactPerson.name+'</option>';
				});
				$('#contact').html(html);
	        }
	    }); 	
	});
    
});

</script>
<?php $this->end(); ?>