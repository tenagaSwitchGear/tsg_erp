<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Enquiry / Tender'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?> 
 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customer Enquiry / Tender</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
            <?php echo $this->Form->create('SaleTender', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
        	<table cellpadding="0" cellspacing="0" class="table">
        		<tr>
        		<td><?php echo $this->Form->input('tender_no', array('placeholder' => 'Tender No', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
        		</td>
        		<td><?php echo $this->Form->input('customer', array('placeholder' => 'Customer', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>
        		 
        		</td>
        		 <td><?php echo $this->Form->input('title', array('placeholder' => 'Tender title', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>
        		 
        		</td>
        		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
        		</tr>
        	</table>
	       <?php echo $this->Form->end(); ?>

        	<div class="table-responsive">
				<table class="table table-hover table-bordered">
				<thead>
				<tr>
						<th><?php echo $this->Paginator->sort('tender_no'); ?></th> 
						<th><?php echo $this->Paginator->sort('customer_id', 'Customer'); ?></th>
						<th><?php echo $this->Paginator->sort('closing_date'); ?></th>
						<th><?php echo $this->Paginator->sort('price', 'Doc Price'); ?></th>
						<!--<th><?php echo $this->Paginator->sort('extend_date'); ?></th>-->
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
					foreach ($saleTenders as $saleTender):
				?>
				<tr> 
					<td><?php echo h($saleTender['SaleTender']['tender_no']); ?>&nbsp;</td> 
					<td><?php echo h($saleTender['Customer']['name']); ?>&nbsp;</td> 
					<td><?php echo h($saleTender['SaleTender']['closing_date']); ?>&nbsp;</td>
					<td><?php echo number_format(h($saleTender['SaleTender']['price']), 2); ?>&nbsp;</td> 
                    <td class="actions">
                        <?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $saleTender['SaleTender']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
                        <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $saleTender['SaleTender']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
                        <?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $saleTender['SaleTender']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete?', $saleTender['SaleTender']['id'])); ?>
                    </td>
				</tr>
                <?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>

        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

