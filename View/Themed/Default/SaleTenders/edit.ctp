<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customers Files'), array('controller' => 'customer_files', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Sale Tender</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
				<div class="saleTenders form">
				<?php echo $this->Form->create('SaleTender', array('class'=>'form-horizontal')); ?>
					<fieldset>
						<?php echo $this->Form->input('id'); ?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Customer Id</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("customer_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Sale Tender Type</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("sale_tender_type_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Title</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("title", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Tender No</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("tender_no", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Detail</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("detail", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Specification</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("specification", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Closing Date</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("closing_date", array('type' => 'text', 'id'=>'datepicker', "class" => "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Price</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("price", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Remark</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("remark", array("type" => "textarea", "class"=> "form-control", "label"=> false, "required" => "false")); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Extend Date</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("extend_date", array('type' => 'text', 'id'=>'datepicker_2', "class" => "form-control", "label"=> false)); ?>
							</div> 
						</div>
					<?php
						//echo $this->Form->input('id');
						//echo $this->Form->input('customer_id');
						//echo $this->Form->input('sale_tender_type_id');
						//echo $this->Form->input('title');
						//echo $this->Form->input('tender_no');
						//echo $this->Form->input('detail');
						//echo $this->Form->input('specification');
						//echo $this->Form->input('closing_date');
						//echo $this->Form->input('price');
						//echo $this->Form->input('user_id');
						//echo $this->Form->input('remark');
						//echo $this->Form->input('extend_date');
						//echo $this->Form->input('status');
					?>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>
