<div class="projectManpowers form">
<?php echo $this->Form->create('ProjectManpower'); ?>
	<fieldset>
		<legend><?php echo __('Edit Project Manpower'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('price_per_hour');
		echo $this->Form->input('price_ot_per_hour');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProjectManpower.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProjectManpower.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Project Manpowers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedule Assigns'), array('controller' => 'project_schedule_assigns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('controller' => 'project_schedule_assigns', 'action' => 'add')); ?> </li>
	</ul>
</div>
