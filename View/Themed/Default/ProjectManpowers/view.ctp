<div class="projectManpowers view">
<h2><?php echo __('Project Manpower'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectManpower['ProjectManpower']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectManpower['User']['id'], array('controller' => 'users', 'action' => 'view', $projectManpower['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Per Hour'); ?></dt>
		<dd>
			<?php echo h($projectManpower['ProjectManpower']['price_per_hour']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Ot Per Hour'); ?></dt>
		<dd>
			<?php echo h($projectManpower['ProjectManpower']['price_ot_per_hour']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project Manpower'), array('action' => 'edit', $projectManpower['ProjectManpower']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project Manpower'), array('action' => 'delete', $projectManpower['ProjectManpower']['id']), array(), __('Are you sure you want to delete # %s?', $projectManpower['ProjectManpower']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Manpowers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Manpower'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedule Assigns'), array('controller' => 'project_schedule_assigns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('controller' => 'project_schedule_assigns', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Project Schedule Assigns'); ?></h3>
	<?php if (!empty($projectManpower['ProjectScheduleAssign'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Project Schedule Child Id'); ?></th>
		<th><?php echo __('Project Manpower Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($projectManpower['ProjectScheduleAssign'] as $projectScheduleAssign): ?>
		<tr>
			<td><?php echo $projectScheduleAssign['id']; ?></td>
			<td><?php echo $projectScheduleAssign['project_schedule_child_id']; ?></td>
			<td><?php echo $projectScheduleAssign['project_manpower_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'project_schedule_assigns', 'action' => 'view', $projectScheduleAssign['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'project_schedule_assigns', 'action' => 'edit', $projectScheduleAssign['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'project_schedule_assigns', 'action' => 'delete', $projectScheduleAssign['id']), array(), __('Are you sure you want to delete # %s?', $projectScheduleAssign['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('controller' => 'project_schedule_assigns', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
