<div class="row"> 
  	<div class="col-xs-12">
  		
  		<?php echo $this->Html->link(__('Add New Contact'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
  		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventorySupplierContact['InventorySupplierContact']['id']), array('class' => 'btn btn-warning')); ?> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Supplier Contact</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

        	<div class="inventorySupplierContacts view-data">
<h2><?php echo __('Inventory Supplier Contact'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Supplier'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierContact['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $inventorySupplierContact['InventorySupplier']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Search Key'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['search_key']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Position'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['position']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Office Phone'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['office_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobile Phone'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['mobile_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierContact['InventorySupplierContact']['email']); ?>
			&nbsp;
		</dd>
	</dl>
</div>


</div>
</div>
</div>
</div>