<div class="row"> 
  	<div class="col-xs-12">
  		
  		<?php echo $this->Html->link(__('Add New Contact'), array('action' => 'add'), array('class' => 'btn btn-success')); ?> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Inventory Supplier</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventorySupplierContacts form">
<?php echo $this->Form->create('InventorySupplierContact', array('class' => 'form-horizontal')); ?>
 	

		<?php echo $this->Form->input('id'); ?>
	<div class="form-group">
		<label class="col-sm-3">Code</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('code', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Supplier</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('inventory_supplier_id', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Name</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('name', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Search Key</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('search_key', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Position</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('position', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Phone (Office)</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('office_phone', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Phone (Mobile)</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('mobile_phone', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Email</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('email', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"></label>
		<div class="col-sm-9"> 
		<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success')); ?>
		
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>