<div class="planningItems view">
<h2><?php echo __('Planning Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Planning'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItem['Planning']['name'], array('controller' => 'plannings', 'action' => 'view', $planningItem['Planning']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $planningItem['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $planningItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Planning Quantity'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['planning_quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Planning General Unit'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['planning_general_unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Supplier'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItem['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $planningItem['InventorySupplier']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($planningItem['PlanningItem']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Planning Item'), array('action' => 'edit', $planningItem['PlanningItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Planning Item'), array('action' => 'delete', $planningItem['PlanningItem']['id']), array(), __('Are you sure you want to delete # %s?', $planningItem['PlanningItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plannings'), array('controller' => 'plannings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning'), array('controller' => 'plannings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
