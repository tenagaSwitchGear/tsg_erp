<?php echo $this->Html->link('Cancel Editing', array('controller' => 'plannings', 'action' => 'view', $item['Planning']['id']), array('class' => 'btn btn-warning')); ?>  

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Partial Delivery: <?php echo $item['InventoryItem']['code']; ?> (<?php echo $item['InventoryItem']['name']; ?>)</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

 
<?php echo $this->Form->create('PlanningPartialDelivery', array('class' => 'form-horizontal')); ?> 

		<div class="form-group">
			<label class="col-sm-3">Quantity To Purchase*</label>
			<div class="col-sm-9">
				<?php  
					$qty = $item['PlanningItem']['planning_qty'] + $item['PlanningItem']['buffer']; 
				 	echo _n2($qty) . ' ' . $item['GeneralUnit']['name'];
				?>
				<input type="hidden" id="total" value="<?php echo $qty; ?>">
			</div>
		</div>
 



				<div class="form-group">
					<h4>Add Partial Delivery</h4> 
				</div>

<?php 
$i = 1;
if($partials) { 
	
foreach($partials as $partial) { ?> 
	<div class="form-group" id="removeBom<?php echo $i; ?>">
	<div class="col-sm-3">
	<input type="text" value="<?php echo $partial['PlanningPartialDelivery']['quantity']; ?>" name="data[PlanningPartialDelivery][quantity][]" class="form-control quantity" placeholder="Quantity"required>
	</div>
	<div class="col-sm-3">
	<input type="text" value="<?php echo $partial['PlanningPartialDelivery']['delivery']; ?>" name="data[PlanningPartialDelivery][delivery][]" id="del<?php echo $i; ?>" class="datepicker2 form-control" placeholder="Delivery Date"required>
	</div>
	<div class="col-sm-5">
	<textarea name="data[PlanningPartialDelivery][remark][]" class="form-control" placeholder="Remark / Note (Optional)"><?php echo $partial['PlanningPartialDelivery']['remark']; ?></textarea>
	</div>
	<div class="col-sm-1">
	<a href="#" class="btn btn-danger" onclick="removeBom(<?php echo $i; ?>); return false"><i class="fa fa-times"></i></a>
	</div>
	</div>
<?php 
$i++;
} 

} ?>

				<div id="output"></div>

				<div class="form-group"><div id="error"></div></div>

				<div class="form-group"> 
		            <div class="col-sm-12">
		                <a href="#" id="addMore" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Partial Delivery</a>
		            </div>
		        </div>

				<div class="form-group">
				<label class="col-sm-3">&nbsp;</label>
				<div class="col-sm-9">
					<?php echo $this->Form->button('Submit', array('id' => 'submit', 'class' => 'btn btn-success pull-right')); ?>
				</div>
			</div> 
			 
		<?php echo $this->Form->end(); ?>
		</div>
		 
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">

 

    function removeBom(id) {
        $('#removeBom'+id).html('');
        return false;
    }

    function selectDate(row) {
	 	$('#datepicker'+row).datepicker({
	      dateFormat: 'yy-mm-dd', 
	      ampm: true
	    }); 
    }

    function selectDateTwo(row) {
	 	$('#datepicker2'+row).datepicker({
	      dateFormat: 'yy-mm-dd', 
	      ampm: true
	    }); 
    }
$(document).on('focus', '.datepicker', function() {
    $(this).datepicker({
  dateFormat: 'yy-mm-dd', 
  ampm: true
});
});

$(document).on('focus', '.datepicker2', function() {
    $(this).datepicker({
  dateFormat: 'yy-mm-dd', 
  ampm: true
});
});


$(document).on("change keyup", ".quantity", function() {
    var sum = 0;
    $(".quantity").each(function(){
        sum += +$(this).val();
    });
    var total = $("#total").val();
    if(sum != total) {
    	 
    	$(this).addClass('border-red');
    	$('#submit').prop('disabled',true);
    } else {
    	if($(this).hasClass('border-red')) {
    		$(this).removeClass('border-red');
    	}
    	$('#submit').prop('disabled',false);
    }
    console.log(sum);
});

$(document).ready(function() { 
	
    var row = <?php echo $i; ?>;
    $('#addMore').click(function() { 
    	var mainCode = $('#name').val();
    	//if(mainCode == '') {
		//	$('#error').html('<p style="color: red">Please fill Job Number before add Sub Station.</p>'); 
		//} else {
			$('#error').html('');
			var html = '<div class="form-group" id="removeBom'+row+'">'; 
	       
	        html += '<div class="col-sm-3">';
	        html += '<input type="text" name="data[PlanningPartialDelivery][quantity][]" class="form-control quantity" placeholder="Quantity"required>'; 
	        html += '</div>'; 
	        html += '<div class="col-sm-3">';
	        html += '<input type="text" name="data[PlanningPartialDelivery][delivery][]" id="del'+row+'" class="datepicker2 form-control" placeholder="Delivery Date"required>';
	        html += '</div>';

	        html += '<div class="col-sm-5">';
	        html += '<textarea name="data[PlanningPartialDelivery][remark][]" class="form-control" placeholder="Remark / Note (Optional)"></textarea>';
	        html += '</div>';

	        html += '<div class="col-sm-1">';
	        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+row+'); return false"><i class="fa fa-times"></i></a>';
	        html += '</div>';
	        html += '</div>';     
	        row++; 

 
	        $("#output").append(html); 	   
		//}
        
        return false;
    });   
 

});  
</script>
<?php $this->end(); ?>