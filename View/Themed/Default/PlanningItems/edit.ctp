<?php echo $this->Html->link('Cancel Editing', array('controller' => 'plannings', 'action' => 'view', $item['Planning']['id']), array('class' => 'btn btn-warning')); ?>  

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit: <?php echo $item['InventoryItem']['code']; ?> (<?php echo $item['InventoryItem']['name']; ?>)</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

 
<?php echo $this->Form->create('PlanningItem', array('class' => 'form-horizontal')); ?>
 
	<?php
		echo $this->Form->input('id'); 
	?>

		<div class="form-group">
			<label class="col-sm-3">Quantity To Purchase*</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('quantity', array('class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">Buffer *</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('buffer', array('class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">UOM *</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('general_unit_id', array('class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">Supplier *</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('inventory_supplier_id', array('class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">Remark *</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('remark', array('class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>

		 <div class="form-group"> 
	    	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success', 'required' => false)); ?> 
	    </div>	  

<?php echo $this->Form->end(); ?> 
 
</div>
</div>
</div>
</div>