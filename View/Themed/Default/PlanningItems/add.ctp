<div class="planningItems form">
<?php echo $this->Form->create('PlanningItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Planning Item'); ?></legend>
	<?php
		echo $this->Form->input('planning_id');
		echo $this->Form->input('inventory_item_id');
		echo $this->Form->input('quantity');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('planning_quantity');
		echo $this->Form->input('planning_general_unit');
		echo $this->Form->input('inventory_supplier_id');
		echo $this->Form->input('remark');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Planning Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Plannings'), array('controller' => 'plannings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning'), array('controller' => 'plannings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
