<div class="planningItems index">
	<h2><?php echo __('Planning Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('planning_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('planning_quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('planning_general_unit'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_supplier_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('remark'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($planningItems as $planningItem): ?>
	<tr>
		<td><?php echo h($planningItem['PlanningItem']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($planningItem['Planning']['name'], array('controller' => 'plannings', 'action' => 'view', $planningItem['Planning']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($planningItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $planningItem['InventoryItem']['id'])); ?>
		</td>
		<td><?php echo h($planningItem['PlanningItem']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($planningItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $planningItem['GeneralUnit']['id'])); ?>
		</td>
		<td><?php echo h($planningItem['PlanningItem']['planning_quantity']); ?>&nbsp;</td>
		<td><?php echo h($planningItem['PlanningItem']['planning_general_unit']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($planningItem['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $planningItem['InventorySupplier']['id'])); ?>
		</td>
		<td><?php echo h($planningItem['PlanningItem']['created']); ?>&nbsp;</td>
		<td><?php echo h($planningItem['PlanningItem']['modified']); ?>&nbsp;</td>
		<td><?php echo h($planningItem['PlanningItem']['remark']); ?>&nbsp;</td>
		<td><?php echo h($planningItem['PlanningItem']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $planningItem['PlanningItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $planningItem['PlanningItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $planningItem['PlanningItem']['id']), array(), __('Are you sure you want to delete # %s?', $planningItem['PlanningItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Plannings'), array('controller' => 'plannings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning'), array('controller' => 'plannings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
