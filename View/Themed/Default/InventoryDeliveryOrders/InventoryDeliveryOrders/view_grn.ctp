<style type="text/css">
.line {    
    border-bottom: 2px dotted #000;
    text-decoration: none;
}
</style>
<?php //error_reporting(E_ALL); ?>
<?php ini_set('display_errors', '0'); ?>
<div class="row"> 
    <div class="col-xs-12">
        <?php echo $this->Html->link(__('List GRN'), array('action' => 'view', $inventoryPurchaseOrder[0]['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-info btn-sm')); ?>
        <?php if($inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['po_no']!=''){ $po_no = $inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['po_no']; } ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('GRN  ('.$po_no.')'); ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>
            <!-- content start-->
           
            <dl>
                <dt class="col-sm-2">PO Number</dt>
                <dd class="col-sm-9">
                    : <?php echo $inventoryPurchaseOrder[0]['InventoryPurchaseOrder']['po_no']; ?>
                    &nbsp;
                </dd>
                <dt class="col-sm-2">GRN Number</dt>
                <dd class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['grn_no']; ?>
                    &nbsp;
                </dd>
                <dt class="col-sm-2">DO Number</dt>
                <dd class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['do_no']; ?>
                    &nbsp;
                </dd>
                <dt class="col-sm-2">Supplier</dt>
                <dd class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder[0]['InventorySupplier']['name']; ?> / <?php echo $inventoryDeliveryOrder[0]['InventorySupplier']['code']; ?>
                    &nbsp;
                </dd>
                <dt class="col-sm-2">Date received</dt>
                <dd class="col-sm-9">
                    : <?php echo date('d-m-Y', strtotime($inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['received_date'])); ?>
                    &nbsp;
                </dd>
                <dt class="col-sm-2">Warehouse</dt>
                <dd class="col-sm-9">
                    : <?php echo $inventoryPurchaseOrder[0]['InventoryDeliveryLocation']['name']; ?>
                    &nbsp;
                </dd>
                <dt class="col-sm-2">Status</dt>
                <dd class="col-sm-9">
                <?php if($inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['status'] == 1){ $status = 'Pending'; }else{ $status = 'Received'; } ?>
                    : <?php echo $status; ?>
                    &nbsp;
                </dd>
            </dl>
            <h4>Items</h4>
            <table class="table table-bordered">
                <thead>
                    <th>#</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Quantity Received</th>
                    <th>Quantity Accepted</th>
                    <th>Quantity Rejected</th>
                    <th>Store</th>
                    <th>Rack</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    <?php $bil = 1; ?>
                    <?php foreach ($inventoryDeliveryOrder[0]['InventoryDeliveryOrderItem'] as $item) { ?>
                    <?php if($item['InventoryItem']['qc_inspect'] == 1) { $qc = 'YES'; }else{ $qc = 'NO'; } ?>
                    <tr>
                        <td><?php echo $bil; ?></td>
                        <td><?php echo '<strong>BARCODE : '.$item['InventoryDeliveryOrderItem']['barcode'].'</strong><br>'.$item['InventoryItem']['name'].'<br>'.$item['InventoryItem']['code'].'<br>QC Inspection : '.$qc; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity']; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity_delivered']; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity_accepted']; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity_rejected']; ?>
                            <br><?php echo 'Remark : '.$item['FinishedGoodComment']['name']; ?>
                        </td>
                        <td><?php echo $item['Store']['name']; ?></td>
                        <td><?php echo $item['Rack']['name']; ?></td>
                        <td><?php echo $this->Html->link('<i class="fa fa-barcode"></i> Print', array('action' => 'view_barcode', $item['InventoryDeliveryOrderItem']['id']), array('class' => 'btn btn-success', 'escape'=>false, 'target'=>'blank')); ?></td>
                    </tr>
                   
                    <?php if(!empty($item['InventoryQcItem'])) { ?>
                    <tr>
                        <td colspan="9">
                            <h4>QC Inspection</h4>
                            <table class="table table-bordered">
                                <thead>
                                    <th>#</th>
                                    <th>Quantity</th>
                                    <th>Quantity Pass</th>
                                    <th>Quantity Rejected</th>
                                    <th>Rejected Remark</th>
                                    <th colspan="2">Action</th>
                                </thead>
                                <tbody>
                                     <?php $bilqc = 1; ?>
                                     <?php foreach ($item['InventoryQcItem'] as $qcitems) { ?>
                                     <tr>
                                        <td><?php echo $bilqc; ?></td>
                                        <td><?php echo $qcitems['InventoryQcItem']['quantity']; ?></td>
                                        <td><?php echo $qcitems['InventoryQcItem']['quantity_pass']; ?></td>
                                        <td><?php echo $qcitems['InventoryQcItem']['quantity_rejected']; ?></td>
                                        <?php if(!empty($qcitems['InventoryQcRemark']['remark'])){ $remark = $qcitems['InventoryQcRemark']['remark']; $attach = $qcitems['InventoryQcRemark']['attachment']; }else{ $remark = ''; $attach = ''; } ?>
                                        <td><?php echo $remark; ?><br><?php echo 'Attachment : '.$attach; ?></td>
                                        <?php $action = array(
                                                'Return to Supplier' => 'Return to Supplier',
                                                'Modified/Reuse' => 'Modified/Reuse',
                                                'Scrap' => 'Scrap'
                                        ); ?>
                                        <?php if(isset($qcitems['InventoryStockRejected']['actions']) && $qcitems['InventoryStockRejected']['actions']!=NULL){ ?>
                                        <td><?php echo $qcitems['InventoryStockRejected']['actions']; ?></td>
                                        <?php }else{ ?>
                                        <?php if($qcitems['InventoryQcItem']['quantity_rejected']!= 0){ ?>                               
                                        <?php echo $this->Form->create('DeliveryNotes', array('class' => 'form-horizontal')); ?>
                                        <?php echo $this->Form->input('stock_rejected_id', array('type'=>'hidden', 'class' => 'form-control', 'value'=>$qcitems['InventoryStockRejected']['id'], 'label'=>false)); ?>
                                        <td style="border-right: 0px"><?php echo $this->Form->input('rejected_action', array('class' => 'form-control', 'options'=>$action, 'label'=>false)); ?></td>
                                        <td style="border-left: 0px"><?php echo $this->Form->submit('Save', array('class' => 'btn btn-success pull-right')); ?></td>
                                        <?php echo $this->Form->end(); ?>
                                        <?php }else{ ?>
                                        <td colspan="2">&nbsp;</td>
                                        <?php } ?>
                                        <?php } ?>
                                     </tr>
                                     <?php $bilqc++; } ?>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php $bil++; } ?>
                </tbody>
            </table>
            
                

            <!-- content end -->
        </div>
    </div>
    </div>
</div>
