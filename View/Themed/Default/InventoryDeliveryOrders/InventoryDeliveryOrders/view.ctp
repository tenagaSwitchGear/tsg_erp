<style type="text/css">
.line {    
    border-bottom: 2px dotted #000;
    text-decoration: none;
}
</style>
<?php //error_reporting(E_ALL); ?>
<?php ini_set('display_errors', '0'); ?>
<div class="row"> 
    <div class="col-xs-12">
        <?php echo $this->Html->link(__('List Purchase Order'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php if($inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['po_no']!=''){ $po_no = $inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['po_no']; } ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('GRN  ('.$po_no.')'); ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>
            <!-- content start-->
            
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>GRN</th>
                        <th>Received Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $bil = 1; ?>
                    <?php $quantity_accepted = 0; ?>
                    <?php foreach($inventoryDeliveryOrder as $inventoyrDeliveryOrder){ ?>
                    <tr>
                        <td><?php echo $bil; ?></td>
                        <td><?php echo $inventoyrDeliveryOrder['InventoryDeliveryOrder']['grn_no']; ?></td>
                        <td><?php echo $inventoyrDeliveryOrder['InventoryDeliveryOrder']['received_date']; ?></td>
                        <?php if($inventoyrDeliveryOrder['InventoryPurchaseOrder']['purchase_status'] == 2){ $status = 'Closed'; }else{ $status = 'Pending'; } ?>

                        <td><?php echo $status; ?></td>
                        <td><?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view_grn', $inventoyrDeliveryOrder['InventoryDeliveryOrder']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?></td>
                    </tr>
                    
                    <?php //$quantity_ordered = 0; ?>
                    <?php 
                    foreach($inventoyrDeliveryOrder['InventoryDeliveryOrderItem'] as $q){
                        $quantity_accepted = $quantity_accepted + $q['InventoryDeliveryOrderItems']['quantity_accepted'];
                        //$quantity_ordered = $quantity_ordered + $q['InventoryDeliveryOrderItems']['quantity'];

                    }
                    
                    $quantity_accepted = number_format($quantity_accepted, 4);
                    $quantity_ordered = $q['InventoryDeliveryOrderItems']['quantity'];
                    $bil++; 

                    } ?>
                </tbody>
            </table>
            <input type="hidden" id="quantity_ordered" value="<?php echo $quantity_ordered; ?>">
            <input type="hidden" id="quantity_accepted" value="<?php echo $quantity_accepted; ?>">
            <input type="hidden" id="po_id" value="<?php echo $inventoryDeliveryOrder[0]['InventoryDeliveryOrder']['inventory_purchase_order_id']; ?>">
            

            <!-- content end -->
        </div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
$( document ).ready(function() {
    
    var q_order = $('#quantity_ordered').val();
    var q_accept = $('#quantity_accepted').val();
    var po_id = $('#po_id').val();

    if(q_order == q_accept){
        $.ajax({ 
            type: "POST", 
            dataType: "json",
            data: "po_id=" + po_id,
            cache: false,
            url: baseUrl + 'inventory_delivery_orders/ajaxupdatestatus', 
            success: function(data) { 
                console.log(data);
                
            },
            error: function(err) { 
                console.log(err);
                
            }
        });
    }    
});
</script>
<?php $this->end(); ?>
