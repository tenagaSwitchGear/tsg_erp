<div class="inventoryDeliveryOrders form">
<?php echo $this->Form->create('InventoryDeliveryOrder'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Delivery Order'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('inventory_supplier_id');
		echo $this->Form->input('inventory_purchase_order_id');
		echo $this->Form->input('referrence');
		echo $this->Form->input('user_id');
		echo $this->Form->input('price');
		echo $this->Form->input('tax');
		echo $this->Form->input('total');
		echo $this->Form->input('status');
		echo $this->Form->input('general_payment_term_id');
		echo $this->Form->input('payment_dateline');
		echo $this->Form->input('receive_location');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventoryDeliveryOrder.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventoryDeliveryOrder.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Orders'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Orders'), array('controller' => 'inventory_purchase_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order'), array('controller' => 'inventory_purchase_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Payment Terms'), array('controller' => 'general_payment_terms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Payment Term'), array('controller' => 'general_payment_terms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Order Items'), array('controller' => 'inventory_delivery_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('controller' => 'inventory_delivery_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Payments'), array('controller' => 'inventory_payments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Payment'), array('controller' => 'inventory_payments', 'action' => 'add')); ?> </li>
	</ul>
</div>
