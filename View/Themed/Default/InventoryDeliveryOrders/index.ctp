<?php echo $this->Html->link(__('Waiting GRN'), array('action' => 'index'), array('class'=>'btn btn-default btn-sm')); ?>

<?php echo $this->Html->link(__('Partially Receive'), array('action' => 'index', 1), array('class'=>'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('Closed'), array('action' => 'index', 2), array('class'=>'btn btn-success btn-sm')); ?>

<?php echo $this->Html->link(__('GRN Lists'), array('action' => 'grn'), array('class'=>'btn btn-primary btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
  		
  		
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Waiting GRN'); ?></h2>
                <?php 
                  echo $this->Html->link('<i class="fa fa-plus"></i> New GRN', array('action' => 'add'), array('class' => 'btn btn-success pull-right btn-sm', 'escape'=>false)); 
                 ?>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
    <?php echo $this->Form->create('InventoryDeliveryOrder', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
    <table cellpadding="0" cellspacing="0" class="table">
      <tr>
      <td><?php echo $this->Form->input('supplier', array('placeholder' => 'Supplier', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
      </td>
      <td><?php echo $this->Form->input('po_no', array('type' => 'text', 'placeholder' => 'PO No', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
      </td>
       
      <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-primary pull-right')); ?></td> 
      </tr>
    </table>
  <?php $this->end(); ?>    	
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo ('#'); ?></th>
                            <th><?php echo $this->Paginator->sort('InventorySupplier.Name', 'Supplier'); ?></th>
                            <th><?php echo $this->Paginator->sort('PO Number'); ?></th>
                            <th><?php echo ('Required Date'); ?></th>
                            <th><?php echo ('Created'); ?></th>
                            <th><?php echo ('PIC'); ?></th>
                            <th><?php echo ('GRN'); ?></th>
                            <th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseOrder']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseOrder']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseOrder']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryPurchaseOrders as $inventoryPurchaseOrder): 
					?>
					<tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td><?php echo ($inventoryPurchaseOrder['InventorySupplier']['name']); ?>&nbsp;</td>
                        <td><?php echo h($inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']); ?>&nbsp;</td>
                        <td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['dateline']))); ?>&nbsp;</td>
                        <td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['created']))); ?>&nbsp;</td>
                        <td><?php echo ($inventoryPurchaseOrder['User']['firstname']); ?>&nbsp;</td>
						<td>
                        <?php if(!empty($inventoryPurchaseOrder['GRN'])){ ?>
                        <?php for($i=0; $i<count($inventoryPurchaseOrder['GRN']); $i++){ 
                                echo '- '.$inventoryPurchaseOrder['GRN'][$i]['InventoryDeliveryOrder']['grn_no'].'<br>';
                         } } ?>
                        </td>
						<td class="actions">
                            <?php if(!empty($inventoryPurchaseOrder['GRN'])){ ?>
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
                            <?php } ?>
							<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php //echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']).'"?', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id'])); ?>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>