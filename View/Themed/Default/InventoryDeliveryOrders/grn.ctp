<?php echo $this->Html->link(__('Waiting GRN'), array('action' => 'index'), array('class'=>'btn btn-default btn-sm')); ?>

<?php echo $this->Html->link(__('Partially Receive'), array('action' => 'index', 1), array('class'=>'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('Closed'), array('action' => 'index', 2), array('class'=>'btn btn-success btn-sm')); ?>

<?php echo $this->Html->link(__('GRN Lists'), array('action' => 'grn'), array('class'=>'btn btn-primary btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
  		
  		
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('GRN List'); ?></h2>
            <?php 
                  echo $this->Html->link('<i class="fa fa-plus"></i> New GRN', array('action' => 'add'), array('class' => 'btn btn-success pull-right btn-sm', 'escape'=>false)); 
                 ?>
                
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
    <?php echo $this->Session->flash(); ?>

    <?php echo $this->Form->create('InventoryDeliveryOrder', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
    <table cellpadding="0" cellspacing="0" class="table">
      <tr>
      <td><?php echo $this->Form->input('grn_no', array('placeholder' => 'GRN No', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
      </td>
      <td><?php echo $this->Form->input('po_no', array('type' => 'text', 'placeholder' => 'PO No', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
      </td>
       
      <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
      </tr>
    </table>
  <?php $this->end(); ?>
        	
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr> 
            <th><?php echo $this->Paginator->sort('grn_no', 'GRN No'); ?></th>
            <th><?php echo $this->Paginator->sort('InventorySupplier.name', 'Supplier'); ?></th>
            <th><?php echo $this->Paginator->sort('InventoryPurchaseOrder.po_no', 'PO No'); ?></th>
            <th><?php echo ('Required Date'); ?></th>
            <th><?php echo ('Created'); ?></th>
            <th><?php echo ('PIC'); ?></th> 
            <th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php 
						foreach ($deliveryorders as $inventoryPurchaseOrder): 
					?>
					<tr>
            <td>
              <?php echo $inventoryPurchaseOrder['InventoryDeliveryOrder']['grn_no']; ?>
            </td>
            <td><?php echo ($inventoryPurchaseOrder['InventorySupplier']['name']); ?>&nbsp;</td>
            <td><?php echo h($inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']); ?>&nbsp;</td>
            <td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['dateline']))); ?>&nbsp;</td>
            <td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['created']))); ?>&nbsp;</td>
            <td><?php echo ($inventoryPurchaseOrder['User']['firstname']); ?>&nbsp;</td>
						            
						<td class="actions">
                             
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view_grn', $inventoryPurchaseOrder['InventoryDeliveryOrder']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
                             
							<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							 
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>