<div class="book page-break">
    <div class="page"> 
        <div id="content" class="container"> 
        <table class="table table-bordered">
            <tr>
                <td class="col-sm-2">GRN Number</td>
                <td class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder['InventoryDeliveryOrder']['grn_no']; ?>
                    &nbsp;
                </td>
            
                <td class="col-sm-2">PO Number</td>
                <td class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder['InventoryPurchaseOrder']['po_no']; ?>
                    &nbsp;
                </td>
            </tr>
            
            <tr>
                <td class="col-sm-2">DO Number</td>
                <td class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder['InventoryDeliveryOrder']['do_no']; ?>
                    &nbsp;
                </td>
            
                <td class="col-sm-2">Supplier</td>
                <td class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder['InventorySupplier']['name']; ?> / <?php echo $inventoryDeliveryOrder['InventorySupplier']['code']; ?>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="col-sm-2">Date received</td>
                <td class="col-sm-9">
                    : <?php echo date('d-m-Y', strtotime($inventoryDeliveryOrder['InventoryDeliveryOrder']['received_date'])); ?>
                    &nbsp;
                </td>
            
                <td class="col-sm-2">Delivery Point</td>
                <td class="col-sm-9">
                    : <?php echo $inventoryDeliveryOrder['InventoryLocation']['name']; ?>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="col-sm-2">PIC Name</td> 
                <td class="col-sm-9">
               : <?php echo $inventoryDeliveryOrder['User']['firstname']; ?>
                    &nbsp;
                </td>
             
                <td class="col-sm-2">Username</td> 
                <td class="col-sm-9">
                : <?php echo $inventoryDeliveryOrder['User']['username']; ?>
                    &nbsp;
                </td>
            </tr>

        </table>
            
            <h4>Items</h4>
            <table class="table table-bordered">
                <thead>
                    <th>No</th>
                    <th>Item</th>
                    <th>UOM</th>
                    <th>Quantity</th>
                    <th>Quantity Received</th>
                    <th>Quantity Accepted</th>
                    <th>Quantity Rejected</th>
                    <th>Store</th>
                    <th>Bin</th> 
                </thead>
                <tbody>
                    <?php $bil = 1; ?>
                    <?php foreach ($items as $item) { ?>
                    <?php if($item['InventoryItem']['qc_inspect'] == 1) { $qc = 'YES'; }else{ $qc = 'NO'; } ?>
                    <tr>
                        <td><?php echo $bil; ?></td>
                        <td><?php echo $item['InventoryItem']['code'].'<br><small>'.$item['InventoryItem']['name'] . '</small>'; ?></td>
                        <td><?php echo $item['GeneralUnit']['name']; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity']; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity_delivered']; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity_accepted']; ?></td>
                        <td><?php echo $item['InventoryDeliveryOrderItem']['quantity_rejected']; ?>
                            <br><?php if($item['InventoryDeliveryOrderItem']['quantity_rejected'] > 0) { echo 'Remark : '.$item['FinishedGoodComment']['name']; } ?>
                        </td>
                        <td><?php echo $item['InventoryStore']['name']; ?></td>
                        <td><?php echo $item['InventoryRack']['name']; ?></td>
                        
                    </tr>
                   
                    <?php if(!empty($item['InventoryQcItem'])) { ?>
                    
                    <?php } ?>
                    <?php  
                    } ?>
                </tbody>
            </table>

            <table class="table table-bordered" style="margin-top: 100px; width: 600px; float: right">
                <tr>
                <td style="width:33%"></td>
                <td style="width:33%" class="text-center">Signature</td>
                <td style="width:33%" class="text-center">Date</td>
                </tr>
                <tr>
                <td class="text-center">Receive By:</td>
                <td style="height:70px"></td>
                <td></td>
                </tr>
            </table>
            
                
 
  </div>
    </div>
</div>