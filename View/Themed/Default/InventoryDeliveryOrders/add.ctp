<?php echo $this->Html->link(__('Waiting GRN'), array('action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('GRN Lists'), array('action' => 'grn'), array('class'=>'btn btn-warning btn-sm')); ?>

<?php echo $this->Html->link(__('Closed'), array('action' => 'index', 3), array('class'=>'btn btn-success btn-sm')); ?>

<div class="row"> 
    <div class="col-xs-12">
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Add new GRN'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
            	<?php echo $this->Session->flash(); ?>
            	
            	<div class="inventoryDeliveryOrders form">
				<?php echo $this->Form->create('InventoryDeliveryOrder', array('class'=>'form-horizontal', 'type' => 'file')); ?>
					<fieldset>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Purchase Order</label>
                            <div class="col-sm-9">
                            	<select class="form-control" name="data[InventoryDeliveryOrder][puchase_order_id]" onchange="get_purchase_order(this.value)">
                            		<option>Select Purchase Order</option>
                            		<?php foreach ($inventoryPurchaseOrders as $po){ ?>
                                    <?php if(empty($po['InventorySupplier'])){ $supplier = ''; }else{ $supplier = 'Supplier : '.$po['InventorySupplier']['name'].'';  } ?>
                                    <?php if($po['Status'] == 'Closed'){}else{ ?>
                            			<option value="<?php echo $po['InventoryPurchaseOrder']['id'] ?>"><?php echo $po['InventoryPurchaseOrder']['po_no'].'&nbsp;&nbsp;&nbsp;&nbsp;'.$supplier; ?></option>
                                    <?php } ?>            
                            		<?php } ?>
                            	</select>
                              <small>If PO No not showing here, please ask PRC dept to update PO Status as <b>Purchased</b></small>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Date Received</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("received_date", array("type"=>"text", "id"=>"dateonly", "class"=> "form-control", "label"=> false, 'required'=>true, 'value' => date('Y-m-d'))); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Delivery Order No</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("delivery_order_no", array("class"=> "form-control", "label"=> false, 'required'=>true)); ?>
                            </div> 
                        </div>  

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Delivery Point</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("inventory_location_id", array("options" => $locations, "class"=> "form-control", "label"=> false, 'required'=>true, 'id' => 'inventory_location_id')); ?>
                            </div> 
                        </div>  

                        <div class="form-group">
		                    <label class="col-sm-3" style="padding-top: 8px">Attachment</label>
		                    <div class="col-sm-9">
		                        <?php echo $this->Form->input("filename", array("class"=> "form-control", "type"=>"file", "label"=> false, "required" => false)); ?>
		                        <?php echo $this->Form->input('dir', array('type' => 'hidden')); ?>
		                    </div>
		                </div>  
                        
					</fieldset>
					<div id="loadPO">
                   
                    	</div>
				<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
function get_purchase_order(id){
	$.ajax({
        type: "GET",                        
        url:baseUrl + 'inventory_purchase_orders/ajaxfindpo',           
        contentType: "application/json",
        dataType: "json",
        data: "id=" + id,
        //data: getParam,                                                    
        success: function (data) { 
   			console.log(data);
        $("#inventory_location_id").val(data[0]['InventoryLocation']['id']).change(); 
   			//$('#loadPO').html(data);
   			var html = '<div class="clearfix">&nbsp;</div><table class="table table-bordered table-responsive">';

   			html += '<tr><td class="col-md-2"><strong>PO Number</strong></td><td>'+data[0]['InventoryPurchaseOrder']['po_no']+'</td></tr>';
   			html += '<tr><td><strong>Supplier<strong></td><td>'+data[0]['InventorySupplier']['name']+'</td></tr>';
   			html += '<tr><td><strong>Remark<strong></td><td>'+data[0]['Remark']['name']+'</td></tr>';
            if(data[0]['Warranty']['name'] == null){ var warranty = ''; }else{ var warranty = data[0]['Warranty']['name']; }
   			html += '<tr><td><strong>Warranty/Guarantee<strong></td><td>'+warranty+'</td></tr>';

   			html += '</table><div class="clearfix">&nbsp;</div>';

   			html += '<strong>Purchase Order Items</strong>';
   			html += '<div class="clearfix">&nbsp;</div><table class="table table-bordered table-responsive">';
   			html += '<tr>';
   			html += '<th>#</th>';
   			html += '<th>Items</th>';
   			html += '<th>Quantity</th>';
   			html += '<th class="col-md-1">Quantity Received</th>';
   			html += '<th class="col-md-1">Quantity Accepted</th>';
   			html += '<th class="col-md-1">Quantity Rejected</th>';
   			html += '<th>Rejected Remark</th>';
   			html += '<th>Store</th>';
   			html += '<th>Rack</th>';
   			html += '</tr>';
   			for(i=0; i<data[0]['InventoryPurchaseOrderItem'].length; i++){
   				if(data[0]['InventoryPurchaseOrderItem'][i]['InventoryItem']['qc_inspect'] == 1){
   					var qc = 'YES';
   				}else{
   					var qc = 'NO';
   				}
   				html += '<tr>';
	   			html += '<td>'+(i+1)+'</td>';
	   			html += '<td>'+data[0]['InventoryPurchaseOrderItem'][i]['InventoryItem']['name']+'<br>'+data[0]['InventoryPurchaseOrderItem'][i]['InventoryItem']['code']+'<br>QC Inspection :'+qc+'<br><input type="hidden" name="purchase_order_item_id[]" value="'+data[0]['InventoryPurchaseOrderItem'][i]['InventoryPurchaseOrderItem']['id']+'"></td>';
	   			html += '<td>'+data[0]['InventoryPurchaseOrderItem'][i]['InventoryPurchaseOrderItem']['quantity']+'</td>';
                var max = data[0]['InventoryPurchaseOrderItem'][i]['InventoryPurchaseOrderItem']['quantity'] - data[0]['InventoryPurchaseOrderItem'][i]['InventoryPurchaseOrderItem']['received_quantity'];
	   			html += '<td><input type="hidden" name="inventory_purchase_order_item_id[]" value="'+data[0]['InventoryPurchaseOrderItem'][i]['InventoryPurchaseOrderItem']['id']+'"><input class="form-control" name="q_delivered[]" type="text" min="0" max="'+max+'"required></td>';
	   			html += '<td><input class="form-control" name="q_accepted[]" type="text" min="0" max="'+max+'"required></td>';
	   			html += '<td><input class="form-control" name="q_rejected[]" type="text" min="0" max="'+max+'" value="0"required></td>';
	   			html += '<td><select class="form-control" name="rejected_remark[]"><option value="0">Select Remark</option><?php foreach ($remark as $key => $value) { ?><option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?></select></td>';
	   			html += '<td><select class="form-control" name="store_id[]" onchange="get_rack(this.value, '+i+')"><option value="0">NON Stock</option>';

	   			for(s=0; s<data[0]['Store'].length; s++){
	   				html += '<option value="'+data[0]['Store'][s]['id']+'">'+data[0]['Store'][s]['name']+'</option>';
	   			}

	   			html += '</select></td>';
	   			html += '<td id="rack'+i+'"><select class="form-control" name="rack_id[]"><option value="0">NON Stock</option></select></td>';
	   			html += '</tr>';

   			}


   			html += '</table>';

            
            //html += data[0]['InventorySupplier']['id'];
            
            $('#loadPO').html(html);

        }
    });
}

function get_rack(id, row){
	$.ajax({
        type: "GET",                        
        url:baseUrl + 'inventory_purchase_orders/ajaxfindrack',           
        contentType: "application/json",
        dataType: "json",
        data: "id=" + id,
        //data: getParam,                                                    
        success: function (data) {
        	console.log(data);
        	var html = '<select class="form-control" name="rack_id[]">';
          html += '<option value="0">NON Stock</option>';
        	for(i=0; i<data.length; i++){
        		html += '<option value="'+data[i]['InventoryRack']['id']+'">'+data[i]['InventoryRack']['name']+'</option>';
        	}
        	html += '</select>';

        	$('#rack'+row).html(html);

        }
    });
}
</script>
<?php $this->end(); ?>