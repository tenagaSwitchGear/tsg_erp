<div class="projectScheduleChildren view">
<h2><?php echo __('Project Schedule Child'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectScheduleChild['ProjectScheduleChild']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Id Delete'); ?></dt>
		<dd>
			<?php echo h($projectScheduleChild['ProjectScheduleChild']['parent_id_delete']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($projectScheduleChild['ProjectScheduleChild']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start'); ?></dt>
		<dd>
			<?php echo h($projectScheduleChild['ProjectScheduleChild']['start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End'); ?></dt>
		<dd>
			<?php echo h($projectScheduleChild['ProjectScheduleChild']['end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost'); ?></dt>
		<dd>
			<?php echo h($projectScheduleChild['ProjectScheduleChild']['cost']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project Schedule Child'), array('action' => 'edit', $projectScheduleChild['ProjectScheduleChild']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project Schedule Child'), array('action' => 'delete', $projectScheduleChild['ProjectScheduleChild']['id']), array(), __('Are you sure you want to delete # %s?', $projectScheduleChild['ProjectScheduleChild']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedule Children'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Child'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedule Assigns'), array('controller' => 'project_schedule_assigns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('controller' => 'project_schedule_assigns', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Project Schedule Assigns'); ?></h3>
	<?php if (!empty($projectScheduleChild['ProjectScheduleAssign'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Project Schedule Child Id'); ?></th>
		<th><?php echo __('Project Manpower Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($projectScheduleChild['ProjectScheduleAssign'] as $projectScheduleAssign): ?>
		<tr>
			<td><?php echo $projectScheduleAssign['id']; ?></td>
			<td><?php echo $projectScheduleAssign['project_schedule_child_id']; ?></td>
			<td><?php echo $projectScheduleAssign['project_manpower_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'project_schedule_assigns', 'action' => 'view', $projectScheduleAssign['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'project_schedule_assigns', 'action' => 'edit', $projectScheduleAssign['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'project_schedule_assigns', 'action' => 'delete', $projectScheduleAssign['id']), array(), __('Are you sure you want to delete # %s?', $projectScheduleAssign['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('controller' => 'project_schedule_assigns', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
