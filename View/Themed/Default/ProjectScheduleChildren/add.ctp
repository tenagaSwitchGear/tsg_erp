<div class="projectScheduleChildren form">
<?php echo $this->Form->create('ProjectScheduleChild'); ?>
	<fieldset>
		<legend><?php echo __('Add Project Schedule Child'); ?></legend>
	<?php
		echo $this->Form->input('parent_id_delete');
		echo $this->Form->input('title');
		echo $this->Form->input('start');
		echo $this->Form->input('end');
		echo $this->Form->input('cost');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Project Schedule Children'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Schedule Assigns'), array('controller' => 'project_schedule_assigns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('controller' => 'project_schedule_assigns', 'action' => 'add')); ?> </li>
	</ul>
</div>
