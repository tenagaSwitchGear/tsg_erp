<div class="projectScheduleChildren index">
	<h2><?php echo __('Project Schedule Children'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('parent_id_delete'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('start'); ?></th>
			<th><?php echo $this->Paginator->sort('end'); ?></th>
			<th><?php echo $this->Paginator->sort('cost'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectScheduleChildren as $projectScheduleChild): ?>
	<tr>
		<td><?php echo h($projectScheduleChild['ProjectScheduleChild']['id']); ?>&nbsp;</td>
		<td><?php echo h($projectScheduleChild['ProjectScheduleChild']['parent_id_delete']); ?>&nbsp;</td>
		<td><?php echo h($projectScheduleChild['ProjectScheduleChild']['title']); ?>&nbsp;</td>
		<td><?php echo h($projectScheduleChild['ProjectScheduleChild']['start']); ?>&nbsp;</td>
		<td><?php echo h($projectScheduleChild['ProjectScheduleChild']['end']); ?>&nbsp;</td>
		<td><?php echo h($projectScheduleChild['ProjectScheduleChild']['cost']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectScheduleChild['ProjectScheduleChild']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectScheduleChild['ProjectScheduleChild']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectScheduleChild['ProjectScheduleChild']['id']), array(), __('Are you sure you want to delete # %s?', $projectScheduleChild['ProjectScheduleChild']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project Schedule Child'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Schedule Assigns'), array('controller' => 'project_schedule_assigns', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('controller' => 'project_schedule_assigns', 'action' => 'add')); ?> </li>
	</ul>
</div>
