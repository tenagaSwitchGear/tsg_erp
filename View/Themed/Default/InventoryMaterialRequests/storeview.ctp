<?php echo $this->Html->link(__('Waiting'), array('action' => 'store'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Item List'), array('action' => 'itemlist'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Ready To Issue'), array('action' => 'store/3'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Partials'), array('action' => 'store/5'), array('class' => 'btn btn-warning btn-sm')); ?>
<?php echo $this->Html->link(__('Transfered'), array('action' => 'store/4'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Requested: <?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	

<div class="inventoryMaterialRequests view-data">
<h2><?php echo __('Inventory Material Request'); ?></h2>
	<dl> 
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Production Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryMaterialRequest['ProductionOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
			&nbsp;
		</dd> 		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['User']['username'] . ' (' . $inventoryMaterialRequest['User']['firstname']. ')', array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id']), array('target' => '_blank')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone No'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['User']['mobile_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo type($inventoryMaterialRequest['InventoryMaterialRequest']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>
			&nbsp;
		</dd>

		<dt>Send Notification</dt>
		<dd>
		<?php echo $this->Form->create('InventoryMaterialRequest', array('class' => 'form-horizontal')); ?>
		<p><b>Note:</b> Please use this form to send notification to the user when your item is ready to issue. Once notification is sent, this MRN will be listed on Ready To Issue section.</p> 
		<div class="form-group"> 
			 
			<?php echo $this->Form->input('store_note', array('placeholder' => 'Type your message here...', 'type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?> 
		</div>

		<div class="form-group"> 
			<?php echo $this->Form->submit('Send Notification', array('name' => 'notification', 'class' => 'btn btn-primary pull-right')); ?> 
			 
		</div>	  
		<?php $this->Form->end(); ?>
		</dd>
	</dl>
</div> 

<?php echo $this->Session->flash(); ?>
 
<div class="related">
 
<h4>Filter Store</h4>
<?php echo $this->Html->link('All Store', array('action' => 'storeview/'.$inventoryMaterialRequest['InventoryMaterialRequest']['id']), array('class' => 'btn btn-sm btn-default')); ?> 
<?php foreach ($stores as $key => $value) { ?>  
	<?php echo $this->Html->link($value, array('action' => 'storeview/'.$inventoryMaterialRequest['InventoryMaterialRequest']['id'].'/'.$key), array('class' => 'btn btn-sm ' . active($store, $key))); ?> 
<?php } ?>
	 

	<h3><?php echo __('Items'); ?></h3>
	<?php if (!empty($inventoryMaterialRequest['InventoryMaterialRequestItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr> 
		<th><?php echo __('#'); ?></th>
		<th><?php echo __('Store'); ?></th> 
		<th><?php echo __('Item'); ?></th> 
		<th><?php echo __('Qty Available'); ?></th>
		<th><?php echo __('Demand'); ?></th> 
		<th><?php echo __('Req. Qty'); ?></th> 
		<th><?php echo __('Issued Qty'); ?></th>
		<th><?php echo __('Issued Balance'); ?></th> 
		<th><?php echo __('Issue'); ?></th>   	
		<th class="align-right"><?php echo __('Action'); ?></th> 
		</tr>
	<?php echo $this->Form->create('InventoryMaterialRequestItem', array('class' => 'form-horizontal', 'id' => 'issuedMaterial')); ?>
	<?php 
	$i = 1; 
	foreach ($items as $inventoryMaterialRequestItem): 
		//$inventoryMaterialRequestItem['InventoryMaterialRequestItem']['production_order_child_id']
	?>
		<tr>  
			<td><?php echo $i; ?> 
			</td>
			<td><?php 
			echo h($inventoryMaterialRequestItem['InventoryStore']['name']); ?><?php echo h($inventoryMaterialRequestItem['InventoryItem']['rack']); ?></td>
			<td><?php echo h($inventoryMaterialRequestItem['InventoryItem']['code']); ?><br/>
			<!--<small><?php echo $this->Html->link($inventoryMaterialRequestItem['InventoryItem']['name'], array('controller' => 'inventoryitems', 'action' => 'view', $inventoryMaterialRequestItem['InventoryItem']['id']), array('target' => '_blank')); ?></small>-->
			</td>
			 
			 
			<td class="<?php echo shortage($inventoryMaterialRequestItem['StockBalance'], $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity'], $inventoryMaterialRequest['InventoryMaterialRequest']['status'], $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']); ?>">
			<?php 

			$stock_balance = _n2($inventoryMaterialRequestItem['StockBalance']); 
			echo $stock_balance; 

			?>

			</td> <!-- it already reserved by HOD. So just show quantity --> 

			<td><?php echo _n2($inventoryMaterialRequestItem['SumDemand']); ?> 
			</td>


			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?><br/>
			<!--<small><?php echo $inventoryMaterialRequestItem['GeneralUnit']['name']; ?></small>-->
			</td>
			
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_quantity']); ?></td>
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']); ?></td> 
			<td>
 
			<input type="hidden" name="production_order_child_id[]" value="<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['production_order_child_id']; ?>">

			<input type="hidden" name="request_quantity[]" id="reqQty<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>" value="<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']; ?>">
			<input type="hidden" name="req_item_id[]" value="<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>">
			<input type="hidden" name="item_id[]" value="<?php echo $inventoryMaterialRequestItem['InventoryItem']['id']; ?>">
			
			<?php if($inventoryMaterialRequestItem['StockBalance']== 0) { ?>
				<input style="width:70px" type="text" id="issuedQty<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>" name="issued_quantity[]" value="0" placeholder="Qty" <?php disable_field($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>readonly>  
			<?php } else if($inventoryMaterialRequestItem['StockBalance'] < $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']) { ?>
				<input style="width:70px" type="text" id="issuedQty<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>" name="issued_quantity[]" value="<?php echo $stock_balance; ?>" placeholder="Qty" <?php disable_field($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>>  
			<?php } else { ?>
				<input style="width:70px" type="text" id="issuedQty<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>" name="issued_quantity[]" value="<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity'] - $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_quantity']; ?>" placeholder="Qty" <?php disable_field($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>>  
			<?php } ?> 

			<div id="error<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>"></div>
			</td> 
	
	<td class="align-right"><?php echo $this->Html->link('Edit', array('controller' => 'inventory_material_request_items', 'action' => 'edit', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'], $inventoryMaterialRequest['InventoryMaterialRequest']['id'], 'MRN'), array('class' => 'btn btn-warning btn-sm')); ?></td>			 
		</tr>

<?php $this->start('script'); ?> 
<script type="text/javascript"> 
var error = {};
$(document).ready(function() {
	var id = '<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>';
	$('#issuedQty'+id).on('keyup', function() {
		var issued = $('#issuedQty'+id).val();
		var request = $('#reqQty'+id).val(); 
		if(issued > request) { 
			console.log(request + ' : ' + issued);
			$('#error'+id).html('<span class="red-bg">Invalid Qty</small>'); 
			error[id] = false;
			$('#submitBtn').prop('disabled', true);
		} 
		if(issued <= request) {
			console.log(request + ' : ' + issued);
			$('#error'+id).html('');
			error[id] = true;
			$('#submitBtn').prop('disabled', false);
		}
	});
});
</script>
<?php $this->end(); ?>

	<?php $i++; endforeach; ?>
	<?php if($inventoryMaterialRequest['InventoryMaterialRequest']['status'] != 4) { ?> 
		<tr>
			<td colspan="8">
				<p>Fill Issue field, then click Issued Material button.</p> 
			</td> 
			<td><?php echo $this->Form->submit('Issued Material', array('type' => 'submit', 'name' => 'issued', 'class' => 'btn btn-success', 'id' => 'submitBtn')); ?></td> 
		</tr>
		<?php } ?>
	<?php $this->Form->end(); ?>
	</table>
<?php endif; ?>
 
</div>

<?php if($inventoryMaterialRequest['InventoryMaterialRequest']['status'] != 4) { ?> 
 
<?php } ?>

</div>
</div>
</div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findDuplicate(value) {
    var result = 0;
    $(".findProduct").each(function(){
        if (this.value == value) {
            result++;
        }
    });
    return result - 1;
}

function removeBom(row) {
    $('#removeItem'+row).html('');
    return false;
}

var station = 1; 



function addField() { 
    var html = '<div id="removeItem'+station+'">'; 
      html += '<div class="form-group">';  
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="item[]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Barcode" required="required"autofocus>'; 
      html += '</div>';
       

      html += '<div class="col-sm-1">';
      html += '<input type="text" name="item[]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
      html += '</div>'; 

      html += '<div class="col-sm-1">';
      html += '<a href="#" class="btn btn-danger" onclick="removeItem('+station+'); return false"><i class="fa fa-times"></i></a>';
      html += '</div>';
      html += '</div></div>';    
      $("#loadItem").append(html);   
     
    return false; 
}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
  clearTimeout (timer);
  timer = setTimeout(callback, ms);
 };
})();

$(function(){  
	$('.findProduct').each(function() {
		$(this).on('keyup', function() {
			delay( 
			function() { 
				addField();
			}, 1000);
		});
	}); 
});

$(document).ready(function() { 
	$('#issuedMaterial').on('submit', function(e) {
		e.preventDefault();
		console.log(error);
		return false;
	});
});
</script>
<?php $this->end(); ?>

<?php

function disable_field($status) {
	if($status == 4) {
		echo 'disabled="disabled"';
	}
}

function active($store, $id) {
	if($store != null) {
		if($store == $id) {
			return 'btn-primary';
		} else {
			return 'btn-default';
		}	
	} else {
		return 'btn-default';
	}
	
}

function shortage($qty, $req, $status, $issue_bal) {
	if($status != 4) {
		if($req > $qty || $issue_bal > $qty) {
			return 'red-bg';
		} else {
			return 'green-bg';
		}	
	} 
}

function status($status) {
	if($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'Partials';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>

 