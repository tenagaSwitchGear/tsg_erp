<?php echo $this->Html->link(__('My Requests'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Request Material'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 
<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryMaterialRequest['InventoryMaterialRequest']['id']), array('class' => 'btn btn-warning btn-sm')); ?> 
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Requested: <?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryMaterialRequests view-data">
<h2><?php echo __('Material Requisition Note (MRN)'); ?></h2>
	<dl>
		 
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Production Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryMaterialRequest['ProductionOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['User']['username'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo type($inventoryMaterialRequest['InventoryMaterialRequest']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
<div class="related">
	<h3><?php echo __('Item Requested'); ?></h3>
	<?php if (!empty($inventoryMaterialRequest['InventoryMaterialRequestItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr> 
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Item'); ?></th> 
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Issued Quantity'); ?></th>
		<th><?php echo __('Issued Balance'); ?></th> 
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($items as $inventoryMaterialRequestItem): ?>
		<tr> 
			<td><?php echo h($inventoryMaterialRequestItem['InventoryItem']['code']); ?></td>
			<td><?php echo h($inventoryMaterialRequestItem['InventoryItem']['name']); ?></td>
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?></td>
			<td><?php echo $inventoryMaterialRequestItem['GeneralUnit']['name']; ?></td>
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_quantity']); ?></td>
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']); ?></td> 
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_material_request_items', 'action' => 'view', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_material_request_items', 'action' => 'edit', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_material_request_items', 'action' => 'delete', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
 


<!-- Show verification history -->
	<?php if($verifications) { ?>
		<h3>Verification</h3>
		<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
			<tr>
				<th>Remark</th> 
				<th>Created</th>
				<th>Status</th> 
				<th>User</th> 
			</tr>
			<?php foreach ($verifications as $verification) { ?>  
			<tr>
				<td><?php echo h($verification['InventoryMaterialRequestVerification']['note']); ?></td> 
				<td><?php echo $verification['InventoryMaterialRequestVerification']['created']; ?></td>
				<td><?php echo status($verification['InventoryMaterialRequestVerification']['status']); ?></td>
				<td><?php echo $verification['User']['username']; ?></td> 
			</tr>
			<?php } ?>
		</table>  
	<?php } ?>

</div>
</div>
</div>
</div>
</div>


<?php

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	} elseif($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'K.I.V';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>