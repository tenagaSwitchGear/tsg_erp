


<?php echo $this->Html->link(__('Waiting Verification'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Ready To Issued'), array('action' => 'index/3'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Draft'), array('action' => 'index/0'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link(__('Incomplete'), array('action' => 'index/5'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Verified'), array('action' => 'index/2'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Rejected'), array('action' => 'index/6'), array('class' => 'btn btn-danger btn-sm')); ?>
<?php echo $this->Html->link(__('Cancelled'), array('action' => 'index/7'), array('class' => 'btn btn-warning btn-sm')); ?>
<?php echo $this->Html->link(__('Transfered'), array('action' => 'index/4'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Request Material'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>My Material Request (MRN)</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
    	<?php echo $this->Session->flash(); ?> 

    	<?php echo $this->Form->create('InventoryMaterialRequest', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr> 
		<td><?php echo $this->Form->input('ref', array('type' => 'text', 'placeholder' => 'MRN No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly_2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
<?php $this->end(); ?> 

		<div class="table-responsive"> 
			<table cellpadding="0" cellspacing="0" class="table">
			<thead>
			<tr> 
				<th><?php echo $this->Paginator->sort('code'); ?></th> 
				<th><?php echo $this->Paginator->sort('sale_job_id', 'Job'); ?></th> 
				<th><?php echo $this->Paginator->sort('created'); ?></th> 				
				<th><?php echo $this->Paginator->sort('type'); ?></th>
				<th><?php echo $this->Paginator->sort('status'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($inventoryMaterialRequests as $inventoryMaterialRequest): ?>
			<tr> 
				<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>&nbsp;</td>
				 
				<td>
					<?php echo $this->Html->link($inventoryMaterialRequest['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
				</td>
				 
				<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>&nbsp;</td> 				
				<td><?php echo type($inventoryMaterialRequest['InventoryMaterialRequest']['type']); ?>&nbsp;</td>
				<td><?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryMaterialRequest['InventoryMaterialRequest']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryMaterialRequest['InventoryMaterialRequest']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryMaterialRequest['InventoryMaterialRequest']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialRequest['InventoryMaterialRequest']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
			</table>
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div> 
	</div>
	</div>
</div>
</div>

<?php

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	} elseif($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'K.I.V';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>