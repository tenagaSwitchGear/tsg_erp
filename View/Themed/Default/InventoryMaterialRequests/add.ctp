<?php echo $this->Html->link(__('My Requests'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?> 
<?php echo $this->Html->link(__('Request Material'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
    <div class="col-xs-12"> 
        <div class="x_panel tile">
            <div class="x_title">
                <h2>Request Material (Manual)</h2> 
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>


    <?php echo $this->Form->create('InventoryMaterialRequest', array('class' => 'form-horizontal')); ?>
     
    <div class="form-group">
        <label class="col-sm-3">Job No (Optional)</label>
        <div class="col-sm-9">
            <?php echo $this->Form->input('job', array('type' => 'text', 'id' => 'findJob', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
            <?php echo $this->Form->input('sale_job_id', array('type' => 'hidden', 'id' => 'sale_job_id', 'class' => 'form-control', 'required' => false)); ?>
            <?php echo $this->Form->input('sale_job_child_id', array('type' => 'hidden', 'id'=>'sale_job_child_id', 'class' => 'form-control', 'required' => false)); ?>
        </div>
    </div>        
    <div class="form-group">
        <label class="col-sm-3">Date Required *</label>
        <div class="col-sm-9">
            <?php echo $this->Form->input('require_date', array('type' => 'text', 'id' => 'datepicker', 'class' => 'form-control', 'label' => false, 'autocomplete' => 'off')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3">Note (Optional)</label>
        <div class="col-sm-9">
            <?php echo $this->Form->input('note', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
        </div>
    </div>  

    <div class="form-group">
        <label class="col-sm-3">Status</label>
        <div class="col-sm-9">
            <?php $status = array(0 => 'Created (Save as draft)', 1 => 'Submit To Store'); ?>
            <?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
        </div>
    </div> 

    <div id="loadMoreBom"></div>

    <div class="form-group"> 
        <div class="col-sm-12"><p>Click Add Item and enter Item Code or Item name.</p></div>
        <div class="col-sm-12">
            <a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Items</a>
        </div>
    </div>

    <div class="form-group"> 
        <label class="col-sm-3"></label>
        <div class="col-sm-9">
            <?php echo $this->Form->submit('Send For Approval', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
        </div>
    </div>  



    <?php $this->Form->end(); ?>
 
</div>
</div>
</div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
    console.log(search);
    $('#findProduct'+row).autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct'+row).val() + "&ref=stock",                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            note: item.note,
                            unit: item.unit,
                            stocks: item.stocks
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#item_id'+row).val( ui.item.id );  
            $('#display'+row).val( ui.item.name );
            $('#unit'+row).val(ui.item.unit);
            $('#display_unit'+row).val(ui.item.unit);
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "<br/>"+item.note+"<br/>Onhand: "+item.stocks+"</small></div>" ).appendTo( ul );
    };
} 

$(document).ready(function() {
    $('#findJob').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'sale_jobs/ajaxfindjob',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findJob').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name,
                            customer: item.customer,
                            customer_id: item.customer_id,
                            sale_job_child_id: item.sale_job_child_id,
                            station: item.station
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#sale_job_id').val( ui.item.id ); 
            $('#sale_job_child_id').val( ui.item.sale_job_child_id ); 

        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>Customer: " + item.station + "</small></div>" ).appendTo( ul );
    };

    var station = 1;
    $('#addItem').click(function() {
        var html = '<div id="removeBom'+station+'">'; 
        html += '<div class="form-group">';  
        html += '<div class="col-sm-4" id="autoComplete">';
        html += '<input type="text" name="name[]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="item_id[]" id="item_id'+station+'">';
         
        html += '</div>'; 

        html += '<div class="col-sm-3">';
        html += '<input type="text" name="display[]" id="display'+station+'" class="form-control"readonly>'; 
        html += '</div>'; 

        html += '<div class="col-sm-2">';
        html += '<input type="text" name="quantity[]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
        html += '</div>'; 
        html += '<div class="col-sm-2">';
        html += '<input type="hidden" name="general_unit_id[]" id="unit'+station+'" class="form-control"required>';   
        html += '<select id="display_unit'+station+'" class="form-control"disabled>'; 
        html += '<option value="">-Unit-</option>';
        html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
        html += '</select>'

        html += '</div>'; 
        html += '<div class="col-sm-1">';
        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';    
        $("#loadMoreBom").append(html);  
        
        findItem(station, $(this).val());  
        station++; 
    });  
 
     
});
</script>
<?php $this->end(); ?>