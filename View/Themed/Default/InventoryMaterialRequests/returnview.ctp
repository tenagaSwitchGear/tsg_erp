<?php echo $this->Html->link(__('Add New RMA'), array('action' => 'returnitem'), array('class' => 'btn btn-success btn-sm')); ?> 

<?php $group = $this->Session->read('Auth.User.group_id'); ?>

<?php if($group == 1 || $group == 13) { ?>
	<?php echo $this->Html->link(__('RMA Lists'), array('action' => 'returnstore'), array('class' => 'btn btn-primary btn-sm')); ?> 
<?php } ?>
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Requested: <?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryMaterialRequests view-data">
<h2><?php echo __('Inventory Material Request'); ?></h2>
	<dl>
		 
		<dt><?php echo __('RMA No'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>
			&nbsp;
		</dd>
 
		<dt><?php echo __('Job No'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['User']['username'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['note']); ?>
			&nbsp;
		</dd>
 
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
<div class="related">
	<h3><?php echo __('Material Request Items'); ?></h3>
	<?php if (!empty($inventoryMaterialRequest['InventoryMaterialRequestItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr> 
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Item'); ?></th> 
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('UOM'); ?></th>  
	</tr>
	<?php foreach ($items as $inventoryMaterialRequestItem): ?>
		<tr> 
			<td><?php echo $inventoryMaterialRequestItem['InventoryItem']['code']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['InventoryItem']['name']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']; ?></td>
			<td><?php echo $inventoryMaterialRequestItem['GeneralUnit']['name']; ?></td> 
			 
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
 
</div>

</div>
</div>
</div>
</div>


<?php

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	} elseif($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'K.I.V';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>