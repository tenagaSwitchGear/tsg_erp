<?php echo $this->Html->link(__('My RMA'), array('action' => 'returnindex'), array('class' => 'btn btn-info btn-sm')); ?>
 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Return Material</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

    <p>Please fill form below to Return Material Advise (RMA). </p>
	<?php echo $this->Form->create('InventoryMaterialRequest', array('class' => 'form-horizontal')); ?>
		
    <div class="form-group">
        <label class="col-sm-3">Job No (Optional)</label>
        <div class="col-sm-9">
            <?php echo $this->Form->input('job', array('type' => 'text', 'id' => 'findJob', 'class' => 'form-control', 'label' => false)); ?>
            <?php echo $this->Form->input('sale_job_id', array('type' => 'hidden', 'id' => 'sale_job_id', 'class' => 'form-control', 'label' => false)); ?>
        </div>
    </div>      

    <div class="form-group">
        <label class="col-sm-3">Production Order No (Optional)</label>
        <div class="col-sm-9">
            <?php echo $this->Form->input('po', array('type' => 'text', 'id' => 'po', 'class' => 'form-control', 'label' => false)); ?>
            <?php echo $this->Form->input('production_order_id', array('type' => 'hidden', 'id' => 'production_order_id', 'class' => 'form-control', 'label' => false)); ?>
        </div>
    </div>   
 
	<div class="form-group">
		<label class="col-sm-3">Note</label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('note', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
		</div>
	</div>  
  

	<div id="loadMoreBom"></div>

    <div class="form-group"> 
        <div class="col-sm-12">
            <a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Items</a>
        </div>
    </div>


    <div class="form-group"> 
    	<label class="col-sm-3"></label>
		<div class="col-sm-9">
    		<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
    	</div>
    </div>	 

	<?php $this->Form->end(); ?>
 
</div>
</div>
</div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
    console.log(search);
    $('#findProduct'+row).autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct'+row).val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            code: item.code,
                            note: item.note 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#item_id'+row).val( ui.item.id ); 
            $('#name'+row).val( ui.item.name );
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br><small>"+item.note+"</small></div>" ).appendTo( ul );
    };
}
  

$(document).ready(function() {
    $('#findJob').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'sale_jobs/ajaxfindjob',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findJob').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name,
                            customer: item.customer,
                            customer_id: item.customer_id 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#sale_job_id').val( ui.item.id );  
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>Customer: " + item.customer + "</small></div>" ).appendTo( ul );
    };

    $('#po').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'production_orders/ajaxfindpo',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#po').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name,
                            bom_code: item.bom_code 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#production_order_id').val( ui.item.id );  
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>Item: " + item.bom_code + "</small></div>" ).appendTo( ul );
    };

    var station = 1;
    $('#addItem').click(function() {
        var html = '<div id="removeBom'+station+'">'; 
        html += '<div class="form-group">';  
        html += '<div class="col-sm-4" id="autoComplete">';
        html += '<input type="text" name="name[]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="item_id[]" id="item_id'+station+'">';
         
        html += '</div>'; 

        html += '<div class="col-sm-4">';
        html += '<input type="text" id="name'+station+'" class="form-control" placeholder=""readonly>';
         
        html += '</div>'; 

        html += '<div class="col-sm-1">';
        html += '<input type="text" name="quantity[]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
        html += '</div>'; 
        html += '<div class="col-sm-2">';
        html += '<select name="general_unit_id[]" class="form-control"required>'; 
        html += '<option value="">-Unit-</option>';
        html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
        html += '</select>'

        html += '</div>'; 
        html += '<div class="col-sm-1">';
        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';    
        $("#loadMoreBom").append(html);  
        
        findItem(station, $(this).val());  
        station++; 
    });  
});
</script>
<?php $this->end(); ?>