<?php echo $this->Html->link(__('Waiting'), array('action' => 'store'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Item List'), array('action' => 'itemlist'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Ready To Issue'), array('action' => 'store/3'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Partials'), array('action' => 'store/5'), array('class' => 'btn btn-warning btn-sm')); ?>
<?php echo $this->Html->link(__('Transfered'), array('action' => 'store/4'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Requested</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('InventoryMaterialRequest', array('action' => 'store', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr> 
		<td><?php echo $this->Form->input('ref', array('type' => 'text', 'placeholder' => 'MRN No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('user', array('type' => 'text', 'placeholder' => 'Username', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly_2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
<?php $this->end(); ?> 
<div class="table-responsive"> 
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th><?php echo $this->Paginator->sort('User.firstname', 'Name'); ?></th> 
			<th><?php echo $this->Paginator->sort('production_order_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_job_id', 'Job'); ?></th> 
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryMaterialRequests as $inventoryMaterialRequest): ?>
	<tr> 
		<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($inventoryMaterialRequest['User']['firstname'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id'])); ?></td> 
		<td>
			<?php echo $this->Html->link($inventoryMaterialRequest['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryMaterialRequest['ProductionOrder']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialRequest['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
		</td> 
		<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>&nbsp;</td>
		
		<td><?php echo type($inventoryMaterialRequest['InventoryMaterialRequest']['type']); ?>&nbsp;</td>
		<td><?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'storeview', $inventoryMaterialRequest['InventoryMaterialRequest']['id'])); ?> 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>


</div>
</div>
</div>
</div>


<?php

function status($status) {
	if($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Ready Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'Partials';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>