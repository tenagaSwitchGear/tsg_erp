<?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>

<?php 
if($inventoryMaterialRequest['InventoryMaterialRequest']['status'] != 7) { 
echo $this->Html->link(__('Cancel This MRN'), array('action' => 'verifyview', $inventoryMaterialRequest['InventoryMaterialRequest']['id'], 7), array('class' => 'btn btn-danger btn-sm')); 
}
?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Requested: <?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	

<div class="inventoryMaterialRequests view-data">
<h2><?php echo __('Inventory Material Request'); ?></h2>
	<dl> 
		<dt><?php echo __('MRN No'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Production Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryMaterialRequest['ProductionOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Job Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['SaleJobItem']['name'], array('controller' => 'sale_job_items', 'action' => 'view', $inventoryMaterialRequest['SaleJobItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['User']['username'] . ' (' . $inventoryMaterialRequest['User']['firstname']. ')', array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id']), array('target' => '_blank')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone No'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['User']['mobile_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo type($inventoryMaterialRequest['InventoryMaterialRequest']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
<div class="related">
	<h3><?php echo __('Request Items'); ?></h3>
	<?php if (!empty($inventoryMaterialRequest['InventoryMaterialRequestItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr> 
		<th><?php echo __('Item'); ?></th> 
		<th><?php echo __('Request Qty'); ?></th>
		<th><?php echo __('BOM Qty'); ?></th>
		<th><?php echo __('BOM Issued Qty'); ?></th>
		<th><?php echo __('BOM Issued Balance'); ?></th> 
		<th><?php echo __('Issued Quantity'); ?></th>
		<th><?php echo __('Issued Balance'); ?></th>
		<th><?php echo __('Demand'); ?></th>
		<th><?php echo __('Stock Qty'); ?></th> 
	</tr>
	<?php echo $this->Form->create('InventoryMaterialRequest', array('class' => 'form-horizontal')); ?> 
	<?php foreach ($items as $inventoryMaterialRequestItem): ?>
		<tr> 
			<td><?php echo h($inventoryMaterialRequestItem['InventoryItem']['name']); ?><br/>
			<small><?php echo h($inventoryMaterialRequestItem['InventoryItem']['code']); ?></small>
			</td> 
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?> <?php echo $inventoryMaterialRequestItem['GeneralUnit']['name']; ?></td>
			<?php if($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['production_order_child_id'] != 0) { ?>
				<td><?php echo _n2($inventoryMaterialRequestItem['ProductionOrderChild']['no_of_order'] * $inventoryMaterialRequestItem['ProductionOrderChild']['quantity']); ?></td>
				<td><?php echo _n2($inventoryMaterialRequestItem['ProductionOrderChild']['issued_qty']); ?></td>
				<td><?php echo _n2($inventoryMaterialRequestItem['ProductionOrderChild']['quantity_bal']); ?></td>
			<?php } else { ?>
				<td><?php echo _n2($inventoryMaterialRequestItem['ProductionOrderItem']['quantity_total']); ?></td>
				<td><?php echo _n2($inventoryMaterialRequestItem['ProductionOrderItem']['quantity_issued']); ?></td>
				<td><?php echo _n2($inventoryMaterialRequestItem['ProductionOrderItem']['quantity_bal']); ?></td>
			<?php } ?>
			
			 
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_quantity']); ?></td>
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']); ?></td>

			<td><?php echo $inventoryMaterialRequestItem['Stock']; ?></td>  
			<td><?php echo !empty($inventoryMaterialRequestItem['Demand']) ? $inventoryMaterialRequestItem['Demand'] : 0; ?></td>  
 
			 
			<td class="actions"> 
			<input type="hidden" name="item_id[]" value="<?php echo $inventoryMaterialRequestItem['InventoryItem']['id']; ?>">
			<input type="hidden" name="quantity[]" value="<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']; ?>">  
			</td>
			</td>
		</tr>
	<?php endforeach; ?>
	
	</table>  
	<?php
	$status = $inventoryMaterialRequest['InventoryMaterialRequest']['status'];
	if(in_array($status, array(1, 6))) { 
	?>
	<div class="form-group">
	<label class="col-sm-3">Verification Note (Optional if Reject Was Selected)</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('InventoryMaterialRequestVerification.note', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3">Status</label>
	<div class="col-sm-9">
		<?php $status = array('2' => 'Verify', '6' => 'Reject'); ?>
		<?php echo $this->Form->input('status', array('empty' => '-Select Status-', 'options' => $status, 'label' => false, 'class' => 'form-control')); ?>	
		</div>
		</div>
	<div class="form-group">
	<label class="col-sm-3"></label>
	<div class="col-sm-9">
		<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-danger pull-right')); ?>	
	</div>
	</div>
	<?php } else { ?> 
		<p class="label label-success">This MRN Has been verified.</p> 
	<?php } ?>

	<!-- Show verification history -->
	<?php if($verifications) { ?>
		<h4>Verification History</h4>
		<table cellpadding = "0" cellspacing = "0" class="table">
			<tr>
				<th>Remark</th> 
				<th>Created</th>
				<th>Status</th> 
				<th>User</th> 
			</tr>
			<?php foreach ($verifications as $verification) { ?>  
			<tr>
				<td><?php echo h($verification['InventoryMaterialRequestVerification']['note']); ?></td> 
				<td><?php echo $verification['InventoryMaterialRequestVerification']['created']; ?></td>
				<td><?php echo status($verification['InventoryMaterialRequestVerification']['status']); ?></td>
				<td><?php echo $verification['User']['username']; ?></td> 
			</tr>
			<?php } ?>
		</table>  
	<?php } ?>

	<?php $this->Form->end(); ?>

<?php endif; ?>
 
</div>

</div>
</div>
</div>
</div>


<?php

function status($status) {
	if($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'K.I.V';
	} elseif($status == 6) {
		$data = 'Rejected';
	} elseif($status == 7) {
		$data = 'Cancelled';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>