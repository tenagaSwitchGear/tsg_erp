<?php
function selected($id, $unit) {
	if($id == $unit) {
		return 'selected';
	}
}
?>

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit: <?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
 
<?php echo $this->Form->create('InventoryMaterialRequest', array('class' => 'form-horizontal')); ?> 
 	

	<?php echo $this->Form->input('id'); ?>   
<div class="form-group">
<label class="col-sm-3">Note</label>
<div class="col-sm-9">
	<?php echo $this->Form->input('note', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?> 
</div>
</div>

<div class="form-group">
<label class="col-sm-3">Date Required</label>
<div class="col-sm-9"> 
	<?php echo $this->Form->input('require_date', array('type' => 'text', 'id' => 'datepicker', 'class' => 'form-control', 'label' => false)); ?> 
</div>
</div>

<div class="form-group">
<label class="col-sm-3">Status</label>
<div class="col-sm-9"> 
	<?php $status = array(0 => 'Created (Save as draft)', 1 => 'Submit To Store'); ?>
	<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?> 
</div>
</div>

   

<div class="form-group">
        <h4 class="col-sm-12">Items</h4> 
    </div>  

<?php
if($items) { 
    foreach($items as $item) { ?>
    <div id="removeBom<?php echo $item['InventoryMaterialRequestItem']['id']; ?>"> 
		<div class="form-group">  
		<div class="col-sm-6" id="autoComplete">
		<input type="text" name="name[]" value="<?php echo $item['InventoryItem']['name']; ?>" id="editProduct<?php echo $item['InventoryMaterialRequestItem']['id']; ?>" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="item_id[]" value="<?php echo $item['InventoryItem']['id']; ?>" id="edit_item_id<?php echo $item['InventoryMaterialRequestItem']['id']; ?>">
		         
		</div> 
		<div class="col-sm-2">
		<input type="text" name="quantity[]" value="<?php echo $item['InventoryMaterialRequestItem']['quantity']; ?>" id="quantity<?php echo $item['InventoryMaterialRequestItem']['id']; ?>" class="form-control" placeholder="Qty"required>
		</div> 
		<div class="col-sm-3">
		<select name="general_unit_id[]" class="form-control"required> 
		<option value="">-Unit-</option>
		<?php foreach($units as $key => $unit) { ?>
		<option value="<?php echo $key; ?>" <?php echo selected($item['InventoryMaterialRequestItem']['general_unit_id'], $key); ?>>
		<?php echo $unit; ?></option><?php } ?>
		</select>'

		</div> 
		<div class="col-sm-1">
		<a href="#" class="btn btn-danger" onclick="removeBom(<?php echo $item['InventoryMaterialRequestItem']['id']; ?>); return false"><i class="fa fa-times"></i></a>
		</div>
		</div>
	</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).ready(function() {
	$("#editProduct<?php echo $item['InventoryMaterialRequestItem']['id']; ?>").autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $("#editProduct<?php echo $item['InventoryMaterialRequestItem']['id']; ?>").val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name,
                            code: item.code,
                            note: item.note 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $("#edit_item_id<?php echo $item['InventoryMaterialRequestItem']['id']; ?>").val( ui.item.id );  
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.code + "</small><br><small>"+item.note+"</small></div>" ).appendTo( ul );
    };
});
</script>
<?php $this->end(); ?>

<?php } } ?>

    <div id="loadMoreBom"></div>

    <div class="form-group"> 
        <div class="col-sm-12">
            <a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Items</a>
        </div>
    </div>
    <div class="form-group"> 
	<label class="col-sm-3"></label>
	<div class="col-sm-9">
		<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
	</div>
</div>	
<?php $this->Form->end(); ?>

</div>
</div>
</div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
    console.log(search);
    $('#findProduct'+row).autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct'+row).val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name,
                            code: item.code,
                            note: item.note 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#item_id'+row).val( ui.item.id );  
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.code + "</small><br><small>"+item.note+"</small></div>" ).appendTo( ul );
    };
}
  

$(document).ready(function() {
  	<?php $total = count($items) + 1; ?>
    var station = <?php echo $total; ?>;
    $('#addItem').click(function() {
        var html = '<div id="removeBom'+station+'">'; 
        html += '<div class="form-group">';  
        html += '<div class="col-sm-6" id="autoComplete">';
        html += '<input type="text" name="name[]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="item_id[]" id="item_id'+station+'">';
         
        html += '</div>'; 
        html += '<div class="col-sm-2">';
        html += '<input type="text" name="quantity[]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
        html += '</div>'; 
        html += '<div class="col-sm-3">';
        html += '<select name="general_unit_id[]" class="form-control"required>'; 
        html += '<option value="">-Unit-</option>';
        html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
        html += '</select>'

        html += '</div>'; 
        html += '<div class="col-sm-1">';
        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';    
        $("#loadMoreBom").append(html);  
        
        findItem(station, $(this).val());  
        station++; 
    });  
});
</script>
<?php $this->end(); ?>