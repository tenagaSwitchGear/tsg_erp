<?php echo $this->Html->link(__('Add New RMA'), array('action' => 'returnitem'), array('class' => 'btn btn-success btn-sm')); ?> 

<?php $group = $this->Session->read('Auth.User.group_id'); ?>

<?php if($group == 1 || $group == 13) { ?>
	<?php echo $this->Html->link(__('RMA Lists'), array('action' => 'returnstore'), array('class' => 'btn btn-primary btn-sm')); ?> 
<?php } ?>
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Return Material Advise: <?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryMaterialRequests view-data"> 
	<dl>
		 
		<dt><?php echo __('RMA No'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>
			&nbsp;
		</dd>
 
		<dt><?php echo __('Job No'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequest['User']['username'] . ' (' . $inventoryMaterialRequest['User']['firstname']. ')', array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id']), array('target' => '_blank')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone No'); ?></dt>
		<dd>
			<?php echo $inventoryMaterialRequest['User']['mobile_number']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['note']); ?>
			&nbsp;
		</dd>
 
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
<div class="related"> 
	<h4>Important Note:</h4>
	<p>1. Please Check each item before click Submitted to Stock button. Items mark as Uncheck will be ignore.</p>
	<p>2. Please make sure item Unit of Measurement (UOM) and Quantity is correct.</p>
	<?php if (!empty($inventoryMaterialRequest['InventoryMaterialRequestItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><input type="checkbox" name="checkall" id="checkall" value="1"> All</th> 
		<th><?php echo __('Code'); ?></th> 
		<th><?php echo __('Quantity'); ?></th> 
		<th><?php echo __('Accept Qty'); ?></th> 
		<th><?php echo __('Accept UOM'); ?></th> 
		<th><?php echo __('Default UOM'); ?></th>  
		<th><?php echo __('Warehouse'); ?></th>
		<th><?php echo __('Store'); ?></th>
		<th><?php echo __('Rack'); ?></th>
		<th><?php echo __('Price/Unit'); ?></th>
		<th><?php echo __('Action'); ?></th>
	</tr> 
	<?php echo $this->Form->create('InventoryMaterialRequest', array('class' => 'form-horizontal')); ?>
	<?php 
	$i = 0;
	foreach ($items as $inventoryMaterialRequestItem): ?>
		<tr>
		<td> 
		<input class="check" id="<?php echo $i; ?>" type="checkbox" name="data[InventoryMaterialRequest][check][]" value="1"> 
		<input id="check<?php echo $i; ?>" type="hidden" name="data[InventoryMaterialRequest][add][]" value="0">
		<input type="hidden" name="data[InventoryMaterialRequest][item_id][]" value="<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['inventory_item_id']; ?>">
		<input type="hidden" name="data[InventoryMaterialRequest][req_item_id][]" value="<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>">
		</td> 
		 
			<td><?php echo $inventoryMaterialRequestItem['InventoryItem']['code']; ?><br/>
			<small><?php echo $inventoryMaterialRequestItem['InventoryItem']['name']; ?></small>

			</td> 
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?> 
			<?php echo $inventoryMaterialRequestItem['GeneralUnit']['name']; ?></td> 
			<td>
<input type="text" name="data[InventoryMaterialRequest][quantity][]" value="<?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?>" class="form-control" style="width:70px;"required>
			</td>
			<td>
			<?php echo $this->Form->input('general_unit_id.', array('empty' => 'Select', 'options' => $units, 'class' => 'form-control', 'required' => false, 'label' => false, 'value' => $inventoryMaterialRequestItem['InventoryItem']['GeneralUnit']['id'])); ?>  
			</td> 
			<td><?php echo $inventoryMaterialRequestItem['GeneralUnit']['name']; ?></td>

			<td>
			<?php echo $this->Form->input('location.', array('empty' => 'Select', 'options' => $locations, 'class' => 'form-control', 'required' => true, 'label' => false, 'value' => 2, 'style' => 'width:100px')); ?>  
			</td> 

			<td>
			<?php echo $this->Form->input('store.', array('empty' => 'Select', 'options' => $stores, 'class' => 'form-control', 'required' => true, 'label' => false, 'value' => 1, 'style' => 'width:80px')); ?>  
			</td> 

			<td>
<input type="text" name="data[InventoryMaterialRequest][rack][]" class="form-control" value="<?php echo isset($inventoryMaterialRequestItem['Stock']['InventoryStock']) ? $inventoryMaterialRequestItem['Stock']['InventoryStock']['rack'] : ''; ?>" style="width:80px;" placeholder="Rack"required>
			</td>

			<td>
<input type="text" name="data[InventoryMaterialRequest][price_per_unit][]" class="form-control" value="<?php echo isset($inventoryMaterialRequestItem['Stock']['InventoryStock']) ? $inventoryMaterialRequestItem['Stock']['InventoryStock']['price_per_unit'] : ''; ?>" style="width:80px;" placeholder="Unit price"required>
			</td>
			
			<td><?php echo $this->Html->link('Edit', array('controller' => 'inventory_material_request_items', 'action' => 'edit', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'], $inventoryMaterialRequest['InventoryMaterialRequest']['id'], 'RMA'), array('class' => 'btn btn-warning btn-sm')); ?></td>	

		</tr>
	<?php 
	$i++; 
	endforeach; ?>

	</table>

	<?php  
		echo $this->Form->submit('Submit To Stock', array('id' => 'submit', 'class' => 'btn btn-success pull-right')); 
	 
	?>
	<?php $this->end(); ?>
<?php endif; ?>
 
</div>

</div>
</div>
</div>
</div>


<?php

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	} elseif($status == 1) {
		$data = 'Waiting Return Material';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'K.I.V';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
	$(document).ready(function() {

		$('#submit').click(function() {
			var checked = $("input[type=checkbox]:checked").length; 
			if(checked == 0) {
				alert("You must check at least one item.");
				return false;
			}  
		});

		$("#checkall").change(function () {  
		    $(".check").prop('checked', $(this).prop("checked"));  
		    $(".check").each(function() {  
				var getId = $(this).attr('id');
				var getVal = $("#check" + getId).val();
				if($(this).prop("checked") == true) {
					$("#check" + getId).val(1);
				} else {
					$("#check" + getId).val(0);
				}  
			});
		});

		$(".check").each(function() {
			$(this).change(function() {
				var getId = $(this).attr('id');
				var getVal = $("#check" + getId).val();
				if(getVal == 0) {
					$("#check" + getId).val(1);
				} else {
					$("#check" + getId).val(0);
				} 
			}); 
		});
	}); 
</script>
<?php $this->end(); ?>