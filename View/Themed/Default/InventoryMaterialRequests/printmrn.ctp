 
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		 
      	<div class="x_content"> 
  
 

<div class="related">
  
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('No'); ?></th> 
		<th><?php echo __('Location'); ?></th>
		<th><?php echo __('Code'); ?></th> 
		<th><?php echo __('Item'); ?></th> 
		<th><?php echo __('Ref'); ?></th>
		<th><?php echo __('Qty Available'); ?></th> 
		<th><?php echo __('Req. Qty'); ?></th> 
		<th><?php echo __('Issued Qty'); ?></th>
		<th><?php echo __('Issued Balance'); ?></th>  
		</tr>
	<?php echo $this->Form->create('InventoryMaterialRequestItem', array('class' => 'form-horizontal', 'id' => 'issuedMaterial')); ?>
	<?php 
	$i = 1; 
	foreach ($items as $inventoryMaterialRequestItem):  
	?>
		<tr>
			<td><?php echo $i; ?></td> 
			<td><?php 
			echo h($inventoryMaterialRequestItem['InventoryStore']['name']); ?> <?php echo h($inventoryMaterialRequestItem['InventoryStock']['rack']); ?></td>
			<td><?php echo h($inventoryMaterialRequestItem['InventoryItem']['code']); ?></td>
			<td><?php echo $this->Html->link($inventoryMaterialRequestItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialRequestItem['InventoryItem']['id']), array('target' => '_blank')); ?><br/>
			
			</td>
			<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequest']['code']); ?></td>  
			<td>
			<h4><?php  
			$stock_balance = _n2($inventoryMaterialRequestItem['StockBalance']);
			?>  
			<?php if($inventoryMaterialRequestItem['StockBalance'] == 0) { ?>
				<span class="label label-danger"><?php echo $stock_balance; ?></span>
			<?php } elseif($inventoryMaterialRequestItem['StockBalance'] >= $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']) { ?>
				<span class="label label-success"><?php echo $stock_balance; ?></span>
			<?php } else { ?>
				<span class="label label-warning"><?php echo $stock_balance; ?></span>
			<?php } ?>
			</h4>
			</td> <!-- it already reserved by HOD. So just show quantity --> 
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?><br/>
			<small><?php echo h($inventoryMaterialRequestItem['GeneralUnit']['name']); ?></small>
			</td>
			
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_quantity']); ?></td>
			<td><?php echo _n2($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']); ?></td> 
			<td>
			 
 
			</td> 			 
		</tr>
<?php $this->start('script'); ?> 
<script type="text/javascript"> 
var error = {};
$(document).ready(function() {
	var id = '<?php echo $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']; ?>';
	$('#issuedQty'+id).on('keyup', function() {
		var issued = $('#issuedQty'+id).val();
		var request = $('#reqQty'+id).val(); 
		if(issued > request) { 
			console.log(request + ' : ' + issued);
			$('#error'+id).html('Invalid Qty'); 
			error[id] = false;
		} 
		if(issued <= request) {
			console.log(request + ' : ' + issued);
			$('#error'+id).html('');
			error[id] = true;
		}
	});
});
</script>
<?php $this->end(); ?>

	<?php $i++; endforeach; ?>
	 
	<?php $this->Form->end(); ?>
	</table> 
 
</div>

 
</div>
</div>
</div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findDuplicate(value) {
    var result = 0;
    $(".findProduct").each(function(){
        if (this.value == value) {
            result++;
        }
    });
    return result - 1;
}

function removeBom(row) {
    $('#removeItem'+row).html('');
    return false;
}

var station = 1; 



function addField() { 
    var html = '<div id="removeItem'+station+'">'; 
      html += '<div class="form-group">';  
      html += '<div class="col-sm-10">';
      html += '<input type="text" name="item[]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Barcode" required="required"autofocus>'; 
      html += '</div>';
       

      html += '<div class="col-sm-1">';
      html += '<input type="text" name="item[]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
      html += '</div>'; 

      html += '<div class="col-sm-1">';
      html += '<a href="#" class="btn btn-danger" onclick="removeItem('+station+'); return false"><i class="fa fa-times"></i></a>';
      html += '</div>';
      html += '</div></div>';    
      $("#loadItem").append(html);   
     
    return false; 
}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
  clearTimeout (timer);
  timer = setTimeout(callback, ms);
 };
})();

$(function(){  
	$('.findProduct').each(function() {
		$(this).on('keyup', function() {
			delay( 
			function() { 
				addField();
			}, 1000);
		});
	}); 
});

$(document).ready(function() { 
	$('#issuedMaterial').on('submit', function(e) {
		e.preventDefault();
		console.log(error);
		return false;
	});
});
</script>
<?php $this->end(); ?>

<?php

function disable_field($status) {
	if($status == 4) {
		echo 'disabled="disabled"';
	}
}

function active($store, $id) {
	if($store != null) {
		if($store == $id) {
			return 'btn-primary';
		} else {
			return 'btn-default';
		}	
	} else {
		return 'btn-default';
	}
	
}

function shortage($qty, $req, $status, $issue_bal) {
	if($status != 4) {
		if($req > $qty || $issue_bal > $qty) {
			return 'red-bg';
		} else {
			return 'green-bg';
		}	
	} 
}

function status($status) {
	if($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'Partials';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>

 