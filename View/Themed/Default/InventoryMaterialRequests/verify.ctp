<?php echo $this->Html->link(__('MRN Verifications'), array('action' => 'verify'), array('class' => 'btn btn-info btn-sm')); ?> 

<?php echo $this->Html->link(__('Verified'), array('action' => 'verify/2'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Rejected'), array('action' => 'verify/6'), array('class' => 'btn btn-danger btn-sm')); ?> 

<?php echo $this->Html->link(__('Cancelled'), array('action' => 'verify/7'), array('class' => 'btn btn-warning btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Requested Verifications</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
    	<?php echo $this->Session->flash(); ?> 

    	<?php echo $this->Form->create('InventoryMaterialRequest', array('action' => 'verify', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr> 
		<td><?php echo $this->Form->input('ref', array('type' => 'text', 'placeholder' => 'MRN No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('user', array('type' => 'text', 'placeholder' => 'Username', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly_2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
<?php $this->end(); ?> 

		<div class="table-responsive"> 
			<table cellpadding="0" cellspacing="0" class="table">
			<thead>
			<tr>
				
				<th><?php echo $this->Paginator->sort('code'); ?></th>
				<th><?php echo $this->Paginator->sort('User.firstname', 'User'); ?></th>
				<th><?php echo $this->Paginator->sort('production_order_id'); ?></th>
				<th><?php echo $this->Paginator->sort('sale_job_child_id', 'Job'); ?></th> 
				<th><?php echo $this->Paginator->sort('created'); ?></th> 
				<th><?php echo $this->Paginator->sort('type'); ?></th>
				<th><?php echo $this->Paginator->sort('status'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($inventoryMaterialRequests as $inventoryMaterialRequest): ?>
			<tr>
				
				<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($inventoryMaterialRequest['User']['firstname'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id']), array('target' => '_blank')); ?>
				</td>
				<td>
					<?php echo $this->Html->link($inventoryMaterialRequest['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryMaterialRequest['ProductionOrder']['id'])); ?>
				</td>
				<td>
					<?php echo $this->Html->link($inventoryMaterialRequest['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['InventoryMaterialRequest']['sale_job_id'])); ?>
				</td> 
				<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>&nbsp;</td> 				<td><?php echo type($inventoryMaterialRequest['InventoryMaterialRequest']['type']); ?>&nbsp;</td>
				<td><?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'verifyview', $inventoryMaterialRequest['InventoryMaterialRequest']['id'])); ?> 
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
			</table>
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div>


</div>
</div>
</div>
</div>


<?php

function status($status) {
	if($status == 1) {
		$data = 'Waiting Verification';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'K.I.V';
	} elseif($status == 6) {
		$data = 'Rejected';
	} elseif($status == 7) {
		$data = 'Cancelled';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>