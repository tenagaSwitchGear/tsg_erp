 
<?php

if(!empty($this->params['pass'])) {
	$param = $this->params['pass'][0]; 
} else {
	$param = 0;
}
// var_dump($param);
?>

<?php $group = $this->Session->read('Auth.User.group_id'); ?>
<?php if($group == 1 || $group == 13) { ?>
	<?php echo $this->Html->link(__('RMA Lists'), array('action' => 'returnstore'), array('class' => 'btn btn-primary btn-sm')); ?> 
<?php } ?>

<?php echo $this->Html->link(__('RMA History'), array('action' => 'returnstore', 4), array('class' => 'btn btn-success btn-sm')); ?> 


<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
      		<?php if($param == 4) { ?>
        		<h2>Return Material Advise History</h2> 
      		<?php } else { ?>
      			<h2>Waiting Return Material Advise (RMA)</h2> 
      		<?php } ?>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
    	<?php echo $this->Session->flash(); ?> 
    	<?php echo $this->Form->create('InventoryMaterialRequest', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'RMA No', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('username', array('placeholder' => 'Username', 'class' => 'form-control', 'required' => false, 'id' => 'findUser', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('from', array('type' => 'text', 'placeholder' => 'From: YYYY-MM-DD', 'class' => 'form-control', 'required' => false, 'id' => 'dateonly', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('to', array('id' => 'dateonly_2', 'placeholder' => 'To: YYYY-MM-DD', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
		<div class="table-responsive"> 
			<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
			<thead>
			<tr> 
				<th><?php echo $this->Paginator->sort('code'); ?></th> 	
				<th><?php echo $this->Paginator->sort('User.firstname', 'User'); ?></th>			
				<th><?php echo $this->Paginator->sort('sale_job_id', 'Job No'); ?></th> 
				<th><?php echo $this->Paginator->sort('created'); ?></th> 
				
				<th><?php echo $this->Paginator->sort('status'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($inventoryMaterialRequests as $inventoryMaterialRequest): ?>
			<tr>
 				<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['code']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($inventoryMaterialRequest['User']['firstname'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequest['User']['id']), array('target' => '_blank')); ?>
				</td> 
				<td>
					<?php echo $this->Html->link($inventoryMaterialRequest['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $inventoryMaterialRequest['SaleJob']['id'])); ?>
				</td> 
				<td><?php echo h($inventoryMaterialRequest['InventoryMaterialRequest']['created']); ?>&nbsp;</td> 
				 
				<td><?php echo status($inventoryMaterialRequest['InventoryMaterialRequest']['status']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'returnviewrma', $inventoryMaterialRequest['InventoryMaterialRequest']['id'])); ?>
					 
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
			</table>
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div> 
	</div>
	</div>
</div>
</div>

<?php

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	} elseif($status == 1) {
		$data = 'Waiting Store';
	} elseif($status == 2) {
		$data = 'Verified';
	} elseif($status == 3) {
		$data = 'Waiting Issue Out';
	} elseif($status == 4) {
		$data = 'Transfered';
	} elseif($status == 5) {
		$data = 'K.I.V';
	} elseif($status == 6) {
		$data = 'Rejected';
	}
	return $data;
}

function type($status) {
	if($status == 1) {
		$data = 'Auto';
	} else {
		$data = 'Manual';
	}
	return $data;
}

?>