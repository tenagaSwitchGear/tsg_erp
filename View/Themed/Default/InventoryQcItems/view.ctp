<?php echo $this->Html->link(__('Incoming Inspection'), array('controller'=>'inventory_qc_items', 'action' => 'index'), array('class'=>'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm')); ?>
<style type="text/css">
	.vc{
		vertical-align: middle;
	}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php //echo $this->Html->link(__('List Purchase Orders'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Qc Inspection'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
            <div class="container table-responsive">
				<dl> 
					<dt class="col-sm-2"><?php echo __('PO No'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryQcItems['InventoryQcItem']['po_no']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Item'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryQcItems['InventoryItem']['name']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Code'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryQcItems['InventoryItem']['code']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryQcItems['InventoryQcItem']['quantity']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo date('d/m/Y', strtotime($inventoryQcItems['InventoryQcItem']['created'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity Passed'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryQcItems['InventoryQcItem']['quantity_pass']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity Rejected'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryQcItems['InventoryQcItem']['quantity_rejected']; ?>
						&nbsp;
					</dd>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
			<?php $total = $inventoryQcItems['InventoryQcItem']['quantity_pass'] + $inventoryQcItems['InventoryQcItem']['quantity_rejected']; ?>
			<?php if($total != $inventoryQcItems['InventoryQcItem']['quantity']) { ?>
			<h4><?php echo __('Actions'); ?></h4>
			<?php $qqq = $inventoryQcItems['InventoryQcItem']['quantity'] - $inventoryQcItems['InventoryQcItem']['quantity_pass'] - $inventoryQcItems['InventoryQcItem']['quantity_rejected']; ?>
			<table>
				<tr>
				<?php echo $this->Form->create('InventoryQcItem'); ?>
				<input type="hidden" name="data[InventoryQcItem][id]" value="<?php echo $inventoryQcItems['InventoryQcItem']['id']; ?>">
                <input type="hidden" name="data[InventoryQcItem][inventory_delivery_order_item]" value="<?php echo $inventoryQcItems['InventoryDeliveryOrderItem']['id']; ?>">
			<input type="hidden" name="data[InventoryQcItem][inventory_item_id]" value="<?php echo $inventoryQcItems['InventoryQcItem']['inventory_item_id']; ?>">
			<input type="hidden" name="data[InventoryQcItem][general_unit_id]" value="<?php echo $inventoryQcItems['InventoryQcItem']['general_unit_id']; ?>">
					<td class="col-sm-2 vc"><?php echo __('Quantity Passed'); ?></td>
					<td class="col-sm-8">
					<?php echo $this->Form->input("quantity_pass", array("type"=>"text", "value"=> $inventoryQcItems['InventoryQcItem']['quantity'], "min"=>"0", "class"=> "form-control", "label"=> false)); ?>
					</td>
					<td><?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?></td>
				</tr>
				
				<td><td>&nbsp;</td></td>			
				<tr>
					<td class="col-sm-2 vc"><?php echo __('Quantity Rejected'); ?></td>
					<td class="col-sm-8">
					<?php echo $this->Form->input("quantity_rejected", array("type"=>"text", "value"=> 0, "min"=>"0", "class"=> "form-control", "label"=> false, "onkeyup"=>"qc_remark(this.value)", "onchange" => "qc_remark(this.value)")); ?>
						</td>
					<td class="col-sm-4" id="remark" style="display: none">
					</td>
				</tr>
				<?php echo $this->Form->end(); ?>
			</table>
			<?php } ?>
			<div class="clearfix">&nbsp;</div>
			<?php echo $this->Form->create('InventoryQcItem', array('class' => 'form-horizontal form-label-left', 'type' => 'file')); ?>
			<input type="hidden" name="data[InventoryQcItem][id]" value="<?php echo $inventoryQcItems['InventoryQcItem']['id']; ?>">
            <input type="hidden" name="data[InventoryQcItem][inventory_delivery_order_item]" value="<?php echo $inventoryQcItems['InventoryDeliveryOrderItem']['id']; ?>">
			<input type="hidden" name="data[InventoryQcItem][inventory_item_id]" value="<?php echo $inventoryQcItems['InventoryQcItem']['inventory_item_id']; ?>">
			<input type="hidden" name="data[InventoryQcItem][general_unit_id]" value="<?php echo $inventoryQcItems['InventoryQcItem']['general_unit_id']; ?>">
			<input type="hidden" name="data[InventoryQcItem][quantity_rejected]" id="reject">
			<div id="remarks"></div>
			<div class="form-group" id="addremark" style="display: none">	
              	<div class="col-sm-12">
              		<?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
                   <a href="#" class="btn btn-primary btn-sm" onclick="addChild(); return false"><i class="fa fa-plus"></i></a>
              	</div> 
          	</div>
          	<?php echo $this->Form->end(); ?>
			<div class="clearfix">&nbsp;</div>
			
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
function qc_remark(nilai){
	var n = nilai;
	
	if(n != '' && n != '0'){
		$('#addremark').show();
		$('#reject').val(nilai);
	}else{
		$('#addremark').hide();
	}
}

var row = 0;
function addChild() {
    var html = '<div class="form-group"><div class="row">';

	html += '<div class="col-sm-4">';
	html += '<textarea name="data[InventoryQcItem][remark][]" class="form-control"></textarea>';
	html += '</div>';
	html += '<div class="col-sm-3">';
	html += '<select name="data[InventoryQcItem][comment_id][]" class="form-control"><option>Select Category</option>';
	html += '<?php foreach ($finishedgoodcomment as $key => $fgc){ ?>';
	html += '<option value="<?php echo $key; ?>"><?php echo $fgc; ?></option>';
	html += '<?php } ?></select>';
	html += '</div>';
	html += '<div class="col-sm-3">';
	html += '<input type="hidden" name="data[InventoryQcItem][attachment_dir][]" class="form-control">'
	html += '<input type="file" name="data[InventoryQcItem][attachment][]" class="form-control">';
	html += '</div>';
	html += '<div class="col-sm-1">';
	html += '<a class="btn btn-danger remove_remark"><i class="fa fa-times"></i></a>';
	html += '</div>';



	html += '</div></div>';

	row += 1;
	$('#remarks').append(html);
}

$(function() {
  	$(document).on("click",".remove_remark",function() {
  		if(confirm('Are you sure you want to remove this remark?'))
  		{
  			$(this).parent().parent().remove();		
  		}				
  	});
});
</script>
<?php $this->end(); ?>

