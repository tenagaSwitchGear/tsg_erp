<div class="inventoryDeliveryOrderItems form">
<?php echo $this->Form->create('InventoryDeliveryOrderItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Inventory Delivery Order Item'); ?></legend>
	<?php
		echo $this->Form->input('inventory_delivery_order_id');
		echo $this->Form->input('inventory_item_id');
		echo $this->Form->input('quantity');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('price');
		echo $this->Form->input('tax');
		echo $this->Form->input('total');
		echo $this->Form->input('general_tax_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Inventory Delivery Order Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Orders'), array('controller' => 'inventory_delivery_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order'), array('controller' => 'inventory_delivery_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Taxes'), array('controller' => 'general_taxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Tax'), array('controller' => 'general_taxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
	</ul>
</div>
