<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Rejected Items'), array('controller'=>'inventorystockrejecteds', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
        <?php //echo $this->Html->link(__('Pending'), array('controller'=>'inventoryQcItems', 'action' => 'pending'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php //echo $this->Html->link(__('Completed'), array('controller'=>'inventoryQcItems', 'action' => 'completed'), array('class'=>'btn btn-default btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Inventory Item'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Completed - QC Inspection'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<?php echo $this->Form->create('InventoryQcItem', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
            <table cellpadding="0" cellspacing="0" class="table">
                <tr>
                <td><?php echo $this->Form->input('po_no', array('placeholder' => 'PO Number', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_start', array('type' => 'text', 'placeholder' => 'From Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_end', array('type' => 'text', 'placeholder' => 'To Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker_2', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
                </tr>
            </table>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
							<th><?php echo $this->Paginator->sort('Items'); ?></th>
							<th><?php echo $this->Paginator->sort('PO Number'); ?></th>
							<th><?php echo $this->Paginator->sort('Created'); ?></th>
							<th><?php echo $this->Paginator->sort('Quantity'); ?></th>
							<th><?php echo $this->Paginator->sort('Quantity Passed'); ?></th>
							<th><?php echo $this->Paginator->sort('Quantity Rejected'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryQcItem']['page']) ? 1 : $this->Paginator->params['paging']['InventoryQcItem']['page']; $limit = $this->Paginator->params['paging']['InventoryQcItem']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryQcItems as $inventoryQcItems): 
					?>
					<tr>
						<td class="text-center"><?php echo $startSN++; ?></td>
						<td><?php echo $inventoryQcItems['InventoryQcItem']['name']; ?>&nbsp;</td>
						<td><?php echo $inventoryQcItems['InventoryQcItem']['po_no']; ?>&nbsp;</td>
						<td><?php echo date('d/m/Y', strtotime($inventoryQcItems['InventoryQcItem']['created'])); ?>&nbsp;</td>
						<td><?php echo $inventoryQcItems['InventoryQcItem']['quantity']; ?>&nbsp;</td>
						<td><?php echo $inventoryQcItems['InventoryQcItem']['quantity_pass']; ?>&nbsp;</td>
						<td><?php echo $inventoryQcItems['InventoryQcItem']['quantity_rejected']; ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryQcItems['InventoryQcItem']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryQcItems['InventoryQcItem']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryQcItems['InventoryQcItem']['name']).'"', $inventoryQcItems['InventoryQcItem']['id'])); ?>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

