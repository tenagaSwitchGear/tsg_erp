<style type="text/css">
.line {    
    border-bottom: 2px dotted #000;
    text-decoration: none;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List QC Items'), array('controller'=>'inventoryqcitems', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Stock Rejected'), array('controller'=>'inventorystockrejecteds', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Stock Rejected</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content">
      		<div class="container table-responsive">
      		
				<dl>
					<dt class="col-sm-2"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['id']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('PO No'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['po_no']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Item'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['name']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['quantity']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo date('d/m/Y', strtotime($inventoryStockRejected[0]['InventoryQcItem']['created'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity Passed'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['quantity_pass']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity Rejected'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['quantity_rejected']; ?>
						&nbsp;
					</dd>
				</dl>
				<h3><?php echo __('Remark'); ?></h3>
				<?php if (!empty($inventoryStockRejected[0]['InventoryQcRemark'])){ ?>

				<table cellpadding = "0" cellspacing = "0" class="table">
				<tr>
					<th><?php echo __('Remark'); ?></th>
					<th><?php echo __('Category'); ?></th>
					<th><?php echo __('Attachment'); ?></th>
					<th><?php echo __('Status'); ?></th>
				</tr>
				<?php foreach ($inventoryStockRejected[0]['InventoryQcRemark'] as $remark) { ?>
				<tr>
					<td><?php echo $remark['InventoryQcRemark']['remark']; ?></td>
					<td><?php echo $remark['Comment']['name']; ?></td>
					<td><?php echo $this->Html->link($remark['InventoryQcRemark']['attachment'], '/files/inventory_qc_remark/attachment/' . $remark['InventoryQcRemark']['attachment_dir'] . '/' . $remark['InventoryQcRemark']['attachment'], array('target' => '_blank', 'class' => 'line')); ?></td>
					<td></td>
				</tr>
				<?php } ?>

				</table>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
</div>
