<?php echo $this->Html->css('/js/libs/jstree/themes/apple/style.css'); ?>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List QC Items'), array('controller'=>'inventoryqcitems', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Stock Items'), array('controller'=>'inventorystocks', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Stock Rejecteds'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
            
        	<div class="table-responsive">
			<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
				<thead>
					<tr>
						<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
						<th><?php echo $this->Paginator->sort('Item'); ?></th>
						<th><?php echo $this->Paginator->sort('quantity'); ?></th>
                        <th><?php echo ('Type'); ?></th>
						<th><?php echo $this->Paginator->sort('status'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php 
						$currentPage = empty($this->Paginator->params['paging']['InventoryStockRejected']['page']) ? 1 : $this->Paginator->params['paging']['InventoryStockRejected']['page']; $limit = $this->Paginator->params['paging']['InventoryStockRejected']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryStockRejecteds as $inventoryStockRejected): 
						?>
					<tr>
						<td class="text-center"><?php echo $startSN++; ?>&nbsp;</td>
						<td><?php echo ($inventoryStockRejected['InventoryItem']['name']); ?><br><?php echo ($inventoryStockRejected['InventoryItem']['code']); ?>&nbsp;</td>
						<td><?php echo ($inventoryStockRejected['InventoryStockRejected']['quantity']); ?>&nbsp;</td>
                        <td><?php if($inventoryStockRejected['InventoryStockRejected']['type']=='1'){ echo "QC Inspection"; }else{ echo "Warehouse Inspection"; } ?>&nbsp;</td>
						<td><a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="status_<?php echo $inventoryStockRejected['InventoryStockRejected']['id']; ?>" class="editable" data-type="select" data-pk="<?php echo $inventoryStockRejected['InventoryStockRejected']['id']; ?>" data-name="#status_<?php echo $inventoryStockRejected['InventoryStockRejected']['id']; ?>" data-emptytext="Select Status" data-url="<?php echo BASE_URL.'inventorystockrejecteds/setStatus'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $inventoryStockRejected['InventoryStockRejected']['status']; ?>" data-source="<?php echo BASE_URL.'inventorystockrejecteds/getStatus'; ?>"></a></td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryStockRejected['InventoryStockRejected']['inventory_qc_item_id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php if($inventoryStockRejected['InventoryStockRejected']['status']=='0'){ ?>
							<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryStockRejected['InventoryStockRejected']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php } ?>
							<?php //echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryStockRejected['InventoryStockRejected']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryStockRejected['InventoryItem']['name']).'"', $inventoryStockRejected['InventoryStockRejected']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<p>
			<?php
				//echo $this->Paginator->counter(array(
				//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				//));
			?>	</p>
			<div class="paging">
			<?php
				echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
				echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
				echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
			?>
			</div>
		</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript">    
    $('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
        }
    });
</script>
<?php $this->end(); ?>