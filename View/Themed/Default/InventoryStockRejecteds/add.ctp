<div class="inventoryStockRejecteds form">
<?php echo $this->Form->create('InventoryStockRejected'); ?>
	<fieldset>
		<legend><?php echo __('Add Inventory Stock Rejected'); ?></legend>
	<?php
		echo $this->Form->input('inventory_stock_id');
		echo $this->Form->input('quantity');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Inventory Stock Rejecteds'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
	</ul>
</div>
