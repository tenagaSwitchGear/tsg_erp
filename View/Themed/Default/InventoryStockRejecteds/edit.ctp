<style type="text/css">
	.vc{
		padding-top: 12px
	}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php //echo $this->Html->link(__('List Inventory Item'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Stock Rejected'); ?></h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		
				<div class="inventoryStockRejecteds form">
				<?php echo $this->Form->create('InventoryStockRejected', array('class'=>'form-horizontal')); ?>
				<input type="hidden" name="id" value="<?php echo $this->request->data['InventoryStockRejected']['id']; ?>">
				<input type="hidden" name="inventory_item_id" value="<?php echo $this->request->data['InventoryStockRejected']['inventory_item_id']; ?>">
				<input type="hidden" name="quantity" value="<?php echo $this->request->data['InventoryStockRejected']['quantity']; ?>">
				<input type="hidden" name="inventory_qc_item_id" value="<?php echo $this->request->data['InventoryStockRejected']['inventory_qc_item_id']; ?>">
					<fieldset>
						<dt class="col-sm-2"><?php echo __('Id'); ?></dt>
						<dd class="col-sm-9">
							: #<?php echo $this->request->data['InventoryStockRejected']['id']; ?>
							&nbsp;
						</dd>
						<dt class="col-sm-2"><?php echo __('Item'); ?></dt>
						<dd class="col-sm-9">
							: <?php echo $this->request->data['InventoryItem']['name']; ?>
							&nbsp;
						</dd>
						<dt class="col-sm-2"><?php echo __('Quantity'); ?></dt>
						<dd class="col-sm-9">
							: <?php echo $this->request->data['InventoryStockRejected']['quantity']; ?>
							&nbsp;
						</dd>
						<dt class="col-sm-2"><?php echo __('General Unit'); ?></dt>
						<dd class="col-sm-9">
							: <?php echo $this->request->data['GeneralUnit']['name']; ?>
							&nbsp;
						</dd>
                        <dt class="col-sm-2"><?php echo __('Remark'); ?></dt>
                        <dd class="col-sm-9">
                            : <?php echo $this->request->data['FinishedGoodComment']['name']; ?>
                            &nbsp;
                        </dd>
						<dt class="col-sm-2 vc"><?php echo __('Status'); ?></dt>
						<dd class="col-sm-2">
							<select class="form-control" name="status">
								<option value="0">Pending</option>
								<option value="1">Replaced</option>
							</select>
							&nbsp;
						</dd>

					</fieldset>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>