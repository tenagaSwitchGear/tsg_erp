<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Create New Quotation</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('SaleQuotation', array('class'=>'form-horizontal')); ?> 
        <div class="form-group">
        	<label class="col-sm-2">Enter Tender Name / No</label>
        	<div class="col-sm-10">
        		<?php echo $this->Form->input('find_tender', array('id' => 'findTender', 'class' => 'form-control', 'label' => false)); ?>
        		<?php echo $this->Form->input('sale_tender_id', array('id' => 'tenderId', 'type' => 'hidden', 'label' => false)); ?>
        		<?php echo $this->Form->input('customer_id', array('type' => 'hidden', 'label' => false)); ?>
        	</div>
        </div>   

        <div class="form-group">
            <label class="col-sm-2">Quotation No (Unique)</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('name', array('type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        </div>

        <div class="form-group">
        	<label class="col-sm-2">Remark</label>
        	<div class="col-sm-10">
        		<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
        	</div>
        </div>

        <div class="form-group">
        	<label class="col-sm-2">Status</label>
        	<div class="col-sm-10">
        		<?php $status = array(0 => 'Draft', 1 => 'Active'); ?> 
        		<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
        		<?php echo $this->Form->input('hod_status', array('type' => 'hidden', 'value' => 0)); ?>
        		<?php echo $this->Form->input('gm_status', array('type' => 'hidden', 'value' => 0)); ?> 
        	</div>
        </div>  
        
        <!--<div class="form-group"> 
            <div class="col-sm-4">
                <?php echo $this->Form->input('find_bom', array('options' => $bomcategories, 'id' => 'bomCategory0', 'class' => 'form-control', 'label' => false, 'onchange' => 'findBomByCategory(0)', 'empty' => '-Select Category-')); ?>
            </div>
            <div class="col-sm-4">
                <?php echo $this->Form->input('bom_id.', array('options' => array(), 'id' => 'moreBomId0', 'class' => 'form-control', 'label' => false, 'empty' => '-Select Finished Goods-')); ?>
            </div>
            <div class="col-sm-1">
                <?php echo $this->Form->input('no_of_order.', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Qty')); ?>
            </div>

            <div class="col-sm-3">
                <?php echo $this->Form->input('general_unit_id.', array('options' => $units, 'class' => 'form-control', 'label' => false, 'placeholder' => 'Qty', 'empty' => '-Select Unit-')); ?>
            </div> 
        </div> -->
        <div class="form-group">
            <h4 class="col-sm-12">Exchange Rate <b><?php echo Configure::read('Site.default_currency'); ?>1</b> agains X</h4> 
        </div>  
        <table class="table">
        <?php foreach($currencies as $currency) { ?> 
            <tr>
                <td><?php echo $currency['GeneralCurrency']['name']; ?></td>
                <td>
                    <?php echo $currency['GeneralCurrency']['symbol']; ?>
                </td>
                <td>
                    <?php echo $currency['GeneralCurrency']['iso_code']; ?>
                </td>
                <td>
                    <?php echo $this->Form->input('SaleQuotationCurrency.general_currency_id.', array('value' => $currency['GeneralCurrency']['id'], 'type' => 'hidden', 'label' => false)); ?>
                    <?php echo $this->Form->input('SaleQuotationCurrency.rate.', array('class' => 'form-control', 'label' => false)); ?>
                </td> 
            </tr>  
        <?php } ?>
        </table>

        <div class="form-group">
            <h4 class="col-sm-12">Items</h4> 
        </div>  

        <div id="loadMoreBom"></div>

        <div class="form-group"> 
            <div class="col-sm-12">
                <a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Items</a>
            </div>
        </div>
 

        <div class="form-group"> 
        		<?php echo $this->Form->submit('Next Step', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>
			 
        </div>	 
		
		<?php $this->Form->end(); ?>
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
    console.log(search);
    $('#findProduct'+row).autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'products/ajaxfindproduct',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct'+row).val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name + ' - ' + item.type,
                            price: item.price,
                            bom_id: item.bom_id
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#productId'+row).val( ui.item.id ); 
            $('#price'+row).val( ui.item.price ); 
            $('#total'+row).val( ui.item.price ); 
            $('#bom_id'+row).val( ui.item.bom_id ); 
            $('#quantity'+row).val(1);
        },
        minLength: 3
    });
}
  

$(document).ready(function() {
    $("#findTender").autocomplete({
        source: function (request, response){
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'saletenders/ajaxfindtender',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findTender').val(),                                                    
                success: function (data) {
                    console.log(data);
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.tender_no,
                            tender_no: item.tender_no,
                            title: item.title,
                            customer: item.customer,
                            closing_date: item.closing_date 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $("#tenderId").val(ui.item.id);//Put Id in a hidden field
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.tender_no + "<br><small>" + item.customer + " - "+ item.title +"</small><br>" +  "</div>" ).appendTo( ul );
    };
    

    var station = 1;
    $('#addItem').click(function() {
        var html = '<div id="removeBom'+station+'">'; 
        html += '<div class="form-group">';  
        html += '<div class="col-sm-4" id="autoComplete">';
        html += '<input type="text" name="data[SaleQuotationItem][name][]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="data[SaleQuotationItem][product_id][]" id="productId'+station+'">';
        html += '<input type="hidden" name="data[SaleQuotationItem][bom_id]" id="bom_id'+station+'">';
        html += '</div>';
     
        html += '<div class="col-sm-1">';
        html += '<input type="text" name="data[SaleQuotationItem][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
        html += '</div>';

        html += '<div class="col-sm-2">';
        html += '<select name="data[SaleQuotationItem][general_unit_id][]" class="form-control"required>'; 
        html += '<option value="">-Unit-</option>';
        html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
        html += '</select></div>';

        html += '<div class="col-sm-2"><input type="text" name="data[SaleQuotationItem][unit_price][]" class="form-control" id="price'+station+'" placeholder="Price/Unit" required></div>'; 
        html += '<div class="col-sm-1"><input type="text" name="data[SaleQuotationItem][discount][]" value="" class="form-control" placeholder="Discount"></div>';  
        html += '<div class="col-sm-1"><input type="text" name="data[SaleQuotationItem][tax][]" value="" class="form-control" placeholder="Tax"></div>';  
      

        html += '<div class="col-sm-1">';
        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';    
        $("#loadMoreBom").append(html);  
        
        findItem(station, $(this).val());  
        station++; 
    });  
});
</script>
<?php $this->end(); ?>


