<<<<<<< HEAD
<div class="projectBoms index">
	<h2><?php echo __('Project Boms'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('bom_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th><?php echo $this->Paginator->sort('remark'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_tender_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_order_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_order_child_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectBoms as $projectBom): ?>
	<tr>
		<td><?php echo h($projectBom['ProjectBom']['id']); ?>&nbsp;</td>
		<td><?php echo h($projectBom['ProjectBom']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projectBom['Bom']['name'], array('controller' => 'boms', 'action' => 'view', $projectBom['Bom']['id'])); ?>
		</td>
		<td><?php echo h($projectBom['ProjectBom']['name']); ?>&nbsp;</td>
		<td><?php echo h($projectBom['ProjectBom']['code']); ?>&nbsp;</td>
		<td><?php echo h($projectBom['ProjectBom']['remark']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projectBom['SaleTender']['title'], array('controller' => 'sale_tenders', 'action' => 'view', $projectBom['SaleTender']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projectBom['SaleOrder']['id'], array('controller' => 'sale_orders', 'action' => 'view', $projectBom['SaleOrder']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projectBom['SaleOrderChild']['name'], array('controller' => 'sale_order_children', 'action' => 'view', $projectBom['SaleOrderChild']['id'])); ?>
		</td>
		<td><?php echo h($projectBom['ProjectBom']['created']); ?>&nbsp;</td>
		<td><?php echo h($projectBom['ProjectBom']['modified']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projectBom['User']['id'], array('controller' => 'users', 'action' => 'view', $projectBom['User']['id'])); ?>
		</td>
		<td><?php echo h($projectBom['ProjectBom']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectBom['ProjectBom']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectBom['ProjectBom']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectBom['ProjectBom']['id']), array(), __('Are you sure you want to delete # %s?', $projectBom['ProjectBom']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Boms'), array('controller' => 'boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom'), array('controller' => 'boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Children'), array('controller' => 'sale_order_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Child'), array('controller' => 'sale_order_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisition Boms'), array('controller' => 'inventory_purchase_requisition_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Bom'), array('controller' => 'inventory_purchase_requisition_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Children'), array('controller' => 'project_bom_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Child'), array('controller' => 'project_bom_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Items'), array('controller' => 'project_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedules'), array('controller' => 'project_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule'), array('controller' => 'project_schedules', 'action' => 'add')); ?> </li>
	</ul>
</div>
=======
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Sales Order'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Sales Tender'), array('controller'=>'sale_tenders', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Project Boms'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
							<th><?php echo $this->Paginator->sort('sale_tender_id'); ?></th>
							<th><?php echo $this->Paginator->sort('created'); ?></th>
							<th><?php echo $this->Paginator->sort('modified'); ?></th>
							<th><?php echo $this->Paginator->sort('user_id'); ?></th>
							<th><?php echo $this->Paginator->sort('status'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['ProjectBom']['page']) ? 1 : $this->Paginator->params['paging']['ProjectBom']['page']; $limit = $this->Paginator->params['paging']['ProjectBom']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($projectBoms as $projectBom):
					?>
					<tr>
						<td class="text-center"><?php echo $startSN++; ?></td>
						<td>
							<?php echo $this->Html->link($projectBom['SaleTender']['title'], array('controller' => 'sale_tenders', 'action' => 'view', $projectBom['SaleTender']['id'])); ?>
						</td>
						<td><?php echo h($projectBom['ProjectBom']['created']); ?>&nbsp;</td>
						<td><?php echo h($projectBom['ProjectBom']['modified']); ?>&nbsp;</td>
						<td>
							<?php echo $this->Html->link($projectBom['User']['id'], array('controller' => 'users', 'action' => 'view', $projectBom['User']['id'])); ?>
						</td>
						<td><?php echo h($projectBom['ProjectBom']['status']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $projectBom['ProjectBom']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $projectBom['ProjectBom']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $projectBom['ProjectBom']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($projectBom['ProjectBom']['name']).'"', $projectBom['ProjectBom']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled  btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'form-horizontal'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled  btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

>>>>>>> d239dc7a08b060d3c15e1171f0b3bdc2f6eb936c
