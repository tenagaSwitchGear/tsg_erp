<div class="saleHandingOverUsers form">
<?php echo $this->Form->create('SaleHandingOverUser'); ?>
	<fieldset>
		<legend><?php echo __('Add Sale Handing Over User'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('receive_date');
		echo $this->Form->input('status');
		echo $this->Form->input('sale_handing_over_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sale Handing Over Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Handing Overs'), array('controller' => 'sale_handing_overs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Handing Over'), array('controller' => 'sale_handing_overs', 'action' => 'add')); ?> </li>
	</ul>
</div>
