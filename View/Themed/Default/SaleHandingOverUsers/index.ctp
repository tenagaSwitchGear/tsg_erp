<div class="saleHandingOverUsers index">
	<h2><?php echo __('Sale Handing Over Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('receive_date'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_handing_over_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleHandingOverUsers as $saleHandingOverUser): ?>
	<tr>
		<td><?php echo h($saleHandingOverUser['SaleHandingOverUser']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleHandingOverUser['User']['id'], array('controller' => 'users', 'action' => 'view', $saleHandingOverUser['User']['id'])); ?>
		</td>
		<td><?php echo h($saleHandingOverUser['SaleHandingOverUser']['receive_date']); ?>&nbsp;</td>
		<td><?php echo h($saleHandingOverUser['SaleHandingOverUser']['status']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleHandingOverUser['SaleHandingOver']['id'], array('controller' => 'sale_handing_overs', 'action' => 'view', $saleHandingOverUser['SaleHandingOver']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleHandingOverUser['SaleHandingOverUser']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleHandingOverUser['SaleHandingOverUser']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleHandingOverUser['SaleHandingOverUser']['id']), array(), __('Are you sure you want to delete # %s?', $saleHandingOverUser['SaleHandingOverUser']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sale Handing Over User'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Handing Overs'), array('controller' => 'sale_handing_overs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Handing Over'), array('controller' => 'sale_handing_overs', 'action' => 'add')); ?> </li>
	</ul>
</div>
