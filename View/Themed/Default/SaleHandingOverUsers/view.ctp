<div class="saleHandingOverUsers view">
<h2><?php echo __('Sale Handing Over User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleHandingOverUser['SaleHandingOverUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleHandingOverUser['User']['id'], array('controller' => 'users', 'action' => 'view', $saleHandingOverUser['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Receive Date'); ?></dt>
		<dd>
			<?php echo h($saleHandingOverUser['SaleHandingOverUser']['receive_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($saleHandingOverUser['SaleHandingOverUser']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Handing Over'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleHandingOverUser['SaleHandingOver']['id'], array('controller' => 'sale_handing_overs', 'action' => 'view', $saleHandingOverUser['SaleHandingOver']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Handing Over User'), array('action' => 'edit', $saleHandingOverUser['SaleHandingOverUser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Handing Over User'), array('action' => 'delete', $saleHandingOverUser['SaleHandingOverUser']['id']), array(), __('Are you sure you want to delete # %s?', $saleHandingOverUser['SaleHandingOverUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Handing Over Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Handing Over User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Handing Overs'), array('controller' => 'sale_handing_overs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Handing Over'), array('controller' => 'sale_handing_overs', 'action' => 'add')); ?> </li>
	</ul>
</div>
