<?php //echo $this->Html->link('Activity Logs', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>   

<div class="row"> 
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Material Transfer Report</h2> 
                <div class="clearfix">&nbsp;</div>
            </div>
            <div class="x_content">
                <table cellpadding="0" cellspacing="0" class="table table-bordered">
                    <tr>
                        <th class="text-center">#</th>
                        <th>Item</th>
                    </tr>
                    <?php 
                    $currentPage = empty($this->Paginator->params['paging']['UserLog']['page']) ? 1 : $this->Paginator->params['paging']['UserLog']['page']; 
                    $limit = $this->Paginator->params['paging']['UserLog']['limit'];
                    $startSN = (($currentPage * $limit) + 1) - $limit;
                    foreach($material_history as $item){ ?>
                    <tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td><?php echo $this->Html->link($item['InventoryItem']['InventoryItem']['name'], array('action' => 'view_record', $item['InventoryItem']['InventoryItem']['id'])); ?></td>
                    </tr>
                    <?php } ?>
                </table>
                <ul class="pagination">
                <?php
                  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
                  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                ?>
                </ul>
            </div>
        </div>
    </div>
    </div>
</div>
