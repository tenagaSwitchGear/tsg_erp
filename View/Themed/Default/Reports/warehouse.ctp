<?php //echo $this->Html->link('Activity Logs', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>   

<div class="row"> 
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Warehouse Report</h2> 
                <div class="clearfix">&nbsp;</div>
            </div>
            <div class="x_content">
                <?php echo $this->Session->flash(); ?>
                <?php if(isset($items)){ ?>
                
                <?php } ?>
                <table cellpadding="0" cellspacing="0" class="table">
                    <?php echo $this->Form->create('warehouse', array('class' => 'form-horizontal', 'type' => 'POST')); ?>
                    <tr>
                        <td>Date</td>
                        <td><?php echo $this->Form->input('date_start', array('type' => 'text', 'placeholder' => 'From Date', 'class' => 'form-control', 'required' => false, 'id' => 'dateonly', 'label' => false)); ?>  
                        </td>
                        <td><?php echo $this->Form->input('date_end', array('type' => 'text', 'placeholder' => 'To Date', 'class' => 'form-control', 'required' => false, 'id' => 'dateonly_2', 'label' => false)); ?>   
                        </td>
                    </tr>
                    <tr>
                        <td>Item Code</td>
                        <td><?php echo $this->Form->input('code_start', array('id' => 'findProduct', 'placeholder' => 'From Item Code', 'class' => 'form-control', 'required' => false, 'label' => false, 'onkeydown' => 'findItem(this.value)')); ?>  
                        </td>
                        <td><?php echo $this->Form->input('code_end', array('id' => 'findProduct2', 'type' => 'text', 'placeholder' => 'To Item Code', 'class' => 'form-control', 'required' => false, 'label' => false, 'onkeydown' => 'findItem2(this.value)')); ?>  
                        </td>
                    </tr>
                    <tr>
                        <td>Item Category</td>
                        <td colspan="2"><?php echo $this->Form->input('category_id', array('empty'=>array(0 =>'Select Category'), 'options'=>$category, 'class' => 'form-control', 'required' => false, 'label' => false)); ?></td>
                    </tr>
                    <tr><td colspan="3"><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'warehouse', 'class' => 'btn btn-success pull-right')); ?></td></tr>
                    <?php echo $this->Form->end(); ?> 
                </table>
            </div>
            <?php if(isset($items)){ ?>
                
            <div class="x_content">
                <table class="table table-hovered table-bordered" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2">Item Code</th>
                            <th rowspan="2">Description</th>
                            <th rowspan="2">UOM</th>
                            <th>Begin Stock</th>
                            <th>Stock In</th>
                            <th>Stock Out</th>
                            <th>End Stock</th>
                        </tr>
                        <tr>
                            <th>Quantity</th>
                            <th>Quantity</th>
                            <th>Quantity</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($items as $item) { ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $item['item']['code']; ?></td>
                            <td><?php echo $item['item']['name']; ?></td>
                            <td><?php echo $item['item']['general_unit']; ?></td>
                            <td class="text-right"><?php echo $item['begin_stock']; ?></td>
                            <td class="text-right"><?php echo $item['in_stock']; ?></td>
                            <td class="text-right"><?php echo $item['issued_quantity']; ?></td>
                            <td class="text-right"><?php echo $item['end_stock']; ?></td>
                        </tr>
                        <?php $i++; } ?>
                    </tbody>
                    
                </table>
            </div>
            <?php } ?>
        </div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript"> 

function findItem(search) { 
    //console.log(search);
    $('#findProduct').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditemreport',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            price: item.price,
                            bom_id: item.bom.id,
                            bom_name: item.bom.name,
                            bom_code: item.bom.code
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            
        },
        minLength: 3 
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>BOM: " + item.bom_code + "</small><br>" +  "</div>" ).appendTo( ul );
    };
}

function findItem2(search) { 
    //console.log(search);
    $('#findProduct2').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditemreport',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct2').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            price: item.price,
                            bom_id: item.bom.id,
                            bom_name: item.bom.name,
                            bom_code: item.bom.code
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            
        },
        minLength: 3 
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>BOM: " + item.bom_code + "</small><br>" +  "</div>" ).appendTo( ul );
    };
}

</script>
<?php $this->end(); ?>
