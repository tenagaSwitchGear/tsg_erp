<?php //echo $this->Html->link('Activity Logs', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>   

<div class="row"> 
	<?php echo $this->Session->flash(); ?>
  <?php 
  $data = array(
    $sale_quotation[0]['Draft'],
    $sale_quotation[0]['WaitingPlanning'],
    $sale_quotation[0]['CostingAdded'],
    $sale_quotation[0]['WaitingApproval'],
    $sale_quotation[0]['Rejected'],
    $sale_quotation[0]['Approved'],
    $sale_quotation[0]['QuotationSubmit'],
    $sale_quotation[0]['Award'],
    $sale_quotation[0]['Failed']
  ); ?>
<div class="col-md-12 col-sm-12 col-xs-12">


<div class="x_panel">
  <div class="x_title">
    <h2>Sales Report</h2> 
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <?php echo $this->Form->create('Reports', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
    <table cellpadding="0" cellspacing="0" class="table table-bordered">
        <tr>
        <td><?php echo $this->Form->input('years', array('placeholder' => 'Year (Ie: 2017)', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
        </td>
        <td><?php echo $this->Form->input('job_no', array('type' => 'text', 'placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
        </td>  
        <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
        </tr>
    </table>
    <?php $this->end(); ?> 
  </div>
</div>
<div class="x_panel">
  <div class="x_title">
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <div class="text-center">
        <?php echo $this->Html->link(__('January'), array('action' => 'sales_month?mth=1'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('February'), array('action' => 'sales_month?mth=2'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('March'), array('action' => 'sales_month?mth=3'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('April'), array('action' => 'sales_month?mth=4'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('May'), array('action' => 'sales_month?mth=5'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('June'), array('action' => 'sales_month?mth=6'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('July'), array('action' => 'sales_month?mth=7'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('August'), array('action' => 'sales_month?mth=8'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('September'), array('action' => 'sales_month?mth=9'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('October'), array('action' => 'sales_month?mth=10'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('November'), array('action' => 'sales_month?mth=11'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('December'), array('action' => 'sales_month?mth=12'), array('class' => 'btn btn-success btn-sm')); ?>        
    </div>
  </div>
</div>
<div class="x_panel">
  <div class="x_title">
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <div id="area-chart" ></div>
  </div>
</div>

<div class="x_panel">
  <div class="x_title">
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
  <input type="hidden" id="Draft" value="<?php echo $sale_quotation[0]['Draft']; ?>">
  <input type="hidden" id="WaitingPlanning" value="<?php echo $sale_quotation[0]['WaitingPlanning']; ?>">
  <input type="hidden" id="CostingAdded" value="<?php echo $sale_quotation[0]['CostingAdded']; ?>">
  <input type="hidden" id="WaitingApproval" value="<?php echo $sale_quotation[0]['WaitingApproval']; ?>">
  <input type="hidden" id="Rejected" value="<?php echo $sale_quotation[0]['Rejected']; ?>">
  <input type="hidden" id="Approved" value="<?php echo $sale_quotation[0]['Approved']; ?>">
  <input type="hidden" id="QuotationSubmit" value="<?php echo $sale_quotation[0]['QuotationSubmit']; ?>">
  <input type="hidden" id="Award" value="<?php echo $sale_quotation[0]['Award']; ?>">
  <input type="hidden" id="Failed" value="<?php echo $sale_quotation[0]['Failed']; ?>">
  <input type="hidden" id="mx" value="<?php echo max($data)+'5'; ?>">

    <canvas id="sale_chart" style="width: 100%; height: 300px;"></canvas>
  </div>
</div>
</div>

</div>
<?php $this->start('script'); ?>
<?php echo $this->Html->script(array(
	'/js/libs/Chart.js/Chart.js',
    '/js/libs/morris/morris.min.js',
    '/js/libs/raphael/raphael.min.js'
)); ?>  
<script type="text/javascript">
var ctx = document.getElementById("sale_chart");

var draft = document.getElementById("Draft").value;
var wp = document.getElementById("WaitingPlanning").value;
var ca = document.getElementById("CostingAdded").value;
var wa = document.getElementById("WaitingApproval").value;
var rej = document.getElementById("Rejected").value;
var app = document.getElementById("Approved").value;
var qs = document.getElementById("QuotationSubmit").value;
var awd = document.getElementById("Award").value;
var fd = document.getElementById("Failed").value;
var mx = document.getElementById("mx").value;
var new_max = 2 * Math.round(parseInt(mx)/2);

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Draft","Waiting Planning","Costing Added","Waiting Approval","Rejected","Approved","Quotation Submitted","Award","Failed"],
        datasets: [{
            label: 'Sale Quoations',
            data: [draft,wp,ca,wa,rej,app,qs,awd,fd],
            backgroundColor: [
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)',
                'rgba(0,128,128, 0.5)'
            ],
            borderColor: [
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)',
                'rgba(0,128,128, 1)'
            ],
            borderWidth: 1,
        }]
    },
    options: {
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Status'
                }
            }],
            yAxes: [{
                display: true,
                    ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 5,
                    max: new_max
                },
            }]
        }
    }
});
</script>
<script type="text/javascript">
var data_chart = <?php echo json_encode($sale_quotation_2) ?>;
var data = data_chart,
    config = {
        element: 'area-chart',
      data: data,
      xkey: 'Months',
      ykeys: ['Submit', 'Award', 'Reject'],
      labels: ['Submit', 'Award', 'Reject'],
      fillOpacity: 0.6,
      hideHover: 'auto',
      behaveLikeLine: true,
      resize: true,
      pointFillColors:['#ffffff'],
      pointStrokeColors: ['black'],
      lineColors:['gray','green', 'red'],
      parseTime:false,
      xLabels:"month"
  };
Morris.Area(config);
</script>
<?php $this->end(); ?>