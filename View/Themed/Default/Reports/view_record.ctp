<?php //echo $this->Html->link('Activity Logs', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>   

<div class="row"> 
    <div class="col-xs-12" >
        <div class="x_panel">
            <div class="x_title">
                <h2>Material Transfer Record</h2> 
                <div class="clearfix">&nbsp;</div>
            </div>
            <div class="x_content">
                <dt class="col-sm-3"><?php echo __('Item'); ?></dt>
                <dd class="col-sm-9">
                    <?php echo $item[0]['InventoryItem']['name']; ?>
                    &nbsp;
                </dd>
                <div>&nbsp;</div>
                <table cellpadding="0" cellspacing="0" class="table table-bordered">
                    <tr>
                        <th>Date</th>
                        <th>Transfer Type</th>
                        <th>Quantity</th>
                    </tr>
                    <?php foreach($history as $htry){ ?>
                    <tr>
                        <td><?php echo date('Y-m-d', strtotime($htry['InventoryMaterialHistory']['created'])); ?></td>
                        <td><?php if($htry['InventoryMaterialHistory']['type']==1){ echo 'Warehouse to Warehouse'; }else if($htry['InventoryMaterialHistory']['type']==2){ echo 'Location to Location'; }else if($htry['InventoryMaterialHistory']['type']==3){ echo 'Code to Code'; }else if($htry['InventoryMaterialHistory']['type']==4){ echo 'Rack to Rack'; }else{ echo $htry['InventoryMaterialHistory']['type']; } ?></td>
                        <td><?php echo $htry['InventoryMaterialHistory']['quantity']; ?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
