<?php //echo $this->Html->link('Purchase Planning', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
<?php //echo $this->Html->link('Job Lists', array('action' => 'job'), array('class' => 'btn btn-default')); ?>  
<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>GRN Report</h2> 
        		<?php echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'view_grn', $grns[0]['InventoryDeliveryOrder']['InventoryDeliveryOrder']['id']), array('class' => 'btn btn-success pull-right btn-sm', 'escape'=>false)); ?>
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
        		<?php echo $this->Session->flash(); ?>
        		
        		<table class="table borderless" cellspacing="0" cellpadding="0">
        			<thead>
        				<tr style="border-bottom: 0px">
        					<td style="border-bottom: 0px"><strong>Receipt No</strong></td>
        					<td style="border-bottom: 0px">: <?php echo $grns[0]['InventoryDeliveryOrder']['InventoryDeliveryOrder']['do_no']; ?></td>
        					<td style="border-bottom: 0px"><strong>Receipt Date</strong></td>
        					<td style="border-bottom: 0px">: <?php echo date('d/m/Y', strtotime($grns[0]['InventoryDeliveryOrder']['InventoryDeliveryOrder']['dateline'])); ?></td>
        					<td style="border-bottom: 0px"><strong>Warehouse</strong></td>
        					<td style="border-bottom: 0px">: <?php echo $grns[0]['InventoryDeliveryOrder']['InventoryLocation']['name']; ?></td>
        				</tr>
        				<tr style="border-top: 0px">
        					<td style="border-top: 0px"><strong>Supplier</strong></td>
        					<td style="border-top: 0px">: <?php echo $grns[0]['InventorySupplier']['code']; ?></td>
        					<td style="border-top: 0px" colspan="2"><?php echo $grns[0]['InventorySupplier']['name']; ?></td>
        					<td style="border-top: 0px"><strong>PO Number</strong></td>
        					<td style="border-top: 0px">: <?php echo $grns[0]['InventoryDeliveryOrder']['InventoryDeliveryOrder']['po_no']; ?></td>
        				</tr>
        			</thead>
        		</table>
        		<table class="table table-bordered">
        			<thead>
        				<tr>
        					<th class="text-center">No</th>
        					<th>Item Code &amp; Description</th>
        					<th>UOM</th>
        					<th>Received</th>
        					<th>Accepted</th>
        					<th>Rejected</th>
        					<th>Location</th>
        				</tr>
        			</thead>
        			<tbody>
        				<?php $bil = 1; ?>
        				<?php foreach ($grns as $grn) { ?>
        				<tr>
        					<td class="text-center"><?php echo $bil; ?></td>
        					<td><?php echo $grn['InventoryItem']['InventoryItem']['code']; ?><br><?php echo $grn['InventoryItem']['InventoryItem']['name']; ?></td>
        					<td><?php echo $grn['InventoryItem']['GeneralUnit']['name']; ?></td>
        					<td><?php echo number_format($grn['InventoryDeliveryOrderItem']['quantity_delivered'], 2); ?></td>
        					<td><?php echo number_format($grn['InventoryQcItem']['quantity_pass'], 2); ?></td>
        					<td><?php echo number_format($grn['InventoryQcItem']['quantity_rejected'], 2); ?></td>
        					<td><?php if(empty($grn['InventoryRack']['name'])){}else{ echo $grn['InventoryRack']['name']; } ?></td>
        				</tr>
        				<?php $bil++; } ?>
        			</tbody>
        		</table>
        		<?php echo $this->Html->link(__('Back'), array('controller'=>'inventory_delivery_orders', 'action' => 'index'), array('class' => 'btn btn-warning btn-sm')); ?>
        	</div>
        </div>
    </div>
</div>
