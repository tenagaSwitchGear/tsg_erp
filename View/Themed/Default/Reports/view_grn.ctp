<style type="text/css">
    .tbl td, th {padding: 6px;}
</style>
<div class="book page-break">
    <div class="page">
        <div id="header">
            <div class="x_title">
                <h5><?php echo strtoupper(Configure::read('Site.company_name')); ?></h5>
                <h6 class="text-center">Good Receipt Note</h6>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="content" class="container">
            <table class="table borderless" cellspacing="0" cellpadding="0">
                <thead>
                    <tr style="border-bottom: 0px">
                        <td style="border-bottom: 0px"><strong>Receipt No</strong></td>
                        <td style="border-bottom: 0px">: <?php echo $grns[0]['InventoryDeliveryOrder']['InventoryDeliveryOrder']['do_no']; ?></td>
                        <td style="border-bottom: 0px"><strong>Receipt Date</strong></td>
                        <td style="border-bottom: 0px">: <?php echo date('d/m/Y', strtotime($grns[0]['InventoryDeliveryOrder']['InventoryDeliveryOrder']['dateline'])); ?></td>
                        <td style="border-bottom: 0px"><strong>Warehouse</strong></td>
                        <td style="border-bottom: 0px">: <?php echo $grns[0]['InventoryDeliveryOrder']['InventoryLocation']['name']; ?></td>
                    </tr>
                    <tr style="border-top: 0px">
                        <td style="border-top: 0px"><strong>Supplier</strong></td>
                        <td style="border-top: 0px">: <?php echo $grns[0]['InventorySupplier']['code']; ?></td>
                        <td style="border-top: 0px" colspan="2"><?php echo $grns[0]['InventorySupplier']['name']; ?></td>
                        <td style="border-top: 0px"><strong>PO Number</strong></td>
                        <td style="border-top: 0px">: <?php echo $grns[0]['InventoryDeliveryOrder']['InventoryDeliveryOrder']['po_no']; ?></td>
                    </tr>
                </thead>
            </table>
            <table class="tbl" style="width: 100%;" cellpadding="5" cellspacing="5">
                <thead style="border-top: 1px dashed black; border-bottom: 1px dashed black;">
                    <tr>
                        <th class="text-center" style="border-right: 1px dashed black;"> No</th>
                        <th style="padding-left: 5px;"> Item Code &amp; Description</th>
                        <th style="border-right: 1px dashed black;">&nbsp;</th>
                        <th style="border-right: 1px dashed black; padding-left: 5px;"> UOM</th>
                        <th style="border-right: 1px dashed black; padding-left: 5px;"> Received</th>
                        <th style="border-right: 1px dashed black; padding-left: 5px;"> Accepted</th>
                        <th style="border-right: 1px dashed black; padding-left: 5px;"> Rejected</th>
                        <th style="padding-left: 5px;"> Location</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $bil = 1; ?>
                    <?php foreach ($grns as $grn) { ?>
                    <tr>
                        <td class="text-center" style="border-right: 1px dashed black;"><?php echo $bil; ?></td>
                        <td style="padding-left: 5px; border-right: 0px !important"><?php echo $grn['InventoryItem']['InventoryItem']['code']; ?></td>
                        <td style="border-left: 0px !important; border-right: 1px dashed black;"><?php echo $grn['InventoryItem']['InventoryItem']['name']; ?></td>
                        <td style="border-right: 1px dashed black; padding-left: 5px;"><?php echo $grn['InventoryItem']['GeneralUnit']['name']; ?></td>
                        <td style="border-right: 1px dashed black; padding-left: 5px;"><?php echo number_format($grn['InventoryDeliveryOrderItem']['quantity_delivered'], 2); ?></td>
                        <td style="border-right: 1px dashed black; padding-left: 5px;"><?php echo number_format($grn['InventoryQcItem']['quantity_pass'], 2); ?></td>
                        <td style="border-right: 1px dashed black; padding-left: 5px;"><?php echo number_format($grn['InventoryQcItem']['quantity_rejected'], 2); ?></td>
                        <td style="padding-left: 5px;"><?php if(empty($grn['InventoryRack']['name'])){}else{ echo $grn['InventoryRack']['name']; } ?></td>
                    </tr>
                    <?php $bil++; } ?>
                </tbody>
            </table>
            <div class="footer" style="margin-top: 300px !important; position: relative; padding-top: 0px; bottom: 0px !important;">
                <table class="table table-bordered" style="width: 50%; float: right">
                    <tbody>
                        <tr>
                            <td></td>
                            <td class="text-center">Signature</td>
                            <td class="text-center">Date</td>
                        </tr>
                        <tr>
                            <td rowspan="3">Received By:<br>(Store)</td>
                            <td rowspan="3" class="text-center"><br><br>.............................</td>
                            <td rowspan="3" class="text-center"><br><br>.............................</td>
                        </tr>
                    </tbody>
                </table>        
            </div>
        </div>
    </div>
</div>
