<?php //echo $this->Html->link('Activity Logs', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>   

<div class="row"> 
	<?php echo $this->Session->flash(); ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Sales Report - <?php echo $sale_quotation[0]['Months']; ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table cellpadding="0" cellspacing="0" class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2">Quotation Submitted</th>
                        </tr>
                        <tr>
                            <th class="col-md-1 text-center">ID</th>
                            <th>Quotation No</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($sale_quotation[0]['Submit'] as $submit){ ?>
                        <tr>
                            <td class="text-center"><?php echo $submit['SaleQuotation']['id']; ?></td>
                            <td><?php echo $this->Html->link($submit['SaleQuotation']['name'], array('controller'=>'sale_quotations', 'action' => 'view', $submit['SaleQuotation']['id'])); ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <br/>
                <table cellpadding="0" cellspacing="0" class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2">Awarded</th>
                        </tr>
                        <tr>
                            <th class="col-md-1 text-center">ID</th>
                            <th>Quotation No</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($sale_quotation[0]['Award'] as $award){ ?>
                        <tr>
                            <td class="text-center"><?php echo $award['SaleQuotation']['id']; ?></td>
                            <td><?php echo $this->Html->link($award['SaleQuotation']['name'], array('controller'=>'sale_quotations', 'action' => 'view', $award['SaleQuotation']['id'])); ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <br/>
                <table cellpadding="0" cellspacing="0" class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2">Rejected / Failed</th>
                        </tr>
                        <tr>
                            <th class="col-md-1 text-center">ID</th>
                            <th>Quotation No</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($sale_quotation[0]['Reject'] as $reject){ ?>
                        <tr>
                            <td class="text-center"><?php echo $reject['SaleQuotation']['id']; ?></td>
                            <td><?php echo $this->Html->link($reject['SaleQuotation']['name'], array('controller'=>'sale_quotations', 'action' => 'view', $reject['SaleQuotation']['id'])); ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>