<?php echo $this->Html->link('Activity Logs', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>   

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Activity Reports</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 

	 
	<?php echo $this->Session->flash(); ?>
 
	<?php echo $this->Form->create('UserLog', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Activity', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('from', array('type' => 'text', 'id' => 'dateonly', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('to', array('type' => 'text', 'id' => 'dateonly_2', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>

	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr> 
		<th><?php echo $this->Paginator->sort('title', 'Subject'); ?></th>  
		<th><?php echo $this->Paginator->sort('user_id', 'User'); ?></th>   
		<th>Full Name</th>  
		<th><?php echo $this->Paginator->sort('created'); ?></th>  
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($logs as $log): ?>
	<tr> 
		<td><?php echo h($log['UserLog']['name']); ?></td> 
		<td><?php echo h($log['User']['username']); ?></td> 
		<td><?php echo h($log['User']['firstname']); ?></td>
		<td><?php echo h($log['UserLog']['created']); ?></td>  
		<td class="actions"> 
			<a href="<?php echo BASE_URL . $log['UserLog']['link']; ?>"><i class="fa fa-search"></i> View</a>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
 
</div>
</div>
</div>
</div>

 
