<?php //echo $this->Html->link('Purchase Planning', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
<?php //echo $this->Html->link('Job Lists', array('action' => 'job'), array('class' => 'btn btn-default')); ?>  
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Progress Report</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create('Reports', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr>
		<td><?php echo $this->Form->input('years', array('placeholder' => 'Year (Ie: 2017)', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('job_no', array('type' => 'text', 'placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>  
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 
 	
<div class="related">
<h3>Job List On: <?php echo __($year); ?></h3> 
<?php  foreach ($stations as $station): ?>
<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover">
	<tr class="bg-info">
		<th colspan="7"><h4><strong><?php echo date('M', strtotime($station['SaleJobChild']['fat_date'])); ?></strong></h4></th>
	</tr>
	<tr>
		<th class="text-center"><?php echo __('#'); ?></th>
		<th><?php echo __('Job'); ?></th> 
		<th colspan="2"><?php echo __('Job No'); ?></th>    
		<th colspan="2"><?php echo __('FAT Date'); ?></th> 
		<th colspan="2"><?php echo __('Delivery Date'); ?></th> 
	</tr>
	<?php $counter = 1;
	 foreach ($station['Jobs'] as $salejobs): ?>
		<tr>
			<td class="text-center"><?php echo $counter; ?></td>
			<td><strong><?php echo $salejobs['SaleJobChild']['name']; ?></strong><br/><small><?php echo $salejobs['SaleJobChild']['station_name']; ?></small></td>
			<td colspan="2"><?php echo $station['SaleJobNo']['SaleJob']['name']; ?></td>
			<td colspan="2"><?php echo date('d/m/Y', strtotime($salejobs['SaleJobChild']['fat_date'])); ?></td>
			<td colspan="2"><?php echo date('d/m/Y', strtotime($salejobs['SaleJobChild']['delivery_date'])); ?></td>
		</tr>
		<tr>
			<td colspan="7">
				<table class="table table-bordered">
					<tr class="bg-danger">  
						<th width="280px">Item</th>
						<th>Qty</th>
						<th>Production</th>  
						<th>Inspection</th> 
						<th>FAT</th> 
						<th>Packing</th>  
						<th>Delivery</th>  
						<th>LC/Invoice</th>  
					</tr>
					<?php foreach($salejobs['SaleJobItem'] as $jobsitem): ?>
						<tr>  
							<td width="280px"><strong><?php echo $jobsitem['InventoryItem']['name']; ?></strong><br/><small><?php echo $jobsitem['InventoryItem']['code']; ?></small></td>
							<td><?php echo $jobsitem['SaleJobItem']['quantity']; ?></td>
							<td><?php echo $jobsitem['FinishedGood']; ?></td>  
							<td><?php echo $jobsitem['Qac']; ?></td> 
							<td><?php echo $jobsitem['Fat']; ?></td> 
							<td><?php echo $jobsitem['Packing']; ?></td>  
							<td><?php echo $jobsitem['Delivery']; ?></td>  
							<td>lc/invoice</td>  
						</tr>
					<?php endforeach; ?>
				</table>
			</td> 
		</tr>
	<?php $counter++; endforeach;  ?>
</table>
 <?php endforeach; ?>
</div>

</div>
</div>
</div>
</div>





<?php 

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	}
	if($status == 1) {
		$data = 'Submitted For Approval';
	}
	if($status == 2) {
		$data = 'Approved';
	}
	if($status == 3) {
		$data = 'Rejected';
	}
	if($status == 4) {
		$data = 'Submitted To Procurement';
	} 
	if($status == 5) {
		$data = 'Purchased';
	} 
	return $data;
}

?>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).on('focus', '.date', function() {
    $(this).datepicker({
	  dateFormat: 'yy-mm-dd', 
	  ampm: true
	});
}); 

$(document).ready(function() { 
	$('#dateonly').on('change', function() {
		var date = $('#dateonly').val();
		$('.date').each(function() {
			$(this).val(date);
		});
	});
});
</script>
<?php $this->end(); ?>