<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Edit Supplier Item'), array('action' => 'edit', $inventorySupplierItem['InventorySupplierItem']['id']), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Purchase Requisitions'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Inventory Supplier Item'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
			<div class="container table-responsive">
				<dl>
					<dt class="col-sm-3"><?php echo __('Supplier'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplierItem['InventorySupplier']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						: #<?php echo h($inventorySupplierItem['InventorySupplierItem']['id']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Inventory Item'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $this->Html->link($inventorySupplierItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventorySupplierItem['InventoryItem']['id'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Price'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplierItem['InventorySupplierItem']['price']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Price Quantity'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplierItem['InventorySupplierItem']['price_per_unit']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('General Unit'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $this->Html->link($inventorySupplierItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventorySupplierItem['GeneralUnit']['id'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplierItem['InventorySupplierItem']['created']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplierItem['InventorySupplierItem']['modified']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Expiry Date'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplierItem['InventorySupplierItem']['expiry_date']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Remark'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventorySupplierItem['InventorySupplierItem']['remark']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
