<?php echo $this->Html->link(__('Item General'), array('controller'=>'inventory_items', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Item Categories'), array('controller' => 'inventory_item_categories', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Price Books'), array('controller' => 'inventory_supplier_items', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add Price Book'); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="inventoryItems form">
			<?php if($id != 0){ ?>
			<input type="hidden" id="item_id" name="item_id" value="<?php echo $id; ?>">
			<?php } ?>
			<?php echo $this->Form->create('InventorySupplierItem', array('class'=>'form-horizontal', 'type' => 'file')); ?>
				 		
						<div class="form-group"> 
							<label class="col-sm-3">Item *</label>
							<div class="col-sm-4">
							<?php echo $this->Form->input("InventoryItem.code", array("id" => "find_item", "class"=> "form-control", "label"=> false, "placeholder" => "Item Code / Name")); ?>
							</div> 
							<div class="col-sm-1 text-center"><h3>OR</h3></div> 
							<div class="col-sm-4"> 
								<?php echo $this->Form->input("inventory_item_id", array("empty" => "-Select Item-", "id" => "inventory_item_id", "class"=> "form-control", "label"=> false, "onchange" => "find_default_supplier();")); ?> 
							</div>
						</div>
 
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Price *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("price", array("type" => "text", "class"=> "form-control", "placeholder"=> "Price", "label"=> false)); ?>
							</div> 
							<label class="col-sm-3" style="padding-top: 8px">Purchase Unit *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("general_unit_id", array("class"=> "form-control", "label"=> false, "id" => "general_unit_id")); ?>
							</div> 
						</div>  
						

						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Conversion Qty (1 Box = 100 pcs) *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("conversion_qty", array("type" => "text", "class"=> "form-control", "placeholder"=> "Conversion Qty", "label"=> false, "value" => 1)); ?>
							</div> 
							<label class="col-sm-3" style="padding-top: 8px">Inventory UOM *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("conversion_unit_id", array("options" => $generalUnits, "class"=> "form-control", "label"=> false, "id" => "unit")); ?>
							</div> 
						</div>  

						<div class="form-group"> 
							<label class="col-sm-3" style="padding-top: 8px">Min Order *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("min_order", array("type" => "text", "class"=> "form-control", "placeholder"=> "Min Order Qty", "label"=> false)); ?>
							</div>  
							<label class="col-sm-3" style="padding-top: 8px">Min Order Unit *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("min_order_unit", array("options" => $generalUnits, "class"=> "form-control", "label"=> false, "id" => "min_order_unit")); ?>
							</div> 
						</div>

						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Supplier *</label>
							<div class="col-sm-3">
								<?php echo $this->Form->input("inventory_supplier_id", array("options" => $InventorySuppliers, "class"=> "form-control", "label"=> false, "id" => "inventory_supplier_id")); ?>
							</div> 
							<label class="col-sm-3" style="padding-top: 8px">Currency *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("general_currency_id", array("options" => $currencies, "class"=> "form-control", "label"=> false, "id" => "general_currency_id")); ?>
							</div> 
							
						</div>
						 
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Lead Time (Day) *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("lead_time", array("type" => "text", "class"=> "form-control", "label"=> false, "placeholder" => "No of The Day")); ?>
							</div> 
						 
							<label class="col-sm-3" style="padding-top: 8px">Ref / Quotation No *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("quotation_number", array("class"=> "form-control", "label"=> false, "placeholder" => "Supplier Ref / Quotation No")); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Default Supplier *</label>
							<div class="col-sm-3">
							<?php $default = array(0 => 'No', 1 => 'Yes - Choose this will ovewrite current default supplier'); ?>
							<?php echo $this->Form->input("is_default", array("options" => $default, "class"=> "form-control", "label"=> false)); ?>
							</div>  
							<label class="col-sm-3" style="padding-top: 8px">Part No (Item code by Supplier)</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("part_number", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>

						

						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Effective Date *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("effective_date", array("type" => "text", "id" => "dateonly", "class"=> "form-control", "placeholder"=> "Start from date", "label"=> false, 'value' => date('Y-m-d'))); ?>
							</div>  
							<label class="col-sm-3" style="padding-top: 8px">Expiry Date *</label>
							<div class="col-sm-3">
							<?php echo $this->Form->input("expiry_date", array("type"=>"text", "id"=>"dateonly_2", "class"=> "form-control", "placeholder"=> "Expiry date", "label"=> false)); ?>
							</div> 
						</div>
						 
						<div class="form-group" id="supplierList"></div>
						
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Remark</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("remark", array("class"=> "form-control", "placeholder"=> "Remark", "label"=> false)); ?>
							</div> 
						</div>
 
						<?php echo $this->Form->input("dir", array("type"=>"hidden")); ?>
	                    <div class="clearfix">&nbsp;</div>
                    	<h4><?php echo __('Quotation Attachment (Optional)'); ?></h4>
                    	<div><?php echo $this->Form->input("quotation", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?></div>
				<div class="clearfix">&nbsp;</div>
 
				<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success')); ?>
 
 
			</div>
        	<!-- content end -->
        	<?php echo $this->Form->end(); ?>
      	</div>
    </div>
    </div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
 

function is_default(status) {
	if(status == 1) {
		return 'Yes';
	} else {
		return 'No';
	}
}

function find_default_supplier() {
	$.ajax({
        type: "GET",                        
        url:baseUrl + 'inventory_supplier_items/ajaxfinddefaultsupplier',           
        contentType: "application/json",
        dataType: "json",
        data: "inventory_item_id=" + $('#inventory_item_id').val(),                                                    
        success: function (data) { 
        	var html = '';
        	if(data.length != 0) {
        		html += '<h4>This data comparison for decision making purpose.</h4>';
	        	html += '<table class="table table-bordered">';
	        	html += '<tr>';
			    html += '<th>Supplier</th>';
			    html += '<th>Default</th>';
			    html += '<th>Lead Time</th>'; 
			    html += '<th>Price</th>';
			    html += '<th>Unit</th>';
			    html += '<th>Conversion Qty</th>';
			    html += '<th>Price/Pcs</th>'; 
			    html += '<th>Min Order</th>'; 
			    html += '<th>Expiry Date</th>';  
			    html += '</tr>';
	        	$.each(data, function(i, item) { 
	        		html += '<tr>';
				    html += '<td>' + item.supplier + '</td>';
				    html += '<td>' + is_default(item.default) + '</td>';
				    html += '<td>' + item.lead_time + '</td>'; 
				    html += '<td>' + item.price + '</td>';
				    html += '<td>' + item.unit + '</td>';
				    html += '<td>' + item.conversion_qty + '</td>';
				    html += '<td>' + item.price_per_unit + '</td>'; 
				    html += '<td>' + item.min_order + '</td>'; 
				    html += '<td><label class="label label-' + item.label + '">' + item.expiry_date + '</label></td>';  
				    html += '</tr>';
				});
				html += '</table>';	
        	} else {
        		html += '<h4>No other supplier found.</h4>';
        	} 
			$('#supplierList').html(html);
    	}
    });
}

$(document).ready(function() { 

	$("#inventory_supplier_id").on('change', function() {
		$.ajax({
            type: "GET",                        
            url: baseUrl + 'inventory_suppliers/ajaxfindsuppliercurrency',           
            contentType: "application/json",
            dataType: "json",
            data: "term=" + $('#inventory_supplier_id').val(),                                                    
            success: function (data) { 
            	console.log(data);
            	if(data) {
            		$('#general_currency_id').val(data.InventorySupplier.general_currency_id);
            	}
        	}
        });
	});

	$("#general_unit_id").on('change', function() {
		var purchase_unit = $("#general_unit_id").val();
		$('#min_order_unit').val(purchase_unit);
	});

	var itemId = $("#item_id").val();
	$("#inventory_item_id").val(itemId);
	
    $("#find_item").autocomplete({
	    source: function (request, response) {
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'inventory_items/ajaxfinditem',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#find_item').val(),                                                    
	            success: function (data) {
	            	 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.code,
		                    name: item.name, 
		                    note: item.note, 
		                    unit: item.unit 
		                }
		            }));
	        	}
	        });
	    },
	    select: function (event, ui) {
	        $("#inventory_item_id").val(ui.item.id); 
	        $("#unit").val(ui.item.unit); 
	        find_default_supplier();
	    },
	    minLength: 3

	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br>" +  "</div>" ).appendTo( ul );
    }; 
});  
</script>
<?php $this->end(); ?>