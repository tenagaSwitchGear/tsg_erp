<?php

function isdefault($status) {
	if($status == 0) {
		return 'No';
	} else {
		return 'Yes';
	}
}

function label_expiry($expiry) {
	if(date('Y-m-d') > date('Y-m-d', strtotime($expiry)) ){
			return 'danger';
	} else {
		return 'success';
	}
}

?>

<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Add New Price Book'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
 
 
  		<?php echo $this->Html->link(__('Items General'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
 
  		<?php echo $this->Html->link(__('Suppliers'), array('controller'=>'inventory_suppliers', 'action' => 'index'), array('class'=>'btn btn-primary btn-sm')); ?> 
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Price Book</h2>
                <?php if(isset($_GET['name']) && $_GET['name']!=''){ $name = $_GET['name']; }else{ $name = ''; } ?>
                <?php if(isset($_GET['part_number']) && $_GET['part_number']!=''){ $part_number = $_GET['part_number']; }else{ $part_number = ''; } ?>
                <?php if(isset($_GET['inventory_supplier_id']) && $_GET['inventory_supplier_id']!=''){ $inventory_supplier_id = $_GET['inventory_supplier_id']; }else{ $inventory_supplier_id = ''; } ?>
                <?php if(isset($_GET['quotation_number']) && $_GET['quotation_number']!=''){ $quotation_number = $_GET['quotation_number']; }else{ $quotation_number = ''; } ?>
                <?php if(isset($_GET['effective_date']) && $_GET['effective_date']!=''){ $effective_date = $_GET['effective_date']; }else{ $effective_date = ''; } ?>
                <?php if(isset($_GET['expiry_date']) && $_GET['expiry_date']!=''){ $expiry_date = $_GET['expiry_date']; }else{ $expiry_date = ''; } ?>
                <?php $arr = array('name'=>$name, 'part_number'=>$part_number, 'inventory_supplier_id'=>$inventory_supplier_id, 'quotation_number'=>$quotation_number, 'effective_date'=>$effective_date, 'expiry_date'=>$expiry_date); ?>
                <?php
                    echo $this->Html->link('<i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Download',array('controller'=>'inventory_supplier_items','action'=>'export?name='.$name.'&part_number='.$part_number.'&inventory_supplier_id='.$inventory_supplier_id.'&quotation_number='.$quotation_number.'&effective_date='.$effective_date.'&expiry_date='.$expiry_date.'&search=Search'), array('target'=>'_blank', 'class'=>'btn btn-info pull-right btn-sm', 'escape'=>false));
                ?>
                <?php 
                    echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'view_pdf_item_list?name='.$name.'&part_number='.$part_number.'&inventory_supplier_id='.$inventory_supplier_id.'&quotation_number='.$quotation_number.'&effective_date='.$effective_date.'&expiry_date='.$expiry_date.'&search=Search'), array('class' => 'btn btn-success pull-right btn-sm', 'escape'=>false, 'target'=>"_blank")); 
                 ?>
 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
    <?php echo $this->Form->create('InventorySupplierItem', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Item code / name', 'class' => 'form-control', 'required' => false, 'id' => 'ft', 'label' => false)); ?>    
		</td>
		<td><?php echo $this->Form->input('part_number', array('type' => 'text', 'placeholder' => 'Part Number', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_supplier_id', array('options' => $suppliers, 'class' => 'form-control', 'required' => false, 'empty' => '-All Supplier-', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('quotation_number', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'placeholder' => 'Ref / Quotation Number', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('effective_date', array('placeholder' => 'Expiry Date', 'id' => 'dateonly', 'class' => 'form-control', 'required' => false, 'type' => 'text', 'label' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('expiry_date', array('placeholder' => 'Expiry Date','id' => 'dateonly_2', 'class' => 'form-control', 'required' => false, 'type' => 'text', 'label' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php echo $this->Form->end(); ?>
	
        	<div class="table-responsive">	
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
					<tr>  
						<th><?php echo $this->Paginator->sort('name', 'Items'); ?></th>
						<th><?php echo $this->Paginator->sort('GeneralCurrency.iso_code', 'Currency'); ?></th>
						<th><?php echo $this->Paginator->sort('price'); ?></th>
						<th><?php echo $this->Paginator->sort('price_per_unit', 'Price / Pcs'); ?></th>
						<th><?php echo ('Unit'); ?></th> 
						<th><?php echo $this->Paginator->sort('effective_date'); ?></th>
						<th><?php echo $this->Paginator->sort('expiry_date'); ?></th>
						<th><?php echo $this->Paginator->sort('InventorySupplier.name', 'Supplier'); ?></th> 
						<th><?php echo h('Default'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody> 
					<?php foreach ($inventorySupplierItems as $inventorySupplierItem): ?>
					<tr> 
						<td><?php echo $inventorySupplierItem['InventoryItem']['code']; ?><br/>
						<small><?php echo $inventorySupplierItem['InventoryItem']['name']; ?></small>
						</td>
						<td><?php echo $inventorySupplierItem['GeneralCurrency']['iso_code']; ?></td>
						<td><?php echo number_format(h($inventorySupplierItem['InventorySupplierItem']['price']), 4); ?>&nbsp;</td>
						<td><?php echo number_format(h($inventorySupplierItem['InventorySupplierItem']['price_per_unit']), 4); ?>&nbsp;</td>
						<td><?php echo ($inventorySupplierItem['GeneralUnit']['name']); ?></td> 
						<td><?php echo $inventorySupplierItem['InventorySupplierItem']['effective_date']; ?>&nbsp;</td>

						<td><span class="label label-<?php echo label_expiry($inventorySupplierItem['InventorySupplierItem']['expiry_date']); ?>"><?php echo $inventorySupplierItem['InventorySupplierItem']['expiry_date']; ?></span></td>

						<td><small><?php echo ($inventorySupplierItem['InventorySupplier']['name']); ?>&nbsp;</small></td>
						<td><?php echo isdefault($inventorySupplierItem['InventorySupplierItem']['is_default']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventorySupplierItem['InventorySupplierItem']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
 
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventorySupplierItem['InventorySupplierItem']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventorySupplierItem['InventoryItem']['name']).'"', $inventorySupplierItem['InventorySupplierItem']['id'])); ?>

							
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div> 
</div>
  
