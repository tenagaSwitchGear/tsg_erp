<?php

function isdefault($status) {
	if($status == 0) {
		return 'No';
	} else {
		return 'Yes';
	}
}

function label_expiry($expiry) {
	if(date('Y-m-d') > date('Y-m-d', strtotime($expiry)) ){
			return 'danger';
	} else {
		return 'success';
	}
}

?>

<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Add New Price Book'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('Items General'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('Suppliers'), array('controller'=>'inventory_suppliers', 'action' => 'index'), array('class'=>'btn btn-primary btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Price Book</h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
    <?php echo $this->Form->create('InventorySupplierItem', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Item code / name', 'class' => 'form-control', 'required' => false, 'id' => 'find_item', 'label' => false)); ?>  
			<?php echo $this->Form->input('inventory_item_id', array('type' => 'hidden', 'class' => 'form-control', 'required' => false, 'id' => 'inventory_item_id', 'label' => false)); ?> 
		</td>
		<td><?php echo $this->Form->input('part_number', array('type' => 'text', 'placeholder' => 'Part Number', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_supplier_id', array('options' => $suppliers, 'class' => 'form-control', 'required' => false, 'empty' => '-All Supplier-', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('quotation_number', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'placeholder' => 'Ref / Quotation Number', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('effective_date', array('placeholder' => 'Effective Date', 'id' => 'dateonly', 'class' => 'form-control', 'required' => false, 'type' => 'text', 'label' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('expiry_date', array('placeholder' => 'Expiry Date','id' => 'dateonly_2', 'class' => 'form-control', 'required' => false, 'type' => 'text', 'label' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
        	<div class="table-responsive">	
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
					<tr> 
						<th><?php echo $this->Paginator->sort('name', 'Items'); ?></th>
						<th><?php echo $this->Paginator->sort('price'); ?></th>
						<th><?php echo $this->Paginator->sort('price_per_unit', 'Price / Pcs'); ?></th>
						<th><?php echo ('Unit'); ?></th> 
						<th><?php echo ('Effective Date'); ?></th>
						<th><?php echo ('Expiry Date'); ?></th>
						<th><?php echo $this->Paginator->sort('InventorySupplier.name', 'Supplier'); ?></th>
						<th><?php echo h('Default'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						 

						foreach ($inventorySupplierItems as $inventorySupplierItem): 
					?>
					<tr>
						<td><?php echo $inventorySupplierItem['InventoryItem']['code']; ?><br/>
						<small><?php echo $inventorySupplierItem['InventoryItem']['name']; ?></small>
						</td>
						<td><?php echo number_format(h($inventorySupplierItem['InventorySupplierItem']['price']), 2); ?>&nbsp;</td>
						<td><?php echo number_format(h($inventorySupplierItem['InventorySupplierItem']['price_per_unit']), 2); ?>&nbsp;</td>
						<td><?php echo ($inventorySupplierItem['GeneralUnit']['name']); ?></td> 
						<td><?php echo $inventorySupplierItem['InventorySupplierItem']['effective_date']; ?>&nbsp;</td>

						<td><span class="label label-<?php echo label_expiry($inventorySupplierItem['InventorySupplierItem']['expiry_date']); ?>"><?php echo $inventorySupplierItem['InventorySupplierItem']['expiry_date']; ?></span></td>

						<td><?php echo ($inventorySupplierItem['InventorySupplier']['name']); ?>&nbsp;</td>
						<td><?php echo isdefault($inventorySupplierItem['InventorySupplierItem']['is_default']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventorySupplierItem['InventorySupplierItem']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventorySupplierItem['InventorySupplierItem']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventorySupplierItem['InventorySupplierItem']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventorySupplierItem['InventoryItem']['name']).'"', $inventorySupplierItem['InventorySupplierItem']['id'])); ?>

							
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).ready(function() {  
    $("#find_item").autocomplete({
	    source: function (request, response) {
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'inventory_items/ajaxfinditem',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#find_item').val(),                                                    
	            success: function (data) {
	            	console.log(data);
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.code,
		                    name: item.name,
		                    note: item.note 
		                }
		            }));
	        	}
	        });
	    },
	    select: function (event, ui) {
	        $("#inventory_item_id").val(ui.item.id); 
	    },
	    minLength: 3

	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br>" +  "</div>" ).appendTo( ul );
    }; 
});  
</script>
<?php $this->end(); ?>