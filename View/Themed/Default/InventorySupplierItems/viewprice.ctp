<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Edit Price Book'), array('action' => 'edit', $inventorySupplierItem['InventorySupplierItem']['id']), array('class' => 'btn btn-warning btn-sm')); ?>
  		<?php echo $this->Html->link(__('Back To Price Books'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Price Book'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventorySupplierItems view-data"> 
	<dl> 
		<dt><?php echo __('Item Code'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItem['InventoryItem']['code'], array('controller' => 'inventory_items', 'action' => 'view', $inventorySupplierItem['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Name'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventorySupplierItem['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Supplier Price'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItem['GeneralCurrency']['iso_code'], array('controller' => 'general_currencies', 'action' => 'view', $inventorySupplierItem['GeneralCurrency']['id'])); ?> <?php echo h($inventorySupplierItem['InventorySupplierItem']['price']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Purchase Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventorySupplierItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Per Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItem['GeneralCurrency']['iso_code'], array('controller' => 'general_currencies', 'action' => 'view', $inventorySupplierItem['GeneralCurrency']['id'])); ?> <?php echo h($inventorySupplierItem['InventorySupplierItem']['price_per_unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Min Order'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['min_order']); ?> <?php echo h($inventorySupplierItem['MinOrderUnit']['name']); ?>
			&nbsp;
		</dd> 
		<dt><?php echo __('Conversion Qty'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['conversion_qty']); ?> <?php echo h($inventorySupplierItem['UnitConversion']['name']); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Effective Date'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['effective_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Expiry Date'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['expiry_date']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Supplier'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItem['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $inventorySupplierItem['InventorySupplier']['id'])); ?>
			&nbsp;
		</dd> 
		<dt><?php echo __('Is Default'); ?></dt>
		<dd>
			<?php echo is_default($inventorySupplierItem['InventorySupplierItem']['is_default']); ?>
			&nbsp;
		</dd> 
		
		<dt><?php echo __('Supplier Part Number'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['part_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lead Time'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['lead_time']); ?> Days
			&nbsp;
		</dd>
		<dt><?php echo __('Quotation Number'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['quotation_number']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItem['InventorySupplierItem']['remark']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Allow Sales Dept To View and Sell'); ?></dt>
		<dd>
			<?php echo is_default($inventorySupplierItem['InventorySupplierItem']['sales']); ?>
			&nbsp;
		</dd>

	</dl>
</div>
 
	</div>
</div>
</div>
</div>


<?php

function is_default($status) {
	if($status == 1) {
		return 'Yes';
	} else {
		return 'No';
	}
}
?>