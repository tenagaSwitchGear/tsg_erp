<?php

function isdefault($status) {
    if($status == 0) {
        return 'No';
    } else {
        return 'Yes';
    }
}
?>
<?php if(isset($_GET['name']) && $_GET['name']!=''){ $name = $_GET['name']; }else{ $name = ''; } ?>
                <?php if(isset($_GET['part_number']) && $_GET['part_number']!=''){ $part_number = $_GET['part_number']; }else{ $part_number = ''; } ?>
                <?php if(isset($_GET['inventory_supplier_id']) && $_GET['inventory_supplier_id']!=''){ $inventory_supplier_id = $_GET['inventory_supplier_id']; }else{ $inventory_supplier_id = ''; } ?>
                <?php if(isset($_GET['quotation_number']) && $_GET['quotation_number']!=''){ $quotation_number = $_GET['quotation_number']; }else{ $quotation_number = ''; } ?>
                <?php if(isset($_GET['effective_date']) && $_GET['effective_date']!=''){ $effective_date = $_GET['effective_date']; }else{ $effective_date = ''; } ?>
                <?php if(isset($_GET['expiry_date']) && $_GET['expiry_date']!=''){ $expiry_date = $_GET['expiry_date']; }else{ $expiry_date = ''; } ?>
                <?php $arr = array('name'=>$name, 'part_number'=>$part_number, 'inventory_supplier_id'=>$inventory_supplier_id, 'quotation_number'=>$quotation_number, 'effective_date'=>$effective_date, 'expiry_date'=>$expiry_date); ?>
<div class="book">
    <div class="page">
        <div id="content" class="container page-break">  
        <h3><strong><?php echo strtoupper('Price Book List'); ?></strong></h3>
            <div class="table-responsive">  
                <table style="font-size: 14px; width: 100%;" class="table table-bordered">
                    <thead>
                    <tr> 
                        <th>#</th>
                        <th><?php echo 'Items'; ?></th>
                        <th><?php echo 'Currency'; ?></th>
                        <th><?php echo 'Price'; ?></th>
                        <th><?php echo 'Price / Pcs'; ?></th>
                        <th><?php echo 'Unit'; ?></th> 
                        <th><?php echo 'Effective Date'; ?></th>
                        <th><?php echo 'Expiry Date'; ?></th>
                        <th><?php echo 'Supplier'; ?></th>
                        <th><?php echo 'Default'; ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $bil=1; ?>
                    <?php foreach ($inventorySupplierItems as $inventorySupplierItem): ?>
                    <tr>
                        <td><?php echo $bil; ?></td>
                        <td><?php echo $inventorySupplierItem['InventoryItem']['code']; ?><br/>
                        <small><?php echo $inventorySupplierItem['InventoryItem']['name']; ?></small>
                        </td>
                        <td><?php echo $inventorySupplierItem['GeneralCurrency']['iso_code']; ?></td>
                        <td><?php echo number_format(h($inventorySupplierItem['InventorySupplierItem']['price']), 4); ?>&nbsp;</td>
                        <td><?php echo number_format(h($inventorySupplierItem['InventorySupplierItem']['price_per_unit']), 4); ?>&nbsp;</td>
                        <td><?php echo ($inventorySupplierItem['GeneralUnit']['name']); ?></td> 
                        <td><?php echo date('d-m-Y', strtotime($inventorySupplierItem['InventorySupplierItem']['effective_date'])); ?>&nbsp;</td>

                        <td><?php echo date('d-m-Y', strtotime($inventorySupplierItem['InventorySupplierItem']['expiry_date'])); ?></td>

                        <td><?php echo ($inventorySupplierItem['InventorySupplier']['name']); ?>&nbsp;</td>
                        <td><?php echo isdefault($inventorySupplierItem['InventorySupplierItem']['is_default']); ?>&nbsp;</td>
                        
                    </tr>
                <?php $bil++;  endforeach; ?>
                    </tbody>
                </table>               
            </div>
        </div>
    </div>
</div>
