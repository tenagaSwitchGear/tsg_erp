<?php echo $this->Html->link(__('Add New Approval'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Approval PIC List</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 

<div class="approvals index">
	<h2><?php echo __('Approvals'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('User.firstname', 'Full Name'); ?></th>
			<th><?php echo $this->Paginator->sort('User.email', 'Email'); ?></th>
			<th><?php echo $this->Paginator->sort('name', 'Approval Type'); ?></th>
			<th><?php echo $this->Paginator->sort('type', 'Level'); ?></th>
			<th><?php echo $this->Paginator->sort('Group.name', 'Department'); ?></th>
			<th><?php echo $this->Paginator->sort('valid_from'); ?></th>
			<th><?php echo $this->Paginator->sort('valid_to'); ?></th>
			<th><?php echo $this->Paginator->sort('role'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($approvals as $approval): ?>
	<tr> 
		<td>
			<?php echo $this->Html->link($approval['User']['username'], array('controller' => 'users', 'action' => 'view', $approval['User']['id'])); ?>
		</td>
		<td><?php echo h($approval['User']['firstname']); ?>&nbsp;</td>
		<td><?php echo h($approval['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($approval['Approval']['name']); ?>&nbsp;</td>
		<td><?php echo h($approval['Approval']['type']); ?>&nbsp;</td>
		<td><?php echo h($approval['Group']['name']); ?>&nbsp;</td>
		<td><?php echo h($approval['Approval']['valid_from']); ?>&nbsp;</td>
		<td><?php echo h($approval['Approval']['valid_to']); ?>&nbsp;</td>
		<td><?php echo h($approval['Approval']['role']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $approval['Approval']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $approval['Approval']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $approval['Approval']['id']), array(), __('Are you sure you want to delete # %s?', $approval['Approval']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
</div>
</div>
</div>
</div>