<?php echo $this->Html->link(__('Approval List'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Html->link(__('Add New Approval'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Approval PIC List</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 

<div class="approvals view-data">
<h2><?php echo __('Approval'); ?></h2>
	<dl> 
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($approval['User']['username'], array('controller' => 'users', 'action' => 'view', $approval['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo $this->Html->link($approval['User']['firstname'], array('controller' => 'users', 'action' => 'view', $approval['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo $this->Html->link($approval['User']['email'], array('controller' => 'users', 'action' => 'view', $approval['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($approval['Approval']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo h($approval['Approval']['role']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($approval['Approval']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($approval['Approval']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Valid From'); ?></dt>
		<dd>
			<?php echo h($approval['Approval']['valid_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Valid To'); ?></dt>
		<dd>
			<?php echo h($approval['Approval']['valid_to']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
</div>
</div>
</div>
</div>