

<?php echo $this->Html->link(__('Approval List'), array('action' => 'index'), array('class' => 'btn btn-success')); ?>

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Approval PIC List</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 

<div class="approvals form">
<?php echo $this->Form->create('Approval'); ?> 
	<?php echo $this->Form->input('id'); ?>
	<?php echo $this->Form->input('user_id', array('class' => 'form-control', 'empty' => '-Select User-')); ?> 
	<?php echo $this->Form->input('group_id', array('options' => $group, 'class' => 'form-control', 'empty' => '-Select Department-')); ?> 
	<?php 
	$name = array(
		'MRN' => 'MRN',
		'Purchase Requisition' => 'Purchase Requisition',
		'Purchase Order' => 'Purchase Order (Procurement Department)',
		'Plan Order' => 'Plan Order (PPC Department)',
		'Quotation' => 'Quotation (Sales Department)',
		'Costing' => 'Costing (PPC Department)',
		'HOD' => 'Handing Over Document (Sales Department)',
	);
	echo $this->Form->input('name', array('options' => $name, 'empty' => '-Select Approval Type-', 'class' => 'form-control')); 

	?> 

	<?php 
	$type = array(
		'Approval' => 'Approval',
		'Verify' => 'Verify' 
	);
	echo $this->Form->input('type', array('options' => $type, 'empty' => '-Select Level-', 'class' => 'form-control')); 

	$role = array(
        'HOS' => 'HOS',
        'HOD' => 'HOD',
        'MD'  => 'MD'
    ); ?>
    <div class="clearfix">&nbsp;</div>
    <?php
    echo $this->Form->input('role', array('id'=>'role', 'options' => $role, 'empty' => '-Select Role-', 'class' => 'form-control', 'label'=>false));

    ?> 

     <div class="clearfix">&nbsp;</div>
    <?php
    echo $this->Form->input('valid_from', array('id'=>'dateonly', 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD')); 
    ?> 

    <div class="clearfix">&nbsp;</div>
    <?php
    echo $this->Form->input('valid_to', array('id'=>'dateonly_2', 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD')); 
    ?> 
    
    <div class="clearfix">&nbsp;</div>

	<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success')); ?>
	<?php echo $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>