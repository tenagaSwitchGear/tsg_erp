<div class="row">
  <div class="col-md-9">

<?php echo $this->Html->link('Back To Quotations', array('action' => 'index'), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php if($saleQuotation['SaleQuotation']['status'] == 9) { ?>
<?php echo $this->Html->link('<i class="fa fa-certificate"></i> Awarded', array('action' => 'status', $saleQuotation['SaleQuotation']['id'], 10, 9), array('class' => 'btn btn-success', 'escape' => false)); ?>

<?php echo $this->Html->link('<i class="fa fa-times"></i> Failed', array('action' => 'status', $saleQuotation['SaleQuotation']['id'], 11, 9), array('class' => 'btn btn-danger', 'escape' => false)); ?>
<?php } ?>

<?php if($saleQuotation['SaleQuotation']['status'] == 6) { ?>
<?php echo $this->Html->link('<i class="fa fa-paper-plane"></i> Submitted To Client', array('action' => 'status', $saleQuotation['SaleQuotation']['id'], 9, 6), array('class' => 'btn btn-success', 'escape' => false)); ?>
 
<?php } ?>
  
<?php echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'view', $saleQuotation['SaleQuotation']['id'] . '?print=1'), array('class' => 'btn btn-primary', 'escape' => false, 'onclick' => 'printIframe(report);')); ?>

 

<?php if(isset($_GET['print'])) { ?>
 <iframe style="display: none;" name="report" id="report" src="<?php echo BASE_URL; ?>sale_quotations/printview/<?php echo $saleQuotation['SaleQuotation']['id']; ?>"></iframe>
<?php } ?>
</div>  
  <div class="col-md-3">
<?php if($saleQuotation['SaleQuotation']['status'] == 2 || $saleQuotation['SaleQuotation']['status'] == 3 || $saleQuotation['SaleQuotation']['status'] == 7) { ?>
<?php echo $this->Form->create('SaleQuotation', array('class' => 'form-horizontal form-label-left')); ?> 
<?php echo $this->Form->input('id'); ?>
<?php echo $this->Form->input('status', array('type' => 'hidden', 'value' => 3)); ?>
<?php echo $this->Form->button('Submit Approval', array('class' => 'btn btn-success pull-right')); ?> 
<?php echo $this->Form->end(); ?>
<?php } ?>
</div>
</div>

<?php 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Planning';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting HOD Approval';
  } elseif($status == 4) {
    $data = 'HOD Approved';
  } elseif($status == 5) {
    $data = 'Waiting GM Verification';
  } elseif($status == 6) {
    $data = 'GM Verified';
  } elseif($status == 7) {
    $data = 'HOD Rejected';
  } elseif($status == 8) {
    $data = 'GM Rejected';
  } elseif($status == 9) {
    $data = 'Quotation Submitted';
  } elseif($status == 10) {
    $data = 'Awarded';
  } elseif($status == 11) {
    $data = 'Failed';
  } 
  return $data;
}

function markup($unit, $amount, $margin) {
  if($unit == 1) {
    $total = ($amount / 100) * $margin + $amount;
  } 
  if($unit == 2) {
    $total = $amount + $margin;
  }
  return $total;
}
?>

 

<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title"> 
        <h2>Quotation: <?php echo h($saleQuotation['SaleQuotation']['name']); ?></h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php echo $this->Session->flash(); ?> 

        

        <table class="table table-bordered">
        <tr>
        <td class="v-top">
        <h4>Customer:</h4> 
        <?php echo $this->Html->link('<i class="fa fa-pencil"></i> Edit Customer', array('controller' => 'customers', 'action' => 'edit', $saleQuotation['Customer']['id']), array('target' => '_blank', 'escape' => false, 'class' => 'btn btn-xs btn-warning pull-right')); ?>
        <b><?php echo $this->Html->link($saleQuotation['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleQuotation['Customer']['id'])); ?></b>
                  <br><?php echo h($saleQuotation['Customer']['address']); ?>
                  <br><?php echo h($saleQuotation['Customer']['postcode']); ?>, <?php echo h($saleQuotation['Customer']['city']); ?>
                  <br> <?php echo h($customer['State']['name']); ?>
                  <br> <?php echo h($customer['Country']['name']); ?>
                  <br>Phone: <?php echo h($customer['Customer']['phone']); ?>
                  <br>Email: <?php echo h($customer['Customer']['email']); ?></td>
        <td><h4>Detail:</h4>
        <p>Status: <b><?php echo status($saleQuotation['SaleQuotation']['status']); ?></b><br/>
        Quotation No: <b><?php echo h($saleQuotation['SaleQuotation']['name']); ?></b>  
              <br>
              Closing Date: <b><?php echo h($saleQuotation['SaleTender']['closing_date']); ?></b>
              <br>
               Tender No: <b><?php echo h($saleQuotation['SaleTender']['tender_no']); ?></b> </td>
        </tr>
        </table> 
           
        

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table">

<?php if($remarks) { ?>
<h4>Verifier Remarks</h4>
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
  <thead>
    <tr> 
      <td>Remark</td>
      <td>PIC</td> 
      <td>Created</td>  
      <td>Status</td> 
    </tr>
  </thead> 
<tbody>
<?php  
  foreach($remarks as $remark) { ?>  
  <tr>
    <td width="50%"><?php echo h($remark['SaleQuotationRemark']['remark']); ?></td>
    <td><?php echo $remark['User']['username']; ?></td> 
    <td><?php echo $remark['SaleQuotationRemark']['created']; ?></td> 
    <td><?php echo status($remark['SaleQuotationRemark']['status']); ?></td>  
  </tr>
  <?php } ?>
   
</tbody> 
</table>
<?php } ?>

              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Item / Code</th> 
                    
                    <th>Cost</th>
                    <th>Margin (%)</th>
                    <th>Price/Unit</th>
                    <th>Qty</th>
                    <th>Sale Price</th>
                    <th>Discount (RM)</th> 
                    <th width="160px" class="align-right">Subtotal</th> 
                  </tr>
                </thead>
                <tbody>
                <?php $i = 1; $total = 0; ?>
                <?php foreach ($items as $item) { ?>
                  <?php 
                   
                  if($item['SaleQuotationItem']['sale_price'] == 0) {
                    $price_quantity = $item['SaleQuotationItem']['unit_price'] * $item['SaleQuotationItem']['quantity'];
                  } else {
                    $price_quantity = $item['SaleQuotationItem']['unit_price'] * $item['SaleQuotationItem']['quantity'];
                  }

                  $total += $item['SaleQuotationItem']['sale_price'];
                  
                  ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td>
                    <?php echo h($item['InventoryItem']['name']); ?><br/>
                    <small><?php echo h($item['InventoryItem']['code']); ?></small> 

                    </td>
                    
                    
                    <td><?php echo h(number_format($item['SaleQuotationItem']['planning_price'], 2)); ?></td>
                    <td><?php echo h(number_format($item['SaleQuotationItem']['sale_margin'], 2)); ?></td>
                    <td><?php echo h(number_format($item['SaleQuotationItem']['unit_price'], 2)); ?></td>
                    <td><?php echo h($item['SaleQuotationItem']['quantity']); ?></td>
                    <td><?php echo h(number_format($price_quantity, 2)); ?></td>
                    <td><?php echo h($item['SaleQuotationItem']['discount']); ?></td> 
                    <td width="160px"  class="align-right"><?php echo h(number_format($item['SaleQuotationItem']['sale_price'], 2)); ?></td> 
                     

                  </tr>
                <?php $i++; ?>
                <?php } ?> 

                <?php 
                 if($saleQuotation['SaleQuotation']['gst'] > 0) {
                  $tax_amount = ($total / 100) * $saleQuotation['SaleQuotation']['gst'];
                 } else {
                  $tax_amount = 0;
                 }   
                ?>

                  <tr>
                    <td colspan="8" class="align-right">Sub Total</td> 
                    <td width="160px" class="align-right"><b><?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($total, 2); ?></b></td> 
                  </tr>

                  <tr>
                    <td colspan="8" class="align-right">GST (<?php echo Configure::read('Site.gst_amount'); ?>%)</td> 
                    <td width="160px" class="align-right"><b><?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($tax_amount, 2); ?></b></td> 
                  </tr>

                  <tr>
                    <td colspan="8" class="align-right">Grand Total</td> 
                    <td width="160px" class="align-right"><b><?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($total + $tax_amount, 2); ?></b></td> 
                  </tr>
                </tbody>
              </table>

               
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row --> 
      </div>
    </div>
  </div>
</div> 
<?php $this->start('script'); ?>

<script type="text/javascript"> 

function printIframe(objFrame){ 
  objFrame.focus(); 
  objFrame.print(); 
  bjFrame.save(); 
}
<?php  
if($saleQuotation['SaleQuotation']['status'] == 2 || $saleQuotation['SaleQuotation']['status'] == 3 || $saleQuotation['SaleQuotation']['status'] == 7) { ?>
$(function() { 
$('#findUser').autocomplete({ 
      source: function (request, response){ 
          $.ajax({
              type: "GET",                        
              url: baseUrl + 'users/ajaxfinduser',           
              contentType: "application/json",
              dataType: "json",
              data: "term=" + $('#findUser').val(),                                                    
              success: function (data) { 
                response($.map(data, function (item) {
                    return {
                        id: item.id,
                        value: item.username,
                        name : item.name,
                        email: item.email,
                        group: item.group 
                    }
                }));
            }
          });
      },
      select: function (event, ui) {  
          $('#user_id').val( ui.item.id );  
      },
      minLength: 3
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>" + item.email + "</small><br>" +  "</div>" ).appendTo( ul );
  };
<?php } ?>
 });
</script>
<?php $this->end(); ?> 