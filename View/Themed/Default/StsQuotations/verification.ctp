<?php echo $this->Html->link('Quotation Approval', array('action' => 'approval'), array('class' => 'btn btn-primary btn-sm')); ?>

<?php 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Planning';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting HOD Verification';
  } elseif($status == 4) {
    $data = 'HOD Approved';
  } elseif($status == 5) {
    $data = 'Waiting GM Approval';
  } elseif($status == 6) {
    $data = 'GM Approved';
  } elseif($status == 7) {
    $data = 'HOD Rejected';
  } elseif($status == 8) {
    $data = 'GM Rejected';
  } elseif($status == 9) {
    $data = 'Quotation Submitted';
  } elseif($status == 10) {
    $data = 'Awarded';
  } elseif($status == 11) {
    $data = 'Failed';
  } 
  return $data;
}

function markup($unit, $amount, $margin) {
	if($unit == 1) {
		$total = ($amount / 100) * $margin + $amount;
	} 
	if($unit == 2) {
		$total = $amount + $margin;
	}
	return $total;
}
?> 

<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Quotation <?php echo status($saleQuotation['SaleQuotation']['status']); ?></h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php echo $this->Session->flash(); ?> 
        <?php if($saleQuotation['SaleQuotation']['status'] == 3 || $saleQuotation['SaleQuotation']['status'] == 5) { ?>
        <div class="box-blue">
        <div id="remark">

          <?php echo $this->Form->create('SaleQuotationRemark', array('class' => 'form-horizontal')); ?> 
          <div class="form-group">
            <div class="col-sm-12"><h4>Approval Section</h4></div>
          </div>
          <div class="form-group">
          <label class="col-sm-3">Remark (Optional)</label>
          <div class="col-sm-9">
          <?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Remark / note...', 'required' => false)); ?> 
          </div>
          </div>

          <div class="form-group">
          <label class="col-sm-3">Status *</label>
          <div class="col-sm-9">
          <?php echo $this->Form->input('status', array('options' => $status, 'empty' => '-Select Status-', 'class' => 'form-control', 'label' => false)); ?> 
          </div>
          </div>

          <div class="form-group">
          <label class="col-sm-3"></label>
          <div class="col-sm-9">
           <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?>
          </div>
          </div>

          <?php $this->end(); ?>
        </div>
        </div>
        <?php } ?>
        

<?php if($remarks) { ?>
<h4>Remarks</h4>
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
  <thead>
    <tr> 
      <td>Remark</td>
      <td>PIC</td> 
      <td>Created</td>  
      <td>Status</td> 
    </tr>
  </thead> 
<tbody>
<?php  
  foreach($remarks as $remark) { ?>  
  <tr>
    <td><?php echo $remark['SaleQuotationRemark']['remark']; ?></td>
    <td><?php echo $remark['User']['username']; ?></td> 
    <td><?php echo $remark['SaleQuotationRemark']['created']; ?></td> 
    <td><?php echo status($remark['SaleQuotationRemark']['status']); ?></td>  
  </tr>
  <?php } ?>
   
</tbody> 
</table>
<?php } ?>
 
          <table class="table table-bordered">
        <tr>
        <td>
        <h4>Customer:</h4>
        <b><?php echo $this->Html->link($saleQuotation['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleQuotation['Customer']['id'])); ?></b>
                  <br><?php echo h($saleQuotation['Customer']['address']); ?>
                  <br><?php echo h($saleQuotation['Customer']['postcode']); ?>, <?php echo h($saleQuotation['Customer']['city']); ?>
                  <br> <?php echo h($customer['State']['name']); ?>
                  <br> <?php echo h($customer['Country']['name']); ?>
                  <br>Phone: <?php echo h($customer['Customer']['phone']); ?>
                  <br>Email: <?php echo h($customer['Customer']['email']); ?></td>
        <td><h4>Detail:</h4>
        <p>Status: <b><?php echo status($saleQuotation['SaleQuotation']['status']); ?></b><br/>
        Quotation No: <b><?php echo h($saleQuotation['SaleQuotation']['name']); ?></b>  
              <br>
              Closing Date: <b><?php echo h($saleQuotation['SaleTender']['closing_date']); ?></b>
              <br>
               Tender No: <b><?php echo h($saleQuotation['SaleTender']['tender_no']); ?></b>
              <br>Estimate Price: <b><?php echo _n2($saleQuotation['SaleTender']['estimate_price']); ?></b>
              <br>Prepared By: <b><?php echo $saleQuotation['User']['username']; ?></b>
              </p></td>
        </tr>
        </table> 

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Item</th> 
                    <th>Qty</th>
                    <th>Cost</th>
                    <th>Margin</th>
                    <th>Price/Unit</th>
                    <th>Discount</th>
                    <th>Tax</th>
                    <th>Subtotal</th>
                     
                  </tr>
                </thead>
                <tbody>
                <?php $i = 1; $total = 0; ?>
                <?php foreach ($items as $item) { ?>
                  <?php 
                   
                  if($item['SaleQuotationItem']['sale_price'] == 0) {
                    $price_quantity = $item['SaleQuotationItem']['planning_price'] * $item['SaleQuotationItem']['quantity'];
                  } else {
                    $price_quantity = $item['SaleQuotationItem']['sale_price'] * $item['SaleQuotationItem']['quantity'];
                  }

                  $total += $price_quantity + $item['SaleQuotationItem']['quantity'];
                  
                  ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td>
                    <?php echo h($item['InventoryItem']['code']); ?><br/>
                    <small><?php echo h($item['InventoryItem']['name']); ?><br/>
                    <?php echo h($item['InventoryItem']['note']); ?>
                    </small>

                    </td>
                    
                    <td><?php echo h($item['SaleQuotationItem']['quantity']); ?></td>
                    <td><?php echo h(number_format($item['SaleQuotationItem']['planning_price'], 2)); ?></td>
                    <td><?php echo h(number_format($item['SaleQuotationItem']['sale_margin'], 2)); ?></td>
                    <td><?php echo h(number_format($item['SaleQuotationItem']['unit_price'], 2)); ?></td>
                    <td><?php echo h($item['SaleQuotationItem']['discount']); ?></td>
                    <td><?php echo h($item['SaleQuotationItem']['tax']); ?></td>
                    <td><?php echo h(number_format($price_quantity, 2)); ?></td> 
                     
                <?php $i++; ?>
                <?php } ?> 
                  <tr>
                    <td colspan="8">Total</td>
                     
                    <td><b><?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($total, 2); ?></b></td> 
                  </tr>
                </tbody>
              </table>

              <?php if($saleQuotation['TermOfPayment']) { ?>
              <h4>Term Of Payment</h4>
              <p><?php echo $saleQuotation['TermOfPayment']['name']; ?></p>
              <?php } ?>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
  
      </div>
    </div>
  </div> 
</div> 

<?php $this->start('script'); ?>
<script type="text/javascript"> 
  $(document).ready(function() {
    $("#reject").click(function() {
      $('#remark').show();
      return false;
    });
  });
</script>
<?php $this->end(); ?>