<?php echo $this->Html->link('Draft', array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?> 
<?php echo $this->Html->link('Active', array('action' => 'index/2'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link('Waiting Approval', array('action' => 'index/3'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Rejected', array('action' => 'index/7'), array('class' => 'btn btn-warning btn-sm')); ?>
<?php echo $this->Html->link('Approved', array('action' => 'index/6'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link('Quotation Submitted', array('action' => 'index/9'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link('Awarded', array('action' => 'index/10'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link('Failed', array('action' => 'index/11'), array('class' => 'btn btn-danger btn-sm')); ?>
<?php echo $this->Html->link('Create New Quotation', array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
<?php 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Planning';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting Approval';
  } elseif($status == 4) {
    $data = 'Approved';
  } elseif($status == 5) {
    $data = 'Waiting Approval';
  } elseif($status == 6) {
    $data = 'Approved';
  } elseif($status == 7) {
    $data = 'Rejected';
  } elseif($status == 8) {
    $data = 'Rejected';
  } elseif($status == 9) {
    $data = 'Quotation Submitted';
  } elseif($status == 10) {
    $data = 'Awarded';
  } elseif($status == 11) {
    $data = 'Failed';
  } 
  return $data;
}
?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Quotation List</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('SaleQuotation', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Quotation No', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('customer', array('placeholder' => 'Customer', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>
		<?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden')); ?>  
		</td>
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php echo $this->Form->end(); ?>

<div class="table-responsive"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name', 'Quotation No'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_tender_id', 'Tender'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id', 'Customer'); ?></th> 
			<th><?php echo $this->Paginator->sort('created'); ?></th>  
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>  
			<th><?php echo $this->Paginator->sort('status'); ?></th>   
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleQuotations as $saleQuotation): ?>
	<tr> 
		<td>
			<?php echo h($saleQuotation['SaleQuotation']['name']); ?>
		</td> 

		<td>
			<?php echo $this->Html->link($saleQuotation['SaleTender']['tender_no'], array('controller' => 'saletenders', 'action' => 'view', $saleQuotation['SaleTender']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleQuotation['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleQuotation['Customer']['id'])); ?>
		</td> 
		 
		<td>
			<?php echo h($saleQuotation['SaleQuotation']['created']); ?>
		</td> 
		<td>
			<?php echo h($saleQuotation['User']['username']); ?>
		</td> 
		<td><?php echo status($saleQuotation['SaleQuotation']['status']); ?></td> 
		<td class="actions"> 
			<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $saleQuotation['SaleQuotation']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>

			<?php 
			$role = $this->Session->read('Auth.User.group_id'); 
			$user_id = $this->Session->read('Auth.User.id'); 
			?>
			<?php if($role == 1) { ?> 
			<?php if($saleQuotation['SaleQuotation']['status'] == 2 || $saleQuotation['SaleQuotation']['status'] == 0 || $saleQuotation['SaleQuotation']['status'] == 7 || $saleQuotation['SaleQuotation']['status'] == 8) { ?> 
			<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $saleQuotation['SaleQuotation']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?>
			<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $saleQuotation['SaleQuotation']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete # %s?', $saleQuotation['SaleQuotation']['id'])); ?>
			<?php } ?> 
			<?php } ?>
			<?php echo $this->Html->link('<i class="fa fa-copy"></i>', array('action' => 'duplicate', $saleQuotation['SaleQuotation']['id']), array('class' => 'btn btn-primary btn-circle-sm', 'escape'=>false)); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
  </div>
    </div>
  </div> 
</div>