<?php echo $this->Html->link('<i class="fa fa-certificate"></i> Awarded', array('action' => 'add'), array('class' => 'btn btn-warning', 'escape' => false)); ?>

<?php echo $this->Html->link('<i class="fa fa-pencil"></i> Edit', array('action' => 'edit', $saleQuotation['SaleQuotation']['id']), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'print', $saleQuotation['SaleQuotation']['id']), array('class' => 'btn btn-default', 'escape' => false, 'target' => '_blank')); ?>



<?php 

function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Planning';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting Approval';
  } elseif($status == 4) {
    $data = 'Approved';
  } elseif($status == 5) {
    $data = 'Waiting Approval';
  } elseif($status == 6) {
    $data = 'Approved';
  } elseif($status == 7) {
    $data = 'Rejected';
  } elseif($status == 8) {
    $data = 'Rejected';
  } elseif($status == 9) {
    $data = 'Quotation Submitted';
  } elseif($status == 10) {
    $data = 'Awarded';
  } elseif($status == 11) {
    $data = 'Failed';
  } 
  return $data;
}

 

function markup($unit, $amount, $margin) {
	if($unit == 1) {
		$total = ($amount / 100) * $margin + $amount;
	} 
	if($unit == 2) {
		$total = $amount + $margin;
	}
	return $total;
}
?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile"> 
      <div class="x_title">
        <h2>Edit</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="view-data">
	<h2><?php echo __('Sale Quotation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleQuotation['SaleQuotation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tender'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleQuotation['SaleTender']['title'], array('controller' => 'sale_tenders', 'action' => 'view', $saleQuotation['SaleTender']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleQuotation['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleQuotation['Customer']['id'])); ?>
			&nbsp;
		</dd>
		
		
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($saleQuotation['SaleQuotation']['remark']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($saleQuotation['SaleQuotation']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($saleQuotation['SaleQuotation']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creted By'); ?></dt>
		<dd>
			<?php echo h($saleQuotation['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($saleQuotation['SaleQuotation']['status']); ?>
			&nbsp;
		</dd>
		 
		 

		</dl>
		<h2><?php echo __('BOM Selected'); ?></h2>
<table class="table"> 
<tr>
	<th>BOM Name</th>
	<th>Code</th>
	<th>Unit Price(RM)</th>
	<th>Order Qty</th>
	<th>Margin</th>
	<th>Total (RM)</th>
</tr> 
<?php $total_amount = 0; ?>
<?php foreach ($boms as $bom) { ?>
	<tr>
		<td><?php echo $this->Html->link($bom['name'], array('action' => 'bom', $bom['id'], $saleQuotation['SaleQuotation']['id'])); ?></td>
		<td><?php echo $bom['code']; ?></td>
		<td><?php echo $bom['price']; ?></td>
		<td><?php echo $bom['no_of_order']; ?></td>
		<td><?php echo $bom['margin_unit'] == 1 ? '%' : 'RM'; ?> <?php echo $bom['margin']; ?></td>
		<?php
			$total = $bom['price'] * $bom['no_of_order']; 
			$markup = markup($bom['margin_unit'], $total, $bom['margin']);
			$total_amount += $markup;
		?> 
		<td><?php echo $markup; ?></td>
	</tr> 
<?php } ?> 

<tr>
	<td><b>Total</b></td>
	<td> </td>
	<td> </td>
	<td> </td>
	<td> </td> 
	<td><b><?php echo Configure::read('Site.default_currency'); ?><?php echo $total_amount; ?></b></td>
</tr> 

</table>
	</div> 
</div>
    </div>
  </div> 
</div>
