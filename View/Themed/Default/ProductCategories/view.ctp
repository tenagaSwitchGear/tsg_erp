<?php echo $this->Html->link(__('Category List'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('Add Category'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $productCategory['ProductCategory']['id']), array('class' => 'btn btn-warning')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="productCategories view-data">
<h2><?php echo __('Product Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productCategory['ProductCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($productCategory['ProductCategory']['name']); ?>
			&nbsp;
		</dd>
		 
	</dl>
</div> 
<div class="table-responsive">
	<h3><?php echo __('Related Products'); ?></h3>
	<?php if (!empty($productCategory['Product'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Product Category Id'); ?></th>
		<th><?php echo __('Is Bom'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Cost'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($productCategory['Product'] as $product): ?>
		<tr>
			<td><?php echo $product['id']; ?></td>
			<td><?php echo $product['name']; ?></td>
			<td><?php echo $product['code']; ?></td>
			<td><?php echo $product['product_category_id']; ?></td>
			<td><?php echo $product['is_bom']; ?></td>
			<td><?php echo $product['price']; ?></td>
			<td><?php echo $product['cost']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'products', 'action' => 'view', $product['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'products', 'action' => 'edit', $product['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'products', 'action' => 'delete', $product['id']), array(), __('Are you sure you want to delete # %s?', $product['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?> 
</div> 
      
      </div> 
    </div>
  </div> 
</div>



