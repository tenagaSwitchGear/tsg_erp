<?php echo $this->Html->link(__('List Product Categories'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 
<?php echo $this->Form->create('ProductCategory', array('class'=>'form-horizontal')); ?> 

	<?php echo $this->Form->input('id'); ?> 
	<div class="form-group">
			<label class="col-sm-3">Name</label>
			<div class="col-sm-9">
	<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?> 
	</div>
	</div> 

		<div class="form-group"> 
        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?> 
        </div>	  
		<?php $this->Form->end(); ?>
 
      
      </div> 
    </div>
  </div> 
</div>

 