<div class="saleOrderChildren form">
<?php echo $this->Form->create('SaleOrderChild'); ?>
	<fieldset>
		<legend><?php echo __('Add Sale Order Child'); ?></legend>
	<?php
		echo $this->Form->input('sale_job_id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sale Order Children'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Jobs'), array('controller' => 'sale_jobs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job'), array('controller' => 'sale_jobs', 'action' => 'add')); ?> </li>
	</ul>
</div>
