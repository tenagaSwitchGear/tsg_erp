<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 		<div class="saleOrderChildren view-data">
		<h2><?php echo __('Sale Order Child'); ?></h2>
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($saleOrderChild['SaleOrderChild']['id']); ?> 
				</dd>
				<dt><?php echo __('Sale Job'); ?></dt>
				<dd>
					<?php echo $this->Html->link($saleOrderChild['SaleJob']['id'], array('controller' => 'sale_jobs', 'action' => 'view', $saleOrderChild['SaleJob']['id'])); ?> 
				</dd>
				<dt><?php echo __('Name'); ?></dt>
				<dd>
					<?php echo h($saleOrderChild['SaleOrderChild']['name']); ?> 
				</dd>
				<dt><?php echo __('Description'); ?></dt>
				<dd>
					<?php echo h($saleOrderChild['SaleOrderChild']['description']); ?> 
				</dd>
				 
			</dl>
		</div>
 		
 		<div class="table">
 		<table class="table">
 			<tr>
 				<th>BOM Name</th>
 				<th>BOM Code</th>
 				<th>Cost</th>
 				<th>Selling Price</th>
 				<th>Action</th>
 			</tr>

 			<?php foreach ($boms as $bom) { ?>
 				<tr>
	 				<td><?php echo $bom['ProjectBom']['name']; ?></td>
	 				<td><?php echo $bom['ProjectBom']['code']; ?></td>
	 				<td><?php echo Configure::read('Site.default_currency'); ?> <?php echo $bom['TotalPrice']; ?></td>
	 				<td><?php echo Configure::read('Site.default_currency'); ?> <?php echo $bom['ProjectBom']['total_price']; ?></td>
	 				<td><?php echo $this->Html->link('View', array('controller' => 'projectboms', 'action' => 'view', $bom['ProjectBom']['id']), array('class' => 'btn btn-success')); ?></td>
	 			</tr> 
 			<?php } ?>

 		</table>
 		</div>
      
      </div>
    </div>
  </div> 
</div> 