<style type="text/css">
.vert{
	padding-top: 12px
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Item'), array('action' => 'transfer'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Transfer Stock'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="container table-responsive">
				
				<dl>
					<dt class="col-sm-3"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						<?php echo h($inventoryItem['InventoryItem']['id']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Name'); ?></dt>
					<dd class="col-sm-9">
						<?php echo h($inventoryItem['InventoryItem']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Quantity'); ?></dt>
					<dd class="col-sm-9">
						<?php echo number_format(h($inventoryItem['InventoryItem']['quantity']), 2); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Unit Price'); ?></dt>
					<dd class="col-sm-9">
						<?php echo number_format(h($inventoryItem['InventoryItem']['unit_price']), 2); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('General Unit'); ?></dt>
					<dd class="col-sm-9">
						<?php echo $this->Html->link($inventoryItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryItem['GeneralUnit']['id'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Code'); ?></dt>
					<dd class="col-sm-9">
						<?php echo h($inventoryItem['InventoryItem']['code']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Qc Inspect'); ?></dt>
					<dd class="col-sm-9">
						<?php if($inventoryItem['InventoryItem']['qc_inspect']=='1'){ echo 'Yes'; }else{ echo 'No'; } ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Warehouse'); ?></dt>
					<dd class="col-sm-9">
						<?php echo ($warehouse_location[0]['inventory_delivery_locations']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Location'); ?></dt>
					<dd class="col-sm-9">
						<?php echo ($store[0]['inventory_stores']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3 vert"><?php echo __('Transfer Type'); ?></dt>
					<dd class="col-sm-9">
						<select class="form-control" onchange="transfer(this.value)">
						<option value="0">Select Type</option>
						<?php foreach ($transfer as $key => $trans) { ?>
							<option value="<?php echo $key; ?>"><?php echo $trans; ?></option>
						<?php } ?>
						</select>
						&nbsp;
					</dd>
					<div id="div_warehouse" style="display: none">
					<?php echo $this->Form->create('InventoryItem'); ?>
					<input type="hidden" name="inventory_item_id" value="<?php echo $inventoryItem['InventoryItem']['id']; ?>">
					<input type="hidden" name="transfer_type" value="1">
					<dt class="col-sm-3 vert"><?php echo __('Warehouse'); ?></dt>
					<dd class="col-sm-9">
						<select class="form-control" name="inventory_delivery_location_id" onchange="get_location(this.value)">
						<option value="0">Select Warehouse</option>
						<?php foreach ($warehouse as $key => $gudang) { ?>
							<?php if($key == $inventoryItem['InventoryItem']['inventory_delivery_location_id']){}else{?>
							<option value="<?php echo $key; ?>"><?php echo $gudang; ?></option>
							<?php } ?>
						<?php } ?>
						</select>
						&nbsp;
					</dd>
					<dt class="col-sm-3 vert"><?php echo __('Location'); ?></dt>
					<dd class="col-sm-9" id="store">
						<select class="form-control" name="inventory_store_id">
							<option value="0">Select Location</option>
						</select>
						&nbsp;
					</dd>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
					<?php echo $this->Form->end(); ?>
					</div>
					<div id="div_location" style="display: none">
					<?php echo $this->Form->create('InventoryItem'); ?>
					<input type="hidden" name="inventory_item_id" value="<?php echo $inventoryItem['InventoryItem']['id']; ?>">
					<input type="hidden" name="transfer_type" value="2">
					<dt class="col-sm-3 vert"><?php echo __('Location'); ?></dt>
					<dd class="col-sm-9">
						<select class="form-control" name="inventory_store_id" >
						<option value="0">Select Location</option>
						<?php for($i=0; $i<count($store_2); $i++){ ?>
						<?php if($store_2[$i]['inventory_stores']['id'] == $inventoryItem['InventoryItem']['inventory_store_id']){}else{ ?>
						<option value="<?php echo $store_2[$i]['inventory_stores']['id']; ?>"><?php echo $store_2[$i]['inventory_stores']['name']; ?></option>
						<?php } ?>
						<?php } ?>
						</select>
						&nbsp;
					</dd>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
					<?php echo $this->Form->end(); ?>
					</div>
					<div id="div_code" style="display: none">
					<?php echo $this->Form->create('InventoryItem'); ?>
					<input type="hidden" name="inventory_item_id" value="<?php echo $inventoryItem['InventoryItem']['id']; ?>">
					<input type="hidden" name="transfer_type" value="3">
					<dt class="col-sm-3 vert"><?php echo __('Code'); ?></dt>
					<dd class="col-sm-9">
						<input type="text" class="form-control" name="code">
						&nbsp;
					</dd>
					<dt class="col-sm-3 vert"><?php echo __('Quantity'); ?></dt>
					<dd class="col-sm-9">
						<input type="text" class="form-control" name="quantity">
						&nbsp;
					</dd>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
					<?php echo $this->Form->end(); ?>
					</div>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="clearfix">&nbsp;</div>
			

        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
function transfer(nilai){
	if(nilai == '1'){
		$('#div_warehouse').show();
		$('#div_location').hide();
		$('#div_code').hide();
	}else if(nilai == '2'){
		$('#div_warehouse').hide();
		$('#div_location').show();
		$('#div_code').hide();
	}else{
		$('#div_warehouse').hide();
		$('#div_location').hide();
		$('#div_code').show();
	}
}

function get_location(nilai){
	var getParam = {
        id: nilai
    }
	$.ajax({ 
	    type: "POST", 
	    data: 'json',
	    dataType: 'json',
	    data: getParam,
	    url: baseUrl + 'inventoryItems/ajaxlocation', 
	    success: function(data) { 
	    	console.log(data);
			var c = '<select name="inventory_store_id" class="form-control"><option value="0">Select Location</option>'; 
            for(i=0; i<data.length; i++){
                c += '<option value='+data[i]['id']+'>';
                c += data[i]['name'];
                c += '</option>';

            }
            c += '</select>';

            $('#store').html(c);
	    },
	    error: function(err){
	    	console.log(err);
	    }
	}); 
	return false;
}

</script>
<?php $this->end(); ?>


