<?php echo $this->Html->link(__('Item General'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Transfer Item'), array('action' => 'transfer'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryItem['InventoryItem']['id']), array('class' => 'btn btn-warning btn-sm')); ?>
<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Inventory Item'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="view-data">
				
				<dl>
					<?php 
					//check image 

					$tmp = explode('.', $inventoryItem['InventoryItem']['attachment']);
					$ext = end($tmp);
					
					?>
					<?php if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif'){ ?>
					<dt><?php echo __('Image'); ?></dt>
					<dd>
						<?php echo $this->Html->image('/files/inventory_item/attachment/' . $inventoryItem['InventoryItem']['attachment_dir'] . '/' . $inventoryItem['InventoryItem']['attachment'], array('border' => '0', 'style' => 'width:350px;')); ?>
						<?php //echo $inventoryItem['InventoryItem']['attachment']; ?>
						&nbsp;
					</dd>
					<?php } ?>
					<dt><?php echo __('Code'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['code']); ?>
						&nbsp;
					</dd>
					<dt><?php echo __('Name'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['name']); ?>
						&nbsp;
					</dd>
					<dt><?php echo __('Category'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItemCategory']['name']); ?>
						&nbsp;
					</dd>
					<dt><?php echo __('Unit Price'); ?></dt>
					<dd>
						<?php echo number_format(h($inventoryItem['InventoryItem']['unit_price']), 2); ?>
						&nbsp;
					</dd>
					<dt><?php echo __('UOM'); ?></dt>
					<dd>
						<?php echo $this->Html->link($inventoryItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryItem['GeneralUnit']['id'])); ?>
						&nbsp;
					</dd>
					
					<dt><?php echo __('Qc Inspect'); ?></dt>
					<dd>
						<?php if($inventoryItem['InventoryItem']['qc_inspect']=='1'){ echo 'Yes'; }else{ echo 'No'; } ?>
						&nbsp;
					</dd>
					<dt><?php echo __('Stock Quantity'); ?></dt>
					<dd>
						<?php 
						$qty = 0;
						foreach($inventoryItem['InventoryStock'] as $stock) { 
							$qty += $stock['quantity'];
						} 
						echo $qty . ' ' . $inventoryItem['GeneralUnit']['name'];
						?> 
					</dd>
					 
					<?php if($ext != 'jpg'){ ?>
					<dt><?php echo __('Attachment'); ?></dt>
					<dd>
						<?php if($inventoryItem['InventoryItem']['attachment'] != null) { ?>
							<?php echo $this->Html->link($inventoryItem['InventoryItem']['attachment'], '/files/inventory_item/attachment/' . $inventoryItem['InventoryItem']['attachment_dir'] . '/' . $inventoryItem['InventoryItem']['attachment'], array('target' => '_blank')); ?>
						<?php } else { ?> 
							<p>Not available</p>
						<?php } ?> 
					</dd>
					<?php } ?>
					<dt><?php echo __('Note'); ?></dt>
					<dd>
						<p class="wrapper"><?php echo h($inventoryItem['InventoryItem']['note']); ?></p>&nbsp;
					</dd>

					<dt><?php echo __('Stock Type'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['is_stock']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Default Store'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryStore']['name']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Default Rack'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryRack']['name']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Search Key 1'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['search_1']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Search Key 2'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['search_2']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Search Key 3'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['search_3']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Search Key 4'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['search_4']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Search Key 5'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['search_5']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Created'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['created']); ?>
						&nbsp;
					</dd>

					<dt><?php echo __('Last Updated'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['InventoryItem']['modified']); ?>
						&nbsp;
					</dd>
					<dt><?php echo __('Added By'); ?></dt>
					<dd>
						<?php echo h($inventoryItem['User']['username']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>  

        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


