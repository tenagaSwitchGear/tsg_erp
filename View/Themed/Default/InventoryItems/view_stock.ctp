<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('Transfer Item'), array('action' => 'transfer'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Inventory Item'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<pre><?php print_r($inventoryItem); ?></pre>
			<div class="container table-responsive">
				
				<dl>
					<dt class="col-sm-3"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						<?php echo h($inventoryItem['InventoryItem']['id']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Name'); ?></dt>
					<dd class="col-sm-9">
						<?php echo h($inventoryItem['InventoryItem']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Quantity'); ?></dt>
					<dd class="col-sm-9">
						<?php echo number_format(h($inventoryItem['InventoryItem']['quantity']), 2); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Unit Price'); ?></dt>
					<dd class="col-sm-9">
						<?php echo number_format(h($inventoryItem['InventoryItem']['unit_price']), 2); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('General Unit'); ?></dt>
					<dd class="col-sm-9">
						<?php echo $this->Html->link($inventoryItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryItem['GeneralUnit']['id'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Code'); ?></dt>
					<dd class="col-sm-9">
						<?php echo h($inventoryItem['InventoryItem']['code']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Warehouse'); ?></dt>
					<dd class="col-sm-9">
						<?php echo $warehouse_location[0]['inventory_delivery_locations']['name']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Location'); ?></dt>
					<dd class="col-sm-9">
						<?php echo $store[0]['inventory_stores']['name']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Rack'); ?></dt>
					<dd class="col-sm-9">
						<?php echo $rack[0]['inventory_racks']['name']; ?>
						&nbsp;
					</dd>
					
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


