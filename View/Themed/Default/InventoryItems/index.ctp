<div class="row"> 
  	<div class="col-xs-12">

  		<?php echo $this->Html->link(__('Item General'), array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?>
  		<?php echo $this->Html->link(__('Add New Item'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		
  		<?php echo $this->Html->link(__('Price Books'), array('controller' => 'inventory_supplier_items', 'action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>

  		<?php echo $this->Html->link(__('Item Categories'), array('controller' => 'inventory_item_categories', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Item General'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
	<?php echo $this->Form->create('InventoryItem', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Item name', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('code', array('type' => 'text', 'placeholder' => 'Code', 'class' => 'form-control', 'required' => false, 'id' => 'username', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_item_category_id', array('options' => $categories, 'class' => 'form-control', 'required' => false, 'empty' => '-All Category-', 'label' => false)); ?>  
		</td>
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
					<tr> 	 
							<th><?php echo $this->Paginator->sort('name', 'Items'); ?></th> 
							<th><?php echo $this->Paginator->sort('inventory_category_id', 'Category'); ?></th> 
							<th>Stock Qty</th> 
							<th>Demand</th> 
							<th>Shortage</th>  
							<th>Type</th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php 

						foreach ($inventoryItems as $inventoryItem): 

						$stock = !empty($inventoryItem['Stock']) ? $inventoryItem['Stock'] : 0;
						$mrn = !empty($inventoryItem['Mrn']) ? $inventoryItem['Mrn'] : 0;

						$shortage = $stock - $mrn;
					?>
					<tr <?php red_bg($stock, $mrn); ?>>  
						<td><?php echo ($inventoryItem['InventoryItem']['code']); ?><br/>
						<small><?php echo ($inventoryItem['InventoryItem']['name']); ?></small></td>
						<td><?php echo $inventoryItem['InventoryItemCategory']['name']; ?></td>
						<td><?php echo _n2($stock); ?><br/>
						<small><?php echo $inventoryItem['GeneralUnit']['name']; ?></small>
						</td> 

						<td><?php echo _n2($mrn); ?></td>

						<td><?php echo $shortage < 0 ? _n2($shortage) : '0.00'; ?></td>

						<td><?php echo $inventoryItem['InventoryItem']['type']; ?></td>
						   
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryItem['InventoryItem']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>

							<?php echo $this->Html->link('<i class="fa fa-history"></i>', array('controller' => 'inventory_material_histories', 'action' => 'index?item='.$inventoryItem['InventoryItem']['name'].'&item_id='.$inventoryItem['InventoryItem']['id'].'&username=&user_id=&store=&store_pic=&type=&store_pic=&from=&to=&search=Search&_method=POST'), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false, 'target' => '_blank')); ?>

							<?php echo $this->Html->link('<i class="fa fa-copy"></i>', array('action' => 'copy', $inventoryItem['InventoryItem']['id']), array('class' => 'btn btn-default btn-circle-sm', 'escape'=>false)); ?>

							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryItem['InventoryItem']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?>

							<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryItem['InventoryItem']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryItem['InventoryItem']['name']).'"', $inventoryItem['InventoryItem']['id'])); ?>	
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
				</table>
			<p>
			<?php
				//echo $this->Paginator->counter(array(
				//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				//));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


<?php
function is_bom($type) {
	if($type == 1) {
		return 'Finished Goods';
	} else {
		return 'Raw Material';
	}
}

function red_bg($stock, $mrn) {
	if($mrn > $stock) {
		echo ' style="background: #ffe6e6!important;"';
	}
}
?>