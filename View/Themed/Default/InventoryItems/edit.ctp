<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Inventory Item'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Edit Inventory Item'); ?></h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
				<div class="inventoryItems form">
				<?php echo $this->Form->create('InventoryItem', array('class'=>'form-horizontal', 'type' => 'file')); ?>
					<fieldset>
					<?php echo $this->Form->input('id'); ?>
						<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Code *</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("code", array("class"=> "form-control", "placeholder" => 'Code', "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Name *</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("name", array("class"=> "form-control", "placeholder" => 'Name', "label"=> false)); ?>
						</div> 
					</div>  
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Inventory Item Category *</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("inventory_item_category_id", array("class"=> "form-control", "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">UOM *</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("general_unit_id", array("class"=> "form-control", "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Require Pricebook?</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("pricebook", array("options" => array(1 => 'Yes', 0 => 'No'), "class"=> "form-control", "label"=> false)); ?>
						<small>Service not require pricebook</small>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Price (Optional)</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("unit_price", array("class"=> "form-control", "label"=> false, "required" => false, "type" => "text")); ?> 
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Item Type *</label>
						<div class="col-sm-9">
						<?php $types = array('Purchase' => 'Purchase', 'Manufactured' => 'Manufactured'); ?>
						<?php echo $this->Form->input("type", array('options' => $types, "class"=> "form-control", "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">QC Inspect *</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("qc_inspect", array('options' => $Qc, "class"=> "form-control", "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Assembly Time (Days)</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("assembly_time", array("class"=> "form-control", "placeholder" => 'Default 0', "label"=> false)); ?> 
						</div> 
					</div> 
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Attachment (PDF / Image)</label>
						<div class="col-sm-9">

						<?php if($inventoryItem['InventoryItem']['attachment'] != null) { ?>
							<?php echo $this->Html->link($inventoryItem['InventoryItem']['attachment'], '/files/inventory_item/attachment/' . $inventoryItem['InventoryItem']['attachment_dir'] . '/' . $inventoryItem['InventoryItem']['attachment'], array('target' => '_blank')); ?>
						<?php } else { ?> 
							<p>Not available</p>
						<?php } ?> 

						<?php echo $this->Form->input("attachment", array("class"=> "form-control", "type"=>"file", "label"=> false, "required" => false)); ?>
						<?php echo $this->Form->input('attachment_dir', array('type' => 'hidden')); ?>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Warehouse</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("inventory_item_location_id", array("options" => $locations, "class"=> "form-control", "label"=> false, "empty" => "-N/A-")); ?>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Store</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("inventory_store_id", array("options" => $stores, "class"=> "form-control", "label"=> false, "empty" => "-N/A-")); ?>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Rack</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("inventory_rack_id", array("options" => $rack, "class"=> "form-control", "label"=> false, "empty" => "-N/A-")); ?>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Note</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("note", array("type" => "textarea", "class"=> "form-control", "label"=> false)); ?>
						</div> 
					</div>
				 	<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Search 1</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("search_1", array("class"=> "form-control", "placeholder" => 'Search key', "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Search 2</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("search_2", array("class"=> "form-control", "placeholder" => 'Search key', "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Search 3</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("search_3", array("class"=> "form-control", "placeholder" => 'Search key', "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Search 4</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("search_4", array("class"=> "form-control", "placeholder" => 'Search key', "label"=> false)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Search 5</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("search_5", array("class"=> "form-control", "placeholder" => 'Search key', "label"=> false)); ?>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">NON Stock Item</label>
						<div class="col-sm-9">
						<?php 
						$options = array(
						    0 => 'NON Stock',
						    1 => 'Stock Item'
						);

						$attributes = array(
						    'legend' => false,
						    'value' => $this->request->data['InventoryItem']['is_stock']
						);

						echo $this->Form->radio('is_stock', $options, $attributes);

						?><br/>
						<small>Some item like services is NON Stock Item</small>
						</div> 
					</div>
 
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>