<?php echo $this->Html->link(__('Add Store Location'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Store Locations'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

        	<div class="inventoryStores form">
<?php echo $this->Form->create('InventoryStore', array('class' => 'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Store'); ?></legend>
	<?php echo $this->Form->input('id'); ?>

	<div class="form-group">
	<label class="col-sm-3">Warehouse</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('inventory_location_id', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3">Store Name</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>

	<div class="form-group"> 
    	<label class="col-sm-3"></label>
		<div class="col-sm-9">
    		<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
    	</div>
    </div>	    
    </fieldset>
	<?php $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventoryStore.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventoryStore.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Stores'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Locations'), array('controller' => 'inventory_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Location'), array('controller' => 'inventory_locations', 'action' => 'add')); ?> </li>
	</ul>
</div>

</div>
</div>
</div>
</div>
