<?php echo $this->Html->link(__('Stores'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Add Store'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>  

<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryStore['InventoryStore']['id']), array('class' => 'btn btn-warning btn-sm')); ?> 
<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>View Rack</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryStores view-data">
<h2><?php echo __('Inventory Store'); ?></h2>
	<dl> 
		<dt><?php echo __('Inventory Location'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryStore['InventoryLocation']['name'], array('controller' => 'inventory_locations', 'action' => 'view', $inventoryStore['InventoryLocation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($inventoryStore['InventoryStore']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
</div>
</div>
</div>
</div>