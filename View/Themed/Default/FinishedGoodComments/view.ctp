<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List of QC Remark'), array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?>
  		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $finishedGoodComment['FinishedGoodComment']['id']), array('class' => 'btn btn-warning btn-sm')); ?>
  		<?php echo $this->Html->link(__('Add New QC Remark'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add QC Remark Status'); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="finishedGoodComments view-data">
<h2><?php echo __('Finished Good Comment'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($finishedGoodComment['FinishedGoodComment']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($finishedGoodComment['FinishedGoodComment']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 

</div>
</div>
</div>
</div>
