<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Add New QC Remark'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add QC Remark Status'); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="finishedGoodComments index"> 
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('category'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($finishedGoodComments as $finishedGoodComment): ?>
	<tr>
		<td><?php echo h($finishedGoodComment['FinishedGoodComment']['id']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodComment['FinishedGoodComment']['name']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodComment['FinishedGoodComment']['category']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $finishedGoodComment['FinishedGoodComment']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $finishedGoodComment['FinishedGoodComment']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $finishedGoodComment['FinishedGoodComment']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodComment['FinishedGoodComment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
	</ul>
</div>
 
</div>
</div>
</div>
</div>
