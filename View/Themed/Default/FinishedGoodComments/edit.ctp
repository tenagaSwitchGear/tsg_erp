<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List of QC Remark'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add QC Remark Status'); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
 

			<?php echo $this->Form->create('FinishedGoodComment', array('class' => 'form-horizontal')); ?> 
			<?php echo $this->Form->input('id'); ?> 
				<div class="form-group">
				<label class="col-sm-3">Remark Status</label>
				<div class="col-sm-9">
				<?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Remark Status', 'label' => false)); ?> </div>
				</div>

				<div class="form-group">
				<label class="col-sm-3">Category</label>
				<div class="col-sm-9">
				<?php $category = array(
					'Routine Test & FAT' => 'Routine Test & FAT',
					'Incoming Inspection' => 'Incoming Inspection',
					'Final Inspection' => 'Final Inspection',
					); ?>
				<?php echo $this->Form->input('category', array('options' => $category, 'empty' => '-Category-', 'class' => 'form-control', 'label' => false)); ?> </div>
				</div>

			<?php echo $this->Form->end(__('Submit')); ?>

</div> 
</div>
</div> 
</div>

