<div class="saleInvoiceItems index">
	<h2><?php echo __('Sale Invoice Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_invoice_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_order_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('finished_good_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleInvoiceItems as $saleInvoiceItem): ?>
	<tr>
		<td><?php echo h($saleInvoiceItem['SaleInvoiceItem']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleInvoiceItem['SaleInvoice']['id'], array('controller' => 'sale_invoices', 'action' => 'view', $saleInvoiceItem['SaleInvoice']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleInvoiceItem['SaleOrderItem']['id'], array('controller' => 'sale_order_items', 'action' => 'view', $saleInvoiceItem['SaleOrderItem']['id'])); ?>
		</td>
		<td><?php echo h($saleInvoiceItem['SaleInvoiceItem']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleInvoiceItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleInvoiceItem['GeneralUnit']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleInvoiceItem['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $saleInvoiceItem['FinishedGood']['id'])); ?>
		</td>
		<td><?php echo h($saleInvoiceItem['SaleInvoiceItem']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleInvoiceItem['SaleInvoiceItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sale Invoice Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Invoices'), array('controller' => 'sale_invoices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Invoice'), array('controller' => 'sale_invoices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
	</ul>
</div>
