<div class="saleInvoiceItems form">
<?php echo $this->Form->create('SaleInvoiceItem'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sale Invoice Item'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('sale_invoice_id');
		echo $this->Form->input('sale_order_item_id');
		echo $this->Form->input('quantity');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('finished_good_id');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SaleInvoiceItem.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SaleInvoiceItem.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Invoice Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Invoices'), array('controller' => 'sale_invoices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Invoice'), array('controller' => 'sale_invoices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
	</ul>
</div>
