<div class="saleInvoiceItems view">
<h2><?php echo __('Sale Invoice Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleInvoiceItem['SaleInvoiceItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Invoice'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoiceItem['SaleInvoice']['id'], array('controller' => 'sale_invoices', 'action' => 'view', $saleInvoiceItem['SaleInvoice']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Order Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoiceItem['SaleOrderItem']['id'], array('controller' => 'sale_order_items', 'action' => 'view', $saleInvoiceItem['SaleOrderItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($saleInvoiceItem['SaleInvoiceItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoiceItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleInvoiceItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoiceItem['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $saleInvoiceItem['FinishedGood']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($saleInvoiceItem['SaleInvoiceItem']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Invoice Item'), array('action' => 'edit', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Invoice Item'), array('action' => 'delete', $saleInvoiceItem['SaleInvoiceItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Invoice Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Invoice Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Invoices'), array('controller' => 'sale_invoices', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Invoice'), array('controller' => 'sale_invoices', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
	</ul>
</div>
