<?php echo $this->Html->link(__('List Products'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('Add New Product'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
		
		<div class="products view-data"> 
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($product['Product']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Name'); ?></dt>
				<dd>
					<?php echo h($product['Product']['name']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Code'); ?></dt>
				<dd>
					<?php echo h($product['Product']['code']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Product Category'); ?></dt>
				<dd>
					<?php echo $this->Html->link($product['ProductCategory']['name'], array('controller' => 'product_categories', 'action' => 'view', $product['ProductCategory']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Is Bom'); ?></dt>
				<dd>
					<?php echo h($product['Product']['is_bom']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Price'); ?></dt>
				<dd>
					<?php echo h($product['Product']['price']); ?>
					&nbsp;
				</dd>
				 
			</dl>
		</div> 
      
      </div> 
    </div>
  </div> 
</div>
 
