<?php echo $this->Html->link(__('Products List'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add New Product</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 
	      	<div class="products form">
			<?php echo $this->Form->create('Product', array('class'=>'form-horizontal')); ?> 
			<div class="form-group">
				<label class="col-sm-3">Name</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Code</label>
				<div class="col-sm-9">		
				<?php echo $this->Form->input('code', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Category</label>
				<div class="col-sm-9">	
				<?php echo $this->Form->input('product_category_id', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Required BOM</label>
				<div class="col-sm-9">	
				<?php $is_bom = array(0 => 'No', 1 => 'Yes'); ?>
				<?php echo $this->Form->input('is_bom', array('options' => $is_bom, 'class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Selling Price</label>
				<div class="col-sm-9">	
				<?php echo $this->Form->input('price', array('class' => 'form-control', 'label' => false, 'type' => 'text')); ?>
				</div>
			</div> 
			<div class="form-group"> 
	        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?> 
	        </div>	  
			<?php $this->Form->end(); ?>
		</div>
      </div> 
    </div>
  </div> 
</div>
 
