<div class="saleQuotationItems view">
<h2><?php echo __('Sale Quotation Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleQuotationItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleQuotationItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit Price'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['unit_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Discount'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['discount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['tax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Price'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['total_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleQuotationItem['Product']['name'], array('controller' => 'products', 'action' => 'view', $saleQuotationItem['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Quotation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleQuotationItem['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $saleQuotationItem['SaleQuotation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItem['SaleQuotationItem']['type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Quotation Item'), array('action' => 'edit', $saleQuotationItem['SaleQuotationItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Quotation Item'), array('action' => 'delete', $saleQuotationItem['SaleQuotationItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotationItem['SaleQuotationItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Item Costs'), array('controller' => 'sale_quotation_item_costs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item Cost'), array('controller' => 'sale_quotation_item_costs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Quotation Item Costs'); ?></h3>
	<?php if (!empty($saleQuotationItem['SaleQuotationItemCost'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Unit Price'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Tax'); ?></th>
		<th><?php echo __('Total Price'); ?></th>
		<th><?php echo __('Sale Quotation Item Id'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($saleQuotationItem['SaleQuotationItemCost'] as $saleQuotationItemCost): ?>
		<tr>
			<td><?php echo $saleQuotationItemCost['id']; ?></td>
			<td><?php echo $saleQuotationItemCost['name']; ?></td>
			<td><?php echo $saleQuotationItemCost['quantity']; ?></td>
			<td><?php echo $saleQuotationItemCost['general_unit_id']; ?></td>
			<td><?php echo $saleQuotationItemCost['unit_price']; ?></td>
			<td><?php echo $saleQuotationItemCost['discount']; ?></td>
			<td><?php echo $saleQuotationItemCost['tax']; ?></td>
			<td><?php echo $saleQuotationItemCost['total_price']; ?></td>
			<td><?php echo $saleQuotationItemCost['sale_quotation_item_id']; ?></td>
			<td><?php echo $saleQuotationItemCost['type']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_quotation_item_costs', 'action' => 'view', $saleQuotationItemCost['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_quotation_item_costs', 'action' => 'edit', $saleQuotationItemCost['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_quotation_item_costs', 'action' => 'delete', $saleQuotationItemCost['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotationItemCost['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Quotation Item Cost'), array('controller' => 'sale_quotation_item_costs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
