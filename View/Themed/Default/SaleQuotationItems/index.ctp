<div class="saleQuotationItems index">
	<h2><?php echo __('Sale Quotation Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('unit_price'); ?></th>
			<th><?php echo $this->Paginator->sort('discount'); ?></th>
			<th><?php echo $this->Paginator->sort('tax'); ?></th>
			<th><?php echo $this->Paginator->sort('total_price'); ?></th>
			<th><?php echo $this->Paginator->sort('product_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_quotation_id'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleQuotationItems as $saleQuotationItem): ?>
	<tr>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['id']); ?>&nbsp;</td>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['name']); ?>&nbsp;</td>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleQuotationItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleQuotationItem['GeneralUnit']['id'])); ?>
		</td>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['unit_price']); ?>&nbsp;</td>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['discount']); ?>&nbsp;</td>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['tax']); ?>&nbsp;</td>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['total_price']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleQuotationItem['Product']['name'], array('controller' => 'products', 'action' => 'view', $saleQuotationItem['Product']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleQuotationItem['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $saleQuotationItem['SaleQuotation']['id'])); ?>
		</td>
		<td><?php echo h($saleQuotationItem['SaleQuotationItem']['type']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleQuotationItem['SaleQuotationItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleQuotationItem['SaleQuotationItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleQuotationItem['SaleQuotationItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotationItem['SaleQuotationItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Item Costs'), array('controller' => 'sale_quotation_item_costs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item Cost'), array('controller' => 'sale_quotation_item_costs', 'action' => 'add')); ?> </li>
	</ul>
</div>
