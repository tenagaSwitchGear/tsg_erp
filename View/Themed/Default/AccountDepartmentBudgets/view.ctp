 
<div class="row"> 
    <div class="col-xs-12"> 
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Department Budget'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>

<div class="accountDepartmentBudgets view-data">
<h2><?php echo __('Account Department Budget'); ?></h2>
	<dl>
		 
		<dt><?php echo __('Department'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudget['AccountDepartment']['Group']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudget['AccountDepartmentBudget']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudget['AccountDepartmentBudget']['total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Year'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudget['AccountDepartmentBudget']['year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Used'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudget['AccountDepartmentBudget']['used']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Balance'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudget['AccountDepartmentBudget']['balance']); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo type($accountDepartmentBudget['AccountDepartmentBudget']['type']); ?>
			&nbsp;
		</dd>
		 
	</dl>
</div>
 
 
 
</div>



</div>
</div>

</div>
</div>

<?php 
function type($type) {
	if($type == 1) {
		return 'CAPEX';
	}
	if($type == 2) {
		return 'OPEX';
	}
	if($type == 3) {
		return 'SERVING CLIENT OR CUSTOMER';
	}
}
?>
