<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Budgets'), array('controller' => 'accountdepartmentbudgets', 'action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Department Account'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Department Account</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
        	<div class="container table-responsive">
				<dl>
					<dt class="col-sm-2"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						: #<?php echo ($accountDepartment['AccountDepartment']['id']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Department Name'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($accountDepartment['Group']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Total'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($accountDepartment['AccountDepartment']['total']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Used'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($accountDepartment['AccountDepartment']['used']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="related table-responsive">
				<h4><?php echo __('Related Budgets'); ?></h4>
				<?php if (!empty($accountDepartment['AccountDepartmentBudget'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<th class="text-center"><?php echo __('Id'); ?></th>
							<th><?php echo __('Customer Id'); ?></th>
							<th><?php echo __('Name'); ?></th>
							<th><?php echo __('Position'); ?></th>
							<th><?php echo __('Office Phone'); ?></th>
							<th><?php echo __('Mobile Phone'); ?></th>
							<th><?php echo __('Email'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<?php foreach ($customer['CustomerContactPersons'] as $customerContactPerson): ?>
					<tbody>
						<tr>
							<td class="text-center"><?php echo $customerContactPerson['id']; ?></td>
							<td><?php echo $customerContactPerson['customer_id']; ?></td>
							<td><?php echo $customerContactPerson['name']; ?></td>
							<td><?php echo $customerContactPerson['position']; ?></td>
							<td><?php echo $customerContactPerson['office_phone']; ?></td>
							<td><?php echo $customerContactPerson['mobile_phone']; ?></td>
							<td><?php echo $customerContactPerson['email']; ?></td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'customer_contact_people', 'action' => 'view', $customerContactPerson['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'customer_contact_people', 'action' => 'edit', $customerContactPerson['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'customer_contact_people', 'action' => 'delete', $customerContactPerson['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete # %s?', $customerContactPerson['id'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Budgets
				<?php endif; ?>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>