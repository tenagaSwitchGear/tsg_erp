<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Department Account Budget</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
                <?php if(!empty($existed_budget)){
                        $a = '';
                        for($i=0; $i<count($existed_budget); $i++){
                            if($existed_budget[$i]['account_department_budgets']['parent_id'] == 0){
                                $a = $a + $existed_budget[$i]['account_department_budgets']['total'];
                            }
                        }
                        $exist = $a;
                        $allocated = $acc_dept[0]['account_departments']['total'] - $exist;
                    }else{
                        $allocated = $acc_dept[0]['account_departments']['total'];
                        } ?>
        		<div class="accountDepartmentBudget form">
                    <h4>Budget Allocated :  <?php echo number_format($allocated, 4); ?> </h4>
				<?php echo $this->Form->create('AccountDepartmentBudgets', array('class' => 'form-horizontal')); ?>
					<fieldset>
                        <?php echo $this->Form->input("account_department_id", array("type"=>"hidden", "class"=> "form-control", "value"=>$acc_dept[0]['account_departments']['id'], "label"=>false)); ?>
                        <?php echo $this->Form->input("year", array("type"=>"hidden", "class"=> "form-control", "value"=>$acc_dept[0]['account_departments']['year'], "label"=>false)); ?>
                        <?php echo $this->Form->input("maxbudget", array("type"=>"hidden", "class"=> "form-control", "value"=>$allocated, "label"=>false)); ?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Name</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("name", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Type</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("type", array("class"=> "form-control", "label"=> false, "options" => array(1 => 'CAPEX', 2 => 'OPEX', 3 => 'Serving Client / Customer'), 'empty' => '-Select Type-', "id" => "type")); ?>
                            </div> 
                        </div>
                        <div class="form-group" id="job_id" style="display:none">
                            <label class="col-sm-3" style="padding-top: 8px">Job</label>
                            <div class="col-sm-9">

                            <?php echo $this->Form->input("SaleJobChild.station_name", array("class"=> "form-control", "label"=> false, "required" => false, "id" => "find_job")); ?>

                            <?php echo $this->Form->input("sale_job_child_id", array("class"=> "form-control", "label"=> false, "value" => 0, "id" => "sale_job_child_id", "type" => "hidden")); ?>
                            </div> 
                        </div>
						<div id="loadCategory"></div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">&nbsp;</label>
                            <div class="col-sm-9">
                                <a href="#" id="addCategory" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Sub-Category</a>
                            </div>
                        </div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
					<?php
						echo $this->Html->link(__('Back'), array('controller'=>'accountdepartment', 'action' => 'view', $acc_dept[0]['account_departments']['id']), array('class' => 'btn btn-warning btn-sm'));
						echo $this->Form->button('Reset', array('type'=>'reset', 'class' => 'btn btn-danger btn-sm','div' => false));
						echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
					?>
					</div>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
var d = new Date();
var n = d.getFullYear();
var m = n+9;
var x = '';
for(i=n; i<m; i++){
	x += '<option value='+i+'>';
	x += i;
	x += '</option>';
}
var x = x;

$('#yearpicker').html(x);
//alert(m);

</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#type').on('change', function() {
        var type = $(this).val();
        if(type == 3) {
            $('#job_id').show();
        } else {
            $('#job_id').hide();
        }
    });
    $('#find_job').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'sale_jobs/ajaxfindjob',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#find_job').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            sale_job_child_id: item.sale_job_child_id,
                            value: item.name,
                            customer: item.customer,
                            station: item.station,
                            customer: item.customer,
                            customer_id: item.customer_id,
                            sale_tender_id: item.sale_tender_id,
                            sale_quotation_id: item.sale_quotation_id, 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {    
            $('#sale_job_child_id').val(ui.item.sale_job_child_id); 
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.station + "</small><br><small>Customer: " + item.customer + "</small>" +  "</div>" ).appendTo( ul );
    }; 

    var category = 1;
        $('#addCategory').click(function() {
            //alert("SINI");
            var html = '<div id="removeCategory'+category+'">'; 
            html += '<div class="form-group">';  
            html += '<label class="col-sm-3" style="padding-top: 8px">Subcategory</label>';
            html += '<div class="col-sm-4">';
            html += '<input type="text" name="subname[]" class="form-control" placeholder="Name">';
            html += '</div>';
             html += '<div class="col-sm-4">';
            html += '<input type="text" name="subbudget[]" class="form-control" placeholder="Budget">';
            html += '</div>';
            html += '<a href="#" class="btn btn-danger" onclick="removeCategory('+category+'); return false"><i class="fa fa-times"></i></a>';
            html += '</div></div>';    
            $("#loadCategory").append(html);  
            category++; 
        });

        function removeCategory(row) {
            $('#removeCategory'+row).html('');
        return false;
    }
});
</script>
<?php $this->end(); ?>