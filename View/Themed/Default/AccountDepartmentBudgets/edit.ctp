<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Department Account Sub-Budget</h2> 
        		<div class="clearfix"></div>
      		</div>

      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
                
        		<div class="accountDepartmentBudget form">
				<?php echo $this->Form->create('AccountDepartmentBudgets', array('class' => 'form-horizontal')); ?>
					<fieldset>
                        <?php echo $this->Form->input("account_department_id", array("type"=>"hidden", "class"=> "form-control", "value"=>$acc_dept[0]['account_departments']['id'], "label"=>false)); ?>
                        <?php echo $this->Form->input("year", array("type"=>"hidden", "class"=> "form-control", "value"=>$acc_dept[0]['account_departments']['year'], "label"=>false)); ?>
                        <?php echo $this->Form->input("maxbudget", array("type"=>"hidden", "class"=> "form-control", "value"=>$this->request->data['AccountDepartment']['total'], "label"=>false)); ?>
                        <?php echo $this->Form->input("id", array("type"=>"hidden", "class"=> "form-control", "value"=>$this->request->data['AccountDepartmentBudgets']['id'], "label"=>false)); ?>
                        <?php echo $this->Form->input("old_total", array("type"=>"hidden", "class"=> "form-control", "value"=>$this->request->data['AccountDepartmentBudgets']['total'], "label"=> false)); ?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Name</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("name", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Total Budget Given</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("total", array("type"=>"number", "id"=>"totalbudget", "class"=> "form-control", "placeholder" => "Total", "label"=> false)); ?>
							</div> 
						</div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
					<?php
						echo $this->Html->link(__('Back'), array('controller'=>'accountdepartment', 'action' => 'view', $this->request->data['AccountDepartment']['id']), array('class' => 'btn btn-warning btn-sm'));
						//echo $this->Form->button('Reset', array('type'=>'reset', 'class' => 'btn btn-danger btn-sm','div' => false));
						echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
					?>
					</div>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
var d = new Date();
var n = d.getFullYear();
var m = n+9;
var x = '';
for(i=n; i<m; i++){
	x += '<option value='+i+'>';
	x += i;
	x += '</option>';
}
var x = x;

$('#yearpicker').html(x);
//alert(m);

</script>
<?php $this->end(); ?>