<div class="row"> 
  	<div class="col-xs-12">

  	<?php echo $this->Html->link(__('Contact Persons'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>

  	<?php echo $this->Html->link(__('Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  	<?php echo $this->Html->link(__('Add Customer Contact Person'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customer Contact Persons</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

	<?php echo $this->Form->create('CustomerContactPerson', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Contact Name', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('company_name', array('type' => 'text', 'placeholder' => 'Company Name', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('office_phone', array('placeholder' => 'Office Phone', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('mobile_phone', array('placeholder' => 'Mobile Phone', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 
        	<!-- content start-->
        	<div class="customerContactPeople index table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-stripped table-bordered">
				<thead>
				<tr> 
						
						<th><?php echo $this->Paginator->sort('name'); ?></th>
						<th><?php echo $this->Paginator->sort('Customer.name', 'Company'); ?></th>
						<th><?php echo $this->Paginator->sort('position'); ?></th>
						<th><?php echo $this->Paginator->sort('office_phone'); ?></th>
						<th><?php echo $this->Paginator->sort('mobile_phone'); ?></th>
						<th><?php echo $this->Paginator->sort('email'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($customerContactPeople as $customerContactPerson): ?>
				<tr> 
					<td><?php echo h($customerContactPerson['CustomerContactPerson']['name']); ?>&nbsp;</td>
					<td><?php echo h($customerContactPerson['Customer']['name']); ?></td>
					
					<td><?php echo h($customerContactPerson['CustomerContactPerson']['position']); ?>&nbsp;</td>
					<td><?php echo h($customerContactPerson['CustomerContactPerson']['office_phone']); ?>&nbsp;</td>
					<td><?php echo h($customerContactPerson['CustomerContactPerson']['mobile_phone']); ?>&nbsp;</td>
					<td><?php echo h($customerContactPerson['CustomerContactPerson']['email']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $customerContactPerson['CustomerContactPerson']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $customerContactPerson['CustomerContactPerson']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $customerContactPerson['CustomerContactPerson']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($customerContactPerson['CustomerContactPerson']['name']).'"', $customerContactPerson['CustomerContactPerson']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


<?php /*<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Customer'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Stations'), array('controller' => 'customer_stations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Station'), array('controller' => 'customer_stations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Types'), array('controller' => 'customer_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Type'), array('controller' => 'customer_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Business Categories'), array('controller' => 'customer_business_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Business Category'), array('controller' => 'customer_business_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Ownerships'), array('controller' => 'customer_ownerships', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Ownership'), array('controller' => 'customer_ownerships', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Contact People'), array('controller' => 'customer_contact_people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Contact Person'), array('controller' => 'customer_contact_people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Files'), array('controller' => 'customer_files', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer File'), array('controller' => 'customer_files', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'add')); ?> </li>
	</ul>
</div> */ ?>