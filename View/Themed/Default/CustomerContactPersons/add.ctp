<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Customer Contact Person'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>

    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Customer Contact Person</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="customerContactPeople form">
			<?php echo $this->Form->create('CustomerContactPerson', array('class' => 'form-horizontal')); ?>
				<fieldset>
				<div class="form-group">
					<label class="col-sm-3" style="padding-top: 8px">Customer</label>
					<div class="col-sm-9">
					<?php if(isset($_GET['id'])){ ?>
					<?php echo $this->Form->input("customer_id", array("class"=> "form-control", "selected" => $_GET['id'], "label"=> false, 'disabled' => 'disabled')); ?>
					<?php }else{ ?>
					<?php echo $this->Form->input("customer_id", array("class"=> "form-control", "label"=> false)); ?>
					<?php } ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="padding-top: 8px">Name</label>
					<div class="col-sm-9">
					<?php echo $this->Form->input("name", array("class"=> "form-control", "placeholder" => 'Name', "label"=> false)); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="padding-top: 8px">Position</label>
					<div class="col-sm-9">
					<?php echo $this->Form->input("position", array("class"=> "form-control", "placeholder" => 'Position', "label"=> false)); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="padding-top: 8px">Office Phone</label>
					<div class="col-sm-9">
					<?php echo $this->Form->input("office_phone", array("class"=> "form-control", "placeholder" => 'Office Phone', "label"=> false)); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="padding-top: 8px">Mobile Phone</label>
					<div class="col-sm-9">
					<?php echo $this->Form->input("mobile_phone", array("class"=> "form-control", "placeholder" => 'Mobile Phone', "label"=> false)); ?>
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-3" style="padding-top: 8px">Email</label>
					<div class="col-sm-9">
					<?php echo $this->Form->input("email", array("class"=> "form-control", "placeholder" => 'Email', "label"=> false)); ?>
					</div> 
				</div>
				<?php
					//echo $this->Form->input('customer_id');
					//echo $this->Form->input('name');
					//echo $this->Form->input('position');
					//echo $this->Form->input('office_phone');
					//echo $this->Form->input('mobile_phone');
					//echo $this->Form->input('email');
				?>
				</fieldset>
				<?php
					if(isset($_GET['id'])){
						echo $this->Html->link(__('Back'), array('action' => '../customers/view/'.$_GET['id']), array('class' => 'btn btn-warning btn-sm'));
					}else{
						echo $this->Html->link(__('Back'), array('action' => '../customer_contact_people'), array('class' => 'btn btn-warning btn-sm'));
					}
					echo $this->Form->button('Reset', array('type'=>'reset', 'class' => 'btn btn-danger btn-sm','div' => false));
					echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
				?>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


<?php /*<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Customer'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customer Stations'), array('controller' => 'customer_stations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Station'), array('controller' => 'customer_stations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Types'), array('controller' => 'customer_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Type'), array('controller' => 'customer_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Business Categories'), array('controller' => 'customer_business_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Business Category'), array('controller' => 'customer_business_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Ownerships'), array('controller' => 'customer_ownerships', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Ownership'), array('controller' => 'customer_ownerships', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Contact People'), array('controller' => 'customer_contact_people', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Contact Person'), array('controller' => 'customer_contact_people', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Files'), array('controller' => 'customer_files', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer File'), array('controller' => 'customer_files', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'add')); ?> </li>
	</ul>
</div> */ ?>
