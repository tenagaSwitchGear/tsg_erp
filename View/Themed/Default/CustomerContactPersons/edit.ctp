<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customers Contact Person'), array('controller' => 'customer_contact_people', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Customer Contact Person</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		<div class="customerContactPeople form">
				<?php echo $this->Form->create('CustomerContactPerson', array('class' => 'form-horizontal')); ?>
					<fieldset>
					<?php echo $this->Form->input('id'); ?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Customer Id</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input("customer_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Name</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input("name", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Position</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input("position", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Office phone</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input("office_phone", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Mobile phone</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input("mobile_phone", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Email</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input("email", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
					<?php
						//echo $this->Form->input('id');
						//echo $this->Form->input('customer_id');
						//echo $this->Form->input('name');
						//echo $this->Form->input('position');
						//echo $this->Form->input('office_phone');
						//echo $this->Form->input('mobile_phone');
						//echo $this->Form->input('email');
					?>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
						<?php				
							echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
						?>
					</div>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>