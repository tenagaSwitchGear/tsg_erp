<div class="row"> 
  	<div class="col-xs-12">
  	<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  	<?php echo $this->Html->link(__('List Customers Contact Person'), array('controller' => 'customer_contact_people', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customer Contact Person</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="container">
				<dl>
					<dt class="col-sm-3"><?php echo __('Customer'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerContactPerson['Customer']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Name'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerContactPerson['CustomerContactPerson']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Position'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerContactPerson['CustomerContactPerson']['position']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Office Phone'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerContactPerson['CustomerContactPerson']['office_phone']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Mobile Phone'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerContactPerson['CustomerContactPerson']['mobile_phone']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Email'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerContactPerson['CustomerContactPerson']['email']); ?>
						&nbsp;
					</dd>
				</dl>
				<div class="clearfix">&nbsp;</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

