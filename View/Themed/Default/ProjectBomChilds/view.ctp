<div class="projectBomChildren view">
<h2><?php echo __('Project Bom Child'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectBomChild['ProjectBomChild']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectBomChild['SaleOrder']['id'], array('controller' => 'sale_orders', 'action' => 'view', $projectBomChild['SaleOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Bom'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectBomChild['ProjectBom']['name'], array('controller' => 'project_boms', 'action' => 'view', $projectBomChild['ProjectBom']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Id X'); ?></dt>
		<dd>
			<?php echo h($projectBomChild['ProjectBomChild']['parent_id_x']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($projectBomChild['ProjectBomChild']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bom Parent'); ?></dt>
		<dd>
			<?php echo h($projectBomChild['ProjectBomChild']['bom_parent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($projectBomChild['ProjectBomChild']['quantity']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project Bom Child'), array('action' => 'edit', $projectBomChild['ProjectBomChild']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project Bom Child'), array('action' => 'delete', $projectBomChild['ProjectBomChild']['id']), array(), __('Are you sure you want to delete # %s?', $projectBomChild['ProjectBomChild']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Children'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Child'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Boms'), array('controller' => 'project_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Items'), array('controller' => 'project_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Project Bom Items'); ?></h3>
	<?php if (!empty($projectBomChild['ProjectBomItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Order Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Project Bom Child Id'); ?></th>
		<th><?php echo __('Project Bom Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Quantity Total'); ?></th>
		<th><?php echo __('Quantity Shortage'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Price Total'); ?></th>
		<th><?php echo __('No Of Order'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($projectBomChild['ProjectBomItem'] as $projectBomItem): ?>
		<tr>
			<td><?php echo $projectBomItem['id']; ?></td>
			<td><?php echo $projectBomItem['sale_order_id']; ?></td>
			<td><?php echo $projectBomItem['inventory_item_id']; ?></td>
			<td><?php echo $projectBomItem['general_unit_id']; ?></td>
			<td><?php echo $projectBomItem['project_bom_child_id']; ?></td>
			<td><?php echo $projectBomItem['project_bom_id']; ?></td>
			<td><?php echo $projectBomItem['quantity']; ?></td>
			<td><?php echo $projectBomItem['quantity_total']; ?></td>
			<td><?php echo $projectBomItem['quantity_shortage']; ?></td>
			<td><?php echo $projectBomItem['price']; ?></td>
			<td><?php echo $projectBomItem['price_total']; ?></td>
			<td><?php echo $projectBomItem['no_of_order']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'project_bom_items', 'action' => 'view', $projectBomItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'project_bom_items', 'action' => 'edit', $projectBomItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'project_bom_items', 'action' => 'delete', $projectBomItem['id']), array(), __('Are you sure you want to delete # %s?', $projectBomItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
