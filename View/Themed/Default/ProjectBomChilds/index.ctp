<div class="projectBomChildren index">
	<h2><?php echo __('Project Bom Children'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_order_id'); ?></th>
			<th><?php echo $this->Paginator->sort('project_bom_id'); ?></th>
			<th><?php echo $this->Paginator->sort('parent_id_x'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('bom_parent'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectBomChildren as $projectBomChild): ?>
	<tr>
		<td><?php echo h($projectBomChild['ProjectBomChild']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projectBomChild['SaleOrder']['id'], array('controller' => 'sale_orders', 'action' => 'view', $projectBomChild['SaleOrder']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projectBomChild['ProjectBom']['name'], array('controller' => 'project_boms', 'action' => 'view', $projectBomChild['ProjectBom']['id'])); ?>
		</td>
		<td><?php echo h($projectBomChild['ProjectBomChild']['parent_id_x']); ?>&nbsp;</td>
		<td><?php echo h($projectBomChild['ProjectBomChild']['name']); ?>&nbsp;</td>
		<td><?php echo h($projectBomChild['ProjectBomChild']['bom_parent']); ?>&nbsp;</td>
		<td><?php echo h($projectBomChild['ProjectBomChild']['quantity']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectBomChild['ProjectBomChild']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectBomChild['ProjectBomChild']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectBomChild['ProjectBomChild']['id']), array(), __('Are you sure you want to delete # %s?', $projectBomChild['ProjectBomChild']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project Bom Child'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Boms'), array('controller' => 'project_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Items'), array('controller' => 'project_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
