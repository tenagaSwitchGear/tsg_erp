<div class="projectBomChildren form">
<?php echo $this->Form->create('ProjectBomChild'); ?>
	<fieldset>
		<legend><?php echo __('Add Project Bom Child'); ?></legend>
	<?php
		echo $this->Form->input('sale_order_id');
		echo $this->Form->input('project_bom_id');
		echo $this->Form->input('parent_id_x');
		echo $this->Form->input('name');
		echo $this->Form->input('bom_parent');
		echo $this->Form->input('quantity');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Project Bom Children'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Boms'), array('controller' => 'project_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Bom Items'), array('controller' => 'project_bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom Item'), array('controller' => 'project_bom_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
