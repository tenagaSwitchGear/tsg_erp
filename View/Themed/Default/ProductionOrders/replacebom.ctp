<?php echo $this->Html->link(__('Plan Production Orders'), array('action' => 'plan'), array('class' => 'btn btn-info btn-sm')); ?>


<div class="row"> 
    <div class="col-xs-12">
      
      <div class="x_panel tile">
          <div class="x_title">
            <h2>Replace Production Order BOM</h2>
              
          <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
          <?php echo $this->Session->flash(); ?>
          <!-- content start-->
          <p>Note: Replace BOM mean delete current Production Order BOM and reinsert new BOM from EAD. Please make sure no MRN has been <b>Issued</b> and new BOM has been edited by EAD.</p>
      <table class="table table-bordered">
       <tr>           
                  <td><?php echo __('Production'); ?></td>
                  <td>
                      <?php echo h($productionOrder['ProductionOrder']['name']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
            <td><?php echo __('Job'); ?></td>
            <td>
                <?php echo $this->Html->link($productionOrder['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $productionOrder['SaleJobChild']['sale_job_id'])); ?>
                &nbsp;
            </td>
        </tr>
        <tr>           
                  <td><?php echo __('Item'); ?></td>
                  <td>
                      <?php echo h($productionOrder['InventoryItem']['code']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>          
                  <td><?php echo __('Item Name'); ?></td>
                  <td>
                      <?php echo h($productionOrder['InventoryItem']['name']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>          
                  <td><?php echo __('Station'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobChild']['name']); ?>
                      &nbsp;
                  </td>
        </tr> 
        <tr>           
                  <td><?php echo __('Quantity'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobItem']['quantity']); ?> 
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('Created'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobItem']['created']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('Modified'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobItem']['modified']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('FAT Date'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobChild']['fat_date']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('Delivery Date'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobChild']['delivery_date']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
            <td><?php echo __('Status'); ?></td>
            <td>
                <?php echo $productionOrder['SaleJobItem']['production_status']; ?>
                &nbsp;
            </td>
        </tr>
      <?php echo $this->Form->create('ProductionOrder', array('class' => 'form-horizontal form-label-left', 'id' => 'form')); ?> 
      <?php echo $this->Form->input('id'); ?>
 

<?php echo $this->Form->input('quantity', array('type' => 'hidden')); ?> 
<?php echo $this->Form->input('sale_job_childs_id', array('type' => 'hidden')); ?>  
        <tr>
            <td>New BOM Code</td>
            <td>
              <?php echo $this->Form->input('Bom.code', array('id' => 'findBom', 'value' => $productionOrder['Bom']['code'], 'label' => false, 'class' => 'form-control')); ?>
              <?php echo $this->Form->input('bom_id', array('id' => 'bom_id', 'type' => 'hidden', 'value' => $productionOrder['SaleJobItem']['bom_id'])); ?> 
            </td>
        </tr>  

        <tr>
            <td>New BOM Name</td>
            <td>
              <?php echo $this->Form->input('Bom.name', array('id' => 'name', 'value' => $productionOrder['Bom']['name'], 'label' => false, 'class' => 'form-control', 'readonly' => true)); ?> 
            </td>
        </tr>

        <tr>
            <td></td>
            <td>
               <?php echo $this->Form->button('Replace BOM', array('name' => 'status', 'value' => 'Confirm', 'class' => 'btn btn-success')); ?>
            </td>
        </tr>
        <?php echo $this->Form->end(); ?>
      </table>
 
 

          <!-- content end -->
        </div>
    </div>
    </div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">  
$(function() { 
  $('#form').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
      e.preventDefault();
      return false;
    }
  });

  $('#findBom').autocomplete({ 
      source: function (request, response){ 
          $.ajax({
              type: "GET",                        
              url:baseUrl + 'boms/ajaxfindbom',           
              contentType: "application/json",
              dataType: "json",
              data: "term=" + $('#findBom').val(),                                                    
              success: function (data) { 
                response($.map(data, function (item) {
                    return {
                        id: item.id, 
                        value: item.code,
                        name: item.name 
                    }
                }));
            }
          });
      },
      select: function (event, ui) {   
          $('#bom_id').val(ui.item.id); 
          $('#name').val(ui.item.name);
           
      },
      minLength: 3
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small></div>" ).appendTo( ul );
  }; 
});
</script>
<?php $this->end(); ?>