<div class="row"> 
    <div class="col-xs-12">

        <?php echo $this->Html->link(__('Work In Progress (WIP)'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>  
        <?php echo $this->Html->link(__('Completed'), array('action' => 'index', 9), array('class' => 'btn btn-success btn-sm')); ?>
       
        
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Production Orders'); ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create('ProductionOrder', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
    <table cellpadding="0" cellspacing="0" class="table table-bordered">
        <tr>
        <td><?php echo $this->Form->input('name', array('placeholder' => 'Production Order No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
        </td>
        <td><?php echo $this->Form->input('station', array('type' => 'text', 'placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
        </td> 
        <td><?php echo $this->Form->input('from', array('placeholder' => 'From (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
        <td><?php echo $this->Form->input('to', array('placeholder' => 'To (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly_2')); ?></td>
        <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
        </tr>
    </table>
<?php $this->end(); ?> 

            <!-- content start-->                   
            <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
                <thead>
                <tr> 
                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                        <th><?php echo $this->Paginator->sort('sale_job_childs_id', 'Job No'); ?></th>
                        <th><?php echo $this->Paginator->sort('sale_job_childs_id', 'Station'); ?></th>
                        <th><?php echo $this->Paginator->sort('inventory_item_id', 'Item'); ?></th>
                        <th><?php echo $this->Paginator->sort('quantity'); ?></th>
                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                        <th><?php echo $this->Paginator->sort('start'); ?></th>
                        <th><?php echo $this->Paginator->sort('end'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $currentPage = empty($this->Paginator->params['paging']['ProductionOrder']['page']) ? 1 : $this->Paginator->params['paging']['ProductionOrder']['page']; $limit = $this->Paginator->params['paging']['ProductionOrder']['limit'];
                    $startSN = (($currentPage * $limit) + 1) - $limit;

                    foreach ($productionOrders as $productionOrder): ?>
                <tr> 
                    <td><?php echo h($productionOrder['ProductionOrder']['name']); ?>&nbsp;</td>
                    <td>
                        <?php echo $this->Html->link($productionOrder['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $productionOrder['SaleJob']['id'])); ?>
                    </td>
                    <td><?php echo h($productionOrder['SaleJobChild']['name']); ?>&nbsp;</td>
                    <td>
                        <?php echo $this->Html->link($productionOrder['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $productionOrder['InventoryItem']['id'])); ?>
                    </td>
                    <td><?php echo h($productionOrder['ProductionOrder']['quantity']); ?>&nbsp;</td>
                    <td><?php echo h($productionOrder['ProductionOrder']['created']); ?>&nbsp;</td>
                    <td><?php echo h($productionOrder['ProductionOrder']['start']); ?>&nbsp;</td>
                    <td><?php echo h($productionOrder['ProductionOrder']['end']); ?>&nbsp;</td>
                    <td><?php echo $status[$productionOrder['ProductionOrder']['status']]; ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $productionOrder['ProductionOrder']['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $productionOrder['ProductionOrder']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $productionOrder['ProductionOrder']['id']), array(), __('Are you sure you want to delete # %s?', $productionOrder['ProductionOrder']['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
                </tbody>
                </table>
                <p>
                <?php
                    //echo $this->Paginator->counter(array(
                    //  'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    //));
                ?>  </p>
                <div class="paging">
                <?php
                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
                    echo $this->Paginator->numbers(array('separator' => ''), array('class' => 'btn btn-default btn-sm'));
                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
                ?>
                </div>
            </div>
            <!-- content end -->
        </div>
    </div>
    </div>
</div>