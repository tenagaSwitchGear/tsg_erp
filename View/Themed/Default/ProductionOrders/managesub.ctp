<?php echo $this->Html->link(__('Back'), array('action' => 'editbom', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?> 

<style type="text/css">
.listing h2 {
  cursor: pointer;
  padding: 10px 20px;
  background-color:#CCC;
  margin-top:0px;
  margin-bottom:10px;
  font-size:18px;
  font-weight:700;
}
</style> 
<div class="row"> 
    <div class="col-xs-12">
       
      <div class="x_panel tile">
          <div class="x_title">
            <h2>Edit Production Order BOM</h2> 
          <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
          <?php echo $this->Session->flash(); ?>
          <!-- content start-->
          <div class="container table-responsive">
        <dl>
                  <dt class="col-sm-3"><?php echo __('Production No'); ?></dt>
                  <dd>
                      <?php echo $productionOrder['ProductionOrder']['name']; ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Job'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['SaleJob']['name'], array('controller' => 'salejobs', 'action' => 'view', $productionOrder['ProductionOrder']['sale_job_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Station'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['SaleJobChild']['name']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('BOM'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['Bom']['name'], array('controller' => 'boms', 'action' => 'view', $productionOrder['ProductionOrder']['bom_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Item'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $productionOrder['InventoryItem']['id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Quantity'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['quantity']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Created'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['created']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['modified']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Start'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['start']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('End'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['end']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Status'); ?></dt>
                  <dd>
                      <?php echo $status[$productionOrder['ProductionOrder']['status']]; ?>
                      &nbsp;
                  </dd>  

              </dl>
      </div>
      <div class="clearfix">&nbsp;</div>
  

      <ol class="breadcrumb">
        <li><?php echo $this->Html->link($productionOrder['Bom']['code'] . ' - ' . $productionOrder['Bom']['name'], array('action' => 'editbom', $productionOrder['ProductionOrder']['id'])); ?></li>
        <?php if(isset($breadcrumb['ProductionOrderChild'])) { ?> 
        <li><?php echo $this->Html->link($breadcrumb['ProductionOrderChild']['name'], array('controller' => 'production_orders', 'action' => 'editbom', $productionOrder['ProductionOrder']['id'], $breadcrumb['ProductionOrderChild']['id'], $breadcrumb['ProductionOrderChild']['bom_parent'])); ?></li>
        <?php } ?> 
      </ol>

        <h4>


        </h4>
<?php echo $this->Form->create('ProductionOrderChild'); ?>
<?php echo $this->Form->input('id'); ?> 
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered" id="tableBom"> 
      <tr>
          <th><?php echo __('Code'); ?></th>  
          <th><?php echo __('Name'); ?></th>
          <th><?php echo __('Qty/BOM'); ?></th>
          <th><?php echo __('UOM'); ?></th>                          
          <th><?php echo __('Total Qty'); ?></th>  
      </tr>  
    <tr>
<td> 
<?php echo $this->Form->input('name', array('id' => 'code', 'label' => false, 'class' => 'childcode', 'style' => 'width:260px')); ?> 

<?php echo $this->Form->input('inventory_item_id', array('id' => 'inventory_item_id', 'type' => 'hidden')); ?> 
</td> 
<td> 
<?php echo $this->Form->input('InventoryItem.name', array('id' => 'name', 'label' => false, 'readonly' => true, 'style' => 'width:260px')); ?> 
</td>  
<td>
<?php echo $this->Form->input('quantity', array('id' => 'quantity', 'label' => false, 'style' => 'width:50px', 'type' => 'text')); ?>
</td>
<td><?php echo $this->Form->input('general_unit_id', array('id' => 'general_unit_id', 'label' => false, 'options' => $units)); ?>
<?php echo $this->Form->input('no_of_order', array('id' => 'order_quantity', 'value' => $productionOrder['ProductionOrder']['quantity'], 'type' => 'hidden')); ?>

</td> 
<td><?php echo $this->Form->input('total_quantity', array('id' => 'total_quantity', 'label' => false, 'style' => 'width:50px', 'type' => 'text', 'readonly' => true)); ?></td> 
 
</tr>

 </table> 
<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>        
<?php echo $this->Form->end(); ?> 
      </div>
    </div>
  </div>
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript"> 

$(document).ready(function() {   
  $('#quantity').on('keyup', function() {
    var quantity = $(this).val();
    var order_quantity = $('#order_quantity').val();  
    var total = quantity * order_quantity;
    $('#total_quantity').val(total.toFixed(2)); 
  });
  
    $('#code').autocomplete({ 
      source: function (request, response) {  
          $.ajax({
              type: "GET",                        
              url:baseUrl + 'inventory_items/ajaxfinditem',           
              contentType: "application/json",
              dataType: "json",
              data: "term=" + $('#code').val(),                                                    
              success: function (data) { 
                response($.map(data, function (item) {
                    return {
                        id: item.id,
                        value: item.code,
                        name : item.name,
                        price: item.price,
                        code: item.code,
                        type: item.type,
                        note: item.note,
                        unit: item.unit 
                    }
                }));
            }
          });
      },
      select: function (event, ui) {  
          $('#inventory_item_id').val( ui.item.id ); 
          $('#name').val( ui.item.name ); 
          $('#code').val( ui.item.code );
          $('#general_unit_id').val(ui.item.unit);   
      },
      minLength: 3
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" ).append( "<div>" + item.name + "<br><small>" + item.code + "</small><br/><small>" + item.type + "</small><br><small>" + item.note +  "</small></div>" ).appendTo( ul );
  }; 

  $('.removeItem').each(function() {
    $(this).on('click', function() {
      var element = $(this).attr('id'); 
      var itemId = element.split('-'); 
      var row = itemId[1];
      $('#trow-'+row).html(''); 
      return false;
    });
  });
});
 
 

</script>
<?php $this->end(); ?>