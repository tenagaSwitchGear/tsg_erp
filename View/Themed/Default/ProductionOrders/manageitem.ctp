 
<?php echo $this->Html->link(__('Back'), array('action' => 'editbom', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?> 
 

<style type="text/css">
.listing h2 {
  cursor: pointer;
  padding: 10px 20px;
  background-color:#CCC;
  margin-top:0px;
  margin-bottom:10px;
  font-size:18px;
  font-weight:700;
}
</style> 
<div class="row"> 
    <div class="col-xs-12">
       
      <div class="x_panel tile">
          <div class="x_title">
            <h2>Edit Production Order BOM</h2> 
          <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
          <?php echo $this->Session->flash(); ?>
           
      <div class="clearfix">&nbsp;</div>
  

      <ol class="breadcrumb">
        <li><?php echo $this->Html->link($productionOrder['Bom']['code'] . ' - ' . $productionOrder['Bom']['name'], array('action' => 'editbom', $productionOrder['ProductionOrder']['id'])); ?></li>
        <?php if(isset($breadcrumb['ProductionOrderChild'])) { ?> 
        <li><?php echo $this->Html->link($breadcrumb['ProductionOrderChild']['name'], array('controller' => 'production_orders', 'action' => 'editbom', $productionOrder['ProductionOrder']['id'], $breadcrumb['ProductionOrderChild']['id'], $breadcrumb['ProductionOrderChild']['bom_parent'])); ?></li>
        <?php } ?> 
      </ol>

        <h4>


        </h4>
<?php echo $this->Form->create('ProductionOrder'); ?>
        <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered" id="tableBom">
         
              <tr>
                  <th><?php echo __('Code'); ?></th>  
                  <th><?php echo __('Name'); ?></th>
                  <th><?php echo __('Qty/BOM'); ?></th>
                  <th><?php echo __('UOM'); ?></th>                          
                  <th><?php echo __('Total Qty'); ?></th> 
                  <th><?php echo __('Action'); ?></th> 
              </tr> 
<?php 
$i = 0;
if($materials) { ?> 
<?php if(isset($materials['items'])) { ?>    
<?php 
$i = $i;
foreach($materials['items'] as $material) { ?>
<tr id="trow-<?php echo $i; ?>">
<td> 
<?php echo $this->Form->input('code.', array('id' => 'code-'.$i, 'label' => false, 'value' => $material['item_code'], 'class' => 'childcode', 'style' => 'width:260px')); ?> 

<?php echo $this->Form->input('inventory_item_id.', array('id' => 'inventory_item_id-'.$i, 'type' => 'hidden', 'value' => $material['inventory_item_id'])); ?> 
</td> 
<td> 
<?php echo $this->Form->input('name.', array('id' => 'name-'.$i, 'label' => false, 'value' => $material['item_name'], 'readonly' => true, 'style' => 'width:260px')); ?> 
</td>  
<td>
<?php echo $this->Form->input('quantity.', array('id' => 'qty-'.$i, 'value' => $material['quantity'], 'label' => false, 'style' => 'width:50px', 'type' => 'text')); ?>
</td>
<td><?php echo $this->Form->input('general_unit_id.', array('id' => 'general_unit_id-'.$i, 'value' => $material['unit_id'], 'label' => false, 'options' => $units)); ?>
</td> 
<td><?php echo $this->Form->input('total_quantity.', array('id' => 'qty-'.$i, 'value' => $material['quantity_total'], 'label' => false, 'style' => 'width:50px', 'type' => 'text', 'readonly' => true)); ?></td> 

<td><a href="#" id="removebtn-<?php echo $i; ?>" class="btn btn-danger btn-xs removeItem"><i class="fa fa-times"></i></a></td>  
</tr>
<?php 
$i++;
} 
// end foreach
?> 
<?php 
// End if material
} 
?> 
<?php } else { ?>
<h6><?php echo __('No Items found'); ?></h6>
<?php } ?>     
 </table>  
<a href="#" id="addNewItem" class="btn btn-default">Add Item</a>               
               
                 

<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>        
<?php echo $this->Form->end(); ?>
          <!-- content end -->
        </div>
    </div>
    </div>
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript"> 

$(function() {   

  var row = <?php echo $i; ?>;
  $('#addNewItem').on('click', function() {
    html = '<tr id="trow-'+row+'" style="background-color:#d8c5c5;">';
    html += '<td><input type="text" name="data[ProductionOrder][code][]" id="code-'+row+'" class="childcode" style="width:260px"></td>'; 
    html += '<input type="hidden" name="data[ProductionOrder][inventory_item_id][]" id="inventory_item_id-'+row+'">'; 
    html += '<td><input type="text" name="data[ProductionOrder][name][]" id="name-'+row+'" style="width:260px"readonly></td>'; 
    html += '<td><input type="text" value="1" name="data[ProductionOrder][quantity][]" id="qty-'+row+'" style="width:50px"></td>'; 
    html += '<td><select name="data[ProductionOrder][general_unit_id][]" id="general_unit_id-'+row+'">';
    html += '<?php foreach($units as $key => $val) { echo '<option value="'.$key.'">'.$val.'</option>'; } ?>';
    html += '</select></td>'; 
    html += '<input type="hidden" name="purchase_qty" id="purchase_qty-'+row+'" value="<?php echo $material['quantity_total']; ?>">';
    html += '<td><input value="<?php echo $material['quantity_total']; ?>" type="text" name="data[ProductionOrder][total_quantity][]" id="total_quantity-'+row+'" style="width:50px"readonly></td>'; 
    html += '<td><a href="#" onclick="removeItem('+row+'); return false;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a></td>';  
    html += '</tr>'; 
    $('#tableBom').append(html);
    findItem(row, $(this).val()); 
    calcTotalQty(row, $(this).val()); 
    row++; 
    return false;
  }); 

  $('.childcode').each(function() { 
    $(this).on('keyup', function() {
      var element = $(this).attr('id'); 
      var itemId = element.split('-'); 
      var row = itemId[1];
      findItem(row, $(this).val()); 
    }); 
  });

  $('.removeItem').each(function() {
    $(this).on('click', function() {
      var element = $(this).attr('id'); 
      var itemId = element.split('-'); 
      var row = itemId[1];
      $('#trow-'+row).html(''); 
      return false;
    });
  });
});

function calcTotalQty(row, val) {
  $('#qty-'+row).on('keyup', function() {
    var qty = $(this).val();
    var purchase_qty = $('#purchase_qty-'+row).val();
    var total = qty * purchase_qty;
    $('#total_quantity-'+row).val(total.toFixed(2));
  });
}

function removeItem(row) {  
    $('#trow-'+row).html('');  
    console.log(row);
    return false; 
}

function findItem(row, search) {  
  $('#code-'+row).autocomplete({ 
      source: function (request, response) {  
          $.ajax({
              type: "GET",                        
              url:baseUrl + 'inventory_items/ajaxfinditem',           
              contentType: "application/json",
              dataType: "json",
              data: "term=" + $('#code-'+row).val(),                                                    
              success: function (data) { 
                response($.map(data, function (item) {
                    return {
                        id: item.id,
                        value: item.code,
                        name : item.name,
                        price: item.price,
                        code: item.code,
                        type: item.type,
                        note: item.note,
                        unit: item.unit 
                    }
                }));
            }
          });
      },
      select: function (event, ui) {  
        // Check duplicate value
          var valueOfChangedInput = $(this).val();
          var timeRepeated = 0;
          $(".childcode").each(function() {
              //Inside each() check the 'valueOfChangedInput' with all other existing input
              if ($(this).val() == valueOfChangedInput ) {
                  timeRepeated++; //this will be executed at least 1 time because of the input, which is changed just now
              }
          });

          if(timeRepeated > 1) {
              alert("Duplicate value found !"); 
              $(this).toggleClass('border-red');
          } else {
            $('#inventory_item_id-'+row).val( ui.item.id ); 
            $('#name-'+row).val( ui.item.name ); 
            $('#code-'+row).val( ui.item.code );
            $('#general_unit_id-'+row).val(ui.item.unit);   
            $(this).toggleClass('border-red');
          } 
      },
      minLength: 3
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.name + "<br><small>" + item.code + "</small><br/><small>" + item.type + "</small><br><small>" + item.note +  "</small></div>" ).appendTo( ul );
    };
} 

</script>
<?php $this->end(); ?>