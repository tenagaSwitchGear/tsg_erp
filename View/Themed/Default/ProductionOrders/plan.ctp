<?php echo $this->Html->link(__('Plan Production Order'), array('action' => 'plan'), array('class' => 'btn btn-info btn-sm')); ?> 
<?php echo $this->Html->link(__('Purchase Item'), array('action' => 'plan', 'Waiting', 0), array('class' => 'btn btn-info btn-sm')); ?>  
<?php echo $this->Html->link(__('Work In Progress'), array('action' => 'plan', 'Started'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Closed'), array('action' => 'add'), array('class' => 'pull-right btn btn-success btn-sm')); ?>

<div class="row"> 
    <div class="col-xs-12"> 
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Plan Production Orders'); ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create('SaleJobItem', array('action' => 'plan', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
    <table cellpadding="0" cellspacing="0" class="table table-bordered">
        <tr>
        <td><?php echo $this->Form->input('name', array('placeholder' => 'Production Order No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
        </td>
        <td><?php echo $this->Form->input('station', array('type' => 'text', 'placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
        </td> 
        <td><?php echo $this->Form->input('from', array('placeholder' => 'From (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
        <td><?php echo $this->Form->input('to', array('placeholder' => 'To (YYYY-MM-DD)', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly2')); ?></td>
        <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
        </tr>
    </table>
<?php $this->end(); ?> 

            <!-- content start-->                   
            <div class="table-responsive">
               <table cellpadding="0" cellspacing="0" class="table table-bordered">
    <thead>
    <tr>
            <th><?php echo $this->Paginator->sort('InventoryItem.code', 'Item'); ?></th>
            <th>Type</th> 
            <th><?php echo $this->Paginator->sort('quantity', 'Qty'); ?></th> 
            <th><?php echo $this->Paginator->sort('SaleJobChild.station_name', 'Job'); ?></th>
            <th><?php echo $this->Paginator->sort('SaleJobChild.name', 'Name'); ?></th>  
            <th><?php echo $this->Paginator->sort('SaleJobChild.fat_date', 'FAT'); ?></th>
            <th><?php echo $this->Paginator->sort('SaleJobChild.delivery_date', 'Delivery'); ?></th> 
            <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($items as $saleJobChild): ?>
    <tr>
        <td><?php echo h($saleJobChild['InventoryItem']['code']); ?><br/><small><?php echo h($saleJobChild['InventoryItem']['name']); ?></small></td>

        <td><?php if($saleJobChild['SaleJobItem']['bom_id'] == 0) { echo 'Purchase'; } else { echo 'Manufacturing'; } ?></td>

        <td><?php echo h($saleJobChild['SaleJobItem']['quantity']); ?> <?php echo h($saleJobChild['GeneralUnit']['name']); ?></td>
        <td>
            <?php echo $this->Html->link($saleJobChild['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleJobChild['SaleJob']['id'])); ?>
        </td>  
        <td><?php echo h($saleJobChild['SaleJobChild']['name']); ?>&nbsp;</td>  
        <td><?php echo h($saleJobChild['SaleJobChild']['fat_date']); ?>&nbsp;</td>
        <td><?php echo h($saleJobChild['SaleJobChild']['delivery_date']); ?>&nbsp;</td> 
        <td class="actions">
            <?php echo $this->Html->link(__('View'), array('action' => 'viewplan', $saleJobChild['SaleJobItem']['id']), array('class' => 'btn btn-success btn-xs')); ?> 
        </td>
    </tr>
<?php endforeach; ?>
    </tbody>
    </table>
                <p>
                <?php
                    //echo $this->Paginator->counter(array(
                    //  'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    //));
                ?>  
                </p>
                <ul class="pagination">
            <?php
              echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
              echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
              echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
            ?>
            </ul>
            </div>
            <!-- content end -->
        </div>
    </div>
    </div>
</div>

 