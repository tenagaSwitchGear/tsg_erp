<?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index'), array('class' => 'btn btn-info')); ?>


<?php 

$group_id = $this->Session->read('Auth.User.group_id'); 

if($group_id == 12 || $group_id == 1) { ?>
<?php echo $this->Html->link(__('Change BOM'), array('action' => 'replacebom', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-danger', 'escape' => false)); ?> 

<?php echo $this->Html->link(__('Edit BOM'), array('action' => 'editbom', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-warning', 'escape' => false)); ?> 
<?php } ?>

<style type="text/css">
.listing h2 {
  cursor: pointer;
  padding: 10px 20px;
  background-color:#CCC;
  margin-top:0px;
  margin-bottom:10px;
  font-size:18px;
  font-weight:700;
}
</style> 
<div class="row">  
    <div class="col-xs-12">
       
      <div class="x_panel tile">
          <div class="x_title">
            <h2>Production Order</h2>
                <?php if($productionOrder['ProductionOrder']['status'] != 9) { ?>
                <?php echo $this->Html->link(__('Complete'), array('controller' => 'production_orders', 'action' => 'complete', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-success pull-right', 'escape' => false), __('Are you sure Production Order # %s completed?',$productionOrder['ProductionOrder']['name'])); ?> 
                <?php } ?>
          <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
          <?php echo $this->Session->flash(); ?>
          <?php echo $this->Session->flash(); ?>
          <!-- content start-->
          <div class="container table-responsive">
        <dl>
                  <dt class="col-sm-3"><?php echo __('Production No'); ?></dt>
                  <dd>
                      <?php echo $productionOrder['ProductionOrder']['name']; ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Job'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['SaleJob']['name'], array('controller' => 'salejobs', 'action' => 'view', $productionOrder['ProductionOrder']['sale_job_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Station'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['SaleJobChild']['name']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Item'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $productionOrder['InventoryItem']['id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Code'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['InventoryItem']['code'], array('controller' => 'inventory_items', 'action' => 'view', $productionOrder['InventoryItem']['id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Quantity'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['quantity']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Created'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['created']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['modified']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Start'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['start']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('End'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['end']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Status'); ?></dt>
                  <dd>
                      <?php echo $status[$productionOrder['ProductionOrder']['status']]; ?>
                      &nbsp;
                  </dd>

                  <dt class="col-sm-3"><?php echo __('Progress'); ?></dt>
                  <dd>  
                      <a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="status_<?php echo $productionOrder['ProductionOrder']['id']; ?>" class="editable" data-type="select" data-pk="<?php echo $productionOrder['ProductionOrder']['id']; ?>" data-name="#status_<?php echo $productionOrder['ProductionOrder']['id']; ?>" data-emptytext="<?php echo $productionOrder['ProductionOrder']['progress']; ?>%" data-url="<?php echo BASE_URL.'production_orders/setprogress'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $productionOrder['ProductionOrder']['progress']; ?>" data-source="<?php echo BASE_URL.'production_orders/getprogress'; ?>"></a>     

                      &nbsp;
                  </dd> 

                  

              </dl>
      </div>
      <div class="clearfix">&nbsp;</div>
  

      <ol class="breadcrumb">
        <li><?php echo $this->Html->link($productionOrder['Bom']['code'] . ' - ' . $productionOrder['Bom']['name'], array('controller' => 'production_orders', 'action' => 'view', $productionOrder['ProductionOrder']['id'])); ?></li>
        <?php if(isset($breadcrumb['ProductionOrderChild'])) { ?> 
        <li><?php echo $this->Html->link($breadcrumb['ProductionOrderChild']['name'], array('controller' => 'production_orders', 'action' => 'view', $productionOrder['ProductionOrder']['id'], $breadcrumb['ProductionOrderChild']['id'], $breadcrumb['ProductionOrderChild']['bom_parent'])); ?></li>
        <?php } ?> 
      </ol>

        <h4>


        </h4>
<?php echo $this->Form->create('ProductionOrder', array('url' => array('action'=>'view', $productionOrder['ProductionOrder']['id'], $child_id, $parent_id))); 
            ?>
        <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
          <thead>
              <tr>
                  <th><?php echo __('Item'); ?></th>  
                  <th><?php echo __('Unit'); ?></th>
                  <th><?php echo __('BOM Qty'); ?></th>
                  <th><?php echo __('Qty Required'); ?></th>                          
                  <th><?php echo __('Qty Issued'); ?></th>
                  <th><?php echo __('Qty Bal'); ?></th>
                  <th width="11%"><?php echo __('Qty Requested'); ?></th>
                  <th width="10%"><?php echo __('Request Qty'); ?></th>
              </tr>
          </thead>    
                <?php if($materials) { ?>
                  <?php if(isset($materials['childs'])) { ?>
                    <?php foreach($materials['childs'] as $material) { ?>
                    <?php if($material['has_item'] == 0 && $material['has_sub'] == 0 || $material['ProductionOrderChild']['type'] == 1) { ?>

<tr>
  <td><b><?php echo $this->Html->link($material['ProductionOrderChild']['name'], array('action' => 'view', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent'])); ?><br/>
  <small><?php echo $material['InventoryItem']['name']; ?></small>
  
  </td>
  <?php
    $qty_bal = $material['ProductionOrderChild']['no_of_order'] * $material['ProductionOrderChild']['quantity'] - $material['ProductionOrderChild']['issued_qty']; 
  ?>      
  <td><?php echo $material['GeneralUnit']['name']; ?></td>
  <td><?php echo $material['ProductionOrderChild']['quantity']; ?></td>

  <?php $qty_total = $material['ProductionOrderChild']['no_of_order'] * $material['ProductionOrderChild']['quantity']; ?>
  <td><?php echo $qty_total; ?></td>             
  <td><?php echo $material['ProductionOrderChild']['issued_qty']; ?></td>
  <td><?php echo $qty_bal; ?></td> 
  <td><?php echo $material['ProductionOrderChild']['quantity_req']; ?></td>
 
  <td>
  <?php if($material['ProductionOrderChild']['inventory_item_id'] != 0) { ?>

  <?php if($material['ProductionOrderChild']['quantity_req'] < $qty_bal) { ?> 

  <?php echo $this->Form->input('quantity_req', array('class' => 'reqQty form-control input-mini', 'label' => false, 'name'=>'quantity_req['.$material['ProductionOrderChild']['id'].'][]', 'value' => $qty_bal, 'data-id' => $qty_bal)); ?>



         <?php echo $this->Form->input('type', array('type' => 'hidden', 'label' => false, 'name'=>'type['.$material['ProductionOrderChild']['id'].'][]', 'value' => 'child')); ?>
       

       <?php echo $this->Form->input('req_qty', array('type' => 'hidden', 'label' => false, 'name'=>'req_qty['.$material['ProductionOrderChild']['id'].'][]', 'value' => $material['ProductionOrderChild']['quantity_req'])); ?>

              <?php echo $this->Form->input('max_qty', array('type' => 'hidden', 'label' => false, 'name'=>'max_qty['.$material['ProductionOrderChild']['id'].'][]', 'value' => $qty_bal)); ?>

            <?php echo $this->Form->input('quantity_total', array('type' => 'hidden', 'name'=>'quantity_total['.$material['ProductionOrderChild']['id'].'][]', 'value' => $qty_total)); ?>  
 
              
              <?php echo $this->Form->input('production_order_id', array('type' => 'hidden', 'label' => false, 'value' => $productionOrder['ProductionOrder']['id'])); ?>
              <?php echo $this->Form->input('production_order_child_id', array('type' => 'hidden', 'label' => false, 'value' => $child_id)); ?>

              <?php echo $this->Form->input('po_child_id', array('type' => 'hidden', 'label' => false, 'name'=>'po_child_id['.$material['ProductionOrderChild']['id'].'][]', 'value' => $material['ProductionOrderChild']['id'])); ?>

              <?php echo $this->Form->input('inventory_item_id', array('type' => 'hidden', 'label' => false, 'name'=>'inventory_item_id['.$material['ProductionOrderChild']['id'].'][]', 'value' => $material['ProductionOrderChild']['inventory_item_id'])); ?>
              <?php echo $this->Form->input('general_unit_id', array('type' => 'hidden', 'label' => false, 'name'=>'general_unit_id['.$material['ProductionOrderChild']['id'].'][]', 'value' => $material['ProductionOrderChild']['general_unit_id'])); ?>
<?php } ?>

<?php } else { ?>
  Invalid item. Please contact PPC To edit BOM
<?php } ?>
  </td>
 
</tr> 
<?php } else { ?>


<tr>
  <td><b><?php echo $this->Html->link($material['ProductionOrderChild']['name'], array('action' => 'view', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent'])); ?></b><br/>
  <small><?php echo $material['InventoryItem']['name']; ?></small></td>
        
  <td>Unt</td>
  <td>1</td>
  <td><?php echo $material['ProductionOrderChild']['no_of_order']; ?></td>             
  <td>-</td>
  <td>-</td>
  <td>-</td>
  <td>-</td>
</tr> 
                    <?php } ?> 
                    <?php } ?>
                  <?php } ?>
                  <?php if(isset($materials['items'])) { ?>
                  
            
            <?php foreach($materials['items'] as $material) { ?>
             <tr>
              <td><?php echo $material['item_code']; ?><br/>
              <small><?php echo $material['item_name']; ?></small></td> 
             
              <td><?php echo $material['unit_name']; ?></td>
              <td><?php echo $material['quantity']; ?></td>
              <td><?php echo $material['quantity_total']; ?></td>             
              <td><?php echo $material['quantity_issued']; ?></td>
              <td><?php echo $material['quantity_bal']; ?></td>
              <td><?php echo $material['quantity_req']; ?></td>
              <td>
              <?php if($material['quantity_req'] < $material['quantity_bal']) { ?> 

              <?php echo $this->Form->input('quantity_req', array('class' => 'reqQty form-control input-mini', 'label' => false, 'name'=>'quantity_req['.$material['id'].'][]', 'value' => $material['quantity_bal'], 'data-id' => $material['quantity_bal'])); ?>


              <?php echo $this->Form->input('type', array('type' => 'hidden', 'label' => false, 'name'=>'type['.$material['id'].'][]', 'value' => 'item')); ?>

              <?php echo $this->Form->input('req_qty', array('type' => 'hidden', 'label' => false, 'name'=>'req_qty['.$material['id'].'][]', 'value' => $material['quantity_req'])); ?>

              <?php echo $this->Form->input('max_qty', array('type' => 'hidden', 'label' => false, 'name'=>'max_qty['.$material['id'].'][]', 'value' => $material['quantity_bal'])); ?>
              
              <?php echo $this->Form->input('production_order_id', array('type' => 'hidden', 'label' => false, 'value' => $productionOrder['ProductionOrder']['id'])); ?>
              <?php echo $this->Form->input('production_order_child_id', array('type' => 'hidden', 'label' => false, 'value' => $child_id)); ?>
              <?php echo $this->Form->input('inventory_item_id', array('type' => 'hidden', 'label' => false, 'name'=>'inventory_item_id['.$material['id'].'][]', 'value' => $material['inventory_item_id'])); ?>
              <?php echo $this->Form->input('general_unit_id', array('type' => 'hidden', 'label' => false, 'name'=>'general_unit_id['.$material['id'].'][]', 'value' => $material['general_unit_id'])); ?>

              <?php echo $this->Form->input('quantity_total', array('type' => 'hidden', 'name'=>'quantity_total['.$material['id'].'][]', 'value' => $material['quantity_total'])); ?>  
              

              <?php } ?>
              </td>
            </tr>
                      <?php } ?>
                      
                  
                  <?php } ?>
          
                <?php } else { ?>
                <h6><?php echo __('No Items found'); ?></h6>
                <?php } ?>
                <tr>
                 
          
          <td colspan="9">
                   <?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
                 </td>
                </tr>
                <?php echo $this->Form->end(); ?>
        </table>        
                
         

          <!-- content end -->
        </div>
    </div>
    </div>
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript"> 
$(function() {  
  $('.reqQty').each(function() {
    $(this).on('keyup', function() {
      var requestQty = $(this).val();
      var attrVal = $(this).attr("data-id");
      if(requestQty > attrVal) {
        alert('Quantity cannot be greather than balance');
        $(this).val(attrVal);
      }
    });
  });
  $('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
        },
        success: function(data) {
          console.log(data);
        }
    });
});
</script>
<?php $this->end(); ?>