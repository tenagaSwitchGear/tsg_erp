<?php echo $this->Html->link(__('Plan Production Orders'), array('action' => 'plan'), array('class' => 'btn btn-info btn-sm')); ?>


<div class="row"> 
    <div class="col-xs-12">
      
      <div class="x_panel tile">
          <div class="x_title">
            <h2>Plan Production Order</h2>
              
          <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
          <?php echo $this->Session->flash(); ?>
          <!-- content start-->
          <p>To start Production Order, please change status to Confirm and click Submit button.</p>
      <table class="table table-bordered">
        <tr>
            <td><?php echo __('Job'); ?></td>
            <td>
                <?php echo $this->Html->link($productionOrder['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $productionOrder['SaleJobChild']['sale_job_id'])); ?>
                &nbsp;
            </td>
        </tr>
        <tr>            
                  <td><?php echo __('Item Code'); ?></td>
                  <td>
                      <?php echo h($productionOrder['InventoryItem']['code']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>          
                  <td><?php echo __('Item Name'); ?></td>
                  <td>
                      <?php echo h($productionOrder['InventoryItem']['name']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>          
                  <td><?php echo __('BOM Name'); ?></td>
                  <td>
                      <?php echo h($productionOrder['Bom']['name']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>          
                  <td><?php echo __('BOM Code'); ?></td>
                  <td>
                      <?php echo h($productionOrder['Bom']['code']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>          
                  <td><?php echo __('Station'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobChild']['name']); ?>
                      &nbsp;
                  </td>
        </tr> 
        <tr>           
                  <td><?php echo __('Quantity'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobItem']['quantity']); ?> <?php echo h($productionOrder['GeneralUnit']['name']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('Created'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobItem']['created']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('Modified'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobItem']['modified']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('FAT Date'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobChild']['fat_date']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
                  <td><?php echo __('Delivery Date'); ?></td>
                  <td>
                      <?php echo h($productionOrder['SaleJobChild']['delivery_date']); ?>
                      &nbsp;
                  </td>
        </tr>
        <tr>
            <td><?php echo __('Status'); ?></td>
            <td>
                <?php echo $productionOrder['SaleJobItem']['production_status']; ?>
                &nbsp;
            </td>
        </tr>
      <?php echo $this->Form->create('ProductionOrder', array('class' => 'form-horizontal form-label-left')); ?> 
      <?php echo $this->Form->input('sale_job_items_id', array('type' => 'hidden', 'value' => $productionOrder['SaleJobItem']['id'])); ?>

      <?php echo $this->Form->input('sale_job_childs_id', array('type' => 'hidden', 'value' => $productionOrder['SaleJobItem']['sale_job_child_id'])); ?>
      <?php echo $this->Form->input('sale_job_id', array('type' => 'hidden', 'value' => $productionOrder['SaleJobItem']['sale_job_id'])); ?>
      <?php echo $this->Form->input('is_bom', array('type' => 'hidden', 'value' => 1)); ?>
      <?php echo $this->Form->input('bom_id', array('type' => 'hidden', 'value' => $productionOrder['SaleJobItem']['bom_id'])); ?>
      <?php echo $this->Form->input('bom_name', array('type' => 'hidden', 'value' => $productionOrder['Bom']['name'])); ?>

      <?php echo $this->Form->input('inventory_item_id', array('type' => 'hidden', 'value' => $productionOrder['SaleJobItem']['inventory_item_id'])); ?>
      
      <?php echo $this->Form->input('status', array('type' => 'hidden', 'value' => 1)); ?>
      <?php echo $this->Form->input('progress', array('type' => 'hidden', 'value' => 0)); ?>

<?php echo $this->Form->input('quantity', array('id' => 'bomid', 'type' => 'hidden', 'value' => $productionOrder['SaleJobItem']['quantity'])); ?>  

        <tr>
            <td>Start Date</td>
            <td>
              <?php echo $this->Form->input('start', array('id' => 'dateonly', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
            </td>
        </tr> 

        <tr>
            <td>Expect Complete Date</td>
            <td>
              <?php echo $this->Form->input('end', array('id' => 'dateonly_2', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
            </td>
        </tr>  

        <tr>
            <td></td>
            <td>
               <?php echo $this->Form->button('Start Production Order', array('name' => 'status', 'value' => 'Confirm', 'class' => 'btn btn-success')); ?>
            </td>
        </tr>
        <?php echo $this->Form->end(); ?>
      </table>
 
 

          <!-- content end -->
        </div>
    </div>
    </div>
</div>

 