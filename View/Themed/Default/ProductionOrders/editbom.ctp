<?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index'), array('class' => 'btn btn-info')); ?>


<?php 

$group_id = $this->Session->read('Auth.User.group_id'); 

if($group_id == 12 || $group_id == 1) { ?>
<?php echo $this->Html->link(__('Change BOM'), array('action' => 'replacebom', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-danger', 'escape' => false)); ?> 

<?php echo $this->Html->link(__('Edit BOM'), array('action' => 'editbom', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-warning', 'escape' => false)); ?> 
<?php } ?>

<style type="text/css">
.listing h2 {
  cursor: pointer;
  padding: 10px 20px;
  background-color:#CCC;
  margin-top:0px;
  margin-bottom:10px;
  font-size:18px;
  font-weight:700;
}
</style> 
<div class="row"> 
    <div class="col-xs-12">
       
      <div class="x_panel tile">
          <div class="x_title">
            <h2>Edit Production Order BOM</h2> 
          <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
          <?php echo $this->Session->flash(); ?>
          <!-- content start-->
          <div class="container table-responsive">
        <dl>
                  <dt class="col-sm-3"><?php echo __('Production No'); ?></dt>
                  <dd>
                      <?php echo $productionOrder['ProductionOrder']['name']; ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Job'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['SaleJob']['name'], array('controller' => 'salejobs', 'action' => 'view', $productionOrder['ProductionOrder']['sale_job_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Station'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['SaleJobChild']['name']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('BOM'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['Bom']['name'], array('controller' => 'boms', 'action' => 'view', $productionOrder['ProductionOrder']['bom_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Item'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $productionOrder['InventoryItem']['id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Quantity'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['quantity']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Created'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['created']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['modified']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Start'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['start']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('End'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['end']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Status'); ?></dt>
                  <dd>
                      <?php echo $status[$productionOrder['ProductionOrder']['status']]; ?>
                      &nbsp;
                  </dd>  

              </dl>
      </div>
      <div class="clearfix">&nbsp;</div>
  

      <ol class="breadcrumb">
        <li><?php echo $this->Html->link($productionOrder['Bom']['code'] . ' - ' . $productionOrder['Bom']['name'], array('action' => 'editbom', $productionOrder['ProductionOrder']['id'])); ?></li>
        <?php if(isset($breadcrumb['ProductionOrderChild'])) { ?> 
        <li><?php echo $this->Html->link($breadcrumb['ProductionOrderChild']['name'], array('controller' => 'production_orders', 'action' => 'editbom', $productionOrder['ProductionOrder']['id'], $breadcrumb['ProductionOrderChild']['id'], $breadcrumb['ProductionOrderChild']['bom_parent'])); ?></li>
        <?php } ?> 
      </ol>

        <h4>


        </h4>
<?php echo $this->Form->create('ProductionOrder', array('url' => array('action'=>'editbom', $productionOrder['ProductionOrder']['id'], $child_id, $parent_id))); 
            ?>
        <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered" id="tableBom">
         
              <tr>
                  <th><?php echo __('Code'); ?></th>  
                  <th><?php echo __('Name'); ?></th>
                  <th><?php echo __('Qty/BOM'); ?></th>
                  <th><?php echo __('UOM'); ?></th>                          
                  <th><?php echo __('Total Qty'); ?></th> 
                  <th><?php echo __('Action'); ?></th> 
              </tr> 
                <?php 
                $i = 0;
                if($materials) { ?>

                  <?php if(isset($materials['childs'])) { ?>
                    <?php foreach($materials['childs'] as $material) { ?>

                    <!-- if purchased & non manufactured -->
                    <?php if($material['has_item'] == 0 && $material['has_sub'] == 0 || $material['ProductionOrderChild']['type'] == 1) { ?>

<tr>
  <td>
  <?php echo $this->Html->link($material['ProductionOrderChild']['name'], array('action' => 'editbom', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent'])); ?> 
  </td>
  <?php $qty_bal = $material['ProductionOrderChild']['no_of_order'] - $material['ProductionOrderChild']['issued_qty']; ?>      
  <td><?php echo $material['InventoryItem']['name']; ?></td>
  <td><?php echo $material['ProductionOrderChild']['quantity']; ?></td>
  <td><?php echo $material['GeneralUnit']['name']; ?></td>
  <?php $qty_total = $material['ProductionOrderChild']['no_of_order'] * $material['ProductionOrderChild']['quantity']; ?>
  <td><?php echo $qty_total; ?></td>              
 
  <td>

  <?php echo $this->Html->link('Edit', array('action' => 'managesub', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent']), array('class' => 'btn btn-info btn-xs')); ?><?php echo $this->Html->link('Manage Child', array('action' => 'manageitem', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent']), array('class' => 'btn btn-primary btn-xs')); ?>
  </td>
 
</tr> 

<!-- end if purchased & non manufactured -->

<?php } else { ?> 
<tr>
  <td>
  <?php echo $this->Html->link($material['ProductionOrderChild']['name'], array('action' => 'editbom', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent'])); ?> 
  </td>
  <?php $qty_bal = $material['ProductionOrderChild']['no_of_order'] - $material['ProductionOrderChild']['issued_qty']; ?>      
  <td><?php echo $material['InventoryItem']['name']; ?></td>
  <td><?php echo $material['ProductionOrderChild']['quantity']; ?></td>
  <td><?php echo $material['GeneralUnit']['name']; ?></td>
  <?php $qty_total = $material['ProductionOrderChild']['no_of_order'] * $material['ProductionOrderChild']['quantity']; ?>
  <td><?php echo $qty_total; ?></td>              
 
  <td>
 <?php echo $this->Html->link('View', array('action' => 'editbom', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent']), array('class' => 'btn btn-success btn-xs')); ?>
  <?php echo $this->Html->link('Edit', array('action' => 'managesub', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent']), array('class' => 'btn btn-info btn-xs')); ?><?php echo $this->Html->link('Manage Child', array('action' => 'manageitem', $material['ProductionOrderChild']['production_order_id'], $material['ProductionOrderChild']['id'], $material['ProductionOrderChild']['bom_parent']), array('class' => 'btn btn-primary btn-xs')); ?>
  </td>
 
</tr> 
                    <?php } ?> 
                    <?php  $i++; } ?>
                  <?php
                  } ?>


<?php if(isset($materials['items'])) { ?>    
<?php 
$i = $i;
foreach($materials['items'] as $material) { ?>
<tr id="trow-<?php echo $i; ?>">
<td> 
<?php echo $this->Form->input('InventoryItem.'.$i.'.code', array('id' => 'code-'.$i, 'label' => false, 'value' => $material['item_code'], 'class' => 'childcode')); ?> 
</td> 
<td> 
<?php echo $this->Form->input('InventoryItem.'.$i.'.name', array('id' => 'name-'.$i, 'label' => false, 'value' => $material['item_name'])); ?> 
</td>  
<td>
<?php echo $this->Form->input('ProductionOrderItem.'.$i.'.quantity', array('id' => 'qty-'.$i, 'value' => $material['quantity'], 'label' => false, 'style' => 'width:50px', 'type' => 'text')); ?>
</td>
<td><?php echo $this->Form->input('ProductionOrderItem.'.$i.'.general_unit_id', array('id' => 'unit-'.$i, 'value' => $material['unit_id'], 'label' => false, 'options' => $units)); ?>
</td> 
<td><?php echo $material['quantity_total']; ?></td>    
<td><a href="#" id="removebtn-<?php echo $i; ?>" class="btn btn-danger btn-xs removeItem"><i class="fa fa-times"></i></a></td>  
</tr>
    <?php 
    $i++;
    } 
    // end foreach
    ?> 
<?php 
  // End if material
  } 
?>

                <?php } else { ?>
                <h6><?php echo __('No Items found'); ?></h6>
                <?php } ?>
              
 </table> 
<?php echo $this->Html->link('Add Subassembly', array('action' => 'managesub', $productionOrder['ProductionOrder']['id'], $child_id, $parent_id), array('class' => 'btn btn-primary')); ?>               
               
                 

<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>        
<?php echo $this->Form->end(); ?>
          <!-- content end -->
        </div>
    </div>
    </div>
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript"> 

$(function() {   

  var row = <?php echo $i; ?>;
  $('#addNewItem').on('click', function() {
    html = '<tr id="trow-'+row+'">';
    html += '<td><input type="text" name="data[InventoryItem]['+row+'][code]" id="code-'+row+'" class="childcode"></td>'; 
    html += '<td><input type="text" name="data[InventoryItem]['+row+'][name]" id="name-'+row+'"></td>'; 
    html += '<td><input type="text" name="data[ProductionOrder]['+row+'][quantity]" id="qty-'+row+'" style="width:50px"></td>'; 
    html += '<td><select name="data[ProductionOrder]['+row+'][general_unit_id]" id="general_unit_id-'+row+'">';
    html += '<?php foreach($units as $key => $val) { echo '<option value="'.$key.'">'.$val.'</option>'; } ?>';
    html += '</select></td>'; 
    html += '<td><?php echo $material['quantity_total']; ?></td>'; 
    html += '<td><a href="#" onclick="removeItem('+row+'); return false;" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a></td>';  
    html += '</tr>'; 
    $('#tableBom').append(html);
    findItem(row, $(this).val()); 
    row++; 
    return false;
  }); 

  $('.childcode').each(function() { 
    $(this).on('keyup', function() {
      var element = $(this).attr('id'); 
      var itemId = element.split('-'); 
      var row = itemId[1];
      findItem(row, $(this).val()); 
    }); 
  });

  $('.removeItem').each(function() {
    $(this).on('click', function() {
      var element = $(this).attr('id'); 
      var itemId = element.split('-'); 
      var row = itemId[1];
      $('#trow-'+row).html(''); 
      return false;
    });
  });
});

function removeItem(row) {  
    $('#trow-'+row).html('');  
    console.log(row);
    return false; 
}

function findItem(row, search) {  
  $('#code-'+row).autocomplete({ 
      source: function (request, response) { 
        if($('#sale_job_child_id').val() == '') {
          alert('Please enter valid Job Number');
        }
          $.ajax({
              type: "GET",                        
              url:baseUrl + 'inventory_items/ajaxfinditem',           
              contentType: "application/json",
              dataType: "json",
              data: "term=" + $('#code-'+row).val(),                                                    
              success: function (data) { 
                response($.map(data, function (item) {
                    return {
                        id: item.id,
                        value: item.code,
                        name : item.name,
                        price: item.price,
                        code: item.code,
                        type: item.type,
                        note: item.note,
                        unit: item.unit 
                    }
                }));
            }
          });
      },
      select: function (event, ui) {  
          $('#inventory_item_id-'+row).val( ui.item.id ); 
          $('#name-'+row).val( ui.item.name ); 
          $('#code-'+row).val( ui.item.code );
      },
      minLength: 3
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.name + "<br><small>" + item.code + "</small><br/><small>" + item.type + "</small><br><small>" + item.note +  "</small></div>" ).appendTo( ul );
    };
} 

</script>
<?php $this->end(); ?>