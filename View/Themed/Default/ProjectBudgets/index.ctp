<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Budgets</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="projectBudgets index"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('SaleJobChild.station_name', 'Job'); ?></th>
			<th><?php echo $this->Paginator->sort('indirect_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('soft_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('material_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('margin'); ?></th>
			<th><?php echo $this->Paginator->sort('planning_price'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('used'); ?></th>
			<th><?php echo $this->Paginator->sort('balance'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>  
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectBudgets as $projectBudget): ?>
	<tr> 
		<td>
			<?php echo $this->Html->link($projectBudget['SaleJobChild']['station_name'], array('controller' => 'sale_job_children', 'action' => 'view', $projectBudget['SaleJob']['id'])); ?>
		</td>
		<td><?php echo h($projectBudget['ProjectBudget']['indirect_cost']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['soft_cost']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['material_cost']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['margin']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['planning_price']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['amount']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['used']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['balance']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['created']); ?>&nbsp;</td>
		<td><?php echo h($projectBudget['ProjectBudget']['modified']); ?>&nbsp;</td>  
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectBudget['ProjectBudget']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectBudget['ProjectBudget']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectBudget['ProjectBudget']['id']), array(), __('Are you sure you want to delete # %s?', $projectBudget['ProjectBudget']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
		</ul>
</div>
 
</div>
</div>
</div>
</div>