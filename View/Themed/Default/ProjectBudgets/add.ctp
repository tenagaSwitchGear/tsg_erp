<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Copy Budged</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="projectBudgets form">
<?php echo $this->Form->create('ProjectBudget'); ?> 
	<fieldset>
		<legend><?php echo __('Add Project Budget'); ?></legend>
	<?php
		echo $this->Form->input('sale_job_id');
		echo $this->Form->input('sale_job_child_id');
		echo $this->Form->input('indirect_cost');
		echo $this->Form->input('soft_cost');
		echo $this->Form->input('material_cost');
		echo $this->Form->input('margin');
		echo $this->Form->input('planning_price');
		echo $this->Form->input('amount');
		echo $this->Form->input('used');
		echo $this->Form->input('balance');
		echo $this->Form->input('user_id');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
 
</div>
</div>
</div>
</div>