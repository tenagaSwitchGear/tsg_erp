<div class="projectBudgets form">
<?php echo $this->Form->create('ProjectBudget'); ?>
	<fieldset>
		<legend><?php echo __('Edit Project Budget'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('sale_job_id');
		echo $this->Form->input('sale_job_child_id');
		echo $this->Form->input('indirect_cost');
		echo $this->Form->input('soft_cost');
		echo $this->Form->input('material_cost');
		echo $this->Form->input('margin');
		echo $this->Form->input('planning_price');
		echo $this->Form->input('amount');
		echo $this->Form->input('used');
		echo $this->Form->input('balance');
		echo $this->Form->input('user_id');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProjectBudget.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProjectBudget.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Project Budgets'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Jobs'), array('controller' => 'sale_jobs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job'), array('controller' => 'sale_jobs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Job Children'), array('controller' => 'sale_job_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Child'), array('controller' => 'sale_job_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
