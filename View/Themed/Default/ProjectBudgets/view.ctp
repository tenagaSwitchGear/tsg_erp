<?php echo $this->Html->link(__('Budget List'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Budget: <?php echo $projectBudget['SaleJobChild']['name']; ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="projectBudgets view-data">

	<dl>
		  
		<dt><?php echo __('Job No'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectBudget['SaleJobChild']['name'], array('controller' => 'sale_job_children', 'action' => 'view', $projectBudget['SaleJobChild']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Indirect Cost'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['indirect_cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Soft Cost'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['soft_cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Material Cost'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['material_cost']); ?>
			&nbsp;
		</dd> 
		<dt><?php echo __('Planning Price'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['planning_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Used'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['used']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Balance'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['balance']); ?> 
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($projectBudget['ProjectBudget']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sales Person'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectBudget['User']['firstname'], array('controller' => 'users', 'action' => 'view', $projectBudget['User']['id'])); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('MRN Cost'); ?></dt>
		<dd>
			<?php echo $issue_cost; ?>
			&nbsp;
		</dd>
		 
	</dl>
</div>

<h4>Budget Detail</h4>
<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
		<th>Subject</th>  
		<th>Amount</th>  
		<th>Used</th>  
		<th>Balance</th> 
	</tr>
	</thead>
	<tbody>
	<?php foreach ($accounts as $account): ?>
		<tr> 
			<td><?php echo h($account['AccountDepartmentBudget']['name']); ?></td> 
			<td><?php echo h($account['AccountDepartmentBudget']['total']); ?></td>
			<td><?php echo h($account['AccountDepartmentBudget']['used']); ?>&nbsp;</td> 
			<td><?php echo h($account['AccountDepartmentBudget']['balance']); ?>&nbsp;</td>   
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>


</div>
</div>
</div>
</div>
