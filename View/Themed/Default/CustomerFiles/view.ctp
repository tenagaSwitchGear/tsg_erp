<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customer Files'), array('controller' => 'customer_files', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customer File</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="container table-responsive">
				<dl>
					<dt class="col-sm-3"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						: #<?php echo h($customerFile['CustomerFile']['id']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Customer'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerFile['Customer']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Name'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customerFile['CustomerFile']['name']); ?>
						&nbsp;
					</dd>
                    <dt class="col-sm-3"><?php echo __('FileName'); ?></dt>
                    <dd class="col-sm-9">
                        : <?php echo h($customerFile['CustomerFile']['filename']); ?>
                        &nbsp;
                    </dd>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
            <?php   
                            if(isset($_GET['id'])){
                                echo $this->Html->link(__('Back'), array('action' => '../customers/view/'.$_GET['id']), array('class' => 'btn btn-warning btn-sm'));
                            }else{
                                echo $this->Html->link(__('Back'), array('action' => '../customer_files/'), array('class' => 'btn btn-warning btn-sm'));
                            }                       
                            //echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
                        ?>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>