<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Customer File'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customer Files</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
				<thead>
				<tr>
						<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
						<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
						<th><?php echo $this->Paginator->sort('name'); ?></th>
                        <th><?php echo $this->Paginator->sort('filename'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php 
					$currentPage = empty($this->Paginator->params['paging']['CustomerFile']['page']) ? 1 : $this->Paginator->params['paging']['CustomerFile']['page']; $limit = $this->Paginator->params['paging']['CustomerFile']['limit'];
					$startSN = (($currentPage * $limit) + 1) - $limit;

					foreach ($customerFiles as $customerFile):
				?>
				<tr>
					<td class="text-center"><?php echo $startSN++; ?></td>
					<td>
						<?php echo $this->Html->link($customerFile['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $customerFile['Customer']['id'])); ?>
					</td>
					<td><?php echo h($customerFile['CustomerFile']['name']); ?>&nbsp;</td>
                    <td><?php echo $this->Html->link($customerFile['CustomerFile']['filename'], '/files/customer_file/filename/' . $customerFile['CustomerFile']['dir'] . '/' . $customerFile['CustomerFile']['filename'], array('target' => '_blank')); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $customerFile['CustomerFile']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $customerFile['CustomerFile']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $customerFile['CustomerFile']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($customerFile['CustomerFile']['name']).'"', $customerFile['CustomerFile']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class' => 'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>