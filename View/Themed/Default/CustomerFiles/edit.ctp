<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Customers Files'), array('controller' => 'customer_files', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Customer File</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		<div class="customerFiles form">
				<?php echo $this->Form->create('CustomerFile', array('class'=>'form-horizontal', 'type'=>'file')); ?>
					<fieldset>
						<?php echo $this->Form->input("id"); ?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Customer Id</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("customer_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Name</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("name", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Attachment (PDF only)</label>
                            <div class="col-sm-9">
                                <?php echo $this->Form->input("filename", array("class"=> "form-control", "type"=>"file", "label"=> false, "required" => false)); ?>
                                <?php echo $this->Form->input('dir', array('type' => 'hidden')); ?>
                            </div> 
                        </div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
						<?php	
                            if(isset($_GET['id'])){
                                echo $this->Html->link(__('Back'), array('action' => '../customers/view/'.$_GET['id']), array('class' => 'btn btn-warning btn-sm'));
                            }else{
                                echo $this->Html->link(__('Back'), array('action' => '../customer_files/'), array('class' => 'btn btn-warning btn-sm'));
                            }						
							echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
						?>
					</div>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>
