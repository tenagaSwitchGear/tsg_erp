<?php echo $this->Html->link(__('List Project Drawings'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add New Drawing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
    <?php echo $this->Form->create('ProjectDrawing', array('class'=>'form-horizontal')); ?>
    <div class="form-group">
    <label class="col-sm-3">Enter Job No</label>
      <div class="col-sm-9">
        <?php echo $this->Form->input('find_job', array('id' => 'find_job', 'class' => 'form-control', 'label' => false)); ?>
        <?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden')); ?>
        <?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden')); ?>
        <?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden')); ?>
        </div>
      </div>
         
       
        <?php echo $this->Form->input('contractor', array('type' => 'hidden', 'label' => false)); ?>
       

      <div class="form-group">
        <label class="col-sm-3">Contractor PIC</label>
        <div class="col-sm-9">
        <?php echo $this->Form->input('contractor_pic', array('class' => 'form-control', 'label' => false)); ?>
        </div>
      </div> 

      <div class="form-group">
        <label class="col-sm-3">Contractor Phone</label>
        <div class="col-sm-9">
        <?php echo $this->Form->input('contractor_phone', array('class' => 'form-control', 'label' => false)); ?>
        </div>
      </div>  

      <div class="form-group">
        <label class="col-sm-3">Note (Optional)</label>
        <div class="col-sm-9">
        <?php echo $this->Form->input('remark', array('class' => 'form-control', 'label' => false, 'type' => 'textarea', 'required' => false)); ?>
        </div>
      </div>   

      <!--<div id="output"></div>

      <a href="#" id="addMore" class="btn btn-default"><i class="fa fa-plus"></i>Add Drawing</a>-->

      <div class="form-group"> 
        <?php echo $this->Form->submit('Next Step', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
      </div>    
      <?php $this->Form->end(); ?>

      </div> 
    </div>
  </div> 
</div> 

<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).ready(function() { 
  $('#find_job').autocomplete({ 
    source: function (request, response){ 
      $.ajax({
        type: "GET",                        
        url:baseUrl + 'sale_jobs/ajaxfindjob',           
        contentType: "application/json",
        dataType: "json",
        data: "term=" + $('#find_job').val(),                                                    
        success: function (data) { 
          response($.map(data, function (item) {
              return {
                  id: item.id,
                  value: item.name,
                  customer_id: item.customer_id,
                  sale_quotation_id: item.sale_quotation_id,
                  customer: item.customer,
                  customer_id: item.customer_id,
                  sale_job_child_id: item.sale_job_child_id
              }
          }));
        }
      });
    },
    select: function (event, ui) {  
        $('#sale_job_id').val( ui.item.id );   
        $('#sale_job_child_id').val( ui.item.sale_job_child_id );
        $('#customer_id').val( ui.item.customer_id );

         
    },
    minLength: 3
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.customer + "</small><br>" +  "</div>" ).appendTo( ul );
  }; 
 
  var row = 0;
  $('#addMore').click(function() {   
    var html = '<div id="removeItem'+row+'">'; 
    html += '<div class="form-group">';
    html += '<label class="col-sm-3">';
    html += 'Drawing (Max 100mb - PDF/JPG/PNG)';  
    html += '</label>';
    html += '<div class="col-sm-4">';
    html += '<input type="file" name="data[ProjectDrawingFile]['+row+'][name]" class="form-control"required>';  
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][directory]">'; 
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][project_drawing_id]">'; 
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][user_id]" value="<?php echo $this->Session->read('Auth.User.id'); ?>">'; 
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][created]" value="<?php echo date('Y-m-d H:i:s'); ?>">';   
    html += '</div>';
    html += '<div class="col-sm-4">';
    html += '<textarea name="data[ProjectDrawingFile]['+row+'][remark]" class="form-control" placeholder="Remark / note (Optional)"></textarea>'; 
    html += '</div>'; 
    html += '<div class="col-sm-1">';
    html += '<a href="#" class="btn btn-danger" onclick="removeItem('+row+'); return false"><i class="fa fa-times"></i></a>';
    html += '</div>';
    html += '</div>';
    html += '</div>';     
    row++;  
    $("#output").append(html);      
    return false;
  });   
});  

function removeItem(row) {
  $('#removeItem'+row).html('');
  return false;
}
</script>
<?php $this->end(); ?>