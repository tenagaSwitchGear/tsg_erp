<?php echo $this->Html->link(__('List Project Drawings'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('Add Status'), array('controller' => 'project_drawing_trackings', 'action' => 'add', $projectDrawing['ProjectDrawing']['id']), array('class' => 'btn btn-success')); ?>
<?php echo $this->Html->link(__('Add Files'), array('controller' => 'project_drawing_files', 'action' => 'add', $projectDrawing['ProjectDrawing']['id']), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Html->link(__('Edit'), array('controller' => 'project_drawings', 'action' => 'edit', $projectDrawing['ProjectDrawing']['id']), array('class' => 'btn btn-warning')); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View Drawing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="projectDrawings view-data"> 
	<dl> 
		<dt><?php echo __('Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectDrawing['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $projectDrawing['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Station'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectDrawing['SaleJobChild']['name'] . ' (' . $projectDrawing['SaleJobChild']['station_name'] . ')', array('controller' => 'sale_jobs', 'action' => 'view', $projectDrawing['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($projectDrawing['ProjectDrawing']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projectDrawing['ProjectDrawing']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($projectDrawing['ProjectDrawing']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectDrawing['User']['username'], array('controller' => 'users', 'action' => 'view', $projectDrawing['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($projectDrawing['ProjectDrawing']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($projectDrawing['ProjectDrawing']['remark']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
<div class="related">
	 

	<h4><?php echo __('Drawing Files'); ?></h4>
	<?php if (!empty($projectDrawing['ProjectDrawingFile'])): ?>
		<table cellpadding = "0" cellspacing = "0" class="table">
		<tr>
		<th><?php echo __('Item'); ?></th>
		<th><?php echo __('Drawing No'); ?></th>
			<th><?php echo __('Name'); ?></th>
			<th><?php echo __('Remark'); ?></th>
			<th><?php echo __('Created'); ?></th> 
			<th><?php echo __('Status'); ?></th> 
			<th><?php echo __('Action'); ?></th> 
		</tr>
		<?php foreach ($projectDrawing['ProjectDrawingFile'] as $file): ?>
			<tr>
			<td><?php echo h($file['product']) ?></td>
				<td><?php echo h($file['drawing_no']); ?></td> 
				<td><?php echo $this->Html->link(__($file['title']), BASE_URL . '/files/project_drawing_file/name/' . $file['directory'] . '/' . $file['name'], array('target' => '_blank')); ?></td>
				<td><?php echo h($file['remark']); ?></td>
				<td><?php echo $file['created']; ?></td> 
				<td><?php echo h($file['status']); ?></td> 
				<td>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'project_drawing_files', 'action' => 'edit', $file['id'], $projectDrawing['ProjectDrawing']['id']), array('class' => 'btn btn-warning btn-sm')); ?>

					<?php echo $this->Html->link(__('Add Status'), array('controller' => 'project_drawing_trackings', 'action' => 'add', $file['id'], $projectDrawing['ProjectDrawing']['id']), array('class' => 'btn btn-primary btn-sm')); ?>

					<?php echo $this->Html->link(__('View Status'), array('controller' => 'project_drawing_files', 'action' => 'view', $file['id'], $projectDrawing['ProjectDrawing']['id']), array('class' => 'btn btn-success btn-sm')); ?>
				</td> 
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?> 

</div>

</div>
</div>
</div>
</div>


