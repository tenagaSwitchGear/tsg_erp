<?php echo $this->Html->link(__('All Drawing'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>

<?php echo $this->Html->link(__('Add New Drawing'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Drawing List</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
<?php echo $this->Form->create('ProjectDrawing', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Drawing No', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('sale_job_child_id', array('type' => 'text', 'placeholder' => 'Job No', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		 <td><?php 
		 $status = array(
		'0' => 'Draft',
		'Submitted' => 'Submitted',
		'Receive From Client' => 'Receive From Client',
		'Rejected' => 'Rejected',
		'Revise & Resubmitted' => 'Revise & Resubmitted',
		'Approved' => 'Approved',
		);
		 echo $this->Form->input('status', array('options' => $status, 'empty' => '-All Status-', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
<div class="table-responsive">


	<h2><?php echo __('Project Drawings'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th>Drawing No</th> 
			<th>Job</th>  
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectDrawings as $projectDrawing): ?>
	<tr>
		<td><?php echo h($projectDrawing['ProjectDrawing']['id']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawing['ProjectDrawing']['name']); ?>&nbsp;</td>
		 
		<td><?php echo h($projectDrawing['SaleJobChild']['station_name']); ?> (<?php echo h($projectDrawing['SaleJobChild']['name']); ?>)</td> 
		<td><?php echo h($projectDrawing['ProjectDrawing']['created']); ?>&nbsp;</td> 
		<td>
			<?php echo $this->Html->link($projectDrawing['User']['username'], array('controller' => 'users', 'action' => 'view', $projectDrawing['User']['id'])); ?>
		</td>
		<td><?php echo $projectDrawing['ProjectDrawing']['status']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectDrawing['ProjectDrawing']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectDrawing['ProjectDrawing']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectDrawing['ProjectDrawing']['id']), array(), __('Are you sure you want to delete # %s?', $projectDrawing['ProjectDrawing']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
		</ul>
</div>
 


		</div> 
    </div>
  </div> 
</div>

<?php

function status($status) {
	if($status == 0) {
		return 'Draft';
	} else {
		return $status;
	}
}
?>