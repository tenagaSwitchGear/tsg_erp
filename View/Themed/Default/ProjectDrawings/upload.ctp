<!-- page content -->
<?php
  echo $this->Html->css('/vendors/dropzone/dist/min/dropzone.min.css');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Upload Drawing</h2>
         
        <div class="clearfix"></div>
      </div>
      <div class="x_content">  
      <?php echo $this->Form->create('ProjectDrawingFile', array('class'=>'dropzone', 'type' => 'file', 'id' => 'dropzoneUploader')); ?>
      <?php echo $this->Form->input('directory', array('type' => 'hidden')); ?>
      <?php echo $this->Form->input('project_drawing_id', array('type' => 'hidden')); ?>
      <?php echo $this->Form->input('name', array('type' => 'file')); ?>
        <br />
        <br />
        <br />
        <br /> 
        <div class="text-right">   
        <?php echo $this->Form->button('Finish',array('class'=>'btn btn-success')); ?>
        <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
 
