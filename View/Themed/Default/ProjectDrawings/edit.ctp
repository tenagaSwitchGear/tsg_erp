<?php echo $this->Html->link(__('List Project Drawings'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>
 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View Drawing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="projectDrawings form">
	<?php echo $this->Form->create('ProjectDrawing', array('class'=>'form-horizontal')); ?> 
		<?php echo $this->Form->input('id'); ?>
		<div class="form-group">
		<label class="col-sm-3">Job No</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('sale_job_id', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>
		<div class="form-group">
		<label class="col-sm-3">Drawing No</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		</div> 
		<div class="form-group">
		<label class="col-sm-3">Status</label>
		<div class="col-sm-9">
		<?php $status = array(
		'Draft' => 'Draft',
		'Submitted' => 'Submitted',
		'Receive From Client' => 'Receive From Client',
		'Rejected' => 'Rejected',
		'Revise & Resubmitted' => 'Revise & Resubmitted',
		'Approved' => 'Approved',
		);
		?>
	<?php echo $this->Form->input('status', array('options' => $status, 'empty' => '-Select Status-', 'class' => 'form-control', 'label' => false)); ?> 
		</div>
		</div> 
	<div class="form-group"> 
        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?> 
        </div>	  
		<?php $this->Form->end(); ?>

	</div>
 

</div>
</div>
</div>
</div>
