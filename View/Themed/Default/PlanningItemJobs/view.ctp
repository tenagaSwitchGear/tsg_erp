<div class="planningItemJobs view">
<h2><?php echo __('Planning Item Job'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($planningItemJob['PlanningItemJob']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Job Child'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItemJob['SaleJobChild']['name'], array('controller' => 'sale_job_children', 'action' => 'view', $planningItemJob['SaleJobChild']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit Price'); ?></dt>
		<dd>
			<?php echo h($planningItemJob['PlanningItemJob']['unit_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Planning Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItemJob['PlanningItem']['id'], array('controller' => 'planning_items', 'action' => 'view', $planningItemJob['PlanningItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Planning'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItemJob['Planning']['name'], array('controller' => 'plannings', 'action' => 'view', $planningItemJob['Planning']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($planningItemJob['PlanningItemJob']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($planningItemJob['PlanningItemJob']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($planningItemJob['PlanningItemJob']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Price'); ?></dt>
		<dd>
			<?php echo h($planningItemJob['PlanningItemJob']['total_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Currency'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planningItemJob['GeneralCurrency']['name'], array('controller' => 'general_currencies', 'action' => 'view', $planningItemJob['GeneralCurrency']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Planning Item Job'), array('action' => 'edit', $planningItemJob['PlanningItemJob']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Planning Item Job'), array('action' => 'delete', $planningItemJob['PlanningItemJob']['id']), array(), __('Are you sure you want to delete # %s?', $planningItemJob['PlanningItemJob']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Item Jobs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item Job'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Job Children'), array('controller' => 'sale_job_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Child'), array('controller' => 'sale_job_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('controller' => 'planning_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plannings'), array('controller' => 'plannings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning'), array('controller' => 'plannings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Currencies'), array('controller' => 'general_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Currency'), array('controller' => 'general_currencies', 'action' => 'add')); ?> </li>
	</ul>
</div>
