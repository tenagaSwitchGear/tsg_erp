<div class="planningItemJobs index">
	<h2><?php echo __('Planning Item Jobs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_job_child_id'); ?></th>
			<th><?php echo $this->Paginator->sort('unit_price'); ?></th>
			<th><?php echo $this->Paginator->sort('planning_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('planning_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('total_price'); ?></th>
			<th><?php echo $this->Paginator->sort('general_currency_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($planningItemJobs as $planningItemJob): ?>
	<tr>
		<td><?php echo h($planningItemJob['PlanningItemJob']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($planningItemJob['SaleJobChild']['name'], array('controller' => 'sale_job_children', 'action' => 'view', $planningItemJob['SaleJobChild']['id'])); ?>
		</td>
		<td><?php echo h($planningItemJob['PlanningItemJob']['unit_price']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($planningItemJob['PlanningItem']['id'], array('controller' => 'planning_items', 'action' => 'view', $planningItemJob['PlanningItem']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($planningItemJob['Planning']['name'], array('controller' => 'plannings', 'action' => 'view', $planningItemJob['Planning']['id'])); ?>
		</td>
		<td><?php echo h($planningItemJob['PlanningItemJob']['created']); ?>&nbsp;</td>
		<td><?php echo h($planningItemJob['PlanningItemJob']['status']); ?>&nbsp;</td>
		<td><?php echo h($planningItemJob['PlanningItemJob']['quantity']); ?>&nbsp;</td>
		<td><?php echo h($planningItemJob['PlanningItemJob']['total_price']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($planningItemJob['GeneralCurrency']['name'], array('controller' => 'general_currencies', 'action' => 'view', $planningItemJob['GeneralCurrency']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $planningItemJob['PlanningItemJob']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $planningItemJob['PlanningItemJob']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $planningItemJob['PlanningItemJob']['id']), array(), __('Are you sure you want to delete # %s?', $planningItemJob['PlanningItemJob']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Planning Item Job'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Job Children'), array('controller' => 'sale_job_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Child'), array('controller' => 'sale_job_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('controller' => 'planning_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plannings'), array('controller' => 'plannings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning'), array('controller' => 'plannings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Currencies'), array('controller' => 'general_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Currency'), array('controller' => 'general_currencies', 'action' => 'add')); ?> </li>
	</ul>
</div>
