<div class="planningItemJobs form">
<?php echo $this->Form->create('PlanningItemJob'); ?>
	<fieldset>
		<legend><?php echo __('Edit Planning Item Job'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('sale_job_child_id');
		echo $this->Form->input('unit_price');
		echo $this->Form->input('planning_item_id');
		echo $this->Form->input('planning_id');
		echo $this->Form->input('status');
		echo $this->Form->input('quantity');
		echo $this->Form->input('total_price');
		echo $this->Form->input('general_currency_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('PlanningItemJob.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('PlanningItemJob.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Planning Item Jobs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Job Children'), array('controller' => 'sale_job_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Child'), array('controller' => 'sale_job_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Planning Items'), array('controller' => 'planning_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning Item'), array('controller' => 'planning_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plannings'), array('controller' => 'plannings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Planning'), array('controller' => 'plannings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Currencies'), array('controller' => 'general_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Currency'), array('controller' => 'general_currencies', 'action' => 'add')); ?> </li>
	</ul>
</div>
