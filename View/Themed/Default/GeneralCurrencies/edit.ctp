<div class="row"> 
  <div class="col-xs-12">
    <?php echo $this->Html->link(__('Currency List'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit Currency</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 
<?php echo $this->Form->create('GeneralCurrency', array('class' => 'form-horizontal')); ?>
<?php echo $this->Form->input('id'); ?>
	<div class="form-group"> 
	<label>Name</label>
	<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	<div class="form-group"> 
	<label>Symbol</label>
	<?php echo $this->Form->input('symbol', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	<div class="form-group"> 
	<label>ISO Code</label>
	<?php echo $this->Form->input('iso_code', array('class' => 'form-control', 'label' => false)); ?>
	</div> 
	<?php echo $this->Form->input('is_default', array('type' => 'hidden', 'value' => 0, 'class' => 'form-control', 'label' => false)); ?>
 
	<div class="form-group"> 
	<label>Rate (RM * Rate)</label>
	<?php echo $this->Form->input('rate', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	<div class="form-group"> 
	<label>Default Margin (%)</label>
	<?php echo $this->Form->input('margin', array('class' => 'form-control', 'label' => false)); ?>
	</div> 
	<div class="form-group">  
		<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?>

	</div>
	<?php $this->Form->end(); ?> 
 
</div>
</div>
</div>
</div>
