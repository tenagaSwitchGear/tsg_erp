<div class="row"> 
  <div class="col-xs-12">
    <?php echo $this->Html->link(__('Currency List'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    <?php echo $this->Html->link(__('Add Currency'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Currency</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="generalCurrencies view-data">
<h2><?php echo __('General Currency'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($generalCurrency['GeneralCurrency']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($generalCurrency['GeneralCurrency']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Symbol'); ?></dt>
		<dd>
			<?php echo h($generalCurrency['GeneralCurrency']['symbol']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Iso Code'); ?></dt>
		<dd>
			<?php echo h($generalCurrency['GeneralCurrency']['iso_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Default'); ?></dt>
		<dd>
			<?php echo h($generalCurrency['GeneralCurrency']['is_default']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rate'); ?></dt>
		<dd>
			<?php echo h($generalCurrency['GeneralCurrency']['rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Margin'); ?></dt>
		<dd>
			<?php echo h($generalCurrency['GeneralCurrency']['margin']); ?>
			&nbsp;
		</dd>
	</dl>
</div> 

</div>
</div>
</div>
</div>