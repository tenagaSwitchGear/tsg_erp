<div class="row"> 
  <div class="col-xs-12">
    <?php echo $this->Html->link(__('Add Currency'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Currency</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="generalCurrencies index">
	<h2><?php echo __('Currencies'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('symbol'); ?></th>
			<th><?php echo $this->Paginator->sort('iso_code'); ?></th>
			<th><?php echo $this->Paginator->sort('is_default'); ?></th>
			<th><?php echo $this->Paginator->sort('rate'); ?></th>
			<th><?php echo $this->Paginator->sort('margin'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($generalCurrencies as $generalCurrency): ?>
	<tr> 
		<td><?php echo h($generalCurrency['GeneralCurrency']['name']); ?>&nbsp;</td>
		<td><?php echo h($generalCurrency['GeneralCurrency']['symbol']); ?>&nbsp;</td>
		<td><?php echo h($generalCurrency['GeneralCurrency']['iso_code']); ?>&nbsp;</td>
		<td><?php echo h($generalCurrency['GeneralCurrency']['is_default']); ?>&nbsp;</td>
		<td><?php echo h($generalCurrency['GeneralCurrency']['rate']); ?>&nbsp;</td>
		<td><?php echo h($generalCurrency['GeneralCurrency']['margin']); ?> (%)</td>
		<td class="actions">
			<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $generalCurrency['GeneralCurrency']['id']), array(
				'class' => 'btn btn-success btn-circle-sm', 'escape'=>false
			)); ?>
			<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $generalCurrency['GeneralCurrency']['id']), array(
				'class' => 'btn btn-warning btn-circle-sm', 'escape'=>false
			)); ?> 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
		</ul>
</div>
 
</div>
</div>
</div>
</div>