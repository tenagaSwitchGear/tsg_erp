<div class="finishedGoodFatRemarks index">
	<h2><?php echo __('Finished Good Fat Remarks'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('finished_good_id'); ?></th>
			<th><?php echo $this->Paginator->sort('remark'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('attachment'); ?></th>
			<th><?php echo $this->Paginator->sort('attachment_dir'); ?></th>
			<th><?php echo $this->Paginator->sort('finished_good_comment_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($finishedGoodFatRemarks as $finishedGoodFatRemark): ?>
	<tr>
		<td><?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($finishedGoodFatRemark['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $finishedGoodFatRemark['FinishedGood']['id'])); ?>
		</td>
		<td><?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['remark']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['status']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($finishedGoodFatRemark['User']['id'], array('controller' => 'users', 'action' => 'view', $finishedGoodFatRemark['User']['id'])); ?>
		</td>
		<td><?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['created']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['modified']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['attachment']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['attachment_dir']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($finishedGoodFatRemark['FinishedGoodComment']['name'], array('controller' => 'finished_good_comments', 'action' => 'view', $finishedGoodFatRemark['FinishedGoodComment']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $finishedGoodFatRemark['FinishedGoodFatRemark']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $finishedGoodFatRemark['FinishedGoodFatRemark']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $finishedGoodFatRemark['FinishedGoodFatRemark']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodFatRemark['FinishedGoodFatRemark']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Finished Good Fat Remark'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Comments'), array('controller' => 'finished_good_comments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Comment'), array('controller' => 'finished_good_comments', 'action' => 'add')); ?> </li>
	</ul>
</div>
