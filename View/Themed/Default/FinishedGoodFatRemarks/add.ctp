<div class="finishedGoodFatRemarks form">
<?php echo $this->Form->create('FinishedGoodFatRemark'); ?>
	<fieldset>
		<legend><?php echo __('Add Finished Good Fat Remark'); ?></legend>
	<?php
		echo $this->Form->input('finished_good_id');
		echo $this->Form->input('remark');
		echo $this->Form->input('status');
		echo $this->Form->input('user_id');
		echo $this->Form->input('attachment');
		echo $this->Form->input('attachment_dir');
		echo $this->Form->input('finished_good_comment_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Finished Good Fat Remarks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Comments'), array('controller' => 'finished_good_comments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Comment'), array('controller' => 'finished_good_comments', 'action' => 'add')); ?> </li>
	</ul>
</div>
