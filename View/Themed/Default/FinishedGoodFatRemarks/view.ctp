<div class="finishedGoodFatRemarks view">
<h2><?php echo __('Finished Good Fat Remark'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodFatRemark['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $finishedGoodFatRemark['FinishedGood']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodFatRemark['User']['id'], array('controller' => 'users', 'action' => 'view', $finishedGoodFatRemark['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Attachment'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['attachment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Attachment Dir'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFatRemark['FinishedGoodFatRemark']['attachment_dir']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good Comment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodFatRemark['FinishedGoodComment']['name'], array('controller' => 'finished_good_comments', 'action' => 'view', $finishedGoodFatRemark['FinishedGoodComment']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Finished Good Fat Remark'), array('action' => 'edit', $finishedGoodFatRemark['FinishedGoodFatRemark']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Finished Good Fat Remark'), array('action' => 'delete', $finishedGoodFatRemark['FinishedGoodFatRemark']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodFatRemark['FinishedGoodFatRemark']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Fat Remarks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Fat Remark'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Comments'), array('controller' => 'finished_good_comments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Comment'), array('controller' => 'finished_good_comments', 'action' => 'add')); ?> </li>
	</ul>
</div>
