<div class="row"> 
    <div class="col-xs-12">
    <?php echo $this->Html->link(__('Delivery Orders'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
    <?php echo $this->Html->link(__('Add New Delivery Order'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2>Edit Delivery Orders</h2> 
                <div class="clearfix"></div>
            </div>
        
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                
                <div class="finishedGoodDeliveries form">
                <?php echo $this->Form->create('FinishedGoodDelivery', array('class'=>'form-horizontal')); ?>
                <?php echo $this->Form->input('id'); ?>
                    <fieldset>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Sale Job</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("sale_job_id", array("class"=> "form-control", "label"=> false, "disabled"=>"disabled")); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Sale Order</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("sale_order_id", array("class"=> "form-control", "label"=> false, "disabled"=>"disabled")); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Customer</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("customer_id", array("class"=> "form-control", "label"=> false, "disabled"=>"disabled")); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Sale Quotation</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("sale_quotation_id", array("class"=> "form-control", "label"=> false, "disabled"=>"disabled")); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">DO Number</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("do_number", array("class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Remark</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("remark", array("class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Status</label>
                        <div class="col-sm-9">
                        <?php $status = array('0' => 'Draft', '1' => 'Submit to Store'); ?>
                        <?php echo $this->Form->input("status", array("class"=> "form-control", 'options'=>$status, "label"=> false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Delivery Date</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("delivery_date", array('type'=>'text', 'id'=>'dateonly', "class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    </fieldset>
                    <div class="clearfix">&nbsp;</div>
                    <h2>Finished Good Delivery Item</h2>
                    <?php echo $this->Form->create('FinishedGoodDeliveryItem' ); ?>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>Item</th>
                                <th>UOM</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $finished = $this->request->data['FinishedGoodDeliveryItem']; ?>
                            <?php $no = 1;
                            for($i=0; $i<count($finished); $i++){ ?>
                            <?php echo $this->Form->input('id.', array('type'=>'hidden', 'value'=>$finished[$i]['FinishedGoodDeliveryItem']['id'])); ?>
                            <?php echo $this->Form->input('sale_order_item_id.', array('type'=>'hidden', 'value'=>$finished[$i]['FinishedGoodDeliveryItem']['sale_order_item_id'])); ?>
                            <?php echo $this->Form->input('inventory_item_id.', array('type'=>'hidden', 'value'=>$finished[$i]['FinishedGoodDeliveryItem']['inventory_item_id'])); ?>
                            <?php echo $this->Form->input('sale_job_id.', array('type'=>'hidden', 'value'=>$finished[$i]['FinishedGoodDeliveryItem']['sale_job_id'])); ?>
                            <?php echo $this->Form->input('sale_job_child_id.', array('type'=>'hidden', 'value'=>$finished[$i]['FinishedGoodDeliveryItem']['sale_job_child_id'])); ?>
                            <tr>
                                <td class="text-center"><?php echo $no; ?></td>
                                <td><strong><?php echo $finished[$i]['InventoryItem'][0]['Item']['name']; ?><br><?php echo 'Code: '.$finished[$i]['InventoryItem'][0]['Item']['code']; ?></strong></td>
                                <td><?php echo $finished[$i]['InventoryItem'][0]['GeneralUnit']['name']; ?></td>
                                
                                <td class="col-md-2"><?php echo $this->Form->input('quantity.', array('type'=>'number', 'class' => 'form-control', 'label' => false, 'value' => $finished[$i]['FinishedGoodDeliveryItem']['quantity'], 'max' => $this->request->data['SaleOrderItem'][$i]['SaleOrderItem']['quantity'], 'min' => 0)); ?></td>
                                
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                    <?php echo $this->Form->end(__('Submit')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>