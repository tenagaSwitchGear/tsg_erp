
<div class="book page-break">
    <div class="page">
        <div id="header">
            <div class="x_title">
            	<p class="text-center">Delivery Order</p>
                <h3><strong><?php echo strtoupper(Configure::read('Site.company_name')); ?></strong></h3>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="content" class="container">
            <table style="font-size: 14px; width: 100%; height: 220px;">
                <tr style="vertical-align: top;">
                    <td style="width: 10%">Billing To</td>
                    <td style="width: 25%">: <?php echo $finished_good_deliveries[0]['Customer']['name']; ?></td>
                    <td style="width: 10%">From</td>
                    <td style="width: 25%">: TENAGA SWITCHGEAR SDN BHD</td>
                    <td style="width: 15%">DO NO</td>
                    <td style="width: 15%">: <?php echo $finished_good_deliveries[0]['DeliveryNote']['do_number']; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top !important">Address</td>
                    <td rowspan="2" style="vertical-align: top !important">: <?php echo $finished_good_deliveries[0]['Customer']['address']; ?></td>
                    <td style="vertical-align: top !important">Address</td>
                    <td>: Lot 3, Jln Teknologi 3/6</td>
                    <td>Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($finished_good_deliveries[0]['DeliveryNote']['delivery_date'])); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>: Tmn Sains Selangor 1</td>
                    <td>Your ref</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>: </td>
                    <td>&nbsp;</td>
                    <td>: Kota Damansara, 47810 Petaling Jaya</td>
                    <td></td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>Tel</td>
                    <td>: </td>
                    <td>Tel</td>
                    <td>: 03-6140 6520</td>
                    <td></td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>: </td>
                    <td>Fax</td>
                    <td>: 03-6140 6530</td>
                    <td>PO Number</td>
                    <td>: <?php echo $finished_good_deliveries[0]['SaleOrder']['SaleOrder']['customer_purchase_order_no'] ?></td>
                </tr>
                <tr>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Delivery Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($finished_good_deliveries[0]['DeliveryNote']['date_delivered'])); ?></td>
                </tr>
                <tr>
                    <td>GST No</td>
                    <td>: </td>
                    <td>GST No</td>
                    <td>: 001237581824</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            <?php if (!empty($finished_good_deliveries[0]['DeliveryNoteItem'])): ?>
            <table class="table table-bordered" style="font-size: 12px; width: 100%">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo __('#'); ?></th>
                        <th><?php echo __('Items'); ?></th>
                        <th><?php echo __('Quantity'); ?></th>
                        <th style="width: 110px"><?php echo __('Remark'); ?></th>
                    </tr>
                </thead>
                <?php 
                    $currentPage = empty($this->Paginator->params['paging']['DeliveryNote']['page']) ? 1 : $this->Paginator->params['paging']['DeliveryNote']['page']; $limit = $this->Paginator->params['paging']['DeliveryNote']['limit'];
                    $startSN = (($currentPage * $limit) + 1) - $limit;

                    foreach ($finished_good_deliveries[0]['DeliveryNoteItem'] as $fg_item): ?>
                    
                    <?php if($startSN > '14'){ echo "<div style='page-break: always'>&nbsp;</div>"; } ?>
                    <tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td><?php echo $fg_item['InventoryItem']['name'] ?>
                            <br>
                            <?php echo $fg_item['InventoryItem']['code'] ?>
                            <br>
                            <?php echo $fg_item['InventoryItem']['note'] ?>
                        </td>
                        <td><?php echo $fg_item['DeliveryNoteItem']['quantity'].' '.$fg_item['GeneralUnit']['name']; ?></td>
                        <td><?php echo $fg_item['DeliveryNoteItem']['remark']; ?></td>
                    </tr>
                <?php endforeach;  ?>
                </table>
            <?php endif; ?> 
            <div class="footer" style="position: relative; padding-top: 10px; bottom: 0px;">
                <h4 class="text-center"><?php echo __('Carrier/Transportation'); ?></h4>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td style="width: 100px">Lorry No </td>
                            <td style="width: 200px">: </td>
                            <td rowspan="7" style="padding-left: 30px;"><br><br>Received in good order and conditions<br><br><br><br><br><br>............................................</td>
                        </tr>
                        <tr>
                            <td>Driver Name </td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Driver IC </td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>Date </td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td rowspan="3" colspan="2">Signature<br><br><br>.............................................</td>
                        </tr>
                    </tbody>
                </table>        
            </div>
        </div>
    </div>
</div>