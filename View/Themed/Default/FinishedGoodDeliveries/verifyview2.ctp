<div class="row"> 
  	<div class="col-xs-12">
  	<?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivery Order'), array('action' => 'dn_index'), array('class' => 'btn btn-info btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivery Note'), array('action' => 'notes_index'), array('class' => 'btn btn-info btn-sm')); ?>
   <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
   <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
   <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
    <div class="x_panel tile">
      	<div class="x_title">
        	<h2><?php echo __('Finished Good Delivery'); ?></h2>
        <div class="clearfix"></div>
	</div>
	<div class="x_content"> 
		<?php echo $this->Session->flash(); ?>
		
		<?php echo $this->Form->create('DeliveryNote', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('value'=>$deliverynote[0]['DeliveryNote']['id'])); ?>
        <div class="finishedGoodDeliveries view-data">
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($deliverynote[0]['DeliveryNote']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Job'); ?></dt>
				<dd><?php if(!empty($deliverynote[0]['SaleJob'])){ ?>
					<?php echo $this->Html->link($deliverynote[0]['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $deliverynote[0]['SaleJob']['id'])); ?>
					<?php } ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Order'); ?></dt>
				<dd>
					<?php echo $this->Html->link($deliverynote[0]['SaleOrder']['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $deliverynote[0]['SaleOrder']['SaleOrder']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer'); ?></dt>
				<dd>
					<?php echo $this->Html->link($deliverynote[0]['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $deliverynote[0]['Customer']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Quotation'); ?></dt>
				<dd>
					<?php echo $this->Html->link($deliverynote[0]['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $deliverynote[0]['SaleQuotation']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo h($deliverynote[0]['DeliveryNote']['created']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php echo h($deliverynote[0]['DeliveryNote']['modified']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Remark'); ?></dt>
				<dd>
					<?php echo h($deliverynote[0]['DeliveryNote']['remark']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Delivery Date'); ?></dt>
				<dd>
					<?php echo h($deliverynote[0]['DeliveryNote']['delivery_date']); ?>
					&nbsp;
				</dd>
			</dl>
		</div>

	<h3><?php echo __('Finished Good Delivery Items'); ?></h3>
	<?php if (!empty($deliverynote[0]['DeliveryNoteItem'])): ?>

	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Items'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('UOM'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Remark'); ?></th>
	</tr>
	<?php foreach ($deliverynote[0]['DeliveryNoteItem'] as $finishedGoodDeliveryItem): ?>
		<tr>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['id']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['InventoryItem']['name'].'<br>'.$finishedGoodDeliveryItem['InventoryItem']['code']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['quantity']; ?></td>
			
			<td><?php echo $finishedGoodDeliveryItem['GeneralUnit']['name']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['type']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['remark']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
	<legend>Approval</legend>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Remark'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('approval_remark', array('type'=>'textarea', 'class' => 'form-control', 'required' => false, 'label'=>false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Status'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('status', array('empty'=>array(0=>'Select Status'), 'options' => array('6'=>'Approved', '7'=>'Rejected'), 'class' => 'form-control', 'required' => false, 'label'=>false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"></label>
		<div class="col-sm-9">
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-success pull-right')); ?>	
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
<?php endif; ?>
</div>
</div>
</div>
</div>