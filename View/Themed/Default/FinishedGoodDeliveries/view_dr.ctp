<div class="row"> 
  	<div class="col-xs-12">
  	<?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
  	<?php echo $this->Html->link(__('Delivery Note'), array('action' => 'dn_index'), array('class' => 'btn btn-primary btn-sm')); ?>
  	<?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
  	<?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
   <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
  	<?php //echo $this->Html->link(__('Add New Delivery Order'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
    <div class="x_panel tile">
      	<div class="x_title">
        	<h2><?php echo __('Finished Good Delivery'); ?></h2>
        <div class="clearfix"></div>
	</div>
	<div class="x_content"> 
		<?php echo $this->Session->flash(); ?>
		<pre><?php print_r($finishedGoodDelivery) ?></pre>
		<?php echo $this->Form->create('FinishedGoodDelivery', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('value'=>$finishedGoodDelivery[0]['FinishedGoodDelivery']['id'])); ?>
        <div class="finishedGoodDeliveries view-data">
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Job'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGoodDelivery[0]['SaleJob']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Order'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleOrder']['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $finishedGoodDelivery[0]['SaleOrder']['SaleOrder']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer PO No'); ?></dt>
				<dd>
					<?php echo $finishedGoodDelivery[0]['SaleOrder']['customer_purchase_order_no']; ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedGoodDelivery[0]['Customer']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Quotation'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $finishedGoodDelivery[0]['SaleQuotation']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['created']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['modified']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Remark'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['remark']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Delivery Date'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['delivery_date']); ?>
					&nbsp;
				</dd>
			</dl>
		</div>

	<h3><?php echo __('Finished Good Delivery Items'); ?></h3>
	<?php if (!empty($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'])): ?>

	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Items'); ?></th>
		<th class="col-md-1"><?php echo __('Quantity'); ?></th>
		<th><?php echo __('UOM'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Type'); ?></th>
	</tr>
	<?php //$finishedGoodDeliveryItem = $finishedGoodDelivery[0]['FinishedGoodDeliveryItem']; ?>
	<?php //for($i=0; $i<count($finishedGoodDeliveryItem); $i++){ ?>
	<?php foreach ($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] as $finishedGoodDeliveryItem): ?>
		<?php echo $this->Form->input('fgid.', array('type'=>'hidden', 'value'=>$finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id'])); ?>
		<tr>
			<td><?php echo $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['InventoryItem'][0]['InventoryItem']['name'].'<br>'.$finishedGoodDeliveryItem['InventoryItem'][0]['InventoryItem']['code']; ?></td>
			<?php for($i=0; $i<count($finishedGoodDelivery[0]['SaleOrderItem']); $i++){
					if($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['sale_order_item_id'] == $finishedGoodDelivery[0]['SaleOrderItem'][$i]['SaleOrderItem']['id']){ $maximum = $finishedGoodDelivery[0]['SaleOrderItem'][$i]['SaleOrderItem']['quantity']; }
				} ?>
			<td><?php echo $this->Form->input('quantity', array('value'=>$finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['quantity'], 'class'=>'form-control', 'label'=>false, 'type'=>'number', 'max'=>$maximum)); ?></td>
			
			<td><?php echo $finishedGoodDeliveryItem['InventoryItem'][0]['GeneralUnit']['name']; ?></td>
			<td>
			<?php if(empty($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['remark'])){ ?>
			<?php echo $this->Form->input('remark.', array('type'=>'textarea', 'class'=>'form-control', 'label'=>false)); ?>
			<?php }else{ ?>
			<?php echo $this->Form->input('remark.', array('type'=>'hidden', 'class'=>'form-control', 'label'=>false, 'value'=>$finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['remark'], 'required'=>false)); ?>
			<?php echo $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['remark']; ?>
			<?php } ?>
			</td>
			<td><?php echo $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['type']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
	<legend>Carrier/Transportation</legend>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Lorry No'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('lorry_no', array('class' => 'form-control', 'required' => false, 'label'=>false, 'value'=>$finishedGoodDelivery[0]['FinishedGoodDelivery']['lorry_no'])); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Driver Name'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('driver_name', array('class' => 'form-control', 'required' => false, 'label'=>false, 'value'=>$finishedGoodDelivery[0]['FinishedGoodDelivery']['driver_name'])); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Driver Ic'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('driver_ic', array('class' => 'form-control', 'required' => false, 'label'=>false, 'value'=>$finishedGoodDelivery[0]['FinishedGoodDelivery']['driver_ic'])); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Date'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('date', array('id'=>'dateonly', 'class' => 'form-control', 'required' => false, 'label'=>false, 'value'=>$finishedGoodDelivery[0]['FinishedGoodDelivery']['date_delivered'])); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Status'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('status', array('empty'=>array(0=>'Select Status'), 'options' => array('2'=>'Draft', '3'=>'Submit for approval'), 'class' => 'form-control', 'required' => false, 'label'=>false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"></label>
		<div class="col-sm-9">
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-success pull-right')); ?>	
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
<?php endif; ?>
</div>
</div>
</div>
</div>