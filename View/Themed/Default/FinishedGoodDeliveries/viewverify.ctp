<div class="row"> 
  	<div class="col-xs-12">
  	<?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivery Order'), array('action' => 'dn_index'), array('class' => 'btn btn-info btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivery Note'), array('action' => 'notes_index'), array('class' => 'btn btn-info btn-sm')); ?>
   <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
   <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
   <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
    <div class="x_panel tile">
      	<div class="x_title">
        	<h2><?php echo __('Finished Good Delivery'); ?></h2>
        <div class="clearfix"></div>
	</div>
	<div class="x_content"> 
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->Form->create('FinishedGoodDelivery', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('value'=>$finishedGoodDelivery[0]['FinishedGoodDelivery']['id'])); ?>
        <div class="finishedGoodDeliveries view-data">
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Job'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGoodDelivery[0]['SaleJob']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Order'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleOrder']['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $finishedGoodDelivery[0]['SaleOrder']['SaleOrder']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedGoodDelivery[0]['Customer']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Quotation'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $finishedGoodDelivery[0]['SaleQuotation']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['created']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['modified']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Remark'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['remark']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Delivery Date'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['delivery_date']); ?>
					&nbsp;
				</dd>
			</dl>
		</div>

	<h3><?php echo __('Related Finished Good Delivery Items'); ?></h3>
	<?php if (!empty($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'])): ?>

	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Items'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('UOM'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Type'); ?></th>
	</tr>
	<?php foreach ($finishedGoodDelivery[0]['FinishedGoodDeliveryItem'] as $finishedGoodDeliveryItem): ?>
		<tr>
			<td><?php echo $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['InventoryItem'][0]['InventoryItem']['name'].'<br>'.$finishedGoodDeliveryItem['InventoryItem'][0]['InventoryItem']['code']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['quantity']; ?></td>
			
			<td><?php echo $finishedGoodDeliveryItem['InventoryItem'][0]['GeneralUnit']['name']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['remark']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['type']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
	<legend>Carrier/Transportation</legend>
	<div class="finishedGoodDeliveries view-data">
	<dl>
		<dt><?php echo __('Lorry No'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['lorry_no']); ?>
		&nbsp;
		</dd>
		<dt><?php echo __('Driver Name'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['driver_name']); ?>
		&nbsp;
		</dd>
		<dt><?php echo __('Driver Ic'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['driver_ic']); ?>
		&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDelivery[0]['FinishedGoodDelivery']['date_delivered']); ?>
		&nbsp;
		</dd>
	</dl>
	</div>
	<div class="form-group">
		<label class="col-sm-3"><?php echo __('Status'); ?></label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('status', array('empty'=>array(0=>'Select Status'), 'options' => array('4'=>'Approved', '5'=>'Rejected'), 'class' => 'form-control', 'required' => false, 'label'=>false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"></label>
		<div class="col-sm-9">
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-success pull-right')); ?>	
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
<?php endif; ?>
</div>
</div>
</div>
</div>