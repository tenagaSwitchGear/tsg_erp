<div class="row"> 
  	<div class="col-xs-12">
  		<?php //echo $this->Html->link(__('Add New Delivery Note'), array('action' => 'add_dn'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
   		<?php echo $this->Html->link(__('Delivery Order'), array('action' => 'dn_index'), array('class' => 'btn btn-info btn-sm')); ?>
   		<?php echo $this->Html->link(__('Delivery Note'), array('action' => 'notes_index'), array('class' => 'btn btn-info btn-sm')); ?>
		<?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
   		<?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
		<?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
		<?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
   		<?php echo $this->Html->link(__('Stock Rejected'), array('action' => 'stockrejected'), array('class' => 'btn btn-danger btn-sm')); ?>
    		<div class="x_panel tile">
     			 <div class="x_title">
				<h2><?php echo __('Finished Good Deliveries'); ?></h2> 
				<div class="clearfix"></div>
      			</div>
      			<div class="x_content"> 
				<?php echo $this->Session->flash(); ?>
				<div class="finishedGoodDeliveries index">
					<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
						<thead>
							<tr> 
								<th><?php echo $this->Paginator->sort('customer_id'); ?></th> 
								<th><?php echo $this->Paginator->sort('sale_job_child_id', 'Job'); ?></th>
								<th><?php echo $this->Paginator->sort('sale_order_id'); ?></th>
								<th><?php echo $this->Paginator->sort('DO Number'); ?></th> 
								<th><?php echo $this->Paginator->sort('delivery_date'); ?></th>
								<th class="actions"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$currentPage = empty($this->Paginator->params['paging']['FinishedGoodDelivery']['page']) ? 1 : $this->Paginator->params['paging']['FinishedGoodDelivery']['page']; $limit = $this->Paginator->params['paging']['FinishedGoodDelivery']['limit'];
							$startSN = (($currentPage * $limit) + 1) - $limit;
							foreach ($finishedGoodDeliveries as $finishedgooddelivery): ?>
							<tr> 
								<td>
									<?php echo $this->Html->link($finishedgooddelivery['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedgooddelivery['Customer']['id'])); ?>
								</td>  
								<td>
									<?php echo $this->Html->link($finishedgooddelivery['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedgooddelivery['SaleJob']['id'])); ?>
								</td>
								<td>
									<?php echo $this->Html->link($finishedgooddelivery['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $finishedgooddelivery['SaleOrder']['id'])); ?>
								</td>
								<td><?php foreach ($finishedgooddelivery['DN'] as $dn) { ?>
									<?php if(empty($dn)){ echo 'DO not found!'; }else{ echo '- '.$dn['DeliveryNote']['do_number'].'<br>'; } ?>	
								<?php } ?></td>
								<td><?php echo h($finishedgooddelivery['FinishedGoodDelivery']['delivery_date']); ?>&nbsp;</td>
								<td class="actions">
									<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $finishedgooddelivery['FinishedGoodDelivery']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>

									<?php //$items = array();
									$q = 0;
									foreach($finishedgooddelivery['FinishedGoodDeliveryItem'] as $dnitem){
										
										$q += $dnitem['quantity'];
									}

									//echo $q;

									$q_dn = 0;
									foreach($finishedgooddelivery['DN'] as $dnitem_2){
										foreach ($dnitem_2['DeliveryNoteItem'] as $dn_item) {
											$q_dn = $q_dn + $dn_item['quantity'];
										}
									}

									//echo $q_dn;
										
										$check_quantity = $q - $q_dn;
									
										//}?>
										<?php //print_r($items); ?>
										<?php //$sum = array_sum($items); ?>
										<?php //echo $sum; ?>
										<?php if($check_quantity == '0'){ $ada = 'x'; }else{ $ada = 'ada'; } ?>
										<?php //echo $ada; ?>
										<?php if($ada == 'ada'){ ?>
									<?php echo $this->Html->link('<i class="fa fa-plus"></i>', array('action' => 'create_do', $finishedgooddelivery['FinishedGoodDelivery']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>

									<?php } ?>
									<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $finishedgooddelivery['FinishedGoodDelivery']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
									<?php //echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $finishedgooddelivery['FinishedGoodDelivery']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($finishedgooddelivery['FinishedGoodDelivery']['id']).'"', $finishedgooddelivery['FinishedGoodDelivery']['id'])); ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<p>
					<?php
						echo $this->Paginator->counter(array(
							'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
					?>
					</p>
					<ul class="pagination">
						<?php
					  	echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
					  	echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
					  	echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>