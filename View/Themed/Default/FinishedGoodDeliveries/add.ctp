<div class="row"> 
  <div class="col-xs-12">
  <?php echo $this->Html->link(__('Request for Delivery'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Request for Delivery</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

 
<?php echo $this->Form->create('FinishedGoodDelivery', array('class' => 'form-horizontal')); ?>
 	<div class="form-group">
        <label class="col-sm-3">Reference No</label>
        <div class="col-sm-9">
        <?php echo $this->Form->input('reference_no', array('placeholder'=>'Unique Reference No', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?> 
        </div>
    </div>
	<div class="form-group">
		<label class="col-sm-3">Sale Order No *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('findSaleOrder', array('id' => 'findSaleOrder', 'class' => 'form-control', 'label' => false)); ?> 
		<?php echo $this->Form->input('sale_order_id', array('id' => 'sale_order_id', 'type' => 'hidden')); ?> 
        <?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden')); ?> 
		<?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden')); ?> 
		<?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden')); ?> 
		<?php echo $this->Form->input('sale_quotation_id', array('id' => 'sale_quotation_id', 'type' => 'hidden')); ?> 
		<?php echo $this->Form->input('user_id', array('type' => 'hidden')); ?>  
		</div>
	</div>
	<div class="form-group">
        <label class="col-sm-3">Address</label>
        <div class="col-sm-9">
        <?php echo $this->Form->input('address', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?> 
        </div>
    </div>
	<div class="form-group">
		<label class="col-sm-3">Note (Optional)</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Status *</label>
		<div class="col-sm-9">
		<?php $status = array('0' => 'Draft', '1' => 'Submit for Approval'); ?>
		<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Delivery Date *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('delivery_date', array('id' => 'dateonly', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?> 
		</div>
	</div>
    <div id="item"></div>
	<div class="form-group">
		<label class="col-sm-3"></label>
		<div class="col-sm-9">  
    	<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'pull-right btn btn-success')); ?> 
    </div>	  
	<?php $this->Form->end(); ?>
    

</div>
</div>
</div>
</div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 

$(document).ready(function() { 
	$("#findSaleOrder").autocomplete({
        source: function (request, response){
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'sale_orders/ajaxfindsaleorder',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findSaleOrder').val(),                                                    
                success: function (data) {
                    console.log(data);
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name,
                            sale_job_id: item.sale_job_id,
                            sale_job_child_id: item.sale_job_child_id,
                            sale_quotation_id: item.sale_quotation_id,
                            customer_id: item.customer_id,
                            sale_tender_id: item.sale_tender_id,
                            customer_name: item.customer_name 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $("#sale_order_id").val(ui.item.id);
            $("#sale_job_id").val(ui.item.sale_job_id);
            $("#sale_job_child_id").val(ui.item.sale_job_child_id);
            $("#sale_quotation_id").val(ui.item.sale_quotation_id);
            $("#customer_id").val(ui.item.customer_id);
            $("#sale_tender_id").val(ui.item.sale_tender_id);

            get_item(ui.item.id);
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.customer_name + "</small><br>" +  "</div>" ).appendTo( ul );
    };

    
});

function get_item($id){
    var id = $id;
    
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'sale_orders/ajaxfindsaleorderitem',           
            contentType: "application/json",
            dataType: "json",
            data: "term=" + id,                                                    
            success: function (response) {
                console.log(response);
                //$("#item").html(JSON.stringify(response));
                //var data = JSON.stringify(response);
                html = '<div class="clearfix">&nbsp;</div>';
                html += '<h2>Finished Good Delivery Item</h2>';
                html += '<table class="table table-bordered">';
                html += '<tr><th class="text-center">#</th><th>Item</th><th>UOM</th><th class="col-md-1">Quantity</th></tr>';
                for (i = 0; i < response.length; i++) {
                    html += '<tr>';
                    html += '<input type="hidden" name="sale_order_item_id[]" value="'+response[i].SaleOrderItem.id+'">';
                    html += '<input type="hidden" name="inventory_item_id[]" value="'+response[i].InventoryItem.id+'">';
                    html += '<input type="hidden" name="sale_job_id[]" value="'+response[i].SaleOrderItem.sale_job_id+'">';
                    html += '<input type="hidden" name="sale_job_child_id[]" value="'+response[i].SaleOrderItem.sale_job_child_id+'">';
                    html += '<td class="text-center">'+(i+1)+'</td>';
                    html += '<td><strong>'+response[i].InventoryItem.name+'<br>'+response[i].InventoryItem.code+'</strong></td>';
                    html += '<td>'+response[i].GeneralUnit.name+'</td>';
                    html += '<td><input type="number" name="quantity_deliver[]" class="form-control" min="0" max="'+response[i].SaleOrderItem.quantity+'" value="'+response[i].SaleOrderItem.quantity+'"></td>';
                    //html += '<td><input type="text" name="remark[]" class="form-control" placeholder="Remark"></td>';
                    html += '</tr>';
                    //$("#item").html(JSON.stringify(response));
                }
                html += '</table>'
                $("#item").html(html);
            }
        });
   
}
</script>
<?php $this->end(); ?>			