<style type="text/css">
.line {    
    font-size: 14px;
    border-bottom: 2px dotted #000;
    text-decoration: none;
   
}
.x_content a{
    color: #1E90FF !important;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  	<?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivery Order'), array('action' => 'dn_index'), array('class' => 'btn btn-info btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivery Note'), array('action' => 'notes_index'), array('class' => 'btn btn-info btn-sm')); ?>
   <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
   <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
   <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
   <?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
    <div class="x_panel tile">
      	<div class="x_title">
        	<h2><?php echo __('Delivery Order'); ?></h2>
        <div class="clearfix"></div>
	</div>
	<div class="x_content"> 
		<?php echo $this->Session->flash(); ?>
		
		<?php echo $this->Form->create('DeliveryNote', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('value'=>$finishedGoodDelivery[0]['DeliveryNote']['id'])); ?>
        <div class="finishedGoodDeliveries view-data">
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Job'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGoodDelivery[0]['SaleJob']['id']), array('class'=>'line')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Order'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleOrder']['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $finishedGoodDelivery[0]['SaleOrder']['SaleOrder']['id']), array('class'=>'line')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer PO No'); ?></dt>
				<dd>
					<?php echo $finishedGoodDelivery[0]['SaleOrder']['SaleOrder']['customer_purchase_order_no']; ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedGoodDelivery[0]['Customer']['id']), array('class'=>'line')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Quotation'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery[0]['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $finishedGoodDelivery[0]['SaleQuotation']['id']), array('class'=>'line')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['created']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['modified']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Remark'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['remark']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Delivery Date'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['delivery_date']); ?>
					&nbsp;
				</dd>
				<?php if($finishedGoodDelivery[0]['DeliveryNote']['status'] == 6){ $status = 'Approved but no delivered yet'; }else if($finishedGoodDelivery[0]['DeliveryNote']['status'] == 7){ $status = 'Rejceted'; }else{ $status = 'Delivered'; } ?>
				<dt><?php echo __('Status'); ?></dt>
				<dd>
					<?php echo $status; ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Notes'); ?></dt>
				<dd>
					<?php echo $finishedGoodDelivery[0]['DeliveryNote']['notes']; ?>
					&nbsp;
				</dd>
			</dl>
		</div>

	<h3><?php echo __('Delivery Order Items'); ?></h3>
	<?php if (!empty($finishedGoodDelivery[0]['DeliveryNoteItem'])): ?>

	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Items'); ?></th>
		<th class="col-md-1"><?php echo __('Quantity'); ?></th>
		<th><?php echo __('UOM'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Type'); ?></th>
	</tr>
	<?php //$finishedGoodDeliveryItem = $finishedGoodDelivery[0]['FinishedGoodDeliveryItem']; ?>
	<?php //for($i=0; $i<count($finishedGoodDeliveryItem); $i++){ ?>
	<?php foreach ($finishedGoodDelivery[0]['DeliveryNoteItem'] as $finishedGoodDeliveryItem): ?>
		<?php echo $this->Form->input('fgid.', array('type'=>'hidden', 'value'=>$finishedGoodDeliveryItem['DeliveryNoteItem']['id'])); ?>
		<tr>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['id']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['InventoryItem']['name'].'<br>'.$finishedGoodDeliveryItem['InventoryItem']['code']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['quantity']; ?></td>	
			<td><?php echo $finishedGoodDeliveryItem['GeneralUnit']['name']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['remark']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['DeliveryNoteItem']['type']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
	<legend>Carrier/Transportation</legend>
	<div class="finishedGoodDeliveries view-data">
	<dl>
		<dt><?php echo __('Lorry No'); ?></dt>
		<dd>
			:<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['lorry_no']); ?>
		&nbsp;
		</dd>
		<dt><?php echo __('Driver Name'); ?></dt>
		<dd>
			:<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['driver_name']); ?>
		&nbsp;
		</dd>
		<dt><?php echo __('Driver Ic'); ?></dt>
		<dd>
			:<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['driver_ic']); ?>
		&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			:<?php echo h($finishedGoodDelivery[0]['DeliveryNote']['date_delivered']); ?>
		&nbsp;
		</dd>
	</dl>
	</div>
	<?php echo $this->Form->end(); ?>
<?php endif; ?>
</div>
</div>
</div>
</div>