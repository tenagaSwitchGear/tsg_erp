<?php echo $this->Html->css('/js/libs/jstree/themes/apple/style.css'); ?>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php //echo $this->Html->link(__('Add New Delivery Note'), array('action' => 'add_dn'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
   		<?php echo $this->Html->link(__('Delivery Order'), array('action' => 'dn_index'), array('class' => 'btn btn-info btn-sm')); ?>
   		<?php echo $this->Html->link(__('Delivery Note'), array('action' => 'notes_index'), array('class' => 'btn btn-info btn-sm')); ?>
		<?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
   		<?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
		<?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
		<?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
   		<?php echo $this->Html->link(__('Stock Rejected'), array('action' => 'stockrejected'), array('class' => 'btn btn-danger btn-sm')); ?>
  		<div class="x_panel tile">
	      		<div class="x_title">
	        		<h2><?php echo __('Stock Rejecteds'); ?></h2>
	        		<div class="clearfix"></div>
      			</div>
      			<div class="x_content"> 
        			<?php echo $this->Session->flash(); ?>
        			<!-- content start-->
            
        			<div class="table-responsive">
					<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
						<thead>
							<tr>
								<th class="text-center col-md-1"><?php echo $this->Paginator->sort('#'); ?></th>
								<th class="col-md-3"><?php echo $this->Paginator->sort('Item'); ?></th>
								<th class="col-md-2"><?php echo $this->Paginator->sort('quantity'); ?></th>
		                        			<th class="col-md-2"><?php echo ('Supplier'); ?></th>
								<th colspan="3"><?php echo $this->Paginator->sort('status'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php 
							$currentPage = empty($this->Paginator->params['paging']['InventoryStockRejected']['page']) ? 1 : $this->Paginator->params['paging']['InventoryStockRejected']['page']; $limit = $this->Paginator->params['paging']['InventoryStockRejected']['limit'];
							$startSN = (($currentPage * $limit) + 1) - $limit;

							foreach ($inventoryStockRejecteds as $inventoryStockRejected): 
						?>
							<tr>
								<td class="text-center"><?php echo $startSN++; ?>&nbsp;</td>
								<td><strong><?php echo ($inventoryStockRejected['InventoryItem']['name']); ?><br><?php echo ($inventoryStockRejected['InventoryItem']['code']); ?></strong>&nbsp;</td>
								<td><?php echo ($inventoryStockRejected['InventoryStockRejected']['quantity']); ?>&nbsp;</td>
	                        				<td><?php echo ($inventoryStockRejected['InventoryDeliveryOrder']['InventorySupplier']['name']); ?>&nbsp;</td>
								<td class="col-md-2" style="border-right: 0px"><a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="status_<?php echo $inventoryStockRejected['InventoryStockRejected']['id']; ?>" class="editable" data-type="select" data-pk="<?php echo $inventoryStockRejected['InventoryStockRejected']['id']; ?>" data-name="#status_<?php echo $inventoryStockRejected['InventoryStockRejected']['id']; ?>" data-emptytext="Select Status" data-url="<?php echo BASE_URL.'finished_good_deliveries/setStatus'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $inventoryStockRejected['InventoryStockRejected']['status']; ?>" data-source="<?php echo BASE_URL.'finished_good_deliveries/getStatus'; ?>"></a></td>
								<?php if($inventoryStockRejected['InventoryStockRejected']['status']=='1'){ ?>
									<td style="border-left: 0px; border-right: 0px"><?php echo $this->Html->link('<i class="fa fa-plus"></i> New Delivery Notes', array('action' => 'new_notes', $inventoryStockRejected['InventoryStockRejected']['id']), array('class' => 'btn btn-success pull-right btn-sm', 'escape'=>false)); ?>	</td>
								<?php } ?>
								<td style="border-left: 0px"><?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view_stockreject', $inventoryStockRejected['InventoryStockRejected']['inventory_qc_item_id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
						echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
					?>
					</div>
				</div>
        			<!-- content end -->
      			</div>
		</div>
    	</div>
</div>
<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript">    
    $('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';

           location.reload();
        }
    });
</script>
<?php $this->end(); ?>