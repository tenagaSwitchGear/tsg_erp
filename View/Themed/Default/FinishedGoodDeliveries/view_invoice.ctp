<div class="book page-break">
    <div class="page">
        <div id="header">
            <div class="x_title">
                <p class="text-center">Invoice</p>
                <h3><strong><?php echo strtoupper(Configure::read('Site.company_name')); ?></strong></h3>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="content" class="container">
            <table style="font-size: 14px; width: 100%; height: 220px;">
                <tr style="vertical-align: top;">
                    <td style="width: 10%">Billing To</td>
                    <td style="width: 25%">: <?php echo $finished_good_deliveries[0]['Customer']['name']; ?></td>
                    <td style="width: 10%">From</td>
                    <td style="width: 25%">: TENAGA SWITCHGEAR SDN BHD</td>
                    <td style="width: 15%">DO NO</td>
                    <td style="width: 15%">: <?php echo $finished_good_deliveries[0]['DeliveryNote']['do_number']; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top !important">Address</td>
                    <td rowspan="2" style="vertical-align: top !important">: <?php echo $finished_good_deliveries[0]['Customer']['address']; ?></td>
                    <td style="vertical-align: top !important">Address</td>
                    <td style="vertical-align: top !important">: Lot 3, Jln Teknologi 3/6</td>
                    <td style="vertical-align: top !important">Date</td>
                    <td style="vertical-align: top !important">: <?php echo date('d/m/Y', strtotime($finished_good_deliveries[0]['DeliveryNote']['delivery_date'])); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top !important">: Tmn Sains Selangor 1</td>
                    <td style="vertical-align: top !important">Your ref</td>
                    <td style="vertical-align: top !important">:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>: </td>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top !important">: Kota Damansara, 47810 Petaling Jaya</td>
                    <td></td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>Tel</td>
                    <td>: </td>
                    <td>Tel</td>
                    <td style="vertical-align: top !important">: 03-6140 6520</td>
                    <td></td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>: </td>
                    <td>Fax</td>
                    <td style="vertical-align: top !important">: 03-6140 6530</td>
                    <td>PO Number</td>
                    <td style="vertical-align: top !important">: <?php echo $finished_good_deliveries[0]['SaleOrder']['SaleOrder']['customer_purchase_order_no'] ?></td>
                </tr>
                <tr>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Delivery Date</td>
                    <td style="vertical-align: top !important">: <?php echo date('d/m/Y', strtotime($finished_good_deliveries[0]['DeliveryNote']['date_delivered'])); ?></td>
                </tr>
                <tr>
                    <td>GST No</td>
                    <td>: </td>
                    <td>GST No</td>
                    <td style="vertical-align: top !important">: 001237581824</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            <?php if (!empty($finished_good_deliveries[0]['DeliveryNoteItem'])): ?>
            <table class="table table-bordered" style="font-size: 12px; width: 100%">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo __('#'); ?></th>
                        <th><?php echo __('Items'); ?></th>
                        <th><?php echo __('Quantity'); ?></th>
                        <th style="width: 110px"><?php echo __('Unit Price'); ?></th>
                        <th style="width: 110px"><?php echo __('Total (RM)'); ?></th>
                    </tr>
                </thead>
                <?php 
                    $t_amount = 0;
                    $tax = 0;
                    $currentPage = empty($this->Paginator->params['paging']['DeliveryNote']['page']) ? 1 : $this->Paginator->params['paging']['DeliveryNote']['page']; $limit = $this->Paginator->params['paging']['DeliveryNote']['limit'];
                    $startSN = (($currentPage * $limit) + 1) - $limit;

                    foreach ($finished_good_deliveries[0]['DeliveryNoteItem'] as $fg_item): ?>
                    
                    <?php if($startSN > '14'){ echo "<div style='page-break: always'>&nbsp;</div>"; } ?>
                    <tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td><?php echo $fg_item['InventoryItem']['name'] ?>
                            <br>
                            <?php echo $fg_item['InventoryItem']['code'] ?>
                            <br>
                            <?php echo $fg_item['InventoryItem']['note'] ?>
                        </td>
                        <td><?php echo $fg_item['DeliveryNoteItem']['quantity'].' '.$fg_item['GeneralUnit']['name']; ?></td>
                        <td class="text-right"><?php echo _n2($fg_item['SaleOrderItem']['unit_price']); ?></td>
                        <td class="text-right"><?php $tp = $fg_item['SaleOrderItem']['unit_price']*$fg_item['DeliveryNoteItem']['quantity']; echo _n2($tp); $t_amount = $t_amount + $tp; $tp = $t_amount; $tax = $tax + (($fg_item['SaleOrderItem']['unit_price']*$fg_item['DeliveryNoteItem']['quantity'])*($fg_item['SaleOrderItem']['tax']/100)); ?></td>
                    </tr>
                <?php endforeach;  ?>
                    <tr>
                        <td colspan="4" class="text-right">Total Amount Excl. GST</td>
                        <td class="text-right"><?php echo _n2($tp); ?></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right">6% GST</td>
                        <td class="text-right"><?php echo _n2($tax); ?></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right">Total Amount Incl. GST</td>
                        <td class="text-right"><?php echo _n2($tp + $tax); ?></td>
                    </tr>
                </table>
            <?php endif; ?> 
            <div>
            <br><br><br><br><br><br><br><br>
                <p>Bank Details</p>
                <p><?php echo strtoupper(Configure::read('Site.bank')); ?></p>
                <p><?php echo strtoupper(Configure::read('Site.caw_bank')); ?></p>
                <p>A/C No : <?php echo strtoupper(Configure::read('Site.account_no')); ?></p>
            </div>
            <div class="footer" style="position: relative; padding: 10px; bottom: 0px; border: 1px solid black; width: 350px">
                <table>
                    <tbody>
                        <tr>
                            <td>Authorised By : </td>
                        </tr>
                        <tr>
                            <td><br><br>............................</td>
                        </tr>
                        <tr>
                            <td><br>Name : Sarina Othman</td>
                        </tr>
                        <tr>
                            <td><br>Designation : Head of Finance</td>
                        </tr>
                    </tbody>
                </table>        
            </div>
        </div>
    </div>
</div>