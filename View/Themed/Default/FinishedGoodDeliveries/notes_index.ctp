<div class="row"> 
  <div class="col-xs-12">
 <?php //echo $this->Html->link(__('Add New Delivery Note'), array('action' => 'add_dn'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Delivery Order'), array('action' => 'dn_index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('Delivery Note'), array('action' => 'notes_index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
        <?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Stock Rejected'), array('action' => 'stockrejected'), array('class' => 'btn btn-danger btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo __('Delivery Notes'); ?></h2> 
        <?php echo $this->Html->link('<i class="fa fa-plus"></i> New Delivery Notes', array('action' => 'new_notes'), array('class' => 'btn btn-success pull-right btn-sm', 'escape'=>false)); ?>
        
        <div class="clearfix"></div>

      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
		<div class="finishedGoodDeliveries index">
			<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
				<thead>
					<tr> 
						<th><?php echo $this->Paginator->sort('customer_id', 'Supplier'); ?></th> 
						<th><?php echo $this->Paginator->sort('reference_no'); ?></th>
						<th><?php echo $this->Paginator->sort('DO No'); ?></th> 
						<th><?php echo $this->Paginator->sort('status'); ?></th>
						<th><?php echo $this->Paginator->sort('delivery_date'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$currentPage = empty($this->Paginator->params['paging']['DeliveryNote']['page']) ? 1 : $this->Paginator->params['paging']['DeliveryNote']['page']; $limit = $this->Paginator->params['paging']['DeliveryNote']['limit'];
					$startSN = (($currentPage * $limit) + 1) - $limit;
					foreach ($delivery_note as $dn): ?>
					<tr> 
						<td>
							<?php echo $this->Html->link($dn['Supplier']['InventorySupplier']['name'], array('controller' => 'customers', 'action' => 'view', $dn['Supplier']['InventorySupplier']['id'])); ?>
						</td>  
						<td>
							<?php echo $this->Html->link($dn['DeliveryNote']['reference_no'], array('controller' => 'users', 'action' => 'view', $dn['DeliveryNote']['reference_no'])); ?>
						</td>
						<?php if($dn['DeliveryNote']['status'] == '5'){ $status = 'Waiting for Store Approval'; }
						else if($dn['DeliveryNote']['status'] == '6'){ $status = 'Approved'; }
						else if($dn['DeliveryNote']['status'] == '7'){ $status = 'Rejected'; }
						else{ $status = 'Delivered'; } ?>
						<td><?php echo $dn['DeliveryNote']['do_number']; ?>&nbsp;</td> 
						<td><?php echo $status; ?>&nbsp;</td> 
						<td><?php echo h(date('Y-m-d', strtotime($dn['DeliveryNote']['date_delivered']))); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view_notes', $dn['DeliveryNote']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $finishedgooddelivery['FinishedGoodDelivery']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php //echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $finishedgooddelivery['FinishedGoodDelivery']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($finishedgooddelivery['FinishedGoodDelivery']['id']).'"', $finishedgooddelivery['FinishedGoodDelivery']['id'])); ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<p>
			<?php
				echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
			?></p>
			<ul class="pagination">
				<?php
			  	echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  	echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  	echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
				?>
			</ul>
		</div>

	</div>
</div>
</div>
</div>