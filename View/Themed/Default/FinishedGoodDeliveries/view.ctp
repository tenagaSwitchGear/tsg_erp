<style type="text/css">
.line {    
    font-size: 14px;
    border-bottom: 2px dotted #000;
    text-decoration: none;
   
}
.x_content a{
    color: #1E90FF !important;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  	<?php echo $this->Html->link(__('Request for Delivery'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
  	<?php //echo $this->Html->link(__('Add New Delivery Order'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
    <div class="x_panel tile">
      	<div class="x_title">
        	<h2><?php echo __('Delivery Order Request'); ?></h2>
        <div class="clearfix"></div>
	</div>
	<div class="x_content"> 
		<?php echo $this->Session->flash(); ?>
		
        <div class="finishedGoodDeliveries view-data">
			<dl> 
				<dt><?php echo __('Sale Job'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGoodDelivery['SaleJob']['id']), array('class'=>'line')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Order'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $finishedGoodDelivery['SaleOrder']['id']), array('class'=>'line')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer'); ?></dt>
				<dd>
					<?php echo $this->Html->link($finishedGoodDelivery['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedGoodDelivery['Customer']['id']), array('class'=>'line')); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Sale Quotation'); ?></dt>
				<dd>
					<?php echo $finishedGoodDelivery['SaleQuotation']['name']; ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery['FinishedGoodDelivery']['created']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery['FinishedGoodDelivery']['modified']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Remark'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery['FinishedGoodDelivery']['remark']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Delivery Date'); ?></dt>
				<dd>
					<?php echo h($finishedGoodDelivery['FinishedGoodDelivery']['delivery_date']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Status'); ?></dt>
				<?php if($finishedGoodDelivery['FinishedGoodDelivery']['status'] == '0'){ $status = 'Draft'; }
						else if($finishedGoodDelivery['FinishedGoodDelivery']['status'] == '1'){ $status = 'Submit to Store'; }
						else if($finishedGoodDelivery['FinishedGoodDelivery']['status'] == '2'){ $status = 'Approved'; }
						else if($finishedGoodDelivery['FinishedGoodDelivery']['status'] == '3'){ $status = 'Waiting for Store Approval'; }
						else if($finishedGoodDelivery['FinishedGoodDelivery']['status'] == '4'){ $status = 'Delivery Order'; }
						else if($finishedGoodDelivery['FinishedGoodDelivery']['status'] == '5'){ $status = 'Rejected'; }
						else{ $status = 'Delivered'; } ?>
				<dd>
					<?php echo $status; ?>
					&nbsp;
				</dd>
			</dl>
		</div>

	<h4><?php echo __('Items'); ?></h4>
	<?php if (!empty($finishedGoodDelivery['FinishedGoodDeliveryItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr> 
		<th><?php echo __('Items'); ?></th>
 
		<th><?php echo __('Quantity'); ?></th> 
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Type'); ?></th>
	</tr>
	<?php foreach ($finishedGoodDelivery['FinishedGoodDeliveryItem'] as $finishedGoodDeliveryItem): ?>
 
		<tr> 
			<td><?php echo $finishedGoodDeliveryItem['InventoryItem']['code']; ?><br/>
			<small><?php echo $finishedGoodDeliveryItem['InventoryItem']['name']; ?></small></td>
			<td><?php echo $finishedGoodDeliveryItem['quantity']; ?></td> 
 
			<td><?php echo $finishedGoodDeliveryItem['created']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['remark']; ?></td>
			<td><?php echo $finishedGoodDeliveryItem['type']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
</div>
</div>
</div>