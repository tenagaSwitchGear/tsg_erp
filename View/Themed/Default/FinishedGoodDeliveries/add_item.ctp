<div class="row"> 
  <div class="col-xs-12">
  <?php echo $this->Html->link(__('Delivery Orders'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo __('Finished Good Delivery Item'); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
     
		<div class="finishedGoodDeliveryItems form">
			<dl>
				<dt class="col-sm-2"><?php echo __('Status'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo ($fgood['FinishedGood']['status']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Remark'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo ($fgood['FinishedGood']['remark']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Delivery Date'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo ($fgood['FinishedGood']['delivery_date']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Sale Order'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo ($fgood['SaleOrder']['name']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Sale Job'); ?></dt>
				<dd class="col-sm-9">
					<?php if(empty($fgood['SaleJobs'])){ echo ':'; }else{?>
					: <?php echo ($fgood['SaleJobs']['name']); ?>
					<?php } ?>
					&nbsp;
				</dd>
			</dl>
			<?php echo $this->Form->create('FinishedGoodDeliveryItem' ); ?>
			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th>Item</th>
						<th>UOM</th>
						<th>Quantity</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1;
					foreach ($fgood['SaleOrderItem'] as $item) { ?>
					<?php echo $this->Form->input('finished_good_delivery_id.', array('type'=>'hidden', 'value'=>$fgood['FinishedGood']['id'])); ?>
					<?php echo $this->Form->input('sale_order_item_id.', array('type'=>'hidden', 'value'=>$item['SaleOrderItem']['id'])); ?>
					<?php echo $this->Form->input('inventory_item_id.', array('type'=>'hidden', 'value'=>$item['InventoryItem']['id'])); ?>
					<?php echo $this->Form->input('sale_job_id.', array('type'=>'hidden', 'value'=>$item['SaleOrderItem']['sale_job_id'])); ?>
					<?php echo $this->Form->input('sale_job_child_id.', array('type'=>'hidden', 'value'=>$item['SaleOrderItem']['sale_job_child_id'])); ?>
					<tr>
						<td class="text-center"><?php echo $no; ?></td>
						<td><strong><?php echo $item['InventoryItem']['name']; ?><br><?php echo 'Code: '.$item['InventoryItem']['code']; ?></strong></td>
						<td><?php echo $item['GeneralUnit']['name']; ?></td>
						<td class="col-md-2"><?php echo $this->Form->input('quantity.', array('type'=>'number', 'class' => 'form-control', 'label' => false, 'value' => $item['SaleOrderItem']['quantity'], 'max' => $item['SaleOrderItem']['quantity'], 'min' => 0)); ?></td>
					</tr>
					<?php $no++; } ?>
				</tbody>
			</table>
			<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'pull-right btn btn-success btn-sm')); ?>
		
		</div>
	</div>
</div>
</div>
</div>
