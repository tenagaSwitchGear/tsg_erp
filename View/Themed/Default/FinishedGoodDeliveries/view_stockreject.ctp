<style type="text/css">
.line {    
    border-bottom: 2px dotted #000;
    text-decoration: none;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Delivery Request'), array('action' => 'store'), array('class' => 'btn btn-primary btn-sm')); ?>
   		<?php echo $this->Html->link(__('Delivery Order'), array('action' => 'dn_index'), array('class' => 'btn btn-info btn-sm')); ?>
   		<?php echo $this->Html->link(__('Delivery Note'), array('action' => 'notes_index'), array('class' => 'btn btn-info btn-sm')); ?>
		<?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'approval'), array('class' => 'btn btn-default btn-sm')); ?>
   		<?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class' => 'btn btn-success btn-sm')); ?>
		<?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-danger btn-sm')); ?>
		<?php echo $this->Html->link(__('Delivered'), array('action' => 'delivered'), array('class' => 'btn btn-default btn-sm')); ?>
   		<?php echo $this->Html->link(__('Stock Rejected'), array('action' => 'stockrejected'), array('class' => 'btn btn-danger btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        			<h2>Stock Rejected</h2> 
        			<div class="clearfix"></div>
      		</div>
      		<div class="x_content">
      			<div class="container table-responsive">
				<dl>
					<dt class="col-sm-2"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['id']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('PO No'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['po_no']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Item'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['name']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Code'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryItem']['code']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['quantity']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo date('d/m/Y', strtotime($inventoryStockRejected[0]['InventoryQcItem']['created'])); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity Passed'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['quantity_pass']; ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quantity Rejected'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo $inventoryStockRejected[0]['InventoryQcItem']['quantity_rejected']; ?>
						&nbsp;
					</dd>
				</dl>
				<h3><?php echo __('Remark'); ?></h3>
				<?php if (!empty($inventoryStockRejected[0]['InventoryQcRemark'])){ ?>

				<table cellpadding = "0" cellspacing = "0" class="table">
				<tr>
					<th><?php echo __('Remark'); ?></th>
					<th><?php echo __('Category'); ?></th>
					<th><?php echo __('Attachment'); ?></th>
				</tr>
				<?php foreach ($inventoryStockRejected[0]['InventoryQcRemark'] as $remark) { ?>
				<tr>
					<td><?php echo $remark['InventoryQcRemark']['remark']; ?></td>
					<td><?php echo $remark['Comment']['name']; ?></td>
					<td><?php echo $this->Html->link($remark['InventoryQcRemark']['attachment'], '/files/inventory_qc_remark/attachment/' . $remark['InventoryQcRemark']['attachment_dir'] . '/' . $remark['InventoryQcRemark']['attachment'], array('target' => '_blank', 'class' => 'line')); ?></td>
				</tr>
				<?php } ?>

				</table>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
</div>
