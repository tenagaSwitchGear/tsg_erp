<div class="inventoryDeliveryOrderItems view">
<h2><?php echo __('Inventory Delivery Order Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Delivery Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['InventoryDeliveryOrder']['id'], array('controller' => 'inventory_delivery_orders', 'action' => 'view', $inventoryDeliveryOrderItem['InventoryDeliveryOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryDeliveryOrderItem['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryDeliveryOrderItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax'); ?></dt>
		<dd>
			<?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['tax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total'); ?></dt>
		<dd>
			<?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Tax'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['GeneralTax']['name'], array('controller' => 'general_taxes', 'action' => 'view', $inventoryDeliveryOrderItem['GeneralTax']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Delivery Order Item'), array('action' => 'edit', $inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Delivery Order Item'), array('action' => 'delete', $inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Order Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Orders'), array('controller' => 'inventory_delivery_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order'), array('controller' => 'inventory_delivery_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Taxes'), array('controller' => 'general_taxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Tax'), array('controller' => 'general_taxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Inventory Stocks'); ?></h3>
	<?php if (!empty($inventoryDeliveryOrderItem['InventoryStock'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Inventory Delivery Order Item Id'); ?></th>
		<th><?php echo __('General Unit Converter Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('Qc Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($inventoryDeliveryOrderItem['InventoryStock'] as $inventoryStock): ?>
		<tr>
			<td><?php echo $inventoryStock['id']; ?></td>
			<td><?php echo $inventoryStock['general_unit_id']; ?></td>
			<td><?php echo $inventoryStock['inventory_item_id']; ?></td>
			<td><?php echo $inventoryStock['inventory_delivery_order_item_id']; ?></td>
			<td><?php echo $inventoryStock['general_unit_converter_id']; ?></td>
			<td><?php echo $inventoryStock['quantity']; ?></td>
			<td><?php echo $inventoryStock['qc_status']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_stocks', 'action' => 'view', $inventoryStock['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_stocks', 'action' => 'edit', $inventoryStock['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_stocks', 'action' => 'delete', $inventoryStock['id']), array(), __('Are you sure you want to delete # %s?', $inventoryStock['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
