<div class="row"> 
  	<div class="col-xs-12">
  		<?php //echo $this->Html->link(__('New Purchase Requisitions'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Inventory Item'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('QC Inspection'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<pre><?php print_r($inventoryDeliveryOrderItems); ?></pre>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
							<th><?php echo $this->Paginator->sort('Items'); ?></th>
							<th><?php echo $this->Paginator->sort('DO Number'); ?></th>
							<th><?php echo $this->Paginator->sort('Quantity'); ?></th>
							<th><?php echo $this->Paginator->sort('Quantity Passed'); ?></th>
							<th><?php echo $this->Paginator->sort('Quantity Rejected'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryDeliveryOrderItem']['page']) ? 1 : $this->Paginator->params['paging']['InventoryDeliveryOrderItem']['page']; $limit = $this->Paginator->params['paging']['InventoryDeliveryOrderItem']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryDeliveryOrderItems as $inventoryDeliveryOrderItems): 
					?>
					<tr>
						<td class="text-center"><?php echo $startSN++; ?></td>
						<td><?php echo $inventoryDeliveryOrderItems['InventoryDeliveryOrder']['title']; ?>&nbsp;</td>
						<td><?php echo $inventoryDeliveryOrderItems['InventoryDeliveryOrder']['description']; ?>&nbsp;</td>
						<td><?php echo h(date('d/m/Y', strtotime($inventoryDeliveryOrderItems['InventoryDeliveryOrder']['created']))); ?>&nbsp;</td>
						<td><?php echo h(date('d/m/Y', strtotime($inventoryDeliveryOrderItems['InventoryDeliveryOrder']['dateline']))); ?>&nbsp;</td>
						<td>
						<?php if($inventoryDeliveryOrders['InventoryDeliveryOrder']['completed_status']=='0'){
								echo "Pending";
							}else{
								echo "Completed";
								} ?>
						<?php ?>
						</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryDeliveryOrders['InventoryDeliveryOrder']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryDeliveryOrders['InventoryDeliveryOrder']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryDeliveryOrders['InventoryDeliveryOrder']['title']).'"', $inventoryDeliveryOrders['InventoryDeliveryOrder']['id'])); ?>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


<div class="inventoryDeliveryOrderItems index">
	<h2><?php echo __('Inventory Delivery Order Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_delivery_order_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('tax'); ?></th>
			<th><?php echo $this->Paginator->sort('total'); ?></th>
			<th><?php echo $this->Paginator->sort('general_tax_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryDeliveryOrderItems as $inventoryDeliveryOrderItem): ?>
	<tr>
		<td><?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['InventoryDeliveryOrder']['id'], array('controller' => 'inventory_delivery_orders', 'action' => 'view', $inventoryDeliveryOrderItem['InventoryDeliveryOrder']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryDeliveryOrderItem['InventoryItem']['id'])); ?>
		</td>
		<td><?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryDeliveryOrderItem['GeneralUnit']['id'])); ?>
		</td>
		<td><?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['price']); ?>&nbsp;</td>
		<td><?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['tax']); ?>&nbsp;</td>
		<td><?php echo h($inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['total']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryDeliveryOrderItem['GeneralTax']['name'], array('controller' => 'general_taxes', 'action' => 'view', $inventoryDeliveryOrderItem['GeneralTax']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryDeliveryOrderItem['InventoryDeliveryOrderItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Delivery Orders'), array('controller' => 'inventory_delivery_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Delivery Order'), array('controller' => 'inventory_delivery_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Taxes'), array('controller' => 'general_taxes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Tax'), array('controller' => 'general_taxes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
	</ul>
</div>
