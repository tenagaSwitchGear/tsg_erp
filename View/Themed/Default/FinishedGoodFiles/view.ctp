<div class="finishedGoodFiles view">
<h2><?php echo __('Finished Good File'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFile['FinishedGoodFile']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodFile['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $finishedGoodFile['FinishedGood']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dir'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFile['FinishedGoodFile']['dir']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFile['FinishedGoodFile']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($finishedGoodFile['FinishedGoodFile']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Finished Good File'), array('action' => 'edit', $finishedGoodFile['FinishedGoodFile']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Finished Good File'), array('action' => 'delete', $finishedGoodFile['FinishedGoodFile']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodFile['FinishedGoodFile']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Files'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good File'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
	</ul>
</div>
