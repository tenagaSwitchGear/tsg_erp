<div class="finishedGoodFiles index">
	<h2><?php echo __('Finished Good Files'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('finished_good_id'); ?></th>
			<th><?php echo $this->Paginator->sort('dir'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($finishedGoodFiles as $finishedGoodFile): ?>
	<tr>
		<td><?php echo h($finishedGoodFile['FinishedGoodFile']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($finishedGoodFile['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $finishedGoodFile['FinishedGood']['id'])); ?>
		</td>
		<td><?php echo h($finishedGoodFile['FinishedGoodFile']['dir']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodFile['FinishedGoodFile']['name']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodFile['FinishedGoodFile']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $finishedGoodFile['FinishedGoodFile']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $finishedGoodFile['FinishedGoodFile']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $finishedGoodFile['FinishedGoodFile']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodFile['FinishedGoodFile']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Finished Good File'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
	</ul>
</div>
