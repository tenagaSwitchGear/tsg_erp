<div class="finishedGoodFiles form">
<?php echo $this->Form->create('FinishedGoodFile'); ?>
	<fieldset>
		<legend><?php echo __('Edit Finished Good File'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('finished_good_id');
		echo $this->Form->input('dir');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FinishedGoodFile.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('FinishedGoodFile.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Good Files'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
	</ul>
</div>
