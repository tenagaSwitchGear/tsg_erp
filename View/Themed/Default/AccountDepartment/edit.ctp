<div class="row"> 
  	<div class="col-xs-12">
  		<?php //echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Customers Contact Person'), array('controller' => 'customer_contact_people', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Customers Files'), array('controller' => 'customer_files', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Account Department</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		
        		<div class="accountDepartment form">
        		<input type="hidden" id="year" value="<?php echo $this->request->data['AccountDepartment']['year']; ?>">
				<?php echo $this->Form->create('AccountDepartment', array('class' => 'form-horizontal')); ?>
				<?php echo $this->Form->input("id", array("type"=>"hidden", "class"=> "form-control", "label"=> false)); ?>
					<fieldset>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Department</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("group_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Year</label>
							<div class="col-sm-9">
							<select name="year" id="yearpicker" class="form-control"></select>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Total Budget Given</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("total", array("type" => "text", "class"=> "form-control", "placeholder" => "Total", "label"=> false)); ?>
							</div> 
						</div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
						<?php
							echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm'));						
							echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-info btn-sm'));
						?>
					</div>
				
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
var c_year = $('#year').val();

var d = new Date();
var n = d.getFullYear();
var m = n+9;
var x = '';
for(i=n; i<m; i++){
	if(i == c_year){
		x += '<option value='+i+' selected>';
		x += i;
		x += '</option>';
	}else{
		x += '<option value='+i+'>';
		x += i;
		x += '</option>';
	}
}
var x = x;

$('#yearpicker').html(x);
//alert(m);

</script>
<?php $this->end(); ?>