<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Budgets'), array('controller' => 'account_department_budgets', 'action' => 'add', $accountDepartment['AccountDepartment']['id']), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Department Budget'), array('controller' => 'account_departments', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Department Budget</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
        	<div class="container table-responsive">
				<dl>
					<dt class="col-sm-2"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						: #<?php echo ($accountDepartment['AccountDepartment']['id']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Department Name'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($accountDepartment['Group']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Total'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($accountDepartment['AccountDepartment']['total']); ?>
						&nbsp;
					</dd>
                    <dt class="col-sm-2"><?php echo __('Allocated'); ?></dt>
                    <dd class="col-sm-9">
                        : <?php echo ($accountDepartment['AccountDepartment']['allocated']); ?>
                        &nbsp;
                    </dd>
					<dt class="col-sm-2"><?php echo __('Used'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($accountDepartment['AccountDepartment']['used']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="related table-responsive">
				<h4><?php echo __('Related Budgets'); ?></h4>
				<?php if (!empty($accountDepartment['AccountDepartmentBudgets'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
					<thead>
						<tr>
							<th class="text-center"><?php echo __('Id'); ?></th>
							<th><?php echo __('Name'); ?></th>
                            <th><?php echo __('Type'); ?></th>
                            <th><?php echo __('Job'); ?></th>
							<th><?php echo __('Budget'); ?></th>
							<th><?php echo __('Used'); ?></th>

							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<?php foreach ($accountDepartment['AccountDepartmentBudgets'] as $accountDepartmentBudgets): ?>
                        
    					<tbody>
    						<tr>
    							<td class="text-center"><?php echo $accountDepartmentBudgets['Parent']['id']; ?></td>
    							<td><?php echo $accountDepartmentBudgets['Parent']['name']; ?></td>
                                <td><?php echo type($accountDepartmentBudgets['Parent']['type']); ?></td>
                                <td><?php echo $accountDepartmentBudgets['SaleJobChild']['name']; ?> (<?php echo $accountDepartmentBudgets['SaleJobChild']['station_name']; ?>)</td>
    							<td><?php echo $accountDepartmentBudgets['Parent']['total']; ?></td>
    							<td><?php echo $accountDepartmentBudgets['Parent']['used']; ?></td>
    							<td class="actions">
    								<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'account_department_budgets', 'action' => 'edit_child', $accountDepartment['AccountDepartment']['id'], $accountDepartmentBudgets['Parent']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
    								<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'account_department_budgets', 'action' => 'delete_2', $accountDepartmentBudgets['Parent']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.ucfirst($accountDepartmentBudgets['Parent']['name'].'"?'), $accountDepartmentBudgets['Parent']['id'])); ?>
    							</td>
                            </tr>
                                <tr>
                                    <td colspan="7"><strong>Sub Budgets : <?php echo $accountDepartmentBudgets['Parent']['name']; ?> </strong></td>
                                </tr>
                                <tr>
                                    <td colspan="7">
                                        <table class="table">
                                            <thead><tr>
                                               
                                                <th>Name</th>
                                                <th>Budget</th>
                                                <th>Used</th>
                                                <th>Actions</th>
                                            </tr></thead>
                                            <tbody>
                                                <?php foreach ($accountDepartmentBudgets['Child'] as $child) { ?>
                                                    <tr>
                                                        
                                                        <td><?php echo $child['AccountDepartmentBudgets']['name']; ?></td>
                                                        <td><?php echo $child['AccountDepartmentBudgets']['total']; ?></td>
                                                        <td><?php echo $child['AccountDepartmentBudgets']['used']; ?></td>
                                                        <td><?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'account_department_budgets', 'action' => 'edit', $accountDepartment['AccountDepartment']['id'], $child['AccountDepartmentBudgets']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
                                                            <?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'account_department_budgets', 'action' => 'delete', $accountDepartmentBudgets['Parent']['id'], $child['AccountDepartmentBudgets']['id'], $child['AccountDepartmentBudgets']['total']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.ucfirst($child['AccountDepartmentBudgets']['name'].'"?'), $child['AccountDepartmentBudgets']['id'])); ?></td>

                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
    						</tr>
    					</tbody>
                        
					<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Budgets
				<?php endif; ?>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php
 
function type($type) {
    if($type == 1) {
        return 'CAPEX';
    }
    if($type == 2) {
        return 'OPEX';
    } 
    if($type == 3) {
        return 'Serving Client';
    }
}

?>