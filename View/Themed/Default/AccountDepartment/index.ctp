<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Department Budget'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>

    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Departments Budget</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
           <?php echo $this->Form->create('AccountDepartment', array('class' => 'form-horizontal')); ?>
        	<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th><?php echo $this->Paginator->sort('Group.name'); ?></th>
							<th><?php echo ('Year'); ?></th>
							<th><?php echo $this->Paginator->sort('total'); ?></th>
							<th><?php echo $this->Paginator->sort('used'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$currentPage = empty($this->Paginator->params['paging']['AccountDepartment']['page']) ? 1 : $this->Paginator->params['paging']['AccountDepartment']['page']; $limit = $this->Paginator->params['paging']['AccountDepartment']['limit'];
							$startSN = (($currentPage * $limit) + 1) - $limit;
							foreach ($accountDepartment as $accountDepartment): ?>
						<tr>
							<td class="text-center"><?php echo $startSN++; ?></td>
							<td><?php echo ($accountDepartment['Group']['name']); ?>&nbsp;</td>
							<td><?php echo ($accountDepartment['AccountDepartment']['year']); ?>&nbsp;</td>
							<td><?php echo ($accountDepartment['AccountDepartment']['total']); ?>&nbsp;</td>
							<td><?php echo ($accountDepartment['AccountDepartment']['used']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $accountDepartment['AccountDepartment']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $accountDepartment['AccountDepartment']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $accountDepartment['AccountDepartment']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "#'.h($accountDepartment['AccountDepartment']['id']).'"', $accountDepartment['AccountDepartment']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p><?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?></p>
				<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
						echo $this->Paginator->numbers(array('separator' => ''), array('class' => 'btn btn-default btn-sm'));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
					?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>