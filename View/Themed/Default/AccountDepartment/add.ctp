<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Department Budget</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		
        		<div class="accountDepartment form">
				<?php echo $this->Form->create('AccountDepartment', array('class' => 'form-horizontal')); ?>
					<fieldset>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Department</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("group_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Year</label>
							<div class="col-sm-9">
							<select name="year" id="yearpicker" class="form-control"></select>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Total Budget Given</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("total", array("class"=> "form-control", "placeholder" => "Total", "label"=> false)); ?>
							</div> 
						</div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
					<?php
						echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm'));
						echo $this->Form->button('Reset', array('type'=>'reset', 'class' => 'btn btn-danger btn-sm','div' => false));
						echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
					?>
					</div>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
var d = new Date();
var n = d.getFullYear();
var m = n+9;
var x = '';
for(i=n; i<m; i++){
	x += '<option value='+i+'>';
	x += i;
	x += '</option>';
}
var x = x;

$('#yearpicker').html(x);
//alert(m);

</script>
<?php $this->end(); ?>