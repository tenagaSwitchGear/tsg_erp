<?php echo $this->Html->link(__('PO Draft'), array('action' => 'create'), array('class'=>'btn btn-warning btn-sm')); ?>  
<?php echo $this->Html->link(__('Purchased'), array('action' => 'create', 1), array('class'=>'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link(__('Partially Received'), array('action' => 'create', 2), array('class'=>'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Closed'), array('action' => 'create', 3), array('class'=>'btn btn-success btn-sm')); ?>

<?php echo $this->Html->link(__('Edit PO'), array('action' => 'edit', $inventoryPurchaseOrders['InventoryPurchaseOrder']['id']), array('class'=>'btn btn-warning btn-sm')); ?>
<style type="text/css">
    .l_right{
        float: right;
        font-size: 16px
    }
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Purchase Order'); ?> (<?php echo $inventoryPurchaseOrders['InventoryPurchaseRequisition']['pr_no']; ?>)</h2>
                <label class="l_right text-danger">Purchase Status : <?php if($inventoryPurchaseOrders['InventoryPurchaseOrder']['purchase_status']=='0'){ echo "Pending"; }else{ echo "Purchased"; } ?></label>
        	<div class="clearfix"></div>
            <div class="x_title"><?php if($inventoryPurchaseOrders['InventoryPurchaseOrder']['status']!= 1){?>
               <?php echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'view_pdf', $inventoryPurchaseOrders['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success', 'escape'=>false, 'target' => '_blank'));
               //echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'view_pdf', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-info', 'escape'=>false)); ?>
                <?php } ?></div>
      	</div>
        
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>Order Date</th>
                    <th>ETA</th>
                    <th>Term of Payment</th>
                    <th>Term of Delivery</th>
                    <th>PO Number</th>
                </tr>
                </thead>
                <tr>
                    <td><?php echo date('d/m/Y', strtotime($inventoryPurchaseOrders['InventoryPurchaseOrder']['created'])); ?></td>
                    <td><?php echo date('d/m/Y', strtotime($inventoryPurchaseOrders['InventoryPurchaseOrder']['dateline'])); ?></td>
                    <td><?php echo $inventoryPurchaseOrders['TermOfPayment']['name']; ?></td>
                    <td><?php echo $inventoryPurchaseOrders['TermOfDelivery']['name']; ?></td>
                    <td><?php echo $inventoryPurchaseOrders['InventoryPurchaseOrder']['po_no']; ?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p><?php echo $this->Html->link(__('Edit Supplier (Then refresh this page)'), array('controller' => 'inventory_suppliers', 'action' => 'edit', $inventoryPurchaseOrders['InventorySupplier']['id']), array('target'=>'_blank')); ?></p>
                        <p><strong>VENDOR:</strong></p>
                        <p><strong><?php echo $inventoryPurchaseOrders['InventorySupplier']['name']; ?><br></strong>
                        <div class="p-no-space">
                        <p class="wrapper"><?php echo $inventoryPurchaseOrders['InventorySupplier']['address']; ?><br></p>
                        <p><?php echo $inventoryPurchaseOrders['InventorySupplier']['postcode']; ?> <?php echo $inventoryPurchaseOrders['InventorySupplier']['city']; ?><br>
                        <?php 
                        echo $supplier['State']['name'] == 'Others' ? $supplier['InventorySupplier']['state'] : $supplier['State']['name'];
                        
                        ?><br/>
                        <?php 
                        echo $supplier['Country']['name'];
                        
                        ?>
                        </p>
                        </div>
                        <p>&nbsp;</p>
                        <p><strong>CONTACT:</strong></p>
                        <p>Attn: <?php echo $inventoryPurchaseOrders['InventorySupplier']['pic']; ?><br/> 
                        Tel : <?php echo $inventoryPurchaseOrders['InventorySupplier']['phone_office']; ?><br/>
                        H/P : <?php echo $inventoryPurchaseOrders['InventorySupplier']['phone_mobile']; ?><br/> 
                        Email: <?php echo $inventoryPurchaseOrders['InventorySupplier']['email']; ?><br/> 
                        </p>
                    </td>
                    <td colspan="2">
                        <p><?php echo $this->Html->link(__('Edit Address (Then refresh this page)'), array('controller' => 'inventory_locations', 'action' => 'edit', $inventoryPurchaseOrders['InventoryLocation']['id']), array('target'=>'_blank')); ?></p>
                        <p><strong>DELIVERY POINT:</strong></p>
                        <p><strong><?php echo $inventoryPurchaseOrders['InventoryLocation']['name'].'<br>'; ?></strong>
                        <p class="wrapper"><?php echo $inventoryPurchaseOrders['InventoryLocation']['address']; ?></p>
                        <?php
                        /* foreach($address as $add){ 
                            echo $add.'<br>';
                        } */
                        ?>
                        <?php //echo $inventoryPurchaseOrders['InventoryLocation']['address'].'<br>'; ?>
                        </p>
                        <p><strong>CONTACT :</strong></p>
                        <p>Contact Person : <?php echo $inventoryPurchaseOrders['User']['firstname'].'<br>'; ?>
                            Contact Number : <?php echo $inventoryPurchaseOrders['User']['mobile_number']; ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><strong>Remark</strong></td>
                    <td colspan="3"><?php echo $inventoryPurchaseOrders['Remark']['name']; ?></td>
                </tr>
                <tr>
                    <td colspan="2"><strong>Warranty/Guarantee</strong></td>
                    <td colspan="3"><?php echo $inventoryPurchaseOrders['Warranty']['name']; ?></td>
                </tr>
                <tr>
                    <td colspan="2"><strong>Total Discount</strong></td>
                    <td colspan="3"><?php echo $inventoryPurchaseOrders['InventoryPurchaseOrder']['bulk_discount']; ?></td>
                </tr> 
            </table>
			
			<div class="clearfix">&nbsp;</div>
			
			<div class="related">
				<h4><?php echo __('Purchase Requisition Items'); ?></h4>
				<?php if (!empty($inventoryPurchaseOrders['InventoryPurchaseOrderItem'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
				<tr>
					<th class="text-center"><?php echo __('#'); ?></th>
					<th><?php echo __('Items'); ?></th>
					<th><?php echo __('Quantity'); ?></th> 
					<th><?php echo __('Unit Price'); ?></th>
					<th><?php echo __('Discount'); ?></th>
					<th><?php echo __('Tax'); ?></th>
					<th><?php echo __('Amount'); ?></th>
                    <th class="align-right"><?php echo __('RM'); ?></th> 
				</tr>

                <?php echo $this->Form->create('InventoryPurchaseOrder', array('class' => 'form-horizontal')); ?>
                <?php echo $this->Form->input('id', array('value' => $inventoryPurchaseOrders['InventoryPurchaseOrder']['id'])); ?>

                <?php

                // Check bulk discount
                if($inventoryPurchaseOrders['InventoryPurchaseOrder']['bulk_discount'] > 0) {
                    $bulk = $inventoryPurchaseOrders['InventoryPurchaseOrder']['bulk_discount'];
                } else {
                    $bulk = 0;
                }
                // Header for sum item in pr
                $header_total = 0;
                //var_dump($items[0]["InventorySupplier"]["gst"]);
                foreach ($items as $inventoryPurchaseOrderItem) {  
                    $header_total += $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['price_per_unit'] * $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['quantity']; 
                }

                ?>

				<?php 
                $no = 1; 
                $total_rm_no_tax = 0;
                $tax_total = 0;
                $total_discount = 0;
                $calc_gst = 0;
				foreach ($items as $inventoryPurchaseOrderItem):
                    $item_price = $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['price_per_unit'];
                    $sub_price = $item_price * $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['quantity'];
                 ?>
					<tr> 
						<td class="text-center"><?php echo $no++; ?></td>
						<td>
							<?php echo $inventoryPurchaseOrderItem['InventoryItem']['code']; ?><br>
							<small><?php echo $inventoryPurchaseOrderItem['InventoryItem']['name']; ?></small><br/>
                            <small><?php echo $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['remark']; ?></small>
							<?php echo $this->Html->link(('<i class="fa fa-external-link" aria-hidden="true"></i>'), array('controller'=>'inventory_purchase_requisition_items', 'action' => 'index', $inventoryPurchaseOrderItem['InventoryItem']['id']), array('escape'=>false, 'target'=>'_blank')); ?>
						</td>
						<td><?php echo $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['quantity']; ?><br><?php 
								echo $inventoryPurchaseOrderItem['GeneralUnit']['name'];
							?></td>
 


						<td><?php echo _n2($item_price); ?></td>
						<td>
						<?php
						if($inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['discount_type'] == '0') { 
							echo number_format($inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['discount'], 2);
							$discount = $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['discount'];
						}else{
							echo number_format($inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['discount'], 2).' %'; 

                            $discount =  ($sub_price / 100) * $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['discount'];
						}
                        $after_discount = $sub_price - $discount;
                        $total_discount += $discount;
						?>
						</td>
						<td>
						<?php
						if($inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['tax'] == '0') {
                            $tax = 0;
							$tax_val = 0;
						} else {
                            $tax = $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['tax'];
							$tax_val = ($after_discount / 100 ) * $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['tax'];
						}
                        $tax_total += $tax_val;
                        echo $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['tax'];
						?>
						</td>
						<td><?php echo $inventoryPurchaseOrderItem['GeneralCurrency']['iso_code'] .' '. _n2($after_discount); ?></td>
                        <td class="align-right">
                        <?php  
                            $rm = $after_discount * $inventoryPurchaseOrderItem['GeneralCurrency']['rate'];
                            $total_rm_no_tax += $rm; 

                            echo _n2($rm);  
                        ?>

                        <?php  

                        // Calculating for bulk & gst
                        //if($bulk > 0) {
                            $a = ($bulk / $header_total) * $rm;
                            $b = $rm - $a;

                            if($tax > 0) {
                                $calc_sum_tax = ($b * $tax) / 100;
                                $calc_gst += $calc_sum_tax;
                            } else {
                                $calc_sum_tax = 0;
                                $calc_gst += $calc_sum_tax;
                            }
                            
                        //} else {
                        //  $calc_sum_tax = 0;
                        //}
                        
                        ?>

                        <?php echo $this->Form->input("total_rm_no_tax.", array("type" => "hidden", "value" => $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['total_rm'])); ?> 
                        <?php echo $this->Form->input('planning_item_jobs.', array('value' => $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['planning_item_jobs'], 'type' => 'hidden')); ?>
                        </td>
					</tr>
				<?php endforeach; ?>

                <?php 
                if($inventoryPurchaseOrders['InventoryPurchaseOrder']['bulk_discount'] != 0 && $tax_total > 0) {
                    $bulk_discount_tax = ($inventoryPurchaseOrders['InventoryPurchaseOrder']['bulk_discount'] / 100) * 6;
                } else {
                    $bulk_discount_tax = 0;
                }  
                $dis_plus_bulk = $inventoryPurchaseOrders['InventoryPurchaseOrder']['bulk_discount'] + $total_discount;
                $tax_bottom = $tax_total - $bulk_discount_tax;
                ?>
                <tr>
                <td colspan="7" class="align-right">Subtotal</td>
                <td class="align-right"><?php echo _n2($total_rm_no_tax); ?></td>
                </tr>
                <tr>
                <td colspan="7" class="align-right">Discount (Item + Bulk)</td>
                <td class="align-right"><?php echo _n2($dis_plus_bulk); ?></td>
                </tr>
                <tr>
                <td colspan="7" class="align-right">Total RM</td>
                <td class="align-right"><?php echo _n2($total_rm_no_tax - $dis_plus_bulk); ?></td>
                </tr>
                <tr>
                <td colspan="7" class="align-right">GST</td>
                <td class="align-right"><?php echo _n2($calc_gst); ?></td>
                </tr>
                <tr>
                <td colspan="7" class="align-right"><b>Grandtotal</b></td>
                <?php
                $grandtotal = $total_rm_no_tax - $dis_plus_bulk + $tax_bottom;

                ?>
                <td class="align-right"><b><?php echo _n2($grandtotal); ?></b></td>
                </tr>
				</table>
				<?php else: ?>
					&bull; No Purchase Order Item
				<?php endif; ?>
			</div>
            
            <?php if($inventoryPurchaseOrders['InventoryPurchaseOrder']['purchase_status'] == 0) { ?>
            <div> 
                <p>Once click Purchase, it will deduct Budget.</p> 
            </div>
			<?php 
            $total_deduct_budget = $total_rm_no_tax - $dis_plus_bulk;
            echo $this->Form->input("total_rm_no_tax", array("type" => "hidden", "value" => $total_deduct_budget)); ?> 
            <?php 
            if($calc_gst > 0) { 
                echo $this->Form->input("tax", array("type" => "hidden", "value" => $calc_gst)); 
            }
            ?> 
			<div class="form-group">
				<label class="col-sm-3"></label>
				<div class="col-sm-9">
					<?php echo $this->Form->submit('Purchase', array('class' => 'btn btn-success pull-right')); ?>	
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
            <?php } ?>
        	<!-- content end -->

            <?php if(!empty($grn_items)) { ?> 
                <h4>GRN</h4>
                <table class="table table-bordered">
                <tr>
                     <th>GRN No</th>
                     <th>Item</th>
                     <th>Qty Delivered</th>
                     <th>Qty Accepted</th>
                     <th>Qty Rejected</th>
                     <th>Qty Remaining</th>
                </tr> 
                <?php foreach ($grn_items as $grn) { ?>
                     <tr>
                     <td> 
<?php echo $this->Html->link($grn['InventoryDeliveryOrder']['grn_no'], array('controller' => 'inventory_delivery_orders', 'action' => 'view_grn', $grn['InventoryDeliveryOrder']['id']), array('target'=>'_blank')); ?>
                     </td>
                     <td><?php echo $grn['InventoryItem']['code']; ?><br/><small><?php echo $grn['InventoryItem']['name']; ?></small></td>
                     <td><?php echo $grn['InventoryDeliveryOrderItem']['quantity_delivered']; ?></td>
                     <td><?php echo $grn['InventoryDeliveryOrderItem']['quantity_accepted']; ?></td>
                     <td><?php echo $grn['InventoryDeliveryOrderItem']['quantity_rejected']; ?></td>
                     <td><?php echo $grn['InventoryDeliveryOrderItem']['quantity_remaining']; ?></td>
                     </tr>   
                <?php } ?>
                </table>
            <?php } ?>
      	</div>
    </div>
    </div>
</div>

