<div class="row"> 
  	<div class="col-xs-12">
        <?php echo $this->Html->link(__('List PR'), array('action' => 'index'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('List PO'), array('action' => 'create'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'pending'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class'=>'btn btn-danger btn-sm')); ?>
        <?php echo $this->Html->link(__('Pending'), array('action' => 'pending_2'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Purchased'), array('action' => 'purchased'), array('class'=>'btn btn-default btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Inventory Item'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Approved Purchase Requisitions'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
           
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo ('#'); ?></th>
                            <th><?php echo $this->Paginator->sort('InventorySupplier.Name', 'Supplier'); ?></th>
							<th><?php echo $this->Paginator->sort('PR Number'); ?></th>
                            <th><?php echo 'PR Value'; ?></th>
							<th><?php echo ('Required Date'); ?></th>
                            <th><?php echo ('Created'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseRequisition']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryPurchaseOrder as $inventoryPurchaseOrder): 
					?>
                    <!-- calculating amount PR -->
                    <?php 

                    $items = $inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisitionItem'];
                    $amount = 0;
                    foreach ($items as $item) {
                        $new_amount = $amount + $item['amount'];

                        $amount = $new_amount;
                    }

                    ?>
					<tr>
						<td class="text-center"><?php echo $startSN++; ?></td>
                        <td><?php echo ($inventoryPurchaseOrder['Supplier'][0]['InventorySupplier']['name']); ?>&nbsp;</td>
						<td><?php echo h($inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisition']['pr_no']); ?>&nbsp;</td>
                        <td><?php echo number_format($amount, 4); ?>&nbsp;</td>
						<td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisition']['dateline']))); ?>&nbsp;</td>
                        <td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisition']['created']))); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view_pr', $inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php //echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisition']['pr_no']).'" ?', $inventoryPurchaseOrder['InventoryPurchaseRequisition']['InventoryPurchaseRequisition']['id'])); ?>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

