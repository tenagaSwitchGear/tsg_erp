<div class="row"> 
  	<div class="col-xs-12">
  	 <?php echo $this->Html->link(__('New Purchase Requisitions'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('List Purchased Requisitions'), array('controller'=>'inventory_purchase_requisitions', 'action' => 'index'), array('class'=>'btn btn-default btn-sm')); ?>
  		<div class="x_panel tile">
      
      		<div class="x_title">
        		<h2><?php echo __('Items');  ?>: <?php echo $item['InventoryItem']['name']; ?> (<?php echo $item['InventoryItem']['code']; ?>)</h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
          
        	<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
				 
				<tr>
						<th><?php echo ('Date'); ?></th>
						<th><?php echo ('Item'); ?></th>
            <th><?php echo ('Supplier'); ?></th>  
            <th><?php echo ('Price Per Unit (RM)'); ?></th>
				</tr>
				<?php

					foreach ($supplier_item as $inventorySupplier):
				?>
				<tr>
					<td><?php echo date('d-m-Y', strtotime($inventorySupplier['InventoryDeliveryOrder']['InventoryPurchaseOrder']['created'])); ?></td>
					<td><?php echo h($inventorySupplier['InventoryItem']['InventoryItem']['name']); ?><br><?php echo h($inventorySupplier['InventoryItem']['InventoryItem']['code']); ?></td>
          <td><?php echo h($inventorySupplier['InventoryDeliveryOrder']['InventorySupplier']['name']); ?></td>
          <td><?php echo number_format($inventorySupplier['InventoryDeliveryOrderItem']['price_per_unit'], 2).'/'.$inventorySupplier['InventoryItem']['GeneralUnit']['name']; ?> </td>
				</tr>
			<?php endforeach; ?>
				 
				</table>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(function() { 
    $('#dateonly2').datepicker({
      dateFormat: 'yy-mm-dd', 
      ampm: true
    }); 
});  
</script>
<?php $this->end(); ?>