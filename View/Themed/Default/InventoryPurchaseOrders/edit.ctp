<?php echo $this->Html->link(__('Back'), array('action' => 'view_verify', $this->request->data['InventoryPurchaseOrder']['id']), array('class'=>'btn btn-primary btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
        
  		<div class="x_panel tile">
      		<div class="x_title">
        		 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
			<?php echo $this->Form->create('InventoryPurchaseOrder', array('class' => 'form-horizontal')); ?>
			<div class="form-group">
	    	<label class="col-sm-3">Terms of Payment*</label>
	    	<div class="col-sm-9">
				<?php echo $this->Form->input('id'); ?>     
				<?php echo $this->Form->input('term_of_payment_id', array('options' => $terms, 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
	    	<label class="col-sm-3">PO Note (Show on Print)</label>
	    	<div class="col-sm-9">		 
				<?php echo $this->Form->input('notes', array('class' => 'form-control', 'label' => false)); ?> 
			</div>
			</div>
			<div class="form-group">
	    	<label class="col-sm-3">Delivery Point </label>
	    	<div class="col-sm-9">	 
	    		<?php echo $this->Form->input('inventory_location_id', array('options' => $loc, 'class' => 'form-control', 'label' => false)); ?>
	    	</div>
			</div>
			<div class="form-group">
	    	<label class="col-sm-3">Warranty </label>
	    	<div class="col-sm-9">	
				<?php echo $this->Form->input('warranty_id', array('options' => $warranty, 'class' => 'form-control', 'label' => false));  ?> 
			</div>
			</div>
			<div class="form-group">
	    	<label class="col-sm-3">Terms of Delivery </label>
	    	<div class="col-sm-9">	
				<?php echo $this->Form->input('term_of_delivery_id', array('options' => $tod, 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
	    	<label class="col-sm-3">Total Discount (Optional) </label>
	    	<div class="col-sm-9">	
				<?php echo $this->Form->input('bulk_discount', array('type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>

			<div class="form-group">
	    	<label class="col-sm-3">ETA </label>
	    	<div class="col-sm-9">	
				<?php echo $this->Form->input('dateline', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?>
			</div>
			</div>

				</fieldset>
			<?php echo $this->Form->end(__('Submit')); ?>
			</div> 
		</div>
	</div>
</div>