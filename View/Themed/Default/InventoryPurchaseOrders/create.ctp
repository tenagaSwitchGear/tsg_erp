
<?php echo $this->Html->link(__('PO Draft'), array('action' => 'create'), array('class'=>'btn btn-warning btn-sm')); ?>  
<?php echo $this->Html->link(__('Purchased'), array('action' => 'create', 1), array('class'=>'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link(__('Partially Received'), array('action' => 'create', 2), array('class'=>'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Closed'), array('action' => 'create', 3), array('class'=>'btn btn-success btn-sm')); ?>
<div class="row"> 
  	<div class="col-xs-12"> 
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Purchase Order'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
            
            <?php echo $this->Form->create('InventoryPurchaseOrder', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
            <table cellpadding="0" cellspacing="0" class="table">
                <tr>
                <td><?php echo $this->Form->input('po_no', array('placeholder' => 'PO Number', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_start', array('type' => 'text', 'placeholder' => 'From Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_end', array('type' => 'text', 'placeholder' => 'To Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker_2', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('status', array('options' => array('x'=>'Select Status', '0'=>'Pending', '1'=>'Purchased'), 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
                </tr>
            </table>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo ('#'); ?></th>
                            <th><?php echo $this->Paginator->sort('InventorySupplier.name', 'Supplier'); ?></th>
                            <th><?php echo $this->Paginator->sort('EndUser.name', 'End User'); ?></th>
                            <th><?php echo $this->Paginator->sort('general_purchase_requisition_type_id', 'Type'); ?></th>
                            <th><?php echo $this->Paginator->sort('PR Number'); ?></th>
							<th><?php echo $this->Paginator->sort('PO Number'); ?></th>
							<th><?php echo ('Required Date'); ?></th>
                            <th><?php echo ('Created'); ?></th>
                            <th><?php echo ('Purchased'); ?></th>
                            <th><?php echo ('PO Status'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseOrder']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseOrder']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseOrder']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryPurchaseOrders as $inventoryPurchaseOrder): 
					?>
                
					<tr>
						<td class="text-center"><?php echo $startSN++; ?></td>
                        <td><?php echo ($inventoryPurchaseOrder['InventorySupplier']['name']); ?>&nbsp;</td>
                        <td><?php echo h($inventoryPurchaseOrder['EndUser']['firstname']); ?>&nbsp;</td>
                        <td><?php echo ($inventoryPurchaseOrder['GeneralPurchaseRequisitionType']['name']); ?>&nbsp;</td>
                        <td><?php echo h($inventoryPurchaseOrder['InventoryPurchaseRequisition']['pr_no']); ?>&nbsp;</td>
						<td><?php echo h($inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']); ?>&nbsp;</td>
						<td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['dateline']))); ?>&nbsp;</td>
                        <td><?php echo h(date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['created']))); ?>&nbsp;</td>
                        <td><?php echo status($inventoryPurchaseOrder['InventoryPurchaseOrder']['purchase_status']); ?>&nbsp;</td>

                        <td><?php echo postatus($inventoryPurchaseOrder['InventoryPurchaseOrder']['status']); ?>&nbsp;</td>

						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view_verify', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php 
							if($inventoryPurchaseOrder['InventoryPurchaseOrder']['purchase_status'] == 2) {
								echo $this->Html->link('<i class="fa fa-truck"></i>', array('action' => 'viewgrn', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); 
							} ?> 
							<?php echo $this->Html->link('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']).'" ?', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id'])); ?>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php

function postatus($no) {
	if($no == 0) {
		return 'Waiting';
	}

	if($no == 1) {
		return 'Partially Received';
	}

	if($no == 2) {
		return 'Closed';
	}  
}

function status($no) {
	if($no == 0) {
		return 'Draft';
	}

	if($no == 1) {
		return 'Purchased';
	}

	if($no == 2) {
		return 'Received';
	} 
	if($no == 3) {
		return 'Closed';
	}
}

?>