<style type="text/css">
    .l_right{
        float: right;
        font-size: 16px
    }
</style>
<?php 
function convertNumber($number)
{
    list($integer, $fraction) = explode(".", (string) $number);

    $output = "";
    //echo $integer[0];
    if ($integer[0] == "-")
    {
        $output = "negative ";
        $integer    = ltrim($integer, "-");
    }
    else if ($integer[0] == "+")
    {
        $output = "positive ";
        $integer    = ltrim($integer, "+");
    }

    if ($integer[0] == "0")
    {
        $output .= "zero";
    }
    else
    {
        $integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
       
        $group   = rtrim(chunk_split($integer, 3, " "), " ");

        $groups  = explode(" ", $group);

        $groups2 = array();
        foreach ($groups as $g)
        {
            $groups2[] = convertThreeDigit($g{0}, $g{1}, $g{2});
        }

        for ($z = 0; $z < count($groups2); $z++)
        {
            if ($groups2[$z] != "")
            {
                $output .= $groups2[$z] . convertGroup(11 - $z) . (
                        $z < 11
                        && !array_search('', array_slice($groups2, $z + 1, -1))
                        && $groups2[11] != ''
                        && $groups[11]{0} == '0'
                            ? " and "
                            : " "
                    );
            }
        }

        $output = rtrim($output, ", ");
    }

    if ($fraction > 0)
    {
        $output .= " and";
        $length = strlen($fraction);
        //echo $length;
        if($length == '2'){
            $output .= " " . convertTwoDigit($fraction[0], $fraction[1]);
        }
    }

    return $output;
}

function convertGroup($index)
{
    switch ($index)
    {
        case 11:
            return " decillion";
        case 10:
            return " nonillion";
        case 9:
            return " octillion";
        case 8:
            return " septillion";
        case 7:
            return " sextillion";
        case 6:
            return " quintrillion";
        case 5:
            return " quadrillion";
        case 4:
            return " trillion";
        case 3:
            return " billion";
        case 2:
            return " million";
        case 1:
            return " thousand";
        case 0:
            return "";
    }
}

function convertThreeDigit($digit1, $digit2, $digit3)
{
    $buffer = "";

    if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
    {
        return "";
    }

    if ($digit1 != "0")
    {
        $buffer .= convertDigit($digit1) . " hundred";
        if ($digit2 != "0" || $digit3 != "0")
        {
            $buffer .= " ";
        }
    }

    if ($digit2 != "0")
    {
        $buffer .= convertTwoDigit($digit2, $digit3);
    }
    else if ($digit3 != "0")
    {
        $buffer .= convertDigit($digit3);
    }

    return $buffer;
}

function convertTwoDigit($digit1, $digit2)
{
    //echo $digit2;
    if ($digit2 == "0")
    {
        switch ($digit1)
        {
            case "1":
                return "ten cents";
            case "2":
                return "twenty cents";
            case "3":
                return "thirty cents";
            case "4":
                return "forty cents";
            case "5":
                return "fifty cents";
            case "6":
                return "sixty cents";
            case "7":
                return "seventy cents";
            case "8":
                return "eighty cents";
            case "9":
                return "ninety cents";

        }
    } else if ($digit1 == "1")
    {
        switch ($digit2)
        {
            case "1":
                return "eleven";
            case "2":
                return "twelve";
            case "3":
                return "thirteen";
            case "4":
                return "fourteen";
            case "5":
                return "fifteen";
            case "6":
                return "sixteen";
            case "7":
                return "seventeen";
            case "8":
                return "eighteen";
            case "9":
                return "nineteen";
        }
    } else
    {
        $temp = convertDigit($digit2);
        switch ($digit1)
        {
            case "2":
                return "twenty-$temp cents";
            case "3":
                return "thirty-$temp cents";
            case "4":
                return "forty-$temp cents";
            case "5":
                return "fifty-$temp cents";
            case "6":
                return "sixty-$temp cents";
            case "7":
                return "seventy-$temp cents";
            case "8":
                return "eighty-$temp cents";
            case "9":
                return "ninety-$temp cents";
        }
    }
}

function convertDigit($digit)
{
    switch ($digit)
    {
        case "0":
            return "zero";
        case "1":
            return "one";
        case "2":
            return "two";
        case "3":
            return "three";
        case "4":
            return "four";
        case "5":
            return "five";
        case "6":
            return "six";
        case "7":
            return "seven";
        case "8":
            return "eight";
        case "9":
            return "nine";
    }
}

 
?>
<div class="book">
    <div class="page">
        <div id="header">
            <div class="x_title">
                <p style="text-align: center; font:10px;">ORIGINAL COPY</p>
                <h3><strong><?php echo strtoupper(Configure::read('Site.company_name')); ?></strong></h3>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="content">
            <table style="font-size: 12px; width: 100%">
                <tr>
                    <td style="width: 5%">To</td>
                    <td style="width: 25%">: <?php echo $inventoryPurchaseOrder['InventorySupplier']['name']; ?></td>
                    <td style="width: 5%">To</td>
                    <td style="width: 25%">: <?php echo $invdelivery_location[0]['inventory_delivery_locations']['name']; ?></td>
                    <td style="width: 5%">PO NO</td>
                    <td style="width: 25%">: <?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']; ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>: <?php echo str_replace(',', '<br> : ', $inventoryPurchaseOrder['InventorySupplier']['address']); ?></td>
                    <td>Address</td>
                    <td>: <?php echo str_replace(',', '<br> : ', $invdelivery_location[0]['inventory_delivery_locations']['address']); ?></td>
                    <td>Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['created'])); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>Your ref</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>Payment Term</td>
                    <td>: <?php foreach ($terms as $key => $term) { 
                        if($key == $inventoryPurchaseOrder['InventoryPurchaseOrder']['term_of_payment_id']){
                            echo $term; 
                        }
                    }
                    ?></td>
                </tr>
                <tr>
                    <td>Tel</td>
                    <td>: <?php echo $inventoryPurchaseOrder['InventorySupplier']['phone_office']; ?></td>
                    <td>Tel</td>
                    <td>: <?php echo $invdelivery_location[0]['inventory_delivery_locations']['contact_num']; ?></td>
                    <td>Currency</td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>: <?php echo $inventoryPurchaseOrder['InventorySupplier']['fax']; ?></td>
                    <td>Fax</td>
                    <td>: </td>
                    <td>Our Reference</td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Delivery Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['dateline'])); ?></td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>: </td>
                    <td>E-mail</td>
                    <td>: </td>
                    <td>Delivery Mode</td>
                    <td>: Ex-TSG</td>
                </tr>
            </table>
            <div class="clearfix">&nbsp;</div>
            <?php if (!empty($inventoryPurchaseOrder['InventoryPurchaseOrderItem'])): ?>
            <table class="table table-bordered" style="font-size: 12px; width: 100%">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo __('#'); ?></th>
                        <th><?php echo __('Items'); ?></th>
                        <th><?php echo __('Quantity'); ?></th>
                        <th><?php echo __('Unit Price'); ?></th>
                        <th><?php echo __('Discount'); ?></th>
                        <th><?php echo __('Tax'); ?></th>
                        <th style="width: 110px"><?php echo __('Amount'); ?></th>
                    </tr>
                </thead>
                <?php 
                    $currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseRequisition']['limit'];
                    $startSN = (($currentPage * $limit) + 1) - $limit;

                    $ttl_amount = 0;
                    $d_amount = 0;
                    $ttl_wth_discount = 0;
                    $ttl_tax = 0;

                    foreach ($inventoryPurchaseOrder['InventoryPurchaseOrderItem'] as $inventoryPO): ?>
                    <?php if($startSN > '14'){ echo "<div style='page-break: always'>&nbsp;</div>"; } ?>
                    <tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td>
                            <?php //echo $inventoryPO['inventory_item_id']; ?>
                            <?php 
                            //print_r($inventoryPurchaseRequisitionItem);
                                    for($i=0; $i<count($inventory_item); $i++){
                                        if($inventoryPO['inventory_item_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_item_id'] && $inventoryPO['inventory_supplier_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_supplier_id']){
                                            echo '<strong>'.$inventory_item[$i]['invitem']['name'].'</strong><br>'.$inventory_item[$i]['invitem']['note'];
                                        }
                                    }
                            ?>
                        </td>
                        <td><?php echo $inventoryPO['quantity'].' '; ?><?php 
                                foreach ($generalUnits as $key => $gu) {
                                    if($inventoryPO['general_unit_id']==$key){
                                        echo $gu;
                                    }
                                }
                            ?></td>
                        <td>
                        <?php 
                            echo $inventoryPO['price_per_unit'];

                            $ttl_wth_discount = $ttl_wth_discount + ($inventoryPO['quantity'] * $inventoryPO['price_per_unit']);
                        ?></td>
                        <td><?php 
                        if($inventoryPO['discount_type'] == '0'){
                            if($inventoryPO['discount'] == '0.0000'){
                                echo "-";
                                $d_amount = $d_amount + 0;
                            }else{
                                echo "RM ".number_format($inventoryPO['discount']);
                                $d_amount = $d_amount + $inventoryPO['discount'];
                            }
                        }else{
                            echo number_format($inventoryPO['discount'], 0).' %';
                            $total_without_discount = $inventoryPO['quantity'] * $inventoryPO['price_per_unit'];
                            $d_amount = $d_amount + ($total_without_discount * ($inventoryPO['discount']/100));
                        }
                        //echo $discount_amount;
                        //echo $d_amount;
                        //echo $d_amount = $d_amount + $discount_amount;
                       //$discount_amount = $discount_amount;
                        ?>
                        </td>
                        <td>
                        <?php 
                        if($inventoryPO['tax'] == '0'){
                            echo "-";

                            $tax_amount = '0';
                        }else{
                            echo $inventoryPO['tax'].' %';
                            //echo $d_amount;
                            if($inventoryPO['discount'] != '0'){
                                $total_deduct_discount = ($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) - (($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) * ($inventoryPO['discount']/100));
                                $tax_amount = ($inventoryPO['tax']/100)*$total_deduct_discount; 
                            }else{
                                $total_deduct_discount = (($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) * ($inventoryPO['tax']/100));
                                $tax_amount = $total_deduct_discount; 
                            }
                            
                            
                        }
                        $ttl_tax = $ttl_tax + $tax_amount;
                        //echo $tax_amount;
                        ?>
                        </td>
                        
                        <td><?php echo $inventoryPO['amount']; $ttl_amount = $ttl_amount + $inventoryPO['amount']; ?></td>
                    </tr>
                <?php endforeach;  ?>
                <?php $ttl_amount = $ttl_amount; 
                $total = number_format($ttl_amount, 2, '.', '');
                ?>
                <?php 
                $total_no_discount = $ttl_wth_discount;
                $ttl_discount = $d_amount; ?>
                    <tr>
                        <td rowspan="4" colspan="4" class="text-vertical" style="padding-top: 40px">
                            <h6 class="text-center">
                                <?php
                                    $num = $total;
                                    $test = convertNumber($num);

                                    echo strtoupper($test).' ONLY';
                                ?>    
                            </h6>
                        </td>
                        <td colspan="2">Total</td>
                        <td><?php echo 'MYR '.number_format($total_no_discount, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Discount</td>
                        <td><?php echo 'MYR '.number_format($ttl_discount, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Tax</td>
                        <td><?php echo 'MYR '.number_format($ttl_tax, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Net Amount</td>
                        <td><?php echo 'MYR '.$total; ?></td>
                    </tr>
                </table>
            <?php endif; ?> 
            <h4><?php echo __('Note :'); ?></h4>
            <h4 class="wrapper"><?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['notes']; ?></h4>
            <div id="page-number" class="front"></div>
            <p class="page-break" style="text-align: center; font:10px; padding-top: 10px">SALINAN ASAL</p>
            <p align="right" id="po" style="padding-top: 20px;">PO Number : <?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']; ?></p>
            <div class="footer">
                <h4 class="text-center">"RIGHT THE FIRST TIME &amp; EVERY TIME</h4>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td colspan="2">GST No : <?php echo $inventoryPurchaseOrder['InventorySupplier']['gst']; ?></td>
                        </tr>
                        <tr>
                            <td class="fbody">
                            We hereby acknowledge and accept the term &amp;
                            conditions of the purchase order.
                            <br>
                            (Please fax back within 3 working days)
                            <br><br><br>
                            ...............................................................
                            <br>
                            Company's Stamp &amp; Signature
                            <br>
                            </td>
                            <td class="fbody">
                            For and on half of TENAGA SWITCHGEAR SDN BHD
                            <br><br><br><br>
                            ...............................................................
                            <br>
                            Authorized Signatory
                            <br>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="page-number" class="last"></div>
        </div>
    </div>
</div>

<div class="book">
    <div class="page">
        <div id="header">
            <div class="x_title">
                <p style="text-align: center; font:10px;">SUPPLIER COPY</p>
                <h3><strong><?php echo strtoupper(Configure::read('Site.company_name')); ?></strong></h3>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="content">
            <table style="font-size: 12px; width: 100%">
                <tr>
                    <td style="width: 5%">To</td>
                    <td style="width: 25%">: <?php echo $inventoryPurchaseOrder['InventorySupplier']['name']; ?></td>
                    <td style="width: 5%">To</td>
                    <td style="width: 25%">: <?php echo $invdelivery_location[0]['inventory_delivery_locations']['name']; ?></td>
                    <td style="width: 5%">PO NO</td>
                    <td style="width: 25%">: <?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']; ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>: <?php echo str_replace(',', '<br> : ', $inventoryPurchaseOrder['InventorySupplier']['address']); ?></td>
                    <td>Address</td>
                    <td>: <?php echo str_replace(',', '<br> : ', $invdelivery_location[0]['inventory_delivery_locations']['address']); ?></td>
                    <td>Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['created'])); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>Your ref</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>Payment Term</td>
                    <td>: <?php foreach ($terms as $key => $term) { 
                        if($key == $inventoryPurchaseOrder['InventoryPurchaseOrder']['term_of_payment_id']){
                            echo $term; 
                        }
                    }
                    ?></td>
                </tr>
                <tr>
                    <td>Tel</td>
                    <td>: <?php echo $inventoryPurchaseOrder['InventorySupplier']['phone_office']; ?></td>
                    <td>Tel</td>
                    <td>: <?php echo $invdelivery_location[0]['inventory_delivery_locations']['contact_num']; ?></td>
                    <td>Currency</td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>: <?php echo $inventoryPurchaseOrder['InventorySupplier']['fax']; ?></td>
                    <td>Fax</td>
                    <td>: </td>
                    <td>Our Reference</td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Delivery Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['dateline'])); ?></td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>: </td>
                    <td>E-mail</td>
                    <td>: </td>
                    <td>Delivery Mode</td>
                    <td>: Ex-TSG</td>
                </tr>
            </table>
            <div class="clearfix">&nbsp;</div>
            <?php if (!empty($inventoryPurchaseOrder['InventoryPurchaseOrderItem'])): ?>
            <table class="table table-bordered" style="font-size: 12px; width: 100%">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo __('#'); ?></th>
                        <th><?php echo __('Items'); ?></th>
                        <th><?php echo __('Quantity'); ?></th>
                        <th><?php echo __('Unit Price'); ?></th>
                        <th><?php echo __('Discount'); ?></th>
                        <th><?php echo __('Tax'); ?></th>
                        <th style="width: 110px"><?php echo __('Amount'); ?></th>
                    </tr>
                </thead>
                <?php 
                    $currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseRequisition']['limit'];
                    $startSN = (($currentPage * $limit) + 1) - $limit;

                    $ttl_amount = 0;
                    $d_amount = 0;
                    $ttl_wth_discount = 0;
                    $ttl_tax = 0;

                    foreach ($inventoryPurchaseOrder['InventoryPurchaseOrderItem'] as $inventoryPO): ?>
                    <?php if($startSN > '14'){ echo "<div style='page-break: always'>&nbsp;</div>"; } ?>
                    <tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td>
                            <?php //echo $inventoryPO['inventory_item_id']; ?>
                            <?php 
                            //print_r($inventoryPurchaseRequisitionItem);
                                    for($i=0; $i<count($inventory_item); $i++){
                                        if($inventoryPO['inventory_item_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_item_id'] && $inventoryPO['inventory_supplier_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_supplier_id']){
                                            echo '<strong>'.$inventory_item[$i]['invitem']['name'].'</strong><br>';
                                        }
                                    }
                            ?>
                        </td>
                        <td><?php echo $inventoryPO['quantity'].' '; ?><?php 
                                foreach ($generalUnits as $key => $gu) {
                                    if($inventoryPO['general_unit_id']==$key){
                                        echo $gu;
                                    }
                                }
                            ?></td>
                        <td>
                        <?php 
                            echo $inventoryPO['price_per_unit'];

                            $ttl_wth_discount = $ttl_wth_discount + ($inventoryPO['quantity'] * $inventoryPO['price_per_unit']);
                        ?></td>
                        <td><?php 
                        if($inventoryPO['discount_type'] == '0'){
                            if($inventoryPO['discount'] == '0.0000'){
                                echo "-";
                                $d_amount = $d_amount + 0;
                            }else{
                                echo "RM ".number_format($inventoryPO['discount']);
                                $d_amount = $d_amount + $inventoryPO['discount'];
                            }
                        }else{
                            echo number_format($inventoryPO['discount'], 0).' %';
                            $total_without_discount = $inventoryPO['quantity'] * $inventoryPO['price_per_unit'];
                            $d_amount = $d_amount + ($total_without_discount * ($inventoryPO['discount']/100));
                        }
                        //echo $discount_amount;
                        //echo $d_amount;
                        //echo $d_amount = $d_amount + $discount_amount;
                       //$discount_amount = $discount_amount;
                        ?>
                        </td>
                        <td>
                        <?php 
                        if($inventoryPO['tax'] == '0'){
                            echo "-";

                            $tax_amount = '0';
                        }else{
                            echo $inventoryPO['tax'].' %';
                            //echo $d_amount;
                            if($inventoryPO['discount'] != '0'){
                                $total_deduct_discount = ($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) - (($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) * ($inventoryPO['discount']/100));
                                $tax_amount = ($inventoryPO['tax']/100)*$total_deduct_discount; 
                            }else{
                                $total_deduct_discount = (($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) * ($inventoryPO['tax']/100));
                                $tax_amount = $total_deduct_discount; 
                            }
                            
                            
                        }
                        $ttl_tax = $ttl_tax + $tax_amount;
                        //echo $tax_amount;
                        ?>
                        </td>
                        
                        <td><?php echo $inventoryPO['amount']; $ttl_amount = $ttl_amount + $inventoryPO['amount']; ?></td>
                    </tr>
                <?php endforeach;  ?>
                <?php $ttl_amount = $ttl_amount; 
                $total = number_format($ttl_amount, 2, '.', '');
                ?>
                <?php 
                $total_no_discount = $ttl_wth_discount;
                $ttl_discount = $d_amount; ?>
                    <tr>
                        <td rowspan="4" colspan="4" class="text-vertical" style="padding-top: 40px">
                            <h4 class="text-center">
                                <?php
                                    $num = $total;
                                    $test = convertNumber($num);

                                    echo strtoupper($test).' ONLY';
                                ?>    
                            </h4>
                        </td>
                        <td colspan="2">Total</td>
                        <td><?php echo 'MYR '.number_format($total_no_discount, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Discount</td>
                        <td><?php echo 'MYR '.number_format($ttl_discount, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Tax</td>
                        <td><?php echo 'MYR '.number_format($ttl_tax, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Net Amount</td>
                        <td><?php echo 'MYR '.$total; ?></td>
                    </tr>
                </table>
            </div>
            <?php endif; ?>
            <h4><?php echo __('Note :'); ?></h4>
            <h4 class="wrapper"><?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['notes']; ?></h4>
            <div id="page-number" class="front"></div>
            <p class="page-break" style="text-align: center; font:10px; padding-top: 10px">SALINAN PEMBEKAL</p>
            <p align="right" id="po" style="padding-top: 20px;">PO Number : <?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']; ?></p>
            <div class="footer">
                <h4 class="text-center">"RIGHT THE FIRST TIME &amp; EVERY TIME</h4>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td colspan="2">GST No : <?php echo $inventoryPurchaseOrder['InventorySupplier']['gst']; ?></td>
                        </tr>
                        <tr>
                            <td class="fbody">
                            We hereby acknowledge and accept the term &amp;
                            conditions of the purchase order.
                            <br>
                            (Please fax back within 3 working days)
                            <br><br><br>
                            ...............................................................
                            <br>
                            Company's Stamp &amp; Signature
                            <br>
                            </td>
                            <td class="fbody">
                            For and on half of TENAGA SWITCHGEAR SDN BHD
                            <br><br><br><br>
                            ...............................................................
                            <br>
                            Authorized Signatory
                            <br>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="page-number" class="last"></div>
        </div>
    </div>
</div>

<div class="book">
    <div class="page">
        <div id="header">
            <div class="x_title">
                <p style="text-align: center; font:12px;">COPY</p>
                <h3><strong><?php echo strtoupper(Configure::read('Site.company_name')); ?></strong></h3>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="content">
            <table style="font-size: 12px; width: 100%">
                <tr>
                    <td style="width: 5%">To</td>
                    <td style="width: 25%">: <?php echo $inventoryPurchaseOrder['InventorySupplier']['name']; ?></td>
                    <td style="width: 5%">To</td>
                    <td style="width: 25%">: <?php echo $invdelivery_location[0]['inventory_delivery_locations']['name']; ?></td>
                    <td style="width: 5%">PO NO</td>
                    <td style="width: 25%">: <?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']; ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>: <?php echo str_replace(',', '<br> : ', $inventoryPurchaseOrder['InventorySupplier']['address']); ?></td>
                    <td>Address</td>
                    <td>: <?php echo str_replace(',', '<br> : ', $invdelivery_location[0]['inventory_delivery_locations']['address']); ?></td>
                    <td>Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['created'])); ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>Your ref</td>
                    <td>:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                    <td>:</td>
                    <td>Payment Term</td>
                    <td>: <?php foreach ($terms as $key => $term) { 
                        if($key == $inventoryPurchaseOrder['InventoryPurchaseOrder']['term_of_payment_id']){
                            echo $term; 
                        }
                    }
                    ?></td>
                </tr>
                <tr>
                    <td>Tel</td>
                    <td>: <?php echo $inventoryPurchaseOrder['InventorySupplier']['phone_office']; ?></td>
                    <td>Tel</td>
                    <td>: <?php echo $invdelivery_location[0]['inventory_delivery_locations']['contact_num']; ?></td>
                    <td>Currency</td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>: <?php echo $inventoryPurchaseOrder['InventorySupplier']['fax']; ?></td>
                    <td>Fax</td>
                    <td>: </td>
                    <td>Our Reference</td>
                    <td>: </td>
                </tr>
                <tr>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Attn</td>
                    <td>: </td>
                    <td>Delivery Date</td>
                    <td>: <?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['dateline'])); ?></td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>: </td>
                    <td>E-mail</td>
                    <td>: </td>
                    <td>Delivery Mode</td>
                    <td>: Ex-TSG</td>
                </tr>
            </table>
            <div class="clearfix">&nbsp;</div>
            <?php if (!empty($inventoryPurchaseOrder['InventoryPurchaseOrderItem'])): ?>
            <table class="table table-bordered" style="font-size: 12px; width: 100%">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo __('#'); ?></th>
                        <th><?php echo __('Items'); ?></th>
                        <th><?php echo __('Quantity'); ?></th>
                        <th><?php echo __('Unit Price'); ?></th>
                        <th><?php echo __('Discount'); ?></th>
                        <th><?php echo __('Tax'); ?></th>
                        <th style="width: 110px"><?php echo __('Amount'); ?></th>
                    </tr>
                </thead>
                <?php 
                    $currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseRequisition']['limit'];
                    $startSN = (($currentPage * $limit) + 1) - $limit;

                    $ttl_amount = 0;
                    $d_amount = 0;
                    $ttl_wth_discount = 0;
                    $ttl_tax = 0;

                    foreach ($inventoryPurchaseOrder['InventoryPurchaseOrderItem'] as $inventoryPO): ?>
                    <?php if($startSN > '14'){ echo "<div style='page-break: always'>&nbsp;</div>"; } ?>
                    <tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td>
                            <?php //echo $inventoryPO['inventory_item_id']; ?>
                            <?php 
                            //print_r($inventoryPurchaseRequisitionItem);
                                    for($i=0; $i<count($inventory_item); $i++){
                                        if($inventoryPO['inventory_item_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_item_id'] && $inventoryPO['inventory_supplier_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_supplier_id']){
                                            echo '<strong>'.$inventory_item[$i]['invitem']['name'].'</strong><br>';
                                        }
                                    }
                            ?>
                        </td>
                        <td><?php echo $inventoryPO['quantity'].' '; ?><?php 
                                foreach ($generalUnits as $key => $gu) {
                                    if($inventoryPO['general_unit_id']==$key){
                                        echo $gu;
                                    }
                                }
                            ?></td>
                        <td>
                        <?php 
                            echo $inventoryPO['price_per_unit'];

                            $ttl_wth_discount = $ttl_wth_discount + ($inventoryPO['quantity'] * $inventoryPO['price_per_unit']);
                        ?></td>
                        <td><?php 
                        if($inventoryPO['discount_type'] == '0'){
                            if($inventoryPO['discount'] == '0.0000'){
                                echo "-";
                                $d_amount = $d_amount + 0;
                            }else{
                                echo "RM ".number_format($inventoryPO['discount']);
                                $d_amount = $d_amount + $inventoryPO['discount'];
                            }
                        }else{
                            echo number_format($inventoryPO['discount'], 0).' %';
                            $total_without_discount = $inventoryPO['quantity'] * $inventoryPO['price_per_unit'];
                            $d_amount = $d_amount + ($total_without_discount * ($inventoryPO['discount']/100));
                        }
                        //echo $discount_amount;
                        //echo $d_amount;
                        //echo $d_amount = $d_amount + $discount_amount;
                       //$discount_amount = $discount_amount;
                        ?>
                        </td>
                        <td>
                        <?php 
                        if($inventoryPO['tax'] == '0'){
                            echo "-";

                            $tax_amount = '0';
                        }else{
                            echo $inventoryPO['tax'].' %';
                            //echo $d_amount;
                            if($inventoryPO['discount'] != '0'){
                                $total_deduct_discount = ($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) - (($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) * ($inventoryPO['discount']/100));
                                $tax_amount = ($inventoryPO['tax']/100)*$total_deduct_discount; 
                            }else{
                                $total_deduct_discount = (($inventoryPO['quantity'] * $inventoryPO['price_per_unit']) * ($inventoryPO['tax']/100));
                                $tax_amount = $total_deduct_discount; 
                            }
                            
                            
                        }
                        $ttl_tax = $ttl_tax + $tax_amount;
                        //echo $tax_amount;
                        ?>
                        </td>
                        
                        <td><?php echo $inventoryPO['amount']; $ttl_amount = $ttl_amount + $inventoryPO['amount']; ?></td>
                    </tr>
                <?php endforeach;  ?>
                <?php $ttl_amount = $ttl_amount; 
                $total = number_format($ttl_amount, 2, '.', '');
                ?>
                <?php 
                $total_no_discount = $ttl_wth_discount;
                $ttl_discount = $d_amount; ?>
                    <tr>
                        <td rowspan="4" colspan="4" class="text-vertical" style="padding-top: 40px">
                            <h4 class="text-center">
                                <?php
                                    $num = $total;
                                    $test = convertNumber($num);

                                    echo strtoupper($test).' ONLY';
                                ?>    
                            </h4>
                        </td>
                        <td colspan="2">Total</td>
                        <td><?php echo 'MYR '.number_format($total_no_discount, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Discount</td>
                        <td><?php echo 'MYR '.number_format($ttl_discount, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Tax</td>
                        <td><?php echo 'MYR '.number_format($ttl_tax, 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Net Amount</td>
                        <td><?php echo 'MYR '.$total; ?></td>
                    </tr>
                </table>
            </div>
            <?php endif; ?>
            <h4><?php echo __('Note :'); ?></h4>
            <h4 class="wrapper"><?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['notes']; ?></h4>
            <div id="page-number" class="front"></div>
            <p  class="page-break" style="text-align: center; font:12px; padding-top: 10px">SALINAN PEMBEKAL</p>
            <p align="right" id="po" style="padding-top: 20px;">PO Number : <?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']; ?></p>
            <div class="footer">
                <h4 class="text-center">"RIGHT THE FIRST TIME &amp; EVERY TIME</h4>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td colspan="2">GST No : <?php echo $inventoryPurchaseOrder['InventorySupplier']['gst']; ?></td>
                        </tr>
                        <tr>
                            <td class="fbody">
                            We hereby acknowledge and accept the term &amp;
                            conditions of the purchase order.
                            <br>
                            (Please fax back within 3 working days)
                            <br><br><br>
                            ...............................................................
                            <br>
                            Company's Stamp &amp; Signature
                            <br>
                            </td>
                            <td class="fbody">
                            For and on half of TENAGA SWITCHGEAR SDN BHD
                            <br><br><br><br>
                            ...............................................................
                            <br>
                            Authorized Signatory
                            <br>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="page-number" class="last"></div>
        </div>
    </div>
</div>