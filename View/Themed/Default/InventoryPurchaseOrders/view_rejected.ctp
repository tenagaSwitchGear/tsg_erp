<style type="text/css">
    .l_right{
        float: right;
        font-size: 16px
    }
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List PR'), array('action' => 'index'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('List PO'), array('action' => 'create'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'pending'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class'=>'btn btn-danger btn-sm')); ?>
        <?php echo $this->Html->link(__('Pending'), array('action' => 'pending_2'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Purchased'), array('action' => 'purchased'), array('class'=>'btn btn-default btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Purchase Order'); ?></h2>
                <label class="l_right text-danger">Status : Rejected</label>
        	<div class="clearfix"></div>
            <div class="x_title"><?php if($inventoryPurchaseOrders['InventoryPurchaseOrder']['status']!= 1){?>
               <?php echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'view_pdf', $inventoryPurchaseOrders['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-success', 'escape'=>false));
               //echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'view_pdf', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class' => 'btn btn-info', 'escape'=>false)); ?>
                <?php } ?></div>
      	</div>
        
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>Order Date</th>
                    <th>ETA</th>
                    <th>Term of Payment</th>
                    <th>Term of Delivery</th>
                    <th>PO Number</th>
                </tr>
                </thead>
                <tr>
                    <td><?php echo date('d/m/Y', strtotime($inventoryPurchaseOrders['InventoryPurchaseOrder']['created'])); ?></td>
                    <td><?php echo date('d/m/Y', strtotime($inventoryPurchaseOrders['InventoryPurchaseOrder']['dateline'])); ?></td>
                    <td><?php echo $inventoryPurchaseOrders['TermOfPayment']['name']; ?></td>
                    <td>Ex-TSG</td>
                    <td><?php echo $inventoryPurchaseOrders['InventoryPurchaseOrder']['po_no']; ?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p><strong>VENDOR:</strong></p>
                        <p><strong><?php echo $inventoryPurchaseOrders['InventorySupplier']['name'].'<br>'; ?></strong>
                        <?php echo $inventoryPurchaseOrders['InventorySupplier']['address'].'<br>'; ?>
                        <?php echo $inventoryPurchaseOrders['InventorySupplier']['postcode']; ?>
                        <?php echo $inventoryPurchaseOrders['InventorySupplier']['city'].'<br>'; ?>
                        <?php foreach($states as $key => $state){
                            if($key == $inventoryPurchaseOrders['InventorySupplier']['state_id']){
                                echo $state.'<br>';
                            }
                            } ?>
                        <?php foreach($countries as $key => $country){
                            if($key == $inventoryPurchaseOrders['InventorySupplier']['country_id']){
                                echo $country.'<br><br>';
                            }
                            } ?>
                        </p>
                        <p><strong>CONTACT:</strong></p>
                        <p>Tel : <?php echo $inventoryPurchaseOrders['InventorySupplier']['phone_office'].'<br>'; ?>
                            H/P : <?php echo $inventoryPurchaseOrders['InventorySupplier']['phone_mobile']; ?>
                        </p>
                    </td>
                    <td colspan="2">
                        <p><strong>DELIVERY POINT:</strong></p>
                        <p><strong><?php echo $inventoryPurchaseOrders['InventoryDeliveryLocation']['name'].'<br>'; ?></strong>
                        <?php echo $inventoryPurchaseOrders['InventoryDeliveryLocation']['address'].'<br>'; ?>
                        <?php echo $inventoryPurchaseOrders['InventoryDeliveryLocation']['postcode'].'<br>'; ?>
                        <?php foreach($states as $key => $state){
                            if($key == $inventoryPurchaseOrders['InventoryDeliveryLocation']['state_id']){
                                echo $state.'<br>';
                            }
                            } ?>
                        <?php foreach($countries as $key => $country){
                            if($key == $inventoryPurchaseOrders['InventoryDeliveryLocation']['country_id']){
                                echo $country.'<br><br>';
                            }
                            } ?>

                        </p>
                        <p><strong>CONTACT :</strong></p>
                        <p>Contact Person : <?php echo $inventoryPurchaseOrders['InventoryDeliveryLocation']['contact_person'].'<br>'; ?>
                            Contact Number : <?php echo $inventoryPurchaseOrders['InventoryDeliveryLocation']['contact_num']; ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><strong>Remark</strong></td>
                    <td colspan="3"><?php foreach ($remark as $key => $remark) { 
                        if($key == $inventoryPurchaseOrders['InventoryPurchaseOrder']['remark_id']){
                            echo $remark; 
                        }
                    }
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2"><strong>Warranty/Guarantee</strong></td>
                    <td colspan="3"><?php foreach ($warranty as $key => $warranty) { 
                        if($key == $inventoryPurchaseOrders['InventoryPurchaseOrder']['warranty_id']){
                            echo $warranty; 
                        }
                    }
                    ?></td>
                </tr>
            </table>
			
			<div class="clearfix">&nbsp;</div>
			
			<div class="related">
				<h4><?php echo __('Purchase Requisition Items'); ?></h4>
				<?php if (!empty($inventoryPurchaseOrders['InventoryPurchaseOrderItem'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
				<tr>
					<th class="text-center"><?php echo __('#'); ?></th>
					<th><?php echo __('Items'); ?></th>
					<th><?php echo __('Quantity'); ?></th>
					<th><?php echo __('Unit Price'); ?></th>
					<th><?php echo __('Discount'); ?></th>
					<th><?php echo __('Tax'); ?></th>
					<th><?php echo __('Amount'); ?></th>
				</tr>
				<?php $no = '1'; 
				foreach ($inventoryPurchaseOrders['InventoryPurchaseOrderItem'] as $inventoryPurchaseOrderItem): ?>
					<tr>
						<td class="text-center"><?php echo $no++; ?></td>
						<td>
							<?php echo '<strong>CODE : '.strtoupper($inventoryPurchaseOrderItem['InventoryItem']['code']).'</strong>'; ?><br>
							<?php 
							//print_r($inventoryPurchaseRequisitionItem);
									echo '<strong>'.$inventoryPurchaseOrderItem['InventoryItem']['name'].'</strong><br>';
									
							?>
							<?php echo $this->Html->link(('<i class="fa fa-external-link" aria-hidden="true"></i>'), array('controller'=>'inventory_purchase_requisition_items', 'action' => 'index', $inventoryPurchaseOrderItem['InventoryItem']['id']), array('escape'=>false, 'target'=>'_blank')); ?>
						</td>
						<td><?php echo $inventoryPurchaseOrderItem['quantity']; ?><br><?php 
								echo $inventoryPurchaseOrderItem['GeneralUnit']['name'];
							?></td>
						<td><?php echo $inventoryPurchaseOrderItem['price_per_unit']; ?></td>
						<td>
						<?php
						if($inventoryPurchaseOrderItem['discount_type'] == '0'){
							if($inventoryPurchaseOrderItem['discount'] == '0.0000'){
								echo "-";
							}else{
								echo "RM ".number_format($inventoryPurchaseOrderItem['discount'], 2);
							}
						}else{
							echo number_format($inventoryPurchaseOrderItem['discount'], 0).' %'; 
						}

						?>
						</td>
						<td>
						<?php
						if($inventoryPurchaseOrderItem['tax'] == '0'){
							echo "-";
						}else{
							echo $inventoryPurchaseOrderItem['tax'].' %';
						}
						?>
						</td>
						<td>
						<?php 
							echo $inventoryPurchaseOrderItem['amount']; 
						?></td>
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Purchase Order Item
				<?php endif; ?>
			</div>
            <div>
                <h4>Approval/Rejected Remark : <?php echo $inventoryPurchaseOrders['InventoryPurchaseOrder']['approval_remark']; ?></h4>
            </div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

