<style type="text/css">
    .l_right{
        float: right;
        font-size: 16px
    }
</style>
<?php 
function convertNumber($input) {
    $f = new \NumberFormatter("en", NumberFormatter::SPELLOUT); 
    $number = explode('.', $input);
    if($number[1] == '00') {
        $output = $f->format($number[0]); 
    } else {
        $output = $f->format($number[0]) . ' and ' . $f->format($number[1]) . ' cent'; 
    }
    
    return ucwords($output); 
    return $output;
} 
?>
<div class="book page-break">
    <div class="page">
        <div id="header">
           
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="content" class="container">
            <table style="width: 100%; height: 220px;">
                <tr style="vertical-align: top;">
                    <td style="width: 8%">Supplier <span class="align-right">:</span></td>
                    <td style="width: 28%"><?php echo $inventoryPurchaseOrder['InventorySupplier']['name']; ?></td>
                    <td style="width: 14%">To <span class="align-right">:</span></td>
                    <td style="width: 30%"><?php echo $invdelivery_location[0]['inventory_delivery_locations']['name']; ?></td>
                    <td style="width: 8%">PO No <span class="align-right">:</span></td>
                    <td style="width: 10%"><?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['po_no']; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top">Address <span class="align-right">:</span></td>
                    <td style="vertical-align: top"><div class="p-no-space"><p class="wrapper"><?php echo $supplier['InventorySupplier']['address']; ?><br></p><p><?php echo $supplier['InventorySupplier']['postcode']; ?> <?php echo $supplier['InventorySupplier']['city']; ?><br>
                    <?php 
                    echo $supplier['State']['name'] == 'Others' ? $supplier['InventorySupplier']['state'] : $supplier['State']['name'];
                    
                    ?><br/>
                    <?php echo $supplier['Country']['name']; ?>
                    </p>
                    </div>
                    <p>&nbsp;</p>

                    </td>
                    <td style="vertical-align: top">Delivery Point <span class="align-right">:</span></td>
                    <td rowspan="2" style="vertical-align: top"><?php echo $inventoryPurchaseOrder['InventoryLocation']['name']; ?><br><p class="wrapper"><?php echo $inventoryPurchaseOrder['InventoryLocation']['address']; ?></p></td>
                    <td style="vertical-align: top">Date <span class="align-right">:</span></td>
                    <td style="vertical-align: top"><?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['created'])); ?></td>
                </tr> 
                <tr>
                    <td>GST ID <span class="align-right">:</span></td>
                    <td><?php echo $inventoryPurchaseOrder['InventorySupplier']['gst']; ?></td>
                    <td></td> 
                    <td>PR No <span class="align-right">:</span></td> 
                    <td><?php echo $purchase_requisition['InventoryPurchaseRequisition']['pr_no']; ?></td> 
                </tr>
                <tr>
                    <td>Phone <span class="align-right">:</span></td>
                    <td><?php echo $inventoryPurchaseOrder['InventorySupplier']['phone_office']; ?></td>
                    <td>Contact Person <span class="align-right">:</span></td>
                    <td><?php echo $hos['User']['firstname']; ?></td>
                    <td>Currency <span class="align-right">:</span></td>
                    <td><?php echo $currency['iso_code']; ?></td>
                </tr>
                <tr>
                    <td>Fax <span class="align-right">:</span></td>
                    <td><?php echo $inventoryPurchaseOrder['InventorySupplier']['fax']; ?></td>
                    <td>Requester <span class="align-right">:</span></td>
                    <td><?php echo $purchase_requisition['User']['firstname']; ?></td>
                    <td>Del. Date <span class="align-right">:</span></td>
                    <td><?php echo date('d/m/Y', strtotime($inventoryPurchaseOrder['InventoryPurchaseOrder']['dateline'])); ?></td>
                </tr>
                <tr>
                    <td>Email <span class="align-right">:</span></td>
                    <td><?php echo $inventoryPurchaseOrder['InventorySupplier']['email']; ?></td>
                    <td></td>
                    <td></td>
                    <td></td> 
                    <td></td> 
                </tr>
                <tr>
                    <td>Attn <span class="align-right">:</span></td>
                    <td><?php echo $inventoryPurchaseOrder['InventorySupplier']['pic']; ?></td>
                    <td></td>
                    <td></td>
                    <td></td> 
                    <td></td> 
                </tr>
            </table>
            <div class="clearfix">&nbsp;</div> 
            <table style="width: 100%;">
                <tr style="vertical-align: top;">
                    <td width="15%">Terms of Payment <span class="align-right">:</span></td>
                    <td width="35%"><?php echo $inventoryPurchaseOrder['TermOfPayment']['name']; ?></td>
                    <td width="15%">Terms of Delivery <span class="align-right">:</span></td>
                    <td width="35%"><?php echo $inventoryPurchaseOrder['TermOfDelivery']['name']; ?></td>
                     
                </tr>  
            </table>

            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
            <?php if (!empty($inventoryPurchaseOrder['InventoryPurchaseOrderItem'])): ?>
            <table class="table table-bordered" style="font-size: 12px; width: 100%">
                <thead>
                    <tr>
                        <th class="text-center"><?php echo __('No'); ?></th>
                        <th><?php echo __('Items'); ?></th>
                        <th><?php echo __('Quantity'); ?></th>
                        <th><?php echo __('Unit Price'); ?></th>
                        <th><?php echo __('Discount'); ?></th>
                        <th><?php echo __('Tax'); ?></th>
                        <th style="width: 110px" class="text-right"><?php echo __('Amount'); ?></th>
                    </tr>
                </thead>

                <?php

                // Check bulk discount
                if($inventoryPurchaseOrder['InventoryPurchaseOrder']['bulk_discount'] > 0) {
                    $bulk = $inventoryPurchaseOrder['InventoryPurchaseOrder']['bulk_discount'];
                } else {
                    $bulk = 0;
                }
                // Header for sum item in pr
                $header_total = 0;
                //var_dump($items[0]["InventorySupplier"]["gst"]);
                foreach ($poitems as $inventoryPurchaseOrderItem) {  
                    $header_total += $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['price_per_unit'] * $inventoryPurchaseOrderItem['InventoryPurchaseOrderItem']['quantity']; 
                }

                ?>

                <?php   


                    $ttl_amount = 0;
                    $d_amount = 0;
                    $ttl_wth_discount = 0;
                    $ttl_tax = 0;
                    $calc_gst = 0;
                    $startSN = 1;
                    foreach ($poitems as $item): ?> 
                    <tr>
                        <td class="text-center"><?php echo $startSN; ?></td>
                        <td> 
                        <?php  
                        /* for($i=0; $i<count($inventory_item); $i++) {
                            if($item['inventory_item_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_item_id'] && $item['inventory_supplier_id'] == $inventory_item[$i]['InventorySupplierItems']['inventory_supplier_id']){
                                echo $inventory_item[$i]['invitem']['name'].'<br><small>'.$inventory_item[$i]['invitem']['code'].'</small><br><small>'.$inventory_item[$i]['invitem']['note'] . '</small>';
                            }
                        } */ 
                        ?>
                        <?php echo $item['InventoryItem']['name']; ?><br/>
                        <small>
                        <?php echo $item['InventoryItem']['code']; ?><br/>
                        <?php   
                            echo $item['InventoryPurchaseOrderItem']['remark']; 
                        ?>

                        </small>
                        </td>
                        <td><?php echo $item['InventoryPurchaseOrderItem']['quantity']; ?> <?php 
                            echo $item['GeneralUnit']['name'];
                                /*foreach ($generalUnits as $key => $gu) {
                                    if($item['general_unit_id']==$key){
                                        echo $gu;
                                    }
                                }*/
                            ?></td>
                        <td>
                        <?php 
                            echo $item['InventoryPurchaseOrderItem']['price_per_unit']; 
                            $ttl_wth_discount = $item['InventoryPurchaseOrderItem']['quantity'] * $item['InventoryPurchaseOrderItem']['price_per_unit'];
                        ?></td>
                        <td><?php 
                        $item_price = $item['InventoryPurchaseOrderItem']['quantity'] * $item['InventoryPurchaseOrderItem']['price_per_unit'];
                        if($item['InventoryPurchaseOrderItem']['discount_type'] == '0'){
                            if($item['InventoryPurchaseOrderItem']['discount'] == 0){
                                echo "-";
                                $d_amount = $d_amount + 0;
                            } else {
                                echo number_format($item['InventoryPurchaseOrderItem']['discount']);
                                $d_amount = $d_amount + $item['InventoryPurchaseOrderItem']['discount'];
                            }
                        } else {
                            echo number_format($item['InventoryPurchaseOrderItem']['discount'], 0).' %';
                            $total_without_discount = $item['InventoryPurchaseOrderItem']['quantity'] * $item['InventoryPurchaseOrderItem']['price_per_unit'];
                            $d_amount = $d_amount + ($total_without_discount * ($item['InventoryPurchaseOrderItem']['discount']/100));
                        } 
                        $item_price = $item_price - $d_amount;
                        ?>
                        </td>
                        <td>
                        <?php 
                        if($item['InventoryPurchaseOrderItem']['tax'] == '0'){
                            echo "-";

                            $tax_amount = '0';
                            $tax = 0;
                        }else{
                            echo $item['InventoryPurchaseOrderItem']['tax'].' %';
                            //echo $d_amount;
                            if($item['InventoryPurchaseOrderItem']['discount'] != '0'){
                                $total_deduct_discount = ($item['InventoryPurchaseOrderItem']['quantity'] * $item['InventoryPurchaseOrderItem']['price_per_unit']) - (($item['InventoryPurchaseOrderItem']['quantity'] * $item['InventoryPurchaseOrderItem']['price_per_unit']) * ($item['InventoryPurchaseOrderItem']['discount']/100));
                                $tax_amount = ($item['InventoryPurchaseOrderItem']['tax']/100)*$total_deduct_discount; 
                            }else{
                                $total_deduct_discount = (($item['InventoryPurchaseOrderItem']['quantity'] * $item['InventoryPurchaseOrderItem']['price_per_unit']) * ($item['InventoryPurchaseOrderItem']['tax']/100));
                                $tax_amount = $total_deduct_discount; 
                            }
                            
                            $tax = $item['InventoryPurchaseOrderItem']['tax'];
                        }
                        $ttl_tax = $ttl_tax + $tax_amount; 
                        ?>
                         <?php  

                        // Calculating for bulk & gst
                        //if($bulk > 0) {
                            $a = ($bulk / $header_total) * $item_price;
                            $b = $item_price - $a;

                            if($tax > 0) {
                                $calc_sum_tax = ($b * $tax) / 100;
                                $calc_gst += $calc_sum_tax;
                            } else {
                                $calc_sum_tax = 0;
                                $calc_gst += $calc_sum_tax;
                            }
                            
                        //} else {
                        //  $calc_sum_tax = 0;
                        //}
                        $startSN++;
                        ?>
                        </td>
                        
                        <td class="text-right"><?php echo _n2($item['InventoryPurchaseOrderItem']['amount']); $ttl_amount = $ttl_amount + $item['InventoryPurchaseOrderItem']['amount']; ?></td>
                    </tr>
                <?php endforeach;  ?>
                <?php $ttl_amount = $ttl_amount; 
                $total = number_format($ttl_amount, 2, '.', '');
                ?>
                <?php 
                $total_no_discount = $ttl_wth_discount;
                $ttl_discount = $inventoryPurchaseOrder['InventoryPurchaseOrder']['bulk_discount'] + $d_amount; 
                
                 if($ttl_discount > 0) {
                    $gst_bulk_dis = ($ttl_discount / 100) * 6;
                } else {
                    $gst_bulk_dis = 0;
                }

                $grnd_total = $header_total - $ttl_discount + $calc_gst;
                $grand_format = number_format($grnd_total, 2, '.', '');

               
                
                 ?>
                    <tr>
                        <td rowspan="4" colspan="4" class="text-vertical" style="padding-top: 40px">
                            <h4 class="text-center">
                                <?php
                                    $num = $grand_format;
                                    $test = convertNumber($num);

                                    echo strtoupper($test).' ONLY';
                                ?>    
                            </h4>
                        </td>
                        <td colspan="2" class="text-right">Total <?php echo $currency['iso_code']; ?></td>
                        <td class="text-right"><?php echo number_format($header_total, 2); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right">Discount <?php echo $currency['iso_code']; ?></td>
                        <td class="text-right"><?php 

                        echo number_format($ttl_discount, 2); 
                        $grnd_total = $header_total - $ttl_discount + $calc_gst;
                        ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right">Tax <?php echo $currency['iso_code']; ?></td>
                        <td class="text-right"><?php echo number_format($calc_gst, 2); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-right">Grand Total <?php echo $currency['iso_code']; ?></td>

                       
                        <td class="text-right"><?php echo _n2($grnd_total); ?></td>
                    </tr>
                </table>
            <?php endif; ?> 
            <?php if(!empty($inventoryPurchaseOrder['InventoryPurchaseOrder']['notes'])){ ?>
            <h4><?php echo __('Note :'); ?></h4>
            <p class="wrapper"><?php echo $inventoryPurchaseOrder['InventoryPurchaseOrder']['notes']; ?></p>
            <?php }?>
            <div class="footer" style="margin-top: 20px !important;">
                <h4 class="text-center">"RIGHT THE FIRST TIME &amp; EVERY TIME"</h4>
                <table class="table table-bordered">
                    <tbody>
                         
                        <tr>
                            <td class="fbody">
                            We hereby acknowledge and accept the term &amp;
                            conditions of the purchase order.
                            <br>
                            (Please fax back within 3 working days)
                            <br><br><br>
                            ...............................................................
                            <br>
                            Company's Stamp &amp; Signature
                            <br>
                            </td>
                            <td class="fbody">
                            For and on half of TENAGA SWITCHGEAR SDN BHD
                            <br><br><br><br>
                            ...............................................................
                            <br>
                            Authorized Signatory
                            <br>
                            </td>
                        </tr>
                    </tbody>
                </table>        
            </div>
        </div>
    </div>
</div>
