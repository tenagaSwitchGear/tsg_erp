<style type="text/css">
.line {    
    font-size: 14px;
    border-bottom: 2px dotted #000;
    text-decoration: none;
   
}
.x_content a{
    color: #1E90FF !important;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List PR'), array('action' => 'index'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('List PO'), array('action' => 'create'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Waiting for Approval'), array('action' => 'pending'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class'=>'btn btn-danger btn-sm')); ?>
        <?php echo $this->Html->link(__('Pending'), array('action' => 'pending_2'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Purchased'), array('action' => 'purchased'), array('class'=>'btn btn-default btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Purchase Requisition'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="container table-responsive">
				<dl>
					<dt class="col-sm-2"><?php echo __('PR Number'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_no']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Supplier'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($items[0]['InventorySupplier']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['created']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Dateline'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['dateline']); ?>
						&nbsp;
					</dd>
					<!-- <dt class="col-sm-2"><?php //echo __('Budget'); ?></dt>
					<dd class="col-sm-9">
						: <?php //echo ($budget[0]['account_department_budgets']['name']); ?>
						&nbsp;
					</dd> -->
					<dt class="col-sm-2"><?php echo __('Terms of payment'); ?></dt>
					<dd class="col-sm-9">
						: <?php foreach ($terms as $key => $term) { ?>
							<?php if($key == $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['term_of_payment_id']){
								echo $term;
							} ?>
						<?php } ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Status'); ?></dt>
					<dd  class="col-sm-9">
						: <?php
							if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='0'){
                                echo "Reject";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='1'){
                                echo "Draft";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='2'){
                                echo "Pending";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='3'){
                                echo "Waiting for HOD approval";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='4'){
                                echo "Waiting for GM approval";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='9'){
                                echo "Approved";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='5'){
                                echo "Insufficient Budget";
                            }
                          ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Total Amount'); ?></dt>
					<dd class="col-sm-9">
						: <?php
							$new_amount = 0;
							foreach ($inventoryPurchaseRequisition['InventoryPurchaseRequisitionItem'] as $inventoryPurchaseRequisitionItem){
								$amount = $inventoryPurchaseRequisitionItem['amount'];
								$new_amount = $new_amount + $amount;	
							} ?>
						<strong style="color: blue; font-size: 16px"><?php echo 'RM '.number_format($new_amount, 2); ?></strong>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Remark'); ?></dt>
					<dd class="col-sm-9">
						: <?php foreach ($remark as $key => $remark) { ?>
							<?php if($key == $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['remark_id']){
								echo $remark;
							} ?>
						<?php } ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Warranty/Guarantee'); ?></dt>
					<dd class="col-sm-9">
						: <?php foreach ($warranty as $key => $warranty) { ?>
							<?php if($key == $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['warranty_id']){
								echo $warranty;
							} ?>
						<?php } ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quotation'); ?></dt>
					<dd class="col-sm-9">
						: <?php if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['quotation'] != null) { ?>
                            <?php echo $this->Html->link($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['quotation'], '/files/inventory_purchase_requisition/quotation/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['dir'] . '/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['quotation'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            <p>Not available</p>
                        <?php } ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Approval'); ?></dt>
					<dd class="col-sm-9">
						: <?php if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['approval'] != null) { ?>
                            <?php echo $this->Html->link($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['approval'], '/files/inventory_purchase_requisition/approval/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['dir'] . '/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['approval'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            <p>Not available</p>
                        <?php } ?>
						&nbsp;
					</dd>

					
				</dl>
				<?php if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='5'){ ?>
				<?php echo $this->Html->link(__('Request Budget Transfer'), array('controller'=>'accountdepartmenttransferbudget', 'action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>
				<?php } ?>

			</div>
			
			<div class="clearfix">&nbsp;</div>
			
			<div class="related">
				<h4><?php echo __('Purchase Requisition Items'); ?></h4>
				<?php if (!empty($inventoryPurchaseRequisition['InventoryPurchaseRequisitionItem'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
				<tr>
					<th class="text-center"><?php echo __('#'); ?></th>
					<th><?php echo __('Items'); ?></th>
					<th><?php echo __('Quantity'); ?></th>
					<th><?php echo __('Unit Price'); ?></th>
					<th><?php echo __('Discount'); ?></th>
					<th><?php echo __('Tax'); ?></th>
					<th><?php echo __('Amount'); ?></th>
				</tr>
				<?php $no = '1'; 
				foreach ($items as $inventoryPurchaseRequisitionItem): ?>
					<tr>
						<td class="text-center"><?php echo $no++; ?></td>
						<td>
							<?php echo '<strong>CODE : '.strtoupper($inventoryPurchaseRequisitionItem['InventoryItem']['code']).'</strong>'; ?><br>
							<?php 
							//print_r($inventoryPurchaseRequisitionItem);
							echo '<strong>'.$inventoryPurchaseRequisitionItem['InventoryItem']['name'].'</strong>';
									//echo $this->Html->link($inventoryPurchaseRequisitionItem['InventoryItem']['name'], array('controller'=>'inventory_purchase_requisition_items', 'action' => 'list_history', $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['inventory_item_id']), array('escape'=>false, 'target'=>'_blank', 'class' => 'line'));
									
							?>
							<?php if($_SESSION['Auth']['User']['group_id']=='11'){ ?>
							<?php echo $this->Html->link(('<i class="fa fa-external-link" aria-hidden="true"></i>'), array('controller'=>'inventory_purchase_requisition_items', 'action' => 'index', $inventoryPurchaseRequisitionItem['InventoryItem']['id']), array('escape'=>false, 'target'=>'_blank')); ?>
							<?php } ?>
							
						</td>
						<td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']; ?><br><?php 
								echo $inventoryPurchaseRequisitionItem['GeneralUnit']['name'];
							?></td>
						<td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit']; ?></td>
						<td>
						<?php
						if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type'] == '0'){
							if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'] == '0.0000'){
								echo "-";
							}else{
								echo "RM ".number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 2);
							}
						}else{
							echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 0).' %'; 
						}

						?>
						</td>
						<td>
						<?php
						if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'] == '0'){
							echo "-";
						}else{
							echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'].' %';
						}
						?>
						</td>
						<td>
						<?php 
							echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['amount']; 
						?></td>
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Purchase Requisition Item
				<?php endif; ?>
			</div>
			
			<?php echo $this->Form->create('InventoryPurchaseRequisition', array('class' => 'form-horizontal')); ?>
			<?php echo $this->Form->input('id', array('value' => $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id'])); ?>
			<div class="form-group">
				<label class="col-sm-3"></label>
				<div class="col-sm-9">
					<?php echo $this->Form->submit('Create PO', array('class' => 'btn btn-success pull-right')); ?>	
				</div>
			</div>
			<?php echo $this->Form->end(); ?>

			<div class="clearfix">&nbsp;</div>
			
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

