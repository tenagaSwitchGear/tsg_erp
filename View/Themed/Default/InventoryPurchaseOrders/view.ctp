<style type="text/css">
    .l_right{
        float: right;
        font-size: 16px
    }
    .line {    
        border-bottom: 2px dotted #000;
        text-decoration: none;
    }
</style>
<div class="row"> 
    <div class="col-xs-12"> 
        <?php echo $this->Html->link(__('Back'), array('action' => 'view_verify', $inventoryPurchaseOrder['InventoryPurchaseOrder']['id']), array('class'=>'btn btn-default btn-sm')); ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Purchase Order'); ?></h2>
                <label class="l_right text-danger">Purchase Status : <?php if($inventoryPurchaseOrder['InventoryPurchaseOrder']['purchase_status']=='0'){ if($inventoryPurchaseOrder['InventoryPurchaseOrder']['status']=='8'){ echo 'Waiting for approval'; }else{ echo "Draft"; } }else{ echo "Purchased"; } ?></label>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>
 
                        <?php echo $this->Form->create('InventoryPurchaseOrder', array('class'=>'form-horizontal')); ?>
               
                <h4><?php echo __('Notes :'); ?></h4>
                
                <p><?php
                echo $this->Form->input("id");
                echo $this->Form->input("notes", array("type"=>"textarea", "class"=> "form-control", "value" => $inventoryPurchaseOrder['InventoryPurchaseOrder']['notes'], "label"=> false, "rows" => "10")); ?></p>
              
                <div class="clearfix">&nbsp;</div>
                


                <?php  
                  echo $this->Form->submit('Save Note', array('class' => 'btn btn-success', 'escape'=>false, "name" => "submit"));  
                ?>
                
                <div class="clearfix">&nbsp;</div>
            </div>
            <!-- content end -->
        </div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
function get_address(nilai){
    if(nilai == 'x'){
        var html = '<input class="form-control" type="text" name="name" placeholder="Name"><br>';
        html += '<textarea class="form-control" name="address" placeholder="Address" rows="3"></textarea><br>';
        html += '<input class="form-control" type="text" name="postcode" placeholder="Postcode"><br>';
        html += '<select class="form-control" name="state_id"><option value="0">Select State</option><?php foreach ($states as $key => $state){ ?><option value="<?php echo $key; ?>"><?php echo $state; ?></option><?php } ?></select><br>';
        html += '<select class="form-control" name="country_id"><option value="0">Select Country</option><?php foreach ($countries as $key => $country){ ?><option value="<?php echo $key; ?>" <?php if($key == "124"){ ?>selected<?php } ?>><?php echo $country; ?></option><?php } ?></select><br><br>';
        html += '<strong>CONTACT : </strong><br>';
        html += '<input class="form-control" type="text" name="contact_person" placeholder="Contact Person"><br>';
        html += '<input class="form-control" type="text" name="contact_number" placeholder="Contact Number"><br>';

        $('#textaddress').html(html);
    }else{
        //alert(nilai);
        var getParam = {
            add_id: nilai
        }
        $.ajax({ 
            type: "GET", 
            dataType: 'json',
            data: getParam,
            url: baseUrl + 'inventoryPurchaseOrders/ajaxaddress', 
            success: function(data) { 
                //console.log(data);
                //console.log(address);
                //var state = data[0]['state_id'];
                $('#negeri').val('4');
                var input_id = '';
                input_id += '<input class="form-control" type="text" name="name" value="'+data[0]['name']+'"><br>';
                input_id += '<textarea class="form-control" name="address" placeholder="Address" rows="3">'+data[0]['address']+'</textarea></br>';
                input_id += '<input class="form-control" type="text" name="postcode" value="'+data[0]['postcode']+'"><br>';
                input_id += '<select id="negeri" class="form-control" name="state_id" ><option value="0">Select State</option><?php foreach ($states as $key => $state){ ?><option value="<?php echo $key; ?>"><?php echo $state; ?></option><?php } ?></select><br>';
                input_id += '<select id="negara" class="form-control" name="country_id" id="country_id"><option value="0">Select Country</option><?php foreach ($countries as $key => $country){ ?><option value="<?php echo $key; ?>"><?php echo $country; ?></option><?php } ?></select><br><br>';
                input_id += '<strong>CONTACT: </strong><br>';
                input_id += '<input class="form-control" type="text" name="contact_person" value="'+data[0]['contact_person']+'"><br>';
                input_id += '<input class="form-control" type="text" name="contact_number" value="'+data[0]['contact_number']+'"><br>';                
                
                $('#textaddress').html(input_id);
                $('#negeri').val(data[0]['state_id']);
                $('#negara').val(data[0]['country_id']);
            }
        }); 
        return false;
    }
}

</script>
<?php $this->end(); ?>