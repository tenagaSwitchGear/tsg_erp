<div class="inventoryMaterialIssueds view">
<h2><?php echo __('Inventory Material Issued'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialIssued['InventoryMaterialIssued']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Material Request Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialIssued['InventoryMaterialRequestItem']['id'], array('controller' => 'inventory_material_request_items', 'action' => 'view', $inventoryMaterialIssued['InventoryMaterialRequestItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Stock'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialIssued['InventoryStock']['id'], array('controller' => 'inventory_stocks', 'action' => 'view', $inventoryMaterialIssued['InventoryStock']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialIssued['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialIssued['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Production Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialIssued['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryMaterialIssued['ProductionOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialIssued['InventoryMaterialIssued']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialIssued['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryMaterialIssued['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialIssued['InventoryMaterialIssued']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialIssued['User']['id'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialIssued['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Material Issued'), array('action' => 'edit', $inventoryMaterialIssued['InventoryMaterialIssued']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Material Issued'), array('action' => 'delete', $inventoryMaterialIssued['InventoryMaterialIssued']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialIssued['InventoryMaterialIssued']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Issueds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Issued'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
