<div class="inventoryMaterialIssueds index">
	<h2><?php echo __('Inventory Material Issueds'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_material_request_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_stock_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('production_order_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryMaterialIssueds as $inventoryMaterialIssued): ?>
	<tr>
		<td><?php echo h($inventoryMaterialIssued['InventoryMaterialIssued']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialIssued['InventoryMaterialRequestItem']['id'], array('controller' => 'inventory_material_request_items', 'action' => 'view', $inventoryMaterialIssued['InventoryMaterialRequestItem']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialIssued['InventoryStock']['id'], array('controller' => 'inventory_stocks', 'action' => 'view', $inventoryMaterialIssued['InventoryStock']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialIssued['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialIssued['InventoryItem']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialIssued['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryMaterialIssued['ProductionOrder']['id'])); ?>
		</td>
		<td><?php echo h($inventoryMaterialIssued['InventoryMaterialIssued']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialIssued['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryMaterialIssued['GeneralUnit']['id'])); ?>
		</td>
		<td><?php echo h($inventoryMaterialIssued['InventoryMaterialIssued']['created']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialIssued['User']['id'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialIssued['User']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryMaterialIssued['InventoryMaterialIssued']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryMaterialIssued['InventoryMaterialIssued']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryMaterialIssued['InventoryMaterialIssued']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialIssued['InventoryMaterialIssued']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Inventory Material Issued'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
