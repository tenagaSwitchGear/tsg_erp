<div class="inventoryMaterialIssueds form">
<?php echo $this->Form->create('InventoryMaterialIssued'); ?>
	<fieldset>
		<legend><?php echo __('Add Inventory Material Issued'); ?></legend>
	<?php
		echo $this->Form->input('inventory_material_request_item_id');
		echo $this->Form->input('inventory_stock_id');
		echo $this->Form->input('inventory_item_id');
		echo $this->Form->input('production_order_id');
		echo $this->Form->input('quantity');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Inventory Material Issueds'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Stocks'), array('controller' => 'inventory_stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Stock'), array('controller' => 'inventory_stocks', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
