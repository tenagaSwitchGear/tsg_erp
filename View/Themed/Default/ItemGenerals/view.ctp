<div class="itemGenerals view">
<h2><?php echo __('Item General'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Search 1'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['search_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Search 2'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['search_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Search 3'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['search_3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Search 4'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['search_4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Group'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['item_group']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($itemGeneral['ItemGeneral']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Item General'), array('action' => 'edit', $itemGeneral['ItemGeneral']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Item General'), array('action' => 'delete', $itemGeneral['ItemGeneral']['id']), array(), __('Are you sure you want to delete # %s?', $itemGeneral['ItemGeneral']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Item Generals'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Item General'), array('action' => 'add')); ?> </li>
	</ul>
</div>
