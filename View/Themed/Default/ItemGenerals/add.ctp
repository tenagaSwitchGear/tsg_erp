<div class="itemGenerals form">
<?php echo $this->Form->create('ItemGeneral'); ?>
	<fieldset>
		<legend><?php echo __('Add Item General'); ?></legend>
	<?php
		echo $this->Form->input('code');
		echo $this->Form->input('name');
		echo $this->Form->input('type');
		echo $this->Form->input('search_1');
		echo $this->Form->input('search_2');
		echo $this->Form->input('search_3');
		echo $this->Form->input('search_4');
		echo $this->Form->input('item_group');
		echo $this->Form->input('unit');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Item Generals'), array('action' => 'index')); ?></li>
	</ul>
</div>
