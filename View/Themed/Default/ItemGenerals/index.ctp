<div class="itemGenerals index">
	<h2><?php echo __('Item Generals'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('search_1'); ?></th>
			<th><?php echo $this->Paginator->sort('search_2'); ?></th>
			<th><?php echo $this->Paginator->sort('search_3'); ?></th>
			<th><?php echo $this->Paginator->sort('search_4'); ?></th>
			<th><?php echo $this->Paginator->sort('item_group'); ?></th>
			<th><?php echo $this->Paginator->sort('unit'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($itemGenerals as $itemGeneral): ?>
	<tr>
		<td><?php echo h($itemGeneral['ItemGeneral']['id']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['code']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['name']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['type']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['search_1']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['search_2']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['search_3']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['search_4']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['InventoryItemCategory']['name']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['GeneralUnit']['name']); ?>&nbsp;</td>
		<td><?php echo h($itemGeneral['ItemGeneral']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $itemGeneral['ItemGeneral']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $itemGeneral['ItemGeneral']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $itemGeneral['ItemGeneral']['id']), array(), __('Are you sure you want to delete # %s?', $itemGeneral['ItemGeneral']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Item General'), array('action' => 'add')); ?></li>
	</ul>
</div>
