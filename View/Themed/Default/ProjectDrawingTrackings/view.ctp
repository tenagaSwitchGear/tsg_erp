<?php echo $this->Html->link(__('List Project Drawings'), array('controller' => 'project_drawings', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link($projectDrawingTracking['ProjectDrawing']['name'], array('controller' => 'project_drawings', 'action' => 'view', $projectDrawingTracking['ProjectDrawing']['id']), array('class' => 'btn btn-success')); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View Drawing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="projectDrawingTrackings view-data">
<h2><?php echo __('Project Drawing Tracking'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectDrawingTracking['ProjectDrawingTracking']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Drawing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectDrawingTracking['ProjectDrawing']['name'], array('controller' => 'project_drawings', 'action' => 'view', $projectDrawingTracking['ProjectDrawing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projectDrawingTracking['ProjectDrawingTracking']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($projectDrawingTracking['ProjectDrawingTracking']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($projectDrawingTracking['ProjectDrawingTracking']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div> 
</div>
</div>
</div>
</div>