<?php echo $this->Html->link(__('List Project Drawings'), array('controller' => 'project_drawings', 'action' => 'index'), array('class' => 'btn btn-success')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add Drawing Status</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
      <h4>Drawing Information</h4>
<table class="table table-bordered">
<tr>
<td>
	Job No:
</td>
<td>
	<?php echo $projectDrawings['SaleJobChild']['station_name']; ?>
</td>
</tr> 
<tr>
<td>
	Station:
</td>
<td>
	<?php echo $projectDrawings['SaleJobChild']['name']; ?>
</td>
</tr> 
<tr>
<td>
	Drawing No:
</td>
<td>
	<?php echo $drawing['ProjectDrawingFile']['drawing_no']; ?>
</td>
</tr>
<tr>
<td>
	Drawing Name:
</td>
<td>
	<?php echo $drawing['ProjectDrawingFile']['title']; ?>
</td>
</tr>
<tr>
<td>
	Product:
</td>
<td>
	<?php echo $drawing['ProjectDrawingFile']['product']; ?>
</td>
</tr>

<tr>
<td>
	Status:
</td>
<td>
	<?php echo $drawing['ProjectDrawingFile']['status']; ?>
</td>
</tr> 

</table>
<?php echo $this->Session->flash(); ?>

<h4><?php echo __('Add Drawing Status'); ?></h4>
<p>Please fill form below to add status.</p>
	<?php echo $this->Form->create('ProjectDrawingTracking', array('class' => 'form-horizontal')); ?> 
	<?php echo $this->Form->input('project_drawing_id', array('type' => 'hidden', 'value' => $id)); ?>  
	<div class="form-group">
	<label class="col-sm-3">Remark (Optional)</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?> 
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3">Date Submitted/Received</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('date_submit', array('type' => 'text', 'class' => 'form-control', 'id' => 'datepicker', 'label' => false)); ?> <?php echo $this->Form->input('project_drawing_file_id', array('type' => 'hidden', 'value' => $id)); ?>
	</div>
	</div>
	 
	<div class="form-group">
	<label class="col-sm-3">Status</label>
	<div class="col-sm-9">
	<?php $status = array(
		'Draft' => 'Draft',
		'Submitted' => 'Submitted',
		'Receive From Client' => 'Receive From Client',
		'Rejected' => 'Rejected',
		'Revise & Resubmitted' => 'Revise & Resubmitted',
		'Approved' => 'Approved',
		'Approved (Info only)' => 'Approved (Info only)',
		);
		?>
	<?php echo $this->Form->input('status', array('options' => $status, 'empty' => '-Select Status-', 'class' => 'form-control', 'label' => false)); ?> 
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3">&nbsp;</label>
	<div class="col-sm-9">
		<?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
	</div>
	</div>
<?php echo $this->Form->end(); ?>
 

	<h4><?php echo __('Drawing Status'); ?></h4>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th>Status</th>
			<th>Created</th>
			<th>Remark</th>
			<th>User</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($trackings as $projectDrawingTracking): ?>
	<tr>  
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['status']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['created']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['remark']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['User']['username']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectDrawingTracking['ProjectDrawingTracking']['id']), array(), __('Are you sure you want to delete # %s?', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
 	</tbody>
 	</table>
</div>
</div>
</div>
</div>
</div>
