<div class="projectDrawingTrackings form">
<?php echo $this->Form->create('ProjectDrawingTracking'); ?>
	<fieldset>
		<legend><?php echo __('Edit Project Drawing Tracking'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('project_drawing_id');
		echo $this->Form->input('remark');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProjectDrawingTracking.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProjectDrawingTracking.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Project Drawing Trackings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Drawings'), array('controller' => 'project_drawings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Drawing'), array('controller' => 'project_drawings', 'action' => 'add')); ?> </li>
	</ul>
</div>
