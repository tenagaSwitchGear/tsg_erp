<div class="projectDrawingTrackings index">
	<h2><?php echo __('Project Drawing Trackings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('project_drawing_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('remark'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectDrawingTrackings as $projectDrawingTracking): ?>
	<tr>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projectDrawingTracking['ProjectDrawing']['name'], array('controller' => 'project_drawings', 'action' => 'view', $projectDrawingTracking['ProjectDrawing']['id'])); ?>
		</td>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['created']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['remark']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectDrawingTracking['ProjectDrawingTracking']['id']), array(), __('Are you sure you want to delete # %s?', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project Drawing Tracking'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Drawings'), array('controller' => 'project_drawings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Drawing'), array('controller' => 'project_drawings', 'action' => 'add')); ?> </li>
	</ul>
</div>
