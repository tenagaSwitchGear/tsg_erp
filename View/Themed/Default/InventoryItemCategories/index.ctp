<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Inventory Item Categories'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Inventory Item'), array('controller'=>'inventory_items', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Inventory Item Categories'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
				<thead>
				<tr>
					<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php 
					$currentPage = empty($this->Paginator->params['paging']['InventoryItemCategory']['page']) ? 1 : $this->Paginator->params['paging']['InventoryItemCategory']['page']; $limit = $this->Paginator->params['paging']['InventoryItemCategory']['limit'];
					$startSN = (($currentPage * $limit) + 1) - $limit;

					foreach ($inventoryItemCategories as $inventoryItemCategory):
					?>
				<tr>
					<td class="text-center"><?php echo $startSN++; ?></td>
					<td><?php echo h($inventoryItemCategory['InventoryItemCategory']['name']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryItemCategory['InventoryItemCategory']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryItemCategory['InventoryItemCategory']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryItemCategory['InventoryItemCategory']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryItemCategory['InventoryItemCategory']['name']).'"', $inventoryItemCategory['InventoryItemCategory']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
