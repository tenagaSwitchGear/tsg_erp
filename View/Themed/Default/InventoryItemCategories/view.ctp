<div class="row"> 
  	<div class="col-xs-12">
  		
  		<?php echo $this->Html->link(__('New Inventory Item Categories'), array('controller' => 'inventoryItemCategories', 'action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Inventory Categories'), array('controller' => 'inventoryItemCategories', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Inventory Item Category'); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
			<div class="container table-responsive">
				<dl>
					<dt class="col-sm-2"><?php echo __('Id'); ?></dt>
					<dd class="col-sm-9">
						: #<?php echo h($inventoryItemCategory['InventoryItemCategory']['id']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Name'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($inventoryItemCategory['InventoryItemCategory']['name']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="related">
				<h4><?php echo __('Related Inventory Items'); ?></h4>
				<?php if (!empty($inventoryItemCategory['InventoryItem'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
				<tr>
					<th><?php echo __('Id'); ?></th>
					<th><?php echo __('Name'); ?></th>
					<th><?php echo __('Quantity'); ?></th>
					<th><?php echo __('Unit Price'); ?></th>
					<th><?php echo __('Inventory Item Category Id'); ?></th>
					<th><?php echo __('General Unit Id'); ?></th>
					<th><?php echo __('Code'); ?></th>
					<th><?php echo __('Qc Inspect'); ?></th>
					<th><?php echo __('Quantity Shortage'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				<?php foreach ($inventoryItemCategory['InventoryItem'] as $inventoryItem): ?>
					<tr>
						<td><?php echo $inventoryItem['id']; ?></td>
						<td><?php echo $inventoryItem['name']; ?></td>
						<td><?php echo $inventoryItem['quantity']; ?></td>
						<td><?php echo $inventoryItem['unit_price']; ?></td>
						<td><?php echo $inventoryItem['inventory_item_category_id']; ?></td>
						<td><?php echo $inventoryItem['general_unit_id']; ?></td>
						<td><?php echo $inventoryItem['code']; ?></td>
						<td><?php echo $inventoryItem['qc_inspect']; ?></td>
						<td><?php echo $inventoryItem['quantity_shortage']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'inventory_items', 'action' => 'view', $inventoryItem['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'inventory_items', 'action' => 'edit', $inventoryItem['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'inventory_items', 'action' => 'delete', $inventoryItem['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryItem['name']).'"', $inventoryItem['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Inventory Item
				<?php endif; ?>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
