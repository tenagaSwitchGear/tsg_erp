<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Inventory Item Categories'), array('controller' => 'inventoryItemCategories', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Edit Inventory Item Category'); ?></h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
				<div class="inventoryItemCategories form">
				<?php echo $this->Form->create('InventoryItemCategory', array('class' => 'form-horizontal')); ?>
					<fieldset>
					<?php echo $this->Form->input('id'); ?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Name</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("name", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Price Book (Use On PR)</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("require_price_book", array("class"=> "form-control", "options"=> array(1 => 'Required', 0 => 'Not Required'), "label"=> false)); ?>
						<small>Some category such as Service not require price book when create Purchase Requisition. So, end user need to enter price manually.</small>
						</div> 
					</div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>
