<div class="row"> 
  	<div class="col-xs-12">
    
        <div class="x_panel tile">
      		<div class="x_title">
        		<h2>View: <?php echo h($finishedGoodPackaging['SaleJobChild']['station_name']); ?> - <?php echo h($finishedGoodPackaging['SaleJobChild']['name']); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	
<div class="finishedGoodPackagings view-data">
<h2><?php echo __('Packaging'); ?></h2>
	<dl>  
		<dt><?php echo __('Sale Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodPackaging['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGoodPackaging['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Job Child'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodPackaging['SaleJobChild']['name'], array('controller' => 'sale_job_children', 'action' => 'view', $finishedGoodPackaging['SaleJobChild']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodPackaging['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedGoodPackaging['Customer']['id'])); ?>
			&nbsp;
		</dd> 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackaging['FinishedGoodPackaging']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackaging['FinishedGoodPackaging']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackaging['FinishedGoodPackaging']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackaging['FinishedGoodPackaging']['remark']); ?>
			&nbsp;
		</dd>
	</dl>
</div> 

<div class="related">
	<h3><?php echo __('Packaging Items'); ?></h3>
	<?php if (!empty($finishedGoodPackagingItem)): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr> 
		<th><?php echo __('Production Order'); ?></th>
		<th><?php echo __('Serial'); ?></th>
	</tr>
	<?php foreach ($finishedGoodPackagingItem as $finishedGoodPackagingItem): ?>
		<tr> 
			<td><strong><?php echo $finishedGoodPackagingItem['FinishedGood']['production_order_name']; ?></strong>
				<br>
				<?php echo $finishedGoodPackagingItem['FinishedGood']['bom_name']; ?>
				<br>
				<?php echo $finishedGoodPackagingItem['FinishedGood']['bom_code']; ?>
			</td>
			<td><?php echo $finishedGoodPackagingItem['FinishedGood']['serial_no']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>
<div class="related">
	<h3><?php echo __('Packaging Boxs'); ?></h3>
	<?php if (!empty($finishedGoodPackagingBox)): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr> 
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Measurement'); ?></th>
		<th><?php echo __('Weight'); ?></th>
		<th><?php echo __('Nett Weight'); ?></th>
		<th><?php echo __('Remark'); ?></th>
	</tr>
	<?php foreach ($finishedGoodPackagingBox as $finishedGoodPackagingBox): ?>
		<tr> 
			<td><?php echo $finishedGoodPackagingBox['FinishedGoodPackagingBox']['name']; ?></td>
			<td><?php echo $finishedGoodPackagingBox['FinishedGoodPackagingBox']['measurement']; ?></td>
			<td><?php echo $finishedGoodPackagingBox['FinishedGoodPackagingBox']['weight']; ?></td>
			<td><?php echo $finishedGoodPackagingBox['FinishedGoodPackagingBox']['nett_weight']; ?></td>
			<td><?php echo nl2br($finishedGoodPackagingBox['FinishedGoodPackagingBox']['remark']); ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
	<?php endif; ?>
</div>

</div>
</div>
</div>
</div>