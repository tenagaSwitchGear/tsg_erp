<?php echo $this->Html->link(__('Packaging Lists'), array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?> 
<?php echo $this->Html->link(__('Ready For Packaging'), array('action' => 'ready'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Tracking'), array('action' => 'waiting'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Add New'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
    
        <div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Packaging Lists'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
  
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr>  
			
			<th><?php echo $this->Paginator->sort('SaleJobChild.name', 'Station'); ?></th>
			<th><?php echo $this->Paginator->sort('SaleJobChild.station_name', 'Station Code'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_job_id', 'Job'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th> 
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('remark'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($finishedGoodPackagings as $finishedGoodPackaging): ?>
	<tr>  
		<td>
			<?php echo $this->Html->link($finishedGoodPackaging['SaleJobChild']['name'], array('controller' => 'sale_job_childs', 'action' => 'view', $finishedGoodPackaging['SaleJobChild']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($finishedGoodPackaging['SaleJobChild']['station_name'], array('controller' => 'sale_job_childs', 'action' => 'view', $finishedGoodPackaging['SaleJobChild']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($finishedGoodPackaging['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGoodPackaging['SaleJob']['id'])); ?>
		</td>
		
		<td>
			<?php echo $this->Html->link($finishedGoodPackaging['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedGoodPackaging['Customer']['id'])); ?>
		</td> 
		<td><?php echo h($finishedGoodPackaging['FinishedGoodPackaging']['created']); ?>&nbsp;</td> 
		<td>
			<?php echo $this->Html->link($finishedGoodPackaging['User']['id'], array('controller' => 'users', 'action' => 'view', $finishedGoodPackaging['User']['id'])); ?>
		</td>
		<td><?php echo h($finishedGoodPackaging['FinishedGoodPackaging']['status']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodPackaging['FinishedGoodPackaging']['remark']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $finishedGoodPackaging['FinishedGoodPackaging']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $finishedGoodPackaging['FinishedGoodPackaging']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $finishedGoodPackaging['FinishedGoodPackaging']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodPackaging['FinishedGoodPackaging']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
	<?php
	  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
	  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	?>
	</ul>
</div>
 
</div>
</div>
</div>