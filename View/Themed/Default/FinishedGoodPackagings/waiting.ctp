<?php echo $this->Html->link(__('Packaging Lists'), array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?> 
<?php echo $this->Html->link(__('Ready For Packaging'), array('action' => 'ready'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Tracking'), array('action' => 'waiting'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Add New'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
        <div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Waiting Packaging'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th>Job No</th>
			<th><?php echo $this->Paginator->sort('station_name', 'Station'); ?></th> 
			<th><?php echo $this->Paginator->sort('name', 'Station Name'); ?></th>
			<th>Qty Order</th> 
			<th>Ready To Pack</th> 
			
			<th>Customer</th> 
			<th>Delivery Date</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead> 
	<tbody>
	<?php foreach ($jobs as $finishedGoodPackaging): ?>
	<tr> 
		<td>
			<?php echo $this->Html->link($finishedGoodPackaging['job']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGoodPackaging['job']['id'])); ?>
		</td>
		<td><?php echo h($finishedGoodPackaging['station_name']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodPackaging['name']); ?>&nbsp;</td>
		<td><?php echo $finishedGoodPackaging['items']; ?>&nbsp;</td>
 
		<td><?php echo h($finishedGoodPackaging['finished_good']); ?>&nbsp;</td> 
		
		<td><?php 
			if($finishedGoodPackaging['customer']) {
				echo $this->Html->link($finishedGoodPackaging['customer']['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $finishedGoodPackaging['customer']['Customer']['id']));
			}  
			?> 
		</td> 
		  
		<td><?php echo h($finishedGoodPackaging['delivery_date']); ?>&nbsp;</td>  
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $finishedGoodPackaging['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $finishedGoodPackaging['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $finishedGoodPackaging['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodPackaging['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
</div>
</div>
</div>