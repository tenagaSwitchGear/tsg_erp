<div class="finishedGoodPackagings form">
<?php echo $this->Form->create('FinishedGoodPackaging'); ?>
	<fieldset>
		<legend><?php echo __('Edit Finished Good Packaging'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('sale_job_id');
		echo $this->Form->input('sale_job_child_id');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('production_order_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('status');
		echo $this->Form->input('remark');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FinishedGoodPackaging.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('FinishedGoodPackaging.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Good Packagings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Jobs'), array('controller' => 'sale_jobs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job'), array('controller' => 'sale_jobs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Job Children'), array('controller' => 'sale_job_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Child'), array('controller' => 'sale_job_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Packaging Items'), array('controller' => 'finished_good_packaging_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Packaging Item'), array('controller' => 'finished_good_packaging_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
