<?php echo $this->Html->link(__('Packaging Lists'), array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?> 
<?php echo $this->Html->link(__('Ready For Packaging'), array('action' => 'ready'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Tracking'), array('action' => 'waiting'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Add New'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
        <div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Ready For Packaging'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->					
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
				<thead>
    				<tr> 
    					<th><?php echo $this->Paginator->sort('serial_no'); ?></th>
                        <th><?php echo $this->Paginator->sort('SaleJobChild.station_name', 'Job No'); ?></th>
                        <th><?php echo $this->Paginator->sort('SaleJobChild.name', 'Station'); ?></th>
                        <th><?php echo $this->Paginator->sort('InventoryItem.code', 'Item'); ?></th>
                        <th><?php echo $this->Paginator->sort('status', 'QC Status'); ?></th>
                        <th><?php echo $this->Paginator->sort('fat_status', 'FAT Status'); ?></th>
                        <th><?php echo $this->Paginator->sort('final_inspect'); ?></th>
                        <th><?php echo $this->Paginator->sort('SaleJobChild.delivery_date', 'Delivery Date'); ?></th>
    					<th><?php echo $this->Paginator->sort('created'); ?></th> 
    				</tr>
				</thead>
				<tbody>
				<?php  
				foreach ($finalGoods as $finishedGood): ?>
				<tr> 
                    <td><?php echo $finishedGood['FinishedGood']['serial_no']; ?></td>
					<td>
						<?php echo $this->Html->link($finishedGood['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $finishedGood['SaleJobChild']['sale_job_id'])); ?>
					</td>
                    <td>
						<?php echo $finishedGood['SaleJobChild']['name']; ?>
					</td>
                    <td><?php echo h($finishedGood['InventoryItem']['code']); ?><br/>
                    <small><?php echo h($finishedGood['InventoryItem']['name']); ?></small>
                    </td>
                    <td><?php echo $finishedGood['FinishedGood']['status']; ?> 
                    </td>
                    <td><?php echo $finishedGood['FinishedGood']['fat_status']; ?>  
                    </td>
                    <td><?php echo $finishedGood['FinishedGood']['final_inspect']; ?>  
                    </td>
                    <td><?php echo $finishedGood['SaleJobChild']['delivery_date']; ?>  
                    </td>


					<td><?php echo h($finishedGood['FinishedGood']['created']); ?>&nbsp;</td>

					 
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div> 