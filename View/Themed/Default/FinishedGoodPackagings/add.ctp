<?php echo $this->Html->link(__('Packaging Lists'), array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?> 
<?php echo $this->Html->link(__('Ready For Packaging'), array('action' => 'ready'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Tracking'), array('action' => 'waiting'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Add New'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
    
        <div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add Packing'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
<h4>Note</h4>
<p>Please fill this form after packing process has been completed. Packaging only referrence for transportation and delivery purpose.</p>

<div class="finishedGoodPackagings form">
<?php echo $this->Form->create('FinishedGoodPackaging', array('class' => 'form-horizontal form-label-left')); ?>
	  
		<div class="form-group">
	    	<label class="col-sm-3">Job No / Station *</label>
	    	<div class="col-sm-9">
    	<?php echo $this->Form->input('find_job', array('id' => 'find_job', 'placeholder' => 'Job No', 'class' => 'form-control', 'label' => false)); ?>
		<?php echo $this->Form->input('sale_job_id', array('type' => 'hidden', 'id' => 'sale_job_id', 'label' => false)); ?>
		 
		<?php echo $this->Form->input('sale_job_child_id', array('type' => 'hidden', 'id' => 'sale_job_child_id', 'label' => false)); ?>

		<?php echo $this->Form->input('customer_id', array('type' => 'hidden', 'id' => 'customer_id', 'label' => false)); ?>
		</div>
		</div>
 
		
		
		<div class="form-group">
	    	<label class="col-sm-3">Note / Remark</label>
	    	<div class="col-sm-9">
		<?php echo $this->Form->input('remark', array('required' => false, 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>
		<div class="form-group">
	    	<label class="col-sm-3">Status *</label>
	    	<div class="col-sm-9">
	    	<?php $status = array('Draft' => 'Draft', 'Save To Finished Good' => 'Save To Finished Good'); ?>
			<?php echo $this->Form->input('status', array('empty' => 'Select Status', 'options' => $status, 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>

		<h4>Packaging Section</h4> 
		<p>Packaging is only referrence for transportation and delivery purpose.</p>
		<div id="loadMoreBom"></div>  
        <div class="form-group">
            <div class="col-sm-12"><a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-cube"></i> Add Packaging</a></div>
        </div>  

        <hr/>
        <h4>Finished Goods Section</h4>
        <p>Once finished goods added to this form, it will marked as Completed on reporting section.</p>
        <div id="loadFinishedGood"></div>  
        <div class="form-group">
            <div class="col-sm-12"><a href="#" id="addFinishedGood" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Finished Good</a></div>
        </div> 


	 	<div class="form-group">
        <label class="col-sm-3">&nbsp;</label>
        <div class="col-sm-9">
            <?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
        </div>
			</div> 
			 
		<?php echo $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).ready(function() {
   $('#find_job').autocomplete({ 
	    source: function (request, response){ 
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_jobs/ajaxfindjob',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#find_job').val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    sale_job_child_id: item.sale_job_child_id,
		                    value: item.name,
		                    station: item.station,
		                    customer: item.customer,
		                    customer_id: item.customer_id,
		                    sale_tender_id: item.sale_tender_id,
		                    sale_quotation_id: item.sale_quotation_id, 
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {   
	        $('#sale_job_id').val(ui.item.id); 
	        $('#sale_job_child_id').val(ui.item.sale_job_child_id);
	        $('#customer_id').val(ui.item.customer_id);  	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.station + "</small><br/><small>" + item.customer + "</small><br>" +  "</div>" ).appendTo( ul );
    };

});  
</script>
<?php $this->end(); ?>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
	console.log(search);
	$('#findProduct'+row).autocomplete({ 
	    source: function (request, response) { 
	    	if($('#sale_job_child_id').val() == '') {
	    		alert('Please enter valid Job Number');
	    	}
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'finished_goods/ajaxfindfinishedgood',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#findProduct'+row).val() + "&sale_job_child_id=" + $('#sale_job_child_id').val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.name,
		                    name : item.name,
		                    sale_job_item_id: item.sale_job_item_id,
		                    inventory_item_code: item.inventory_item_code,
		                    inventory_item_name: item.inventory_item_name,
		                    sale_job_child_id: item.sale_job_child_id,
		                    station_name: item.station_name,

		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {  
	        $('#fgId'+row).val( ui.item.id ); 
	        $('#jobItemId'+row).val( ui.item.sale_job_item_id ); 
	        $('#itemName'+row).val( ui.item.inventory_item_code );   
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.name + "<br><small>" + item.inventory_item_name + "</small><br/><small>" + item.station_name + "</small><br>" +  "</div>" ).appendTo( ul );
    };
}
  
 
$(function() {  

	var station = 1;
	$('#addItem').click(function() {
		var html = '<div id="removeBom'+station+'">'; 
	    html += '<div class="form-group">';  
	    html += '<div class="col-sm-2" id="autoComplete">';
	    html += '<input type="text" name="box_name[]" class="form-control findProduct" placeholder="Box name/No/Code"required>'; 
	    html += '</div>'; 
	    html += '<div class="col-sm-2"><input type="text" class="form-control" name="box_size[]" placeholder="Size: L x W x H (Ie: 13 x 10 x 8 Feet)">';
	    html += '</div>'; 
        html += '<div class="col-sm-2"><input type="text" class="form-control" name="box_weight[]" placeholder="Weight (Ie: 5.3 Tonnes)">';
	    html += '</div>';
        html += '<div class="col-sm-2"><input type="text" class="form-control" name="box_nett_weight[]" placeholder="Nett Weight (Ie: 5.3 Tonnes)">';
        html += '</div>';
	    html += '<div class="col-sm-1">';
	    html += '<input type="text" name="box_qty[]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
	    html += '</div>'; 
        html += '<div class="col-sm-2"><textarea class="form-control" name="remark[]" placeholder="Remark"></textarea>';
        html += '</div>';
	    html += '<div class="col-sm-1">';
	    html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div>'; 
	    html += '</div></div>';    
	    $("#loadMoreBom").append(html);   
		station++; 
	});  

	var fg = 1;
	$('#addFinishedGood').click(function() {
		var html = '<div id="removeFinishedGood'+fg+'">'; 
	    html += '<div class="form-group">';  
	    html += '<div class="col-sm-5" id="autoComplete">';
	    html += '<input type="text" name="findFg" id="findProduct'+fg+'" class="form-control findProduct" placeholder="Serial No"required>'; 
	     html += '<input type="hidden" name="finished_good_id[]" id="fgId'+fg+'" class="form-control findProduct" placeholder="Serial No"required>';
	     html += '<input type="hidden" name="sale_job_item_id[]" id="jobItemId'+fg+'" class="form-control findProduct" placeholder="Serial No"required>'; 
	    html += '</div>'; 
	    html += '<div class="col-sm-6"><input type="text" class="form-control" id="itemName'+fg+'" placeholder="Item Name"readonly>'; 
	    html += '</div>';   
	    html += '<div class="col-sm-1">';
	    html += '<a href="#" class="btn btn-danger" onclick="removeFinishedGood('+fg+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div>'; 
	    html += '</div></div>';    
	    $("#loadFinishedGood").append(html);  
	    
		findItem(fg, $(this).val());  
		fg++; 
	});

});
</script>
<?php $this->end(); ?>