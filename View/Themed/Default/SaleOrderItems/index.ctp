<div class="saleOrderItems index">
	<h2><?php echo __('Sale Order Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('unit_price'); ?></th>
			<th><?php echo $this->Paginator->sort('discount'); ?></th>
			<th><?php echo $this->Paginator->sort('tax'); ?></th>
			<th><?php echo $this->Paginator->sort('total_price'); ?></th>
			<th><?php echo $this->Paginator->sort('product_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_order_id'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleOrderItems as $saleOrderItem): ?>
	<tr>
		<td><?php echo h($saleOrderItem['SaleOrderItem']['id']); ?>&nbsp;</td>
		<td><?php echo h($saleOrderItem['SaleOrderItem']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleOrderItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleOrderItem['GeneralUnit']['id'])); ?>
		</td>
		<td><?php echo h($saleOrderItem['SaleOrderItem']['unit_price']); ?>&nbsp;</td>
		<td><?php echo h($saleOrderItem['SaleOrderItem']['discount']); ?>&nbsp;</td>
		<td><?php echo h($saleOrderItem['SaleOrderItem']['tax']); ?>&nbsp;</td>
		<td><?php echo h($saleOrderItem['SaleOrderItem']['total_price']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleOrderItem['Product']['name'], array('controller' => 'products', 'action' => 'view', $saleOrderItem['Product']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleOrderItem['SaleOrder']['id'], array('controller' => 'sale_orders', 'action' => 'view', $saleOrderItem['SaleOrder']['id'])); ?>
		</td>
		<td><?php echo h($saleOrderItem['SaleOrderItem']['type']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleOrderItem['SaleOrderItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleOrderItem['SaleOrderItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleOrderItem['SaleOrderItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleOrderItem['SaleOrderItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
