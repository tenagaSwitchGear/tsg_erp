<div class="saleOrderItems view">
<h2><?php echo __('Sale Order Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleOrderItem['SaleOrderItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($saleOrderItem['SaleOrderItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrderItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleOrderItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit Price'); ?></dt>
		<dd>
			<?php echo h($saleOrderItem['SaleOrderItem']['unit_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Discount'); ?></dt>
		<dd>
			<?php echo h($saleOrderItem['SaleOrderItem']['discount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax'); ?></dt>
		<dd>
			<?php echo h($saleOrderItem['SaleOrderItem']['tax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Price'); ?></dt>
		<dd>
			<?php echo h($saleOrderItem['SaleOrderItem']['total_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrderItem['Product']['name'], array('controller' => 'products', 'action' => 'view', $saleOrderItem['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrderItem['SaleOrder']['id'], array('controller' => 'sale_orders', 'action' => 'view', $saleOrderItem['SaleOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($saleOrderItem['SaleOrderItem']['type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Order Item'), array('action' => 'edit', $saleOrderItem['SaleOrderItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Order Item'), array('action' => 'delete', $saleOrderItem['SaleOrderItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleOrderItem['SaleOrderItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
