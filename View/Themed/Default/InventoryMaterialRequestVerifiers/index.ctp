<div class="inventoryMaterialRequestVerifiers index">
	<h2><?php echo __('Inventory Material Request Verifiers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('group_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryMaterialRequestVerifiers as $inventoryMaterialRequestVerifier): ?>
	<tr>
		<td><?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id']); ?>&nbsp;</td>
		<td><?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['group_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialRequestVerifier['User']['id'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequestVerifier['User']['id'])); ?>
		</td>
		<td><?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['modified']); ?>&nbsp;</td>
		<td><?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Verifier'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Requests'), array('controller' => 'inventory_material_requests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request'), array('controller' => 'inventory_material_requests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
