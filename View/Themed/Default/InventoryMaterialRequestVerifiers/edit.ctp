<div class="inventoryMaterialRequestVerifiers form">
<?php echo $this->Form->create('InventoryMaterialRequestVerifier'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Material Request Verifier'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('group_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventoryMaterialRequestVerifier.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventoryMaterialRequestVerifier.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Verifiers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Requests'), array('controller' => 'inventory_material_requests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request'), array('controller' => 'inventory_material_requests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
