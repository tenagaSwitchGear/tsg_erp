<div class="inventoryMaterialRequestVerifiers view">
<h2><?php echo __('Inventory Material Request Verifier'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Group Id'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['group_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequestVerifier['User']['id'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialRequestVerifier['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Material Request Verifier'), array('action' => 'edit', $inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Material Request Verifier'), array('action' => 'delete', $inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialRequestVerifier['InventoryMaterialRequestVerifier']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Verifiers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Verifier'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Requests'), array('controller' => 'inventory_material_requests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request'), array('controller' => 'inventory_material_requests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
