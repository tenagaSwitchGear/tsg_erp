<?php echo $this->Html->link(__('Store Verifier List'), array('action' => 'index'), array('class'=>'btn btn-info')); ?>

<div class="row">  
  	<div class="col-xs-12"> 
  		
 
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add Store Verifier'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 

<div class="inventoryMaterialRequestVerifiers form">
<?php echo $this->Form->create('InventoryMaterialRequestVerifier'); ?>
	 
	<?php echo $this->Form->input('group_id'); ?>
	<?php echo $this->Form->input('user_id'); ?> 
	<?php echo $this->Form->input('status'); ?> 
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
 
</div>
</div> 
</div>
</div>