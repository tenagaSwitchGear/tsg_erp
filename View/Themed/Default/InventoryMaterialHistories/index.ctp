<?php echo $this->Html->link(__('Material Transfer History'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('History'), array('action' => 'index/1'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Request Material'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Transfer History</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<div class="inventoryMaterialHistories index"> 
	<?php echo $this->Form->create('inventoryMaterialHistory', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('item', array('placeholder' => 'Item name / code', 'class' => 'form-control', 'label' => false, 'id' => 'findProduct')); ?> 
		<?php echo $this->Form->input('item_id', array('type' => 'hidden', 'id' => 'item_id')); ?>
		</td>
		<td><?php echo $this->Form->input('username', array('type' => 'text', 'placeholder' => 'Req PIC', 'class' => 'form-control', 'label' => false, 'id' => 'username')); ?> 
		<?php echo $this->Form->input('user_id', array('type' => 'hidden', 'id' => 'user_id')); ?>
		</td>
		<td><?php echo $this->Form->input('store', array('placeholder' => 'Store PIC', 'class' => 'form-control', 'label' => false, 'id' => 'username2')); ?> 
		<?php echo $this->Form->input('store_pic', array('type' => 'hidden', 'id' => 'store_pic')); ?>
		</td>
		<td><?php echo $this->Form->input('type', array('options' => $type, 'class' => 'form-control', 'label' => false, 'empty' => 'All Type')); ?> 
		<?php echo $this->Form->input('store_pic', array('type' => 'hidden', 'id' => 'store_pic')); ?>
		</td>
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('inventory_item_id', 'Code'); ?></th> 
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('store_pic'); ?></th> 

            <th><?php echo $this->Paginator->sort('issued_quantity', 'Qty Out'); ?></th>

            <th><?php echo $this->Paginator->sort('issued_quantity', 'Qty In'); ?></th>

			<th><?php echo $this->Paginator->sort('quantity_balance', 'Stock Bal.'); ?></th>
			
			
			<th><?php echo $this->Paginator->sort('inventory_material_request_id', 'Ref'); ?></th> 
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryMaterialHistories as $inventoryMaterialHistory): ?>

    <?php $type = $inventoryMaterialHistory['InventoryMaterialHistory']['transfer_type']; ?>
	<tr>
		<td><?php echo $inventoryMaterialHistory['InventoryItem']['code']; ?><br/>
        <small><?php echo $this->Html->link($inventoryMaterialHistory['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialHistory['InventoryItem']['id'])); ?></small>
        </td>
        
		<td>
			<?php echo $this->Html->link($inventoryMaterialHistory['User']['username'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialHistory['User']['id'])); ?>
		</td>
		<td><?php echo h($inventoryMaterialHistory['StorePic']['username']); ?>&nbsp;</td>
		
         <td><?php 
         // Out : 1:Issued, 2:Warehouse transfer, 3: location transfer 4: code transfer, 
         // In: 5: RMA, 6: grn, 7  
            if($type == 'Out') { 
                echo _n2($inventoryMaterialHistory['InventoryMaterialHistory']['issued_quantity']); 
            }
         ?>
         </td>

         <td><?php  
            if($type == 'In') { 
                echo _n2($inventoryMaterialHistory['InventoryMaterialHistory']['issued_quantity']); 
            }
         ?>
         </td>
         
		<td><?php echo _n2($inventoryMaterialHistory['InventoryMaterialHistory']['quantity_balance']); ?>&nbsp;</td>
		
		<td>
            <?php echo h($inventoryMaterialHistory['InventoryMaterialHistory']['reference']); ?>&nbsp;<br/> 
			<?php 
            //echo $this->Html->link($inventoryMaterialHistory['InventoryMaterialRequest']['code'], array('controller' => 'inventory_material_requests', 'action' => 'view', $inventoryMaterialHistory['InventoryMaterialRequest']['id'])); 
            ?>
		</td> 
		<td><?php echo h($inventoryMaterialHistory['InventoryMaterialHistory']['created']); ?>&nbsp;</td> 
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryMaterialHistory['InventoryMaterialHistory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryMaterialHistory['InventoryMaterialHistory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryMaterialHistory['InventoryMaterialHistory']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialHistory['InventoryMaterialHistory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
 
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
 

</div>
</div>
</div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$( function() {

	$('#username').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'users/ajaxfinduser',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#username').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.username,
                            username: item.name 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#user_id').val( ui.item.id );  
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small></div>" ).appendTo( ul );
    };

    $('#username2').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'users/ajaxfinduser',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#username2').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.username,
                            name: item.name 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#store_pic').val( ui.item.id );  
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small></div>" ).appendTo( ul );
    };

    $('#findProduct').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
 
                url:baseUrl + 'inventory_items/ajaxfinditem',    
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.name,
                            code: item.code,
                            note: item.note 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#item_id').val( ui.item.id );  
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.code + "</small></div>" ).appendTo( ul );
    };

    $('#dateonly2').datepicker({
      dateFormat: 'yy-mm-dd', 
      ampm: true
    }); 
  });  
</script>
<?php $this->end(); ?>