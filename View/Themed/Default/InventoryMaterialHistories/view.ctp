<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Material Transfer History</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryMaterialHistories view-data">
<h2><?php echo __('Inventory Material History'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialHistory['InventoryMaterialHistory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialHistory['User']['username'], array('controller' => 'users', 'action' => 'view', $inventoryMaterialHistory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Store Pic'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialHistory['StorePic']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item Code'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialHistory['InventoryItem']['code'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialHistory['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialHistory['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialHistory['InventoryItem']['id'])); ?>
			&nbsp;
		</dd> 

		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialHistory['InventoryMaterialHistory']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Issued Quantity'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialHistory['InventoryMaterialHistory']['issued_quantity']); ?>
			&nbsp;
		</dd>
	 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialHistory['InventoryMaterialHistory']['created']); ?>
			&nbsp;
		</dd>
		 
	</dl>
</div>
 
</div>
</div>
</div>
</div>
