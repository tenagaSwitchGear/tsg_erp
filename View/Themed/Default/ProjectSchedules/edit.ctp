<div class="projectSchedules form">
<?php echo $this->Form->create('ProjectSchedule'); ?>
	<fieldset>
		<legend><?php echo __('Edit Project Schedule'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('project_bom_id');
		echo $this->Form->input('status');
		echo $this->Form->input('progress');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProjectSchedule.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProjectSchedule.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Project Schedules'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Boms'), array('controller' => 'project_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
