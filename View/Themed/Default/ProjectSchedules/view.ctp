<div class="projectSchedules view">
<h2><?php echo __('Project Schedule'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectSchedule['ProjectSchedule']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Bom'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectSchedule['ProjectBom']['id'], array('controller' => 'project_boms', 'action' => 'view', $projectSchedule['ProjectBom']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projectSchedule['ProjectSchedule']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($projectSchedule['ProjectSchedule']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($projectSchedule['ProjectSchedule']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Progress'); ?></dt>
		<dd>
			<?php echo h($projectSchedule['ProjectSchedule']['progress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectSchedule['User']['id'], array('controller' => 'users', 'action' => 'view', $projectSchedule['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project Schedule'), array('action' => 'edit', $projectSchedule['ProjectSchedule']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project Schedule'), array('action' => 'delete', $projectSchedule['ProjectSchedule']['id']), array(), __('Are you sure you want to delete # %s?', $projectSchedule['ProjectSchedule']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedules'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Boms'), array('controller' => 'project_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Bom'), array('controller' => 'project_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Production Orders'); ?></h3>
	<?php if (!empty($projectSchedule['ProductionOrder'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Tender Id'); ?></th>
		<th><?php echo __('Project Schedule Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($projectSchedule['ProductionOrder'] as $productionOrder): ?>
		<tr>
			<td><?php echo $productionOrder['id']; ?></td>
			<td><?php echo $productionOrder['sale_tender_id']; ?></td>
			<td><?php echo $productionOrder['project_schedule_id']; ?></td>
			<td><?php echo $productionOrder['created']; ?></td>
			<td><?php echo $productionOrder['modified']; ?></td>
			<td><?php echo $productionOrder['start']; ?></td>
			<td><?php echo $productionOrder['end']; ?></td>
			<td><?php echo $productionOrder['status']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'production_orders', 'action' => 'view', $productionOrder['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'production_orders', 'action' => 'edit', $productionOrder['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'production_orders', 'action' => 'delete', $productionOrder['id']), array(), __('Are you sure you want to delete # %s?', $productionOrder['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
