<?php echo $this->Html->link(__('Purchase Requisition'), array('action' => 'procurementindex'), array('class' => 'btn btn-info btn-sm')); ?>
 

<div class="row"> 
    <div class="col-xs-12"> 
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('View Purchase Requisition'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                <!-- content start-->
                
                <div class="inventoryPurchaseRequisitions form">
                <?php echo $this->Form->create('InventoryPurchaseRequisition', array("class"=>"form-horizontal", 'type' => 'file')); ?>
                    <fieldset>
                        <?php echo $this->Form->input("id"); ?>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">PR Number</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("pr_no", array("class"=> "form-control", "label"=> false, "readonly" => "readonly")); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Username</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("User.username", array("class"=> "form-control", "label"=> false, "readonly" => "readonly")); ?>
                             <?php echo $this->Form->input("User.group_id", array("id"=> "xmptrc", "type"=> "hidden")); ?>
                            </div> 
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Full Name</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("User.firstname", array("class"=> "form-control", "label"=> false, "readonly" => "readonly")); ?>
                            </div> 
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Phone</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("User.mobile_number", array("class"=> "form-control", "label"=> false, "readonly" => "readonly")); ?>
                            </div> 
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Type</label> 
                            <div class="col-sm-9 radio-box">
                            <?php 
                            $options = array(
                                1 => 'CAPEX',
                                2 => 'OPEX',
                                3 => 'SERVING CLIENTS/CUSTOMERS'
                            );

                             
                            $attributes=array('legend'=>false, 'value'=>$this->request->data['InventoryPurchaseRequisition']['pr_type'], 'onclick' => 'toggleJob(this.value)', 'class' => 'pr_trpe');

                            echo $this->Form->radio('pr_type', $options, $attributes);

                            ?>
                            </div> 
                        </div>
                        <div class="form-group" id="jobs" style="display: none">
                            <label class="col-sm-3" style="padding-top: 8px">Job No</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("SaleJobChild.station_name", array("placeholder"=>"Job No", "id"=>"job", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false)); ?>
                            <?php echo $this->Form->input("sale_job_child_id", array("type"=>"hidden", "id"=>"sale_job_child_id", 'required'=>false)); ?>
                            </div> 
                        </div> 
                        
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Required Date *</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("dateline", array("type"=>"text", "id"=>"dateonly", "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Budget</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("account_department_budget_id", array("options"=>$budgets, "id"=>"available_budget", "class"=> "form-control", "label"=> false, "onchange" => "checkbudget(this.value);")); ?>
                            

                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Term Of Payment *</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("term_of_payment_id", array('options' => $termOfPayment, 'empty' => '-Select Term Of Payment-', "class"=> "form-control", "label"=> false)); ?> 
                            </div> 
                        </div>
                        <div class="form-group" id="otherpaymentmode" style="display: none;">
                            <label class="col-sm-3" style="padding-top: 8px">&nbsp;</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("new_term", array("class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div> 
                        
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">PO Type *</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("po_type", array('options' => array(1 => 'PO', 2 => 'Non PO'), "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Remark *</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("remark_id", array('options' => $remark, "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div> 

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Warranty *</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("warranty_id", array('options' => $warranty, "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div> 
 
                         <?php echo $this->Form->input("dir", array("type"=>"hidden")); ?>
                        <?php echo $this->Form->input("approval_dir", array("type"=>"hidden")); ?>
                    
                    
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Attach Quotation</label>
                        <div class="col-sm-9"><?php echo $this->Form->input("quotation", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?>
                        </div>
                    </div>
                     
                  
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Attach MTC Approval</label>
                        <div class="col-sm-9"><?php echo $this->Form->input("approval", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?>
                        </div>
                    </div>

                    </fieldset>
                    <div class="clearfix">&nbsp;</div>
                    <h4 class="text-right">Available Budget : <input type="text" name="a_budget" id="a_budget" value="<?php echo $this->request->data['AccountDepartmentBudget']['balance']; ?>"></h4>
                    <h4><?php echo __('Item'); ?></h4> 
                    <table cellspacing="1" cellpadding="1" class="table">
                        <thead>
                            <tr>
                                <th width="180">Item</th>
                                <th width="180">Supplier</th>
                                <th>Qty</th>
                                <th>UOM</th>
                                <th>Unit Price</th>
                                <th>CR</th> 
                                <th>Subtotal</th>
                                <th>Tax(%)</th>
                                <th>RM</th> 
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="p_scents">
                        
                        <?php 
                        $i = 0; 
                        foreach($items as $item){ ?>
                        
                            <tr id="row<?php echo $i; ?>">
                                <td id="itemlist<?php echo $i; ?>">
                                    <?php echo $item['InventoryItem']['code']; ?><br/>
                                    <small><?php echo $item['InventoryItem']['name']; ?> - <?php echo $item['InventoryPurchaseRequisitionItem']['id']; ?></small>
                                    
                                    <input type="hidden" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][id]" value="<?php echo $item['InventoryPurchaseRequisitionItem']['id']; ?>">

                                    <input type="hidden" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][inventory_item_id]" value="<?php echo $item['InventoryPurchaseRequisitionItem']['inventory_item_id']; ?>">
                                </td>
                                <td>
                                    <select style="width:180px" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][inventory_supplier_id]" onchange="getsupplieritem(this.value, <?php echo $item['InventoryPurchaseRequisitionItem']['inventory_item_id']; ?>, <?php echo $i; ?>)">
                                        <option value="<?php echo $item['InventorySupplier']['id']; ?>"><?php echo $item['InventorySupplier']['name']; ?></option>
                                        <?php foreach($item['Suppliers'] as $supplier){ ?>
                                        <option value="<?php echo $supplier['InventorySupplier']['id']; ?>"><?php echo $supplier['InventorySupplier']['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    
                                </td>

                                <td> 
                                <input style="width:50px" type="text" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][quantity]" class="item_quantity" id="quantity-<?php echo $i; ?>" value="<?php echo $item['InventoryPurchaseRequisitionItem']['quantity'] ?>"></td>

                                <td> 
                                <?php echo $this->Form->input("InventoryPurchaseRequisitionItem.$i.general_unit_id", array('options' => $generalUnits, "label"=> false, 'value' => $item['InventoryPurchaseRequisitionItem']['general_unit_id'])); ?>

                               </td>

                                <td><input style="width:80px" type="text" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][price_per_unit]" class="item_unit_price" value="<?php echo $item['InventoryPurchaseRequisitionItem']['price_per_unit']; ?>" id="unitprice-<?php echo $i; ?>"></td>
                                
                                <td>
                                <small id="currency-<?php echo $i; ?>"><?php echo $item['GeneralCurrency']['iso_code']; ?></small>
                                 <input type="hidden" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][general_currency_id]" value="<?php echo $item['InventoryPurchaseRequisitionItem']['general_currency_id']; ?>" id="currencyid-<?php echo $i; ?>">

                                </td>

                                <td><input style="width:90px" type="text" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][amount]" id="amount-<?php echo $i; ?>" value="<?php echo $item['InventoryPurchaseRequisitionItem']['amount']; ?>" readonly="readonly"></td>
                                <td><input style="width:20px" type="text" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][tax]" class="item_tax" id="tax-<?php echo $i; ?>" value="<?php echo $item['InventoryPurchaseRequisitionItem']['tax']; ?>"></td>
                                <?php 
                                $total = $item['InventoryPurchaseRequisitionItem']['amount'] * $item['GeneralCurrency']['rate']; 
                                if($item['InventoryPurchaseRequisitionItem']['tax'] != 0) {
                                    $total_rm = (($total / 100) * $item['InventoryPurchaseRequisitionItem']['tax']) + $total;
                                } else {
                                    $total_rm = $total;
                                }
                                ?> 

                                <td><input style="width:90px" type="text" name="data[InventoryPurchaseRequisitionItem][<?php echo $i; ?>][total_rm]" id="totalrm-<?php echo $i; ?>" readonly="readonly" value="<?php echo $total_rm; ?>"></td> 
                                
                                <td>
                                <?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'inventory_supplier_items', 'action' => 'index', '?name='.$item['InventoryItem']['code'].'&part_number=&inventory_supplier_id=&quotation_number=&effective_date=&expiry_date=&search=Search'), array('class' => 'btn btn-info btn-sm', 'escape' => false, 'target' => '_blank')); ?>
                                
                                </td>
                            </tr>
                        <?php 
                        $i++;
                        } ?>
                             
                        </tbody>
                    </table>
                   
                    </fieldset>
                    <div class="clearfix">&nbsp;</div>
                    <?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
                </div>
                <!-- content end -->
            </div>
        </div>
    </div> 
</div>

<?php
    /*
    [4/19, 7:21 PM] azrulharis: 0 Draft
1 Submit to Hos
2 Hos rjct
3 Hos approve (waiting prc) 
4 Prc rjct
5 Prc approve (Waiting hod) 
6 HOD rjct
7 HOD approve
[4/19, 7:27 PM] azrulharis: Find lofa by amount
[4/19, 7:30 PM] azrulharis: 8 MD rjct
9 MD approved (lofa not required)
10 Insuficent budget
11 Convert to PO
*/
?>

<?php $this->start('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.item_quantity').each(function() {

            $(this).on('keyup', function() {
                var res = $(this).attr('id'); 
                var res_2 = res.split("-"); 
                var id = res_2[1];

                var unit_price = $('#unitprice-'+id).val();
                var subtotal = $(this).val() * unit_price;
                $('#amount-'+id).val(subtotal);

                var tax = $('#tax-'+id).val();
                if(tax == 0){
                    $('#totalrm-'+id).val(subtotal);
                }else{
                    var after_tax = (parseFloat(tax)/100) * parseFloat(subtotal)+parseFloat(subtotal);
                    $('#totalrm-'+id).val(after_tax);
                }
                
            });
        });

        $('.item_unit_price').each(function() {
            $(this).on('keyup', function() {
                var res = $(this).attr('id'); 
                var res_2 = res.split("-"); 
                var id = res_2[1]; 

                var qty = $('#quantity-'+id).val();
                var unit_price = $(this).val();
                var subtotal = qty * unit_price;
                $('#amount-' + id).val(subtotal);

                var tax = $('#tax-'+id).val();
                if(tax == 0){
                    $('#totalrm-' + id).val(subtotal);
                }else{
                    var after_tax = (parseFloat(tax)/100) * parseFloat(subtotal)+parseFloat(subtotal);
                    $('#totalrm-' + id).val(after_tax);
                }
                
            });
        });

        $('.item_tax').each(function() {
            $(this).on('keyup', function() {
                var res = $(this).attr('id'); 
                var res_2 = res.split("-"); 
                var id = res_2[1]; 

                var subtotal = $('#amount-' + id).val();
                var tax = ($(this).val()/100) * subtotal;
                var after_tax = parseFloat(tax) + parseFloat(subtotal);
                
                $('#totalrm-' + id).val(after_tax);
    
                
            });
        });


        var id_sups = $('#sid').val();
        $('#supplier_id').val(id_sups);

        var row_count = $('#count_row').val();
        
        for(i=1; i<=row_count; i++){
            if($('#id'+i). prop("checked") == true){               
                $('#q'+i).prop('disabled', false);
                $('#p_unit'+i).prop('disabled', false);
                $('#gu'+i).prop('disabled', false);
                $('#r'+i).prop('disabled', false);

                var guid = $('#gu_id'+i).val();
                $('#gu'+i).val(guid);
            }
        }
        $('#budgets').click(function(){ 
            var value = $(".pr_trpe:checked").val();
            var job = $("#sale_job_child_id").val();
            if(value == 3 && job == 0) {
                $('#job').addClass('border-red');
            } else {
                if($('#job').hasClass('border-red')) {
                    $('#job').toggleClass('border-red');
                }
            }
        }); 

        $('#budgets').on('change', function(){ 
            var id = $(this).val();
            $.ajax({ 
                type: "POST", 
                //dataType: 'json',
                data: {id: id},
                url: baseUrl + 'inventory_purchase_requisitions/ajaxbudget', 
                success: function(data) { 
                    //console.log(data);
                    $('#a_budget').val(data);
                }
            }); 
            return false;  
        }); 

        
    });

    function toggleJob(nilai){
        if(nilai == 3){
            $("#jobs").show();
        } else {
            $("#jobs").hide();
        }  
        var xmptrc = $("#xmptrc").val();
        $.ajax({ 
            type: "GET", 
            dataType: "json",
            data: 'type='+nilai+'&sale_job_child_id=0&xmptrc='+xmptrc,
            cache: false,
            url: baseUrl + 'account_department_budgets/ajaxfindbudget', 
            success: function(data) {  
                var html = '<option value="">-Select Budget-</option>';
                $.each(data, function(i, item) { 
                     html += '<option value="'+ item['AccountDepartmentBudget']['id'] +'">'+item['AccountDepartmentBudget']['name']+ ' - '+item['AccountDepartmentBudget']['year']+'</option>';
                });
                $('#available_budget').html(html);
            } 
        });  
    }
    function getsupplieritem(supplier_id, item_id, row) {
        var cacheBuster = new Date().getTime();
         $.ajax({ 
            type: "GET", 
            dataType: 'json',
            data: {supplier_id: supplier_id, inventory_item_id: item_id, cache: cacheBuster},
            url: baseUrl + 'inventory_supplier_items/ajaxfinditembysupplier', 
            success: function(data) {  
                $('#genunit-'+row).val(data.InventorySupplierItem.min_order_unit);
                $('#unitprice-'+row).val(data.InventorySupplierItem.price);
                $('#currency-' + row).html(data.GeneralCurrency.iso_code);
                $('#currencyid-'+ row).val(data.GeneralCurrency.id);
                $('#quantity-'+row).val(data.InventorySupplierItem.min_order);
                $('#tax-'+row).val(data.Tax);

                var subtotal = data.InventorySupplierItem.min_order * data.InventorySupplierItem.price;
                $('#amount-'+row).val(subtotal);
                var totalRm = subtotal * data.GeneralCurrency.rate; 
                $('#totalrm-'+row).val(totalRm.toFixed(2));

                $('#lead-' + row).html(data.InventorySupplierItem.lead_time);
            }
        }); 
    }


    function checkbudget(id){
        var id = id;
        $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventory_purchase_requisitions/ajaxbudget', 
            success: function(data) { 
                //console.log(data);
                $('#a_budget').val(data);
            }
        }); 
        return false;  
    }

    function checkmode(mode){
        var mode = $("#mode option[value='"+mode+"']").text();
        if(mode == 'Others...'){
            $('#otherpaymentmode').show();
        }
        
    }

    

    function enabletextbox(rowid, price) {
        
        if($('#id'+rowid).prop("checked") == true){
            $('#q'+rowid).prop('disabled', false);
            $('#p_unit'+rowid).prop('disabled', false);
            $('#gu'+rowid).prop('disabled', false);
            $('#r'+rowid).prop('disabled', false);
        }
        else if($('#id'+rowid).prop("checked") == false){
            $('#q'+rowid).prop('disabled', true);
            $('#p_unit'+rowid).prop('disabled', true);
            $('#gu'+rowid).prop('disabled', true);
            $('#r'+rowid).prop('disabled', true);
        }

        var price = price;

        var adnilai = $('#p_unit'+rowid).val();

        if(adnilai == ''){
            $('#p_unit'+rowid).val(price);
        }
       
    }

    function checkremark(nilai){
        var mode = $("#rmk option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherremark').show();
        }
        
    }

    function checkwarranty(nilai){
        var mode = $("#wrty option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherwarranty').show();
        }
        
    }

    function updatedetails(nilai, row = null) {
        //$('#unit_price').attr('id','unit_price_'+nilai);
        //alert(row);
        if(nilai == 'xxx'){
            var supplier_id = $('#id_supplier').val();
            var getParam = {
                supplier_id: supplier_id
            }

            var scntDiv = $('#p_scents');
            var c = $('#p_scents tr').size()-1;
            var ci = c;

            //alert(ci);

            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventory_purchase_requisitions/ajaxitemgeneral', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(item);
                        
                        var html = '<br><select class="form-control" name="inventory_supplier_item_id[]" onchange="updatedetails_2(this.value, '+ci+')"><option value="0">Please Select Item</option>'; 
                        var row = 0;
                        $.each(data, function(i, item) { 
                            //console.log(item);
                            html += '<option value='+item.id+'>';
                            html += item.name;
                            html += '</option>';
                        });
                        html += '</select>';
                        //console.log(ci);

                        if(ci == '0'){
                            $('#newitem').show();
                            $('#newitem').html(html);
                        }else{
                            $('#newitem'+ci).show();
                            $('#newitem'+ci).html(html);
                            
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false;

        }else{

            //alert(nilai);
            //alert(row);
            var getParam = {
                id: nilai
            }
        
            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventory_purchase_requisitions/ajaxitemdetails', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(row);
                        if(row == null || row == '0'){
                            $('#unit_price').val(item.unit_price);
                            $('#newitem').hide();
                        }else{
                            $('#unit_price'+row).val(item.unit_price);
                            $('#newitem'+row).hide();
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false; 
        } 
    }

    function updatedetails_2(nilai, row = null) {
        //$('#unit_price').attr('id','unit_price_'+nilai);
        //alert(nilai);
        //alert(nilai);
        //alert(row);
        var getParam = {
            id: nilai
        }
    
        $.ajax({ 
            type: "GET", 
            dataType: "json",
            data: getParam,
            cache: false,
            url: baseUrl + 'inventory_purchase_requisitions/ajaxitemdetails', 
            success: function(data) { 
                $.each(data, function(i, item) { 
                    //console.log(row);
                    if(row == null || row == '0'){
                        $('#unit_price').val(item.unit_price);
                        //$('#newitem').hide();
                    }else{
                        $('#unit_price'+row).val(item.unit_price);
                        //$('#newitem').hide();
                    }
                });
            },
            error: function(err) { 
                console.log(err);
                
            }
        }); 
        return false; 
        
    }

    function addrow(){
        
        

    }

    function removerow(nilai) {
        //alert(nilai);
        $('#row'+nilai).remove();
        
    };

  </script>
  <?php $this->end(); ?>