<div class="row"> 
    <div class="col-xs-12">
        <?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Edit Purchase Requisition'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                <!-- content start--> 
                <div class="inventoryPurchaseRequisitions form">
                <?php echo $this->Form->create('InventoryPurchaseRequisition', array("class"=>"form-horizontal", "type" => "file")); ?>
                <?php echo $this->Form->input("id"); ?>
                     
                
                <div class="form-group">
                    <label class="col-sm-3" style="padding-top: 8px">PR type</label>
                    <div class="col-sm-9" style="padding-top: 8px;">
                    <?php 
                        $default = $this->request->data['InventoryPurchaseRequisition']['pr_type'];
                        $options=array('1'=>'&nbsp;CAPEX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','2'=>'&nbsp;OPEX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', '3'=>'&nbsp;SERVING CLIENTS/CUSTOMERS');
                        $attributes=array('legend'=>false, 'value'=>$default, 'onclick' => 'toggleJob(this.value)');
                        echo $this->Form->radio('pr_type',$options,$attributes); ?>
                    </div> 
                </div>
                <div class="form-group" id="jobs"<?php if($default != 3) { ?> style="display: none" <?php } ?>>
                    <label class="col-sm-3" style="padding-top: 8px">Job No</label>
                    <div class="col-sm-9">
                    <?php echo $this->Form->input("SaleJobChild.station_name", array("placeholder"=>"Job No", "id"=>"job", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false)); ?>

                    <?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden')); ?>  
                    <?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden')); ?>

                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-sm-3" style="padding-top: 8px">Budget</label>
                    <div class="col-sm-9">

                    <?php 
                    $budg = array();
                    foreach ($budgets as $bgt) {
                        $budg[$bgt['AccountDepartmentBudget']['id']] = $bgt['AccountDepartmentBudget']['name'];
                    }
                    echo $this->Form->input("account_department_budget_id", array("options" => $budg, "empty" => "-Select Budget-", "class" => "form-control", "label" => false, 'value' => $this->request->data['InventoryPurchaseRequisition']['account_department_budget_id'], 'id' => 'budgets')); ?> 
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-sm-3" style="padding-top: 8px">PR Number</label>
                    <div class="col-sm-9">
                    <?php echo $this->Form->input("pr_no", array("class"=> "form-control", "label"=> false, "readonly" => true)); ?>
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-sm-3" style="padding-top: 8px">Referrence</label>
                    <div class="col-sm-9">
                    <?php echo $this->Form->input("ref", array("class"=> "form-control", "label"=> false, "required" => false, 'placeholder' => 'Quotation no / Job no')); ?>
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-sm-3" style="padding-top: 8px">Required Date</label>
                    <div class="col-sm-9">
                    <?php echo $this->Form->input("dateline", array("type"=>"text", "id"=>"dateonly", "class"=> "form-control", "label"=> false)); ?>
                    </div> 
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3" style="padding-top: 8px">Mode Of Payment</label>
                    <div class="col-sm-9">
                    <select name="term_of_payment_id" class="form-control" onchange="checkmode(this.value)" id="mode">
                        <option value="0">Please Select Terms</option>
                        <?php foreach ($terms as $key => $term) { ?>
                           <option value="<?php echo $key; ?>" <?php if($key == $this->request->data['InventoryPurchaseRequisition']['term_of_payment_id']) { echo "selected"; }  ?>><?php echo $term; ?></option>
                        <?php } ?>
                    </select>

                    </div> 
                </div>
                <div class="form-group" id="otherpaymentmode" style="display: none;">
                    <label class="col-sm-3" style="padding-top: 8px">&nbsp;</label>
                    <div class="col-sm-9">
                    <?php echo $this->Form->input("new_term", array("class"=> "form-control", "label"=> false)); ?>
                    </div> 
                </div> 
                <div class="form-group">
                    <label class="col-sm-3" style="padding-top: 8px">PO Type</label>
                    <div class="col-sm-9">
                    <?php echo $this->Form->input("po_type", array("options"=>$po_type, "empty"=>"Please Select Type", "class"=> "form-control", "label"=> false)); ?>
                    </div>
                    <input type="hidden" id="id_supplier"> 
                </div>

                 

                <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Term of Delivery</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("term_of_delivery_id", array("required" => true, "options" => $tod, "empty"=>"-Select Term-", "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Total Discount (Optional)</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("bulk_discount", array("required" => false, "empty"=>"-Select Term-", "class"=> "form-control", "label"=> false, "type" => "text")); ?>
                            <small>Note: Once you fill Total Discount, you are <b>not allow to add item by different supplier</b></small>
                            </div> 
                        </div>

 
                    <div class="clearfix">&nbsp;</div>

                    <?php if($purchaseRequisition['InventoryPurchaseRequisition']['general_purchase_requisition_type_id'] == 1) { ?> 
                    <h4 class="text-right">Available Budget : <input type="text" name="a_budget" id="a_budget" class="text-center" readonly="readonly" value="<?php echo $purchaseRequisition['AccountDepartmentBudget']['balance']; ?>"></h4>
                    <?php } else { ?>
                    <input type="hidden" name="a_budget" id="a_budget" value="99999999999999">
                    <?php } ?>
                    <h4><?php echo __('Items'); ?></h4> 
                    
                    <table cellspacing="1" cellpadding="1" class="table form-no-padding">
                        <thead>
                            <tr>
                                <th class="col-sm-3">Item</th>
                                <th class="col-sm-2">Remark</th>
                                <th class="col-sm-2">Supplier</th>
                                <th class="col-sm-1">Curr.</th>
                                <th class="col-sm-1">Qty</th>
                                <th class="col-sm-1">Unit Price</th>
                                <th class="col-sm-1">UOM</th>
                                <th class="col-sm-1">Discount</th>
                                <th class="col-sm-1">Disc.Type</th>
                                <th class="col-sm-1">Tax (%)</th>
                                <th class="col-sm-1">Subtotal</th>
                                <th class="col-sm-1">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="p_scents">
                        <input type="hidden" id="rowcount" value="<?php echo count($pritems); ?>">
                        <?php 
                        $i = 0;
                        if(!empty($pritems)) { 
                        foreach($pritems as $inv_PR) { ?>
                        
                            <tr id="row<?php echo $i; ?>">

                                <td><input type="text" class="form-control itemlist" placeholder="Item Code/Name" id="itemlist<?php echo $i; ?>" value="<?php echo $inv_PR['InventoryItem']['code']; ?>" onkeyup="find(<?php echo $i; ?>)"><input type="hidden" name="inventory_item_id[]" class="form-control" id="itemid<?php echo $i; ?>" value="<?php echo $inv_PR['InventoryItem']['id']; ?>"><br></td>


                                <td><textarea name="remark[]" id="remark<?php echo $i; ?>" placeholder="Remark" class="form-control max-200"><?php echo $inv_PR['InventoryPurchaseRequisitionItem']['remark']; ?></textarea></td>
                                <td>

                                
                                <?php echo $this->Form->input("supplier_id", array("id" => "supplier" . $i, "name" => "supplier_id[]", "options" => $suppliers, "empty" => "-Select Supplier-", "class" => "form-control", "label"=> false, "value" => $inv_PR['InventoryPurchaseRequisitionItem']['inventory_supplier_id'])); ?>

                                </td> 
                                <td>  
                               

                                <?php 
                                $curr = array();
                                foreach ($gcurrencies as $currency) {
                                    $curr[$currency['GeneralCurrency']['id']] = $currency['GeneralCurrency']['symbol'];
                                }
                                echo $this->Form->input("currency_id", array("name" => "currency_id[]", "options"=>$curr, "empty"=>"-Select-", "class"=> "form-control", "label"=> false, "value" => $inv_PR['InventoryPurchaseRequisitionItem']['general_currency_id'], "id" => "currency_id".$i)); ?>

                                </td> 
                                <td> 
                                 

                                <input type="text" class="form-control" name="quantity[]" id="quantity<?php echo $i; ?>" value="<?php echo $inv_PR['InventoryPurchaseRequisitionItem']['quantity']; ?>"></td>
                                
                                <td>
                                <input type="text" class="form-control" name="unit_price[]" id="unit_price<?php echo $i; ?>" value="<?php echo $inv_PR['InventoryPurchaseRequisitionItem']['price_per_unit']; ?>">
                                </td>
                                
                                <td><select name="general_unit[]" id="genunit<?php echo $i; ?>" class="form-control"><option value="0">UOM</option><?php foreach ($generalUnits as $key => $gUnit) { ?><option value="<?php echo $key; ?>" <?php if($key == $inv_PR['InventoryPurchaseRequisitionItem']['general_unit_id']){ echo 'selected="selected"'; } ?>><?php echo $gUnit; ?></option> 
                        <?php } ?></select></td>

                                <td><input type="text" class="form-control" name="discount[]" id="discount<?php echo $i; ?>" value="<?php echo $inv_PR['InventoryPurchaseRequisitionItem']['discount']; ?>"></td>

                                <td><select class="form-control" id="dis_type" name="discount_type[]"><option value="x">Select</option><option value="0" <?php if($inv_PR['InventoryPurchaseRequisitionItem']['discount_type'] == 0) { echo 'selected="selected"'; } ?>>Amount</option>
                                <option value="1" <?php if($inv_PR['InventoryPurchaseRequisitionItem']['discount_type']==1){ echo 'selected="selected"'; } ?>> % </option></select></td>
                                <td><input type="text" class="form-control" name="tax[]" id="tax<?php echo $i; ?>" value="<?php echo $inv_PR['InventoryPurchaseRequisitionItem']['tax']; ?>"></td>
                                <td>
                                <input type="text" class="form-control sub" name="sub[]" id="sub<?php echo $i; ?>" value="<?php echo $inv_PR['InventoryPurchaseRequisitionItem']['amount']; ?>">
                                </td>
                                <td> 

                                <button type="button" onclick="removerow(<?php echo $i; ?>)" class="btn btn-sm btn-danger" id="minusbtn"><i class="fa fa-minus"></i></button>
                                 </td>
                            </tr>

                        <?php 
                        $i += 1;
                        } 
                    } else { 
                        ?>
                        <tr>
                                <td><input type="text" class="form-control" placeholder="Item Code/Name" id="itemlist0"required><input type="hidden" name="inventory_item_id[]" class="form-control" id="itemid0"><br></td>
 

                                <td><textarea name="remark[]" id="remark0" placeholder="Note/Remark" class="form-control max-200"></textarea></td>

                                <td><?php echo $this->Form->input("supplier_id", array("id" => "supplier0", "name" => "supplier_id[]", "options" => $suppliers, "empty" => "-Select Supplier-", "class" => "form-control", "label"=> false, "value" => $inv_PR['InventoryPurchaseRequisitionItem']['inventory_supplier_id'])); ?></td>

                                <td><select class="form-control" name="currency_id[]" id="currency_id0" required>
                                <option value="">-Select-</option>
                                <?php foreach ($gcurrencies as $currency) { ?>
                                    <option value="<?php echo $currency['GeneralCurrency']['id']; ?>"><?php echo $currency['GeneralCurrency']['symbol']; ?></option>
                                <?php } ?>
                                </select></td>

                                <td><input type="hidden" id="supplierid0"><input type="text" class="form-control" name="quantity[]" id="quantity0"></td>
                                <td><input type="text" class="form-control" name="unit_price[]" id="unit_price0"required></td>
                                <td><select name="general_unit[]" id="genunit0" class="form-control"required><option value="0">UOM</option><?php foreach ($generalUnits as $key => $gUnit) { ?><option value="<?php echo $key; ?>"><?php echo $gUnit; ?></option>
                        <?php } ?></select></td>
                                <td><input type="text" class="form-control" name="discount[]" id="discount" value="0"></td>
                                <td><select class="form-control" id="dis_type" name="discount_type[]"><option value="x">Type</option><option value="0">Fix</option><option value="1"> % </option></select></td>
                                <td><input type="text" class="form-control" name="tax[]" id="tax0"></td>
                                <td></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <button type="button" onclick="addrow()" class="btn btn-sm btn-success" id="plusbtn"><i class="fa fa-plus"></i> Add More Item</button>
                    <?php echo $this->Form->input("dir", array("type"=>"hidden")); ?>
                    <?php echo $this->Form->input("approval_dir", array("type"=>"hidden")); ?>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                    
                    <div class="col-sm-12">

                    

                    <p><b>Note:</b> Once upload new file, current attachment will be override</p>
                    <h4><?php echo __('Quotation Attachment (Optional)'); ?></h4>
                    <?php echo $this->Form->input("quotation", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?>
                    <small><?php echo $this->Html->link($this->request->data['InventoryPurchaseRequisition']['quotation'], '/files/inventory_purchase_requisition/quotation/' . $this->request->data['InventoryPurchaseRequisition']['dir'] . '/' . $this->request->data['InventoryPurchaseRequisition']['quotation'], array('target' => '_blank', 'class'=>'line')); ?>

                    </small>
                    </div>
                    
                    <div class="col-sm-12">
                    <h4><?php echo __('MTC Approval Attachment (Optional)'); ?></h4>
                    <?php echo $this->Form->input("approval", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?>
                    <small><?php echo $this->Html->link($this->request->data['InventoryPurchaseRequisition']['approval'], '/files/inventory_purchase_requisition/approval/' . $this->request->data['InventoryPurchaseRequisition']['approval_dir'] . '/' . $this->request->data['InventoryPurchaseRequisition']['approval'], array('target' => '_blank', 'class'=>'line')); ?></small>
                    </div> 
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Remark</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("remark_id", array("id" => "rmk", "options" => $remark, "empty" => array(0=>"Select Remark"), "class"=> "form-control", "label"=> false, "onchange" => "checkremark(this.value)")); ?>
                        </div> 
                    </div>
                    <div class="form-group" id="otherremark" style="display: none;">
                        <label class="col-sm-2" style="padding-top: 8px">&nbsp;</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("new_remark", array("class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Warranty</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("warranty_id", array("id" => "wrty", "options" => $warranty, "empty" => array(0 => "Select Warranty"), "class"=> "form-control", "label"=> false, "onchange" => "checkwarranty(this.value)")); ?>
                        </div> 
                    </div>
                    <div class="form-group" id="otherwarranty" style="display: none;">
                        <label class="col-sm-2" style="padding-top: 8px">&nbsp;</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("new_warranty", array("class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Status</label>
                        <div class="col-sm-10"> 
                        <?php 
                        $status = $this->request->data['InventoryPurchaseRequisition']['status'];
                        $statusoptions=array(0 => 'Save as Draft &nbsp &nbsp &nbsp', 1 => 'Submit for Approval');
                        $statusattributes=array('legend'=>false, 'value'=>$status);
                        echo $this->Form->radio('status',$statusoptions,$statusattributes); ?>

                        </div> 
                    </div>
                    </fieldset>
                    <div class="clearfix">&nbsp;</div>
                    <?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
                </div>
                <!-- content end -->
            </div>
        </div>
    </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {

        $('.sub').each(function() {

        })



        var id_sups = $('#sid').val();
        $('#supplier_id').val(id_sups);

        var row_count = $('#count_row').val();
        
        for(i=1; i<=row_count; i++){
            if($('#id'+i). prop("checked") == true){               
                $('#q'+i).prop('disabled', false);
                $('#p_unit'+i).prop('disabled', false);
                $('#gu'+i).prop('disabled', false);
                $('#r'+i).prop('disabled', false);

                var guid = $('#gu_id'+i).val();
                $('#gu'+i).val(guid);
            }
        }      

    });  

    function find(row){
        $('#itemlist'+row).autocomplete({ 
                source: function (request, response){ 
                    $.ajax({
                        type: "GET",                        
                        url:baseUrl + 'inventory_supplier_items/ajaxfinditem_2',           
                        contentType: "application/json",
                        dataType: "json",
                        data: "term=" + $('#itemlist'+row).val(),
                        success: function (data) {
                           
                            response($.map(data, function (item) {
                                return {
                                    id: item.id,
                                value: item.code,
                                //supplierid: item.InventorySupplier.id,
                                //supplier: item.InventorySupplier.name,
                                name : item.name,
                                //price: item.InventorySupplierItem.price,
                                code: item.code,
                                note: item.note,
                                //gst: item.InventorySupplier.gst,
                                //type: item.type,
                                //general_currency_id : item.InventorySupplierItem.general_currency_id,
                                unit: item.general_unit_id,
                                InventorySupplier: item.InventorySupplier,
                                InventorySupplierItem: item.InventorySupplierItem,
                                GeneralCurrency: item.GeneralCurrency,
                                Suppliers: item.Suppliers
                                }
                            }));
                        }
                    });
                },
                select: function (event, ui) {  
                  
                    $(this).val( ui.item.value );  
                    if(ui.item.InventorySupplierItem != null) { 
                        $("#supplier"+row).val(ui.item.InventorySupplier.id).change(); 
                    }
                    $("#itemid"+row).val(ui.item.id); 
                    $("#quantity"+row).val(ui.item.min_unit);
                    $("#supplierid"+row).val(ui.item.supplierid);
                    $("#supplier"+row).val(ui.item.supplier);
                    $("#unit_price"+row).val(ui.item.price); 
                    $("#genunit"+row).val(ui.item.unit);  
                    if(ui.item.gst == null || ui>item.gst == ''){
                        ui.item.gst = '';
                    }else{
                        ui.item.gst = 6;
                    }
                    $("#tax"+row).val(ui.item.gst);
                },
                minLength: 3
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" ).append( "<div>" + item.code + "<br><small>" + item.name + "</small><br/></div>" ).appendTo( ul );
            };
    }

    $(document).ready(function() {
          
    });

    function toggleJob(nilai){
        if(nilai == 3){
            $("#jobs").show();
        } else {
            $("#jobs").hide();
            $('#sale_job_child_id').val(0);
            $('#job').val('');
        }  
        $.ajax({ 
            type: "GET", 
            dataType: "json",
            data: 'type='+nilai+'&sale_job_child_id=0',
            cache: false,
            url: baseUrl + 'account_department_budgets/ajaxfindbudget', 
            success: function(data) { 
                console.log(data);
                var html = '<option value="">-Select Budget-</option>';
                $.each(data, function(i, item) { 
                     html += '<option value="'+ item['AccountDepartmentBudget']['id'] +'">'+item['AccountDepartmentBudget']['name']+ '</option>';
                });
                $('#budgets').html(html);
            } 
        });  
    }


    function checkbudget(id){
        var id = id;
        $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventory_purchase_requisitions/ajaxbudget', 
            success: function(data) { 
                //console.log(data);
                $('#a_budget').val(data);
            }
        }); 
        return false;  
    }

    function checkmode(mode){
        var mode = $("#mode option[value='"+mode+"']").text();
        if(mode == 'Others...'){
            $('#otherpaymentmode').show();
        }
        
    }

$(document).ready(function() { 
        $('#job').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'sale_jobs/ajaxfindjob',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#job').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            sale_job_child_id: item.sale_job_child_id,
                            value: item.name,
                            station: item.station
                             
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {   
            $('#sale_job_id').val(ui.item.id); 
            $('#sale_job_child_id').val(ui.item.sale_job_child_id);  
            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: 'type=3&sale_job_child_id='+ui.item.sale_job_child_id,
                cache: false,
                url: baseUrl + 'account_department_budgets/ajaxfindbudget', 
                success: function(data) { 
                    
                    var html = '<option value="">-Select Budget-</option>';
                    $.each(data, function(i, item) { 
                         html += '<option value="' +  item['AccountDepartmentBudget']['id'] +'">' + item['AccountDepartmentBudget']['name']+ '</option>';
                    });
                    $('#budgets').html(html);
                },
                error: function(err) {  
                }
            });  
            if($('#job').hasClass('border-red')) {
                $('#job').toggleClass('border-red');
            }
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "&nbsp;&nbsp;/&nbsp;&nbsp;<strong>" + item.station + "<br>" +  "</strong></div>" ).appendTo( ul );
    }; 


     

    $('#budgets').on('change', function() {
        var id = $(this).val();
            $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventory_purchase_requisitions/ajaxbudget', 
            success: function(data) {  
                $('#a_budget').val(data);
            }
        });  
    });
});



    function enabletextbox(rowid, price) {
        
        if($('#id'+rowid).prop("checked") == true){
            $('#q'+rowid).prop('disabled', false);
            $('#p_unit'+rowid).prop('disabled', false);
            $('#gu'+rowid).prop('disabled', false);
            $('#r'+rowid).prop('disabled', false);
        }
        else if($('#id'+rowid).prop("checked") == false){
            $('#q'+rowid).prop('disabled', true);
            $('#p_unit'+rowid).prop('disabled', true);
            $('#gu'+rowid).prop('disabled', true);
            $('#r'+rowid).prop('disabled', true);
        }

        var price = price;

        var adnilai = $('#p_unit'+rowid).val();

        if(adnilai == ''){
            $('#p_unit'+rowid).val(price);
        }
       
    }

    function checkremark(nilai){
        var mode = $("#rmk option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherremark').show();
        }
        
    }

    function checkwarranty(nilai){
        var mode = $("#wrty option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherwarranty').show();
        }
        
    }

    function updatedetails(nilai, row = null) {
       
        if(nilai == 'xxx'){
            var supplier_id = $('#id_supplier').val();
            var getParam = {
                supplier_id: supplier_id
            }

            var scntDiv = $('#p_scents');
            var c = $('#p_scents tr').size()-1;
            var ci = c;

            //alert(ci);

            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventory_purchase_requisitions/ajaxitemgeneral', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(item);
                        
                        var html = '<br><select class="form-control" name="inventory_supplier_item_id[]" onchange="updatedetails_2(this.value, '+ci+')"><option value="0">Please Select Item</option>'; 
                        var row = 0;
                        $.each(data, function(i, item) { 
                            //console.log(item);
                            html += '<option value='+item.id+'>';
                            html += item.name;
                            html += '</option>';
                        });
                        html += '</select>';
                        //console.log(ci);

                        if(ci == '0'){
                            $('#newitem').show();
                            $('#newitem').html(html);
                        }else{
                            $('#newitem'+ci).show();
                            $('#newitem'+ci).html(html);
                            
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false;

        } else { 
            var getParam = {
                id: nilai
            }
        
            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventory_purchase_requisitions/ajaxitemdetails', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(row);
                        if(row == null || row == '0'){
                            $('#unit_price').val(item.unit_price);
                            $('#newitem').hide();
                        }else{
                            $('#unit_price'+row).val(item.unit_price);
                            $('#newitem'+row).hide();
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false; 
        } 
    }

    function updatedetails_2(nilai, row = null) {
   
        var getParam = {
            id: nilai
        }
    
        $.ajax({ 
            type: "GET", 
            dataType: "json",
            data: getParam,
            cache: false,
            url: baseUrl + 'inventory_purchase_requisitions/ajaxitemdetails', 
            success: function(data) { 
                $.each(data, function(i, item) { 
                    //console.log(row);
                    if(row == null || row == '0'){
                        $('#unit_price').val(item.unit_price);
                        //$('#newitem').hide();
                    }else{
                        $('#unit_price'+row).val(item.unit_price);
                        //$('#newitem').hide();
                    }
                });
            },
            error: function(err) { 
                console.log(err);
                
            }
        }); 
        return false; 
        
    }

    function addrow() { 
        var scntDiv = $('#p_scents');
        var c = $('#p_scents tr').size();
        var ci = c;

        scntDiv.append('<tr id="row'+c+'"><td><input type="text" placeholder="Item Code/Name" class="form-control" id="itemlist'+c+'"><input type="hidden" name="inventory_item_id[]" class="form-control" id="itemid'+c+'"><br></td><td><textarea name="remark[]" class="form-control max-200" placeholder="Remark"></textarea></td><td><select id="supplier'+c+'" name="supplier_id[]" class="form-control"required><option value="">-Select-</option><?php foreach($suppliers as $key => $supplier) { ?><option value="<?php echo $key; ?>"><?php echo h($supplier); ?></option><?php } ?></select></td><td><select id="general_currency_id'+c+'" class="form-control" name="currency_id[]" required> <option value="">-Select-</option> <?php foreach ($gcurrencies as $currency) { ?> <option value="<?php echo $currency['GeneralCurrency']['id']; ?>"><?php echo $currency['GeneralCurrency']['symbol']; ?></option> <?php } ?> </select></td><td> <input type="text" class="form-control" name="quantity[]" id="quantity'+c+'"></td><td><input type="text" class="form-control" name="unit_price[]" id="unit_price'+c+'"></td><td><select name="general_unit[]" id="genunit'+c+'" class="form-control"required><option value="">UOM</option><?php foreach ($generalUnits as $key => $gUnit) { ?><option value="<?php echo $key; ?>"><?php echo $gUnit; ?></option><?php } ?></select></td><td><input type="text" class="form-control" name="discount[]" id="discount" value="0"></td><td><select class="form-control" id="dis_type" name="discount_type[]"><option value="x">Select Type</option><option value="0">Amount</option><option value="1"> % </option></select></td><td><input type="text" class="form-control" name="tax[]" id="tax'+c+'"></td><td><input type="text" class="form-control sub" name="sub[]" id="sub'+c+'"></td><td><button type="button" class="btn btn-danger btn-sm" onclick="removerow('+c+')"><i class="fa fa-minus"></i></button></td></tr>');
        findItem(c, $(this).val());
        c++; 
    }

    function findItem(row, val) {
        //alert(row);
        $('#itemlist'+row).autocomplete({ 
            source: function (request, response){ 
                $.ajax({
                    type: "GET",                        
                    url:baseUrl + 'inventory_supplier_items/ajaxfinditem_2',           
                    contentType: "application/json",
                    dataType: "json",
                    data: "term=" + $('#itemlist'+row).val(),
                    success: function (data) {  
                        response($.map(data, function (item) {
                            return {
                                id: item.id,
                                value: item.code,
                                //supplierid: item.InventorySupplier.id,
                                //supplier: item.InventorySupplier.name,
                                name : item.name,
                                //price: item.InventorySupplierItem.price,
                                code: item.code,
                                note: item.note,
                                //gst: item.InventorySupplier.gst,
                                //type: item.type,
                                //general_currency_id : item.InventorySupplierItem.general_currency_id,
                                unit: item.general_unit_id,
                                InventorySupplier: item.InventorySupplier,
                                InventorySupplierItem: item.InventorySupplierItem,
                                GeneralCurrency: item.GeneralCurrency 
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {   
                if(ui.item.InventorySupplierItem != null) { 
                    $("#supplier"+row).val(ui.item.InventorySupplier.id).change(); 
                    if(ui.item.InventorySupplier.gst == '' || ui.item.InventorySupplier.gst == null) {
                        tax = '';
                    } else {
                        tax = 6;
                    } 
                    $("#tax"+row).val(tax); 
                    $("#quantity"+row).val(ui.item.InventorySupplierItem.min_order);
                    $("#unit_price"+row).val(ui.item.InventorySupplierItem.price_per_unit); 
                    $("#general_currency_id"+row).val(ui.item.InventorySupplier.general_currency_id);
                } else { 

                    $("#tax"+row).val(0); 
                    $("#quantity"+row).val(1);
                    $("#unit_price"+row).val(0); 
                    $("#general_currency_id"+row).val(1);
                    
                }
                $("#itemid"+row).val(ui.item.id);  
                $("#genunit"+row).val(ui.item.unit).change(); 
                //getsupplier_2(ui.item.id, row);
            },
            minLength: 3
        }).autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" ).append( "<div>" + item.code + "<br><small>" + item.name + "</small><br/></div>" ).appendTo( ul );
        }; 
    }

    function getsupplier_2($id, row){
        var iditem = $('#itemid'+row).val();
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'inventory_suppliers/ajaxfindsupplier',           
            contentType: "application/json",
            dataType: "json",
            data: "itemId=" + $id,                                                    
            success: function (data) { 
                console.log(data);
                var html = '<option value="">-Select-</option>';
                for(i=0; i<data.length; i++){
                    html += '<option value="'+data[i]['id']+'">'+data[i]['supplier']+'</option>';
                }
                 
                $('#supplier'+row).html(html);
            }
        });
    }

    function chgsupplier_2(nilai, id, row){
        
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'inventory_supplier_items/ajaxfinditemdetail',           
            contentType: "application/json",
            dataType: "json",
            data: "itemId="+id+"&supplierId="+nilai,                                                    
            success: function (data) { 
                console.log(data);
                if(data.length > 0) {
                    var min_quantity = data[0]['InventorySupplierItem']['min_order'];
                    var u_price = data[0]['InventorySupplierItem']['price_per_unit'];
                    var g_unit = data[0]['InventorySupplierItem']['general_unit_id'];
                    var s_id = data[0]['InventorySupplier']['id'];
                    if(data[0]['InventorySupplier']['gst'] == '' || data[0]['InventorySupplier']['gst'] == null) {
                        var tax = '';
                    } else {
                        var tax = 6;
                    } 
                    $("#quantity"+row).val(min_quantity);
                    $("#supplierid"+row).val(s_id);
                    $("#unit_price"+row).val(u_price);
                    $("#genunit"+row).val(g_unit);
                    $("#tax"+row).val(tax);    
                } 
            }
        });
    }

    function removerow(nilai) {
        //alert(nilai);
        $('#row'+nilai).remove();
        
    };
</script>
<?php $this->end(); ?>