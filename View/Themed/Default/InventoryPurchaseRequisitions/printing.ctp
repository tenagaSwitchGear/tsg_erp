 <div class="book page-break">
    <div class="page">
        <div id="header">
           
        </div> 
        <div id="content" class="container">
 
                <?php echo $this->Session->flash(); ?>
                <!-- content start-->

                <h4>Verifier &amp; Approval Remarks</h4>

                <table class="table table-bordered"> 
                    <tr>
                        <th>User</th>
                        <th>Remark</th> 
                        <th>Status</th> 
                        <th>Created</th> 
                        <th>Role</th>
                    </tr>
                    <?php foreach ($approvalremarks as $approvalremark) { ?> 
                        <tr>
                            <td><?php echo h($approvalremark['User']['firstname']); ?></td>
                            <td><?php echo h($approvalremark['InventoryPurchaseRequisitionRemark']['remark']); ?></td> 
                            <td><?php echo remarkStatus($approvalremark['InventoryPurchaseRequisitionRemark']['status']); ?></td>
                            <td><?php echo h($approvalremark['InventoryPurchaseRequisitionRemark']['created']); ?></td> 
                            <td><?php echo h($approvalremark['InventoryPurchaseRequisitionRemark']['type']); ?></td> 
                        </tr> 
                    <?php } ?>
                </table>

                

                <h4>Purchase Requisition Detail</h4>

                <div class="inventoryPurchaseRequisitions form-horizontal">
               
                    <table class="table table-bordered">
                        
                        <tr>
                            <td>PR No</td>
                            <td>
                            <?php echo $pr['InventoryPurchaseRequisition']['pr_no']; ?> 
                            </td> 
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td>
                            <?php echo $pr['User']['username']; ?> 
                            </td> 
                        </tr>

                        <tr>
                            <td>Full Name</td>
                            <td>
                             <?php echo $pr['User']['firstname']; ?> 
                            </td> 
                        </tr> 

                        <tr>
                            <td>Phone</td>
                            <td>
                             <?php echo $pr['User']['mobile_number']; ?> 
                            </td> 
                        </tr>

                        <tr>
                            <td>Type</td>
                            <td>
                            <?php
                            if(($pr['InventoryPurchaseRequisition']['pr_type']) == 1){
                                echo "CAPEX";
                            }else if(($pr['InventoryPurchaseRequisition']['pr_type'])==2){
                                echo "OPEX";
                            }else if(($pr['InventoryPurchaseRequisition']['pr_type'])==3){
                                echo "SERVING FOR CLIENT";
                            }else if(($pr['InventoryPurchaseRequisition']['pr_type'])==4){
                                echo "SERVING FOR CLIENT";
                            }  
                        ?>
                            </td> 
                        </tr>

                        
                        <tr>
                            <td>Required Date</td>
                            <td>
                            <?php echo $pr['InventoryPurchaseRequisition']['dateline']; ?> 
                            </td> 
                        </tr>
                        
                        <tr>
                            <td>Term Of Payment</td>
                            <td>
                            <?php echo $pr['TermOfPayment']['name']; ?>
                            </div> 
                        </tr> 
                        <tr>
                            <td>PO Type</td>
                            <td>
                           <?php echo $pr['InventoryPurchaseRequisition']['po_type'] == 1 ? 'PO' : 'Non PO'; ?>
                            </td> 
                        </tr> 

                        <tr>
                            <td><?php echo __('Total Amount'); ?></td>
                            <td><?php
                            $total = 0;
                        foreach ($items as $inventoryPurchaseRequisitionItem) {
                            $item_price = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit'] * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity'];

                            if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type'] == 0){
                                 
                                $discount_amount = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'];
                                 
                            }else{ 
                                $discount_amount = ($item_price / 100) * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount']; 
                            }

                                $total_rm = ($item_price * $inventoryPurchaseRequisitionItem['GeneralCurrency']['rate']) - $discount_amount;
                                $total += $total_rm;
                            } ?>
                        <?php 
                        $total = $total - $pr['InventoryPurchaseRequisition']['bulk_discount'];
                        echo _n2($total); ?>   
                            </td>
                        </tr>

                       <tr>
                            <td>Budget</td>
                            <td><?php echo $pr['AccountDepartmentBudget']['name']; ?> (<?php echo $pr['AccountDepartmentBudget']['balance']; ?>)
                            
                            </td> 
                        </tr>  
                        <tr>
                        <td><?php echo __('Approval Authority'); ?></td>
                    <td> 
                        <?php foreach($lofas as $lofa) { ?>
                            <?php if($lofa['InternalLofa']['price_min'] <= $total && $lofa['InternalLofa']['price_max'] >= $total) { ?>
                                <?php echo $lofa['InternalLofa']['role']; ?>
                            <?php } ?>
                        <?php } ?> 
                    </td>
                    </tr>
                        <tr>
                            <td>Remark</td>
                            <td>
                            <?php echo $pr['Remark']['name']; ?>
                            </td> 
                        </tr> 

                        <tr>
                            <td>Warranty</td>
                            <td>
                            <?php echo $pr['Warranty']['name']; ?>
                            </td> 
                        </tr> 

                        
 
                        <tr>
                            <td>Status</td>
                            <td>
                            <?php echo status($pr['InventoryPurchaseRequisition']['status']); ?> 
                            </td>  
                        </tr> 

                        <tr>
                            <td>Quotation Attachment</td>
                            <td>
                            <?php if($pr['InventoryPurchaseRequisition']['quotation'] != null) { ?>
                            <?php echo $this->Html->link($pr['InventoryPurchaseRequisition']['quotation'], '/files/inventory_purchase_requisition/quotation/' . $pr['InventoryPurchaseRequisition']['dir'] . '/' . $pr['InventoryPurchaseRequisition']['quotation'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            <p>Not available</p>
                        <?php } ?>
                            </td>  
                        </tr>

                        <tr>
                            <td>MTC Approval Attachment</td>
                            <td>
                            <?php if($pr['InventoryPurchaseRequisition']['approval'] != null) { ?>
                            <?php echo $this->Html->link($pr['InventoryPurchaseRequisition']['approval'], '/files/inventory_purchase_requisition/approval/' . $pr['InventoryPurchaseRequisition']['dir'] . '/' . $pr['InventoryPurchaseRequisition']['approval'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            <p>Not available</p>
                        <?php } ?>
                            </td>  
                        </tr>
                    </table>

                <div class="clearfix">&nbsp;</div> 
                <h4><?php echo __('Items'); ?></h4> 
                <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                <tr>
                    <th class="text-center" width="20px"><?php echo __('No'); ?></th>
                    <th><?php echo __('Items'); ?></th>
                    <th><?php echo __('Supplier'); ?></th>
                    <th><?php echo __('Quantity'); ?></th>
                    <th><?php echo __('Unit Price'); ?></th>
                    <th><?php echo __('Discount'); ?></th>
                    <th><?php echo __('Tax %'); ?></th>
                    <th><?php echo __('Tax Amount'); ?></th>
                    <th class="align-right"><?php echo __('Amount'); ?></th>
                    <th class="align-right"><?php echo __('RM'); ?></th>
                </tr>
                <?php

                // Check bulk discount
                if($pr['InventoryPurchaseRequisition']['bulk_discount'] > 0) {
                    $bulk = $pr['InventoryPurchaseRequisition']['bulk_discount'];
                } else {
                    $bulk = 0;
                }
                // Header for sum item in pr
                $header_total = 0;
                //var_dump($items[0]["InventorySupplier"]["gst"]);
                foreach ($items as $inventoryPurchaseRequisitionItem) {  
                    $header_total += $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit'] * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']; 
                }
                if($items[0]["InventorySupplier"]["gst"] != '' || $items[0]["InventorySupplier"]["gst"] != null) {
                    $is_gst = 6;
                } else {
                    $is_gst = 0;
                }
                ?>


                <?php 
                $no = '1'; 
                $total_rm = 0;
                $total_tax = 0;
                $total_discount_amount = 0;
                $calc_gst = 0;
                foreach ($items as $inventoryPurchaseRequisitionItem): 
                    $item_price = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit'] * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity'];

                    ?>

                    <tr>
                        <td class="text-center"><?php echo $no++; ?></td>
                        <td>
                            <?php echo h($inventoryPurchaseRequisitionItem['InventoryItem']['code']); ?><br>
                            <?php  
                                echo '<small>'.h($inventoryPurchaseRequisitionItem['InventoryItem']['name']).'</small>';  
                            ?>
                            <br><small><?php echo h($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['remark']); ?></small>
                            <?php if($_SESSION['Auth']['User']['group_id'] == '11') { ?>
                            <?php echo $this->Html->link(('<i class="fa fa-external-link" aria-hidden="true"></i>'), array('controller'=>'inventory_purchase_requisition_items', 'action' => 'index', $inventoryPurchaseRequisitionItem['InventoryItem']['id']), array('escape'=>false, 'target'=>'_blank')); ?>
                            <?php } ?>
                            
                        </td>
                        <td><?php echo $inventoryPurchaseRequisitionItem['InventorySupplier']['name']; ?></td>
                        <td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']; ?><?php 
                                echo ' '.$inventoryPurchaseRequisitionItem['GeneralUnit']['name'];
                            ?></td>
                        <td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit']; ?></td>
                        <td>
                        <?php
                        if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type'] == '0'){
                            if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'] == '0.0000'){
                                echo "-";
                            }else{
                                echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 2);
                            }
                            $discount_amount = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'];
                        }else{
                            echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 0).' %';
                            $discount_amount = ($item_price / 100) * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount']; 
                        }
                        $total_discount_amount += $discount_amount;
                        ?>
                        </td>
                        <td>
                        <?php
                        if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'] == '0'){
                            echo "-";
                        }else{
                            echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'].' %';
                        }
                        ?>
                        </td>

                        <td>

                        <?php 
                        if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'] == '0'){
                            $taxed = 0.00;
                            $tax = 0;
                        }else{
                            $tax = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'];
                            $taxed = (($item_price - $discount_amount) / 100) * $tax; 
                        }

                        $sub = $item_price - $discount_amount; 
                        $rm = $sub * $inventoryPurchaseRequisitionItem['GeneralCurrency']['rate']; 
                        $total_rm += $rm;

                        $a = ($bulk / $header_total) * $rm;
                        $b = $rm - $a;

                        if($tax > 0) {
                            $calc_sum_tax = ($b * $tax) / 100;
                            $calc_gst += $calc_sum_tax;
                        } else {
                            $calc_sum_tax = 0;
                            $calc_gst += $calc_sum_tax;
                        }   
                        ?>

                        <?php 
                        
                        $total_tax += $taxed;
                        echo _n2($taxed);
                        $taxed_value =  $taxed + $item_price;
                        ?>
                        </td>

                        <td class="align-right">
                        <?php 
                            echo $inventoryPurchaseRequisitionItem['GeneralCurrency']['iso_code']; 
                        ?> <?php echo $sub; 
                        ?></td>

                        <td class="align-right"><b><?php echo _n2($rm); ?></b></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                <td colspan="9" class="align-right"><b>Subtotal RM</b></td>
                <td class="align-right"><b><?php echo _n2($total_rm); ?></b></td>
                </tr>
                <tr>
                <td colspan="9" class="align-right"><b>Bulk Discount</b></td>
                <td class="align-right"><b><?php echo _n2($pr['InventoryPurchaseRequisition']['bulk_discount']); ?></b></td>
                </tr>
                <?php $after_discount = $total_rm - $pr['InventoryPurchaseRequisition']['bulk_discount']; ?>
                <tr>
                <td colspan="9" class="align-right"><b>Total RM</b></td>
                <td class="align-right"><b><?php echo _n2($after_discount); ?></b></td>
                </tr>
                <tr>
                <td colspan="9" class="align-right"><b>Tax</b></td>
                <td class="align-right"><b><?php  
                    echo _n2($calc_gst);  

                ?></b></td>
                </tr>
                <tr>
                <td colspan="9" class="align-right"><b>Grand Total RM</b></td>
                <td class="align-right"><b><?php echo _n2($after_discount + $calc_gst); ?></b></td>
                </tr>

                </table>
                <div class="clearfix">&nbsp;</div>
                
                <h4><?php echo __('Budget Control'); ?></h4>
            <table cellpadding = "0" cellspacing = "0" class="table table-bordered">
                <tr>  
                    <th>Subject</th>
                    <th>Amount (RM)</th> 
                </tr> 
                <tr>
                    <td>Budget</td>
                    <td><?php echo $pr['AccountDepartmentBudget']['name']; ?> </td> 
                </tr>
                <tr>
                    <td>Budget Total</td>
                    <td><?php echo _n2($pr['AccountDepartmentBudget']['total']); ?> </td> 
                </tr>
                <tr>
                    <td>Budget Used</td>
                    <td><?php echo _n2(abs($pr['AccountDepartmentBudget']['used'])); ?> </td> 
                </tr>

                <tr>
                    <td>Budget Balance (A)</td>
                    <td><?php echo _n2($pr['AccountDepartmentBudget']['balance']); ?> </td> 
                </tr>

                <tr>
                    <td>This Purchase (B)</td>
                    <td><?php echo _n2($after_discount); ?></td> 
                </tr>   
                <tr>
                    <td>Other Purchase (C)</td>
                    <td><?php echo _n2($total_waiting); ?></td> 
                </tr>   
                <tr>
                    <td>Total Demand (B + C)</td>
                    <td><?php 
                    $total_demand = $after_discount + $total_waiting;
                    echo _n2($total_demand); 

                    $after_balance = $pr['AccountDepartmentBudget']['balance'] - $total_demand;
                    ?></td> 
                </tr>
                <tr<?php if($after_balance < 0) { echo ' class="red-bg"'; } else { echo 'class="green-bg"'; } ?>>
                    <td>Balance (A - B + C)</td>
                    <td><b><?php  
                    echo _n2($after_balance); 
                    ?></b></td> 
                </tr>
            </table>

                </div>
                <!-- content end -->
            </div>
        </div>
    </div> 
</div>

<?php
    /*
    [4/19, 7:21 PM] azrulharis: 0 Draft
1 Submit to Hos
2 Hos rjct
3 Hos approve (waiting prc) 
4 Prc rjct
5 Prc approve (Waiting hod) 
6 HOD rjct
7 HOD approve
[4/19, 7:27 PM] azrulharis: Find lofa by amount
[4/19, 7:30 PM] azrulharis: 8 MD rjct
9 MD approved (lofa not required)
10 Insuficent budget
11 Convert to PO
*/
function status($status) {
    if($status == 0) {
        $data = 'Draft';
    }
    if($status == 1) {
        $data = 'Submitted To HOS';
    }
    if($status == 2) {
        $data = 'HOS Rejected';
    }
    if($status == 3) {
        $data = 'Waiting Procurement';
    }
    if($status == 4) {
        $data = 'Rejected By Procurement';
    }
    if($status == 5) {
        $data = 'Waiting HOD';
    }
    if($status == 6) {
        $data = 'Rejected By HOD';
    }
    if($status == 7) {
        $data = 'Waiting MD';
    }
    if($status == 8) {
        $data = 'MD Rejected';
    }
    if($status == 9) {
        $data = 'Approved';
    }
    if($status == 10) {
        $data = 'Insuficent Budget';
    } 
    if($status == 11) {
        $data = 'Purchased';
    } 
    return $data;
}

function remarkStatus($status) {
    if($status == 0) {
        $data = 'Draft';
    }
    if($status == 1) {
        $data = 'Submitted To HOS';
    }
    if($status == 2) {
        $data = 'HOS Rejected';
    }
    if($status == 3) {
        $data = 'Verified';
    }
    if($status == 4) {
        $data = 'Rejected';
    }
    if($status == 5) {
        $data = 'Verified';
    }
    if($status == 6) {
        $data = 'Rejected';
    }
    if($status == 7) {
        $data = 'Approved';
    }
    if($status == 8) {
        $data = 'MD Rejected';
    }
    if($status == 9) {
        $data = 'Approved';
    }
    if($status == 10) {
        $data = 'Insuficent Budget';
    } 
    if($status == 11) {
        $data = 'Purchased';
    } 
    return $data;
}
?>

<?php $this->start('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var id_sups = $('#sid').val();
        $('#supplier_id').val(id_sups);

        var row_count = $('#count_row').val();
        
        for(i=1; i<=row_count; i++){
            if($('#id'+i). prop("checked") == true){               
                $('#q'+i).prop('disabled', false);
                $('#p_unit'+i).prop('disabled', false);
                $('#gu'+i).prop('disabled', false);
                $('#r'+i).prop('disabled', false);

                var guid = $('#gu_id'+i).val();
                $('#gu'+i).val(guid);
            }
        }

    });

    function getsupplieritem(supplier_id, item_id, row) {
        var cacheBuster = new Date().getTime();
         $.ajax({ 
            type: "GET", 
            dataType: 'json',
            data: {supplier_id: supplier_id, inventory_item_id: item_id, cache: cacheBuster},
            url: baseUrl + 'inventory_supplier_items/ajaxfinditembysupplier', 
            success: function(data) {  
                $('#genunit-'+row).val(data.InventorySupplierItem.min_order_unit);
                $('#unitprice-'+row).val(data.InventorySupplierItem.price);
                $('#currency-' + row).html(data.GeneralCurrency.iso_code);
                $('#quantity-'+row).val(data.InventorySupplierItem.min_order);
                $('#tax-'+row).val(data.Tax);

                var subtotal = data.InventorySupplierItem.min_order * data.InventorySupplierItem.price;
                $('#amount-'+row).val(subtotal);
                var totalRm = subtotal * data.GeneralCurrency.rate; 
                $('#totalrm-'+row).val(totalRm.toFixed(2));

                $('#lead-' + row).html(data.InventorySupplierItem.lead_time);
            }
        }); 
    }


    function checkbudget(id){
        var id = id;
        $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventory_purchase_requisitions/ajaxbudget', 
            success: function(data) { 
                //console.log(data);
                $('#a_budget').html(data);
            }
        }); 
        return false;  
    }

    function checkmode(mode){
        var mode = $("#mode option[value='"+mode+"']").text();
        if(mode == 'Others...'){
            $('#otherpaymentmode').show();
        }
        
    }

    $(document).ready(function() {
        var id = <?php echo $pr['InventoryPurchaseRequisition']['account_department_budget_id']; ?>;
        $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventory_purchase_requisitions/ajaxbudget', 
            success: function(data) {  
                $('#a_budget').html(data);
            }
        }); 
        return false;
    }); 
</script>
<?php $this->end(); ?>