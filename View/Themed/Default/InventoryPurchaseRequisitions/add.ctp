<?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm')); ?>

<div class="row"> 
    <div class="col-xs-12">
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Add Purchase Requisition'); ?></h2>  
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                <!-- content start-->
                
                <div class="inventoryPurchaseRequisitions form">
                <?php echo $this->Form->create('InventoryPurchaseRequisition', array('class'=>'form-horizontal', 'type' => 'file')); ?>
                    <fieldset>
                        <?php echo $this->Form->input('user_id', array("type"=>'hidden', 'value'=>$_SESSION['Auth']['User']['id'])); ?>    
                        
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">PR type</label>
                            <div class="col-sm-9" style="padding-top: 8px;">
                            <?php 
                                $default = '1';
                                $options=array('1'=>'&nbsp;CAPEX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','2'=>'&nbsp;OPEX&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', '3'=>'&nbsp;SERVING CLIENTS/CUSTOMERS');
                                $attributes=array('legend'=>false, 'value'=>$default, 'onclick' => 'toggleJob(this.value)', 'class' => 'pr_trpe');
                                echo $this->Form->radio('pr_type',$options,$attributes); ?>
                            </div> 
                        </div>   
                        <div class="form-group" id="jobs" style="display: none">
                            <label class="col-sm-3" style="padding-top: 8px">Job No</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("SaleJobChild.station_name", array("placeholder"=>"Job No", "id"=>"job", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false)); ?>
                           
                            <?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden', 'value' => 0)); ?>  
                        <?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden', 'value' => 0)); ?> 
                            </div> 
                        </div>             
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Required Date</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("dateline", array("required" => true, "type"=>"text", "id"=>"dateonly", "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Budget</label>
                            <div class="col-sm-9">

                            <?php echo $this->Form->input("account_department_budget_id", array("required" => true, "options" => $budgets, "empty" => "-Select Budget-", "id" => "budgets", "class" => "form-control", "label" => false)); ?> 
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Mode of Payment</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("term_of_payment_id", array("required" => true, "id"=>"mode", "class"=> "form-control", "empty" => "Please Select Terms", "options" => $terms, "label"=> false, "onchange"=>"checkmode(this.value)")); ?>
                            </div> 
                        </div>
                        <div class="form-group" id="otherpaymentmode" style="display: none;">
                            <label class="col-sm-3" style="padding-top: 8px">&nbsp;</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("new_term", array("type" => "textarea", "class"=> "form-control", "label"=> false, "required" => false)); ?>
                            </div> 
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Supplier</label>
                            <div class="col-sm-9">
                            <?php //echo $this->Form->input("inventory_supplier_id", array("id"=>"supplier_id", "options"=>$supplier, "empty"=>"Please Select Supplier", "class"=> "form-control", "label"=> false)); ?>
                            </div>
                            <input type="hidden" id="id_supplier"> 
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">PO Type</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("po_type", array("required" => true, "options"=>$po_type, "empty"=>"Please Select Type", "class"=> "form-control", "label"=> false)); ?>
                            </div>
                            <input type="hidden" id="id_supplier"> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Delivery Location</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("inventory_location_id", array("required" => true, "options" => $locations, "empty"=> "-Select Delivery Location-", "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Term of Delivery</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("term_of_delivery_id", array("required" => true, "options" => $tod, "empty"=>"-Select Term-", "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Referrence</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("ref", array("class"=> "form-control", "label"=> false, "required" => false, 'placeholder' => 'Quotation no / Job no')); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Total Discount (Optional)</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("bulk_discount", array("required" => false, "type"=>"text", "class"=> "form-control", "label"=> false, 'id' => 'bulk_discount')); ?>
                            <small>Note: Once you fill Total Discount, you are <b>not allow to add item by different supplier</b></small>
                            </div> 
                        </div>

                    </fieldset>
                    <div class="clearfix">&nbsp;</div>
                    <h4 class="text-right">Available Budget : <input type="text" name="a_budget" id="a_budget" class="text-center" readonly="readonly"></h4>
                    <h4><?php echo __('Item'); ?></h4>
                    <table cellspacing="1" cellpadding="1" class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-sm-3">Item</th>
                                <th class="col-sm-2">Note</th>
                                <th class="col-sm-2">Supplier</th>
                                <th class="col-sm-1">Curr.</th>
                                <th class="col-sm-1">Qty</th> 
                                <th class="col-sm-1">Unit Price</th>
                                <th class="col-sm-1">UOM</th>
                                <th class="col-sm-1">Discount</th>
                                <th class="col-sm-1">Disc.Type</th>
                                <th class="col-sm-1">Tax (%)</th> 
                                <th class="col-sm-1">Subtotal</th> 
                            </tr>
                        </thead>
                        <tbody id="p_scents" class="form-no-padding">
                            <tr>
                                <td><input type="text" class="form-control" placeholder="Item Code/Name" id="itemlist-0"><input type="hidden" name="inventory_item_id[]" class="form-control" id="itemid-0"required><br></td>
 

                                <td><textarea name="remark[]" id="remark-0" placeholder="Note/Remark" class="form-control max-200"></textarea></td>

                                <td><select id="supplier-0" name="supplier_id[]" class="form-control"required><option value="">-Select-</option><?php foreach($supplier as $key => $supp) { ?><option value="<?php echo $key; ?>"><?php echo h($supp); ?></option><?php } ?></select></td>

                                <td><select class="form-control" name="general_currency_id[]" id="supplier-0" required>
                                <option value="">-Select-</option>
                                <?php foreach ($gcurrencies as $currency) { ?>
                                    <option value="<?php echo $currency['GeneralCurrency']['id']; ?>"><?php echo $currency['GeneralCurrency']['symbol']; ?></option>
                                <?php } ?>
                                </select></td>

                                <td>
                                <input type="hidden" id="supplierid0">
                                <input type="text" class="form-control calc" name="quantity[]" id="quantity-0"></td>
                                <td>
                                <input type="text" class="form-control calc" name="unit_price[]" id="unit_price-0"required></td>
                                
                                <td>
                                <select name="general_unit[]" id="genunit-0" class="form-control"required><option value="0">UOM</option><?php foreach ($generalUnits as $key => $gUnit) { ?><option value="<?php echo $key; ?>"><?php echo $gUnit; ?></option>
                        <?php } ?></select></td>
                                <td><input type="text" class="form-control calc" name="discount[]" id="discount-0" value="0"></td>
                                <td><select class="form-control calc" id="dis_type-0" name="discount_type[]"><option value="x">Type</option><option value="0">Fix</option><option value="1"> % </option></select></td>
                                <td><input type="text" class="form-control calc" name="tax[]" id="tax-0"></td>
                                <td>
                                <input type="text" class="form-control sub" name="sub[]" id="sub-0">
                                </td>
                            </tr>  
                        </tbody>
                    </table>
                    <table class="table">
                    <tr>
                    <td><button type="button" class="btn btn-sm btn-success" id="plusbtn"><i class="fa fa-plus"></i> Add More Item</button></td>
                    <td class="align-right"><p>Subtotal: <span id="subtotal">0.00</span></p></td>
                    </tr>
                     
                    <tr>
                    <td></td>
                    <td class="align-right"><p>Total: <span id="grandtotal">0.00</span></p></td>
                    </tr>
                    </table>
                    
                    <div id="output">
                    </div>
                    <div id="loadMoreItem">
                    
                    </div>
                    <?php echo $this->Form->input("dir", array("type"=>"hidden")); ?>
                    <?php echo $this->Form->input("approval_dir", array("type"=>"hidden")); ?>
                    <div class="clearfix">&nbsp;</div>
                    <h4><?php echo __('Quotation Attachment (Optional)'); ?></h4>
                    <div><?php echo $this->Form->input("quotation", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?></div>
                    <div class="clearfix">&nbsp;</div>
                    <h4><?php echo __('MTC Approval Attachment (Optional)'); ?></h4>
                    <div><?php echo $this->Form->input("approval", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?></div>
                    <div class="clearfix"><br>&nbsp;</div>

                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Remark (Optional)</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("remark", array("id" => "rmk", "options" => $remark, "empty" => "Select Remark", "class"=> "form-control", "label"=> false, "onchange" => "checkremark(this.value)")); ?>
                        </div> 
                    </div>
                    <div class="form-group" id="otherremark" style="display: none;">
                        <label class="col-sm-2" style="padding-top: 8px">&nbsp;</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("new_remark", array("type" => "textarea", "class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Warranty</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("warranty_id", array("id" => "wrty", "options" => $warranty, "empty" => "Select Warranty", "class"=> "form-control", "label"=> false, "onchange" => "checkwarranty(this.value)")); ?>
                        </div> 
                    </div>
                    <div class="form-group" id="otherwarranty" style="display: none;">
                        <label class="col-sm-2" style="padding-top: 8px">&nbsp;</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("new_warranty", array("type" => "textarea", "class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Status</label>
                        <div class="col-sm-10">
                        <?php 
                        $status = 0;
                        $statusoptions=array(0 => 'Save as Draft &nbsp &nbsp &nbsp', 1 => 'Submit for Approval');
                        $statusattributes=array('legend'=>false, 'value'=>$status);
                        echo $this->Form->radio('status',$statusoptions, $statusattributes); ?>

                        </div> 
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div>
                    <?php 
                        //echo $this->Form->button('Reset', array('type'=>'reset', 'class' => 'btn btn-danger btn-sm','div' => false));
                        echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success'));
                    ?>
                    </div>
                </div>
                <!-- content end -->
                <?php $this->Form->end(); ?>
            </div>
        </div>
    </div> 
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
function calcItem(id, val) {
    $('.calc').on('keyup, change', function() { 
        attrId = id;
        //unit_price quantity sub tax dis_type discount
        var unit_price = $('#unit_price-'+attrId).val();
        var quantity = $('#quantity-'+attrId).val();
        var tax = $('#tax-'+attrId).val();
        var discount = $('#discount-'+attrId).val();
        var dis_type = $('#dis_type-'+attrId).val();
        var subtotal = 0;
        if(unit_price != '' && quantity != '') {
            subtotal = unit_price * quantity;
        }
        if(discount != '') {
            if(dis_type == 0) {
                // Fix
                subtotal = subtotal - discount;
            } 
            if(dis_type == 1) {
                // %
                var dis = (subtotal / 100) * discount;
                subtotal = subtotal - dis;
            }
        }
        if(tax != '') {
            var taxamt = (subtotal / 100) * tax;
            subtotal = subtotal + taxamt;
        }
        $('#sub-'+attrId).val(subtotal);
        sumTotal(val);
    }); 
}

function sumTotal(val, bulkDis) {
    var sum = 0;
    $('.sub').each(function() {
        sum += Number($(this).val());
    });
    console.log(sum);
    $('#bulkdiscount').html(bulkDis);
    $('#grandtotal').html(sum);
}
$(document).ready(function() { 
    var bulkDiscount = $('#bulk_discount').val();
    $('.calc').each(function() {
        $(this).on('keyup, change', function() {
            var attrId = $(this).attr('id');
            attrId = attrId.split('-');
            attrId = attrId[1];
            //unit_price quantity sub tax dis_type discount bulk_discount
            
            var unit_price = $('#unit_price-'+attrId).val();
            var quantity = $('#quantity-'+attrId).val();
            var tax = $('#tax-'+attrId).val();
            var discount = $('#discount-'+attrId).val();
            var dis_type = $('#dis_type-'+attrId).val();
            var subtotal = 0;
            if(unit_price != '' && quantity != '') {
                subtotal = unit_price * quantity;
            }
            if(discount != '') {
                if(dis_type == 0) {
                    // Fix
                    subtotal = subtotal - discount;
                } 
                if(dis_type == 1) {
                    // %
                    var dis = (subtotal / 100) * discount;
                    subtotal = subtotal - dis;
                }
            }
            if(tax != '') {
                var taxamt = (subtotal / 100) * tax;
                subtotal = subtotal + taxamt;
            }
            $('#sub-'+attrId).val(subtotal);
            sumTotal($(this).val(), bulkDiscount);
        });    
    });
    
    var c = 1;
    $('#plusbtn').on('click', function() {  
        //console.log(ci);
        $('#p_scents').append('<tr id="row'+c+'"><td> <input type="text" placeholder="Item Code/Name" class="form-control" id="itemlist-'+c+'"required><input type="hidden" name="inventory_item_id[]" class="form-control" id="itemid-'+c+'"><br></td><td><textarea name="remark[]" class="form-control max-200" placeholder="Remark"></textarea></td><td><select id="supplier-'+c+'" name="supplier_id[]" class="form-control"required><option value="">-Select-</option><?php foreach($supplier as $key => $supp) { ?><option value="<?php echo $key; ?>"><?php echo h($supp); ?></option><?php } ?></select></td><td><select id="general_currency_id-'+c+'" class="form-control" name="general_currency_id[]" required> <option value="">-Select-</option> <?php foreach ($gcurrencies as $currency) { ?> <option value="<?php echo $currency['GeneralCurrency']['id']; ?>"><?php echo $currency['GeneralCurrency']['symbol']; ?></option> <?php } ?> </select></td><td><input type="hidden" id="supplierid-'+c+'"><input type="text" class="form-control calc" name="quantity[]" id="quantity-'+c+'"required></td><td><input type="text" class="form-control calc" name="unit_price[]" id="unit_price-'+c+'"required></td><td><select name="general_unit[]" id="genunit-'+c+'" class="form-control"required><option value="">UOM</option><?php foreach ($generalUnits as $key => $gUnit) { ?><option value="<?php echo $key; ?>"><?php echo $gUnit; ?></option><?php } ?></select></td><td><input type="text" class="form-control calc" name="discount[]" id="discount-'+c+'" value="0"></td><td><select class="form-control calc" id="dis_type-'+c+'" name="discount_type[]"><option value="x">Type</option><option value="0">Fix</option><option value="1"> % </option></select></td><td><input type="text" class="form-control calc" name="tax[]" id="tax-'+c+'"></td><td><input type="text" class="form-control sub" name="sub[]" id="sub-'+c+'"></td> <td><button type="button" class="btn btn-danger btn-sm" onclick="removerow('+c+')"><i class="fa fa-minus"></i></button></td></tr>');
        findItem(c, $(this).val());

        calcItem(c, $(this).val(), bulkDiscount);
        c++;
    });

    $('#budgets').click(function(){ 
        var value = $(".pr_trpe:checked").val();
        var job = $("#sale_job_child_id").val();
        if(value == 3 && job == 0) {
            $('#job').addClass('border-red');
        } else {
            if($('#job').hasClass('border-red')) {
                $('#job').toggleClass('border-red');
            }
        }
    }); 

    $('#budgets').on('change', function(){ 
        var id = $(this).val();
        $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventory_purchase_requisitions/ajaxbudget', 
            success: function(data) {  
                $('#a_budget').val(data);
            }
        }); 
        return false;  
    }); 
});
    function toggleJob(nilai){
        if(nilai == 3){
            $("#jobs").show();
        } else {
            $("#jobs").hide();
        }  
        $.ajax({ 
            type: "GET", 
            dataType: "json",
            data: 'type='+nilai+'&sale_job_child_id=0',
            cache: false,
            url: baseUrl + 'account_department_budgets/ajaxfindbudget', 
            success: function(data) { 
                console.log(data);
                var html = '<option value="">-Select Budget-</option>';
                $.each(data, function(i, item) { 
                     html += '<option value="'+ item['AccountDepartmentBudget']['id'] +'">'+item['AccountDepartmentBudget']['name']+ '</option>';
                });
                $('#budgets').html(html);
            } 
        });  
    }

    function deleteField(id) {
        $('#deleteId'+id).html('');
        return false; // prevent click link
    }
    function ajaxitem(supplier_id = null){
        var getParam = {
            supplier_id: $('#supplier_id').val()
        }
        $.ajax({ 
            type: "GET", 
            dataType: 'json',
            data: getParam,
            url: baseUrl + 'inventory_purchase_requisitions/ajaxitem', 
            success: function(data) { 
                //console.log(data);
                var html = '<select class="form-control" name="inventory_item_id[]" onchange="updatedetails(this.value)" id="item_id"><option value="0">Please Select Item</option>'; 
                var row = 0;
                $.each(data, function(i, item) { 
                    //console.log(item);
                    html += '<option value='+item.id+'>';
                    html += item.name;
                    html += '</option>';
                });
                html += '<option value="xxx">NEW ITEM</options>';
                html += '</select><div id="newitem" style="display:none"></div>';

                $('#id_supplier').val(supplier_id);
                $('#itemlist').html(html);
            }
        }); 
        return false;  
    };

    function checkbudget(id){
        
    };

    function checkmode(mode){
        var mode = $("#mode option[value='"+mode+"']").text();

        if(mode == 'Others...'){
            $('#otherpaymentmode').show();
        }
        
    }

    function checkremark(nilai){
        var mode = $("#rmk option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherremark').show();
        } else {
            $('#otherremark').hide();
        }
        
    }

    function checkwarranty(nilai){
        var mode = $("#wrty option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherwarranty').show();
        } else {
            $('#otherwarranty').hide();
        } 
    }

    function enabletextbox(rowid) {
        
        if($('#id'+rowid).prop("checked") == true){
            $('#q'+rowid).prop('disabled', false);
            $('#p_unit'+rowid).prop('disabled', false);
            $('#gu'+rowid).prop('disabled', false);
            $('#r'+rowid).prop('disabled', false);
        }
        else if($('#id'+rowid).prop("checked") == false){
            $('#q'+rowid).prop('disabled', true);
            $('#p_unit'+rowid).prop('disabled', true);
            $('#gu'+rowid).prop('disabled', true);
            $('#r'+rowid).prop('disabled', true);
        }
       
    };

    function updatedetails(nilai, row = null) { 
        if(nilai == 'xxx'){
            var supplier_id = $('#id_supplier').val();
            var getParam = {
                supplier_id: supplier_id
            }

            var scntDiv = $('#p_scents');
            var c = $('#p_scents tr').size()-1;
            var ci = c;

            //alert(ci);

            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventory_purchase_requisitions/ajaxitemgeneral', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(item);
                        
                        var html = '<br><select class="form-control" name="inventory_item_id[]" onchange="updatedetails_2(this.value, '+ci+')"><option value="0">Please Select Item</option>'; 
                        var row = 0;
                        $.each(data, function(i, item) { 
                            //console.log(item);
                            html += '<option value='+item.id+'>';
                            html += item.name;
                            html += '</option>';
                        });
                        html += '</select>';
                        //console.log(ci);

                        if(ci == '0'){
                            $('#newitem').show();
                            $('#newitem').html(html);
                        }else{
                            $('#newitem'+ci).show();
                            $('#newitem'+ci).html(html);
                            
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false;

        } else { 
            var getParam = {
                id: nilai
            }
        
            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventory_purchase_requisitions/ajaxitemdetails', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(row);
                        if(row == null || row == '0'){
                            $('#unit_price').val(item.unit_price);
                            $('#newitem').hide();
                        }else{
                            $('#unit_price'+row).val(item.unit_price);
                            $('#newitem'+row).hide();
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false; 
        } 
    }

    function updatedetails_2(nilai, row = null) { 
        var getParam = {
            id: nilai
        }
    
        $.ajax({ 
            type: "GET", 
            dataType: "json",
            data: getParam,
            cache: false,
            url: baseUrl + 'inventory_purchase_requisitions/ajaxitemdetails', 
            success: function(data) { 
                $.each(data, function(i, item) { 
                    //console.log(row);
                    if(row == null || row == '0'){
                        $('#unit_price').val(item.unit_price);
                        //$('#newitem').hide();
                    }else{
                        $('#unit_price'+row).val(item.unit_price);
                        //$('#newitem').hide();
                    }
                });
            },
            error: function(err) { 
                console.log(err);
                
            }
        }); 
        return false; 
        
    }

    function addrow(){ 
        var supplier_id = $('#supplier_id').val();
        var genUnit = $("#genunit").clone();
        var discount_type = $("#dis_type").clone();
        var itemlist = $("#itemlist").clone(); 
    }
    

    function removerow(nilai) {
        //alert(nilai);
        $('#row'+nilai).remove();
        
    };

    function getsupplier($id){
        var iditem = $('#itemid0').val();
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'inventory_suppliers/ajaxfindsupplier',           
            contentType: "application/json",
            dataType: "json",
            data: "itemId=" + $id,                                                    
            success: function (data) { 
                console.log(data);
                var html = '<select class="form-control" onchange="chgsupplier(this.value, '+iditem+')">';
                for(i=0; i<data.length; i++){
                    html += '<option value="'+data[i]['id']+'">'+data[i]['supplier']+'</option>';
                }
                html += '</select>';
                $('#supplier-0').html(html);
            }
        });
    }

    function getsupplier_2($id, row){
        var iditem = $('#itemid'+row).val();
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'inventory_suppliers/ajaxfindsupplier',           
            contentType: "application/json",
            dataType: "json",
            data: "itemId=" + $id,                                                    
            success: function (data) { 
                console.log(data);
                var html = '<select class="form-control" onchange="chgsupplier_2(this.value, '+iditem+', '+row+')">';
                for(i=0; i<data.length; i++){
                    html += '<option value="'+data[i]['id']+'">'+data[i]['supplier']+'</option>';
                }
                html += '</select>';
                $('#supplier-'+row).html(html);
            }
        });
    }

    function chgsupplier(nilai, id) {
        //alert(nilai);
        //alert(id);
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'inventory_supplier_items/ajaxfinditemdetail',           
            contentType: "application/json",
            dataType: "json",
            data: "itemId="+id+"&supplierId="+nilai,                                                    
            success: function (data) { 
                console.log(data);
                var min_quantity = data[0]['InventorySupplierItem']['min_order'];
                var u_price = data[0]['InventorySupplierItem']['price_per_unit'];
                var g_unit = data[0]['InventorySupplierItem']['general_unit_id'];
                var s_id = data[0]['InventorySupplier']['id'];
                if(data[0]['InventorySupplier']['gst'] == '' || data[0]['InventorySupplier']['gst'] == null){
                    var tax = '';
                }else{
                    var tax = 6;
                }

                $("#quantity-0").val(min_quantity);
                $("#supplierid-0").val(s_id);
                $("#unit_price-0").val(u_price);
                $("#genunit-0").val(g_unit);
                $("#tax-0").val(tax);
                
            }
        });
    }

    function chgsupplier_2(nilai, id, row){
        //alert(nilai);
        //alert(id);
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'inventory_supplier_items/ajaxfinditemdetail',           
            contentType: "application/json",
            dataType: "json",
            data: "itemId="+id+"&supplierId="+nilai,                                                    
            success: function (data) { 
                console.log(data);
                var min_quantity = data[0]['InventorySupplierItem']['min_order'];
                var u_price = data[0]['InventorySupplierItem']['price_per_unit'];
                var g_unit = data[0]['InventorySupplierItem']['general_unit_id'];
                var s_id = data[0]['InventorySupplier']['id'];
                if(data[0]['InventorySupplier']['gst'] == '' || data[0]['InventorySupplier']['gst'] == null){
                    var tax = '';
                }else{
                    var tax = 6;
                }

                $("#quantity"+row).val(min_quantity);
                $("#supplierid"+row).val(s_id);
                $("#unit_price"+row).val(u_price);
                $("#genunit"+row).val(g_unit);
                $("#tax"+row).val(tax);
                
            }
        });
    }
    function findItem(row, search) {   
        $('#itemlist-'+row).autocomplete({  
            source: function (request, response){ 
                $.ajax({
                    type: "GET",                        
                    url:baseUrl + 'inventory_supplier_items/ajaxfinditem_2',           
                    contentType: "application/json",
                    dataType: "json",
                    data: "term=" + $('#itemlist'+row).val(),
                    success: function (data) { 
                        
                         
                        response($.map(data, function (item) {
                            return {
                                id: item.id,
                                value: item.code,
                                //supplierid: item.InventorySupplier.id,
                                //supplier: item.InventorySupplier.name,
                                name : item.name,
                                //price: item.InventorySupplierItem.price,
                                code: item.code,
                                note: item.note,
                                //gst: item.InventorySupplier.gst,
                                //type: item.type,
                                //general_currency_id : item.InventorySupplierItem.general_currency_id,
                                unit: item.general_unit_id,
                                InventorySupplier: item.InventorySupplier,
                                InventorySupplierItem: item.InventorySupplierItem,
                                GeneralCurrency: item.GeneralCurrency,
                                Suppliers: item.Suppliers  
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) { 

                console.log('S-' + ui.item.InventorySupplier);

                if(ui.item.InventorySupplierItem != null) { 
                    $("#supplier-"+row).val(ui.item.InventorySupplier.id).change(); 
                    if(ui.item.InventorySupplier.gst == '' || ui.item.InventorySupplier.gst == null) {
                        tax = '';
                    } else {
                        tax = 6;
                    } 
                    $("#tax-"+row).val(tax); 
                    $("#quantity-"+row).val(ui.item.InventorySupplierItem.min_order);
                    $("#unit_price-"+row).val(ui.item.InventorySupplierItem.price_per_unit); 
                    $("#general_currency_id-"+row).val(ui.item.InventorySupplier.general_currency_id);
                } else {

                     $("#supplier-"+row).val('').change(); 

                    $("#tax-"+row).val(0); 
                    $("#quantity-"+row).val(1);
                    $("#unit_price-"+row).val(0); 
                    $("#general_currency_id-"+row).val(1);
                     
                }
                $("#itemid-"+row).val(ui.item.id);  
                $("#genunit-"+row).val(ui.item.unit).change(); 
                //getsupplier_2(ui.item.id, row);
            },
            minLength: 3
        }).autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li>" ).append( "<div>" + item.code + "<br><small>" + item.name + "</small><br/></div>" ).appendTo( ul );
        };  
    }

    $(function() { 
        $('#itemlist-0').autocomplete({  
            source: function (request, response){ 
                $.ajax({
                    type: "GET",                        
                    url:baseUrl + 'inventory_supplier_items/ajaxfinditem_2',           
                    contentType: "application/json", 
                    dataType: "json",
                    data: "term=" + $('#itemlist0').val(),
 
                    success: function (data) {   
                         
                        response($.map(data, function (item) {  
                            return {
                                id: item.id,
                                value: item.code,
                                //supplierid: item.InventorySupplier.id,
                                //supplier: item.InventorySupplier.name,
                                name : item.name,
                                //price: item.InventorySupplierItem.price,
                                code: item.code,
                                note: item.note,
                                //gst: item.InventorySupplier.gst,
                                //type: item.type,
                                //general_currency_id : item.InventorySupplierItem.general_currency_id,
                                unit: item.general_unit_id,
                                InventorySupplier: item.InventorySupplier,
                                InventorySupplierItem: item.InventorySupplierItem,
                                GeneralCurrency: item.GeneralCurrency,
                                Suppliers: item.Suppliers
                            }
                        }));    
                    }
                });
            }, 
            select: function (event, ui) {  
                
                if(ui.item.InventorySupplierItem != null) { 
                    $("#supplier-0").val(ui.item.InventorySupplier.id).change(); 
                    var tax = 0;
                    if(ui.item.InventorySupplier.gst == '' || ui.item.InventorySupplier.gst == null) {
                        tax = '';
                    } else {
                        tax = 6;
                    }
                    $("#tax-0").val(tax); 
                    $("#quantity-0").val(ui.item.InventorySupplierItem.min_order);
                    $("#unit_price-0").val(ui.item.InventorySupplierItem.price_per_unit); 
                    $("#general_currency_id-0").val(ui.item.InventorySupplier.general_currency_id);
                } else {
                    $("#tax-0").val(0); 
                    $("#quantity-0").val(1);
                    $("#unit_price-0").val(0); 
                    $("#general_currency_id-0").val(1);
                     
                }
                $("#itemid-0").val(ui.item.id);  
                $("#genunit-0").val(ui.item.unit).change(); 
                /* 
                if(ui.item.id == 0) {
                    alert('Price books not found. Please contact Procurement');
                    return false;
                }
                $(this).val( ui.item.value );  
                
                $("#supplierid0").val(ui.item.supplierid);
                $("#supplier0").val(ui.item.supplier);
                
                $("#general_currency_id0").val(ui.item.general_currency_id); 
                if(ui.item.gst == '' || ui.item.gst == null){
                    ui.item.gst = '';
                } else {
                    ui.item.gst = 6;
                }
                $("#tax0").val(ui.item.gst);
                getsupplier(ui.item.id);
                */
            },
            minLength: 3
        }).autocomplete( "instance" )._renderItem = function( ul, item ) {
             
            return $( "<li>" ).append( "<div>" + item.code + "<br><small>" + item.name + "<br/>"+item.note+"</small></div>" ).appendTo( ul );
        };


        $('#job').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'sale_jobs/ajaxfindjob',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#job').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            sale_job_child_id: item.sale_job_child_id,
                            value: item.name,
                            station: item.station
                             
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {   
            $('#sale_job_id').val(ui.item.id); 
            $('#sale_job_child_id').val(ui.item.sale_job_child_id); 
 
            // Find budget by job
            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: 'type=3&sale_job_child_id='+ui.item.sale_job_child_id,
                cache: false,
                url: baseUrl + 'account_department_budgets/ajaxfindbudget', 
                success: function(data) { 
                    
                    var html = '<option value="">-Select Budget-</option>';
                    $.each(data, function(i, item) { 
                         html += '<option value="'+ item['AccountDepartmentBudget']['id'] +'">'+item['AccountDepartmentBudget']['name']+ '</option>';
                    });
                    $('#budgets').html(html);
                },
                error: function(err) {  
                }
            }); 

            // Remove border red if it exist
            if($('#job').hasClass('border-red')) {
                $('#job').toggleClass('border-red');
            }
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "&nbsp;&nbsp;/&nbsp;&nbsp;<strong>" + item.station + "<br>" +  "</strong></div>" ).appendTo( ul );
    }; 

        //alert(ci);

          
    });

  </script>
  <?php $this->end(); ?>