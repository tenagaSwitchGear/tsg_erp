<?php echo $this->Html->link(__('Purchase Requisition'), array('action' => 'procurementindex'), array('class' => 'btn btn-info btn-sm')); ?>

 

<?php if($pr['InventoryPurchaseRequisition']['status'] == 9) { ?>
    <?php echo $this->Html->link(__('Create Purchase Order'), array('action' => 'procurementcreatepo', $pr['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-success btn-sm')); ?>
<?php } ?>

<div class="row"> 
    <div class="col-xs-12"> 
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Convert Purchase Requisition'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                    
                    <h4><?php echo __('Items'); ?></h4> 
                    <?php echo $this->Form->create('InventoryPurchaseRequisition', array("class"=>"form-horizontal")); ?>
                        <?php foreach($items as $supplier) { ?>
                        <input type="hidden" name="supplier[]" value="<?php echo $supplier['InventorySupplier']['id']; ?>"> 
                        <table cellspacing="1" cellpadding="1" class="table table-bordered">
                            <tr>
                                <th><?php echo $supplier['InventorySupplier']['name'] ?></th>
                                <th><?php echo $supplier['InventorySupplier']['city'] ?></th>
                                <th><?php echo $supplier['InventorySupplier']['phone_office'] ?></th>
                                <th><?php echo $supplier['InventorySupplier']['phone_mobile'] ?></th>
                                <th><?php echo $supplier['InventorySupplier']['company_expiry_date'] ?></th>  
                            </tr>
                        <tr>
                        <td colspan="5">
                        <table cellspacing="1" cellpadding="1" class="table table-bordered">
                        <thead>
                           <tr>
                    <th class="text-center"><?php echo __('#'); ?></th>
                    <th><?php echo __('Items'); ?></th>
                    <th><?php echo __('Supplier'); ?></th>
                    <th><?php echo __('Quantity'); ?></th>
                    <th><?php echo __('Unit Price'); ?></th>
                    <th><?php echo __('Discount'); ?></th>
                    <th><?php echo __('Tax(%)'); ?></th>
                    <th><?php echo __('Tax Amount'); ?></th>
                    <th class="align-right"><?php echo __('Amount'); ?></th>
                    <th class="align-right"><?php echo __('RM'); ?></th>
                </tr>
                        </thead>
                        <tbody id="p_scents">
                        <?php 
                $no = '1'; 
                $total_rm = 0;
                $total_tax = 0;
                $total_discount_amount = 0;
                        foreach($supplier['Items'] as $inventoryPurchaseRequisitionItem) {  
                           $item_price = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit'] * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity'];

                    ?>

                    <tr>
                        <td class="text-center"><?php echo $no++; ?></td>
                        <td>
                            <?php echo h($inventoryPurchaseRequisitionItem['InventoryItem']['code']); ?><br>
                            <?php  
                                echo '<small>'.h($inventoryPurchaseRequisitionItem['InventoryItem']['name']).'</small>';  
                            ?>
                            <br><small><?php echo h($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['remark']); ?></small>
                            <?php if($_SESSION['Auth']['User']['group_id'] == '11') { ?>
                            <?php echo $this->Html->link(('<i class="fa fa-external-link" aria-hidden="true"></i>'), array('controller'=>'inventory_purchase_requisition_items', 'action' => 'index', $inventoryPurchaseRequisitionItem['InventoryItem']['id']), array('escape'=>false, 'target'=>'_blank')); ?>
                            <?php } ?>
                            
                        </td>
                        <td><?php echo $inventoryPurchaseRequisitionItem['InventorySupplier']['name']; ?></td>
                        <td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']; ?><?php 
                                echo ' '.$inventoryPurchaseRequisitionItem['GeneralUnit']['name'];
                            ?></td>
                        <td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit']; ?></td>
                        <td>
                        <?php
                        if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type'] == '0'){
                            if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'] == '0.0000'){
                                echo "-";
                            }else{
                                echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 2);
                            }
                            $discount_amount = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'];
                        }else{
                            echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 0).' %';
                            $discount_amount = ($item_price / 100) * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount']; 
                        }
                        $total_discount_amount += $discount_amount;
                        ?>
                        </td>
                        <td>
                        <?php
                        if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'] == '0'){
                            echo "-";
                        }else{
                            echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'].' %';
                        }
                        ?>
                        </td>

                        <td>
                        <?php
                        if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'] == '0'){
                            $taxed = 0.00;
                        }else{
                            $tax = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'];
                            $taxed = (($item_price - $discount_amount) / 100) * $tax; 
                        }
                        $total_tax += $taxed;
                        echo _n2($taxed);
                        $taxed_value =  $taxed + $item_price;
                        ?>
                        </td>

                        <td class="align-right">
                        <?php 
                            echo $inventoryPurchaseRequisitionItem['GeneralCurrency']['iso_code']; 
                        ?> <?php $sub = $item_price - $discount_amount; echo $sub; 
                        ?></td>
                        <?php $rm = $sub * $inventoryPurchaseRequisitionItem['GeneralCurrency']['rate']; 
                        $total_rm += $rm;
                        ?>
                        <td class="align-right"><b><?php echo _n2($rm); ?></b></td>
                    </tr>

                            <input type="hidden" name="inventory__location_id" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisition']['inventory_location_id']; ?>">

                            <input type="hidden" name="inventory_item_id[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['inventory_item_id']; ?>">

                            <input type="hidden" name="quantity[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']; ?>">
                            <input type="hidden" name="general_unit_id[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['general_unit_id']; ?>">
                            <input type="hidden" name="inventory_supplier_item_id[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['inventory_supplier_item_id']; ?>">
                            <input type="hidden" name="inventory_supplier_id[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['inventory_supplier_id']; ?>">
                            <input type="hidden" name="price_per_unit[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit']; ?>">
                            <input type="hidden" name="amount[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $item_price; ?>">
                            <input type="hidden" name="discount[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount']; ?>">
                            <input type="hidden" name="discount_type[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type']; ?>">
                            <input type="hidden" name="tax[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax']; ?>">
                            <input type="hidden" name="planning_item_jobs[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['planning_item_jobs']; ?>">
                            <input type="hidden" name="delivery_remark[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['delivery_remark']; ?>">
                            <input type="hidden" name="general_currency_id[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['general_currency_id']; ?>">
                            <input type="hidden" name="total_rm[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $total_rm; ?>">
                            <input type="hidden" name="remark[<?php echo $supplier['InventorySupplier']['id'] ?>][]" value="<?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['remark']; ?>">
                        <?php 
                        $no++;
                        } ?> 

                        <tr>
                <td colspan="9" class="align-right"><b>Subtotal RM</b></td>
                <td class="align-right"><b><?php echo _n2($total_rm); ?></b></td>
                </tr>
                <tr>
                <td colspan="9" class="align-right"><b>Bulk Discount</b></td>
                <td class="align-right"><b><?php echo _n2($pr['InventoryPurchaseRequisition']['bulk_discount']); ?></b></td>
                </tr>
                <?php $after_discount = $total_rm - $pr['InventoryPurchaseRequisition']['bulk_discount']; ?>
                <tr>
                <td colspan="9" class="align-right"><b>Total RM</b></td>
                <td class="align-right"><b><?php echo _n2($after_discount); ?></b></td>
                </tr>
                <tr>
                <td colspan="9" class="align-right"><b>Tax</b></td>
                <td class="align-right"><b><?php echo _n2($total_tax); ?></b></td>
                </tr>
                <tr>
                <td colspan="9" class="align-right"><b>Grand Total RM</b></td>
                <td class="align-right"><b><?php echo _n2($after_discount + $total_tax); ?></b></td>
                </tr>

                   
                        </tbody>
                        </table>
                        </td>
                        </tr> 
                    </table>
                        <?php } ?>
                        
                     <div class="form-group"> 
                        <div class="col-sm-12">
                        <p>After click Save button, it will generate Purchase Order based on Supplier with status Draft. Please review every PO before submit to Supplier, then change status to Purchased.</p>
                        <?php echo $this->Form->submit('Generate PO', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
                        </div> 
                    </div>
                
                <?php echo $this->Form->end(); ?>
                    
                </div>
                <!-- content end -->
            </div>
        </div>
    </div> 
</div>
 