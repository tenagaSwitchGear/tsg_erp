<?php echo $this->Html->link(__('My Purchase Requisitions'), array('action' => 'index'), array('class' => 'btn btn-info')); ?>

<?php 
$rejected_status = array(0, 2, 4, 6, 8);
if(in_array($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'], $rejected_status)) { 
echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-warning')); 
}
?>

<?php 
if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'] == 0) { 
echo $this->Html->link(__('Submit Approval'), array('action' => 'view', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id'], 'Approval'), array('class' => 'btn btn-success')); 
}
?>

<?php echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'procurementview/'.$inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id'].'?print=TRUE'), array('class' => 'btn btn-default btn-sm', 'escape' => false, 'onclick' => 'printIframe(report);')); ?>

<style type="text/css">
.line {    
    border-bottom: 2px dotted #000;
    text-decoration: none;
}
.x_content a{
    color: #1E90FF !important;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">  
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Purchase Requisition'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="container table-responsive">
				<h4>Verifier &amp; Approval Remarks</h4>

                <table class="table table-bordered"> 
                    <tr>
                        <th>User</th>
                        <th>Remark</th> 
                        <th>Status</th> 
                        <th>Created</th> 
                        <th>Role</th>
                    </tr>
                    <?php foreach ($approvalremarks as $approvalremark) { ?> 
                        <tr>
                            <td><?php echo h($approvalremark['User']['firstname']); ?></td>
                            <td><?php echo h($approvalremark['InventoryPurchaseRequisitionRemark']['remark']); ?></td> 
                            <td><?php echo remarkStatus($approvalremark['InventoryPurchaseRequisitionRemark']['status']); ?></td>
                            <td><?php echo h($approvalremark['InventoryPurchaseRequisitionRemark']['created']); ?></td> 
                            <td><?php echo h($approvalremark['InventoryPurchaseRequisitionRemark']['type']); ?></td> 
                        </tr> 
                    <?php } ?>
                </table>
                <h4>Purchase Requisition Detail</h4>
				<table class="table table-bordered">
					<tr><td><?php echo __('PR Number'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_no']); ?>
						&nbsp;
					</td></tr>
					<tr><td><?php echo __('Username'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['User']['username']); ?>
						&nbsp;
					</td></tr>

					<tr><td><?php echo __('Full Name'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['User']['firstname']); ?>
						&nbsp;
					</td></tr>

					<tr><td><?php echo __('Phone'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['User']['mobile_number']); ?>
						&nbsp;
					</td></tr>

					<tr><td><?php echo __('Department'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['Group']['name']); ?>
						&nbsp;
					</td></tr>

					<?php if($inventoryPurchaseRequisition['SaleJobChild']['station_name']) { ?>
					<tr><td><?php echo __('Job'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['SaleJobChild']['station_name']); ?> (<?php echo ($inventoryPurchaseRequisition['SaleJobChild']['name']); ?>)
					</td></tr> 
					<?php } ?>
					<tr><td><?php echo __('Created'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['created']); ?>
						&nbsp;
					</td></tr>
					<tr><td><?php echo __('Require Date'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['dateline']); ?>
						&nbsp;
					</td></tr>
					<!-- <tr><td><?php //echo __('Budget'); ?></td>
					<td>
						: <?php //echo ($inventoryPurchaseRequisition['AccountDepartmentBudget']['name']); ?>
						&nbsp;
					</td></tr> -->
					<tr><td><?php echo __('Mode of payment'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['TermOfPayment']['name']); ?>
						&nbsp;
					</td></tr>
					<tr><td><?php echo __('Status'); ?></td>
					<td>
						: <?php echo status($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status']); ?>
					</td></tr>
					<tr><td><?php echo __('PR Type'); ?></td>
					<td>
						: <?php
                            if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_type']) == 1){
                                echo "CAPEX";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_type'])==2){
                                echo "OPEX";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_type'])==3){
                                echo "SERVING FOR CLIENT";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_type'])==4){
                                echo "SERVING FOR CLIENT";
                            }  
                        ?>
						&nbsp;
					</td></tr> 
					<tr><td><?php echo __('PO Type'); ?></td>
					<td>
						: <?php
                            if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['po_type']) == 1){
                                echo "PO";
                            } else { 
                                echo "NON PO";
                            }  
                        ?> 
					</td></tr> 

					<tr><td><?php echo __('Total Amount'); ?></td>
					<td>
						: <?php
							$total = 0;
						foreach ($items as $inventoryPurchaseRequisitionItem) {
							$item_price = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit'] * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity'];

							if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type'] == 0){
								 
							    $discount_amount = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'];
								 
							}else{ 
								$discount_amount = ($item_price / 100) * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount']; 
							}

								$total_rm = ($item_price * $inventoryPurchaseRequisitionItem['GeneralCurrency']['rate']) - $discount_amount;
								$total += $total_rm;
							} ?>
						<?php 
						$total = $total - $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount'];
						echo _n2($total); ?>
						&nbsp;
					</td></tr>

					<tr><td><?php echo __('Budget'); ?></td>
					<td>
						: <?php echo $inventoryPurchaseRequisition['AccountDepartmentBudget']['name']; ?> 
					</td></tr>
					<tr><td><?php echo __('Budget Balance'); ?></td>
					<td>
						: <?php echo _n2($inventoryPurchaseRequisition['AccountDepartmentBudget']['balance']); ?> 
					</td>
					</tr>

					<tr>
					<td><?php echo __('Approval Authority'); ?></td>
					<td>
						:  
						<?php foreach($lofas as $lofa) { ?>
							<?php if($lofa['InternalLofa']['price_min'] <= $total && $lofa['InternalLofa']['price_max'] >= $total) { ?>
								<?php echo $lofa['InternalLofa']['role']; ?>
							<?php } ?>
						<?php } ?>
						&nbsp;
					</td>
					</tr>
					<tr><td><?php echo __('Remark'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['Remark']['name']); ?>
						&nbsp;
					</td>
					</tr>
					<tr>
					<td><?php echo __('Warranty'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['Warranty']['name']); ?>
						&nbsp;
					</td>
					</tr>
					<tr>
					<td><?php echo __('Quotation Attachment'); ?></td>
					<td>
						: <?php if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['quotation'] != null) { ?>
                            <?php echo $this->Html->link($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['quotation'], '/files/inventory_purchase_requisition/quotation/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['dir'] . '/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['quotation'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            Not available
                        <?php } ?>
						&nbsp;
					</td>
					</tr>
					<tr>
					<td><?php echo __('MTC Approval Attachment'); ?></td>
					<td>
						: <?php if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['approval'] != null) { ?>
                            <?php echo $this->Html->link($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['approval'], '/files/inventory_purchase_requisition/approval/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['approval_dir'] . '/' . $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['approval'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            Not available
                        <?php } ?>
						&nbsp;
					</td>
					</tr>

					<tr>
					<td><?php echo __('Term of Delivery'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['TermOfDelivery']['name']); ?>
						&nbsp;
					</td>
					</tr>

					<tr>
					<td><?php echo __('Total Discount'); ?></td>
					<td>
						: <?php echo ($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount']); ?>
						&nbsp;
					</td>
					</tr>
				</table> 

				<?php if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status']) == 10){ ?>
				<?php echo $this->Html->link(__('Request Budget Transfer'), array('controller'=>'accountdepartmenttransferbudget', 'action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>
				<?php } ?>

			</div>
			
			
			<div class="clearfix">&nbsp;</div>
			<div class="related">
				<h4><?php echo __('Purchase Requisition Items'); ?></h4>
				<?php if (!empty($inventoryPurchaseRequisition['InventoryPurchaseRequisitionItem'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
				<tr>
					<th class="text-center"><?php echo __('#'); ?></th>
					<th><?php echo __('Items'); ?></th>
                    <th><?php echo __('Supplier'); ?></th>
					<th><?php echo __('Quantity'); ?></th>
					<th><?php echo __('Unit Price'); ?></th>
					<th><?php echo __('Discount'); ?></th>
					<th><?php echo __('Tax'); ?></th>
					<th><?php echo __('Tax Amount'); ?></th>
					<th class="align-right"><?php echo __('Amount'); ?></th>
					<th class="align-right"><?php echo __('RM'); ?></th>
				</tr>

				<?php

				// Check bulk discount
				if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount'] > 0) {
					$bulk = $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount'];
				} else {
					$bulk = 0;
				}
				// Header for sum item in pr
				$header_total = 0;
				//var_dump($items[0]["InventorySupplier"]["gst"]);
				foreach ($items as $inventoryPurchaseRequisitionItem) {  
					if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type'] == '0'){
						if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'] == '0.0000'){
							echo "-";
						}else{
							echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 2);
						}
						$discount_amount = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'];
					}else{
						echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 0).' %';
						$discount_amount = ($item_price / 100) * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount']; 
					}

					$header_total += $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit']  * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity'] - $discount_amount; 
				}

				if($items[0]["InventorySupplier"]["gst"] != '' || $items[0]["InventorySupplier"]["gst"] != null) {
					$is_gst = 6;
				} else {
					$is_gst = 0;
				}

				?>

				<?php 
				$no = '1'; 
				$total_rm = 0;
				$total_tax = 0;
				$total_discount_amount = 0;
				$calc_gst = 0;
				foreach ($items as $inventoryPurchaseRequisitionItem): 
					$item_price = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit'] * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']; 

					?>

					<tr>
						<td class="text-center"><?php echo $no++; ?></td>
						<td>
							<?php echo h($inventoryPurchaseRequisitionItem['InventoryItem']['code']); ?><br>
							<?php  
								echo '<small>'.h($inventoryPurchaseRequisitionItem['InventoryItem']['name']).'</small>';  
							?>
                            <br><small><?php echo h($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['remark']); ?></small>
							<?php if($_SESSION['Auth']['User']['group_id'] == '11') { ?>
							<?php echo $this->Html->link(('<i class="fa fa-external-link" aria-hidden="true"></i>'), array('controller'=>'inventory_purchase_requisition_items', 'action' => 'index', $inventoryPurchaseRequisitionItem['InventoryItem']['id']), array('escape'=>false, 'target'=>'_blank')); ?>
							<?php } ?>
							
						</td>
                        <td><?php echo $inventoryPurchaseRequisitionItem['InventorySupplier']['name']; ?></td>
						<td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']; ?><?php 
								echo ' '.$inventoryPurchaseRequisitionItem['GeneralUnit']['name'];
							?></td>
						<td><?php echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['price_per_unit']; ?></td>
						<td>
						<?php
						if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount_type'] == '0'){
							if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'] == '0.0000'){
								echo "-";
							}else{
								echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 2);
							}
							$discount_amount = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'];
						}else{
							echo number_format($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount'], 0).' %';
							$discount_amount = ($item_price / 100) * $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['discount']; 
						}
						$total_discount_amount += $discount_amount;
						?>
						</td>
						<td>
						<?php
						if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'] == '0'){
							echo "-";
						}else{
							echo $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'].' %';
						}
						?>
						</td>

						<td>
						<?php
						if($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'] == '0'){
							$tax = 0;
							$taxed = 0.00;
						}else{
							$tax = $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['tax'];
							$taxed = (($item_price - $discount_amount) / 100) * $tax; 
						}
						$total_tax += $taxed;
						echo _n2($taxed);
						$taxed_value =  $taxed + $item_price;
						?>
						</td> 
						
						<td>
						<?php 
							echo $inventoryPurchaseRequisitionItem['GeneralCurrency']['iso_code']; 
						?> 
						<?php 
						$sub = $item_price - $discount_amount; 
						echo $sub; 
						

						?></td>
						<?php $rm = $sub * $inventoryPurchaseRequisitionItem['GeneralCurrency']['rate']; 
						$total_rm += $rm;

						// Calculating for bulk & gst
						//if($bulk > 0) {
							$a = ($bulk / $header_total) * $sub;
							$b = $sub - $a;

							 
								$calc_sum_tax = ($b * $tax) / 100;
								$calc_gst += $calc_sum_tax;
							 
						//} else {
						//	$calc_sum_tax = 0;
						//}
						
						?> 
						<td class="align-right"><b><?php echo _n2($rm); ?></b></td>
					</tr>
				<?php endforeach; ?>
				<tr>
				<td colspan="9" class="align-right"><b>Subtotal RM</b></td>
				<td class="align-right"><b><?php echo _n2($total_rm); ?></b></td>
				</tr>
				<tr>
				<td colspan="9" class="align-right"><b>Bulk Discount</b></td>
				<td class="align-right"><b><?php echo _n2($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount']); ?></b></td>
				</tr>
				<?php 
				if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount'] > 0) { 
					$bulk_tax = $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount'] / 100 * 6;	
				} else {
					$bulk_tax = 0;
				}
				
				$after_discount = $total_rm - $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['bulk_discount']; 
				$sum_tax = $total_tax - $bulk_tax;
				?>
				<tr>
				<td colspan="9" class="align-right"><b>Total RM</b></td>
				<td class="align-right"><b><?php echo _n2($after_discount); ?></b></td>
				</tr>
				<tr>
				<td colspan="9" class="align-right"><b>Tax</b></td>

				<td class="align-right"><b><?php 
				echo _n2($calc_gst); 
				 

				?></b></td>
				</tr>
				<tr>
				<td colspan="9" class="align-right"><b>Grand Total RM</b></td>
				<td class="align-right"><b><?php echo _n2($after_discount + $calc_gst); ?></b></td>
				</tr>

				</table>
				<?php else: ?>
					&bull; No Purchase Requisition Item
				<?php endif; ?>
			<?php if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'] < 11) { ?> 
				<div class="clearfix">&nbsp;</div>
				<h4><?php echo __('Budget Control'); ?></h4>
			<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
				<tr>  
                    <th>Subject</th>
					<th>Amount (RM)</th> 
				</tr> 
				<tr>
					<td>Budget</td>
					<td><?php echo $inventoryPurchaseRequisition['AccountDepartmentBudget']['name']; ?> </td> 
				</tr>
				<tr>
					<td>Budget Total</td>
					<td><?php echo _n2($inventoryPurchaseRequisition['AccountDepartmentBudget']['total']); ?> </td> 
				</tr>
				<tr>
					<td>Budget Used</td>
					<td><?php echo _n2($inventoryPurchaseRequisition['AccountDepartmentBudget']['used']); ?> </td> 
				</tr>

				<tr>
					<td>Budget Balance (A)</td>
					<td><?php echo _n2($inventoryPurchaseRequisition['AccountDepartmentBudget']['balance']); ?> </td> 
				</tr>

				<tr>
					<td>This Purchase (B)</td>
					<td><?php echo _n2($after_discount); ?></td> 
				</tr>	
				<tr>
					<td>Other Purchase (C)</td>
					<td><?php echo _n2($total_waiting); ?></td> 
				</tr>	
				<tr>
					<td>Total Demand (B + C)</td>
					<td><?php 
					$total_demand = $after_discount + $total_waiting;
					echo _n2($total_demand); 

					$after_balance = $inventoryPurchaseRequisition['AccountDepartmentBudget']['balance'] - $total_demand;
					?></td> 
				</tr>
				<tr<?php if($after_balance < 0) { echo ' class="red-bg"'; } else { echo 'class="green-bg"'; } ?>>
					<td>Balance (A - B + C)</td>
					<td><b><?php  
					echo _n2($after_balance); 
					?></b></td> 
				</tr>
			</table>
			<?php } ?>
			</div>
			
			
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php

function status($status) {
    if($status=='0'){
        return "Draft";
    }else if($status=='1'){
        return "Waiting HOS Verification";
    }else if($status=='2'){
        return "HOS Rejected";
    }else if($status=='3'){
        return "Waiting Procurement Verification";
    }else if($status=='4'){
        return "Procurement Rejected";
    }else if($status=='5'){
        return "Waiting HOD Approval";    
    }else if($status=='6'){
        return "HOD Rejected";
    }else if($status=='7'){
        return "Waiting MD";
    }else if($status=='8'){
        return "MD Rejected";
    }else if($status=='9'){
        return "Approved";
    }else if($status=='10'){
        return "Insufficient Budget";
    }else if($status=='11'){
        return "Purchased";
    }
}

function remarkStatus($status) {
    if($status == 0) {
        $data = 'Draft';
    }
    if($status == 1) {
        $data = 'Submitted To HOS';
    }
    if($status == 2) {
        $data = 'HOS Rejected';
    }
    if($status == 3) {
        $data = 'Verified';
    }
    if($status == 4) {
        $data = 'Rejected';
    }
    if($status == 5) {
        $data = 'Verified';
    }
    if($status == 6) {
        $data = 'Rejected';
    }
    if($status == 7) {
        $data = 'Approved';
    }
    if($status == 8) {
        $data = 'MD Rejected';
    }
    if($status == 9) {
        $data = 'Approved';
    }
    if($status == 10) {
        $data = 'Insuficent Budget';
    } 
    if($status == 11) {
        $data = 'Purchased';
    } 
    if($status == 20) {
        $data = 'Notify User';
    }
    return $data;
}

?>