<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Purchase Requisitions'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('Purchase Requisitions'), array('action' => 'index'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Approved'), array('action' => 'approved'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Pending'), array('action' => 'pending'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Draft'), array('action' => 'draft'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Insufficient Budget'), array('action' => 'insufficient_budget'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Waiting HOD Approval'), array('action' => 'hod'), array('class'=>'btn btn-default btn-sm')); ?>
        <?php echo $this->Html->link(__('Waiting GM Approval'), array('action' => 'gm'), array('class'=>'btn btn-default btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Inventory Item'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Waiting for GM Approval - Purchase Requisitions'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
            <?php echo $this->Form->create('InventoryPurchaseRequisition', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
            <table cellpadding="0" cellspacing="0" class="table">
                <tr>
                <td><?php echo $this->Form->input('pr_no', array('placeholder' => 'PR Number', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_start', array('type' => 'text', 'placeholder' => 'From Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_end', array('type' => 'text', 'placeholder' => 'To Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker_2', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('status', array('options' => array('x'=>'Select Status', '0'=>'Reject', '1'=>'Draft', '2'=>'Pending', '3'=>'Waiting for HOD approval', '4'=>'Waiting for GM approval', '9'=>'Approved', '5'=>'Insufficient Budget'), 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
                </tr>
            </table>
            <?php $this->end(); ?>  
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
							<th><?php echo $this->Paginator->sort('PR Number'); ?></th>
							<th><?php echo $this->Paginator->sort('created'); ?></th>
							<th><?php echo $this->Paginator->sort('dateline'); ?></th>
							<th><?php echo $this->Paginator->sort('status'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseRequisition']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryPurchaseRequisitions as $inventoryPurchaseRequisition): 
                        //if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status']=='9'){
					?>
					<tr>
						<td class="text-center"><?php echo $startSN++; ?></td>
						<td><?php echo ucfirst($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_no']); ?>&nbsp;</td>
						<td><?php echo h($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['created']); ?>&nbsp;</td>
						<td><?php echo ucfirst($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['dateline']); ?>&nbsp;</td>
						<td>
                        <?php
                            if($_SESSION['Auth']['User']['group_id']=='1'){
                        ?>
                        <?php
                            if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='0'){
                                echo "Reject";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='1'){
                                echo "Draft";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='2'){
                                echo "Pending";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='3'){
                                echo "Waiting for HOD approval";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='4'){
                                echo "Waiting for GM approval";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='9'){
                                echo "Approved";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='5'){
                                echo "Insufficient Budget";
                            }
                            
                        ?>
                        <?php }else if($_SESSION['Auth']['User']['group_id']=='12'){ ?>
                        <?php 
                            if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='0'){
                                echo "Reject";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='1'){
                                echo "Draft";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='2'){
                                echo "Pending";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='3'){
                                echo "Waiting for HOD approval";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='4'){
                                echo "Waiting for GM approval";
                            }
                        ?>
                        <?php }else if($_SESSION['Auth']['User']['group_id']=='13'){ ?>
                        <?php 
                            if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='0'){
                                echo "Reject";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='1'){
                                echo "Draft";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='2'){
                                echo "Waiting for HOS approval";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='3'){
                                echo "Pending";
                            }else if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='4'){
                                echo "Waiting for GM approval";
                            }
                        ?>
                        <?php } ?>
                        &nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
                            <?php if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status']!='9'){ ?>
                            <?php if($_SESSION['Auth']['User']['group_id']=='12' || $_SESSION['Auth']['User']['group_id']=='13'){}else{ ?>
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
                            <?php } ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_no']).'"', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id'])); ?>
                            <?php } ?>

						</td>
					</tr>
                    <?php // } ?>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

