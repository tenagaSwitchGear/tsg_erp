<style type="text/css">
.line {    
    border-bottom: 2px dotted #000;
    text-decoration: none;
}
.x_content a{
    color: #1E90FF !important;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Inventory Purchase Requisitions'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Purchase Requisition'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="container table-responsive">
				<dl>
					<dt class="col-sm-2"><?php echo __('PR Number'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['pr_no']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['created']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Dateline'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['dateline']); ?>
						&nbsp;
					</dd>
					<!-- <dt class="col-sm-2"><?php //echo __('Budget'); ?></dt>
					<dd class="col-sm-9">
						: <?php //echo ($inventoryPurchaseRequisitions['AccountDepartmentBudget']['name']); ?>
						&nbsp;
					</dd> -->
					<dt class="col-sm-2"><?php echo __('Mode of payment'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisitions['TermOfPayment']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Status'); ?></dt>
					<dd  class="col-sm-9">
						: <?php
                            if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='0'){
                                echo "Draft";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='1'){
                                echo "Submit to HOS";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='2'){
                                echo "HOS Rejected";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='3'){
                                echo "Waiting Procurement Verification";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='4'){
                                echo "Procurement Rejected";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='5'){
                                echo "Waiting HOD Approval";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='6'){
                                echo "HOD Rejected";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='7'){
                                echo "HOD Approved";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='8'){
                                echo "MD Rejected";
                            }else if(($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['status'])=='9'){
                                echo "MD Approved";
                            }
                        ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Total Amount'); ?></dt>
					<dd class="col-sm-9">
						: <?php
							$new_amount = '';
							foreach ($inventoryPurchaseRequisitions['InventoryPurchaseRequisitionItem'] as $inventoryPurchaseRequisitionItem){
								$amount = $inventoryPurchaseRequisitionItem['amount'];
								$new_amount += $new_amount + $amount;	
							} ?>
						<?php echo number_format($new_amount, 4); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Remark'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisitions['Remark']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Warranty/Guarantee'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo ($inventoryPurchaseRequisitions['Warranty']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Quotation'); ?></dt>
					<dd class="col-sm-9">
						: <?php if($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['quotation'] != null) { ?>
                            <?php echo $this->Html->link($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['quotation'], '/files/inventory_purchase_requisition/quotation/' . $inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['dir'] . '/' . $inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['quotation'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            Not available
                        <?php } ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('MTC Approval'); ?></dt>
					<dd class="col-sm-9">
						: <?php if($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['approval'] != null) { ?>
                            <?php echo $this->Html->link($inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['approval'], '/files/inventory_purchase_requisition/approval/' . $inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['approval_dir'] . '/' . $inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['approval'], array('target' => '_blank', 'class'=>'line')); ?>
                        <?php } else { ?> 
                            Not available
                        <?php } ?>
						&nbsp;
					</dd>
				</dl>
			</div>
			
			<div class="clearfix">&nbsp;</div>
			
			<div class="related">
				<h4><?php echo __('Purchase Requisition Items'); ?></h4>
				<?php if (!empty($inventoryPurchaseRequisitions['InventoryPurchaseRequisitionItem'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
				<tr>
					<th class="text-center"><?php echo __('#'); ?></th>
					<th><?php echo __('Items'); ?></th>
					<th><?php echo __('Supplier'); ?></th>
					<th><?php echo __('Quantity'); ?></th>
					<th><?php echo __('Unit Price'); ?></th>
					<th><?php echo __('Discount'); ?></th>
					<th><?php echo __('Tax'); ?></th>
					<th><?php echo __('Amount'); ?></th>
				</tr>
				<?php $no = '1'; 
				foreach ($inventoryPurchaseRequisitions['InventoryPurchaseRequisitionItem'] as $inventoryPurchaseRequisitionItem): ?>
					<tr>
						<td class="text-center"><?php echo $no++; ?></td>
						<td>
							<?php echo '<strong>CODE : '.strtoupper($inventoryPurchaseRequisitionItem['InventoryItem']['code']).'</strong>'; ?><br>
							<?php 
							//print_r($inventoryPurchaseRequisitionItem);
									echo '<strong>'.$inventoryPurchaseRequisitionItem['InventoryItem']['name'].'</strong>';
									
							?>
							<br><?php echo $inventoryPurchaseRequisitionItem['remark']; ?>
							<?php if($_SESSION['Auth']['User']['group_id']=='11'){ ?>
							<?php echo $this->Html->link(('<i class="fa fa-external-link" aria-hidden="true"></i>'), array('controller'=>'inventory_purchase_requisition_items', 'action' => 'index', $inventoryPurchaseRequisitionItem['InventoryItem']['id']), array('escape'=>false, 'target'=>'_blank')); ?>
							<?php } ?>
							
						</td>
						<td><?php echo $inventoryPurchaseRequisitionItem['InventorySupplier']['name']; ?></td>
						<td><?php echo $inventoryPurchaseRequisitionItem['quantity']; ?><br><?php 
								echo $inventoryPurchaseRequisitionItem['GeneralUnit']['name'];
							?></td>
						<td><?php echo $inventoryPurchaseRequisitionItem['price_per_unit']; ?></td>
						<td>
						<?php
						if($inventoryPurchaseRequisitionItem['discount_type'] == '0'){
							if($inventoryPurchaseRequisitionItem['discount'] == '0.0000'){
								echo "-";
							}else{
								echo "RM ".number_format($inventoryPurchaseRequisitionItem['discount'], 2);
							}
						}else{
							echo number_format($inventoryPurchaseRequisitionItem['discount'], 0).' %'; 
						}

						?>
						</td>
						<td>
						<?php
						if($inventoryPurchaseRequisitionItem['tax'] == '0'){
							echo "-";
						}else{
							echo $inventoryPurchaseRequisitionItem['tax'].' %';
						}
						?>
						</td>
						<td>
						<?php 
							echo $inventoryPurchaseRequisitionItem['amount']; 
						?></td>
					</tr>
				<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Purchase Requisition Item
				<?php endif; ?>
			</div>
			<?php echo $this->Form->create('InventoryPurchaseRequisition', array('class' => 'form-horizontal')); ?>
			<?php echo $this->Form->input('pr_type', array('value' => $inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['pr_type'], 'type'=>'hidden')); ?>
			<?php echo $this->Form->input('pr_no', array('value' => $inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['pr_no'], 'type'=>'hidden')); ?>
			<?php echo $this->Form->input('amount', array('value' => $amount, 'type'=>'hidden')); ?>
			<?php echo $this->Form->input('id', array('value' => $inventoryPurchaseRequisitions['InventoryPurchaseRequisition']['id'])); ?>
			<div class="clearfix">&nbsp;</div>
			<div class="form-group">
				<label class="col-sm-3">Remark</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('approval_remark', array('type' => 'textarea', 'label' => false, 'class' => 'form-control')); ?>	
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Status</label>
				<div class="col-sm-9"><?php $status = array('9' => 'Approved', '0' => 'Reject'); ?>
					<?php echo $this->Form->input('status', array('empty' => '-Select Status-', 'options' => $status, 'label' => false, 'class' => 'form-control')); ?>	
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3"></label>
				<div class="col-sm-9">
					<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-danger pull-right')); ?>	
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

