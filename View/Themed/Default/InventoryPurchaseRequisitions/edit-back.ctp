<div class="row"> 
    <div class="col-xs-12">
        <?php echo $this->Html->link(__('List Inventory Item'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Edit Purchase Requisition'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                <!-- content start-->
                
                <div class="inventoryPurchaseRequisitions form">
                <?php echo $this->Form->create('InventoryPurchaseRequisition', array("class"=>"form-horizontal")); ?>
                    <fieldset>
                        <?php echo $this->Form->input("id"); ?>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">PR Number</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("pr_no", array("class"=> "form-control", "label"=> false, "readonly" => "readonly")); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Required Date</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("dateline", array("type"=>"text", "id"=>"datepicker", "class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Budget</label>
                            <div class="col-sm-9">
                            <select name="account_department_budget_id" id="available_budget" class="form-control" onchange="checkbudget(this.value);">
                                <option value="0">Please Select Budget</option>
                                <?php for($i=0; $i<count($data_budget); $i++){ ?>
                                <option value="<?php echo $data_budget[$i]['id']; ?>" <?php if($data_budget[$i]['id'] == $this->request->data['InventoryPurchaseRequisition']['account_department_budget_id']){ ?>selected<?php } ?>><?php echo $data_budget[$i]['name']; ?></option>
                                <?php } ?>
                            </select>

                            </div> 
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Term Of Payment</label>
                            <div class="col-sm-9">
                            <select name="term_of_payment_id" class="form-control" onchange="checkmode(this.value)" id="mode">
                                <option value="0">Please Select Terms</option>
                                <?php foreach ($terms as $key => $term) { ?>
                                   <option value="<?php echo $key; ?>" <?php if($key == $this->request->data['InventoryPurchaseRequisition']['term_of_payment_id']) { echo "selected"; }  ?>><?php echo $term; ?></option>
                                <?php } ?>
                            </select>

                            </div> 
                        </div>
                        <div class="form-group" id="otherpaymentmode" style="display: none;">
                            <label class="col-sm-3" style="padding-top: 8px">&nbsp;</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("new_term", array("class"=> "form-control", "label"=> false)); ?>
                            </div> 
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Supplier</label>
                            <div class="col-sm-9">
                            <?php echo $this->Form->input("inventory_supplier_id", array("id"=>"supplier_id", "options"=>$supplier, "empty"=>"Please Select Supplier", "class"=> "form-control", "label"=> false)); ?>
                            <?php if(isset($this->request->data['InventoryPurchaseRequisitionItem']) && $this->request->data['InventoryPurchaseRequisitionItem'] != '') { echo $this->Form->input("sid", array("type"=>"hidden", "class"=> "form-control", "label"=> false, "id"=>"sid", "value"=>$this->request->data['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id'])); } ?>
                            </div>  
                        </div>

                        <?php if($pr['InventoryPurchaseRequisition']['status'] == 1) { ?> 
                        <div class="form-group">
                            <label class="col-sm-3" style="padding-top: 8px">Status</label>
                            <div class="col-sm-9">
                            <?php 
                            $status = array(1 => 'Draft', 2 => 'Submit For Approval'); 
                            echo $this->Form->input("status", array("options"=>$status, "empty"=>"-Select Status-", "class"=> "form-control", "label"=> false)); ?> 
                            </div>  
                        </div>
                        <?php } ?>

                    </fieldset>
                    <div class="clearfix">&nbsp;</div>
                    <h4 class="text-right">Available Budget : <input type="text" name="a_budget" id="a_budget" class="text-center" readonly="readonly"></h4>
                    <h4><?php echo __('Item'); ?></h4>
                    <?php $inv_PR = $this->request->data['InventoryPurchaseRequisitionItem']; ?>
                    <table cellspacing="1" cellpadding="1" class="table">
                        <thead>
                            <tr>
                                <th class="col-sm-3">Item</th>
                                <th class="col-sm-2">Supplier</th>
                                <th class="col-sm-1">Quantity</th>
                                <th class="col-sm-1">Unit Price</th>
                                <th class="col-sm-1">UOM</th>
                                <th class="col-sm-1">Discount</th>
                                <th class="col-sm-1">Type</th>
                                <th class="col-sm-1">Tax (%)</th>
                                <th class="col-sm-1">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="p_scents">
                        <?php for($i=0; $i<count($inv_PR); $i++){ ?>
                        
                            <tr id="row<?php echo $i; ?>">
                                <td id="itemlist<?php echo $i; ?>">
                                    <input class="form-control" name="name[]" onchange="updatedetails(this.value, <?php echo $i; ?>)" value="<?php echo $items_2[$v]['InventoryItem']['name']; ?>">
                                       
                                    <input class="form-control" name="inventory_item_id[]" value="<?php echo $items_2[$v]['InventoryItem']['id']; ?>">
                                </td>
                                <td>
                                    <select class="form-control" name="inventory_supplier_id[]" onchange="updatedetails(this.value, <?php echo $i; ?>)">
                                        <option value="0">Supplier</option>
                                        <?php for($v=0; $v<count($items_2); $v++){ ?>
                                        <option value="<?php echo $items_2[$v]['InventoryItem']['id']; ?>" <?php if($items_2[$v]['InventoryItem']['id'] == $inv_PR[$i]['inventory_item_id']){ echo "selected"; } ?>><?php echo $items_2[$v]['InventoryItem']['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    
                                </td>

                                <td><input type="text" class="form-control" name="quantity[]" id="quantity" value="<?php echo $inv_PR[$i]['quantity'] ?>"></td>
                                <td><input type="text" class="form-control" name="unit_price[]" value="<?php echo $inv_PR[$i]['price_per_unit'] ?>" id="unit_price<?php echo $i; ?>"></td>
                                <td><select name="general_unit[]" id="genunit" class="form-control"><option value="0">UOM</option><?php foreach ($generalUnits as $key => $gUnit) { ?><option value="<?php echo $key; ?>" <?php if($key == $inv_PR[$i]['general_unit_id']){ echo "selected"; } ?>><?php echo $gUnit; ?></option>
                        <?php } ?></select></td>
                                <td><input type="text" class="form-control" name="discount[]" id="discount" value="<?php echo $inv_PR[$i]['discount'] ?>"></td>
                                <td><select class="form-control" id="dis_type" name="discount_type[]"><option value="x">Select Type</option><option value="0" <?php if($inv_PR[$i]['discount_type']=='0'){ echo "selected"; } ?>>Amount(RM)</option><option value="1" <?php if($inv_PR[$i]['discount_type']=='1'){ echo "selected"; } ?>> % </option></select></td>
                                <td><input type="text" class="form-control" name="tax[]" id="tax" value="<?php echo $inv_PR[$i]['tax'] ?>"></td>
                                <td><?php if($i != '0') { ?><button type="button" onclick="removerow(<?php echo $i; ?>)" class="btn btn-sm btn-danger" id="minusbtn"><i class="fa fa-minus"></i></button><?php  }else{ ?><button type="button" onclick="addrow()" class="btn btn-sm btn-success" id="plusbtn"><i class="fa fa-plus"></i></button><?php } ?></td>
                            </tr>
                        <?php } ?>
                            
                            <input type="hidden" id="id_supplier" value="<?php echo $this->request->data['InventoryPurchaseRequisitionItem'][0]['inventory_supplier_id']; ?> "> 
                        </tbody>
                    </table>
                    <div class="clearfix">&nbsp;</div> 

                    <?php echo $this->Form->input("dir", array("type"=>"hidden")); ?>
                    <?php echo $this->Form->input("approval_dir", array("type"=>"hidden")); ?>
                    <div class="clearfix">&nbsp;</div>
                    <h4><?php echo __('Quotation Attachment (Optional)'); ?></h4>
                    <div><?php echo $this->Form->input("quotation", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?></div>
                    <div class="clearfix">&nbsp;</div>
                    <h4><?php echo __('MTC Approval Attachment (Optional)'); ?></h4>
                    <div><?php echo $this->Form->input("approval", array("type"=>"file", "multiple", "class"=> "form-control", "label"=> false, "required" => false)); ?></div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Remark</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("remark_id", array("id" => "rmk", "options" => $remark, "empty" => array(0=>"Select Remark"), "class"=> "form-control", "label"=> false, "onchange" => "checkremark(this.value)")); ?>
                        </div> 
                    </div>
                    <div class="form-group" id="otherremark" style="display: none;">
                        <label class="col-sm-2" style="padding-top: 8px">&nbsp;</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("new_remark", array("class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2" style="padding-top: 8px">Warranty</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("warranty_id", array("id" => "wrty", "options" => $warranty, "empty" => array(0 => "Select Warranty"), "class"=> "form-control", "label"=> false, "onchange" => "checkwarranty(this.value)")); ?>
                        </div> 
                    </div>
                    <div class="form-group" id="otherwarranty" style="display: none;">
                        <label class="col-sm-2" style="padding-top: 8px">&nbsp;</label>
                        <div class="col-sm-10">
                        <?php echo $this->Form->input("new_warranty", array("class"=> "form-control", "label"=> false)); ?>
                        </div> 
                    </div>
                    <?php
                        //echo $this->Form->input('id');
                        //echo $this->Form->input('user_id');
                        //echo $this->Form->input('title');
                        //echo $this->Form->input('description');
                        //echo $this->Form->input('internal_department_id');
                        //echo $this->Form->input('general_purchase_requisition_type_id');
                        //echo $this->Form->input('dateline');
                        //echo $this->Form->input('status');
                    ?>
                    </fieldset>
                    <div class="clearfix">&nbsp;</div>
                    <?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
                </div>
                <!-- content end -->
            </div>
        </div>
    </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var id_sups = $('#sid').val();
        $('#supplier_id').val(id_sups);

        var row_count = $('#count_row').val();
        
        for(i=1; i<=row_count; i++){
            if($('#id'+i). prop("checked") == true){               
                $('#q'+i).prop('disabled', false);
                $('#p_unit'+i).prop('disabled', false);
                $('#gu'+i).prop('disabled', false);
                $('#r'+i).prop('disabled', false);

                var guid = $('#gu_id'+i).val();
                $('#gu'+i).val(guid);
            }
        }

    });


    function checkbudget(id){
        var id = id;
        $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventoryPurchaseRequisitions/ajaxbudget', 
            success: function(data) { 
                //console.log(data);
                $('#a_budget').val(data);
            }
        }); 
        return false;  
    }

    function checkmode(mode){
        var mode = $("#mode option[value='"+mode+"']").text();
        if(mode == 'Others...'){
            $('#otherpaymentmode').show();
        }
        
    }

    $(document).ready(function(){
        var id = $('#available_budget').val();
        $.ajax({ 
            type: "POST", 
            //dataType: 'json',
            data: {id: id},
            url: baseUrl + 'inventoryPurchaseRequisitions/ajaxbudget', 
            success: function(data) { 
                //console.log(data);
                $('#a_budget').val(data);
            }
        }); 
        return false;
    })

    function enabletextbox(rowid, price) {
        
        if($('#id'+rowid).prop("checked") == true){
            $('#q'+rowid).prop('disabled', false);
            $('#p_unit'+rowid).prop('disabled', false);
            $('#gu'+rowid).prop('disabled', false);
            $('#r'+rowid).prop('disabled', false);
        }
        else if($('#id'+rowid).prop("checked") == false){
            $('#q'+rowid).prop('disabled', true);
            $('#p_unit'+rowid).prop('disabled', true);
            $('#gu'+rowid).prop('disabled', true);
            $('#r'+rowid).prop('disabled', true);
        }

        var price = price;

        var adnilai = $('#p_unit'+rowid).val();

        if(adnilai == ''){
            $('#p_unit'+rowid).val(price);
        }
       
    }

    function checkremark(nilai){
        var mode = $("#rmk option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherremark').show();
        }
        
    }

    function checkwarranty(nilai){
        var mode = $("#wrty option[value='"+nilai+"']").text();

        if(mode == 'Others...'){
            $('#otherwarranty').show();
        }
        
    }

    function updatedetails(nilai, row = null) {
        //$('#unit_price').attr('id','unit_price_'+nilai);
        //alert(row);
        if(nilai == 'xxx'){
            var supplier_id = $('#id_supplier').val();
            var getParam = {
                supplier_id: supplier_id
            }

            var scntDiv = $('#p_scents');
            var c = $('#p_scents tr').size()-1;
            var ci = c;

            //alert(ci);

            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventoryPurchaseRequisitions/ajaxitemgeneral', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(item);
                        
                        var html = '<br><select class="form-control" name="inventory_supplier_item_id[]" onchange="updatedetails_2(this.value, '+ci+')"><option value="0">Please Select Item</option>'; 
                        var row = 0;
                        $.each(data, function(i, item) { 
                            //console.log(item);
                            html += '<option value='+item.id+'>';
                            html += item.name;
                            html += '</option>';
                        });
                        html += '</select>';
                        //console.log(ci);

                        if(ci == '0'){
                            $('#newitem').show();
                            $('#newitem').html(html);
                        }else{
                            $('#newitem'+ci).show();
                            $('#newitem'+ci).html(html);
                            
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false;

        }else{

            //alert(nilai);
            //alert(row);
            var getParam = {
                id: nilai
            }
        
            $.ajax({ 
                type: "GET", 
                dataType: "json",
                data: getParam,
                cache: false,
                url: baseUrl + 'inventoryPurchaseRequisitions/ajaxitemdetails', 
                success: function(data) { 
                    $.each(data, function(i, item) { 
                        //console.log(row);
                        if(row == null || row == '0'){
                            $('#unit_price').val(item.unit_price);
                            $('#newitem').hide();
                        }else{
                            $('#unit_price'+row).val(item.unit_price);
                            $('#newitem'+row).hide();
                        }
                    });
                },
                error: function(err) { 
                    console.log(err);
                    
                }
            }); 
            return false; 
        } 
    }

    function updatedetails_2(nilai, row = null) {
        //$('#unit_price').attr('id','unit_price_'+nilai);
        //alert(nilai);
        //alert(nilai);
        //alert(row);
        var getParam = {
            id: nilai
        }
    
        $.ajax({ 
            type: "GET", 
            dataType: "json",
            data: getParam,
            cache: false,
            url: baseUrl + 'inventoryPurchaseRequisitions/ajaxitemdetails', 
            success: function(data) { 
                $.each(data, function(i, item) { 
                    //console.log(row);
                    if(row == null || row == '0'){
                        $('#unit_price').val(item.unit_price);
                        //$('#newitem').hide();
                    }else{
                        $('#unit_price'+row).val(item.unit_price);
                        //$('#newitem').hide();
                    }
                });
            },
            error: function(err) { 
                console.log(err);
                
            }
        }); 
        return false; 
        
    }

    function addrow(){
        
        var supplier_id = $('#id_supplier').val();
        var genUnit = $("#genunit").clone();
        var discount_type = $("#dis_type").clone();
        var itemlist = $("#itemlist").clone();

        if(supplier_id != ''){
            var getParam = {
                supplier_id: supplier_id
            }

            $.ajax({ 
                type: "GET", 
                dataType: 'json',
                data: getParam,
                url: baseUrl + 'inventoryPurchaseRequisitions/ajaxitem', 
                success: function(data) { 
                    var scntDiv = $('#p_scents');
                    var c = $('#p_scents tr').size();
                    var ci = c;

                    scntDiv.append('<tr id="row'+c+'"><td id="itemlist'+c+'"><select class="form-control"><option>Please Select Item</option></select></td><td><input type="text" class="form-control" name="quantity[]" id="quantity"></td><td><input type="text" class="form-control" name="unit_price[]" id="unit_price'+c+'"></td><td><select name="general_unit[]" id="genunit" class="form-control"><option value="0">Select General Unit</option><?php foreach ($generalUnits as $key => $gUnit) { ?><option value="<?php echo $key; ?>"><?php echo $gUnit; ?></option><?php } ?></select></td><td><input type="text" class="form-control" name="discount[]" id="discount"></td><td><select class="form-control" id="dis_type" name="discount_type[]"><option value="x">Select Type</option><option value="0">Amount(RM)</option><option value="1"> % </option></select></td><td><input type="text" class="form-control" name="tax[]" id="tax"></td><td><button type="button" class="btn btn-danger btn-sm" onclick="removerow('+c+')"><i class="fa fa-minus"></i></button></td></tr>');
                    c++;

                    var html = '<select class="form-control" name="inventory_supplier_item_id[]" onchange="updatedetails(this.value, '+ci+')"><option value="0">Please Select Item</option>'; 
                var row = 0;
                $.each(data, function(i, item) { 
                    //console.log(item);
                    html += '<option value='+item.id+'>';
                    html += item.name;
                    html += '</option>';
                    
                });
                html += '<option value="xxx">NEW ITEM</options>';
                html += '</select><div id="newitem'+ci+'" style="display:none"></div>';
            
                $('#itemlist'+ci).html(html);
                }
            }); 
            
           
        }else{
            alert("Select supplier!");
        }

    }

    function removerow(nilai) {
        //alert(nilai);
        $('#row'+nilai).remove();
        
    };

  </script>
  <?php $this->end(); ?>