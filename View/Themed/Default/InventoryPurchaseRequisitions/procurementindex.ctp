<?php echo $this->Html->link(__('Waiting Verification'), array('action' => 'procurementindex'), array('class'=>'btn btn-warning btn-sm')); ?>
<?php echo $this->Html->link(__('Waiting Approval'), array('action' => 'procurementindex', 5), array('class'=>'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Rejected'), array('action' => 'procurementindex', 4), array('class'=>'btn btn-danger btn-sm')); ?>
<?php echo $this->Html->link(__('Approved'), array('action' => 'procurementindex', 9), array('class'=>'btn btn-success btn-sm')); ?> 
<?php echo $this->Html->link(__('Converted To PO'), array('action' => 'procurementindex', 11), array('class'=>'btn btn-primary btn-sm')); ?>
 

<div class="row"> 
  	<div class="col-xs-12">
  		 
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Purchase Requisitions'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->

            <?php echo $this->Form->create('InventoryPurchaseRequisition', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
            <table cellpadding="0" cellspacing="0" class="table">
                <tr>
                <td><?php echo $this->Form->input('pr_no', array('placeholder' => 'PR Number', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('ref', array('placeholder' => 'Ref', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_start', array('type' => 'text', 'placeholder' => 'From Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('date_end', array('type' => 'text', 'placeholder' => 'To Date', 'class' => 'form-control', 'required' => false, 'id' => 'datepicker_2', 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->input('status', array('options' => array('x'=>'Select Status', '0'=>'Reject', '1'=>'Draft', '2'=>'Pending for approval', '9'=>'Approved', '5'=>'Insufficient Budget'), 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
                </td>
                <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
                </tr>
            </table>
            <?php $this->end(); ?>  
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
							<th><?php echo $this->Paginator->sort('pr_no', 'PR No'); ?></th>
                            <th><?php echo $this->Paginator->sort('ref', 'Referrence'); ?></th>
                            <th><?php echo $this->Paginator->sort('User.firstname', 'User'); ?></th>
                            <th><?php echo $this->Paginator->sort('GeneralPurchaseRequisitionType.id', 'Type'); ?></th>
							<th><?php echo $this->Paginator->sort('created'); ?></th>
							<th><?php echo $this->Paginator->sort('dateline', 'Deadline'); ?></th> 
							<th><?php echo $this->Paginator->sort('status'); ?></th> 
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseRequisition']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseRequisition']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryPurchaseRequisitions as $inventoryPurchaseRequisition): 
                        //if($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status']=='9'){
					?>
					<tr>
						<td class="text-center"><?php echo $startSN++; ?></td>
						<td><?php echo ucfirst($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['pr_no']); ?>&nbsp;</td>
                        <td><?php echo $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['ref']; ?>&nbsp;</td>
                        <td>
                    <?php echo $this->Html->link($inventoryPurchaseRequisition['User']['firstname'], array('controller' => 'users', 'action' => 'view', $inventoryPurchaseRequisition['User']['id']), array('target' => '_blank')); ?>
                </td>
                        
                        <td><?php echo $inventoryPurchaseRequisition['GeneralPurchaseRequisitionType']['name']; ?>&nbsp;</td>
						<td><?php echo h($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['created']); ?>&nbsp;</td>
						<td><?php echo ucfirst($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['dateline']); ?>&nbsp;</td>
						<td>
                        <?php
                            echo status($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status']);
                        ?>
                        &nbsp;</td>
                        
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'procurementview', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
                            <?php if(($inventoryPurchaseRequisition['InventoryPurchaseRequisition']['status'])=='3') { ?>
                            <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'procurementedit', $inventoryPurchaseRequisition['InventoryPurchaseRequisition']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?>
                            <?php } ?>
						</td>
					</tr>
                    <?php // } ?>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
            <?php
              echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
              echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
              echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
            ?>
            </ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php

function status($status) {
    if($status=='0'){
        return "Draft";
    }else if($status=='1'){
        return "Submit to HOS";
    }else if($status=='2'){
        return "HOS Rejected";
    }else if($status=='3'){
        return "Waiting Procurement Verification";
    }else if($status=='4'){
        return "Procurement Rejected";
    }else if($status=='5'){
        return "Waiting HOD Approval";
    }else if($status=='6'){
        return "HOD Rejected";
    }else if($status=='7'){
        return "Waiting MD";
    }else if($status=='8'){
        return "MD Rejected";
    }else if($status=='9'){
        return "Approved";
    }else if($status=='10'){
        return "Insufficient Budget";
    }else if($status=='11'){
        return "PO Created";
    }
}

function pr_type($type) {
    if($type == 1) {
        return 'CAPEX';
    } elseif($type == 2) {
        return 'OPEX';
    } elseif($type == 3) {
        return 'SCOC';
    } elseif($type == 4) {
        return 'MRP';
    }
}

?>