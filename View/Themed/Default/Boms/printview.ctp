 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile"> 
      <div class="x_content">  
<h4><?php echo h($bom['Bom']['name']); ?></h4> 
<table style="width:100%;" class="table-bordered" cellpadding="0" cellspacing="0">
  <tr>
    <td>Name</td> 
    <td> : <?php echo h($bom['Bom']['name']); ?></td>  
  </tr>
  <tr>
    <td>Code</td> 
    <td> : <?php echo h($bom['Bom']['code']); ?></td>  
  </tr>
  <tr>
    <td>Description</td> 
    <td> : <?php echo h($bom['Bom']['description']); ?></td>  
  </tr>
  <tr>
    <td>Created</td> 
    <td> : <?php echo h($bom['Bom']['created']); ?></td>  
  </tr>
  <tr>
    <td>User</td> 
    <td> : <?php echo h($bom['User']['firstname']); ?></td>  
  </tr> 
</table> 

<table style="width:100%;" class="bordered" cellpadding="0" cellspacing="0">

  <?php foreach ($childs as $child) { ?>
  <tr>
    <td style="border-left: 1px solid #000;">&#8702; <?php echo $child['BomChild']['name']; ?></td> 
    <td><?php echo $child['InventoryItem']['name']; ?></td>  
    <td><?php echo $child['BomChild']['quantity']; ?> <?php echo $child['GeneralUnit']['name']; ?></td>
  </tr> 
 
  <!-- Load child via ajax -->
  <tr style="padding:0; margin:0">
    <td style="padding-left:20px; border-left: 1px solid #000;" colspan="3" id="loadChild-<?php echo $child['BomChild']['id']; ?>"></td>
  </tr>
  <!-- end ajax -->

<?php $this->start('script'); ?> 
  <script type="text/javascript"> 
    $(document).ready(function() {
      loadChild(<?php echo $child['BomChild']['id']; ?>, "<?php echo $child['BomChild']['name']; ?>");
    });
  </script>
  <?php $this->end(); ?>

<?php } ?> 
</table>

 
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
  function loadChild(id, parent_name) { 
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'parent_id=' + id + '&bom_id=' + <?php echo $bom['Bom']['id']; ?>,
        url: baseUrl + 'boms/ajaxloadchild', 
        success: function(respond) {  
            var html = '';  
            $.each(respond.data, function(i, item) {   
                var childName = item['BomChild']['name']; 
                var postDelete = item['BomChild']['id'];
                html += '<tr>'; 
                html += '<td style="border-left: 1px solid #000;">&#8702; '+ item['BomChild']['name']; 
                html += '</td>';
                html += '<td>'+ item['InventoryItem']['name']+'</td>'; 
                 html += '<td>'+ item['BomChild']['quantity']+' ' + item['GeneralUnit']['name'] + '</td>'; 
                html += '</tr>';   
                html += '<tr><td style="padding-left:20px; border-left: 1px solid #000;" colspan="3" id="loadChild-' + item['BomChild']['id'] + '"></td></tr>';
                //html += '<div style="margin-left: 20px;" id="addMoreChild-' + item['BomChild']['id'] + '"></div>';
                //html += '<div style="margin-left: 20px;" id="addMoreItem-' + item['BomChild']['id'] + '"></div>';

                loadChild(item['BomChild']['id'], item['BomChild']['name']);
            }); 

            $.each(respond.items, function(i, item) {  
                var total = item['BomItem']['quantity'] * item['InventoryItem']['unit_price'];

                html += '<tr>'; 
                html += '<td style="border-left: 1px solid #000;">&#8702; ' + item['InventoryItem']['code'];  
                html += '</td>'; 
                html += '<td>' + item['InventoryItem']['name'];
                html += '</td>';  
                html += '<td>' + item['BomItem']['quantity'] + ' ' +  item['GeneralUnit']['name']; 
                html += '</td>';  
                html += '</tr>';  
                //html += '<div id="appendItem-'+item['BomItem']['id']+'-'+id+'"></div>';
            });

            $('#loadChild-'+id).html('<table class="bordered" style="width: 100%;" cellpadding="0" cellspacing="0">' + html + '</table>');
        }
    }); 

    return false; 
  }

  var bomId = "<?php echo $bom['Bom']['id']; ?>";
  var row = 1;
  function addChild(parent, parent_name) {
    var html = '<div id="remove-'+parent+ '-'+row+'">'; 
    html += '<div class="form-group">';
    html += '<label class="col-sm-3"> &nbsp; &#8627; </label>';
    html += '<div class="col-sm-7">';
    html += '<input type="text" name="data[BomChild][name]" class="form-control" placeholder="Enter Child Name" id="childName-'+row+parent+'">';
    html += '<input type="hidden" name="data[BomChild][parent_id]" id="childParentId-'+row+parent+'" value="'+parent+'">';
    html += '<input type="hidden" name="data[BomChild][parent_name]" id="childParentName-'+row+parent+'" value="'+parent_name+'">';
    html += '<div id="respond-'+row+parent+'"></div>';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<a href="#" onclick="deleteChild('+row+', '+ parent +'); return false" class="btn btn-danger"><i class="fa fa-times"></i></a>';
    html += '<a href="#" onclick="saveChild('+row+', '+ parent + '); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
    html += '</div>'; 
    html += '</div>';
    html += '</div>';
    row ++;
    $('#child-' + parent).append(html);
    return false;
  } 

  var moreRow = 1;
  function addMoreChild(parent, parent_name) {
    var html = '<div id="remove-'+parent+ '-'+moreRow+'">'; 
    html += '<div class="form-group">';
    html += '<label class="col-sm-1"> &nbsp; &#8627; </label>';
    html += '<div class="col-sm-9">';
    html += '<input type="text" name="data[BomChild][name]" class="form-control" placeholder="Enter Child Name" id="childName-'+moreRow+parent+'">';
    html += '<input type="hidden" name="data[BomChild][parent_id]" id="childParentId-'+moreRow+parent+'" value="'+parent+'">';
    html += '<input type="hidden" name="data[BomChild][parent_name]" id="childParentName-'+moreRow+parent+'" value="'+parent_name+'">';
    html += '<div id="respond-'+moreRow+parent+'"></div>';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<a href="#" onclick="deleteChild('+moreRow+', '+ parent +'); return false" class="btn btn-danger"><i class="fa fa-times"></i></a>';
    html += '<a href="#" onclick="saveChild('+moreRow+', '+ parent + '); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
    html += '</div>'; 
    html += '</div>';
    html += '</div>';
    moreRow ++;
    $('#addMoreChild-' + parent).append(html);
    return false;
  }
  var rowItem = 1;
  function addMoreItem(parent, parent_name) {
    var html = '<div id="removeitem-'+parent+ '-'+rowItem+'">'; 
    html += '<div class="form-group">';
    html += '<div class="col-sm-3">';
    html += '<select name="category" id="category-'+parent+'-'+rowItem+'" class="form-control" onchange="getItemByCategoryId('+parent+', '+rowItem+'); return false"><option value="">Select Category</option>';
    <?php foreach($categories as $category) { ?> html += '<option value="<?php echo $category['InventoryItemCategory']['id']; ?>"><?php echo $category['InventoryItemCategory']['name']; ?></option>'; <?php } ?> 
    html += '</select></div>';
    html += '<div class="col-sm-3">';
    html += '<select name="data[BomItem][inventory_item_id][]" class="form-control" id="item_id-'+parent+'-'+rowItem+'"><option value="">Select Item</option>';
    html += '</select><input type="hidden" name="data[BomItem][bom_child_id]" id="parent_id-'+parent+'-'+rowItem+'" value="'+parent+'">'; 
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<input type="text" id="quantity-'+parent+'-'+rowItem+'" class="form-control">';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<select name="unit" id="quantity_unit_id-'+parent+'-'+rowItem+'" class="form-control"><option value="">Select Qty Unit</option>';
    <?php foreach($units as $unit) { ?> html += '<option value="<?php echo $unit['GeneralUnit']['id']; ?>"><?php echo $unit['GeneralUnit']['name']; ?></option>'; <?php } ?> 
    html += '</select></div>';
    html += '<div class="col-sm-2">';
    html += '<a href="#" onclick="deleteItem('+rowItem+', '+ parent +'); return false" class="btn btn-danger"><i class="fa fa-times"></i></a>';
     html += '<a href="#" onclick="saveItem('+rowItem+', '+ parent +'); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
    html += '</div>';
    html += '<div class="col-sm-2" id="respondItem-'+rowItem+parent+'"></div>';
    html += '</div>';
    html += '</div>';
    rowItem ++;
    $('#addMoreItem-' + parent).append(html);
    return false;
  }

  function saveItem(id, parent_id) {
    var item_id = $('#item_id-'+ parent_id +'-'+ id).val();
    var parent_id = $('#parent_id-'+ parent_id +'-'+ id).val();
    var quantity = $('#quantity-'+ parent_id +'-'+ id).val();
    var quantity_unit_id = $('#quantity_unit_id-'+ parent_id +'-'+ id).val();
    var bom_id = <?php echo $bom['Bom']['id']; ?>;
    var post = {
      item_id : item_id,
      parent_id : parent_id,
      quantity : quantity,
      quantity_unit_id : quantity_unit_id, 
      bom_id : bom_id
    }
    $.ajax({ 
        type: "POST", 
        dataType: 'json',
        data: post,
        url: baseUrl + 'boms/ajaxsaveitem', 
        success: function(respond) { 
          console.log(respond);
          if(respond.status === false) {
            $('#respondItem-'+ id + parent_id).html('<div class="text-danger">'+respond.message+'</div>');
          } else { 
            var html = '';
            html += '<div class="form-group">'; 
            html += '<label class="col-sm-4"> &nbsp; ' + ' &#8627; ' + respond.name; 
            html += '</label>'; 
            html += '<div class="col-sm-8">' + respond.quantity + ' ' + respond.unit;
            html += '</div>'; 
            html += '</div>';  
            $('#removeitem-'+ parent_id + '-' + id).html(html);
          }  
        }
    }); 
    return false; 
  }

  var rowItem2 = 1;
  function addItem(parent, parent_name) {
    var html = '<div id="removeitem-'+parent+ '-'+rowItem2+'">'; 
    html += '<div class="form-group">';
    html += '<div class="col-sm-3">';
    html += '<select name="category" id="category-'+parent+'-'+rowItem2+'" class="form-control" onchange="getItemByCategoryId('+parent+', '+rowItem2+'); return false"><option value="">Select Category</option>';
    <?php foreach($categories as $category) { ?> html += '<option value="<?php echo $category['InventoryItemCategory']['id']; ?>"><?php echo $category['InventoryItemCategory']['name']; ?></option>'; <?php } ?> 
    html += '</select></div>';
    html += '<div class="col-sm-3">';
    html += '<select name="data[BomItem][inventory_item_id][]" class="form-control" id="item_id-'+parent+'-'+rowItem2+'"><option value="">Select Item</option>';
    html += '</select><input type="hidden" name="data[BomItem][bom_child_id]" id="parent_id-'+parent+'-'+rowItem2+'" value="'+parent+'">'; 
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<input type="text" id="quantity-'+parent+'-'+rowItem2+'" class="form-control">';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<select name="unit" id="quantity_unit_id-'+parent+'-'+rowItem2+'" class="form-control"><option value="">Select Qty Unit</option>';
    <?php foreach($units as $unit) { ?> html += '<option value="<?php echo $unit['GeneralUnit']['id']; ?>"><?php echo $unit['GeneralUnit']['name']; ?></option>'; <?php } ?> 
    html += '</select></div>';
    html += '<div class="col-sm-2">';
    html += '<a href="#" onclick="deleteItem('+rowItem2+', '+ parent +'); return false" class="btn btn-danger"><i class="fa fa-times"></i></a>';
    html += '<a href="#" onclick="saveItem('+rowItem2+', '+ parent +'); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
    html += '</div>';
    html += '<div class="col-sm-2" id="respondItem-'+rowItem2+parent+'"></div>';
    html += '</div>';
    html += '</div>';
    rowItem2 ++;
    $('#child-' + parent).append(html);
    return false;
  }

  function saveChild(id, parent_id) { 
    var name = $('#childName-'+ id + parent_id).val();
    var parent = $('#childParentId-'+ id + parent_id).val();
    var parent_name = $('#childParentName-'+ id + parent_id).val();
    var bom_id = <?php echo $bom['Bom']['id']; ?>;
    var post = {
      name : name,
      parent_id : parent,
      bom_id : bom_id
    }
    $.ajax({ 
        type: "POST", 
        dataType: 'json',
        data: post,
        url: baseUrl + 'boms/ajaxsavechild', 
        success: function(respond) { 
          if(respond.status === false) {
            $('#respond-'+ id + parent_id).html('<div class="text-danger">'+respond.message+'</div>');
          } else { 
            var html = '';
            html += '<div class="form-group">'; 
            html += '<label class="col-sm-8"> &nbsp; ' + ' &#8627; ' + name; 
            html += '</label>'; 
            html += '<div class="col-sm-4"><a class="btn btn-primary btn-xs" onclick="addMoreChild(' + respond.bom_child_id + ', ' + id + ')"><i class="fa fa-plus"></i></a><a class="btn btn-default btn-xs" onclick="addMoreItem(' + respond.bom_child_id + ', ' + id + ')"><i class="fa fa-plus"></i></a>';
            html += '</div>'; 
            html += '</div>'; 
            html += '<div style="margin-left: 20px;" id="addMoreChild-' + respond.bom_child_id + '"></div>';
            html += '<div style="margin-left: 20px;" id="addMoreItem-' + respond.bom_child_id + '"></div>';
            $('#remove-'+ parent_id + '-' + id).html(html);
          }  
        }
    }); 
    return false; 
  } 
  

  function deleteItem(id, parent) {
    $('#removeitem-'+parent+ '-'+id).html('');
    return false;
  }

  function deleteChild(id, parent) {
    $('#remove-'+parent+ '-'+id).html('');
    return false;
  }

  // Remove require ajax. delete data from database
  function removeChild(id, parent) {
    //$('#removeitem-'+parent+ '-'+id).html('');
   var data = {
      id : id,
      parent_id : parent
    }
    $.ajax({ 
        type: "POST", 
        dataType: 'json',
        data: data,
        url: baseUrl + 'boms/ajaxremovechild', 
        success: function(respond) {
          if(respond.status === false) {

          } else {
            $('#removeItem-' + id).html('');
          }  
        }
    });  

    return false;
  }

  function removeItem(id, parent) {
    //$('#remove-'+parent+ '-'+id).html('');
    var data = {
      id : id,
      parent_id : parent
    } 
    $.ajax({ 
        type: "POST", 
        dataType: 'json',
        data: data,
        url: baseUrl + 'boms/ajaxremoveitem',  
        success: function(json) {
          console.log(json);
          if(json.status === false) {
            $('#removeItem-' + id).append('<p class="text-danger">'+json.message+'</p>');
          } else {
            $('#removeItem-' + id).html('');
          }  
        }
    }); 
    return false;
  }

  function editChild(id, parent, name) { 
    var html = '<div class="form-group">';
    html += '<label class="col-sm-1"> &nbsp; &#8627; </label>';
    html += '<div class="col-sm-9">';
    html += '<input type="text" name="name" class="form-control" placeholder="Enter Child Name" id="childName-'+ id + parent + '" value="'+name+'">';
    html += '<input type="hidden" name="child_id" id="child_id-' + id + '" value="' + id + '">'; 
    html += '<div id="edit-respond-'+id + parent+'"></div>';
    html += '</div>';
    html += '<div class="col-sm-2">'; 
    html += '<a href="#" onclick="updateChild('+id+', '+ parent + '); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
    html += '</div>'; 
    html += '</div>'; 
    $('#removeChild-' + id).html(html);
    return false;
  }      

  function updateChild(id, parent) {
    var name = $('#childName-' + id + parent).val(); 
    console.log(id);
    var data = {
      id : id,
      parent_id : parent,
      name : name
    } 
    $.ajax({ 
        type: "POST", 
        dataType: 'json',
        data: data,
        url: baseUrl + 'boms/ajaxupdatechild', 
        success: function(respond) {
          if(respond.status === false) {
            $('#edit-respond-'+ id + parent).html('<div class="text-danger">'+respond.message+'</div>');
          } else { 
            var html = '';
            html += '<div class="form-group">'; 
            html += '<label class="col-sm-8"> &nbsp; ' + ' &#8627; ' + name; 
            html += '</label>'; 
            html += '<div class="col-sm-4"><a class="btn btn-primary btn-xs" onclick="addMoreChild(' + id + ', ' + id + ')"><i class="fa fa-plus"></i></a><a class="btn btn-default btn-xs" onclick="addMoreItem(' + id + ', ' + id + ')"><i class="fa fa-plus"></i></a>';
            html += '</div>'; 
            html += '</div>';  
            $('#removeChild-'+ id ).html(html);
          }   
        }
    }); 
    return false;
  }

  function getItemByCategoryId(id, item) { 
    var category_id = $('#category-'+id+'-'+item).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#category-'+id+'-'+item).val(),
        url: baseUrl + 'boms/ajaxitemcategory', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) {
                console.log(90); 
                option += '<option value="' + item.id + '">' + item.name + '</option>';
            }); 
            $('#item_id-'+id +'-'+item).html(option);
        }
    }); 
    return false; 
  }

  function getItemSelected(id, item) {  
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'id=' + id,
        url: baseUrl + 'boms/ajaxitemselected', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.id + '">' + item.name + '</option>';
            }); 
            $('#item_id-'+id +'-'+item).html(option);
        }
    }); 
    return false; 
  }
  
  function cancelEdit(id, parent_id, divid) {
    $('#appendItem-' + id + '-' + parent_id).html('');
      return false;
  }

  function editItem(id, parent_id, selected_unit_id) { 
 
      $("#quantity_unit_id-"+id+" option[value='"+selected_unit_id+"']").attr("selected","selected"); 

      var html = '<div class="form-group" id="removeitem-'+parent_id+'-'+id+'">';
      html += '<div class="col-sm-3">';
      html += '<select name="category" id="category-'+id+'-'+parent_id+'" class="form-control" onchange="getItemByCategoryId('+id+', '+parent_id+'); return false"><option value="">Select Category</option>';
      <?php foreach($categories as $category) { ?> html += '<option value="<?php echo $category['InventoryItemCategory']['id']; ?>"><?php echo $category['InventoryItemCategory']['name']; ?></option>'; <?php } ?> 
      html += '</select></div>';
      html += '<div class="col-sm-3">';
      html += '<select name="item_id" class="form-control" id="item_id-'+id +'-'+parent_id+'"><option value="">Select Item</option>';
      html += '</select>'; 
      html += '</div>';
      html += '<div class="col-sm-2">';
      html += '<input type="text" id="quantity-'+id+'" class="form-control">';
      html += '</div>';
      html += '<div class="col-sm-2">';
      html += '<select name="unit" id="quantity_unit_id-'+id+'" class="form-control"><option value="">Select Qty Unit</option>';
      <?php foreach($units as $unit) { ?>   
        
        html += '<option value="<?php echo $unit['GeneralUnit']['id']; ?>" id="item_selected'+id+'"><?php echo $unit['GeneralUnit']['name']; ?></option>'; 
      <?php } ?> 
      html += '</select></div>';
      html += '<div class="col-sm-2">'; 
      html += '<a alt="Save" href="#" onclick="updateItem('+ id +'); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
      html += '<a alt="Cancel" href="#" onclick="cancelEdit('+ id +', '+parent_id+'); return false" class="btn btn-warning"><i class="fa fa-remove"></i></a>';
      html += '</div>';
      html += '<div class="col-sm-2" id="respondItem-'+id+'"></div>';
      html += '</div>'; 
       
      $('#appendItem-' + id + '-' + parent_id).append(html);
      return false;
  }



  
 
</script>
<?php $this->end(); ?> 