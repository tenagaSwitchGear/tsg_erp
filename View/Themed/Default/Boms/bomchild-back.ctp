
function editItem(id, parent_id) { 
      var html = '<div class="form-group">';
      html += '<div class="col-sm-3">';
      html += '<select name="category" id="category-'+id+'-'+parent_id+'" class="form-control" onchange="getItemByCategoryId('+id+', '+parent_id+'); return false"><option value="">Select Category</option>';
      <?php foreach($categories as $category) { ?> html += '<option value="<?php echo $category['InventoryItemCategory']['id']; ?>"><?php echo $category['InventoryItemCategory']['name']; ?></option>'; <?php } ?> 
      html += '</select></div>';
      html += '<div class="col-sm-3">';
      html += '<select name="item_id" class="form-control" id="item_id-'+id +'-'+parent_id+'"><option value="">Select Item</option>';
      html += '</select>'; 
      html += '</div>';
      html += '<div class="col-sm-2">';
      html += '<input type="text" id="quantity-'+id+'" class="form-control">';
      html += '</div>';
      html += '<div class="col-sm-2">';
      html += '<select name="unit" id="quantity_unit_id-'+id+'" class="form-control"><option value="">Select Qty Unit</option>';
      <?php foreach($units as $unit) { ?> html += '<option value="<?php echo $unit['GeneralUnit']['id']; ?>"><?php echo $unit['GeneralUnit']['name']; ?></option>'; <?php } ?> 
      html += '</select></div>';
      html += '<div class="col-sm-2">'; 
      html += '<a href="#" onclick="updateItem('+ id +'); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
      html += '</div>';
      html += '<div class="col-sm-2" id="respondItem-'+id+'"></div>';
      html += '</div>'; 
       
      $('#removeItem-' + id).html(html);
      return false;
  }


html += '<div class="form-group line-bottom" id="removeItem-'+item['BomItem']['id']+'">'; 
html += '<label class="col-sm-4"> &nbsp; ' + ' &#8627; ' + item['InventoryItem']['name'];  
html += '</label>';  
html += '<div class="col-sm-4">' + item['BomItem']['quantity'] + ' ' +  item['GeneralUnit']['name'];
html += '</div>'; 
html += '<div class="col-sm-4">';
html += '<a class="btn btn-xs text-danger" onclick="removeItem(' + item['BomItem']['id'] + ', ' + id + ')"><i class="fa fa-remove"></i></a> <a class="btn btn-xs text-warning" onclick="editItem(' + item['BomItem']['id'] + ', ' + id + ')"><i class="fa fa-pencil"></i></a>';
html += '</div>'; 
html += '</div>';  









<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo h($bom['Bom']['name']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Form->create('BomChild', array('class' => 'form-horizontal form-label-left')); ?> 
        <?php foreach ($bom['BomChild'] as $child) { ?>
        <div class="row border-bottom">
          <div class="form-group">
            <label class="col-sm-8"><?php echo $child['name']; ?></label>
            <div class="col-sm-2"> 
            </div>
            <div class="col-sm-2"> 
            </div>
          </div>
          <div id="child-<?php echo $child['id']; ?>"></div>  
          <div class="form-group">
            <label class="col-sm-8">&nbsp;</label>
            <div class="col-sm-2">
              <a href="#" class="btn btn-primary" onclick="addChild(<?php echo $child['id']; ?>, '<?php echo $child['name']; ?>'); return false"><i class="fa fa-plus"></i> Add Child</a>
            </div>
            <div class="col-sm-2">
              <a href="#" class="btn btn-default" onclick="addItem(<?php echo $child['id']; ?>, '<?php echo $child['name']; ?>'); return false"><i class="fa fa-plus"></i> Add Item</a>
            </div>
          </div>
        </div>
        <?php } ?>  
        <div class="form-group">
        <label class="col-sm-3">&nbsp;</label>
        <div class="col-sm-9">
          <?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
        </div>
      </div> 
        <?php echo $this->Form->end(); ?>  

      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
  var row = 1;
  function addChild(parent, parent_name) {
    var html = '<div id="remove-'+parent+ '-'+row+'">'; 
    html += '<div class="form-group">';
    html += '<label class="col-sm-3"> &nbsp; '+parent_name+' <i class="fa fa-angle-right"></i></label>';
    html += '<div class="col-sm-8">';
    html += '<?php echo $this->Form->input('BomChild.name.', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Enter Title')); ?>';
    html += '</div>';
    html += '<div class="col-sm-1">';
    html += '<a href="#" onclick="deleteChild('+row+', '+ parent +'); return false" class="btn btn-danger"><i class="fa fa-times"></i></a>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    row ++;
    $('#child-' + parent).append(html);
    return false;
  }
 

  function addItem(parent, parent_name) {
    var html = '<div id="removeitem-'+parent+ '-'+row+'">'; 
    html += '<div class="form-group">';
    html += '<div class="col-sm-3">';
    html += '<select name="category" id="category-'+parent+'-'+row+'" class="form-control" onchange="getItemByCategoryId('+parent+'); return false"><option value="">Select Category</option>';
    <?php foreach($categories as $category) { ?> html += '<option value="<?php echo $category['InventoryItemCategory']['id']; ?>"><?php echo $category['InventoryItemCategory']['name']; ?></option>'; <?php } ?> 
    html += '</select></div>';
    html += '<div class="col-sm-3">';
    html += '<select name="data[BomItem][inventory_item_id][]" class="form-control" id="itemId-'+parent+'"><option value="">Select Item</option>';
    html += '</select><input type="hidden" name="data[BomItem][bom_child_id][]" value="'+parent+'">'; 
    html += '</div>';
    html += '<div class="col-sm-3">';
    html += '<?php echo $this->Form->input('BomItem.quantity.', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Qty')); ?>';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<?php echo $this->Form->input('BomItem.name.', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Enter Title')); ?>';
    html += '</div>';
    html += '<div class="col-sm-1">';
    html += '<a href="#" onclick="deleteItem('+row+', '+ parent +'); return false" class="btn btn-danger"><i class="fa fa-times"></i></a>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    row ++;
    $('#child-' + parent).append(html);
    return false;
  }

  function deleteItem(id, parent) {
    $('#removeitem-'+parent+ '-'+id).html('');
    return false;
  }

  function deleteChild(id, parent) {
    $('#remove-'+parent+ '-'+id).html('');
    return false;
  }

  function getItemByCategoryId(id) { 
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#category-'+id).val(),
        url: baseUrl + 'boms/ajaxitemcategory', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) {
                console.log(90); 
                option += '<option value="' + item.id + '">' + item.name + '</option>';
            }); 
            $('#itemId-'+id).html(option);
        }
    }); 
    return false; 
  }
</script>
<?php $this->end(); ?>