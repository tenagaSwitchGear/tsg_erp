<?php echo $this->Html->css('/js/libs/jstree/themes/apple/style.css'); ?>

<?php echo $this->Html->link('BOM Template', array('action' => 'index'), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php echo $this->Html->link('BOM Active', array('action' => 'index/1'), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php echo $this->Html->link('BOM Draft', array('action' => 'index/0'), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php echo $this->Html->link('<i class="fa fa-plus"></i> Add New BOM', array('action' => 'add'), array('class' => 'btn btn-success', 'escape' => false)); ?>

<?php echo $this->Html->link('<i class="fa fa-pencil"></i> Edit', array('action' => 'edit', $bom['Bom']['id']), array('class' => 'btn btn-warning', 'escape' => false)); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View: <?php echo h($bom['Bom']['code']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="customerAddresses view-data">
		<h2><?php echo __('Bom'); ?></h2>
			<dl>
 
				<dt><?php echo __('Finished Good'); ?></dt>
				<dd>
					<?php echo $this->Html->link($bom['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $bom['InventoryItem']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Name'); ?></dt>
				<dd>
					<?php echo h($bom['Bom']['name']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Code'); ?></dt>
				<dd>
					<?php echo h($bom['Bom']['code']); ?>
					&nbsp;
				</dd>

				<dt><?php echo __('Job'); ?></dt>
				<dd>
					<?php echo h($bom['SaleJob']['name']); ?>&nbsp; 
				</dd> 
				<dt><?php echo __('Station'); ?></dt>
				<dd>
					<?php echo h($bom['SaleJobChild']['name']); ?> - <?php echo h($bom['SaleJobChild']['station_name']); ?> 
				</dd>

				<dt><?php echo __('Description'); ?></dt>
				<dd>
					<?php echo h($bom['Bom']['description']); ?> 
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo h($bom['Bom']['created']); ?> 
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php echo h($bom['Bom']['modified']); ?> 
				</dd>
				<dt><?php echo __('Added By'); ?></dt>
				<dd>
					<?php echo $this->Html->link($bom['User']['username'], array('controller' => 'users', 'action' => 'view', $bom['User']['id'])); ?> 
				</dd>
				<dt><?php echo __('Status'); ?></dt>
				<dd>
					<?php echo ($bom['Bom']['status'] == 1) ? 'Active' : 'Draft'; ?> 
				</dd>
				 
			</dl>
		</div>

		<div class="col-xs-12">
			<ul><li><?php echo h($bom['Bom']['name']); ?> (<?php echo h($bom['Bom']['code']); ?>)</li></ul>
		  	<ul id="tree_root_1"></ul>
		</div>
      
      </div>
    </div>
  </div> 
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('libs/jstree/jquery.jstree.js'); ?> 
<script type="text/javascript">
$(function() { 
	$("#tree_root_1").jstree({ 
		"plugins" : ["themes", "json_data", "ui", "cookie"],
		"json_data" : {
		    "ajax" : {
		        "type": 'GET',
		        "url": function (node) {
		            if (node == -1) {
		                url = baseUrl + "boms/ajaxtree/<?php echo h($bom['Bom']['id']); ?>";
		            } else {
		                nodeId = node.attr('id');
		                url = baseUrl + "boms/ajaxtreechild/<?php echo h($bom['Bom']['id']); ?>/?id=" + nodeId;
		            } 
		            return url;
		        },
		        "success": function (new_data) {
		        	console.log(new_data);
		            return new_data;
		        }
		    }
		}  
	});
});
</script>
<?php $this->end(); ?>