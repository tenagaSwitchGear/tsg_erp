<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit: <?php echo $bom['Bom']['name']; ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 

	<div class="boms form">
	<?php echo $this->Form->create('Bom'); ?> 
		<?php echo $this->Form->input('id'); ?>
		<div class="form-group">
			<label>Product</label> 
			<?php echo $this->Form->input('InventoryItem.code', array('id' => 'find_product', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Type item name or code')); ?>
				<?php echo $this->Form->input('inventory_item_id', array('id' => 'product_id', 'type' => 'hidden', 'label' => false)); ?> 
		</div>
		<div class="form-group">
		<label>Job / Station</label> 
		<?php echo $this->Form->input('job', array('id' => 'job', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Job No / Station No')); ?>
			<?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden', 'label' => false)); ?>
			<?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden', 'label' => false)); ?>
		 
		</div>
		<div class="form-group">
		<label>Category</label>
		<?php echo $this->Form->input('bom_category_id', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		<div class="form-group">
		<label>Name</label>
		<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		<div class="form-group">
		<label>Code</label>
		<?php echo $this->Form->input('code', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		<div class="form-group">
		<label>Description</label>
		<?php echo $this->Form->input('description', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
		</div> 
		<div class="form-group">
		<label>Status</label>
		<?php $status = array(0 => 'Save as Draft', 1 => 'Template (Active)', 3 => 'Obsolete'); ?>
		<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
		</div> 
	 	<div class="form-group"> 
		<?php echo $this->Form->button('Save', array('class' => 'btn btn-success')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
		</div> 
	</div>
    </div>
  </div> 
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 

    $(document).ready(function() {
    	$("#find_product").autocomplete({
		    source: function (request, response) {
		        $.ajax({
		            type: "GET",                        
		            url:baseUrl + 'inventory_items/ajaxfinditem',           
		            contentType: "application/json",
		            dataType: "json",
		            data: "term=" + $('#find_product').val(),                                                    
		            success: function (data) {
		            	console.log(data);
			            response($.map(data, function (item) {
			                return {
			                    id: item.id,
			                    value: item.code,
			                    name: item.name 
			                }
			            }));
		        	}
		        });
		    },
		    select: function (event, ui) {
		        $("#product_id").val(ui.item.id);//Put Id in a hidden field 
		    },
		    minLength: 3

		}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>Name: " + item.name + "</small><br>" +  "</div>" ).appendTo( ul );
	    };
    	$('#job').autocomplete({ 
		    source: function (request, response){ 
		        $.ajax({
		            type: "GET",                        
		            url:baseUrl + 'sale_jobs/ajaxfindjob',           
		            contentType: "application/json",
		            dataType: "json",
		            data: "term=" + $('#job').val(),                                                    
		            success: function (data) { 
			            response($.map(data, function (item) {
			                return {
			                    id: item.id,
			                    sale_job_child_id: item.sale_job_child_id,
			                    value: item.name,
			                    station: item.station,
			                    customer: item.customer,
			                    customer_id: item.customer_id,
			                    sale_tender_id: item.sale_tender_id,
			                    sale_quotation_id: item.sale_quotation_id,
			                     
			                }
			            }));
			        }
		        });
		    },
		    select: function (event, ui) {   
		        $('#sale_job_id').val(ui.item.id); 
		        $('#sale_job_child_id').val(ui.item.sale_job_child_id); 
		    },
		    minLength: 3
		}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	      	return $( "<li>" ).append( "<div>" + item.value + "<br>" + item.station + "<br><small>" + item.customer + "</small><br>" +  "</div>" ).appendTo( ul );
	    };
	    
    	$("#find_product").autocomplete({
		    source: function (request, response) {
		        $.ajax({
		            type: "GET",                        
		            url:baseUrl + 'inventory_items/ajaxfinditem',           
		            contentType: "application/json",
		            dataType: "json",
		            data: "term=" + $('#find_product').val(),                                                    
		            success: function (data) {
		            	console.log(data);
			            response($.map(data, function (item) {
			                return {
			                    id: item.id,
			                    value: item.name,
			                    code: item.code 
			                }
			            }));
		        	}
		        });
		    },
		    select: function (event, ui) {
		        $("#product_id").val(ui.item.id);//Put Id in a hidden field 
		    },
		    minLength: 3 

		}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>Code: " + item.code + "</small><br>" +  "</div>" ).appendTo( ul );
	    };
    });
</script>
<?php $this->end(); ?>