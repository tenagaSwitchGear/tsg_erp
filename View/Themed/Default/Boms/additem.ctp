

<?php echo $this->Html->link(__('Back To BOM'), array('action' => 'bomchild', $child['BomChild']['id']), array('class' => 'btn btn-success')); ?> 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo h($bom['Bom']['code']); ?> - <?php echo h($child['BomChild']['name']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content">  
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Form->create('BomChild', array('class' => 'form-horizontal form-label-left')); ?>  
        
        <div class="row border-bottom">
          <div class="form-group">
            <label class="col-sm-4"><?php echo $child['BomChild']['name']; ?></label>
            <div class="col-sm-6"><?php echo $child['InventoryItem']['name']; ?> 
            </div>

            <div class="col-sm-2"><?php echo $child['BomChild']['quantity']; ?> <?php echo $child['GeneralUnit']['name']; ?>  

            <input type="hidden" id="sub_assembly_qty" value="<?php echo $child['BomChild']['quantity']; ?>">
            </div> 
          </div>
          <?php $i = 1; ?>
          <?php foreach ($items as $item) { ?>
            <div class="form-group" id="removeBom<?php echo $i; ?>">
              <div class="col-sm-3" id="autoComplete">
              <input type="text" name="find" id="product" class="form-control findProduct" placeholder="Item Code/Name" value="<?php echo $item['InventoryItem']['code']; ?>"required>
              <input type="hidden" name="data[BomItem][inventory_item_id][]" value="<?php echo $item['BomItem']['inventory_item_id']; ?>" id="editItem-<?php echo $i; ?>">
              </div> 
              
              <div class="col-sm-4">
              <input type="text" name="name" id="itemName<?php echo $i; ?>" class="form-control" value="<?php echo $item['InventoryItem']['name']; ?>" readonly>
              </div>

              <div class="col-sm-2">
              <input type="text" name="data[BomItem][temp_quantity][]" id="temp_quantity-<?php echo $i; ?>" class="form-control temp_quantity" placeholder="Qty/Assembly" value="<?php echo $item['BomItem']['temp_quantity'] == 0 ? $item['BomItem']['quantity'] : $item['BomItem']['temp_quantity']; ?>"required>
              </div>

              <div class="col-sm-1">
              <input type="text" name="data[BomItem][quantity][]" id="quantity-<?php echo $i; ?>" class="form-control" placeholder="Total" value="<?php echo $item['BomItem']['quantity']; ?>"readonly>
              </div>

              <div class="col-sm-1">
              <select name="data[BomItem][general_unit_id][]" id="itemUnit<?php echo $i; ?>" class="form-control"required>
              <option value="">-Unit-</option>
              <?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"<?php echo selected($key, $item['BomItem']['general_unit_id']); ?>><?php echo $unit; ?></option><?php } ?>
              </select></div> 

              <div class="col-sm-1">
              <a href="#" class="btn btn-danger" onclick="removeItem(<?php echo $i; ?>); return false"><i class="fa fa-times"></i></a>
              </div>
              </div>
          <?php $i++; ?>
          <?php } ?>
          
          <!-- Load child via ajax -->
          <div id="loadItem"></div>
          <!-- end ajax -->
 
          <div class="form-group"> 
            <div class="col-sm-12"> 
              <a href="#" id="addItem" class="btn btn-default"><i class="fa fa-plus"></i> Add Item</a>
            </div>
          </div>
        </div>
       
      
      <div class="form-group">
        <label class="col-sm-3">&nbsp;</label>
        <div class="col-sm-9">
          <?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right', 'id' => 'checkDuplicate')); ?>
        </div>
      </div> 
        <?php echo $this->Form->end(); ?>   
      </div>
    </div>
  </div> 
</div>

<?php 

function selected($id, $selected) {
  if($id == $selected) {
    return 'selected';
  }
} 
?>

<?php $this->start('script'); ?>
<script type="text/javascript"> 



function findDuplicate(value) {
    var result = 0;
    $(".findProduct").each(function(){
        if (this.value == value) {
            result++;
        }
    });
    return result - 1;
}

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function removeItem(row) {
    $('#removeBom'+row).html('');
    return false;
}

function calcQty(row, val) {
  $('#temp_quantity'+row).on('keyup', function() {
    var temp_quantity = $('#temp_quantity'+row).val();
    var sub_assembly_qty = $('#sub_assembly_qty').val();
    var total = temp_quantity * sub_assembly_qty;  
    $('#quantity'+row).val(total);
  }); 
}

function findItem(row, search) { 
  console.log(search);
  $('#findProduct'+row).autocomplete({ 
      source: function (request, response){ 
          $.ajax({
              type: "GET",                        
              url:baseUrl + 'inventory_items/ajaxfinditem',           
              contentType: "application/json",
              dataType: "json",
              data: "term=" + $('#findProduct'+row).val(),                                                     
              success: function (data) { 
                response($.map(data, function (item) {
                    return {
                        id: item.id,
                        value: item.code,
                        name : item.name,
                        price: item.price,
                        code: item.code,
                        type: item.type,
                        unit: item.unit
                    }
                }));
            }
          });
      },
      select: function (event, ui) {  
          $('#productId'+row).val( ui.item.id ); 
          $('#itemName'+row).val( ui.item.name );  
          $('#itemUnit'+row).val( ui.item.unit ); 

          var data = findDuplicate($('#findProduct'+row).val());

          if(data > 0) {
            $("#findProduct" + row).addClass('border-red');
            $(".findProduct[value='" + ui.item.value + "']").addClass('border-red');
            alert('Item code already exist.');
          } else {
            // Remove class  
            $(".findProduct").removeClass('border-red');   
          }
      },
      minLength: 3
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" ).append( "<div>" + item.code + "<br><small>" + item.name + "</small><br/><small>" + item.type + "</small><br>" +  "</div>" ).appendTo( ul );
  };
} 

$(function() {  

  // Calculate quantity
  $('.temp_quantity').each(function() {
    $(this).on('keyup', function() {
      var element = this.id;
      element = element.split('-');
      element = element[1];
      var temp_quantity = $(this).val();
      var sub_assembly_qty = $('#sub_assembly_qty').val();
      var total = temp_quantity * sub_assembly_qty;  
      $('#quantity-'+element).val(total);
    });
  }); 

  $(".input_name").bind("blur", function () {
        alert(findDuplicate(this.value));
  });
  var count = "<?php echo $i; ?>";
  var station = count + 1;
  $('#addItem').click(function() {
    var html = '<div id="removeBom'+station+'">'; 
      html += '<div class="form-group">';  
      html += '<div class="col-sm-3" id="autoComplete">';
      html += '<input type="text" name="find" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required>';
      html += '<input type="hidden" name="data[BomItem][inventory_item_id][]" id="productId'+station+'">';
      html += '</div>';
      
      html += '<div class="col-sm-4">';
      html += '<input type="text" name="name" id="itemName'+station+'" class="form-control" readonly>';
      html += '</div>';

      html += '<div class="col-sm-2">';
      html += '<input type="text" name="data[BomItem][temp_quantity][]" id="temp_quantity'+station+'" class="form-control" placeholder="Qty/Assembly"required>';
      html += '</div>';

      html += '<div class="col-sm-1">';
      html += '<input type="text" name="data[BomItem][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Total"readonly>';
      html += '</div>';

      html += '<div class="col-sm-1">';
      html += '<select name="data[BomItem][general_unit_id][]" id="itemUnit'+station+'" class="form-control"required>'; 
      html += '<option value="">-Unit-</option>';
      html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
      html += '</select></div>'; 

      html += '<div class="col-sm-1">';
      html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
      html += '</div>';
      html += '</div></div>';    
      $("#loadItem").append(html);  
    
    calcQty(station, $(this).val());  
    findItem(station, $(this).val());  
    station++; 
    return false;
  });  
  var i = 1;
  $("input.findProduct").each(function() {
    $(this).autocomplete({ 
      source: function (request, response){ 
        $.ajax({
            type: "GET",                        
            url:baseUrl + 'inventory_items/ajaxfinditem',           
            contentType: "application/json",
            dataType: "json",
            data: "term=" + $("input.findProduct").val(),                                                    
            success: function (data) { 
              response($.map(data, function (item) {
                return {
                  id: item.id,
                  value: item.code,
                  name : item.name,
                  price: item.price,
                  code: item.code,
                  type: item.type,
                  unit: item.unit
                }
              }));
            }
          });
        },
        select: function (event, ui) {  
          $(this).val( ui.item.value );  
          $("#editItem"+i).val(ui.item.id); 
          $("#itemName"+i).val(ui.item.name); 
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" ).append( "<div>" + item.code + "<br><small>" + item.name + "</small><br/><small>" + item.type + "</small><br>" +  "</div>" ).appendTo( ul );
    };  
    i++;
  }); 
});
</script>
<?php $this->end(); ?>