<?php echo $this->Html->link('BOM Template (Active)', array('action' => 'index'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
 
<?php echo $this->Html->link('BOM Draft', array('action' => 'index/0'), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php echo $this->Html->link('Obsolete', array('action' => 'index/3'), array('class' => 'btn btn-danger', 'escape' => false)); ?>

<?php echo $this->Html->link('<i class="fa fa-plus"></i> Add New BOM', array('action' => 'add'), array('class' => 'btn btn-success', 'escape' => false)); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>B.O.M</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 


        <?php echo $this->Session->flash(); ?>

    <?php echo $this->Form->create('Bom', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
		<table cellpadding="0" cellspacing="0" class="table">
			<tr>
			<td><?php echo $this->Form->input('name', array('placeholder' => 'Bom name', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
			</td>
			<td><?php echo $this->Form->input('code', array('type' => 'text', 'placeholder' => 'Code', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
			</td>
			 
			<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
			</tr>
		</table>
	<?php $this->end(); ?>

        <div class="table-responsive"> 
			<table cellpadding="0" cellspacing="0" class="table table-bordered">
			<thead>
			<tr> 
					<th><?php echo $this->Paginator->sort('code'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<!--<th><?php echo $this->Paginator->sort('description'); ?></th>-->
					<th><?php echo $this->Paginator->sort('created'); ?></th>
					<!--<th><?php echo $this->Paginator->sort('modified'); ?></th>-->
					<!--<th><?php echo $this->Paginator->sort('user_id'); ?></th>-->
					<th><?php echo $this->Paginator->sort('status'); ?></th>
					<!--<th><?php echo $this->Paginator->sort('price'); ?></th>-->
					<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($boms as $bom): ?>
			<tr> 
				 
				<td><?php echo h($bom['Bom']['code']); ?>&nbsp;</td>
				<td><?php echo h($bom['Bom']['name']); ?>&nbsp;</td>
				<!--<td><?php echo h($bom['Bom']['description']); ?>&nbsp;</td>-->
				<td><?php echo h($bom['Bom']['created']); ?>&nbsp;</td>
				<!--<td><?php echo h($bom['Bom']['modified']); ?>&nbsp;</td>-->
				<!--<td>
					<?php echo $this->Html->link($bom['User']['id'], array('controller' => 'users', 'action' => 'view', $bom['User']['id'])); ?>
				</td>-->
				<td><?php echo status($bom['Bom']['status']); ?></td>
				<!--<td><?php echo h($bom['Bom']['price']); ?>&nbsp;</td>-->
				<td class="actions">
					<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $bom['Bom']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
					<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $bom['Bom']['id']), array('class' => 'btn btn-default', 'escape' => false)); ?>

					<?php echo $this->Html->link('<i class="fa fa-sitemap"></i>', array('action' => 'bomchild', $bom['Bom']['id']), array('class' => 'btn btn-warning', 'escape' => false)); ?>

					<?php echo $this->Form->postLink('<i class="fa fa-copy"></i>', array('action' => 'copy', $bom['Bom']['id']), array('class' => 'btn btn-default', 'escape' => false), __('Are you sure you want to Copy %s?', $bom['Bom']['name'])); ?>

					<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $bom['Bom']['id']), array('class' => 'btn btn-danger', 'escape' => false), __('Are you sure you want to delete %s?', $bom['Bom']['name'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
			</table>
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div>
      
      </div>
    </div>
  </div> 
</div>

 
 
<?php function status($status) {

	if($status == 0) {
		return 'Draft';
	} elseif($status == 1) {
		return 'Active';
	} elseif($status == 2) {
		return 'Costing';
	}
}

?>