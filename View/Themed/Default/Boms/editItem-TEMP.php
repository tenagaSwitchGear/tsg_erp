

function editItem(id) { 
    var html = '<div class="form-group">';
    html += '<div class="col-sm-3">';
    html += '<select name="category" id="category-'+id+'" class="form-control" onchange="getItemByCategoryId('+id+', '+rowItem2+'); return false"><option value="">Select Category</option>';
    <?php foreach($categories as $category) { ?> html += '<option value="<?php echo $category['InventoryItemCategory']['id']; ?>"><?php echo $category['InventoryItemCategory']['name']; ?></option>'; <?php } ?> 
    html += '</select></div>';
    html += '<div class="col-sm-3">';
    html += '<select name="item_id" class="form-control" id="item_id-'+id +'"><option value="">Select Item</option>';
    html += '</select>'; 
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<input type="text" id="quantity-'+id+'" class="form-control">';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<select name="unit" id="quantity_unit_id-'+id+'" class="form-control"><option value="">Select Qty Unit</option>';
    <?php foreach($units as $unit) { ?> html += '<option value="<?php echo $unit['GeneralUnit']['id']; ?>"><?php echo $unit['GeneralUnit']['name']; ?></option>'; <?php } ?> 
    html += '</select></div>';
    html += '<div class="col-sm-2">'; 
    html += '<a href="#" onclick="updateItem('+ id +'); return false" class="btn btn-success"><i class="fa fa-save"></i></a>';
    html += '</div>';
    html += '<div class="col-sm-2" id="respondItem-'+id+'"></div>';
    html += '</div>'; 
     
    $('#removeItem-' + id).html(html);
    return false;
}