<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo __('Add BOM'); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="boms form">
		<?php echo $this->Form->create('Bom', array('class' => 'form-horizontal form-label-left')); ?> 
			
			<p>To create Billed Of Material (B.O.M), please make sure Finished Good already added to Item General. All field mark with * is required.</p>
			<div class="form-group">
				<label class="col-sm-3">Finished Good Name / Code *</label>
				<div class="col-sm-9">
				<?php echo $this->Form->input('find_product', array('id' => 'find_product', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Type item name or code', 'required' => true)); ?>
					<?php echo $this->Form->input('inventory_item_id', array('id' => 'product_id', 'type' => 'hidden', 'label' => false)); ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3">Job / Station (Optional)</label>
				<div class="col-sm-9">
				<?php echo $this->Form->input('job', array('id' => 'job', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Job No / Station No', 'required' => false)); ?>
					<?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden', 'label' => false, 'required' => false)); ?>
					<?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden', 'label' => false, 'required' => false)); ?>
				</div>
			</div>

			<div class="form-group" style="display: none;">
				<label class="col-sm-3">Category</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('bom_category_id', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">BOM Name *</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">BOM Code *</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('code', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Description (Optional)</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('description', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
				</div>
			</div> 
			<div class="form-group">
				<label class="col-sm-3">Status *</label>
				<div class="col-sm-9">
					<?php $status = array(0 => 'Save as Draft', 1 => 'Template (Active)'); ?>
					<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>

			<!-- BOM Child 
			<div class="form-group">	
				<label class="col-sm-3">Sub Assembly</label>
				<div class="col-sm-8">
					<?php echo $this->Form->input('BomChild.name.', array('id' => 'sub0', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Enter Title')); ?>
				</div>
				<div class="col-sm-1">
					 
				</div>
			</div> -->
			<div class="form-group">	 
				<div class="col-sm-12"><p>Please add at least 1 sub assembly.</p></div>
			</div>

			<div id="child"></div> 

			<div class="form-group">	 
				<div class="col-sm-12">
					 <a href="#" class="btn btn-primary" id="addChild"><i class="fa fa-plus"></i> Add Sub Assembly</a>
				</div> 
			</div> 

			<!-- End child --> 

			<div class="form-group">
				<label class="col-sm-3">&nbsp;</label>
				<div class="col-sm-9">
					<?php echo $this->Form->button('Next Step', array('class' => 'btn btn-success pull-right', 'id' => 'submit')); ?>
				</div>
			</div> 
			<?php echo $this->Form->end(); ?>  
		
		</div> 
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
	

    function deleteChild(id) {
        $('#remove-'+id).html('');
        return false;
    }

    function findItem(row, search) { 
	  
	  $('#sub'+row).autocomplete({ 
	      source: function (request, response){ 
	      	var term = $('#sub' + row).val();
	      	var url = '';
	      	console.log(term.substr(0, 8));
	      	if(term.substr(0, 8) == 'copybom-') {
	      		url = 'boms/ajaxcopybom';
	      	} else if(term.substr(0, 8) == 'copysub-') {
	      		url = 'boms/ajaxcopychild';
	      	} else {
	      		url = 'inventory_items/ajaxfinditem';
	      	}
	      	if(url != '') {
		      	$.ajax({
					type: "GET",                        
					url:baseUrl + url,           
					contentType: "application/json",
					dataType: "json",
					data: "term=" + term,                                                    
					success: function (data) { 
						response($.map(data, function (item) {
							if(term.substr(0, 8) == 'copybom-') {
							    return {
							        id: item.id,
							        value: item.code,
							        name : item.name,
							        type : 'bom', 
							        bom_id : item.id,
							        inventory_item_id: item.inventory_item_id
							    }
						    } else if(term.substr(0, 8) == 'copysub-') {
						    	return {
							        id: item.id,
							        value: item.name,
							        code : item.code, 
							        bom_name : item.bom_name,
							        bom_id : item.bom_id,
							        type: 'child',
							        inventory_item_id: item.inventory_item_id
							    }
						    } else {
						    	return {
			                        id: item.id,
			                        value: item.code,
			                        name : item.name,
			                        price: item.price,
			                        code: item.code,
			                        type: item.type,
			                        unit: item.unit,
							        inventory_item_id: item.inventory_item_id
			                    }
						    }
						}));
					}
				});	
	        } 
	      },
	      select: function (event, ui) {    
	          if(ui.item.type == 'bom') { 
	          	$('#copy_from_id' + row).val(ui.item.id);
	          	$('#type' + row).val('bom');
	          	$('#bom_id' + row).val(ui.item.bom_id);
	          	$('#item_type' + row).val(0);
	          	$('#inventory_item_id'+row).val(ui.item.inventory_item_id);
	          } else if(ui.item.type == 'child') { 
	          	$('#copy_from_id' + row).val(ui.item.id);
	          	$('#type' + row).val('child');
	          	$('#bom_id' + row).val(ui.item.bom_id);
	          	$('#item_type' + row).val(0);
	          	$('#inventory_item_id'+row).val(ui.item.inventory_item_id);
	          } else {
	          	$('#copy_from_id' + row).val(ui.item.id);
	          	$('#type' + row).val('item');
	          	$('#bom_id' + row).val(ui.item.id);
	          	$('#item_type' + row).val(ui.item.type);
	          	$('#inventory_item_id'+row).val(ui.item.id); 
	          	$('#unit'+row).val(ui.item.unit);
	          	$('#name'+row).val(ui.item.name);
	          }
	      },
	      minLength: 3
	  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
	  		if(item.type == 'bom') {
	  			return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/>" +  "</div>" ).appendTo( ul );
	  		} else if(item.type == 'child') {
	  			return $( "<li>" ).append( "<div>" + item.value + "<br><small>BOM: " + item.code + "</small><br/>" +  "</div>" ).appendTo( ul );
	  		} else {
	  			return $( "<li>" ).append( "<div>" + item.value + "<br><small>Item: " + item.name + "</small><br/>" +  "</div>" ).appendTo( ul );
	  		}
	  };
	} 

    $(document).ready(function() {

    	$('#submit').on('click', function() {
    		var sub_assembly = $('.sub-assembly').val();
    		if(sub_assembly == undefined) {
    			alert('Please add at lease 1 sub assembly');
    			return false;
    		}
    	});
    	var row = 1; 
    	$('#addChild').on('click', function() {
	    	var html = '<div id="remove-'+row+'">'; 
	        html += '<div class="form-group">'; 
	        html += '<div class="col-sm-4">';
	        html += '<input name="data[BomChild][name][]" id="sub'+row+'" class="form-control sub-assembly" placeholder="Sub assembly" type="text" required="required">';
	        html += '<input name="data[BomChild][copy_from_id][]" id="copy_from_id'+row+'" type="hidden">';
	        html += '<input name="data[BomChild][bom_id][]" id="bom_id'+row+'" type="hidden">';

	        html += '<input name="data[BomChild][type][]" id="type'+row+'" type="hidden">';
	        html += '<input name="data[BomChild][item_type][]" id="item_type'+row+'" type="hidden">';
	        
	        html += '</div>'; 
	        html += '<input name="data[BomChild][inventory_item_id][]" id="inventory_item_id'+row+'" type="hidden" value="0">';
	        html += '<div class="col-sm-4">';
	        html += '<input id="name'+row+'" class="form-control sub-assembly" placeholder="Name" type="text"readonly></div>';
	        html += '<div class="col-sm-1">';
	        html += '<input name="data[BomChild][quantity][]" id="quantity'+row+'" type="text" class="form-control" placeholder="Quantity" value="1">';
	        html += '</div>';

	        html += '<div class="col-sm-2">';
	       	html += '<select name="data[BomChild][general_unit_id][]" id="unit'+row+'" class="form-control">';
	       	html += '<option value="">-Unit-</option>';
      		html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
      		html += '</select>'; 
	        html += '</div>';

	        html += '<div class="col-sm-1">';
	        html += '<a href="#" onclick="deleteChild('+row+'); return false" class="btn btn-danger"><i class="fa fa-times"></i></a>';
	        html += '</div>';
	        html += '</div>';
	        html += '</div>';
	        $('#child').append(html);
	        findItem(row, $(this).val());  
	        row ++;
	        
	        return false; 	
    	});
        

    	$('#job').autocomplete({ 
		    source: function (request, response){ 
		        $.ajax({
		            type: "GET",                        
		            url:baseUrl + 'sale_jobs/ajaxfindjob',           
		            contentType: "application/json",
		            dataType: "json",
		            data: "term=" + $('#job').val(),                                                    
		            success: function (data) { 
			            response($.map(data, function (item) {
			                return {
			                    id: item.id,
			                    sale_job_child_id: item.sale_job_child_id,
			                    value: item.name,
			                    station: item.station,
			                    customer: item.customer,
			                    customer_id: item.customer_id,
			                    sale_tender_id: item.sale_tender_id,
			                    sale_quotation_id: item.sale_quotation_id,
			                     
			                }
			            }));
			        }
		        });
		    },
		    select: function (event, ui) {   
		        $('#sale_job_id').val(ui.item.id); 
		        $('#sale_job_child_id').val(ui.item.sale_job_child_id); 
		    },
		    minLength: 3
		}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	      	return $( "<li>" ).append( "<div>" + item.value + "<br>" + item.station + "<br><small>" + item.customer + "</small><br>" +  "</div>" ).appendTo( ul );
	    };

    	$("#find_product").autocomplete({
		    source: function (request, response) {
		        $.ajax({
		            type: "GET",                        
		            url:baseUrl + 'inventory_items/ajaxfinditem',           
		            contentType: "application/json",
		            dataType: "json",
		            data: "term=" + $('#find_product').val(),                                                    
		            success: function (data) {
		            	console.log(data);
			            response($.map(data, function (item) {
			                return {
			                    id: item.id,
			                    value: item.code,
			                    name: item.name 
			                }
			            }));
		        	}
		        });
		    },
		    select: function (event, ui) {
		        $("#product_id").val(ui.item.id);//Put Id in a hidden field 
		    },
		    minLength: 3

		}).autocomplete( "instance" )._renderItem = function( ul, item ) {
	      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>Name: " + item.name + "</small><br>" +  "</div>" ).appendTo( ul );
	    };
    });
</script>
<?php $this->end(); ?>
 