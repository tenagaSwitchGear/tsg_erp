
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo __('Copy Item From Sub Assembly'); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 

    <h4>BOM Code: <?php echo $bom['Bom']['code']; ?></h4>

    <?php if($child != null) { ?>
    	<h5>Parent: <?php echo $child['BomChild']['name']; ?></h5>
    <?php } ?>

    <?php echo $this->Session->flash(); ?> 
	<?php echo $this->Form->create('BomChild', array('class' => 'form-horizontal')); ?> 
	<?php 
		echo $this->Form->input('bom_id', array('type' => 'hidden', 'value' => $bom['Bom']['id']));
		echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' => $child['BomChild']['id']));
	?> 

	<div class="form-group row">
	<label class="col-sm-3">Copy From Sub Assembly</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('name', array('id' => 'sub', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Please enter sub assembly name / code')); ?>
	<?php echo $this->Form->input('copy_from_id', array('id' => 'copy_from_id', 'type' => 'hidden')); ?> 
	<?php echo $this->Form->input('copy_from_bom_id', array('id' => 'copy_from_bom_id', 'type' => 'hidden')); ?> 
	<div id="additional"></div>
	</div>
	</div>  
	<div class="form-group">
		<label class="col-sm-3">&nbsp;</label>
		<div class="col-sm-9">
			<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
		</div>
	</div> 
	<?php echo $this->Form->end(); ?>
</div>
</div> 
</div> 
</div>


<?php $this->start('script'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#sub').autocomplete({ 
	      source: function (request, response){ 
	      	var term = $('#sub').val(); 
	      	$.ajax({
				type: "GET",                        
				url: baseUrl + 'boms/ajaxcopyitem',           
				contentType: "application/json",
				dataType: "json",
				data: "term=" + term,                                                    
				success: function (data) { 
					response($.map(data, function (item) { 
				    	return {
	                        id: item.id,
	                        value: item.name,
	                        bom_name : item.bom_name, 
	                        bom_code: item.bom_code,
	                        bom_id: item.bom_id
	                    } 
					}));
				}
			});	 
	      },
	      select: function (event, ui) {     
          	$('#copy_from_id').val(ui.item.id); 
          	$('#copy_from_bom_id').val(ui.item.bom_id); 
          	$('#additional').html('Sub Assembly Name: ' + ui.item.name + ' - From BOM: <b>' + ui.item.bom_code + '</b>'); 
	      },
	      minLength: 3
	  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
	  		return $( "<li>" ).append( "<div>"+ item.id + " - " + item.value + "<br><small>BOM: " + item.bom_code + "</small><br/>" +  "</div>" ).appendTo( ul ); 
	  };
	});
</script>
<?php $this->end(); ?> 