<?php 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Costing';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting Approval';
  } elseif($status == 4) {
    $data = 'Approved';
  } elseif($status == 7) {
    $data = 'Rejected';
  } 
  return $data;
}

function markup($unit, $amount, $margin) {
  if($unit == 1) {
    $total = ($amount / 100) * $margin + $amount;
  } 
  if($unit == 2) {
    $total = $amount + $margin;
  }
  return $total;
}
?> 


<div class="row">
<div class="col-sm-5">
<?php echo $this->Html->link('Back To Costing List', array('action' => 'index'), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php echo $this->Html->link('Currency', array('action' => 'currency', $saleQuotation['SaleQuotation']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
</div>

<div class="col-sm-7">
<?php if($saleQuotation['SaleQuotation']['status'] == 2 || $saleQuotation['SaleQuotation']['status'] == 7) { ?>
<?php echo $this->Form->create('SaleQuotation', array('class'=>'form-horizontal')); ?> 
<?php echo $this->Form->input('status', array('type' => 'hidden', 'value' => 3)); ?> 
<?php echo $this->Form->submit('Submit For Approval', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>
<?php $this->Form->end(); ?>
<?php } ?>
</div>

  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Costing</h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php echo $this->Session->flash(); ?>
        

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table">

            <div class="box-blue">
<?php if($saleQuotation['SaleQuotation']['status'] == 3) { ?>
<h4>Approval Section</h4>
        <div id="remark">
          <?php echo $this->Form->create('SaleQuotationRemark', array('class' => 'form-horizontal')); ?> 

          <div class="form-group">
          <label class="col-sm-3">Remark</label>
          <div class="col-sm-9">
          <?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Remark / note...', 'required' => false)); ?> 
          </div>
          </div>

          <div class="form-group">
          <label class="col-sm-3">Status</label>
          <div class="col-sm-9">
          <?php echo $this->Form->input('status', array('options' => array(4 => 'Approve', 7 => 'Reject'), 'empty' => '-Select Status-', 'class' => 'form-control', 'label' => false, 'required' => true)); ?> 
          </div>
          </div>
          <div class="form-group">
            <label class="col-sm-12">Price Book Section</label>
            <div class="col-sm-12"><p>By choosing Save To Price Book will allow Sales Department to select Price during create quotation for customers. Choose Default Price will <b>Override</b> current default Price Book.</p></div>
            </div>
          <div class="form-group">
            <label class="col-sm-3">Save To Price Book</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('pricebook', array('options' => array(1 => 'Yes', 0 => 'No'), 'empty' => '-Select-', 'class' => 'form-control', 'label' => false, 'required' => true)); ?> 
            </div>
            </div>
          <div class="form-group">
            <label class="col-sm-3">Expiry Date</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('SaleQuotation.date', array('id' => 'dateonly', 'type' => 'text', 'class' => 'form-control', 'label' => false, 'required' => true)); ?> 
            </div>
            </div>
          <div class="form-group">
            <label class="col-sm-3">Default Price</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('default', array('options' => array(1 => 'Yes', 0 => 'No'), 'empty' => '-Select-', 'class' => 'form-control', 'label' => false, 'required' => true)); ?> 
            </div>
        </div> 

          <div class="form-group">
          <label class="col-sm-3"></label>
          <div class="col-sm-9">
           <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?>
          </div>
          </div>

          <?php $this->end(); ?>
        </div>
        <?php } ?>

<?php if($books) { ?>
<h4>Current Price Book</h4>
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
  <thead>
    <tr> 
      <td>Item</td>
      <td>Is Default</td>
      <td>Expiry Date</td> 
      <td>Created</td>  
      <td>Price</td> 
    </tr>
  </thead> 
<tbody>
<?php  
  foreach($books as $book) { ?>  
  <tr>
    <td><?php echo $book['InventoryItem']['code']; ?></td> 
    <td><?php echo $book['InventorySupplierItem']['is_default'] == 1 ? 'Yes' : 'No'; ?></td>
    <td><?php echo $book['InventorySupplierItem']['expiry_date']; ?></td> 
    <td><?php echo $book['InventorySupplierItem']['created']; ?></td> 
    <td><?php echo _n2($book['InventorySupplierItem']['price_per_unit']); ?></td>  
  </tr>
  <?php } ?>
   
</tbody> 
</table>
<?php } else { ?>
<p>Price book not found.</p>
<?php } ?>

<?php if($remarks) { ?>
<h4>Verifier Remarks</h4>
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
  <thead>
    <tr> 
      <td>Remark</td>
      <td>PIC</td> 
      <td>Created</td>  
      <td>Status</td> 
    </tr>
  </thead> 
<tbody>
<?php  
  foreach($remarks as $remark) { ?>  
  <tr>
    <td width="50%"><?php echo $remark['SaleQuotationRemark']['remark']; ?></td>
    <td><?php echo $remark['User']['username']; ?></td> 
    <td><?php echo $remark['SaleQuotationRemark']['created']; ?></td> 
    <td><?php echo status($remark['SaleQuotationRemark']['status']); ?></td>  
  </tr>
  <?php } ?>
   
</tbody> 
</table>
<?php } ?>

</div>

<h4>Costing Info</h4>
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">  
  <tr>
    <td width="30%">Item</td>
    <td><?php echo $saleQuotation['InventoryItem']['code']; ?></td>  
  </tr> 
  <tr>
    <td width="30%">BOM</td>
    <td><?php echo $saleQuotation['Bom']['code']; ?></td>  
  </tr> 
  <tr>
    <td width="30%">Created</td>
    <td><?php echo $saleQuotation['SaleQuotation']['created']; ?></td>  
  </tr> 
  <tr>
    <td width="30%">User</td>
    <td><?php echo $saleQuotation['User']['username']; ?></td>  
  </tr> 
  <tr>
    <td width="30%">Status</td>
    <td><?php echo status($saleQuotation['SaleQuotation']['status']); ?></td>  
  </tr> 
  <tr>
    <td width="30%">Expiry Date</td>
    <td><?php echo $saleQuotation['SaleQuotation']['date']; ?></td>  
  </tr>   
</table>

              <table class="table table-striped">
                <thead>
                  <tr> 
                    <th>Code</th> 
                    <th>Qty</th>
                    <th>Cost</th>
                    <th>Margin %</th>
                    <th>Price</th> 
                    
                    <th>Subtotal</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody> 
                <?php foreach ($items as $item) { ?> 
                  <tr> 
                    <td><?php echo h($item['InventoryItem']['code']); ?><br/>
                    <small><?php echo h($item['InventoryItem']['name']); ?></small>
                    </td> 
                    <td><?php echo h($item['SaleQuotationItem']['quantity']); ?></td>
                    <td><?php echo h(number_format($item['SaleQuotationItem']['total_cost'], 4)); ?></td> 
                    
                    <td><?php echo h($item['SaleQuotationItem']['planning_margin']); ?></td> 
                    <td><?php echo h(number_format($item['SaleQuotationItem']['planning_price'], 4)); ?></td> 
                    <?php 
                    $sub_total = $item['SaleQuotationItem']['planning_price'];
                    
                    $total = $item['SaleQuotationItem']['planning_price'] * $item['SaleQuotationItem']['quantity'];
 
                    ?>

                    <td class="align-right"><b><?php echo h(number_format($total, 2)); ?></b></td> 
                    <td>
                    <?php echo $this->Html->link(__('Costing'), array('controller' => 'sale_boms', 'action' => 'costing', $item['SaleQuotationItem']['id']), array('class' => 'btn btn-xs btn-default')); ?>

                    <?php echo $this->Html->link(__('Item Cost'), array('action' => 'itemcost', $item['SaleQuotationItem']['id']), array('class' => 'btn btn-xs btn-default')); ?>

                    <?php echo $this->Html->link(__('Margin'), array('action' => 'viewitem', $item['SaleQuotationItem']['id']), array('class' => 'btn btn-xs btn-default')); ?>
                     
                    </td> 
                  </tr> 
                <?php } ?> 
                   
                </tbody>
              </table>

             

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row 

          <div class="row">
           
            <div class="col-xs-6">
               
              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
              </p>
            </div>
           
            <div class="col-xs-6">
              <p class="lead">Amount Due 2/22/2014</p>
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <tr>
                      <th style="width:50%">Subtotal:</th>
                      <td>$250.30</td>
                    </tr>
                    <tr>
                      <th>Tax (9.3%)</th>
                      <td>$10.34</td>
                    </tr>
                    <tr>
                      <th>Shipping:</th>
                      <td>$5.80</td>
                    </tr>
                    <tr>
                      <th>Total:</th>
                      <td>$265.24</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
          -->

          <!-- this row will not appear when printing 
          <div class="row no-print">
            <div class="col-xs-12">
              <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button> 
            </div>
          </div>--> 
      </div>
    </div>
  </div>
</div>