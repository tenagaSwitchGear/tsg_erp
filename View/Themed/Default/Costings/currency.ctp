<?php echo $this->Html->link('Back To Costing', array('action' => 'view', $this->params['pass'][0]), array('class' => 'btn btn-default', 'escape' => false)); 
 

?>


<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Costing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>        

        <div class="form-group">
            <h4 class="col-sm-12">Exchange Rate <b><?php echo Configure::read('Site.default_currency'); ?>1</b> agains X</h4> 
        </div>  
        <?php echo $this->Form->create('SaleQuotationCurrency', array('class'=>'form-horizontal')); ?> 
        <table class="table">
        <tr>
        <th>Name</th>
        <th>Symbol</th>
        <th>ISO Code</th>
        <th>Actual Rate</th>
        <th>Margin(%)</th>
        <th>Total</th>
        </tr>
        <?php if($rates) { ?>
            <?php foreach($rates as $currency) { ?>  
                <tr id="tr<?php echo $currency['GeneralCurrency']['iso_code']; ?>">
                    <td><?php echo $currency['GeneralCurrency']['name']; ?></td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['symbol']; ?>
                    </td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['iso_code']; ?>
                    </td>
                    <td> 
                        <?php echo $this->Form->input('actual_rate.', array('value' => $currency['SaleQuotationCurrency']['actual_rate'], 'class' => 'form-control', 'label' => false, 'id' => 'actual'.$currency['GeneralCurrency']['iso_code'])); ?> 
                    </td> 
                    <td> 
                        <?php echo $this->Form->input('margin.', array('value' => $currency['SaleQuotationCurrency']['margin'], 'class' => 'form-control', 'label' => false, 'id' => 'margin'.$currency['GeneralCurrency']['iso_code'])); ?> 
                    </td> 
                    <td>
                        <?php echo $this->Form->input('general_currency_id.', array('value' => $currency['GeneralCurrency']['id'], 'type' => 'hidden', 'label' => false)); ?>
                        <?php echo $this->Form->input('rate.', array('value' => $currency['SaleQuotationCurrency']['rate'], 'class' => 'form-control', 'label' => false, 'id' => 'rate'.$currency['GeneralCurrency']['iso_code'])); ?> 
                    </td> 
                </tr>  

<?php $this->start('script'); ?>
<script type="text/javascript">  
    $(document).ready(function() {
        var id = "<?php echo $currency['GeneralCurrency']['iso_code']; ?>";
        ("#margin"+id).on('keyup', function() {
            var actual_rate = parseFloat($('#actual'+id).val());    
            var margin = parseFloat($('#margin'+id).val());  
            var total = 0;
            if(actual_rate != '' && margin != '') {
                if(margin != 0) {
                    total = ((actual_rate / 100) * margin) + actual_rate;
                } else {
                    total = actual_rate;
                }
                
                //console.log(total.toFixed(4));
                $('#rate'+id).val(total.toFixed(5));
            } 
        });
    });
</script>
<?php $this->end(); ?>

            <?php } ?>

        <?php } else { ?>
            <?php foreach($currencies as $currency) { ?>  
                <tr id="tr<?php echo $currency['GeneralCurrency']['iso_code']; ?>">
                    <td><?php echo $currency['GeneralCurrency']['name']; ?></td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['symbol']; ?>
                    </td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['iso_code']; ?>
                    </td>
                    <td> 
                        <?php echo $this->Form->input('actual_rate.', array('value' => $currency['GeneralCurrency']['rate'], 'class' => 'form-control', 'label' => false, 'id' => 'actual'.$currency['GeneralCurrency']['iso_code'])); ?> 
                    </td> 
                    <td> 
                        <?php echo $this->Form->input('margin.', array('class' => 'form-control', 'label' => false, 'id' => 'margin'.$currency['GeneralCurrency']['iso_code'], 'value' => $currency['GeneralCurrency']['margin'])); ?>  
                    </td> 
                    <?php $total = $currency['GeneralCurrency']['rate'] * $currency['GeneralCurrency']['margin']; ?>
                    <td>
                        <?php echo $this->Form->input('general_currency_id.', array('value' => $currency['GeneralCurrency']['id'], 'type' => 'hidden', 'label' => false)); ?>
                        <?php echo $this->Form->input('rate.', array('class' => 'form-control', 'label' => false, 'id' => 'rate'.$currency['GeneralCurrency']['iso_code'], 'value' => $total)); ?> 
                    </td> 
                </tr>  
<?php $this->start('script'); ?>
<script type="text/javascript">  
    $(document).ready(function() {
        var id = "<?php echo $currency['GeneralCurrency']['iso_code']; ?>";
        $("#margin"+id).on('keyup', function() {
            var actual_rate = parseFloat($('#actual'+id).val());    
            var margin = parseFloat($('#margin'+id).val());  
            var total = 0;
            if(actual_rate != '' && margin != '') {
                if(margin != 0) {
                    total = ((actual_rate / 100) * margin) + actual_rate;
                } else {
                    total = actual_rate;
                }
                //console.log(total.toFixed(4));
                $('#rate'+id).val(total.toFixed(5));
            } 
        });
    });
</script>
<?php $this->end(); ?>
            <?php } ?>

        <?php } ?>
        </table>

        <?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success')); ?>
             
        <?php $this->Form->end(); ?> 
        </div>
        </div>
    </div>
</div>

