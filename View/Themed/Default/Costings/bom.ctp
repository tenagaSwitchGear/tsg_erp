<?php echo $this->Html->css('/js/libs/jstree/themes/apple/style.css'); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="view-data">
		<h2><?php echo __('Bom'); ?></h2>
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($salebom['SaleBom']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Bom Category'); ?></dt>
				<dd>
					<?php echo $this->Html->link($bom['BomCategory']['name'], array('controller' => 'bomcategories', 'action' => 'view', $bom['BomCategory']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Name'); ?></dt>
				<dd>
					<?php echo h($salebom['SaleBom']['name']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Code'); ?></dt>
				<dd>
					<?php echo h($salebom['SaleBom']['code']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Description'); ?></dt>
				<dd>
					<?php echo h($salebom['SaleBom']['description']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php echo h($salebom['SaleBom']['created']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php echo h($salebom['SaleBom']['modified']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('User'); ?></dt>
				<dd>
					<?php echo $this->Html->link($bom['User']['username'], array('controller' => 'users', 'action' => 'view', $bom['User']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Status'); ?></dt>
				<dd>
					<?php echo h($salebom['SaleBom']['status']); ?>
					&nbsp;
				</dd>
				 
			</dl>
		</div>

		<div class="col-xs-12">
			<ul><li><?php echo h($salebom['SaleBom']['name']); ?> (<?php echo h($salebom['SaleBom']['code']); ?>)</li></ul>
		  	<ul id="tree_root_1"></ul>
		</div>
      
      </div>
    </div>
  </div> 
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('libs/jstree/jquery.jstree.js'); ?> 
<script type="text/javascript">
$(function() { 
	$("#tree_root_1").jstree({ 
		"plugins" : ["themes", "json_data", "ui", "cookie"],
		"json_data" : {
		    "ajax" : {
		        "type": 'GET',
		        "url": function (node) {
		            if (node == -1) {
		                url = baseUrl + "sale_quotations/ajaxtree/<?php echo h($salebom['SaleBom']['id']); ?>";
		            } else {
		                nodeId = node.attr('id');
		                url = baseUrl + "sale_quotations/ajaxtreechild/<?php echo h($salebom['SaleBom']['id']); ?>/?id=" + nodeId;
		            } 
		            return url;
		        },
		        "success": function (new_data) {
		        	console.log(new_data);
		            return new_data;
		        }
		    }
		}  
	});
});
</script>
<?php $this->end(); ?>