<?php

function discount($type) {
	if($type == 1) {
		$data = 'Percentage (%)';
	} elseif($type == 2) {
		$data = 'Fixed';
	} else {
		$data = 'N/A';
	}
	return $data;
}
 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Planning';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting HOD Verification';
  } elseif($status == 4) {
    $data = 'HOD Verified';
  } elseif($status == 5) {
    $data = 'Waiting GM Approval';
  } elseif($status == 6) {
    $data = 'GM Approved';
  } elseif($status == 7) {
    $data = 'HOD Rejected';
  } elseif($status == 8) {
    $data = 'GM Rejected';
  } elseif($status == 9) {
    $data = 'Quotation Submitted';
  } elseif($status == 10) {
    $data = 'Awarded';
  } elseif($status == 11) {
    $data = 'Failed';
  } 
  return $data;
}
?>

<?php echo $this->Html->link('Back To Quotation', array('controller' => 'sale_quotations', 'action' => 'view', $detail['SaleQuotation']['id']), array('class' => 'btn btn-success')); ?>

<?php if($is_bom) { ?> 
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo $bom['SaleBom']['name']; ?> (<?php echo $bom['SaleBom']['code']; ?>) Qty: <?php echo $price['SaleQuotationItem']['quantity']; ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        <?php echo $this->Session->flash(); ?> 
<?php if(isset($_GET['ref'])) { ?>
	<?php echo $this->Form->create('SaleQuotationRemark', array('class' => 'form-horizontal')); ?> 

	<div class="form-group">
	<label class="col-sm-3">Remark</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Remark / note...', 'required' => false)); ?> 
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3">Status</label>
	<div class="col-sm-9">
	<?php $status = array(6 => 'Approve', 7 => 'Reject'); ?>
	<?php echo $this->Form->input('status', array('options' => $status, 'empty' => '-Select Status-', 'class' => 'form-control', 'label' => false)); ?> 
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3"></label>
	<div class="col-sm-9">
	 <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?>
	</div>
	</div>

	<?php $this->end(); ?>
<?php } ?>

<?php if($remarks) { ?>
<h4>Remarks</h4>
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
	<thead>
		<tr> 
			<td>Remark</td>
			<td>PIC</td> 
			<td>Created</td>  
			<td>Status</td> 
		</tr>
	</thead> 
<tbody>
<?php  
	foreach($remarks as $remark) { ?>  
  <tr>
    <td><?php echo $remark['SaleQuotationRemark']['remark']; ?></td>
    <td><?php echo $remark['User']['username']; ?></td> 
    <td><?php echo $remark['SaleQuotationRemark']['created']; ?></td> 
    <td><?php echo status($remark['SaleQuotationRemark']['status']); ?></td>  
  </tr>
  <?php } ?>
   
</tbody> 
</table>
<?php } ?>

        <?php if(isset($_GET['ref'])) { ?>

        <table class="table table-bordered">
        <tr>
        	<td colspan="2"><h4>Planning Department</h4></td> 
        </tr>
        <tr>
        	<td>Material Cost:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['material_cost'], 2); ?></td>
        </tr>
        <tr>
        	<td>Indirect Cost:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['indirect_cost'], 2); ?></td>
        </tr>
        <tr>
        	<td>Soft Cost:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['soft_cost'], 2); ?></td>
        </tr>
        <tr>
        	<td>Planning Margin:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['planning_margin'], 2); ?>% (<?php echo number_format($price['SaleQuotationItem']['margin_value'], 2); ?>)</td>
        </tr>
        <tr>
        	<td>Planning Total:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['planning_price'], 2); ?></td>
        </tr>
        <tr>
        	<td colspan="2"><h4>Sales Department</h4></td> 
        </tr> 
        <tr>
        	<td>Discount:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['discount'], 2); ?></td>
        </tr>
        <tr>
        	<td>Discount Type:</td>
        	<td><?php echo discount($price['SaleQuotationItem']['discount_type']); ?></td>
        </tr>
        <tr>
        	<td>Tax:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['tax'], 2); ?></td>
        </tr>
        <tr>
        	<td>Sales Margin:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['sale_margin'], 2); ?>% (<?php echo number_format($price['SaleQuotationItem']['sale_margin_value'], 2); ?>)</td>
        </tr>
        <tr>
        	<td><h4>Selling Price:</h4></td>
        	<td><h4><?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($price['SaleQuotationItem']['sale_price'], 2); ?></h4></td>
        </tr>
        </table>

        <?php } else { ?>

        <?php echo $this->Form->create('SaleQuotationItem', array('class'=>'form-horizontal', 'id' => 'saleMargin')); ?> 
        <?php echo $this->Form->input('id'); ?>
        <div class="form-group">
        	<label class="col-sm-2">Planning Price</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('planning_price', array('id' => 'total_value', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>  
	        <div class="form-group">
	        	<label class="col-sm-2">Sale Margin (%)</label>
	        	<div class="col-sm-5">
	        		<?php echo $this->Form->input('sale_margin', array('type' => 'text', 'id' => 'margin', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        	<div class="col-sm-5">
	        		<?php echo $this->Form->input('sale_margin_value', array('type' => 'text', 'id' => 'sale_margin_value', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div> 
	        <div class="form-group">
	        	<label class="col-sm-2">Subtotal</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('subtotal', array('type' => 'text', 'id' => 'subtotal', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>     
	        <div class="form-group">
	        	<label class="col-sm-2">Discount</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('discount', array('type' => 'text', 'id' => 'discount', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>
	        <div class="form-group">
	        	<label class="col-sm-2">Discount Type</label>
	        	<div class="col-sm-10">
	        	<?php $type = array(0 => '-Select Discount Type-', 1 => 'Percentage (%)', 2 => 'Fix Amount'); ?>
	        		<?php echo $this->Form->input('discount_type', array('options' => $type, 'id' => 'discount_type', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>
	        <div class="form-group">
	        	<label class="col-sm-2">Tax (%)</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('tax', array('type' => 'text', 'id' => 'tax', 'value' => Configure::read('Site.gst_amount'), 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div> 
	        <div class="form-group">
	        	<label class="col-sm-2">Total</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('sale_price', array('type' => 'text', 'id' => 'total', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>  

	        <div class="form-group">  
	        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>  
	        </div>	  
		
		<?php $this->Form->end(); ?>

		<?php } ?>

        	<div class="related table-responsive">
				<h4>Item Cost</h4>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<td>Code</td>
							<td>Name</td>
							<td>Rate</td> 
							<td>Currency</td>
							<td class="align-right">Price/Qty</td>
							<td class="align-right">Qty</td>  
							<td class="align-right">Qty Total</td>   
							<td class="align-right">Subtotal</td> 
							<td><p class="pull-right">Total*Rate</p></td>
						</tr>
					</thead> 
					<?php $total = 0; ?>
					<?php foreach ($items as $item): ?>

					<?php
					$subtotal = $item['price_total'] * $item['rate'];
					?>

					<tbody>
						<tr>
							<td><?php echo $item['code']; ?></td>
							<td><?php echo $item['name']; ?></td>
							<td><?php echo $item['rate']; ?></td> 
							<td><?php echo $item['currency']; ?></td>
							<td class="align-right"><?php echo number_format($item['unit_price'], 2); ?></td>
								<td class="align-right"><?php echo number_format($item['quantity'], 2); ?></td>  
								<td class="align-right"><?php echo number_format($item['quantity_total'], 2); ?> <small><?php echo $item['unit_name']; ?></small>
								</td>   
								<td class="align-right"><?php echo number_format($item['price_total'], 2); ?></td> 
								<td class="align-right"><p class="pull-right"><b><?php echo number_format($subtotal, 2); ?></b></p></td> 
						</tr>
					</tbody>
					<?php $total += $subtotal; ?>
					<?php endforeach; ?>

					<tbody>
						<tr>
							<td class="text-center">Total</td>
							<td></td>
							<td></td> 
							<td></td>
							<td></td>  
							<td></td>  
							<td></td> 
							<td></td> 
							<td class="align-right"><b><?php echo number_format($total, 2); ?></b></td> 
						</tr>
					</tbody>

				</table>

			<h4>Indirect Cost</h4>
					<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
						<thead>
							<tr> 
								<td>Item Code/Name</td>
								<td>Qty</td> 
								<td>Margin (%)</td>
								<td>Price / Unt</td>   
								<td>Total</td>
							</tr>
						</thead> 
	                <tbody>
	                <?php 
	                $total_cost = 0;
	                foreach($costs as $cost) { ?> 
	                <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
	                  <tr>
	                    <td><?php echo $cost['InventoryItem']['code']; ?><br/>
	                    <small><?php echo $cost['InventoryItem']['name']; ?></small>
	                    </td>
	                    <td><?php echo $cost['SaleQuotationItemCost']['quantity']; ?> <?php echo $cost['GeneralUnit']['name']; ?></td>
	                    <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 4); ?></td>  
	                    <td class="align-right"><?php echo number_format($cost['SaleQuotationItemCost']['price'], 2); ?></td> 
	                    <td class="align-right"><?php echo number_format($cost['SaleQuotationItemCost']['total_price'], 2); ?></td> 
	                  </tr>
	                  <?php } ?>
	                  <tr>
	                    <td><p><b>Total</b></p></td>
	                    <td> </td>
	                    <td> </td> 
	                    <td> </td> 
	                    <td class="align-right"><b><?php echo number_format($total_cost, 2); ?></b></td> 
	                  </tr>
	                </tbody> 
	              </table>
				<?php $total_value = $total_cost + $total; ?>
	              <input type="hidden" id="indirectCost" value="<?php echo $total_cost; ?>">  
	              <h4>Soft Cost</h4>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr> 
							<td>Item Code</td>  
							<td>Item Name</td> 
							<td>Margin (%)</td>    
							<td>Total</td>   
						</tr>
					</thead> 
                <tbody>
                <?php 
                $total_soft_cost = 0;
                foreach($soft_costs as $cost) { ?> 
                 <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
                 <?php $percent = $total_value / 100 * $cost['SaleQuotationItemCost']['margin']; ?>
                 <?php $total_soft_cost += $percent; ?>
	                  <tr>
	                    <td><?php echo $cost['SaleQuotationItemCost']['name']; ?></td>
	                    <td><?php echo $cost['InventoryItem']['name']; ?></td> 
	                    <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 2); ?></td> 
	                    <td class="align-right"><?php echo number_format($percent, 2); ?></td>   
	                  </tr>
	                  <?php } ?>
                  <tr>
                    <td><p><b>Total</b></p></td>
                    <td> </td>
                    <td> </td>   
                    <td class="align-right"><b><?php echo number_format($total_soft_cost, 2); ?></b></td> 
                  </tr>
                </tbody> 
              </table>
 
	              <?php $grand_total = $total_soft_cost + $total_value; ?>
	              <input type="hidden" id="totalValue" value="<?php echo $price['SaleQuotationItem']['planning_price']; ?>">
	              <h2>Grand Total Cost: <?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($grand_total, 2); ?></h2>

	   		
   		


   		</div>

 

    </div>
</div>
</div>
</div>
<?php } else { ?> 
	<div class="row"> 
	  	<div class="col-xs-12"> 
	    	<div class="x_panel tile">
	      		<div class="x_title">
	        		<h2><?php echo $item['name']; ?> - <?php echo $item['code']; ?></h2> 
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content"> 
	        	<?php echo $this->Session->flash(); ?> 


<?php if(isset($_GET['ref'])) { ?>
	<?php echo $this->Form->create('SaleQuotationRemark', array('class' => 'form-horizontal')); ?> 

	<div class="form-group">
	<label class="col-sm-3">Remark</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Remark / note...', 'required' => false)); ?> 
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3">Status</label>
	<div class="col-sm-9">
	<?php $status = array(6 => 'Approve', 7 => 'Reject'); ?>
	<?php echo $this->Form->input('status', array('options' => $status, 'empty' => '-Select Status-', 'class' => 'form-control', 'label' => false)); ?> 
	</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3"></label>
	<div class="col-sm-9">
	 <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?>
	</div>
	</div>

	<?php $this->end(); ?>
<?php } ?>

<?php if($remarks) { ?>
<h4>Remarks</h4>
<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
	<thead>
		<tr> 
			<td>Remark</td>
			<td>PIC</td> 
			<td>Created</td>  
			<td>Status</td> 
		</tr>
	</thead> 
<tbody>
<?php  
	foreach($remarks as $remark) { ?>  
  <tr>
    <td><?php echo $remark['SaleQuotationRemark']['remark']; ?></td>
    <td><?php echo $remark['User']['username']; ?></td> 
    <td><?php echo $remark['SaleQuotationRemark']['created']; ?></td> 
    <td><?php echo status($remark['SaleQuotationRemark']['status']); ?></td>  
  </tr>
  <?php } ?>
   
</tbody> 
</table>
<?php } ?>


        <?php if(isset($_GET['ref'])) { ?>

        <table class="table table-bordered">
        <tr>
        	<td colspan="2"><h4>Planning Department</h4></td> 
        </tr>
        <tr>
        	<td>Material Cost:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['material_cost'], 2); ?></td>
        </tr>
        <tr>
        	<td>Indirect Cost:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['indirect_cost'], 2); ?></td>
        </tr>
        <tr>
        	<td>Soft Cost:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['soft_cost'], 2); ?></td>
        </tr>
        <tr>
        	<td>Planning Margin:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['planning_margin'], 2); ?>% (<?php echo number_format($price['SaleQuotationItem']['margin_value'], 2); ?>)</td>
        </tr>
        <tr>
        	<td>Planning Total:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['planning_price'], 2); ?></td>
        </tr>
        <tr>
        	<td colspan="2"><h4>Sales Department</h4></td> 
        </tr> 
        <tr>
        	<td>Discount:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['discount'], 2); ?></td>
        </tr>
        <tr>
        	<td>Discount Type:</td>
        	<td><?php echo discount($price['SaleQuotationItem']['discount_type']); ?></td>
        </tr>
        <tr>
        	<td>Tax:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['tax'], 2); ?></td>
        </tr>
        <tr>
        	<td>Sales Margin:</td>
        	<td><?php echo number_format($price['SaleQuotationItem']['sale_margin'], 2); ?>% (<?php echo number_format($price['SaleQuotationItem']['sale_margin_value'], 2); ?>)</td>
        </tr>
        <tr>
        	<td><h4>Selling Price:</h4></td>
        	<td><h4><?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($price['SaleQuotationItem']['sale_price'], 2); ?></h4></td>
        </tr>
        </table>

        <?php } else { ?>
	<?php echo $this->Form->create('SaleQuotationItem', array('class'=>'form-horizontal', 'id' => 'saleMargin')); ?> 
        <?php echo $this->Form->input('id'); ?>
        <div class="form-group">
        	<label class="col-sm-2">Planning Price</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('planning_price', array('id' => 'total_value', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>  
	        <div class="form-group">
	        	<label class="col-sm-2">Sale Margin (%)</label>
	        	<div class="col-sm-5">
	        		<?php echo $this->Form->input('sale_margin', array('type' => 'text', 'id' => 'margin', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        	<div class="col-sm-5">
	        		<?php echo $this->Form->input('sale_margin_value', array('type' => 'text', 'id' => 'sale_margin_value', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div> 
	        <div class="form-group">
	        	<label class="col-sm-2">Subtotal</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('subtotal', array('type' => 'text', 'id' => 'subtotal', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>     
	        <div class="form-group">
	        	<label class="col-sm-2">Discount</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('discount', array('type' => 'text', 'id' => 'discount', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>
	        <div class="form-group">
	        	<label class="col-sm-2">Discount Type</label>
	        	<div class="col-sm-10">
	        	<?php $type = array(0 => '-Select Discount Type-', 1 => 'Percentage (%)', 2 => 'Fix Amount'); ?>
	        		<?php echo $this->Form->input('discount_type', array('options' => $type, 'id' => 'discount_type', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>
	        <div class="form-group">
	        	<label class="col-sm-2">Tax (%)</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('tax', array('type' => 'text', 'id' => 'tax', 'value' => Configure::read('Site.gst_amount'), 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div> 
	        <div class="form-group">
	        	<label class="col-sm-2">Total</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('sale_price', array('type' => 'text', 'id' => 'total', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>  

	        <div class="form-group">  
	        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>  
	        </div>	  
		
		<?php $this->Form->end(); ?>
<?php } ?>
	        	<div class="related table-responsive">
					<h4>Item Currency</h4>
					<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
						<thead>
							<tr>
								<td>Code</td>
								<td>Name</td>
								<td>Rate</td> 
								<td>Currency</td>
								<td class="align-right">Price/Qty</td> 
								<td class="align-right">Qty</td>  
								<td class="align-right">Qty Total</td>   
								<td class="align-right">Subtotal</td> 
								<td class="align-right">Total*Rate</td>
							</tr>
						</thead> 
						<?php $total = 0; ?> 

						<?php
						$subtotal = $item['price_total'] * $item['rate'];
						?>

						<tbody>
							<tr>
								<td><?php echo $item['code']; ?></td>
								<td><?php echo $item['name']; ?></td>
								<td><?php echo $item['rate']; ?></td> 
								<td><?php echo $item['currency']; ?></td>
								<td class="align-right"><?php echo number_format($item['unit_price'], 2); ?></td>
								<td class="align-right"><?php echo number_format($item['quantity'], 2); ?></td>  
								<td class="align-right"><?php echo number_format($item['quantity_total'], 2); ?></td>   
								<td class="align-right"><?php echo number_format($item['price_total'], 2); ?></td> 
								<td class="align-right"><b><?php echo number_format($subtotal, 2); ?></b></td> 
							</tr>
						</tbody>
						<?php $total = $subtotal; ?> 

						<tbody>
							<tr>
								<td class="text-center">Total</td>
								<td></td>
								<td></td> 
								<td></td>
								<td></td>  
								<td></td>  
								<td></td> 
								<td></td> 
								<td class="align-right"><b><?php echo number_format($total, 2); ?></b></td> 
							</tr>
						</tbody>

					</table>

				<h4>Indirect Cost</h4>
					<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
						<thead>
							<tr> 
								<td>Item Code/Name</td>
								<td>Qty</td> 
								<td>Margin (%)</td>
								<td>Price / Unt</td>   
								<td>Total</td>
							</tr>
						</thead> 
	                <tbody>
	                <?php 
	                $total_cost = 0;
	                foreach($costs as $cost) { ?> 
	                <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
	                  <tr>
	                    <td><?php echo $cost['InventoryItem']['code']; ?><br/>
	                    <small><?php echo $cost['InventoryItem']['name']; ?></small>
	                    </td>
	                    <td><?php echo $cost['SaleQuotationItemCost']['quantity']; ?> <?php echo $cost['GeneralUnit']['name']; ?></td>
	                    <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 4); ?></td>  
	                    <td class="align-right"><?php echo number_format($cost['SaleQuotationItemCost']['price'], 2); ?></td> 
	                    <td class="align-right"><?php echo number_format($cost['SaleQuotationItemCost']['total_price'], 2); ?></td> 
	                  </tr>
	                  <?php } ?>
	                  <tr>
	                    <td><p><b>Total</b></p></td>
	                    <td> </td>
	                    <td> </td> 
	                    <td> </td> 
	                    <td class="align-right"><b><?php echo number_format($total_cost, 2); ?></b></td> 
	                  </tr>
	                </tbody> 
	              </table>
				<?php $total_value = $total_cost + $total; ?>
	              <input type="hidden" id="indirectCost" value="<?php echo $total_cost; ?>">  
	              <h4>Soft Cost</h4>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr> 
							<td>Item Code</td>  
							<td>Item Name</td> 
							<td>Margin (%)</td>    
							<td>Total</td>   
						</tr>
					</thead> 
                <tbody>
                <?php 
                $total_soft_cost = 0;
                foreach($soft_costs as $cost) { ?> 
                 <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
                 <?php $percent = $total_value / 100 * $cost['SaleQuotationItemCost']['margin']; ?>
                 <?php $total_soft_cost += $percent; ?>
	                  <tr>
	                    <td><?php echo $cost['SaleQuotationItemCost']['name']; ?></td>
	                    <td><?php echo $cost['InventoryItem']['name']; ?></td> 
	                    <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 2); ?></td> 
	                    <td class="align-right"><?php echo number_format($percent, 2); ?></td>   
	                  </tr>
	                  <?php } ?>
                  <tr>
                    <td><p><b>Total</b></p></td>
                    <td> </td>
                    <td> </td>   
                    <td class="align-right"><b><?php echo number_format($total_soft_cost, 2); ?></b></td> 
                  </tr>
                </tbody> 
              </table>
 
	              <?php $grand_total = $total_soft_cost + $total_value; ?>
	              <input type="hidden" id="totalValue" value="<?php echo $price['SaleQuotationItem']['planning_price']; ?>">
	              <h2>Grand Total Cost: <?php echo Configure::read('Site.default_currency'); ?> <?php echo number_format($grand_total, 2); ?></h2>

	   		

	   		</div>
	    </div>
	</div>
</div>
<?php } ?>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).ready(function() {
	var costing = Number.parseFloat($('#totalValue').val());
	$('#total_value').val(costing);
	var margin = Number.parseFloat($('#margin').val());
	var percent = (costing / 100) * margin;
	var total = costing + Number.parseFloat(percent); 
	var tax = $('#tax').val();

	$('#subtotal').val(total);
	/*
	var aftertax = 0;
	if(tax > 0) {
		aftertax = ((total / 100) * tax) + total;
	} else {
		aftertax = total;
	}
	$('#total').val(aftertax);
	*/

	$('#saleMargin').on('keyup change', 'input, select', function() { 
	    var tax = Number.parseFloat($('#tax').val());
	    var subtotal = Number.parseFloat($('#subtotal').val());
	    var margin = Number.parseFloat($('#margin').val());
	    
	    var discount = Number.parseFloat($('#discount').val());
	    var discount_type = Number.parseFloat($('#discount_type').val());
	    var total_value = Number.parseFloat($('#total_value').val());
	    // Calculate
	    var sale_margin_value = Number.parseFloat($('#sale_margin_value').val());

	    var total = total_value;
	    var total_margin = total + addmargin(total_value, margin);
	    $('#subtotal').val(Number.parseFloat(total_margin).toFixed(2));

	    var display_margin = total_value / 100 * margin;

	    $('#sale_margin_value').val(display_margin.toFixed(2));

	    var total_discount = total_margin - adddiscount(total_margin, discount, discount_type);

	    var total_tax = total_discount + addtax(total_discount, tax);

	    $('#total').val(total_tax.toFixed(2));
	});
 	
});

function addmargin(cost, margin) {
	var total = 0;
	if(margin > 0) {
		total = (cost / 100) * margin;
	} else {
		total = 0;
	}
	console.log(total)
	return total;
}

function addtax(subtotal, tax) {
	var total = 0;
	if(tax > 0) {
		total = (subtotal / 100) * tax;
	} else {
		total = 0;
	}
	console.log(total)
	return total;
}

function adddiscount(subtotal, discount, type) {
	var total = 0;
	if(type == 1) {
		// %
		total = (subtotal / 100) * discount;
	}
	if(type == 2) {
		// Fixed
		total = discount;
	}

	console.log(total)
	return total;
}

/*




	$('#margin').on('keyup', function() {
		var margin = Number.parseFloat($('#margin').val());
		var percent = (costing / 100) * margin;
		var total = costing + Number.parseFloat(percent);
		$('#subtotal').val(total);
		if(tax > 0) {
			aftertax = ((total / 100) * tax) + total;
		} else {
			aftertax = total;
		}
		$('#total').val(aftertax);
	});

	$('#discount').on('keyup', function() {
		var margin = Number.parseFloat($('#discount').val());
		var percent = (costing / 100) * margin;
		var total = costing + Number.parseFloat(percent); 
		$('#total').val(total);
	});
	var afterdiscount = 0;
	$('#discount_type').change(function() {
		var type = $('#discount_type').val();
		var discount = Number.parseFloat($('#discount').val());
		if(type == 1) {
			// %
			afterdiscount = aftertax - ((aftertax / 100) * discount);
		}
		if(type == 2) {
			// Fixed
			afterdiscount = aftertax - discount;
		}
		aftertax = afterdiscount;
		$('#total').val(aftertax);
	});

	$('#tax').on('keyup', function() {
		var tax = Number.parseFloat($('#tax').val());
		var subtotal = $('#subtotal').val();
		var percent = (subtotal / 100) * tax;
		var total = subtotal + Number.parseFloat(percent); 
		$('#total').val(total);
	});
	*/

</script>
<?php $this->end(); ?>