<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Create New Costing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('SaleQuotation', array('class'=>'form-horizontal')); ?> 
        <div class="form-group">
            <label class="col-sm-3">Expiry Date *</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('date', array('id' => 'dateonly', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
                <?php echo $this->Form->input('sale_tender_id', array('id' => 'tenderId', 'type' => 'hidden', 'value' => '0')); ?>
                <?php echo $this->Form->input('customer_id', array('type' => 'hidden', 'label' => false, 'value' => '0')); ?>
            </div>
        </div>    

        <div class="form-group">
            <label class="col-sm-3">Remark (Optional)</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3">Status *</label>
            <div class="col-sm-9">
                <?php $status = array(0 => 'Draft', 1 => 'Active'); ?> 
                <?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
                <?php echo $this->Form->input('hod_status', array('type' => 'hidden', 'value' => 0)); ?>
                <?php echo $this->Form->input('gm_status', array('type' => 'hidden', 'value' => 0)); ?> 
            </div>
        </div>  
        
        <div class="form-group"> 
            <label class="col-sm-3">Item *</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('SaleQuotationItem.name', array('id' => 'findProduct', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Item name/code')); ?>
                <?php echo $this->Form->input('SaleQuotationItem.inventory_item_id', array('id' => 'productId', 'type' => 'hidden', 'label' => false)); ?>
                <?php echo $this->Form->input('SaleQuotationItem.bom_id', array('type' => 'hidden', 'id' => 'bom_id')); ?>
            </div> 
        </div>
        <div class="form-group"> 
            <label class="col-sm-3">BOM Name *</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('Bom.name', array('id' => 'bom_name', 'class' => 'form-control', 'label' => false, 'placeholder' => 'BOM Name', 'readonly' => true)); ?>
                
            </div> 
        </div>
        
 

        <div class="form-group"> 
                <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>
             
        </div>   
        
        <?php $this->Form->end(); ?>
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
 
  

$(document).ready(function() {
    $('#findProduct').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct').val() + "&costing=2",                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            price: item.price,
                            bom_id: item.bom.id,
                            bom_name: item.bom.name,
                            bom_code: item.bom.code
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#productId').val( ui.item.id ); 
            $('#price').val( ui.item.price ); 
            $('#total').val( ui.item.price ); 
            $('#bom_id').val( ui.item.bom_id ); 
            $('#bom_name').val( ui.item.bom_name );
            $('#quantity').val(1); 
        },
        minLength: 3 
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>BOM: " + item.bom_code + "</small><br>" +  "</div>" ).appendTo( ul );
    };
});
</script>
<?php $this->end(); ?>