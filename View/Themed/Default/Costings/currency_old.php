<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Costing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>        

        <div class="form-group">
            <h4 class="col-sm-12">Exchange Rate <b><?php echo Configure::read('Site.default_currency'); ?>1</b> agains X</h4> 
        </div>  
        <?php echo $this->Form->create('SaleQuotationCurrency', array('class'=>'form-horizontal')); ?> 
        <table class="table">

        <?php if($rates) { ?>
            <?php foreach($rates as $currency) { ?>  
                <tr>
                    <td><?php echo $currency['GeneralCurrency']['name']; ?></td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['symbol']; ?>
                    </td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['iso_code']; ?>
                    </td>
                    <td>
                        <?php echo $this->Form->input('general_currency_id.', array('value' => $currency['GeneralCurrency']['id'], 'type' => 'hidden', 'label' => false)); ?>
                        <?php echo $this->Form->input('rate.', array('value' => $currency['SaleQuotationCurrency']['rate'], 'class' => 'form-control', 'label' => false)); ?>
 
                    </td> 
                </tr>  
            <?php } ?>

        <?php } else { ?>
            <?php foreach($currencies as $currency) { ?>  
                <tr>
                    <td><?php echo $currency['GeneralCurrency']['name']; ?></td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['symbol']; ?>
                    </td>
                    <td>
                        <?php echo $currency['GeneralCurrency']['iso_code']; ?>
                    </td>
                    <td>
                        <?php echo $this->Form->input('general_currency_id.', array('value' => $currency['GeneralCurrency']['id'], 'type' => 'hidden', 'label' => false)); ?>
                        <?php echo $this->Form->input('rate.', array('class' => 'form-control', 'label' => false)); ?>
 
                    </td> 
                </tr>  
            <?php } ?>

        <?php } ?>
        </table>

        <?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success')); ?>
             
        <?php $this->Form->end(); ?>

        </div>
        </div>
    </div>
</div>

