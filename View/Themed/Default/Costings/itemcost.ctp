<?php echo $this->Html->link('Back To Quotation', array('action' => 'view', $detail['SaleQuotation']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>

<?php echo $this->Html->link('Edit BOM Costing', array('action' => 'editbom', $detail['SaleQuotation']['id']), array('class' => 'btn btn-danger', 'escape' => false)); ?>

<?php echo $this->Html->link('Notify Procurement To Add Price Book', array('action' => 'viewitem', $id, '?price_books'), array('class' => 'btn btn-warning', 'escape' => false)); ?>
 
<?php if($is_bom) { ?> 
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo $bom['SaleBom']['name']; ?> - Qty: <?php echo $bom['SaleBom']['no_of_order']; ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 
        	<div id="safeOutput">df</div>
        <?php echo $this->Form->create('SaleQuotationItem', array('class'=>'form-horizontal')); ?> 
 
  
        	<div class="related table-responsive">
				<h4>Materials</h4>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<td>#</td>
							<td>Code/Name</td>
							<td>Supplier</td>  
							<td>Qty</td>  
							<td>Price/Unt</td>   
							<td>Subtotal</td>
							<td>Cur. Rate (%)</td>  
							<td>Subtotal (RM)</td> 
							<td>Buffer (%)</td>
							<td>Total (RM)</td>
						</tr>
					</thead> 
					<?php 
					$total = 0; 
					$i = 1;
					?>
					<?php foreach ($items as $item): ?>

					<?php
					$subtotal = $item['price_total'] * $item['rate'];
					?>

					<tbody>
						<tr>
						<td><?php echo $i; ?></td>
						<!-- Field-->
						<?php if($item['type'] == 'S.A') { ?>
							<input type="hidden" name="type[]" value="1">
						<?php } else { ?>
							<input type="hidden" name="type[]" value="0">
						<?php } ?>
						
						<!-- Field-->
							<td><?php echo $item['code']; ?><br/><small><?php echo $item['name']; ?></small></td>
							<td><?php echo $item['supplier']; ?></td>  
							<td><?php echo _n2($item['quantity']); ?> <small><?php echo $item['unit']; ?></small></td> 
							<td class="align-right"><?php echo _n2($item['unit_price']); ?></td>   
							<td class="align-right"><?php echo $item['currency']; ?> <?php echo _n2($item['price_total']); ?></td> 
							<td><?php echo $item['rate']; ?></td> 
							<td class="align-right">RM <?php echo _n2($subtotal); ?></td>  
							
							<td><input style="width:30px;" type="text" name="buffer[]" value="<?php echo $item['buffer']; ?>" class="buffer" id="<?php echo $item['id']; ?>"></td>

							<?php if($item['total_price'] == 0) {
								$total_price = $subtotal;
							} else {
								$total_price = $item['total_price'];
							} ?>
							<td><input style="width:90px;" type="text" name="total[]" class="total_rm" value="<?php echo _n2n($total_price); ?>" id="total-<?php echo $item['id']; ?>"readonly> </td>

							<input type="hidden" name="total_rm[]" value="<?php echo $subtotal; ?>" id="total_rm-<?php echo $item['id']; ?>">

							<input type="hidden" name="id[]" value="<?php echo $item['id']; ?>">	
							<input type="hidden" name="inventory_supplier_item_id[]" value="<?php echo $item['inventory_supplier_item_id']; ?>">
							<input type="hidden" name="price[]" value="<?php echo $item['unit_price']; ?>">
							
 
						</tr>
					</tbody>
					<?php 
					$total += $subtotal; 
					$i++;
					?>
					<?php endforeach; ?>

					<tbody>
						<tr>
							<td colspan="9" class="align-right">Total <?php echo Configure::read('Site.default_currency'); ?> </td> 
							<td class="align-right"><b id="totalBottom"><?php echo number_format($total, 2); ?></b></td>  
						</tr>
					</tbody>
					<input type="hidden" id="totalMaterial" value="<?php echo $total; ?>">
				</table>
				<div class="form-group"> 
		        	<?php 
		        	echo $this->Form->submit('Save Price', array('div' => false, 'class' => 'btn btn-success pull-right')); 
		        	?> 
		        </div>	  
				<?php $this->Form->end(); ?> 
   		</div>
    </div>
</div>
</div>
<?php } else { ?> 
	<div class="row"> 
	  	<div class="col-xs-12"> 
	    	<div class="x_panel tile">
	      		<div class="x_title">
	        		<h2><?php echo $item['name']; ?> - <?php echo $item['code']; ?></h2> 
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content"> 
	        	<?php echo $this->Session->flash(); ?>  

	        	<div class="related table-responsive">
					<h3>Material Cost</h3>
					<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
						<thead>
							<tr> 
								<td>Item Name</td>
								<td>Type</td>
								<td>Item Type</td>
								<td>Cur. Rate (%)</td> 
								<td>Currency</td>
								<td>Supplier</td>
								<td>Qty</td>  
								<td>UOM</td>   
								<td class="align-right">Price/Qty</td> 
								<td class="align-right">Subtotal</td> 
								<td class="align-right">Total (RM)</td> 
							</tr>
						</thead> 
						<?php 
						$total = 0; 
						$i = 1;
						?> 

						<?php
						$subtotal = $item['price_total'] * $item['rate'];
						?>

						<tbody>
						<tr> 
							<td><?php echo $item['code']; ?><br/><small><?php echo $item['name']; ?></small></td>
							<td><?php echo $item['type']; ?></td>
							<td><?php echo $item['item_type']; ?></td>
							<td><?php echo $item['rate']; ?></td> 
							<td><?php echo $item['currency']; ?></td>
							<td><?php echo $item['supplier']; ?></td>
							<td><?php echo _n2($item['quantity']); ?></td>   
							<td><?php echo $item['unit']; ?></td>  
							<td class="align-right"><?php echo _n2($item['unit_price']); ?></td> 
							<td class="align-right"><?php echo _n2($item['price_total']); ?></td> 
							<td class="align-right"><b><?php echo _n2($subtotal); ?><b></td>  
					</tbody>
						<?php 
						$total = $subtotal; 
						$i++;
						?> 

						<tbody>
							<tr>
								<td>Total</td>
								<td></td>
								<td></td> 
								<td></td>
								<td></td>
								<td></td>  
								<td></td>  
								<td></td> 
								<td></td>  
								<td class="align-right"><b><?php echo number_format($total, 2); ?></b></td> 
							</tr>
						</tbody>

					</table>
					 
	   		</div>
	    </div>
	</div>
</div>
<?php } ?>

<?php $this->start('script'); ?>
<script type="text/javascript">  

$(document).ready(function() {
	$('<div>').text("<script alert='fasdf'><script>").html('#safeOutput');
	$('.buffer').each(function() {
		$(this).on('keyup', function() {
			var buffer = Number.parseFloat($(this).val());
			var attrId = $(this).attr('id');
			var totalRm = Number.parseFloat($('#total_rm-'+attrId).val());
			var totalAmount = 0;
			if(buffer > 0) {
				totalAmount = ((totalRm / 100) * buffer) + totalRm;
				$('#total-'+attrId).val(totalAmount.toFixed(2));
			} else {
				$('#total-'+attrId).val(totalRm.toFixed(2));
			}
		}); 
	});

	

	var totalMaterial = Number.parseFloat($('#totalMaterial').val());
	var indirectCost = Number.parseFloat($('#indirectCost').val());
	var totalSoftCost = Number.parseFloat($('#totalSoftCost').val());

	var costing = totalMaterial + indirectCost + totalSoftCost;
	$('#total_value').val(Number.parseFloat(costing).toFixed(2));
	<?php if($is_bom) { ?>
		<?php if($bom['SaleQuotationItem']['planning_price'] > 0) { ?>
			$('#total').val(<?php echo $bom['SaleQuotationItem']['planning_price']; ?>);
		<?php } else { ?>
			$('#total').val(costing.toFixed(2));
		<?php } ?> 
	<?php } else { ?>
		<?php if($item['planning_price'] > 0) { ?>
			$('#total').val(<?php echo $item['planning_price']; ?>);
		<?php } else { ?>
			$('#total').val(costing.toFixed(2));
		<?php } ?> 
	<?php } ?>

	 $('#material_cost').val(totalMaterial.toFixed(2)); 
	 $('#indirect_cost').val(indirectCost.toFixed(2));
	 $('#soft_cost').val(totalSoftCost.toFixed(2));

	$('#margin').on('keyup', function() {
		var margin = Number.parseFloat($('#margin').val());
		var percent = (costing / 100) * margin;
		var total = costing + Number.parseFloat(percent);
		$('#margin_value').val(percent.toFixed(2));
		$('#total').val(total.toFixed(2));
	});
});

$(document).on("keyup", ".buffer", function() {
    var sum = 0;
    $(".total_rm").each(function(){
        sum += +$(this).val();
    });
    $("#totalMaterial").val(sum.toFixed(2)); 
    $("#totalBottom").html(sum.toFixed(2));
});
</script>


<?php $this->end(); ?>