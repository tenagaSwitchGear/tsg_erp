
<?php echo $this->Html->link('Waiting Approval', array('action' => 'approval', 3), array('class' => 'btn btn-warning')); ?>
<?php echo $this->Html->link('Approved', array('action' => 'approval', 4), array('class' => 'btn btn-success')); ?>
<?php echo $this->Html->link('Rejected', array('action' => 'approval', 7), array('class' => 'btn btn-danger')); ?> 
<?php 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Costing';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting Approval';
  } elseif($status == 4) {
    $data = 'Approved';
  } elseif($status == 7) {
    $data = 'Rejected';
  } else {
  	$data = 'Draft';
  }  
  return $data;
}
?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Costing List</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('SaleQuotation', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Quotation No', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('customer', array('placeholder' => 'Customer', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>
		<?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden')); ?>  
		</td>
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
<div class="table-responsive"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
	<thead>
	<tr> 
		<th><?php echo $this->Paginator->sort('InventoryItem.name', 'Item Name'); ?></th>
		<th><?php echo $this->Paginator->sort('InventoryItem.code', 'Item code'); ?></th>
		<th><?php echo $this->Paginator->sort('Bom.code', 'BOM'); ?></th> 
		<th><?php echo $this->Paginator->sort('SaleQuotation.planning_price', 'Cost'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('date', 'Expiry'); ?></th>
		<th><?php echo $this->Paginator->sort('user_id'); ?></th> 
		<th><?php echo $this->Paginator->sort('status'); ?></th>   
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleQuotations as $saleQuotation): ?>
	<tr> 
		<td>
			<?php echo h($saleQuotation['InventoryItem']['name']); ?>
		</td> 
		<td>
			<?php echo h($saleQuotation['InventoryItem']['code']); ?>
		</td> 
		<td>
			<?php echo h($saleQuotation['Bom']['code']); ?>
		</td> 
		<td>
			<?php echo _n2($saleQuotation['SaleQuotation']['planning_price']); ?>
		</td> 
		<td>
			<?php echo h($saleQuotation['SaleQuotation']['created']); ?>
		</td> 
		<td>
			<?php echo h($saleQuotation['SaleQuotation']['date']); ?>
		</td> 
		<td>
			<?php echo $this->Html->link($saleQuotation['User']['username'], array('controller' => 'users', 'action' => 'view', $saleQuotation['User']['id'])); ?>
		</td> 
		<td><?php echo status($saleQuotation['SaleQuotation']['status']); ?></td>  
		<td class="actions"> 
			<?php echo $this->Html->link(__('View'), array('action' => 'verifyview', $saleQuotation['SaleQuotation']['id'])); ?> 
			 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
	<?php
	  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
	  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	?>
	</ul>
</div>
 
  </div>
    </div>
  </div> 
</div>