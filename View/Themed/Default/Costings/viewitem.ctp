<?php echo $this->Html->link('Back', array('controller' => 'costings', 'action' => 'view', $this->request->data['SaleQuotation']['id']), array('class' => 'btn btn-primary')); ?>

<?php echo $this->Html->link('Print', array('action' => 'viewitem', $this->request->data['SaleQuotationItem']['id'].'?print=N'), array('class' => 'btn btn-success', 'onclick' => 'printIframe(report);')); ?>

<?php if(isset($_GET['print'])) { ?>
 <iframe style="display: none;" name="report" id="report" src="<?php echo BASE_URL; ?>costings/printcosting/<?php echo $this->request->data['SaleQuotationItem']['id']; ?>"></iframe>
<?php } ?>

<?php if($is_bom) { ?> 
<div class="row"> 
    <div class="col-xs-12"> 
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo $bom['SaleBom']['name']; ?></h2> 
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?> 

    <div class="box-blue">
    <h3>Margin & Total Price</h3>
    <p>Note: Make sure you have added Currency, Soft Cost & Indirect Cost before Submitting this form. Every changes on Currency, Item Cost, Soft or Indirect Cost you have to Update this form.</p>
        <?php echo $this->Form->create('SaleQuotationItem', array('class'=>'form-horizontal')); ?> 
        <?php echo $this->Form->input('id'); ?>

        <div class="form-group">
            <label class="col-sm-2">Material Cost</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('material_cost', array('type' => 'text', 'id' => 'material_cost', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2">Indirect Cost</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('indirect_cost', array('type' => 'text', 'id' => 'indirect_cost', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2">Soft Cost</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('soft_cost', array('type' => 'text', 'id' => 'soft_cost', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2">Subtotal</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('total_cost', array('type' => 'text', 'id' => 'total_value', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-2">Margin (%)</label>
            <div class="col-sm-5">
                <?php echo $this->Form->input('planning_margin', array('type' => 'text', 'id' => 'margin', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
            <div class="col-sm-5">
                <?php echo $this->Form->input('margin_value', array('type' => 'text', 'id' => 'margin_value', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-2">Total</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('planning_price', array('type' => 'text', 'id' => 'total', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div>   

        <div class="form-group"> 
 
        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
        </div>	 
		
		<?php $this->Form->end(); ?>
	</div>
        	<div class="related table-responsive">
				<h4>Item List</h4>
				<p>Note: Cur: Currency.</p>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<td>#</td>
							<td>Item</td>  
                            <td>Qty</td>
                            <td>Price/Unt</td> 
							<td>Cur. Rate (%)</td>  
							<td>Subtotal</td> 
                            <td>Buffer</td>
							<td class="align-right">Total(MYR)</td> 
						</tr>
					</thead> 
					<?php 
					$total = 0; 
					$i = 1;
					?>
					<?php foreach ($items as $item): ?> 

                    <?php
                    $subtotal = $item['price_total'] * $item['rate'];
                    ?>
					<tbody>
						<tr>
						<td><?php echo $i; ?></td>
							<td><?php echo $item['code']; ?><br/><small><?php echo $item['name']; ?></small></td> 
					        <td><?php echo _n2($item['quantity_total']); ?> <?php echo $item['unit']; ?></td>  
                            <td><?php echo _n2($item['unit_price']); ?></td> 
							<td><?php echo $item['rate']; ?></td>   
							
							<td><?php echo $item['currency']; ?> <?php echo number_format($item['price_total'], 2); ?></td> 
                            <td><?php echo _n2($item['buffer']); ?></td> 
							<td class="align-right"><b><?php echo number_format($subtotal, 2); ?><b></td>  
 
                        </tr>
                    </tbody>
                    <?php 
                    $total += $subtotal; 
                    $i++;
                    ?>
                    <?php endforeach; ?>

                    <tbody>
                        <tr>
                            <td>Total</td> 
                            <td></td> 
                            <td></td>   
                            <td></td>  
                            <td></td>
                            <td></td> 
                            <td></td> 
                            <td class="align-right"><b><?php echo number_format($total, 2); ?></b></td>  
                        </tr>
                    </tbody>
                    <input type="hidden" id="totalMaterial" value="<?php echo $total; ?>">
                </table>

                <h4>Indirect Cost</h4>
                <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                    <thead>
                        <tr> 
                            <td>Item Name/Code</td> 
                            <td>Qty</td>
                            <td>Margin (%)</td> 
                            <td>Price / Unt</td>  
                            <td class="align-right">Total</td>   
                        </tr>
                    </thead> 
                <tbody>
                <?php 
                $total_cost = 0;
                foreach($costs as $cost) { ?> 
                 <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
                      <tr>
                        <td><?php echo $cost['SaleQuotationItemCost']['name']; ?><br/>
                        <small><?php echo $cost['InventoryItem']['name']; ?></small>
                        </td>
                        <td><?php echo $cost['SaleQuotationItemCost']['quantity']; ?> <?php echo $cost['GeneralUnit']['name']; ?></td>
                        <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 2); ?></td>  
                        <td><?php echo number_format($cost['SaleQuotationItemCost']['price'], 2); ?></td> 
                         <td class="align-right"><?php echo number_format($cost['SaleQuotationItemCost']['total_price'], 2); ?></td> 
                      </tr>
                      <?php } ?>
                  <tr>
                    <td><b>Total Indirect Cost</b></td>
                    <td> </td>
                    <td> </td> 
                    <td> </td> 
                    <td class="align-right"><b><?php echo number_format($total_cost, 2); ?></b></td> 
                  </tr>
                </tbody> 
              </table>
              <?php $total_value = $total_cost + $total; ?>
              <input type="hidden" id="totalValue" value="<?php echo $total_cost; ?>">
              <input type="hidden" id="indirectCost" value="<?php echo $total_cost; ?>">
              

              <h4>Soft Cost</h4>
                <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                    <thead>
                        <tr> 
                            <td>Item Code</td> 
                            <td>Name</td>
                            <td>Margin (%)</td>   
                            <td class="align-right">Total</td>   
                        </tr>
                    </thead> 
                <tbody>
                <?php 
                $total_soft_cost = 0;
                foreach($soft_costs as $cost) { ?> 
                 <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
                 <?php $percent = $total_value / 100 * $cost['SaleQuotationItemCost']['margin']; ?>
                 <?php $total_soft_cost += $percent; ?>
                      <tr>
                        <td><?php echo $cost['InventoryItem']['code']; ?></td>
                        <td><?php echo $cost['InventoryItem']['name']; ?></td> 
                        <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 2); ?></td> 
                        <td class="align-right"><?php echo number_format($percent, 2); ?></td>   
                      </tr>
                      <?php } ?>
                  <tr>
                    <td><b>Total Soft Cost</b></td>
                    <td> </td>
                    <td> </td>   
                    <td class="align-right"><b><?php echo number_format($total_soft_cost, 2); ?></b></td> 
                  </tr>
                </tbody> 
              </table> 
              <?php $grand_total = $total_value + $total_soft_cost; ?>
              <input type="hidden" id="totalSoftCost" value="<?php echo $total_soft_cost; ?>">
              <input type="hidden" id="totalValue" value="<?php echo $grand_total; ?>">
              <h3 class="pull-right">Grand Total: <?php echo Configure::read('Site.default_currency'); ?><?php echo number_format($grand_total, 2); ?></h3>
        </div>
    </div>
</div>
</div>
<?php } else { ?> 
 
	<div class="row"> 
	  	<div class="col-xs-12"> 
	    	<div class="x_panel tile">
	      		<div class="x_title">
	        		<h2><?php echo $item['name']; ?> - <?php echo $item['code']; ?></h2> 
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content"> 
	        	<?php echo $this->Session->flash(); ?> 
	<div class="box-blue">
	    <?php echo $this->Form->create('SaleQuotationItem', array('class'=>'form-horizontal')); ?> 
	        <?php echo $this->Form->input('id'); ?>
	        <div class="form-group">
        	<label class="col-sm-2">Material Cost</label>
        	<div class="col-sm-10">
        		<?php echo $this->Form->input('material_cost', array('id' => 'material_cost', 'class' => 'form-control', 'label' => false)); ?> 
        	</div>
 
        </div> 

        <div class="form-group">
            <label class="col-sm-2">Indirect Cost</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('indirect_cost', array('id' => 'indirect_cost', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2">Soft Cost</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('soft_cost', array('id' => 'soft_cost', 'class' => 'form-control', 'label' => false)); ?> 
            </div>
        </div> 
 
	        <div class="form-group">
	        	<label class="col-sm-2">Total Cost</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('total_cost', array('id' => 'total_value', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>  
	        <div class="form-group">
	        	<label class="col-sm-2">Margin</label>
	        	<div class="col-sm-5">
	        		<?php echo $this->Form->input('planning_margin', array('value' => 0, 'type' => 'text', 'id' => 'margin', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        	<div class="col-sm-5">
	        		<?php echo $this->Form->input('margin_value', array('value' => 0, 'type' => 'text', 'id' => 'margin_value', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>  
	        <div class="form-group">
	        	<label class="col-sm-2">Total</label>
	        	<div class="col-sm-10">
	        		<?php echo $this->Form->input('planning_price', array('type' => 'text', 'id' => 'total', 'class' => 'form-control', 'label' => false)); ?> 
	        	</div>
	        </div>  
	        <div class="form-group"> 
	        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
	        </div>	 
			
			<?php $this->Form->end(); ?>
		</div>	
	        	<div class="related table-responsive">
					<h3>Material Cost</h3>
					<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
						<thead>
							<tr>
								<td>#</td>
								<td>Code</td>
								<td>Name</td>
								<td>Margin (%)</td> 
								<td>Currency</td>
								<td>Qty</td>  
								<td>Qty Total</td>  
								<td>Price/Qty</td> 
								<td>Subtotal</td> 
								<td class="align-right">Total*Rate</td> 
							</tr>
						</thead> 
						<?php 
						$total = 0; 
						$i = 1;
						?> 
 

                        <?php
                        $subtotal = $item['price_total'] * $item['rate'];
                        ?>

                        <tbody>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $item['code']; ?></td>
                                <td><?php echo $item['name']; ?></td>
                                <td><?php echo $item['rate']; ?></td> 
                                <td><?php echo $item['currency']; ?></td>
                                <td><?php echo _n2($item['quantity']); ?></td>  
                                <td><?php echo _n2($item['quantity_total']); ?></td>  
                                <td><?php echo $item['unit_price']; ?></td> 
                                <td><?php echo number_format($item['price_total'], 2); ?></td> 
                                <td class="align-right"><b><?php echo number_format($subtotal, 2); ?></b></td> 
                                 
                            </tr>
                        </tbody>
                        <?php 
                        $total = $subtotal; 
                        $i++;
                        ?> 

                        <tbody>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td></td> 
                                <td></td>
                                <td></td>  
                                <td></td>  
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td class="align-right"><b><?php echo number_format($total, 2); ?></b></td> 
                            </tr>
                        </tbody>

                    </table>
                    <input type="hidden" id="totalMaterial" value="<?php echo $total; ?>">
                    <h3>Indirect Cost</h3>
                    <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                        <thead>
                            <tr> 
                                <td>Item Code/Name</td>
                                <td>Qty</td> 
                                <td>Margin (%)</td>
                                <td>Price / Unt</td>   
                                <td class="align-right">Total</td>
                            </tr>
                        </thead> 
                    <tbody>
                    <?php 
                    $total_cost = 0;
                    foreach($costs as $cost) { ?> 
                    <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
                      <tr>
                        <td><?php echo $cost['SaleQuotationItemCost']['name']; ?><br/>
                        <small><?php echo $cost['InventoryItem']['name']; ?></small>
                        </td>
                        <td><?php echo $cost['SaleQuotationItemCost']['quantity']; ?> <?php echo $cost['GeneralUnit']['name']; ?></td>
                        <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 4); ?></td>  
                        <td class="align-right"><?php echo number_format($cost['SaleQuotationItemCost']['price'], 2); ?></td> 
                        <td class="align-right"><?php echo number_format($cost['SaleQuotationItemCost']['total_price'], 2); ?></td> 
                      </tr>
                      <?php } ?>
                      <tr>
                        <td><p><b>Total</b></p></td>
                        <td> </td>
                        <td> </td> 
                        <td> </td> 
                        <td class="align-right"><b><?php echo number_format($total_cost, 2); ?></b></td> 
                      </tr>
                    </tbody> 
                  </table>
                  <?php $total_value = $total_cost + $total; ?>
                  <input type="hidden" id="indirectCost" value="<?php echo $total_cost; ?>">  

                  <h3>Soft Cost</h3>
                <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                    <thead>
                        <tr> 
                            <td>Item Code</td>  
                            <td>Item Name</td> 
                            <td>Margin (%)</td>    
                            <td class="align-right">Total</td>   
                        </tr>
                    </thead> 
                <tbody>
                <?php 
                $total_soft_cost = 0;
                foreach($soft_costs as $cost) { ?> 
                 <?php $total_cost += $cost['SaleQuotationItemCost']['total_price']; ?>
                 <?php $percent = $total_value / 100 * $cost['SaleQuotationItemCost']['margin']; ?>
                 <?php $total_soft_cost += $percent; ?>
                      <tr>
                        <td><?php echo $cost['SaleQuotationItemCost']['name']; ?></td>
                        <td><?php echo $cost['InventoryItem']['name']; ?></td> 
                        <td><?php echo number_format($cost['SaleQuotationItemCost']['margin'], 2); ?></td> 
                        <td class="align-right"><?php echo number_format($percent, 2); ?></td>   
                      </tr>
                      <?php } ?>
                  <tr>
                    <td><p><b>Total</b></p></td>
                    <td> </td>
                    <td> </td>   
                    <td class="align-right"><b><?php echo number_format($total_soft_cost, 2); ?></b></td> 
                  </tr>
                </tbody> 
              </table>
              <?php $grand_total = $total_soft_cost + $total_value; ?>
              <input type="hidden" id="totalSoftCost" value="<?php echo $total_soft_cost; ?>">
              <input type="hidden" id="totalValue" value="<?php echo $grand_total; ?>">
              <h3 class="pull-right">Grand Total: <?php echo number_format($grand_total, 2); ?></h3>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php $this->start('script'); ?>

<script type="text/javascript"> 

function printIframe(objFrame){ 
  objFrame.focus(); 
  objFrame.print(); 
  bjFrame.save(); 
}
$(document).ready(function() {
    var totalMaterial = Number.parseFloat($('#totalMaterial').val());
    var indirectCost = Number.parseFloat($('#indirectCost').val());
    var totalSoftCost = Number.parseFloat($('#totalSoftCost').val());

    var costing = totalMaterial + indirectCost + totalSoftCost;
    $('#total_value').val(Number.parseFloat(costing).toFixed(2));
    <?php if($is_bom) { ?>
        <?php if($bom['SaleQuotationItem']['planning_price'] > 0) { ?>
            $('#total').val(<?php echo $bom['SaleQuotationItem']['planning_price']; ?>);
        <?php } else { ?>
            $('#total').val(costing.toFixed(2));
        <?php } ?> 
    <?php } else { ?>
        <?php if($item['planning_price'] > 0) { ?>
            $('#total').val(<?php echo $item['planning_price']; ?>);
        <?php } else { ?>
            $('#total').val(costing.toFixed(2));
        <?php } ?> 
    <?php } ?>

     $('#material_cost').val(totalMaterial.toFixed(2)); 
     $('#indirect_cost').val(indirectCost.toFixed(2));
     $('#soft_cost').val(totalSoftCost.toFixed(2));

    $('#margin').on('keyup', function() {
        var margin = Number.parseFloat($('#margin').val());
        var percent = (costing / 100) * margin;
        var total = costing + Number.parseFloat(percent);
        $('#margin_value').val(percent.toFixed(2));
        $('#total').val(total.toFixed(2));
    });
});
</script>
<?php $this->end(); ?>