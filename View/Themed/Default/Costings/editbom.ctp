<?php 
      echo $this->Html->css(array(
        '/js/libs/ludo-jquery-treetable/css/jquery.treetable.theme.default.css' 
      ));   
    ?>

<?php echo $this->Html->link('Back To Quotation', array('action' => 'view', $detail['SaleQuotation']['id']), array('class' => 'btn btn-default', 'escape' => false)); ?>

<?php echo $this->Html->link('Notify Procurement To Add Price Book', array('action' => 'viewitem', $id, '?price_books'), array('class' => 'btn btn-warning', 'escape' => false)); ?>




<?php if($is_bom) { ?> 
<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo $bom['SaleBom']['name']; ?> - Qty: <?php echo $bom['SaleBom']['no_of_order']; ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 

        <?php echo $this->Form->create('SaleQuotationItem', array('class'=>'form-horizontal')); ?> 
 


  
        	<div class="related table-responsive">
				<h4>Materials</h4>
<div id="loadingImage" style="display: none">
    <img src="//i.stack.imgur.com/FhHRx.gif" />
</div>		
				<table id="example-advanced">
			<caption>
          <button onclick="jQuery('#example-advanced').treetable('expandAll'); return false;">Expand all</button>
          <button href="#" onclick="jQuery('#example-advanced').treetable('collapseAll'); return false;">Collapse all</button>
        </caption>
					<thead> 
						<tr>
							 
							<td>Code</td> 
							<td>Name</td>
							<td>Qty</td>  
							   
							<td>UOM</td> 
							 
						</tr> 
						</thead>
<tbody>    
<?php foreach ($items as $item): ?> 

<tr data-tt-id="<?php echo $item['bom_parent']; ?>" data-tt-branch="true" data-id="<?php echo $item['id']; ?>">   

<?php if($item['type'] == 'S.A') { ?>   
	<td><span class="folder"><?php echo $item['bom_parent']; ?> - <?php echo $item['name']; ?></span></td>   
<?php } else { ?>
	<td><?php echo $item['parent_id']; ?> - <?php echo $item['name']; ?></span></td>   
<?php } ?> 
	<td><?php echo $item['name']; ?></td>
	<td><?php echo $item['quantity']; ?></td>
	<td><?php echo $item['unit']; ?></td>   
</tr>  
<?php endforeach; ?> 
</tbody>

			</table>

 
			<div class="form-group"> 
	        	<?php 
	        	echo $this->Form->submit('Save Price', array('div' => false, 'class' => 'btn btn-success pull-right')); 
	        	?> 
	        </div>	  
			<?php $this->Form->end(); ?>  
   		</div>
    </div>
</div>
</div> 
<?php } ?>

<?php $this->start('script'); ?>
<?php
      echo $this->Html->script(array(
        '/js/libs/jquery-sortable/jquery-sortable.js',
        '/js/libs/ludo-jquery-treetable/jquery.treetable.js'
      ));
    ?>  

<script type="text/javascript">    

function getNodeViaAjax(parentNodeID) {
    $("#loadingImage").show();
    
    // ajax should be modified to only get childNode data from selected nodeID
    // was created this way to work in jsFiddle
    $.ajax({
		type: 'POST',
        url: '/echo/json/',
        data: {
            json: JSON.stringify( jsonData )
        },
        success: function(data) {
            $("#loadingImage").hide();
    
            var childNodes = data.nodeID[parentNodeID];
            
            if(childNodes) {
                var parentNode = $("#example-basic").treetable("node", parentNodeID);

                for (var i = 0; i < childNodes.length; i++) {
                    var node = childNodes[i];

                    var nodeToAdd = $("#example-basic").treetable("node",node['ID']);

                    // check if node already exists. If not add row to parent node
                    if(!nodeToAdd) {
                        // create row to add
                        var row ='<tr data-tt-id="' + 
                            node['ID'] + 
                            '" data-tt-parent-id="' +
                            parentNodeID + '" ';
                        if(node['childNodeType'] == 'branch') {
                            row += ' data-tt-branch="true" ';
                        }

                        row += ' >';

                        // Add columns to row
                        for (var index in node['childData']) {
                            var data = node['childData'][index];
                            row += "<td>" + data + "</td>";
                        }

                        // End row
                        row +="</tr>";
                        
                        $("#example-basic").treetable("loadBranch", parentNode, row);
                    }



                }
            
            }

        },
        error:function(error){
            $("#loadingImage").hide();
            alert('there was an error');  
        },
        dataType: 'json'
    });
}
// End
function loadBranchNode(parentNodeID) { 
	$("#loadingImage").show();
	$.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'parent_id=' + parentNodeID+"&bom_id=<?php echo $bom['SaleBom']['id']; ?>",
        url: baseUrl + 'costings/ajaxfindchild', 
        success: function(data) {  

    		console.log(parentNodeID);

            var childNodes = data.nodeID[parentNodeID];
            
            if(childNodes) {
                var parentNode = $("#example-advanced").treetable("node", parentNodeID);

                for (var i = 0; i < childNodes.length; i++) {
                    var node = childNodes[i];

                    var nodeToAdd = $("#example-advanced").treetable("node", node['bom_parent']); 
                    // check if node already exists. If not add row to parent node
                    if(!nodeToAdd) {
                        // create row to add
                        var row ='<tr data-tt-id="' + 
                            node['bom_parent'] + 
                            '" data-tt-parent-id="' +
                            parentNodeID + '" ';
                        if(node['childNodeType'] == 'branch') {
                            row += ' data-tt-branch="true" ';
                        } 
                        row += ' >'; 
                        row += "<td><span class='folder'>" + node['code'] + "</span></td>";
                        row += "<td>" + node['name'] + "</td>";
                        row += "<td>" + node['quantity'] + "</td>";
                        row += "<td>" + node['unit'] + "</td>";
                        // End row
                        row +="</tr>"; 
                        $("#example-advanced").treetable("loadBranch", parentNode, row);
                    } 
                } 
            }

            $("#loadingImage").hide();
        }, 
        error:function(error){
            $("#loadingImage").hide(); 
        }
    }); 
}

function initLoadBranch() {
	loadBranchNode(this.id);
}

$(document).ready(function() {   
  
	$("#example-advanced").treetable({ 
      	expandable: true,  
      	onNodeExpand: initLoadBranch
      });

      // Highlight selected row
      $("#example-advanced tbody").on("mousedown", "tr", function() {
        $(".selected").not(this).removeClass("selected");
        $(this).toggleClass("selected");
      });

      // Drag & Drop Example Code
      $("#example-advanced .file, #example-advanced .folder").draggable({
        helper: "clone",
        opacity: .75,
        refreshPositions: true, // Performance?
        revert: "invalid",
        revertDuration: 300,
        scroll: true
      });

      $("#example-advanced .folder").each(function() {
        $(this).parents("#example-advanced tr").droppable({
          accept: ".file, .folder",
          drop: function(e, ui) {
            var droppedEl = ui.draggable.parents("tr");
            $("#example-advanced").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
          },
          hoverClass: "accept",
          over: function(e, ui) {
            var droppedEl = ui.draggable.parents("tr");
            if(this != droppedEl[0] && !$(this).is(".expanded")) {
              $("#example-advanced").treetable("expandNode", $(this).data("ttId"));
            }
          }
        });
      });

      $("form#reveal").submit(function() {
        var nodeId = $("#revealNodeId").val();
        try {
          $("#example-advanced").treetable("reveal", nodeId);
        }
        catch(error) {
          alert(error.message);
        } 
        return false;
      }); 

    $('#example-advanced .loadChild').each(function() {
		
	});  
    // End tree

	$('.buffer').each(function() {
		$(this).on('keyup', function() {
			var buffer = Number.parseFloat($(this).val());
			var attrId = $(this).attr('id');
			var totalRm = Number.parseFloat($('#total_rm-'+attrId).val());
			var totalAmount = 0;
			if(buffer > 0) {
				totalAmount = ((totalRm / 100) * buffer) + totalRm;
				$('#total-'+attrId).val(totalAmount.toFixed(2));
			} else {
				$('#total-'+attrId).val(totalRm.toFixed(2));
			}
		}); 
	});

	

	var totalMaterial = Number.parseFloat($('#totalMaterial').val());
	var indirectCost = Number.parseFloat($('#indirectCost').val());
	var totalSoftCost = Number.parseFloat($('#totalSoftCost').val());

	var costing = totalMaterial + indirectCost + totalSoftCost;
	$('#total_value').val(Number.parseFloat(costing).toFixed(2));
	<?php if($is_bom) { ?>
		<?php if($bom['SaleQuotationItem']['planning_price'] > 0) { ?>
			$('#total').val(<?php echo $bom['SaleQuotationItem']['planning_price']; ?>);
		<?php } else { ?>
			$('#total').val(costing.toFixed(2));
		<?php } ?> 
	<?php } else { ?>
		<?php if($item['planning_price'] > 0) { ?>
			$('#total').val(<?php echo $item['planning_price']; ?>);
		<?php } else { ?>
			$('#total').val(costing.toFixed(2));
		<?php } ?> 
	<?php } ?>

	 $('#material_cost').val(totalMaterial.toFixed(2)); 
	 $('#indirect_cost').val(indirectCost.toFixed(2));
	 $('#soft_cost').val(totalSoftCost.toFixed(2));

	$('#margin').on('keyup', function() {
		var margin = Number.parseFloat($('#margin').val());
		var percent = (costing / 100) * margin;
		var total = costing + Number.parseFloat(percent);
		$('#margin_value').val(percent.toFixed(2));
		$('#total').val(total.toFixed(2));
	});
});

$(document).on("keyup", ".buffer", function() {
    var sum = 0;
    $(".total_rm").each(function(){
        sum += +$(this).val();
    });
    $("#totalMaterial").val(sum.toFixed(2)); 
    $("#totalBottom").html(sum.toFixed(2));
});
</script>


<?php $this->end(); ?>