<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleQuotations form">
<?php echo $this->Form->create('SaleQuotation', array('class' => 'form-horizontal form-label-left')); ?> 
	<?php echo $this->Form->input('id'); ?>
	<div class="form-group">
		<label class="col-sm-3">Tender</label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('sale_tender_id', array('class' => 'form-control', 'label' => false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Customer</label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('customer_id', array('class' => 'form-control', 'label' => false)); ?>
		</div>
	</div> 
	
	<div class="form-group">
	<label class="col-sm-3">HOD Verification</label>
		<div class="col-sm-9">
			<?php echo $data['SaleQuotation']['hod_status'] == 1 ? 'Verified' : 'Waiting Verification'; ?>
		</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3">GM Verification</label>
		<div class="col-sm-9">
			<?php echo $data['SaleQuotation']['gm_status'] == 1 ? 'Verified' : 'Waiting Verification'; ?>
		</div>
	</div>

	<?php foreach ($saleBoms as $bom) { ?>
		
		<div class="form-group">
		<label class="col-sm-3">BOM</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('SaleBom.name', array('value' => $bom['SaleBom']['name'], 'class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>

	<?php } ?>

	<div class="form-group">
	<label class="col-sm-3">Remark</label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
		</div>
	</div>

	<div class="form-group">
	<label class="col-sm-3">Status</label>
		<div class="col-sm-9">
			<?php $status = array(0 => 'Draft', 1 => 'Submit For Verification'); ?>
			<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3"></label>
		<div class="col-sm-9">
			<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
		</div>
	</div>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SaleQuotation.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SaleQuotation.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
	</ul>
</div>
</div>
    </div>
  </div> 
</div>