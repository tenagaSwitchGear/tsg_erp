<div class="inventorySupplierCategories form">
<?php echo $this->Form->create('InventorySupplierCategory'); ?>
	<fieldset>
		<legend><?php echo __('Add Inventory Supplier Category'); ?></legend>
	<?php
		echo $this->Form->input('inventory_supplier_id');
		echo $this->Form->input('inventory_item_category_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Inventory Supplier Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Item Categories'), array('controller' => 'inventory_item_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item Category'), array('controller' => 'inventory_item_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
