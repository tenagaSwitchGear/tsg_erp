<div class="inventorySupplierCategories view">
<h2><?php echo __('Inventory Supplier Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierCategory['InventorySupplierCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Supplier'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierCategory['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $inventorySupplierCategory['InventorySupplier']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierCategory['InventoryItemCategory']['name'], array('controller' => 'inventory_item_categories', 'action' => 'view', $inventorySupplierCategory['InventoryItemCategory']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Supplier Category'), array('action' => 'edit', $inventorySupplierCategory['InventorySupplierCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Supplier Category'), array('action' => 'delete', $inventorySupplierCategory['InventorySupplierCategory']['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierCategory['InventorySupplierCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Item Categories'), array('controller' => 'inventory_item_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item Category'), array('controller' => 'inventory_item_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
