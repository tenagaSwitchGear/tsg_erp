<div class="finishedGoodPackagingItems view">
<h2><?php echo __('Finished Good Packaging Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackagingItem['FinishedGoodPackagingItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good Packaging'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodPackagingItem['FinishedGoodPackaging']['name'], array('controller' => 'finished_good_packagings', 'action' => 'view', $finishedGoodPackagingItem['FinishedGoodPackaging']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodPackagingItem['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $finishedGoodPackagingItem['FinishedGood']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Production Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodPackagingItem['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $finishedGoodPackagingItem['ProductionOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackagingItem['FinishedGoodPackagingItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackagingItem['FinishedGoodPackagingItem']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($finishedGoodPackagingItem['FinishedGoodPackagingItem']['remark']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Finished Good Packaging Item'), array('action' => 'edit', $finishedGoodPackagingItem['FinishedGoodPackagingItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Finished Good Packaging Item'), array('action' => 'delete', $finishedGoodPackagingItem['FinishedGoodPackagingItem']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodPackagingItem['FinishedGoodPackagingItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Packaging Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Packaging Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Packagings'), array('controller' => 'finished_good_packagings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Packaging'), array('controller' => 'finished_good_packagings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
