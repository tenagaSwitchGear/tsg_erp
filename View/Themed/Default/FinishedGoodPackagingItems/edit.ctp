<div class="finishedGoodPackagingItems form">
<?php echo $this->Form->create('FinishedGoodPackagingItem'); ?>
	<fieldset>
		<legend><?php echo __('Edit Finished Good Packaging Item'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('finished_good_packaging_id');
		echo $this->Form->input('finished_good_id');
		echo $this->Form->input('production_order_id');
		echo $this->Form->input('remark');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('FinishedGoodPackagingItem.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('FinishedGoodPackagingItem.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Good Packaging Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Good Packagings'), array('controller' => 'finished_good_packagings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Packaging'), array('controller' => 'finished_good_packagings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
