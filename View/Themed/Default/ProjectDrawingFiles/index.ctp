<div class="projectDrawingFiles index">
	<h2><?php echo __('Project Drawing Files'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('project_drawing_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('directory'); ?></th>
			<th><?php echo $this->Paginator->sort('extension'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectDrawingFiles as $projectDrawingFile): ?>
	<tr>
		<td><?php echo h($projectDrawingFile['ProjectDrawingFile']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projectDrawingFile['ProjectDrawing']['name'], array('controller' => 'project_drawings', 'action' => 'view', $projectDrawingFile['ProjectDrawing']['id'])); ?>
		</td>
		<td><?php echo h($projectDrawingFile['ProjectDrawingFile']['name']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingFile['ProjectDrawingFile']['directory']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingFile['ProjectDrawingFile']['extension']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectDrawingFile['ProjectDrawingFile']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectDrawingFile['ProjectDrawingFile']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectDrawingFile['ProjectDrawingFile']['id']), array(), __('Are you sure you want to delete # %s?', $projectDrawingFile['ProjectDrawingFile']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project Drawing File'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Drawings'), array('controller' => 'project_drawings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Drawing'), array('controller' => 'project_drawings', 'action' => 'add')); ?> </li>
	</ul>
</div>
