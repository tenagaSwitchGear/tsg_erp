<?php echo $this->Html->link(__('List Project Drawings'), array('controller' => 'project_drawings', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add New Drawing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
 			<?php echo $this->Form->create('ProjectDrawingFile', array('class'=>'form-horizontal', 'type' => 'file')); ?>
 		  
      <h4>Click Add Drawing to upload (Max 100mb - PDF/JPG/PNG)</h4>

			<div id="output"></div>

			<a href="#" id="addMore" class="btn btn-default"><i class="fa fa-plus"></i>Add Drawing</a>

			<div class="form-group"> 
			<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
			</div>	  
		  <?php $this->Form->end(); ?> 
      </div> 
    </div>
  </div> 
</div> 

<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).ready(function() {  
  
  var row = 0;
  $('#addMore').click(function() {   
    var html = '<div id="removeItem'+row+'">'; 
    html += '<div class="form-group">'; 
    html += '<div class="col-sm-2">';
    html += '<input type="text" name="data[ProjectDrawingFile]['+row+'][product]" class="form-control" placeholder="Item Code/Name"required>'; 
    html += '</div>'; 
    html += '<div class="col-sm-2">';
    html += '<input type="text" name="data[ProjectDrawingFile]['+row+'][drawing_no]" class="form-control" placeholder="Drawing No"required>'; 
    html += '</div>'; 
    html += '<div class="col-sm-2">';
    html += '<input type="text" name="data[ProjectDrawingFile]['+row+'][title]" class="form-control" placeholder="Drawing Name"required>'; 
    html += '</div>'; 
    html += '<div class="col-sm-3">';
    html += '<input type="file" name="data[ProjectDrawingFile]['+row+'][name]" class="form-control"required>';  
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][directory]">'; 
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][user_id]" value="<?php echo $this->Session->read('Auth.User.id'); ?>">'; 
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][created]" value="<?php echo date('Y-m-d H:i:s'); ?>">';  
    html += '<input type="hidden" name="data[ProjectDrawingFile]['+row+'][project_drawing_id]" value="<?php echo $id; ?>">';  
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<select name="data[ProjectDrawingFile]['+row+'][status]" class="form-control"required>';
    html += '<option value="Draft">Draft</option><option value="Submitted">Submitted For Approval</option><option value="Receive From Client">Receive From Client</option><option value="Rejected">Rejected</option><option value="Revise & Resubmitted">Revise & Resubmitted</option><option value="Approved">Approved</option><option value="Approved (Info only)">Approved (Info only)</option>';
    html +='</select>'; 
    html += '</div>'; 

     
    html += '<div class="col-sm-1">';
    html += '<a href="#" class="btn btn-danger" onclick="removeItem('+row+'); return false"><i class="fa fa-times"></i></a>';
    html += '</div>';
    html += '</div>';
    html += '</div>';     
    row++;  
    $("#output").append(html);      
    return false;
  });   
});  

function removeItem(row) {
  $('#removeItem'+row).html('');
  return false;
}
</script>
<?php $this->end(); ?>