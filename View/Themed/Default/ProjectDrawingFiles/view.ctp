<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View Drawing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="projectDrawings view-data"> 
 
<h2><?php echo __('Project Drawing File'); ?></h2>
	<dl>  
		<dt><?php echo __('Project Drawing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectDrawingFile['ProjectDrawing']['name'], array('controller' => 'project_drawings', 'action' => 'view', $projectDrawingFile['ProjectDrawing']['id'])); ?>
			&nbsp;
		</dd> 
		<dt>Attachment</dt>
		<dd>
			<?php echo $this->Html->link(__($projectDrawingFile['ProjectDrawingFile']['title']), BASE_URL . '/files/project_drawing_file/name/' . $projectDrawingFile['ProjectDrawingFile']['directory'] . '/' . $projectDrawingFile['ProjectDrawingFile']['name'], array('target' => '_blank')); ?>
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($projectDrawingFile['ProjectDrawingFile']['status']); ?>
			&nbsp;
		</dd> 
	</dl>
	</div>

	<h4><?php echo __('Drawing Status'); ?></h4>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th>Status</th>
			<th>Created</th>
			<th>Remark</th>
			<th>User</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($trackings as $projectDrawingTracking): ?>
	<tr>  
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['status']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['created']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['ProjectDrawingTracking']['remark']); ?>&nbsp;</td>
		<td><?php echo h($projectDrawingTracking['User']['username']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectDrawingTracking['ProjectDrawingTracking']['id']), array(), __('Are you sure you want to delete # %s?', $projectDrawingTracking['ProjectDrawingTracking']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
</div>
</div>
</div>