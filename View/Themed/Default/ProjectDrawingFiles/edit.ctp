<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit Drawing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="projectDrawingFiles form">
<?php echo $this->Form->create('ProjectDrawingFile', array('class'=>'form-horizontal', 'type' => 'file')); ?>
<?php $status = array(
		'Draft' => 'Draft',
		'Submitted' => 'Submitted',
		'Receive From Client' => 'Receive From Client',
		'Rejected' => 'Rejected',
		'Revise & Resubmitted' => 'Revise & Resubmitted',
		'Approved' => 'Approved',
		);
		?>
	<fieldset>
		<legend><?php echo __('Edit Project Drawing File'); ?></legend>
	<?php
		echo $this->Form->input('id'); 
		echo $this->Form->input('title', array('class' => 'form-control'));
		echo $this->Form->input('name', array('class' => 'form-control', 'type' => 'file'));
		echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control'));
		echo $this->Form->input('directory', array('type' => 'hidden')); 
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
 
</div>
</div>
</div>
</div>