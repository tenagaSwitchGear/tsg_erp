<div class="saleQuotationItemCosts view">
<h2><?php echo __('Sale Quotation Item Cost'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleQuotationItemCost['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleQuotationItemCost['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Unit Price'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['unit_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Discount'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['discount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['tax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Price'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['total_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Quotation Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleQuotationItemCost['SaleQuotationItem']['name'], array('controller' => 'sale_quotation_items', 'action' => 'view', $saleQuotationItemCost['SaleQuotationItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($saleQuotationItemCost['SaleQuotationItemCost']['type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Quotation Item Cost'), array('action' => 'edit', $saleQuotationItemCost['SaleQuotationItemCost']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Quotation Item Cost'), array('action' => 'delete', $saleQuotationItemCost['SaleQuotationItemCost']['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotationItemCost['SaleQuotationItemCost']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Item Costs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item Cost'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
