<div class="saleQuotationItemCosts form">
<?php echo $this->Form->create('SaleQuotationItemCost'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sale Quotation Item Cost'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('quantity');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('unit_price');
		echo $this->Form->input('discount');
		echo $this->Form->input('tax');
		echo $this->Form->input('total_price');
		echo $this->Form->input('sale_quotation_item_id');
		echo $this->Form->input('type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SaleQuotationItemCost.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SaleQuotationItemCost.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Item Costs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
