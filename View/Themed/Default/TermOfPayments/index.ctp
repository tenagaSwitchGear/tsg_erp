<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Payment Term'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
        
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Term Of Payments'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<div class="termOfPayments index">
				
				<table cellpadding="0" cellspacing="0" class="table table-bordered">
				<thead>
				<tr>
						<th><?php echo $this->Paginator->sort('id'); ?></th>
						<th><?php echo $this->Paginator->sort('name'); ?></th>
						<th><?php echo $this->Paginator->sort('created'); ?></th>
						<th><?php echo $this->Paginator->sort('modified'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($termOfPayments as $termOfPayment): ?>
				<tr>
					<td><?php echo h($termOfPayment['TermOfPayment']['id']); ?>&nbsp;</td>
					<td><?php echo h($termOfPayment['TermOfPayment']['name']); ?>&nbsp;</td>
					<td><?php echo h($termOfPayment['TermOfPayment']['created']); ?>&nbsp;</td>
					<td><?php echo h($termOfPayment['TermOfPayment']['modified']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $termOfPayment['TermOfPayment']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $termOfPayment['TermOfPayment']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $termOfPayment['TermOfPayment']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($termOfPayment['TermOfPayment']['name']).'"', $termOfPayment['TermOfPayment']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
				//echo $this->Paginator->counter(array(
				//'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
				</ul>
			</div>
		</div>
	</div>
</div>
</div>