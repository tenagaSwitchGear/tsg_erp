<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Payment Term'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Term of Payment'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Edit Term Of Payment'); ?></h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
       	 		<div class="termOfPayments form">
				<?php echo $this->Form->create('TermOfPayment', array('class'=>'form-horizontal')); ?>
				<?php
					echo $this->Form->input('id');
				?>
				<fieldset>
					<div class="form-group">
						<label class="col-sm-3">Name</label>
						<div class="col-sm-9">
							<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Name')); ?>
						</div>
					</div>
				</fieldset>
			<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
			</div>
		</div>
	</div>
</div>
</div>

