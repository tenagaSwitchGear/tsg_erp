<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Payment Term'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Term of Payment'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Inventory Supplier</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<div class="container table-responsive">
				<h2><?php echo __('Term Of Payment'); ?></h2>
				<dl>
					<dt class="col-sm-2"><?php echo __('Name'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($termOfPayment['TermOfPayment']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($termOfPayment['TermOfPayment']['created']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-2"><?php echo __('Modified'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($termOfPayment['TermOfPayment']['modified']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>
		</div>
	</div>
</div>
</div>
