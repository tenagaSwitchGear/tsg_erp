<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('New Customer Adresses'), array('controller' => 'customer_addresses', 'action' => 'add?id='.$customer['Customer']['id']), array('class' => 'btn btn-success btn-sm')); ?> 
  		<?php echo $this->Html->link(__('New Customer Contact Person'), array('controller' => 'customer_contact_persons', 'action' => 'add?id='.$customer['Customer']['id']), array('class' => 'btn btn-success btn-sm')); ?> 
  		<?php echo $this->Html->link(__('New Tender'), array('controller' => 'sale_tenders', 'action' => 'add?id='.$customer['Customer']['id']), array('class' => 'btn btn-success btn-sm')); ?> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customers</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 
        	<!-- content start-->
        	<div class="container table-responsive">
				<dl> 
					<dt class="col-sm-3"><?php echo __('Name'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['name']); ?>
						&nbsp;
					</dd> 
					<dt class="col-sm-3"><?php echo __('Address'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['address']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Postcode'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['postcode']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('City'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['city']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('State'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['State']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('State (Oversea)'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['state']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Country'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Country']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Email'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['email']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Phone'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['phone']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Customer Type'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['CustomerType']['name']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Website'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['website']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Reg No'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['reg_no']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Tax ID'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['gst_tax']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Customer Business Category'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['CustomerBusinessCategory']['name']); ?><br/>
						<?php echo h($customer['Customer']['other_business_category']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Customer Ownership'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['CustomerOwnership']['name']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Credit Limit (RM)'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo _n2($customer['Customer']['credit_limit']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Created'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['created']); ?>
						&nbsp;
					</dd>
					<dt class="col-sm-3"><?php echo __('Added By'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['User']['firstname']); ?>
						&nbsp;
					</dd>

					<dt class="col-sm-3"><?php echo __('Remark'); ?></dt>
					<dd class="col-sm-9">
						: <?php echo h($customer['Customer']['remark']); ?>
						&nbsp;
					</dd>
				</dl>
			</div>
			<div class="clearfix">&nbsp;</div> 

			<div class="related table-responsive">
				<h4><?php echo __('Related Customer Address'); ?></h4>
				<?php if (!empty($customer['CustomerAddress'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<th><?php echo __('Address'); ?></th>
							<th><?php echo __('City'); ?></th>
							<th><?php echo __('State'); ?></th>
							<th><?php echo __('Contact'); ?></th>
							<th><?php echo __('Email'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<?php foreach ($customer['CustomerAddress'] as $address): ?>
					<tbody>
						<tr>
							<td><?php echo h($address['address']); ?></td>
							<td><?php echo h($address['city']); ?></td>
							<td><?php echo h($address['state']); ?></td>
							<td><?php echo h($address['phone']); ?></td> 
							<td><?php echo h($address['email']); ?></td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'customer_addresses', 'action' => 'view', $address['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'customer_addresses', 'action' => 'edit', $address['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'customer_addresses', 'action' => 'delete', $address['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete # %s?', $address['id'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Address
				<?php endif; ?>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="related table-responsive">
				<h4><?php echo __('Related Customer Contact Persons'); ?></h4>
				<?php if (!empty($customer['CustomerContactPersons'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr> 
							<th><?php echo __('Name'); ?></th>
							<th><?php echo __('Position'); ?></th>
							<th><?php echo __('Office Phone'); ?></th>
							<th><?php echo __('Mobile Phone'); ?></th>
							<th><?php echo __('Email'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<?php foreach ($customer['CustomerContactPersons'] as $customerContactPerson): ?>
					<tbody>
						<tr> 
							<td><?php echo h($customerContactPerson['name']); ?></td>
							<td><?php echo h($customerContactPerson['position']); ?></td>
							<td><?php echo h($customerContactPerson['office_phone']); ?></td>
							<td><?php echo h($customerContactPerson['mobile_phone']); ?></td>
							<td><?php echo h($customerContactPerson['email']); ?></td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'customer_contact_persons', 'action' => 'view', $customerContactPerson['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'customer_contact_persons', 'action' => 'edit', $customerContactPerson['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'customer_contact_persons', 'action' => 'delete', $customerContactPerson['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete # %s?', $customerContactPerson['id'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Customer Contact Person
				<?php endif; ?>
			</div>
 

			<div class="clearfix">&nbsp;</div>
			<div class="related table-responsive">
				<h4><?php echo __('Sale Enquiry'); ?></h4>
				<?php if (!empty($customer['SaleTender'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
				<thead>
					<tr> 
						<th><?php echo __('Title'); ?></th>
						<th><?php echo __('Tender No'); ?></th>
						<th><?php echo __('Detail'); ?></th>
						<th><?php echo __('Closing Date'); ?></th>
						<th><?php echo __('Price'); ?></th>
						<th><?php echo __('Remark'); ?></th>
						<th><?php echo __('Extend Date'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($customer['SaleTender'] as $saleTender): ?>
					<tr> 
						<td><?php echo h($saleTender['title']); ?></td>
						<td><?php echo h($saleTender['tender_no']); ?></td>
						<td><?php echo h($saleTender['detail']); ?></td>
						<td><?php echo h($saleTender['closing_date']); ?></td>
						<td><?php echo h($saleTender['price']); ?></td>
						<td><?php echo h($saleTender['remark']); ?></td>
						<td><?php echo h($saleTender['extend_date']); ?></td>
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('controller' => 'sale_tenders', 'action' => 'view', $saleTender['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'sale_tenders', 'action' => 'edit', $saleTender['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('controller' => 'sale_tenders', 'action' => 'delete', $saleTender['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete # %s?', $saleTender['id'])); ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				</table>
				<?php else: ?>
					&bull; No Related Sale Tender
				<?php endif; ?>
			</div> 
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>