<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Customer'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('Customer Contact Person'), array('controller' => 'customer_contact_persons','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>  
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customers</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
 
    <?php echo $this->Form->create('Customer', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Business Name', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('reg_no', array('type' => 'text', 'placeholder' => 'Reg No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 
        	<div class="table-responsive">
        	
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
						<tr> 
							<th><?php echo $this->Paginator->sort('name'); ?></th>
							<th><?php echo $this->Paginator->sort('email'); ?></th>
							<th><?php echo $this->Paginator->sort('phone'); ?></th>
							<th><?php echo $this->Paginator->sort('reg_no'); ?></th>
							<th><?php echo $this->Paginator->sort('city'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php  
							foreach ($customers as $customer): ?>
						<tr> 
							<td><?php echo h($customer['Customer']['name']); ?>&nbsp;</td>
							<td><?php echo h($customer['Customer']['email']); ?>&nbsp;</td>
							<td><?php echo h($customer['Customer']['phone']); ?>&nbsp;</td>
							<td><?php echo h($customer['Customer']['reg_no']); ?>&nbsp;</td>
							<td><?php echo h($customer['Customer']['city']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $customer['Customer']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $customer['Customer']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $customer['Customer']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($customer['Customer']['name']).'"', $customer['Customer']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p><?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?></p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$( function() { 
    $('#dateonly2').datepicker({
      dateFormat: 'yy-mm-dd', 
      ampm: true
    }); 
  });  
</script>
<?php $this->end(); ?>