<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Customers</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		<div class="customers form">
				<?php echo $this->Form->create('Customer', array('class' => 'form-horizontal')); ?>
					<fieldset>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Company Name</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("name", array("class"=> "form-control", "placeholder" => "Name", "label"=> false)); ?>
							<?php echo $this->Form->input("customer_station_id", array("type" =>"hidden", "value" => "1")); ?>
							</div> 
						</div> 
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Address</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("address", array("type" => "textarea", "class"=> "form-control", "placeholder" => "Address", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Postcode</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("postcode", array("class"=> "form-control", "placeholder" => "Postcode", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">City</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("city", array("class"=> "form-control", "placeholder" => "City", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">State</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("state_id", array('id' => 'state', "class"=> "form-control", "label"=> false, 'empty' => array(0 =>'-Select State-'))); ?>
							</div> 
						</div>
						<div class="form-group" style="display:none" id="oversea">
							<label class="col-sm-3" style="padding-top: 8px">State (International)</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("state", array('type' => 'text', "class"=> "form-control", "label"=> false, 'placeholder' => 'Enter State Name (International)', 'required' => false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Country</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("country_id", array("class"=> "form-control", "label"=> false, 'selected' => '124')); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Email</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("email", array("class"=> "form-control", "placeholder" => 'Email', "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Phone</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("phone", array("class"=> "form-control", "placeholder" => 'Phone', "label"=> false)); ?>
							</div> 
						</div>

						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Fax</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("phone", array("class"=> "form-control", "placeholder" => 'Phone', "label"=> false, 'required' => false)); ?>
							</div> 
						</div>

						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Customer Type</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("customer_type_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Website (Optional)</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("website", array("class"=> "form-control", "placeholder" => 'Website', "label"=> false, 'required' => false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Register No (Optional)</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("reg_no", array("class"=> "form-control", "placeholder" => 'Register No', "label"=> false, 'required' => false)); ?>
							</div> 
						</div>
						<div class="form-group"> 
							<label class="col-sm-3" style="padding-top: 8px">GST ID (Optional)</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("gst_tax", array("class"=> "form-control", "placeholder" => 'Tax ID', "label"=> false, 'required' => false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Customer Business Category</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("customer_business_category_id", array("class"=> "form-control", "label"=> false, "id" => "customer_business_category_id")); ?>
							</div> 
						</div> 
						<div class="form-group" style="display:none" id="other_business_category">
							<label class="col-sm-3" style="padding-top: 8px">Business Category</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("other_business_category", array("type" => "textarea", "class"=> "form-control", "placeholder" => 'Customer Business Category', "label"=> false, 'required' => false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Customer Ownership</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("customer_ownership_id", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div> 
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Remark (Optional)</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("remark", array("type" => "textarea", "class"=> "form-control", "placeholder" => 'Remark', "label"=> false, 'required' => false)); ?>
							</div> 
						</div>			 
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
					<?php
						echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm'));
						echo $this->Form->button('Reset', array('type'=>'reset', 'class' => 'btn btn-danger btn-sm','div' => false));
						echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
					?>
					</div>
					<?php echo $this->Form->end(); ?>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">

$(document).ready(function() {
 $('#state').on('change', function() {
 	var state_id = $(this).val();
 	if(state_id == 16) {
 		$('#oversea').show();
 	} else {
 		$('#oversea').hide();
 	}
 });

 $("#customer_business_category_id").on('change', function() {
 	var customer_business_category_id = $(this).val();
 	if(customer_business_category_id == 4) {
 		$('#other_business_category').show();
 	} else {
 		$('#other_business_category').hide();
 	}
 });

});

</script>
<?php $this->end(); ?>