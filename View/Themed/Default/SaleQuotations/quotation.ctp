<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Quotation ID: <?php echo $saleQuotation['SaleQuotation']['name']; ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('SaleQuotationItemCost', array('class'=>'form-horizontal')); ?> 

        <?php foreach($items as $item) { ?> 
        <div class="form-group">
        	<label class="col-sm-6"><?php echo $item['Product']['name']; ?></label>
        	<div class="col-sm-3">
                <p>Qty: <?php echo $item['SaleQuotationItem']['quantity']; ?></p> 
        	</div>
            <div class="col-sm-3">
                <p>Price / Qty: <?php echo $item['SaleQuotationItem']['unit_price']; ?></p> 
            </div>
        </div>    
        
        <div id="loadCost<?php echo $item['SaleQuotationItem']['id']; ?>"></div>

        <div class="form-group"> 
            <div class="col-sm-12">
                <a href="#" onclick="addCost('<?php echo $item['SaleQuotationItem']['quantity']; ?>'); return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Items</a>
            </div>
        </div>
        <?php } ?>

        <div class="form-group"> 
        		<?php echo $this->Form->submit('Next Step', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>
			 
        </div>	 
		
		<?php $this->Form->end(); ?>
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

 
$(document).ready(function() {
     
    var station = 1;
    $('#addItem').click(function() {
        var html = '<div id="removeBom'+station+'">'; 
        html += '<div class="form-group">';  
        html += '<div class="col-sm-4" id="autoComplete">';
        html += '<input type="text" name="data[SaleQuotationItem][name][]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="data[SaleQuotationItem][product_id][]" id="productId'+station+'">';
        html += '<input type="hidden" name="data[SaleQuotationItem][bom_id]" id="bom_id'+station+'">';
        html += '</div>';
     
        html += '<div class="col-sm-1">';
        html += '<input type="text" name="data[SaleQuotationItem][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
        html += '</div>';

        html += '<div class="col-sm-2">';
        html += '<select name="data[SaleQuotationItem][general_unit_id][]" class="form-control"required>'; 
        html += '<option value="">-Unit-</option>';
        html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
        html += '</select></div>';

        html += '<div class="col-sm-2"><input type="text" name="data[SaleQuotationItem][unit_price][]" class="form-control" id="price'+station+'" placeholder="Price/Unit" required></div>'; 
        html += '<div class="col-sm-1"><input type="text" name="data[SaleQuotationItem][discount][]" value="" class="form-control" placeholder="Discount"></div>';  
        html += '<div class="col-sm-1"><input type="text" name="data[SaleQuotationItem][tax][]" value="" class="form-control" placeholder="Tax"></div>';  
      

        html += '<div class="col-sm-1">';
        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';    
        $("#loadMoreBom").append(html);  
        
        findItem(station, $(this).val());  
        station++; 
    });  
});
</script>
<?php $this->end(); ?>


