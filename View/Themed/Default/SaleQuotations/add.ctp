<?php echo $this->Html->link('Draft', array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?> 
<?php echo $this->Html->link('Active', array('action' => 'index', 2), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link('Waiting Approval', array('action' => 'index', 3), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Rejected', array('action' => 'index', 7), array('class' => 'btn btn-warning btn-sm')); ?>
<?php echo $this->Html->link('Approved', array('action' => 'index', 6), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link('Quotation Submitted', array('action' => 'index', 9), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link('Awarded', array('action' => 'index', 10), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link('Failed', array('action' => 'index', 11), array('class' => 'btn btn-danger btn-sm')); ?>
<?php echo $this->Html->link('Create New Quotation', array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Create New Quotation</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
      
      
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Form->create('SaleQuotation', array('class'=>'form-horizontal', 'id' => 'form')); ?> 

        <div class="form-group">
            <label class="col-sm-3">Customer *</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('customer_id', array('empty' => '-Select Customer-', 'class' => 'form-control', 'label' => false, 'id' => 'customer_id')); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3">Tender No (Optional)</label>
            <div class="col-sm-3">
                <?php if($this->request->data){ ?>
                <?php echo $this->Form->input('find_tender', array('id' => 'findTender', 'class' => 'form-control', 'label' => false, 'value' => $this->request->data['SaleTender']['tender_no'])); ?>
                <?php echo $this->Form->input('sale_tender_id', array('id' => 'tenderId', 'type' => 'hidden', 'label' => false, 'value' => $this->request->data['SaleTender']['id'])); ?> 
                <?php }else{ ?>
                <?php echo $this->Form->input('find_tender', array('id' => 'findTender', 'class' => 'form-control', 'label' => false)); ?>
                <?php echo $this->Form->input('sale_tender_id', array('id' => 'tenderId', 'type' => 'hidden', 'value' => 0, 'label' => false)); ?> 
                <?php } ?>
                
            </div> 
            <label class="col-sm-3">GST (%) *</label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('gst', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'value' => Configure::read('Site.gst_amount'))); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3">Quotation Date *</label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('date', array('value' => date('Y-m-d'), 'type' => 'text', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?>
            </div>
        
            <label class="col-sm-3">Attention (Optional)</label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('attention', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'required' => false, 'placeholder' => 'Person in charge (Phone no)')); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3">Header (Optional)</label>
            <div class="col-sm-9">
                <?php 
                $val = 'Thank you for giving us the opportunity to quote for the above tender. We are very pleased to quote you our best price, delivery, terms and conditions as follows:-';
                echo $this->Form->input('header', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false, 'value' => $val)); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3">Remark (Optional)</label>
            <div class="col-sm-9">
                <?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
            </div>
        </div>

         
                <?php echo $this->Form->input('status', array('value' => 2, 'type' => 'hidden', 'label' => false)); ?>
                <?php echo $this->Form->input('hod_status', array('type' => 'hidden', 'value' => 0)); ?>
                <?php echo $this->Form->input('gm_status', array('type' => 'hidden', 'value' => 0)); ?> 
                <?php echo $this->Form->input('term_of_payment_id', array('type' => 'hidden', 'value' => '1')); ?>

        <div class="form-group">
        <div class="col-md-12"><h4>Terms &amp; Conditions</h4></div>
        </div> 

        <div class="form-group">
            <label class="col-sm-3">Offer Not Include *</label>
            <div class="col-sm-9">
                <?php 
$not_include = "•   Site erection, testing and commissioning works.<br/>
•   Support structures and foundation plinth.<br/>
•   Multicore and Auxiliary cables, cable ladders/trays.<br/>
•   Earthing from GCB/DS to earth grid.<br/>
•   Others which are not specifically mentioned.<br/>";

                echo $this->Form->input('notinclude', array('type' => 'textarea', 'value' => $not_include, 'class' => 'form-control', 'label' => false)); 
                ?>
                <small>This icon • can be copy and paste. Every line must be end with &lsaquo;br&frasl;&rsaquo; to prevent it display as 1 line.</small>
            </div>
        
             
        </div>

             
        <div class="form-group">
            <label class="col-sm-3">Price Validity </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('validwithin', array('value' => '90 days', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        
            <label class="col-sm-3">Payment Within </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('paidwithin', array('value' => 'Thirty (30) days', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3">Warranty Period </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('warrantyperiod', array('value' => 'twelve (12) months', 'class' => 'form-control', 'label' => false)); ?>
            </div>
         
            <label class="col-sm-3">2nd Warranty Period </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('orwarrantyperiod', array('value' => 'eighteen (18) months', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3">Testing </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('testedaccordingto', array('value' => 'Twenty One (21) days', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        
            <label class="col-sm-3">Notify Purchaser </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('notifupurchaser', array('value' => 'Twenty One (21) days', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        </div>
         
         <div class="form-group">
            <label class="col-sm-3">Termination </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('terminationday', array('value' => 'Fourteen (14) working days', 'class' => 'form-control', 'label' => false)); ?>
            </div>
         
            <label class="col-sm-3">Day Of Acceptance </label>
            <div class="col-sm-3">
                <?php echo $this->Form->input('dayofacceptance', array('value' => '30 days', 'class' => 'form-control', 'label' => false)); ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
            <h4>Select Items</h4> 
            <p>Costing value is come from Price Book that added by Procurement or PPC Department. If you see costing amount is 0, you can type it manually or contact Procurement / PPC if you&#39;re not sure.</p>
            </div>
        </div>  

        <div id="loadMoreBom"></div>

        <div class="form-group"> 
            <div class="col-sm-12">
                <a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Items</a>
            </div>
        </div>
 

        <div class="form-group"> 
                <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?>
             
        </div>   
        
        <?php $this->Form->end(); ?>
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
    console.log(search);
    $('#findProduct'+row).autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct'+row).val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            price: item.price,
                            bom_id: item.bom.id,
                            bom_name: item.bom.name,
                            bom_code: item.bom.code,
                            note: item.note,
                            unit: item.unit,
                            costings: item.costings,
                            pricebooks: item.pricebooks 
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {   
            console.log(ui.item);
            $('#productId'+row).val( ui.item.id ); 
            $('#price'+row).val( ui.item.price ); 
            $('#total'+row).val( ui.item.price ); 
            $('#bom_id'+row).val( ui.item.bom_id ); 
            $('#bom_name'+row).val( ui.item.bom_name );
            $('#quantity'+row).val(1);
            $('#unit'+row).val(ui.item.unit);
            $('#itemname'+row).val(ui.item.name); 
            if(ui.item.pricebooks.length != 0) {
                $('#supplier'+row).val(ui.item.pricebooks.InventorySupplier.name); 
                $('#planning_price'+row).val(ui.item.pricebooks.InventorySupplierItem.price_per_unit); 
                $('#price_book'+row).val(ui.item.pricebooks.InventorySupplierItem.id); 
            } else {
                $('#supplier'+row).val('Price Book Not Found'); 
                $('#planning_price'+row).val(0); 
                $('#price_book'+row).val(0); 
            }
        },
        minLength: 3 
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>Note: " + item.note + "</small><br>" +  "</div>" ).appendTo( ul );
    };
}

function calcPrice(row, val) { 
    
    
    $('#margin' + row + ', #quantity'+row+', #tax'+row+', #planning_price'+row).on('keyup', function() {
        var planning_price = parseFloat($('#planning_price'+row).val());
        var quantity = parseFloat($('#quantity'+row).val());
        var tax = parseFloat($('#tax'+row).val());
        var discount = parseFloat($('#discount'+row).val()); 
        var margin = parseFloat($('#margin'+row).val()); 
        margin = (planning_price / 100) * margin;
        $('#margin_value'+row).val(margin);

        var total = margin + planning_price;
        var sub_total = total * quantity;
        var taxed = total * quantity - discount;
        $('#sale_price'+row).val(total.toFixed(2));
        $('#sub_total'+row).val(sub_total.toFixed(2));
        $('#total_price'+row).val(taxed.toFixed(2));
    });    

    $('#discount' + row).on('keyup', function() { 
        var discount = parseFloat($('#discount'+row).val()); 
        var sub_total = parseFloat($('#sub_total'+row).val()); 
        var total = sub_total - discount; 
        $('#total_price'+row).val(total.toFixed(2));
    });
}
 

$(document).ready(function() { 
    $('#form').on('keyup', '.number', function(e) {  
        var value = $(this).val(); 
        if(!$.isNumeric(value)) {
            $(this).addClass('border-red');
        } else {
            if($(this).hasClass('border-red')) {
                $(this).removeClass('border-red');
            }
        }
    });

    $('#form').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        return false;
      }
    }); 

    var station = 1;
    $('#addItem').click(function() {
        var html = '<div id="removeBom'+station+'">'; 
        html += '<div class="row">';  
        html += '<div class="col-sm-4" id="autoComplete">Item';
        html += '<input type="text" name="data[SaleQuotationItem][name][]" id="findProduct'+station+'" class="form-control" placeholder="Item Code/Name"required><input type="hidden" name="data[SaleQuotationItem][product_id][]" id="productId'+station+'">';
       
        html += '</div>';

        html += '<div class="col-sm-3">Item Name';
        html += '<input type="text" id="itemname'+station+'" class="form-control" placeholder="Item Name"readonly>'
        html += '</div>';

        html += '<div class="col-sm-2">Cost';
         html += '<input type="text" name="data[SaleQuotationItem][planning_price][]" id="planning_price'+station+'" class="form-control number" placeholder="Cost">';  
        html += '<input type="hidden" name="data[SaleQuotationItem][inventory_supplier_item_id][]" id="price_book'+station+'" class="form-control" value="0">';  
        html += '</div>';
 
        html += '<input type="hidden" name="data[SaleQuotationItem][tax][]" value="<?php echo Configure::read('Site.gst_amount'); ?>" id="tax'+station+'" class="form-control" placeholder="Tax">';  

        html += '<div class="col-sm-1">Qty';
        html += '<input type="text" name="data[SaleQuotationItem][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Qty" value="1"required>';
        html += '</div>';

        html += '<div class="col-sm-2">UOM';
        html += '<select name="data[SaleQuotationItem][general_unit_id][]" id="unit'+station+'" class="form-control"required>'; 
        html += '<option value="">UOM</option>';
        html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
        html += '</select>' 
        html += '</div>'; 

        
        html += '<div class="col-sm-2">Margin %';
        html += '<input name="data[SaleQuotationItem][sale_margin][]" type="text" id="margin'+station+'" class="form-control number" placeholder="Margin %/Unit"required>'; 
        html += '</div>';
        html += '<div class="col-sm-2">Margin Value';
        html += '<input name="data[SaleQuotationItem][sale_margin_value][]" type="text" id="margin_value'+station+'" class="form-control number" placeholder="Margin Value"required>'; 
        html += '</div>';

         html += '<div class="col-sm-2">Price';
        html += '<input name="data[SaleQuotationItem][sale_price][]" type="text" id="sale_price'+station+'" class="form-control number" placeholder="Unit price">'; 
        html += '</div>'; 
        html += '<div class="col-sm-2">Subtotal';
        html += '<input type="text" name="data[SaleQuotationItem][sub_total][]" id="sub_total'+station+'" class="form-control number" placeholder="Subtotal">'; 
        html += '</div>';

        html += '<div class="col-sm-1">Discount';
        html += '<input type="text" name="data[SaleQuotationItem][discount][]" id="discount'+station+'" class="form-control number" placeholder="Discount" value="0">'; 
        html += '</div>';

        html += '<div class="col-sm-2">Total';
        html += '<input type="text" name="data[SaleQuotationItem][total_price][]" id="total_price'+station+'" class="form-control number" placeholder="Total">'; 
        html += '</div>';
        html += '<div class="col-sm-1">&nbsp; &nbsp;';
        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div><hr/></div>';    
        $("#loadMoreBom").append(html);  
        findItem(station, $(this).val());
        calcPrice(station, $(this).val());  
        station++; 
    }); 

   

    $("#findTender").autocomplete({
        source: function (request, response){
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'sale_tenders/ajaxfindtender',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findTender').val(),                                                    
                success: function (data) {
                    console.log(data);
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.tender_no,
                            tender_no: item.tender_no,
                            title: item.title,
                            customer: item.customer,
                            customer_id:item.customer_id,
                            closing_date: item.closing_date
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {
            $("#tenderId").val(ui.item.id);//Put Id in a hidden field
            $("#customer_id").val(ui.item.customer_id);
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.tender_no + "<br><small>" + item.customer + " - "+ item.title +"</small><br>" +  "</div>" ).appendTo( ul );
    }; 
});
</script>
<?php $this->end(); ?>