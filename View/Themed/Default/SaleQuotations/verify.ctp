
<?php if($group == 17) { ?> 
	<?php echo $this->Html->link('Waiting Approval', array('action' => 'verify/3'), array('class' => 'btn btn-default btn-sm')); ?>
<?php } if($group == 19) { ?> 
	<?php echo $this->Html->link('Waiting Approval', array('action' => 'verify/5'), array('class' => 'btn btn-default btn-sm')); ?>
<?php } ?> 

<?php echo $this->Html->link('Rejected', array('action' => 'verify/7'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Approved', array('action' => 'verify/6'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Quotation Submitted', array('action' => 'verify/9'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Awarded', array('action' => 'verify/10'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Failed', array('action' => 'verify/11'), array('class' => 'btn btn-default btn-sm')); ?> 
<?php 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Planning';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting Approval';
  } elseif($status == 4) {
    $data = 'Approved';
  } elseif($status == 5) {
    $data = 'Waiting Approval';
  } elseif($status == 6) {
    $data = 'Approved';
  } elseif($status == 7) {
    $data = 'Rejected';
  } elseif($status == 8) {
    $data = 'Rejected';
  } elseif($status == 9) {
    $data = 'Quotation Submitted';
  } elseif($status == 10) {
    $data = 'Awarded';
  } elseif($status == 11) {
    $data = 'Failed';
  } 
  return $data;
}
?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Quotation List</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="table-responsive"> 
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_tender_id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th>   
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleQuotations as $saleQuotation): ?>
	<tr>
		<td><?php echo h($saleQuotation['SaleQuotation']['id']); ?>&nbsp;</td>
		<td>
			<?php echo h($saleQuotation['SaleQuotation']['name']); ?>
		</td> 

		<td>
			<?php echo $this->Html->link(substr($saleQuotation['SaleTender']['title'], 0, 50), array('controller' => 'saletenders', 'action' => 'view', $saleQuotation['SaleTender']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleQuotation['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleQuotation['Customer']['id'])); ?>
		</td> 
		<td><?php echo status($saleQuotation['SaleQuotation']['status']); ?></td>  
		<td class="actions"> 
			<?php echo $this->Html->link(__('View'), array('action' => 'verification', $saleQuotation['SaleQuotation']['id'])); ?>

			<?php 
			$role = $this->Session->read('Auth.User.group_id'); 
			$user_id = $this->Session->read('Auth.User.id'); 
			?>

			<?php if($role == 1) { ?> 
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleQuotation['SaleQuotation']['id'])); ?> 

			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleQuotation['SaleQuotation']['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotation['SaleQuotation']['id'])); ?>
			<?php } else { ?>
			<?php if($saleQuotation['SaleQuotation']['status'] == 0) { ?> 
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleQuotation['SaleQuotation']['id'])); ?>
			<?php } ?>
			<?php } ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
  </div>
    </div>
  </div> 
</div>