<?php echo $this->Html->link('Waiting HOD Approval', array('action' => 'approval/3'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Waiting GM Verification', array('action' => 'approval/5'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Rejected', array('action' => 'approval/7'), array('class' => 'btn btn-default btn-sm')); ?>
<?php echo $this->Html->link('Approved', array('action' => 'approval/6'), array('class' => 'btn btn-success btn-sm')); ?>

<?php 
function status($status) {
  if($status == 0) {
    $data = 'Draft';
  } elseif($status == 1) {
    $data = 'Waiting Planning';
  } elseif($status == 2) {
    $data = 'Costing Added';
  } elseif($status == 3) {
    $data = 'Waiting Approval';
  } elseif($status == 4) {
    $data = 'Approved';
  } elseif($status == 5) {
    $data = 'Waiting Approval';
  } elseif($status == 6) {
    $data = 'Approved';
  } elseif($status == 7) {
    $data = 'Rejected';
  } elseif($status == 8) {
    $data = 'Rejected';
  } elseif($status == 9) {
    $data = 'Quotation Submitted';
  } elseif($status == 10) {
    $data = 'Awarded';
  } elseif($status == 11) {
    $data = 'Failed';
  } 
  return $data;
}
?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Quotation List</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('SaleQuotation', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Quotation No', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('customer', array('placeholder' => 'Customer', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>
		<?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden')); ?>  
		</td>
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>

<div class="table-responsive"> 
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name', 'Quotation No'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_tender_id', 'Tender'); ?></th>
			<th><?php echo $this->Paginator->sort('SaleTender.closing_date', 'Closing Date'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id', 'Customer'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th>   
			<th><?php echo $this->Paginator->sort('user_approval', 'User Approval'); ?></th>  
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleQuotations as $saleQuotation): ?>
	<tr> 
		<td>
			<?php echo h($saleQuotation['SaleQuotation']['name']); ?>
		</td> 

		<td>
			<?php echo $this->Html->link($saleQuotation['SaleTender']['tender_no'], array('controller' => 'sale_tenders', 'action' => 'view', $saleQuotation['SaleTender']['id'])); ?>
		</td>
		<td>
			<?php echo $saleQuotation['SaleTender']['closing_date']; ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleQuotation['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleQuotation['Customer']['id'])); ?>
		</td> 
		<td><?php echo status($saleQuotation['SaleQuotation']['status']); ?></td>  
		<td><?php echo $saleQuotation['QuotationApproval']['username']; ?></td> 
		<td class="actions"> 
			<?php echo $this->Html->link(__('View'), array('action' => 'verification', $saleQuotation['SaleQuotation']['id'])); ?>

			<?php 
			$role = $this->Session->read('Auth.User.group_id'); 
			$user_id = $this->Session->read('Auth.User.id'); 
			?>
            <!-- temp change role -->
			<?php if($role == 9) { ?> 
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleQuotation['SaleQuotation']['id'])); ?> 

			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleQuotation['SaleQuotation']['id']), array(), __('Are you sure you want to delete # %s?', $saleQuotation['SaleQuotation']['id'])); ?>
			<?php } else { ?>
			<?php if($saleQuotation['SaleQuotation']['status'] == 0) { ?> 
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleQuotation['SaleQuotation']['id'])); ?>
			<?php } ?>
			<?php } ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
  </div>
    </div>
  </div> 
</div>