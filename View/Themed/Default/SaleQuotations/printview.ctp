<div class="book">
    <div class="page"> 
          
      <div class="x_content">
        
        <table style="font-size: 11px; width: 100%;"> 
        <?php if($saleQuotation['SaleQuotation']['show_tender_no'] == 1) { ?>
          <tr>
            <td style="width:120px"><p>Tender No</p></td>
            <td><p>: <?php echo h($saleQuotation['SaleTender']['tender_no']); ?></p></td>
          </tr>
        <?php } ?>  
          <tr>
            <td style="width:120px"><p>Our Ref</p></td>
            <td><p>: <?php echo h($saleQuotation['SaleQuotation']['name']); ?></p></td>
          </tr>
          <tr>
            <td style="width:120px"><p>Date</p></td>
            <td><p>: <?php echo date("jS F Y", strtotime($saleQuotation['SaleQuotation']['date']));; ?></p></td>
          </tr>
        </table>

        <table style="margin-top: 10px; font-size: 11px; width: 100%;">
        <tr>
        <td> 
        <p><b><?php echo $saleQuotation['Customer']['name']; ?></b></p>
        <div class="p-no-space">
          <p><span class="wrapper"><?php echo h($saleQuotation['Customer']['address']); ?></span> 
          <br><?php echo h($saleQuotation['Customer']['postcode']); ?>, <?php echo h($saleQuotation['Customer']['city']); ?>
          <br> <?php echo h($customer['State']['name']); ?>
          <br> <?php echo h($customer['Country']['name']); ?></p>
        </div>
        </td>
        
        </tr>
        </table> 
        
        <?php if(!empty($saleQuotation['SaleQuotation']['attention'])) { ?>  
        <table style="margin-top: 10px; font-size: 11px; width: 100%;">
          <tr>
            <td style="width:80px"><p>Attention</p></td>
            <td><p>: <?php echo h($saleQuotation['SaleQuotation']['attention']); ?></p></td>
          </tr>
           
        </table>
        <?php } ?>

        <h4 class="text-center" style="text-decoration: underline;"><b><?php echo h($saleQuotation['SaleTender']['title']); ?></b></h4>
        <?php if(!empty($saleQuotation['SaleQuotation']['header'])) { ?> 

        <hr/> 
        <p><?php echo h($saleQuotation['SaleQuotation']['header']); ?></p>

        <?php } ?>

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table">
 

              <table class="table table-bordered" style="font-size: 12px; width: 100%;">
                 
                  <tr>
                    <td><b>No</b></td>
                    <td><b>Description</b></td> 
                    <td><b>Qty</b></td> 
                    <td class="text-center"><b>Price Per Unit<br/>(RM)<br/>Ex-factory</b></td> 
                    <td width="140px" class="text-center"><b>Total Price<br/>(RM)<br/>Ex-factory</b></td> 
                  </tr>
                
                <?php $i = 1; $total = 0; ?>
                <?php 
                $all_items = '';
                foreach ($items as $item) { ?>

                <?php $all_items .= '<blockquote style="margin: 0 0 0 40px; border: none; padding: 0px;"><div><font size="3">•<span class="Apple-tab-span" style="white-space:pre">  </span>'.$item['InventoryItem']['name']. '<br/><small>'.$item['InventoryItem']['note'] . '</small></font></div></blockquote>'; ?>

                  <?php 
                   
                  if($item['SaleQuotationItem']['sale_price'] == 0) {
                    $price_quantity = $item['SaleQuotationItem']['unit_price'] * $item['SaleQuotationItem']['quantity'];
                  } else {
                    $price_quantity = $item['SaleQuotationItem']['unit_price'] * $item['SaleQuotationItem']['quantity'];
                  }

                  $total += $price_quantity;
                  
                  ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td>
                    <?php echo h($item['InventoryItem']['name']); ?><br/>
                    <small><?php echo h($item['InventoryItem']['code']); ?></small><br/>
                    <small><?php echo h($item['InventoryItem']['note']); ?></small>

                    </td>
                    
                    <td><?php echo h($item['SaleQuotationItem']['quantity']); ?> <?php echo h($item['GeneralUnit']['name']); ?></td>
                     
                    <td class="align-right"><?php echo _n2($item['SaleQuotationItem']['unit_price']); ?></td>
                    
                    <td width="140px" class="align-right"><?php echo _n2($price_quantity); ?></td> 
                     
                  </tr>
                <?php $i++; ?>
                <?php }  
                ?> 

                  <tr>
                    <td colspan="2" class="align-right"><b>TOTAL</b></td> 
                    <td colspan="3" class="align-right"><b><?php echo _n2($total); ?></b></td> 
                  </tr>
                  <?php 
                  $tax = ($total / 100) * Configure::read('Site.gst_amount');
                  $grand_total = $tax + $total;
                  ?>
                  <tr>
                    <td colspan="2" class="align-right"><b>GST <?php echo Configure::read('Site.gst_amount'); ?>%</b></td> 
                    <td colspan="3" class="align-right"><b> <?php echo _n2($tax); ?></b></td> 
                  </tr>
                  <tr>
                    <td colspan="2" class="align-right"><b>GRAND TOTAL</b></td> 
                    <td colspan="3" class="align-right"><b><?php echo _n2($grand_total); ?></b></td> 
                  </tr>
              </table>

             
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row --> 
      </div>
      
       <?php 

                $array = array(
                    'LISTOFPRODUCT' => $all_items,
                    'OFFERNOTINCLUDES' => $saleQuotation['SaleQuotation']['notinclude'], 
                    'DATETODAY' => $saleQuotation['SaleQuotation']['date'], 
                    'VALIDWITHIN' => $saleQuotation['SaleQuotation']['validwithin'],   
                    'PAIDWITHIN' => $saleQuotation['SaleQuotation']['paidwithin'],  
                    'WARRANTYPERIOD' => $saleQuotation['SaleQuotation']['warrantyperiod'],  
                    'ORWARRANTYPERIOD' => $saleQuotation['SaleQuotation']['orwarrantyperiod'],  
                    'TESTEDACCORDINGTO' => $saleQuotation['SaleQuotation']['testedaccordingto'], 
                    'NOTIFYPURCHASER' => $saleQuotation['SaleQuotation']['notifypurchaser'],  
                    'TERMINATIONDAY' => $saleQuotation['SaleQuotation']['terminationday'],  
                    'DAYOFACCEPTANCE' => $saleQuotation['SaleQuotation']['dayofacceptance'],
                    'BREAKONTHISLINE' => '<div class="page-break">&nbsp;</div>'
                  );
                echo strtr($template['PrintTemplate']['body'], $array); 
              ?>

  </div>
</div>  