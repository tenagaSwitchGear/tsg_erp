<?php $group_id = $this->Session->read('Auth.User.group_id'); ?>
<?php echo $this->Html->link(__('Active'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Draft'), array('action' => 'index/0'), array('class' => 'btn btn-warning btn-sm')); ?>
<?php 
if($group_id == 1) { 
	echo $this->Html->link(__('Add Sale Order (STS / Project)'), array('action' => 'sts_project_add'), array('class' => 'btn btn-primary btn-sm'));
	echo $this->Html->link(__('Add Sale Order'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); 
} else if($group_id == 9) {
	echo $this->Html->link(__('Add Sale Order'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); 
} else {
	echo $this->Html->link(__('Add Sale Order'), array('action' => 'sts_project_add'), array('class' => 'btn btn-success btn-sm')); 
}
?> 

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Sales Order</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

         <?php echo $this->Form->create('SaleOrder', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('customer_id', array('type' => 'text', 'placeholder' => 'Customer', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 


<div class="saleOrders table-responsive">
	<h2><?php echo __('Sale Orders'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name', 'SO No'); ?></th> 
			<th><?php echo $this->Paginator->sort('SaleJobChild.station_name', 'Job No'); ?></th>   
			<th>Po No</th>  
			<th><?php echo $this->Paginator->sort('date', 'PO Date'); ?></th>  
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleOrders as $saleOrder): ?>
	<tr> 
		<td>
			<?php echo h($saleOrder['SaleOrder']['name']); ?>
		</td> 
		<td><?php echo h($saleOrder['SaleJobChild']['station_name']); ?> - <?php echo h($saleOrder['SaleJobChild']['name']); ?></td>
		<td><?php echo h($saleOrder['SaleOrder']['customer_purchase_order_no']); ?>&nbsp;</td>
	   
		<td><?php echo h($saleOrder['SaleOrder']['date']); ?>&nbsp;</td>  
		<td><?php echo status($saleOrder['SaleOrder']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $saleOrder['SaleOrder']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $saleOrder['SaleOrder']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?> 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
      
      </div>
    </div>
  </div> 
</div>


<?php

function status($status) {
	if($status == 1) {
		return 'Active';
	} else {
		return 'Draft';
	}
}
?>