<?php echo $this->Html->link(__('Sales Order'), array('action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleOrder['SaleOrder']['id']), array('class' => 'btn btn-warning btn-sm')); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Sale Order No: <?php echo $saleOrder['SaleOrder']['name']; ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>


    
<div class="inventoryStocks view-data">
<h2><?php echo __('Sales Order'); ?></h2>
	<dl>
		 
		<dt><?php echo __('Sales Order No'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Station Name'); ?></dt>
		<dd>
			<?php echo $this->Html->link(h($saleOrder['SaleJobChild']['name']), array('controller' => 'sale_jobs', 'action' => 'view', $saleOrder['SaleOrder']['sale_job_id'])); ?>
			&nbsp;
		</dd> 

		<dt><?php echo __('Station Code'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrder['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleOrder['SaleOrder']['sale_job_id'])); ?>
			&nbsp;
		</dd> 

		<dt><?php echo __('Job No'); ?></dt>
		<dd>
			<?php echo $this->Html->link(h($saleOrder['SaleJob']['name']), array('controller' => 'sale_jobs', 'action' => 'view', $saleOrder['SaleOrder']['sale_job_id'])); ?>
			&nbsp;
		</dd> 

		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['created']); ?> 
			&nbsp;
		</dd>
		  
			<dt><?php echo __('Customer PO No'); ?></dt>
			<dd>
				<?php echo h($saleOrder['SaleOrder']['customer_purchase_order_no']); ?> 
				&nbsp;
			</dd>  
		
		<dt><?php echo __('Added By'); ?></dt>
		<dd>
			<?php echo h($saleOrder['User']['username']); ?> 
			&nbsp;
		</dd>

	</dl>
</div>


 
 
	<table class="table table-bordered">
	<thead>
	<tr>
		<th>No</th>
		<th>Name</th>
		<th>Type</th>
		<th>Qty</th> 
		<th>Unit Price</th> 
		<th>Discount</th> 
		<th>Tax</th> 
		<th>Total</th>  
	</tr>
	</thead>
	<tbody>
	<?php $i = 1;
	$total = 0; ?>
	<?php foreach ($items as $item): ?>
	<?php $total += $item['SaleOrderItem']['total_price']; ?> 
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo h($item['InventoryItem']['name']); ?></td>
		<td><?php
			if(!empty($item['Bom']['code'])) {
				echo h($item['Bom']['code']);
			} else {
				echo 'Purchase';
			}

		?></td>
		<td><?php echo h($item['SaleOrderItem']['quantity']); ?> </td> 
		<td><?php echo number_format($item['SaleOrderItem']['unit_price'], 2); ?> </td> 
		<td><?php echo number_format($item['SaleOrderItem']['discount'], 2); ?> </td> 
		<td><?php echo number_format($item['SaleOrderItem']['tax'], 2); ?> </td> 
		<td><?php echo number_format($item['SaleOrderItem']['total_price'], 2); ?> </td>  
	</tr>
	<?php $i += 1; endforeach; ?>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td> 
		<td></td> 
		<td></td> 
		<td></td> 
		<td><b><?php echo number_format($total, 2); ?></b></td>  
	</tr>
	</tbody>
	</table>
  
      </div>
    </div>
  </div> 
</div>



