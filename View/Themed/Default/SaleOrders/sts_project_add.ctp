<?php echo $this->Html->link(__('Sale Orders'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add New Sale Order</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
      <div class="form-group">
 

      </div>

        <?php echo $this->Session->flash(); ?>
 		<div class="saleJobs form">
		<?php echo $this->Form->create('SaleOrder', array('class' => 'form-horizontal', 'id' => 'form')); ?> 

		<!--<div class="form-group">
			<label class="col-sm-3">Enter Quotation No</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('quotation', array('id' => 'quotation', 'class' => 'form-control', 'label' => false)); ?> 
				
			</div>
		</div> -->
 
		<div class="form-group">
			<label class="col-sm-3">Enter Job No *</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('SaleJobChild.station_name', array('id' => 'job', 'class' => 'form-control', 'label' => false)); ?> 
				<small>Job number must have Quotation to copy budget.</small>
				<?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden')); ?>  
				<?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden', 'value' => 0)); ?>  
				<?php echo $this->Form->input('sale_quotation_id', array('id' => 'sale_quotation_id', 'type' => 'hidden', 'value' => 0)); ?>
				<?php echo $this->Form->input('sale_tender_id', array('id' => 'sale_tender_id', 'type' => 'hidden', 'value' => 0)); ?> 
				<?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden', 'value' => 0)); ?> 
			</div>
		</div> 

		<div class="form-group">
			<label class="col-sm-3">Customer PO No *</label>
			<div class="col-sm-9">
		<?php echo $this->Form->input('customer_purchase_order_no', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>

		

		<div class="form-group">
			<label class="col-sm-3">Date *</label>
			<div class="col-sm-9">
		<?php echo $this->Form->input('date', array('value' => date('Y-m-d'), 'id' => 'dateonly', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">Delivery Date</label>
			<div class="col-sm-9">
		<?php echo $this->Form->input('date_delivery', array('id' => 'dateonly_2', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>		 

		<div class="form-group">
			<label class="col-sm-3">Payment Term *</label>
			<div class="col-sm-9">
			<?php //$status = array(0 => 'Save As Draft', 1 => 'Active'); ?>
			<?php echo $this->Form->input('term_of_payment_id', array('options'=>$terms, 'class' => 'form-control', 'label' => false)); ?> 
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">Category</label>
			<div class="col-sm-9">
			<?php //$status = array(0 => 'Save As Draft', 1 => 'Active'); ?>
			<?php echo $this->Form->input('sale_order_category_id', array('options'=>$categories, 'class' => 'form-control', 'label' => false, 'id' => 'category_id')); ?> 
			<small>By choosing Service or Project, it will copy budget from Quotation (Item cost X qty). Standard product will copy Budget from PPC Costing.</small>
			</div>
		</div>
		 
		<?php echo $this->Form->input('status', array('value' => 1, 'type' => 'hidden')); ?>  

		<!-- items-->
		<div class="form-group" id="note">
             
        </div>
         <div class="row">  
		<div class="col-sm-4">
		Item</div>

		<div class="col-sm-1">
		Qty
		</div>

		<div class="col-sm-1">
		UOM</div>

		<div class="col-sm-2">Price (RM)</div> 
		<div class="col-sm-1">Dis.Type</div>  
		 <div class="col-sm-1">Dis. Value</div>  
		<div class="col-sm-1">Tax</div>   

		<div class="col-sm-1">
		Action
		</div>
		</div>
		<div class="container" id="loadMoreBom"></div>  
        <div class="form-group">
            <div class="col-sm-12"><a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Item</a></div>
        </div>  

        <div class="form-group">
			<label class="col-sm-3">&nbsp;</label>
			<div class="col-sm-9">   
				<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
			</div>
		</div>

		<?php echo $this->Form->end(); ?>
		</div>
      
      </div>
    </div>
  </div> 
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
	console.log(search);
	$('#findProduct'+row).autocomplete({ 
	    source: function (request, response) { 
	    	if($('#sale_job_child_id').val() == '') {
	    		alert('Please enter valid Job Number');
	    	}
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'inventory_items/ajaxfinditem',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#findProduct'+row).val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.code,
		                    name : item.name,
		                    price: item.price,
		                    code: item.code,
		                    type: item.type,
		                    note: item.note,
		                    unit: item.unit,
		                    bom_id: item.bom['id'],
		                    bom_name: item.bom['code']
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {  
	        $('#productId'+row).val( ui.item.id ); 
	        $('#price'+row).val( ui.item.price ); 
	        $('#total'+row).val( ui.item.price ); 
	        $('#bom_id'+row).val( ui.item.bom_id ); 
	        $('#bom_name'+row).html( ui.item.bom_name );
	        $('#unit'+row).val(ui.item.unit);
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.name + "<br><small>" + item.code + "</small><br/><small>" + item.type + "</small><br><small>" + item.note +  "</small></div>" ).appendTo( ul );
    };
}
  
 
$(function() { 
	$('#category_id').on('change', function() {
		var category = $(this).val();
		if(category == 2) {

		}
	});

	$('#form').on('keyup keypress', function(e) {
	  var keyCode = e.keyCode || e.which;
	  if (keyCode === 13) { 
	    e.preventDefault();
	    return false;
	  }
	});

	$('#job').autocomplete({ 
	    source: function (request, response){ 
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_jobs/ajaxfindjob',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#job').val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    sale_job_child_id: item.sale_job_child_id,
		                    value: item.name,
		                    customer: item.customer,
		                    station: item.station,
		                    customer: item.customer,
		                    customer_id: item.customer_id,
		                    sale_tender_id: item.sale_tender_id,
		                    sale_quotation_id: item.sale_quotation_id, 
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {   
	        $('#sale_job_id').val(ui.item.id); 
	        $('#sale_job_child_id').val(ui.item.sale_job_child_id);
	        $('#customer_id').val(ui.item.customer_id);
	        $('#sale_quotation_id').val(ui.item.sale_quotation_id);
	        $('#sale_tender_id').val(ui.item.sale_tender_id);  
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.station + "</small><br><small>Customer: " + item.customer + "</small>" +  "</div>" ).appendTo( ul );
    };

	

	var station = 1;
	$('#addItem').click(function() {
		var html = '<div id="removeBom'+station+'">'; 
	    html += '<div class="row">';  
	    html += '<div class="col-sm-4" id="autoComplete">';
	    html += '<input type="text" name="find" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="data[SaleOrderItem][inventory_item_id][]" id="productId'+station+'">';
	    html += '<input type="hidden" name="data[SaleOrderItem][bom_id][]" id="bom_id'+station+'"></div>';
	 
	    html += '<div class="col-sm-1">';
	    html += '<input type="text" name="data[SaleOrderItem][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
	    html += '</div>';

	    html += '<div class="col-sm-1">';
	    html += '<select name="data[SaleOrderItem][general_unit_id][]" id="unit'+station+'" class="form-control"required>'; 
	    html += '<option value="">-Unit-</option>';
	    html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
	    html += '</select></div>';

	    html += '<div class="col-sm-2"><input type="text" name="data[SaleOrderItem][unit_price][]" class="form-control" id="price'+station+'" placeholder="Price/Unit" required></div>'; 
	    html += '<div class="col-sm-1"><select name="data[SaleOrderItem][discount_type][]" value="" class="form-control">';
	    html += '<option value="1">Fix Amount</option><option value="2">%</option>';  
	    html += '</select></div>';  
	     html += '<div class="col-sm-1"><input type="text" name="data[SaleOrderItem][discount][]" value="0" class="form-control" placeholder="Discount"></div>';  
	    html += '<div class="col-sm-1"><input type="text" value="<?php echo Configure::read('Site.gst_amount'); ?>" name="data[SaleOrderItem][tax][]" value="" class="form-control" placeholder="Tax"></div>';  
	  

	    html += '<div class="col-sm-1">';
	    html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div>';

	    html += '<div class="col-sm-12">';
	    html += '<div id="bom_name'+station+'"></div>';
	    html += '</div>';

	    html += '</div></div>';    
	    $("#loadMoreBom").append(html);  
	    
		findItem(station, $(this).val());  
		station++; 
	});  
});
</script>
<?php $this->end(); ?>