<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Generate Sales Order</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

        <div class="row">
	        <div class="col-sm-6">
	        <h2>From</h2>
	        <h2><?php echo Configure::read('Site.company_name'); ?></h2>
	        <p class="wrapper"><?php echo Configure::read('Site.address'); ?></p>
	        </div>
	        <div class="col-sm-6">
	        	<div class="form-group">
	        	<label class="col-sm-2">Date:</label>
	        	<div class="col-sm-9"> 
				<?php echo $this->Form->input('date', array('class' => 'form-control', 'label' => false)); ?> 
				</div>
				</div>

				<div class="form-group">
	        	<label class="col-sm-2">PO No:</label>
	        	<div class="col-sm-9"> 
				<?php echo $this->Form->input('customer_purchase_order_no', array('class' => 'form-control', 'label' => false)); ?> 
				</div>
				</div>

				<div class="form-group">
	        	<label class="col-sm-2">PO No:</label>
	        	<div class="col-sm-9"> 
				<?php echo $this->Form->input('customer_purchase_order_no', array('class' => 'form-control', 'label' => false)); ?> 
				</div>
				</div>

	        </div>
        </div>

        <div class="row">
	        <div class="col-sm-6">
		        <h2>To</h2>
		        <h2><?php echo $job['Customer']['name']; ?></h2>
		        <p class="wrapper"><?php echo $job['Customer']['address']; ?></p>
		        <p><?php echo $job['Customer']['postcode']; ?> <?php echo $job['Customer']['city']; ?><br/>
		        <?php echo $customer['State']['name']; ?></p>
	        </div>
	        <div class="col-sm-6">
	        	<h2>Ship To</h2>
		        <h2><?php echo $job['Customer']['name']; ?></h2>
		        <p class="wrapper"><?php echo $job['Customer']['address']; ?></p>
		        <p><?php echo $job['Customer']['postcode']; ?> <?php echo $job['Customer']['city']; ?><br/>
		        <?php echo $customer['State']['name']; ?></p>
	        </div>
        </div>
 		
 		<div class="table-responsive">
 		<table class="table">
 		<tr>
 			<th>No</th>
 			<th>Item</th>
 			<th>Qty</th>
 			<th>Unit Price</th>
 			<th>Discount</th>
 			<th>Tax</th>
 			<th>Total</th>
		</tr>
		<?php $i = 1; ?>
 		<?php foreach ($boms as $bom) { ?>
 			 <tr>
	 			<td><?php echo $i; ?></td>
	 			<td><?php echo $bom['ProjectBom']['name']; ?> (<?php echo $bom['ProjectBom']['code']; ?>)</td>
	 			<td><?php echo $bom['ProjectBom']['quantity']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['unit_price']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['discount']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['tax']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['total_price']; ?></td>
			</tr>
			<?php $i ++; ?>
 		<?php } ?>

 		<?php foreach ($items as $item) { ?>
 			 <tr>
	 			<td><?php echo $i; ?></td>
	 			<td><?php echo $item['SaleJobItem']['name']; ?> (<?php echo $item['SaleJobItem']['code']; ?>)</td>
	 			<td><?php echo $bom['ProjectBom']['quantity']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['unit_price']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['discount']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['tax']; ?></td>
	 			<td><?php echo $bom['ProjectBom']['total_price']; ?></td>
			</tr>
			<?php $i ++; ?>
 		<?php } ?>

 		</table>
 		</div>
 
      	<?php echo $this->Form->create('SaleOrder', array('class'=>'form-horizontal')); ?> 


      	<div class="form-group"> 
        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?> 
        </div>	  
		<?php $this->Form->end(); ?>
      </div> 
    </div>
  </div> 
</div>
