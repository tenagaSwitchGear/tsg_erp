<?php echo $this->Html->link(__('Add Sale Order'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>

<?php echo $this->Html->link(__('Active'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Draft'), array('action' => 'index/0'), array('class' => 'btn btn-warning btn-sm')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Sales Order</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

         <?php echo $this->Form->create('SaleOrder', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'SO No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('job', array('type' => 'text', 'placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly_2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 


<div class="saleOrders table-responsive"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name', 'SO No'); ?></th>   
			<th><?php echo $this->Paginator->sort('SaleJobChild.name', 'Job'); ?></th>  
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th>Customer</th>
			<th>Status</th>
			<th class="align-right">PO Amount</th>
			<th class="align-right">Delivered</th>
			<th class="align-right">Balance</th>
			

	</tr>
	</thead>
	<tbody>
	<?php 
	$sum_price = 0;
	$sum_delivered = 0;
	$sum_balance = 0;
	foreach ($saleOrders as $saleOrder): ?>
	<?php 
		$total_price = $saleOrder['Sum']['total_price'];
		$total_delivered = $saleOrder['SumDelivered']['total_price'];
		$balance = $total_price - $total_delivered;

		$sum_price += $total_price;
		$sum_delivered += $total_delivered;
		$sum_balance += $balance;
	?>
	<tr> 
		<td><?php echo h($saleOrder['SaleOrder']['name']); ?></td>   
		<td><?php echo h($saleOrder['SaleJobChild']['name']); ?>&nbsp;</td>  
		<td><?php echo h($saleOrder['SaleOrder']['date']); ?>&nbsp;</td>
		<td><?php echo h($saleOrder['Customer']['name']); ?>&nbsp;</td>
		<td><?php echo h($saleOrder['SaleOrder']['progress']); ?></td>
		<td class="align-right"><?php echo _n2($total_price); ?>&nbsp;</td>
		<td class="align-right"><?php echo _n2($total_delivered); ?>&nbsp;</td>
		<td class="align-right"><?php echo _n2($balance); ?>&nbsp;</td> 
		
	</tr>
<?php endforeach; ?>
	<tr> 
		<td><b>Total</b></td>   
		<td> </td>  
		<td> </td>
		<td> </td>
		<td> </td>
		<td class="align-right"><b><?php echo _n2($sum_price); ?></b></td>
		<td class="align-right"><b><?php echo _n2($sum_delivered); ?></b></td>
		<td class="align-right"><b><?php echo _n2($sum_balance); ?></b></td> 
		
	</tr>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>  
      </div>
    </div>
  </div> 
</div>


<?php

function status($status) {
	if($status == 1) {
		return 'Active';
	} else {
		return 'Draft';
	}
}
?>