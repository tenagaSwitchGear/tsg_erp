<?php echo $this->Html->link(__('Racks'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Add Rack'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>  
 
<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Rack</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryRacks form">
<?php echo $this->Form->create('InventoryRack', array('class' => 'form-horizontal')); ?> 
<div class="form-group"> 
	<label class="col-xs-3">Store</label>
	<div class="col-xs-9">
	<?php echo $this->Form->input('inventory_store_id', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>

	<div class="form-group"> 
	<label class="col-xs-3">Rack Name (Rack-Level-Bin)</label>
	<div class="col-xs-9">
	<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Enter Rack Name')); ?>
	</div>
	</div> 
<div class="form-group"> 
    	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
    </div>	  
	<?php $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>