
<?php echo $this->Html->link(__('Racks'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Add Rack'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>  

<?php echo $this->Html->link(__('Set Default Rack'), array('action' => 'setdefaultrackindex'), array('class' => 'btn btn-info btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Racks</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
 
    <?php echo $this->Form->create('InventoryRack', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Rack Name', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_store_id', array('options' => $stores, 'empty' => 'All Store', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>  
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 

<div class="inventoryRacks index"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
		<th><?php echo $this->Paginator->sort('name', 'Rack Name'); ?></th>
		<th><?php echo $this->Paginator->sort('InventoryStore.name', 'Store'); ?></th>
		<th><?php echo $this->Paginator->sort('InventoryLocation.name', 'Warehouse'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryRacks as $inventoryRack): ?>
	<tr> 
		<td><?php echo h($inventoryRack['InventoryRack']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryRack['InventoryStore']['name'], array('controller' => 'inventory_stores', 'action' => 'view', $inventoryRack['InventoryStore']['id'])); ?>
		</td>
		<td><?php echo h($inventoryRack['InventoryStore']['InventoryLocation']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryRack['InventoryRack']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryRack['InventoryRack']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryRack['InventoryRack']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryRack['InventoryRack']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
</div>
</div>
</div>
</div>