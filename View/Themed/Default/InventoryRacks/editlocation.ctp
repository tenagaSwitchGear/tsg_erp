<?php echo $this->Html->link(__('Back'), array('action' => 'setdefaultrackindex'), array('class' => 'btn btn-primary btn-sm')); ?>

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Edit Item Default Location'); ?></h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
				<div class="inventoryItems form">
				<?php echo $this->Form->create('InventoryItem', array('class'=>'form-horizontal', 'type' => 'file')); ?>
					<fieldset>
					<?php echo $this->Form->input('id'); ?>
						<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Code *</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("code", array("class"=> "form-control", "placeholder" => 'Code', "label"=> false, "disabled" => true)); ?>
						</div> 
					</div>
					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Name *</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("name", array("class"=> "form-control", "placeholder" => 'Name', "label"=> false, "disabled" => true)); ?>
						</div> 
					</div> 

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Warehouse</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("inventory_location_id", array("options" => $locations, "class"=> "form-control", "label"=> false, "empty" => "-Select Warehouse-")); ?>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Store</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("inventory_store_id", array("options" => $stores, "class"=> "form-control", "label"=> false, "empty" => "-Select Store-")); ?>
						</div> 
					</div>

					<div class="form-group">
						<label class="col-sm-3" style="padding-top: 8px">Rack</label>
						<div class="col-sm-9">
						<?php echo $this->Form->input("inventory_rack_id", array("options" => $rack, "class"=> "form-control", "label"=> false, "empty" => "-Select Rack-")); ?>
						</div> 
					</div> 
 
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>