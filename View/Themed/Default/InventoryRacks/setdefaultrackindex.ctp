<?php echo $this->Html->link(__('Set Default Rack'), array('action' => 'setdefaultrackindex'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Rack Setting'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('Store Setting'), array('controller' => 'inventory_stores', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('Warehouse Setting'), array('controller' => 'inventory_locations', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>

<div class="row"> 
  	<div class="col-xs-12"> 
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Item General Location Setting'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
	<?php echo $this->Form->create('InventoryItem', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Item name', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('code', array('type' => 'text', 'placeholder' => 'Code', 'class' => 'form-control', 'required' => false, 'id' => 'username', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_item_category_id', array('options' => $categories, 'class' => 'form-control', 'required' => false, 'empty' => '-All Category-', 'label' => false)); ?>  
		</td>
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
					<tr> 	 
							<th><?php echo $this->Paginator->sort('name', 'Items'); ?></th> 
							<th><?php echo $this->Paginator->sort('inventory_category_id', 'Category'); ?></th> 
							 
							<th><?php echo $this->Paginator->sort('InventoryRack.name', 'Rack'); ?></th> 
							<th><?php echo $this->Paginator->sort('InventoryStore.name', 'Store'); ?></th> 
							<th><?php echo $this->Paginator->sort('InventoryLocation.name', 'Warehouse'); ?></th> 
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php 

						foreach ($inventoryItems as $inventoryItem):  
					?>
					<tr>  
						<td><?php echo ($inventoryItem['InventoryItem']['code']); ?><br/>
						<small><?php echo ($inventoryItem['InventoryItem']['name']); ?></small></td>
						<td><?php echo $inventoryItem['InventoryItemCategory']['name']; ?></td>
						 

						 

						<td><?php echo $inventoryItem['InventoryRack']['name']; ?></td>
						
						<td><?php echo $inventoryItem['InventoryStore']['name']; ?></td>
						<td><?php echo $inventoryItem['InventoryLocation']['name']; ?></td>  
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'editlocation', $inventoryItem['InventoryItem']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?>
 
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
				</table>
			<p>
			<?php
				//echo $this->Paginator->counter(array(
				//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				//));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
 