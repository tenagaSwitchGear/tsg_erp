<?php echo $this->Html->link(__('Racks'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Add Rack'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>  

<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryRack['InventoryRack']['id']), array('class' => 'btn btn-warning btn-sm')); ?> 
<div class="row"> 
  	<div class="col-xs-12">
  		
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>View Rack</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryRacks view-data">
<h2><?php echo __('Inventory Rack'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryRack['InventoryRack']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Store'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryRack['InventoryStore']['name'], array('controller' => 'inventory_stores', 'action' => 'view', $inventoryRack['InventoryStore']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($inventoryRack['InventoryRack']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div> 
<div class="related">
	<h3><?php echo __('Item In This Rack'); ?></h3>
	<?php if (!empty($inventoryRack['InventoryItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr> 
		<th><?php echo __('Name'); ?></th>     
		<th><?php echo __('Inventory Location Id'); ?></th>
		<th><?php echo __('Inventory Store Id'); ?></th>
		<th><?php echo __('Inventory Rack Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($items as $inventoryItem): ?>
		<tr> 
			<td><?php echo $inventoryItem['InventoryItem']['name']; ?><br/>
			<small><?php echo $inventoryItem['InventoryItem']['code']; ?></small> </td>    
			<td><?php echo $inventoryItem['InventoryLocation']['name']; ?></td>
			<td><?php echo $inventoryItem['InventoryStore']['name']; ?></td>
			<td><?php echo $inventoryItem['InventoryRack']['name']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inventory_items', 'action' => 'view', $inventoryItem['InventoryItem']['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inventory_items', 'action' => 'edit', $inventoryItem['InventoryItem']['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inventory_items', 'action' => 'delete', $inventoryItem['InventoryItem']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryItem['InventoryItem']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	 
</div>
</div>
</div>
</div>