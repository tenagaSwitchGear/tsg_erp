<?php echo $this->Html->link(__('RFI Lists'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>

<?php echo $this->Html->link(__('Add RFI'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Requisition for Invoicing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create('SaleInvoice', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<tr>
	<td><?php echo $this->Form->input('station_name', array('placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
	</td>
	<td><?php echo $this->Form->input('so', array('placeholder' => 'Sale Order No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
	</td>
	<td><?php echo $this->Form->input('customer', array('type' => 'text', 'placeholder' => 'Customer Name', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
	</td> 
	 
	<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
	</tr>
</table>
<?php $this->end(); ?> 


<div class="saleInvoices index"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('Customer.name', 'Customer'); ?></th> 
			<th><?php echo $this->Paginator->sort('SaleJobChild.station_name', 'Job'); ?></th>
			<th><?php echo $this->Paginator->sort('SaleOrder.name', 'Sale Order'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th> 
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th><?php echo $this->Paginator->sort('fat_date'); ?></th>
			
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleInvoices as $saleInvoice): ?>
	<tr> 
		<td>
			<?php echo $this->Html->link($saleInvoice['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleInvoice['Customer']['id'])); ?>
		</td>  
		<td>
			<?php echo $this->Html->link($saleInvoice['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleInvoice['SaleJob']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleInvoice['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $saleInvoice['SaleOrder']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleInvoice['User']['username'], array('controller' => 'users', 'action' => 'view', $saleInvoice['User']['id'])); ?>
		</td>
		<td><?php echo h($saleInvoice['SaleInvoice']['status']); ?>&nbsp;</td> 
		<td><?php echo h($saleInvoice['SaleInvoice']['date']); ?>&nbsp;</td>
		<td><?php echo h($saleInvoice['SaleInvoice']['created']); ?>&nbsp;</td> 
		<td><?php echo h($saleInvoice['SaleInvoice']['fat_date']); ?>&nbsp;</td> 
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleInvoice['SaleInvoice']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleInvoice['SaleInvoice']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleInvoice['SaleInvoice']['id']), array(), __('Are you sure you want to delete # %s?', $saleInvoice['SaleInvoice']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
</div>
</div>
</div>
</div>