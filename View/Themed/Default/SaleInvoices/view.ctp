<?php echo $this->Html->link(__('Back To RFI Lists'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View Requisition for Invoicing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleInvoices view-data">
<h2><?php echo __('Job Info'); ?></h2>
	<dl> 
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoice['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleInvoice['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact Person'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['customer_contact_person']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['customer_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoice['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleInvoice['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Station'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoice['SaleJobChild']['name'], array('controller' => 'sale_job_children', 'action' => 'view', $saleInvoice['SaleJobChild']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoice['SaleOrder']['name'], array('controller' => 'sale_orders', 'action' => 'view', $saleInvoice['SaleOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoice['User']['username'], array('controller' => 'users', 'action' => 'view', $saleInvoice['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Term Of Payment'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleInvoice['TermOfPayment']['name'], array('controller' => 'term_of_payments', 'action' => 'view', $saleInvoice['TermOfPayment']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['created']); ?>
			&nbsp;
		</dd> 
		<dt><?php echo __('Fat Date'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['fat_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Delivery Date'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['delivery_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($saleInvoice['SaleInvoice']['remark']); ?>
			&nbsp;
		</dd>
	</dl>
</div>  

<div class="saleInvoices view-data">
<h2><?php echo __('Supporting Document'); ?></h2>
	<dl> 
		<dt><?php echo __('Document Report'); ?></dt>
		<dd>
			<?php echo tick($saleInvoice['SaleInvoice']['document_report']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Purchase Order'); ?></dt>
		<dd>
			<?php echo tick($saleInvoice['SaleInvoice']['purchase_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Delivery Order'); ?></dt>
		<dd>
			<?php echo tick($saleInvoice['SaleInvoice']['delivery_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contract'); ?></dt>
		<dd>
			<?php echo tick($saleInvoice['SaleInvoice']['contract']); ?>
			&nbsp;
		</dd>
		
	</dl>
</div> 
<div class="related">
	<h4><?php echo __('Items'); ?></h4>
	<?php if (!empty($saleInvoice['SaleInvoiceItem'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>  
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Unit Price'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th><?php echo __('Discount'); ?></th>
		<th><?php echo __('Tax'); ?></th> 
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr> 
	<?php foreach ($items as $saleInvoiceItem): ?>
		<tr>  
			<td><?php echo $saleInvoiceItem['SaleOrderItem']['InventoryItem']['code']; ?></td>
			<td><?php echo $saleInvoiceItem['SaleOrderItem']['InventoryItem']['name']; ?></td>
			<td><?php echo $saleInvoiceItem['SaleOrderItem']['unit_price']; ?></td>
			<td><?php echo $saleInvoiceItem['SaleInvoiceItem']['quantity']; ?></td>
			<td><?php echo $saleInvoiceItem['GeneralUnit']['name']; ?></td>
			<td><?php echo $saleInvoiceItem['SaleOrderItem']['discount']; ?></td>
			<td><?php echo $saleInvoiceItem['SaleOrderItem']['tax']; ?></td> 
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_invoice_items', 'action' => 'view', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_invoice_items', 'action' => 'edit', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_invoice_items', 'action' => 'delete', $saleInvoiceItem['SaleInvoiceItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleInvoiceItem['SaleInvoiceItem']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?> 
</div>

</div>
</div>
</div>
</div>

<?php

function tick($status) {
	if($status == 1) {
		return 'Yes';
	} else {
		return 'No';
	}
}
?>