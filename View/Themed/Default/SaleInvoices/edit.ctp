<?php echo $this->Html->link(__('Back To RFI Lists'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit Requisition for Invoicing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleInvoices form">
<?php echo $this->Form->create('SaleInvoice', array('class' => 'form-horizontal')); ?> 
	<?php echo $this->Form->input('id'); ?>
	<div class="form-group">
		<label class="col-sm-3">Sale Order No *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('SaleOrder.name', array('id' => 'findso', 'class' => 'form-control', 'placeholder' => 'Enter Sale Order No', 'label' => false)); ?>
		</div>
	</div>
		<?php echo $this->Form->input('sale_job_id', array('id' => 'sale_job_id', 'type' => 'hidden')); ?>
		<?php echo $this->Form->input('sale_job_child_id', array('id' => 'sale_job_child_id', 'type' => 'hidden')); ?>
		<?php echo $this->Form->input('sale_order_id', array('id' => 'sale_order_id', 'type' => 'hidden')); ?>
	<?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden')); ?>
	<div class="form-group">
		<label class="col-sm-3">Contact Person *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('customer_contact_person', array('class' => 'form-control', 'placeholder' => 'Contact Name', 'label' => false)); ?>
		</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-3">Phone No *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('customer_phone', array('class' => 'form-control', 'placeholder' => 'Phone No', 'label' => false)); ?>
		</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-3">Term of Payment *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('term_of_payment_id', array('class' => 'form-control', 'label' => false)); ?>
		</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-3">Date *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('date', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD', 'id' => 'dateonly', 'label' => false)); ?>
		</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-3">Fat Date *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('fat_date', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD', 'id' => 'dateonly_2', 'label' => false)); ?>
		</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-3">Delivery Date *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('delivery_date', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'YYYY-MM-DD', 'id' => 'dateonly_3', 'label' => false)); ?>
		</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-3">Contact Person *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('remark', array('class' => 'form-control', 'placeholder' => 'Remark / Note', 'required' => false, 'label' => false)); ?>
		</div>
	</div> 	
	<div class="form-group">
		<label class="col-sm-3">Status *</label>
		<div class="col-sm-9">
		<?php 
		$status = array('Draft' => 'Draft', 'Submit To Finance' => 'Submit To Finance'); 
		echo $this->Form->input('status', array('empty' => '-Select Status-', 'options' => $status, 'class' => 'form-control', 'label' => false)); 
		?>
		</div>
	</div>  
	 <div class="form-group">
	 <div class="col-md-12"><h4>Document Required</h4></div>
	 </div>
	<div class="form-group">
		<div class="col-sm-6"><?php echo $this->Form->checkbox('document_report', array('type' => 'checkbox')); ?> Test Report / Service Report</div>
		<div class="col-sm-6"><?php echo $this->Form->checkbox('purchase_order', array('type' => 'checkbox')); ?> Purchase Order</div>
	</div>
	<div class="form-group">
		<div class="col-sm-6"><?php echo $this->Form->checkbox('delivery_order', array('type' => 'checkbox')); ?> Delivery Order</div>
		<div class="col-sm-6"><?php echo $this->Form->checkbox('contract', array('type' => 'checkbox')); ?> Contract</div>
	</div>    
	 
	<div id="loadMoreBom"></div>   

		<div class="form-group">
			<label class="col-sm-3">&nbsp;</label>
			<div class="col-sm-9">
				<?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findItem(row, search) { 
	console.log(search);
	$('#findProduct'+row).autocomplete({ 
	    source: function (request, response) { 
	    	if($('#sale_job_child_id').val() == '') {
	    		alert('Please enter valid Job Number');
	    	}
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'inventory_items/ajaxfinditem',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#findProduct'+row).val() + "&sale_job_child_id=" + $('#sale_job_child_id').val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.code,
		                    name : item.name,
		                    price: item.price,
		                    code: item.code,
		                    type: item.type,
		                    bom_id: item.bom['id'],
		                    bom_name: item.bom['code']
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {  
	        $('#productId'+row).val( ui.item.id ); 
	        $('#price'+row).val( ui.item.price ); 
	        $('#total'+row).val( ui.item.price ); 
	        $('#bom_id'+row).val( ui.item.bom_id );
	        console.log(ui.item);
	        $('#bom_name'+row).html( ui.item.bom_name );
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.name + "<br><small>" + item.code + "</small><br/><small>" + item.type + "</small><br>" +  "</div>" ).appendTo( ul );
    };
}

 
$(function() { 

	$('#findso').autocomplete({ 
	    source: function (request, response){ 
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_orders/ajaxfindsaleorder',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#findso').val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    sale_job_child_id: item.sale_job_child_id,
		                    value: item.name,
		                    customer: item.customer_name,
		                    customer_id: item.customer_id,
		                    sale_tender_id: item.sale_tender_id,
		                    sale_quotation_id: item.sale_quotation_id, 
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {   
	        $('#sale_job_id').val(ui.item.id); 
	        $('#sale_job_child_id').val(ui.item.sale_job_child_id);
	        $('#customer_id').val(ui.item.customer_id);
	        $('#sale_quotation_id').val(ui.item.sale_quotation_id);
	        $('#sale_tender_id').val(ui.item.sale_tender_id); 
	        $('#sale_order_id').val(ui.item.id);  

	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_orders/ajaxfindsaleorderitem',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + ui.item.id,                                                    
	            success: function (data) { 
	            	var station = 0;
	            	var html = ''; 
		            $.each(data, function(i, item) { 
					    console.log(item); 
					    $('#unit'+station).val(item.SaleOrderItem.general_unit_id);
	    html += '<div id="removeBom'+station+'">';
	    html += '<div class="form-group">';  
	    html += '<div class="col-sm-4" id="autoComplete">';
	    html += '<input type="text" name="find" id="findProduct'+station+'" class="form-control findProduct" value="'+item.InventoryItem.name+'" placeholder="Item Code/Name"required><input value="'+item.InventoryItem.id+'" type="hidden" name="data[SaleInvoiceItem][inventory_item_id][]" id="productId'+station+'"><input value="'+item.SaleOrderItem.id+'" type="hidden" name="data[SaleInvoiceItem][sale_order_item_id][]" id="productId'+station+'">';
	    html += '</div>'; 
	    html += '<div class="col-sm-1">';
	    html += '<input type="text" value="'+item.SaleOrderItem.quantity+'" name="data[SaleInvoiceItem][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
	    html += '</div>'; 
	    html += '<div class="col-sm-1">';
	    html += '<select name="data[SaleInvoiceItem][general_unit_id][]" class="form-control" id="unit'+station+'"required>'; 
	    html += '<option value="">-Unit-</option>';
	    html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
	    html += '</select></div>';

	    html += '<div class="col-sm-2"><input type="text" value="'+item.SaleOrderItem.unit_price+'" name="data[SaleInvoiceItem][unit_price][]" class="form-control" id="price'+station+'" placeholder="Price/Unit" required></div>'; 
	    html += '<div class="col-sm-1"><select value="'+item.SaleOrderItem.discount_type+'" name="data[SaleInvoiceItem][discount_type][]" value="" class="form-control">';
	    html += '<option value="0">-Select-</option><option value="1">Fix</option><option value="2">%</option>';  
	    html += '</select></div>';  
	     html += '<div class="col-sm-1"><input type="text" value="'+item.SaleOrderItem.discount+'" name="data[SaleInvoiceItem][discount][]" value="" class="form-control" placeholder="Discount"></div>';  
	    html += '<div class="col-sm-1"><input type="text" value="'+item.SaleOrderItem.tax+'" name="data[SaleInvoiceItem][tax][]" value="" class="form-control" placeholder="Tax"></div>';   
	    html += '<div class="col-sm-1">';
	    html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div>';  
	    html += '</div></div>';   
	    station++;
					});
					$('#loadMoreBom').html(html);
		        }
	        }); 
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.value + "<br>" + item.customer + "<br>" +  "</div>" ).appendTo( ul );
    };

	

	var station = 1;
	$('#addItem').click(function() {
		var html = '<div id="removeBom'+station+'">'; 
	    html += '<div class="form-group">';  
	    html += '<div class="col-sm-4" id="autoComplete">';
	    html += '<input type="text" name="find" id="findProduct'+station+'" class="form-control findProduct" placeholder="Item Code/Name"required><input type="hidden" name="data[SaleOrderItem][inventory_item_id][]" id="productId'+station+'">';
	    html += '<input type="hidden" name="data[SaleOrderItem][bom_id][]" id="bom_id'+station+'"></div>';
	 
	    html += '<div class="col-sm-1">';
	    html += '<input type="text" name="data[SaleOrderItem][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
	    html += '</div>';

	    html += '<div class="col-sm-1">';
	    html += '<select name="data[SaleOrderItem][general_unit_id][]" class="form-control"required>'; 
	    html += '<option value="">-Unit-</option>';
	    html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
	    html += '</select></div>';

	    html += '<div class="col-sm-2"><input type="text" name="data[SaleOrderItem][unit_price][]" class="form-control" id="price'+station+'" placeholder="Price/Unit" required></div>'; 
	    html += '<div class="col-sm-1"><select name="data[SaleOrderItem][discount_type][]" value="" class="form-control">';
	    html += '<option value="0">-Select-</option><option value="1">Fix</option><option value="2">%</option>';  
	    html += '</select></div>';  
	     html += '<div class="col-sm-1"><input type="text" name="data[SaleOrderItem][discount][]" value="" class="form-control" placeholder="Discount"></div>';  
	    html += '<div class="col-sm-1"><input type="text" name="data[SaleOrderItem][tax][]" value="" class="form-control" placeholder="Tax"></div>';  
	  

	    html += '<div class="col-sm-1">';
	    html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div>';

	    html += '<div class="col-sm-12">';
	    html += '<div id="bom_name'+station+'"></div>';
	    html += '</div>';

	    html += '</div></div>';    
	    $("#loadMoreBom").append(html);  
	    
		findItem(station, $(this).val());  
		station++; 
	});  
});
</script>
<?php $this->end(); ?>