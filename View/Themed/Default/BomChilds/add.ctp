
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo __('Add Sub Assembly'); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 

    <h4>BOM Code: <?php echo $bom['Bom']['code']; ?></h4>

    <?php if($child != null) { ?>
    	<h5>Parent: <?php echo $child['BomChild']['name']; ?></h5>
    <?php } ?>

    <?php echo $this->Session->flash(); ?> 
	<?php echo $this->Form->create('BomChild', array('class' => 'form-horizontal')); ?> 
	<?php 
		echo $this->Form->input('bom_id', array('type' => 'hidden', 'value' => $bom['Bom']['id']));
		echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' => 0));
	?>

	<p>You can attach Sub-assembly or BOM by using following method:<br/>
	copybom-BomCode or copysub-SubAssemblyName.</p>

	<div class="form-group row">
	<label class="col-sm-3">Sub Assembly</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('name', array('id' => 'sub', 'class' => 'form-control', 'label' => false)); ?>
	<?php echo $this->Form->input('copy_from_id', array('id' => 'copy_from_id', 'type' => 'hidden')); ?>
	<?php echo $this->Form->input('from_bom_id', array('id' => 'bom_id', 'type' => 'hidden')); ?>
	<?php echo $this->Form->input('type', array('id' => 'type', 'type' => 'hidden')); ?>
	<?php echo $this->Form->input('old_id', array('type' => 'hidden')); ?>
	<?php echo $this->Form->input('parent_id', array('type' => 'hidden')); ?>


	</div>
	</div> 
	<div class="form-group">
		<label class="col-sm-3">&nbsp;</label>
		<div class="col-sm-9">
			<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
		</div>
	</div> 
	<?php echo $this->Form->end(); ?>
</div>
</div> 
</div> 
</div>


<?php $this->start('script'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#sub').autocomplete({ 
	      source: function (request, response){ 
	      	var term = $('#sub').val();
	      	var url = '';
	      	console.log(term.substr(0, 8));
	      	if(term.substr(0, 8) == 'copybom-') {
	      		url = 'boms/ajaxcopybom';
	      	} else if(term.substr(0, 8) == 'copysub-') {
	      		url = 'boms/ajaxcopychild';
	      	} 
	      	if(url != '') {
		      	$.ajax({
					type: "GET",                        
					url:baseUrl + url,           
					contentType: "application/json",
					dataType: "json",
					data: "term=" + term,                                                    
					success: function (data) { 
						response($.map(data, function (item) {
							if(term.substr(0, 8) == 'copybom-') {
							    return {
							        id: item.id,
							        value: item.code,
							        name : item.name,
							        type : 'bom',
							        bom_id : item.id
							    }
						    } else {
						    	return {
							        id: item.id,
							        value: item.name,
							        code : item.code,
							        bom_name : item.bom_name,
							        bom_id : item.bom_id,
							        type: 'child' 
							    }
						    }
						}));
					}
				});	
	        } 
	      },
	      select: function (event, ui) {    
	          if(ui.item.type == 'bom') { 
	          	$('#copy_from_id').val(ui.item.id);
	          	$('#type').val('bom');
	          	$('#bom_id').val(ui.item.bom_id);
	          }
	          if(ui.item.type == 'child') { 
	          	$('#copy_from_id').val(ui.item.id);
	          	$('#type').val('child');
	          	$('#bom_id').val(ui.item.bom_id);
	          }
	      },
	      minLength: 3
	  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
	  		if(item.type == 'bom') {
	  			return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/>" +  "</div>" ).appendTo( ul );
	  		}
	      	if(item.type == 'child') {
	  			return $( "<li>" ).append( "<div>" + item.value + "<br><small>BOM: " + item.code + "</small><br/>" +  "</div>" ).appendTo( ul );
	  		}
	  };
	});
</script>
<?php $this->end(); ?> 