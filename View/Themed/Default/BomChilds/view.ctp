<div class="bomChildren view">
<h2><?php echo __('Bom Child'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($bomChild['BomChild']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($bomChild['BomChild']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bom'); ?></dt>
		<dd>
			<?php echo $this->Html->link($bomChild['Bom']['name'], array('controller' => 'boms', 'action' => 'view', $bomChild['Bom']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Id'); ?></dt>
		<dd>
			<?php echo h($bomChild['BomChild']['parent_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Bom Child'), array('action' => 'edit', $bomChild['BomChild']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Bom Child'), array('action' => 'delete', $bomChild['BomChild']['id']), array(), __('Are you sure you want to delete # %s?', $bomChild['BomChild']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Bom Children'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom Child'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Boms'), array('controller' => 'boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom'), array('controller' => 'boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bom Items'), array('controller' => 'bom_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom Item'), array('controller' => 'bom_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Bom Items'); ?></h3>
	<?php if (!empty($bomChild['BomItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Inventory Item Id'); ?></th>
		<th><?php echo __('Bom Child Id'); ?></th>
		<th><?php echo __('Quantity'); ?></th>
		<th><?php echo __('General Unit Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($bomChild['BomItem'] as $bomItem): ?>
		<tr>
			<td><?php echo $bomItem['id']; ?></td>
			<td><?php echo $bomItem['inventory_item_id']; ?></td>
			<td><?php echo $bomItem['bom_child_id']; ?></td>
			<td><?php echo $bomItem['quantity']; ?></td>
			<td><?php echo $bomItem['general_unit_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'bom_items', 'action' => 'view', $bomItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'bom_items', 'action' => 'edit', $bomItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'bom_items', 'action' => 'delete', $bomItem['id']), array(), __('Are you sure you want to delete # %s?', $bomItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bom Item'), array('controller' => 'bom_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
