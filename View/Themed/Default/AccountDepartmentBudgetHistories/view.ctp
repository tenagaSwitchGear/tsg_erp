<div class="accountDepartmentBudgetHistories view">
<h2><?php echo __('Account Department Budget History'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Account Department Budget'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accountDepartmentBudgetHistory['AccountDepartmentBudget']['name'], array('controller' => 'account_department_budgets', 'action' => 'view', $accountDepartmentBudgetHistory['AccountDepartmentBudget']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Purchase Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accountDepartmentBudgetHistory['InventoryPurchaseOrder']['id'], array('controller' => 'inventory_purchase_orders', 'action' => 'view', $accountDepartmentBudgetHistory['InventoryPurchaseOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount Before'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['amount_before']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount Used'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['amount_used']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount Balance'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['amount_balance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($accountDepartmentBudgetHistory['User']['id'], array('controller' => 'users', 'action' => 'view', $accountDepartmentBudgetHistory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End User'); ?></dt>
		<dd>
			<?php echo h($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['end_user']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Account Department Budget History'), array('action' => 'edit', $accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Account Department Budget History'), array('action' => 'delete', $accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id']), array(), __('Are you sure you want to delete # %s?', $accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Account Department Budget Histories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account Department Budget History'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Account Department Budgets'), array('controller' => 'account_department_budgets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account Department Budget'), array('controller' => 'account_department_budgets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Orders'), array('controller' => 'inventory_purchase_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order'), array('controller' => 'inventory_purchase_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
