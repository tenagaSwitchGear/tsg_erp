<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Transaction History</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('AccountDepartmentBudgetHistory', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
<table cellpadding="0" cellspacing="0" class="table">
	<tr>
	<td><?php echo $this->Form->input('po_no', array('placeholder' => 'PO No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
	</td> 
	<td><?php echo $this->Form->input('from', array('placeholder' => 'From', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
	<td><?php echo $this->Form->input('to', array('placeholder' => 'To', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly_2')); ?></td>
	<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
	</tr>
</table>
<?php $this->end(); ?> 

<div class="accountDepartmentBudgetHistories index"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('AccountDepartmentBudget.name', 'Budget'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_purchase_order_id', 'PO No'); ?></th>
			<th><?php echo $this->Paginator->sort('amount_before'); ?></th>
			<th><?php echo $this->Paginator->sort('amount_used'); ?></th>
			<th><?php echo $this->Paginator->sort('amount_balance'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id', 'Procurement'); ?></th>
			<th><?php echo $this->Paginator->sort('end_user'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($accountDepartmentBudgetHistories as $accountDepartmentBudgetHistory): ?>
	<tr> 
		<td>
			<?php echo $this->Html->link($accountDepartmentBudgetHistory['AccountDepartmentBudget']['name'], array('controller' => 'account_department_budgets', 'action' => 'view', $accountDepartmentBudgetHistory['AccountDepartmentBudget']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($accountDepartmentBudgetHistory['InventoryPurchaseOrder']['po_no'], array('controller' => 'inventory_purchase_orders', 'action' => 'view', $accountDepartmentBudgetHistory['InventoryPurchaseOrder']['id'])); ?>
		</td>
		<td><?php echo _n2($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['amount_before']); ?>&nbsp;</td>
		<td><?php echo _n2($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['amount_used']); ?>&nbsp;</td>
		<td><?php echo _n2($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['amount_balance']); ?>&nbsp;</td>
		<td><?php echo h($accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['created']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($accountDepartmentBudgetHistory['User']['firstname'], array('controller' => 'users', 'action' => 'view', $accountDepartmentBudgetHistory['User']['id'])); ?>
		</td>
		<td><?php echo h($accountDepartmentBudgetHistory['EndUser']['firstname']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id']), array(), __('Are you sure you want to delete # %s?', $accountDepartmentBudgetHistory['AccountDepartmentBudgetHistory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
	<?php
	  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
	  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	?>
	</ul>
</div>
 
</div>
</div>
</div>
</div>
