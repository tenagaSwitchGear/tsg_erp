<div class="accountDepartmentBudgetHistories form">
<?php echo $this->Form->create('AccountDepartmentBudgetHistory'); ?>
	<fieldset>
		<legend><?php echo __('Add Account Department Budget History'); ?></legend>
	<?php
		echo $this->Form->input('account_department_budget_id');
		echo $this->Form->input('inventory_purchase_order_id');
		echo $this->Form->input('amount_before');
		echo $this->Form->input('amount_used');
		echo $this->Form->input('amount_balance');
		echo $this->Form->input('user_id');
		echo $this->Form->input('end_user');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Account Department Budget Histories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Account Department Budgets'), array('controller' => 'account_department_budgets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account Department Budget'), array('controller' => 'account_department_budgets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Orders'), array('controller' => 'inventory_purchase_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Order'), array('controller' => 'inventory_purchase_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
