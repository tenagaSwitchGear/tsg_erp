<?php echo $this->Html->link(__('Waiting Verification'), array('action' => 'index', 'Waiting Verification'), array('class' => 'btn btn-warning')); ?>

<?php echo $this->Html->link(__('Waiting Approval'), array('action' => 'index', 'Waiting Approval'), array('class' => 'btn btn-info')); ?>

<?php echo $this->Html->link(__('Sent To Dept.'), array('action' => 'index', 'Approved'), array('class' => 'btn btn-primary')); ?>


<?php echo $this->Html->link(__('Received'), array('action' => 'index', 'Received'), array('class' => 'btn btn-success')); ?>
 

<?php echo $this->Html->link(__('Add Handing Over Document'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Handing Over Document</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create('SaleHandingOverDocument', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('customer_id', array('type' => 'text', 'placeholder' => 'Customer', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 


<div class="saleHandingOvers index"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('sale_job_child_id', 'Station'); ?></th> 
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th> 
			
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th><?php echo $this->Paginator->sort('user_id', 'User'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('date_hod', 'Date HOD'); ?></th> 
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleHandingOvers as $saleHandingOver): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($saleHandingOver['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleHandingOver['SaleJob']['id'])); ?><br/>
			<small><?php echo $saleHandingOver['SaleJobChild']['name']; ?></small>
		</td>
		 
		<td>
			<?php echo $this->Html->link($saleHandingOver['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleHandingOver['Customer']['id'])); ?>
		</td> 
		<td><?php echo h($saleHandingOver['SaleHandingOver']['created']); ?>&nbsp;</td> 
		<td>
			<?php echo $this->Html->link($saleHandingOver['User']['username'], array('controller' => 'users', 'action' => 'view', $saleHandingOver['User']['id'])); ?>
		</td> 
		<td><?php echo h($saleHandingOver['SaleHandingOver']['status']); ?>&nbsp;</td>
		<td><?php echo h($saleHandingOver['SaleHandingOver']['date_hod']); ?>&nbsp;</td> 
		<td class="actions">
			<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $saleHandingOver['SaleHandingOver']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $saleHandingOver['SaleHandingOver']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?> 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
	</ul>
</div>
 
</div>
</div>
</div>
</div>