<?php echo $this->Html->link(__('Handing Over Lists'), array('action' => 'index'), array('class' => 'btn btn-success')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add New Handing Over Document</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 		
 		<p><b>Note: </b>Before adding Handing Over Document, please make sure you have created Sale Order first. Item will be automatically select from Sales Order.</p> 
		<?php echo $this->Form->create('SaleHandingOver', array('class' => 'form-horizontal')); ?> 

		<div class="form-group">
			<label class="col-sm-3">Job No</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('find_job', array('type' => 'text', 'class' => 'form-control', 'id' => 'find_job', 'label' => false)); ?> 
				<?php echo $this->Form->input('sale_quotation_id', array('type' => 'hidden', 'id' => 'sale_quotation_id')); ?> 
				<?php echo $this->Form->input('customer_id', array('type' => 'hidden', 'id' => 'customer_id')); ?>  
				<?php echo $this->Form->input('sale_job_id', array('type' => 'hidden', 'id' => 'sale_job_id')); ?> 
				<?php echo $this->Form->input('sale_job_child_id', array('type' => 'hidden', 'id' => 'sale_job_child_id')); ?>  
				 
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">Date</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('date_hod', array('type' => 'text', 'id' => 'dateonly', 'class' => 'form-control', 'label' => false)); ?> 
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3">Commercial</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('commercial', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?> 
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3">Remark</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?> 
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3">Other Note</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('other', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?> 
			</div>
		</div>

		<hr/>
		<div class="form-group">
			<div class="col-sm-12"><p>Please add atleast 1 Recipient by click button below and enter Username or Full name.</p></div>
		</div> 
		<div id="loadUser"></div>

		<div class="form-group">
			<div class="col-sm-12">
				<a href="#" id="addUser" class="btn btn-default"><i class="fa fa-user"></i> Add Recipient</a>
			</div>
		</div>

		<div class="form-group">
		<label class="col-sm-3">&nbsp;</label>
		<div class="col-sm-9">
			<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
		</div>
		</div>		 
		<?php echo $this->Form->end(); ?>
 
</div>
</div>
</div>
</div>




<?php $this->start('script'); ?>
<script type="text/javascript"> 
 
function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function findUser(row, search) { 
	console.log(search);
	$('#findUser'+row).autocomplete({ 
	    source: function (request, response){ 
	        $.ajax({
	            type: "GET",                        
	            url: baseUrl + 'users/ajaxfinduser',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#findUser'+row).val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.username,
		                    name : item.name,
		                    email: item.email,
		                    group: item.group 
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {  
	        $('#user_id'+row).val( ui.item.id );
	        $('#name'+row).val( ui.item.name );
	        $('#department'+row).val( ui.item.group );  
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>" + item.email + "</small><br>" +  "</div>" ).appendTo( ul );
    };
}
 
 
$(function() { 

	$('#find_job').autocomplete({ 
	    source: function (request, response){ 
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_jobs/ajaxfindjob',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#find_job').val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    sale_job_child_id: item.sale_job_child_id,
		                    value: item.name,
		                    station: item.station,
		                    customer: item.customer,
		                    customer_id: item.customer_id,
		                    sale_tender_id: item.sale_tender_id,
		                    sale_quotation_id: item.sale_quotation_id,
		                     
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {   
	        $('#sale_job_id').val(ui.item.id); 
	        $('#sale_job_child_id').val(ui.item.sale_job_child_id);
	        $('#customer_id').val(ui.item.customer_id);
	        $('#sale_quotation_id').val(ui.item.sale_quotation_id);
	        $('#sale_tender_id').val(ui.item.sale_tender_id); 
	         
	        
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.station + " - " + item.customer + "</small><br>" +  "</div>" ).appendTo( ul );
    };

	var station = 1;
	$('#addUser').click(function() {
		var html = '<div id="removeBom'+station+'">'; 
	    html += '<div class="form-group">';  
	    html += '<div class="col-sm-3" id="autoComplete">';
	    html += '<input type="text" name="find" id="findUser'+station+'" class="form-control findProduct" placeholder="Username / Name / Email"required>';
	    html += '<input type="hidden" name="user_id[]" id="user_id'+station+'">';
	    html += '</div>'; 
	    html += '<div class="col-sm-4">';
	    html += '<input type="text" name="name" id="name'+station+'" class="form-control" placeholder="Name"disabled>';
	  
	    html += '</div>'; 
	    html += '<div class="col-sm-4">';
	    html += '<input type="text" name="find" id="department'+station+'" class="form-control" placeholder="Department"disabled>';
	   
	    html += '</div>'; 
	    html += '<div class="col-sm-1">';
	    html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
	    html += '</div>';
	    html += '</div></div>';    
	    $("#loadUser").append(html);  
	    
		findUser(station, $(this).val());  
		station++; 
		return false;
	});  
});
</script>
<?php $this->end(); ?> 