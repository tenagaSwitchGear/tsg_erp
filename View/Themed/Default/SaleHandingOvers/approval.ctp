<?php echo $this->Html->link(__('All'), array('action' => 'approval'), array('class' => 'btn btn-info')); ?>

<?php echo $this->Html->link(__('Waiting Verification'), array('action' => 'approval', 'Waiting Verification'), array('class' => 'btn btn-warning')); ?>
<?php echo $this->Html->link(__('Waiting Approval'), array('action' => 'approval', 'Waiting Approval'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Html->link(__('Sent'), array('action' => 'approval', 'Approved'), array('class' => 'btn btn-success')); ?>

<?php echo $this->Html->link(__('Received'), array('action' => 'approval', 'Received'), array('class' => 'btn btn-success')); ?>

<?php echo $this->Html->link(__('Rejected'), array('action' => 'approval', 'Rejected'), array('class' => 'btn btn-danger')); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Handing Over Document</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleHandingOvers index"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('sale_job_child_id', 'Job'); ?></th> 
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th> 
			
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th><?php echo $this->Paginator->sort('user_id', 'User'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('date_hod', 'Date HOD'); ?></th> 
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleHandingOvers as $saleHandingOver): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($saleHandingOver['SaleJobChild']['station_name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleHandingOver['SaleJob']['id'])); ?><br/>
			<small><?php echo $saleHandingOver['SaleJobChild']['name']; ?></small>
		</td>
		 
		<td>
			<?php echo $this->Html->link($saleHandingOver['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleHandingOver['Customer']['id'])); ?>
		</td> 
		<td><?php echo h($saleHandingOver['SaleHandingOver']['created']); ?>&nbsp;</td> 
		<td>
			<?php echo $this->Html->link($saleHandingOver['User']['username'], array('controller' => 'users', 'action' => 'view', $saleHandingOver['User']['id'])); ?>
		</td> 
		<td><?php echo h($saleHandingOver['SaleHandingOver']['status']); ?>&nbsp;</td>
		<td><?php echo h($saleHandingOver['SaleHandingOver']['date_hod']); ?>&nbsp;</td> 
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'verifyview', $saleHandingOver['SaleHandingOver']['id'])); ?> 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
	</ul>
</div>
 
</div>
</div>
</div>
</div>