
<?php $user_id = $this->Session->read('Auth.User.id'); ?>

<?php if($saleHandingOver['User']['id'] == $user_id) { ?>
<?php echo $this->Html->link(__('Handing Over Document'), array('action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleHandingOver['SaleHandingOver']['id']), array('class' => 'btn btn-warning btn-sm')); ?>
<?php } ?>

<?php if(in_array($user_id, $recipient)) { ?>
<?php echo $this->Html->link(__('Handing Over Document'), array('action' => 'receiveindex'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Mark As Receive'), array('action' => 'view', $saleHandingOver['SaleHandingOver']['id'], 'Receive', $user_id), array('class' => 'btn btn-danger btn-sm', 'escape' => false)); ?>
<?php } ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Handing Over Document</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleHandingOvers view-data"> 
	<dl> 
		 
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleHandingOver['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleHandingOver['Customer']['id'])); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleHandingOver['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleHandingOver['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Station'); ?></dt>
		<dd>
			<?php echo $saleHandingOver['SaleJobChild']['station_name']; ?> - <?php echo $saleHandingOver['SaleJobChild']['name']; ?> 
		</dd>

		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($saleHandingOver['SaleHandingOver']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($saleHandingOver['SaleHandingOver']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created By'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleHandingOver['User']['username'], array('controller' => 'users', 'action' => 'view', $saleHandingOver['User']['id'])); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($saleHandingOver['SaleHandingOver']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Hod'); ?></dt>
		<dd>
			<?php echo h($saleHandingOver['SaleHandingOver']['date_hod']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Commercial'); ?></dt>
		<dd>
			<p class="wrapper"><?php echo h($saleHandingOver['SaleHandingOver']['commercial']); ?></p>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<p class="wrapper"><?php echo h($saleHandingOver['SaleHandingOver']['remark']); ?></p>
			&nbsp;
		</dd>
		<dt><?php echo __('Other'); ?></dt>
		<dd>
			<p class="wrapper"><?php echo h($saleHandingOver['SaleHandingOver']['other']); ?></p>
			&nbsp;
		</dd>
	</dl>
</div>

<h4>Items</h4>
<table class="table table-bordered">
	<thead>
	<tr>
		<th>No</th> 
		<th>Code</th>
		<th>Name</th> 
		<th>Qty</th>  
	</tr>
	</thead>
	<tbody>
	<?php $i = 1;
	$total = 0; ?>
	<?php foreach ($items as $item): ?>
	<?php $total += $item['SaleOrderItem']['total_price']; ?> 
	<tr>
		<td><?php echo $i; ?></td> 
		<td><?php echo h($item['InventoryItem']['code']); ?></td>
		<td><p><?php echo h($item['InventoryItem']['name']); ?><br/>
		<small><?php echo h($item['InventoryItem']['note']); ?></small></p>
		</td>
		 
		<td><?php echo $item['SaleOrderItem']['quantity']; ?> </td>  
	</tr>
	<?php $i += 1; endforeach; ?>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>    
	</tr>
	</tbody>
	</table>
 
 <h4>Users</h4>
<table class="table table-bordered">
<tr> 
	<th>Username</th>
	<th>Name</th>
	<th>Email</th>
	<th>Status</th>
	<th>Receive Date</th> 
</tr>
<?php foreach($users as $user) { ?> 
	<tr>
		<td><?php echo $user['User']['username']; ?></td>
		<td><?php echo h($user['User']['firstname']); ?></td>
		<td><?php echo $user['User']['email']; ?></td>
		<td><?php if($user['SaleHandingOverUser']['status'] == 'Received') { ?>
			<label class="label label-success"><?php echo $user['SaleHandingOverUser']['status']; ?></label>
		<?php } else { ?>
			<label class="label label-danger"><?php echo $user['SaleHandingOverUser']['status']; ?></label>
		<?php } ?>
		</td>
		<td><?php echo $user['SaleHandingOverUser']['receive_date']; ?></td> 
	</tr>
<?php } ?>
</table> 
	</div>
</div>
</div>
</div>

