<div class="properties form">
<?php echo $this->Form->create('Property'); ?>
	<fieldset>
		<legend><?php echo __('Edit Property'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('state_id');
		echo $this->Form->input('city_id');
		echo $this->Form->input('property_category_id');
		echo $this->Form->input('property_current_occupancy_id');
		echo $this->Form->input('property_furnishing_type_id');
		echo $this->Form->input('property_land_title_id');
		echo $this->Form->input('property_post_type_id');
		echo $this->Form->input('property_tenure_type_id');
		echo $this->Form->input('property_title_id');
		echo $this->Form->input('property_unit_type_id');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('slug');
		echo $this->Form->input('price');
		echo $this->Form->input('size');
		echo $this->Form->input('bedroom');
		echo $this->Form->input('bathroom');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Property.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Property.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Properties'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Categories'), array('controller' => 'property_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Category'), array('controller' => 'property_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Current Occupancies'), array('controller' => 'property_current_occupancies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Current Occupancy'), array('controller' => 'property_current_occupancies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Furnishing Types'), array('controller' => 'property_furnishing_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Furnishing Type'), array('controller' => 'property_furnishing_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Land Titles'), array('controller' => 'property_land_titles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Land Title'), array('controller' => 'property_land_titles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Post Types'), array('controller' => 'property_post_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Post Type'), array('controller' => 'property_post_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Tenure Types'), array('controller' => 'property_tenure_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Tenure Type'), array('controller' => 'property_tenure_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Titles'), array('controller' => 'property_titles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Title'), array('controller' => 'property_titles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Unit Types'), array('controller' => 'property_unit_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Unit Type'), array('controller' => 'property_unit_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Facility Selecteds'), array('controller' => 'property_facility_selecteds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Facility Selected'), array('controller' => 'property_facility_selecteds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Images'), array('controller' => 'property_images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Image'), array('controller' => 'property_images', 'action' => 'add')); ?> </li>
	</ul>
</div>
