<div class="">
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2><?php echo __($property['Property']['title']); ?></h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li class="dropdown">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            <ul class="dropdown-menu" role="menu">
	              <li><a href="#">Settings 1</a>
	              </li>
	              <li><a href="#">Settings 2</a>
	              </li>
	            </ul>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	      	<div class="col-sm-4 col-xs-6">
	      	<p><i class="fa fa-map-marker"></i> <?php echo __($property['City']['name']); ?>, <?php echo __($property['State']['name']); ?></p>
	      	</div>

	      	<div class="col-sm-4 col-xs-6">
	      	<p><i class="fa fa-building"></i> <?php echo __($property['PropertyCategory']['name']); ?> - <?php echo __($property['PropertyPostType']['name']); ?></p>
	      	</div>

			<div class="col-sm-2 col-xs-6">
	      	<p><i class="fa fa-user"></i> <?php echo __($property['User']['firstname']); ?></p>
	      	</div>

	      	<div class="col-sm-2 col-xs-6">
	      	<p><i class="fa fa-clock-o"></i> <?php echo __($property['Property']['created']); ?></p>
	      	</div>

	      	

	        <div class="col-md-7 col-sm-7 col-xs-12">
	            <div class="product-image" id="displayImage"> 
		            <?php echo $this->Html->image('Users/Original/'.$property['PropertyImage'][0]['name'], array('class' => 'img-responsive')); ?> 
	            </div>
	            <div class="product_gallery">
	            <?php foreach ($property['PropertyImage'] as $image) { ?> 
	            	<?php $gallery = $image['name']; ?>
		            <a onclick="galleryImage('<?php echo $gallery; ?>');">
		            	<?php echo $this->Html->image('Users/Original/'.$image['name'], array('class' => 'img-responsive')); ?>
		            </a>
                <?php } ?>
	             
	            </div>
	        </div>

	        <div class="col-md-5 col-sm-5 col-xs-12">
	        <div class="product-features" style="border:1px solid #e5e5e5;">
	          <h3 class="prod_title"><?php echo __($property['Property']['title']); ?></h3> 
	          <br />  
	          
	          <p><span>Occupancy:</span> <?php echo __($property['PropertyCurrentOccupancy']['name']); ?></p>
	          <p><span>Land Title:</span> <?php echo __($property['PropertyLandTitle']['name']); ?></p>
	          <p><span>Unit:</span> <?php echo __($property['PropertyUnitType']['name']); ?></p>
	          <p><span>Title:</span> <?php echo __($property['PropertyTitle']['name']); ?></p>
	          <p><span>Tenure:</span> <?php echo __($property['PropertyTenureType']['name']); ?></p> 
	          <p><span>Furnishing:</span> <?php echo __($property['PropertyFurnishingType']['name']); ?></p>

	          <p><span>Size:</span> <?php echo __($property['Property']['size']); ?></p> 
	          <p><span>Bedroom:</span> <?php echo __($property['Property']['bedroom']); ?></p>
	          <p><span>Bathroom:</span> <?php echo __($property['Property']['bathroom']); ?></p>
	          <p><span>Furnishing:</span> <?php echo __($property['PropertyFurnishingType']['name']); ?></p> 
	           
	          <!--
  

	          <div class="">
	            <h2>Available Colors</h2>
	            <ul class="list-inline prod_color">
	              <li>
	                <p>Green</p>
	                <div class="color bg-green"></div>
	              </li>
	              <li>
	                <p>Blue</p>
	                <div class="color bg-blue"></div>
	              </li>
	              <li>
	                <p>Red</p>
	                <div class="color bg-red"></div>
	              </li>
	              <li>
	                <p>Orange</p>
	                <div class="color bg-orange"></div>
	              </li>

	            </ul>
	          </div>
	          <br />-->
	          <?php if($property['PropertyFacilitySelected']) { ?>
	          <div class="">
	            <h2>Facilities</h2>
	            <ul class="list-inline prod_size">
	            	<?php foreach($facilities as $facility) { ?> 
						<li>
							<button type="button" class="btn btn-default btn-xs"><?php echo $facility['PropertyFacility']['name']; ?></button>
						</li>
		            <?php } ?>   
	            </ul>
	          </div>
	          
	          <?php } ?>
				<br />
	          <div class="">
	            <div class="product_price">
	              <h1 class="price">RM<?php echo __(number_format($property['Property']['price'])); ?></h1>
	              
	              <br>
	            </div>
	          </div>

	          <!--<div class="">
	            <button type="button" class="btn btn-default btn-lg">Call Advertiser</button>
	            <button type="button" class="btn btn-default btn-lg">Add to Wishlist</button>
	          </div>

	          <div class="product_social">
	            <ul class="list-inline">
	              <li><a href="#"><i class="fa fa-facebook-square"></i></a>
	              </li>
	              <li><a href="#"><i class="fa fa-twitter-square"></i></a>
	              </li>
	              <li><a href="#"><i class="fa fa-envelope-square"></i></a>
	              </li>
	              <li><a href="#"><i class="fa fa-rss-square"></i></a>
	              </li>
	            </ul>
	          </div>--> 
	          </div>
	        </div> 
	        <div class="col-md-12"> 
	          <div class="" role="tabpanel" data-example-id="togglable-tabs">
	            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
	              <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Home</a>
	              </li>
	              <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Profile</a>
	              </li>
	              <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
	              </li>
	            </ul>
	            <div id="myTabContent" class="tab-content">
	              <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
	                <pre><?php echo __($property['Property']['description']); ?></pre>
	              </div>
	              <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
	                <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
	                  booth letterpress, commodo enim craft beer mlkshk aliquip</p>
	              </div>
	              <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
	                <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
	                  photo booth letterpress, commodo enim craft beer mlkshk </p>
	              </div>
	            </div>
	          </div> 
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
	function galleryImage(name) {
		var image = baseUrl + 'img/Users/Original/' + name;
		$('#displayImage').html('<img src="' + image + '" class="img-responsive">');
		return false;
	}
</script>
<?php $this->end(); ?>