<!-- page content -->
<?php
  echo $this->Html->css('/vendors/dropzone/dist/min/dropzone.min.css');
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo __($property['Property']['title']); ?> - Upload Images</h2>
         
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div id="wizard" class="form_wizard wizard_horizontal">
          <ul class="wizard_steps">
            <li>
              <a href="#" onclick="return false;">
                <span class="step_no complete">1</span>
                <span class="step_descr">
                    Step 1<br />
                    <small>Property Information</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#" onclick="return false;">
                <span class="step_no active">2</span>
                <span class="step_descr">
                    Step 2<br />
                    <small>Upload Images</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#" onclick="return false;">
                <span class="step_no">3</span>
                <span class="step_descr">
                    Step 3<br />
                    <small>You are Done!</small>
                </span>
              </a>
            </li> 
          </ul>
          </div>

      <?php echo $this->Session->flash(); ?>
      <form action="/property/dashboard/properties/initupload/<?php echo $id; ?>" method="post" enctype="multipart/form-data" class="dropzone" id="dropzoneUploader">
      </form>
      
        <br />
        <br />
        <br />
        <br /> 
        <div class="text-right"> 
        <?php echo $this->Form->create('Property', array('class'=> 'horizontal-form', 'novalidate'=>true)); ?>
        <?php echo $this->Form->hidden('id'); ?>
        <?php echo $this->Form->button('Next Step',array('class'=>'btn btn-success')); ?>
        <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
 
<!-- /page content --> 
<?php $this->start('script'); ?>
<?php 
  echo $this->Html->script('/vendors/dropzone/dist/min/dropzone.min.js');
?> 
<script type="text/javascript"> 
Dropzone.autoDiscover = false;
// or disable for specific dropzone:
// Dropzone.options.myDropzone = false;
new Dropzone("#dropzoneUploader", { 
    maxFilesize: 2, // MB
    init: function() {
        this.on("success", function(file, responseText) {
            console.log(responseText);
        });
    }
});


  //myDropzone.on("complete", function(file) {
  //  myDropzone.removeFile(file);
  //});

 
</script>

<?php $this->end(); ?>