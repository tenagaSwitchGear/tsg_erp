<!-- page content -->
 

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add New Property Listing</h2>
         
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

      <div id="wizard" class="form_wizard wizard_horizontal">
          <ul class="wizard_steps">
            <li>
              <a href="#" onclick="return false;">
                <span class="step_no active">1</span>
                <span class="step_descr">
                    Step 1<br />
                    <small>Property Information</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#" onclick="return false;">
                <span class="step_no">2</span>
                <span class="step_descr">
                    Step 2<br />
                    <small>Upload Images</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#" onclick="return false;">
                <span class="step_no">3</span>
                <span class="step_descr">
                    Step 3<br />
                    <small>You are Done!</small>
                </span>
              </a>
            </li> 
          </ul>
          </div>
      <?php echo $this->Session->flash(); ?>

      <?php echo $this->Form->create('Property', array('class' => 'form-horizontal form-label-left', 'novalidate'=>true)); ?> 
      <p>Please fill following form field to add property listing. Field mark with (*) is required.</p>  

<?php
echo $this->Form->input('user_id', array('type' => 'hidden'));
echo $this->Form->input('status', array('type' => 'hidden'));
echo $this->Form->input('slug', array('type'=> 'hidden'));

echo $this->Form->input('state_id', array(
  'empty' => 'Select State', 
  'id' => 'state', 
  'class' => 'form-control', 
  'div' => array('class' => 
    'item form-group col-xs-6'), 
  'label' => 'State <span>*</span>'));
echo $this->Form->input('city_id', array('empty' => 'Select City', 'id' => 'cityId', "class"=> "form-control", 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'City <span>*</span>'));
echo $this->Form->input('property_category_id', array('id' => 'category', 'empty' => 'Select Category', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Category <span>*</span>'));
echo $this->Form->input('property_current_occupancy_id', array('empty' => 'Select Current Occupancy', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Current Occupancy <span>*</span>'));

echo $this->Form->input('property_land_title_id', array('empty' => 'Select Land Title', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6')));
echo $this->Form->input('property_post_type_id', array('empty' => 'Select Type', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Post Type <span>*</span>'));
echo $this->Form->input('property_tenure_type_id', array('empty' => 'Select Tenure Type', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Tenure <span>*</span>'));
echo $this->Form->input('property_title_id', array('empty' => 'Select Title', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Property Title <span>*</span>'));
?>

<div id="hideCategory" style="<?php if($showHide == 2) { echo 'display: none;'; } ?>">
<?php 
echo $this->Form->input('property_furnishing_type_id', array('empty' => 'Select Furnishing Type', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Furnishing Type <span>*</span>'));
echo $this->Form->input('property_unit_type_id', array('empty' => 'Select Unit Type', 'class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Unit Type <span>*</span>'));

echo $this->Form->input('bedroom', array('class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6')));
echo $this->Form->input('bathroom', array('class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'))); 

?>
</div>

<div id="hideFacility" class="row" style="<?php if($showHide != 1) { echo 'display: none;'; } ?>">
  <div class="col-lg-12" id="checkBoxFlat">
  <?php echo $this->Form->input('PropertyFacilitySelected.property_facility_id', array(
      'type' => 'select',
      'multiple' => 'checkbox',
      'options' => $facilities,
      'class' => 'col-xs-4', 
      'label' => false
      )
  );
  ?> 
  </div>
</div>

<?php
echo $this->Form->input('title', array('class'=> 'form-control', 'div' => array('class' => 'item form-group'), 'label' => 'Title <span>*</span>'));
echo $this->Form->input('description', array('class'=> 'form-control', 'div' => array('class' => 'item form-group'), 'label' => 'Description <span>*</span>'));

echo $this->Form->input('price', array('class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Price RM<span>*</span>'));
echo $this->Form->input('size', array('class'=> 'form-control', 'div' => array('class' => 'item form-group col-xs-6'), 'label' => 'Size <span id="size">*</span>'));
?>

                    
          <div class="form-group">
            <div class="col-md-12 text-right"> 
              <?php echo $this->Form->button('<i class="fa fa-angle-right"></i> Next', array('class' => 'btn btn-success')); ?>
            </div>
          </div>
        <?php echo $this->Form->end(); ?> 
      </div>
    </div>
  </div>
</div>
           
        <!-- /page content -->


<?php $this->start('script'); ?>

<?php echo $this->Html->script('/vendors/iCheck/icheck.min.js'); ?>  
<script type="text/javascript"> 
	$(document).ready(function() { 
    $('#checkBoxFlat input[type="checkbox"').addClass('flat');
		$('#state').change(function() { 
			$.ajax({ 
				type: "GET", 
				dataType: 'json',
				data: 'state_id=' + $('#state').val(),
				url: baseUrl + 'properties/ajaxstate', 
				success: function(respond) { 
					var option = ''; 
					$.each(respond, function(i, item) {
						console.log(90); 
					    option += '<option value="' + item.id + '">' + item.name + "</option>";
			        }); 
			        $('#cityId').html(option);
				}
		    }); 
		    return false;
		}); 

    $('#category').change(function() { 
      var category = $('#category').val();
      if(category == 2 || category == 3) {
        $('#hideFacility').show();
      } else if(category >= 6) {
        $('#hideCategory').hide();
        $('#size').html('Acres *');
        $('#hideFacility').hide();
      } else {
        $('#hideFacility').hide();
        $('#hideCategory').show();
        $('#size').html('Square Feet *');
      } 
      return false;
    });  
	});
</script>
<?php $this->end(); ?>