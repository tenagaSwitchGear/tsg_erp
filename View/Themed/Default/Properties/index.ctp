<!-- page content -->
 <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add New Property Listing</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
      	<div class="table-responsive">
	<h2><?php echo __('Properties'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th> 
			<th><?php echo $this->Paginator->sort('title'); ?></th>  
			<th><?php echo $this->Paginator->sort('price'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($properties as $property): ?>
	<tr>
		<td><?php echo h($property['Property']['id']); ?>&nbsp;</td> 
		<td><?php echo h($property['Property']['title']); ?>&nbsp;</td> 
		<td>RM<?php echo h($property['Property']['price']); ?>&nbsp;</td> 
		<td><?php echo $property['Property']['status'] == 1 ? 'Active' : 'Inactive'; ?>&nbsp;</td>
		<td><?php echo h($property['Property']['created']); ?>&nbsp;</td> 
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $property['Property']['id']), array('class' => 'btn btn-success')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $property['Property']['id']), array('class' => 'btn btn-warning')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $property['Property']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $property['Property']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
       
      </div>
    </div>
  </div>
</div>
 




<!--<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Property'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Categories'), array('controller' => 'property_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Category'), array('controller' => 'property_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Current Occupancies'), array('controller' => 'property_current_occupancies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Current Occupancy'), array('controller' => 'property_current_occupancies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Furnishing Types'), array('controller' => 'property_furnishing_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Furnishing Type'), array('controller' => 'property_furnishing_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Land Titles'), array('controller' => 'property_land_titles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Land Title'), array('controller' => 'property_land_titles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Post Types'), array('controller' => 'property_post_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Post Type'), array('controller' => 'property_post_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Tenure Types'), array('controller' => 'property_tenure_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Tenure Type'), array('controller' => 'property_tenure_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Titles'), array('controller' => 'property_titles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Title'), array('controller' => 'property_titles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Unit Types'), array('controller' => 'property_unit_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Unit Type'), array('controller' => 'property_unit_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Facility Selecteds'), array('controller' => 'property_facility_selecteds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Facility Selected'), array('controller' => 'property_facility_selecteds', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Images'), array('controller' => 'property_images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Image'), array('controller' => 'property_images', 'action' => 'add')); ?> </li>
	</ul>
</div>-->
