<?php echo $this->Html->css(array('/vendors/DataTables-1.10.13/media/css/jquery.dataTables.min.css')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Planning</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
  

	<?php echo $this->Form->create('Planning', array('class' => 'form-horizontal')); ?> 
		<div class="form-group">
			<label class="col-sm-3">Note</label>
			<div class="col-sm-9">
				<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3">Status</label>
			<div class="col-sm-9">
				<?php $status = array('0' => 'Created', '1' => 'Submit For Approval'); ?>
				<?php echo $this->Form->input('status', array('options' => $status, 'empty' => '-Select Status-', 'class' => 'form-control', 'label' => false, 'required' => 'required')); ?>
			</div>
		</div>
	<h4>Job Selected</h4>
	<div class="table table-responsive">
	 <table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('#'); ?></th>
		<th><?php echo __('Substation'); ?></th>
		<th><?php echo __('Item Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Unit'); ?></th>
		<th><?php echo __('Assembly Time'); ?></th>
		<th><?php echo __('FAT Date'); ?></th>
		<th><?php echo __('Expect Start'); ?></th>
		<th><?php echo __('Type'); ?></th>
		 
		 
	</tr>
	<?php 
	$job_station = '';
	$job_item = '';
	$no = 1;
	foreach ($items as $item): ?>
		<?php $job_station = ', ' . $item['SaleJobChild']['id']; ?>
		<?php $job_item = ', ' . $item['InventoryItem']['id']; ?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $item['SaleJobChild']['station_name']; ?></td>
			<td><?php echo $item['InventoryItem']['name']; ?></td>
			<td><?php echo $item['InventoryItem']['code']; ?></td>
			<td><?php echo $item['SaleJobItem']['quantity']; ?></td>
			<td><?php echo $item['GeneralUnit']['name']; ?></td>
			<td><?php echo $item['InventoryItem']['assembly_time']; ?></td>
			<td><?php echo $item['SaleJobChild']['fat_date']; ?></td>
			<td></td>
			<td><?php echo type($item['SaleJobItem']['type']); ?></td> 
			 
		</tr> 
	<?php 
	$no++;
	endforeach; ?>
	<?php echo $this->Form->input('job_station', array('type' => 'hidden', 'value' => $job_station)); ?>
	<?php echo $this->Form->input('job_item', array('type' => 'hidden', 'value' => $job_item)); ?>
	</table>
	</div> 

	<h4>Raw Material</h4> 
	<div class="table-responsive">
	<!--    -->
	<table cellpadding = "0" id="tableScrollAble" cellspacing = "0" class="table table-bordered table-hover">
	<thead>
	<tr>  
		<th><input type="checkbox" name="checkall" id="checkall" value="1"> All</th> 
		<th><?php echo __('Item'); ?></th>  
		<th><?php echo __('On hand'); ?></th>

		<th><?php echo __('Waiting Inspect'); ?></th>
		<th><?php echo __('On hold'); ?></th>
		<th><?php echo __('On order'); ?></th>  
		
		<th><?php echo __('BOM Required'); ?></th>   <!-- BOM + PO-->
		<th><?php echo __('MRN'); ?></th>
		<th><?php echo __('Req. Qty'); ?></th>
		<th><?php echo __('Qty To Buy'); ?></th>
		<th><?php echo __('Buffer'); ?></th>
		<th><?php echo __('UOM'); ?></th>
		<th><?php echo __('Default Supplier'); ?></th> 
		<th>Min Order</th>
		<th>Conversion</th>
		<th>Lead Time</th>
		<th>Price/Unit</th>
	</tr> 
	</thead>
	<tbody>
	<?php 
	$i = 1;
	foreach ($bomitems as $bomitem): ?>
		<?php $required = ($bomitem['total'] + $bomitem['mrn']) - ($bomitem['in_stock'] + $bomitem['waiting_qc'] + $bomitem['ordered']); ?>
		<?php $balance = $required - $bomitem['balance']; ?>
		<tr> 
			<td> 
				<input class="check" id="<?php echo $i; ?>" type="checkbox" name="data[PlanningItem][check][]" value="1"> 
				<input id="check<?php echo $i; ?>" type="hidden" name="data[PlanningItem][purchase][]" value="0">
			</td>
			<td> 
			<input type="text" name="data[PlanningItem][job_list_json][]" value="<?php echo $bomitem['jobs']; ?>">
			<input type="hidden" name="data[PlanningItem][is_sub_assembly][]" value="<?php echo $bomitem['is_sub_assembly']; ?>">
			<input type="hidden" name="data[PlanningItem][item_id][]" value="<?php echo $bomitem['item_id']; ?>"> 
			<?php if(empty($bomitem['in_stock'])) { ?>
				<input type="hidden" name="data[PlanningItem][on_hand][]" value="0"> 
			<?php } else { ?>
				<input type="hidden" name="data[PlanningItem][on_hand][]" value="<?php echo $bomitem['in_stock']; ?>"> 
			<?php } ?> 
			<?php if($required > 0) { ?>
				<input type="hidden" name="data[PlanningItem][required_qty][]" value="<?php echo $required; ?>"> 
			<?php } else { ?>
				<input type="hidden" name="data[PlanningItem][required_qty][]" value="0"> 
			<?php } ?> 
			<input type="hidden" name="data[PlanningItem][bom_qty][]" value="<?php echo $bomitem['total']; ?>">
			<input type="hidden" name="data[PlanningItem][mrn_qty][]" value="<?php echo $bomitem['mrn']; ?>"> 
			 <?php echo $bomitem['code']; ?><br/>
			<small><?php echo $bomitem['name']; ?></small>
			</td> 
			<td><?php echo $bomitem['in_stock'] == NULL ? 0 : _n2($bomitem['in_stock']); ?></td>

			<td><?php echo $bomitem['waiting_qc'] == NULL ? 0 : _n2($bomitem['waiting_qc']); ?></td>

			<td><?php echo $bomitem['rejected'] == NULL ? 0 : _n2($bomitem['rejected']); ?></td>

			<td><?php echo $bomitem['ordered'] == NULL ? 0 : _n2($bomitem['ordered']); ?></td> 

			<td><?php echo _n2($bomitem['total']); ?> <small><?php echo $bomitem['unit_name']; ?></small>
			</td>  
			<td><?php echo $bomitem['mrn'] == NULL ? 0 : _n2($bomitem['mrn']); ?></td> 
			<td><?php if($required > 0) { echo _n2($required) . ' ' .  $bomitem['unit_name']; } else { echo 0; } ?></td>
			<td style="width:40px!important">  
				<input style="width:40px;" type="text" name="data[PlanningItem][planning_qty][]" value="<?php if($required > 0 && !empty($bomitem['supplier']['InventorySupplierItem'])) { echo calc_min_qty($bomitem['supplier']['InventorySupplierItem']['min_order'], $bomitem['supplier']['InventorySupplierItem']['conversion_qty'], $required); } else { echo 0; } ?>">
				<input type="hidden" name="data[PlanningItem][quantity][]" value="<?php if($required > 0 && !empty($bomitem['supplier']['InventorySupplierItem'])) { echo calc_min_qty($bomitem['supplier']['InventorySupplierItem']['min_order'], $bomitem['supplier']['InventorySupplierItem']['conversion_qty'], $required); } else { echo 0; } ?>">
			</td>

			<td style="width:40px!important"> 
				<input type="text" name="data[PlanningItem][buffer][]" value="" style="width:40px;"> 
				<input type="hidden" name="data[PlanningItem][unit][]" value="<?php echo $bomitem['unit']; ?>"> 
			</td>
			<td>
				<select name="data[PlanningItem][planning_general_unit][]" id="uom<?php echo $i; ?>" style="width:60px;" class="planningUnit">
				<option value="">-UOM-</option>
				</select>
			</td>
			<td>
				<select name="data[PlanningItem][supplier][]" id="supplier<?php echo $i; ?>" style="width:120px;" onchange="getSupplier(<?php echo $bomitem['item_id']; ?>, <?php echo $i; ?>); return false" class="supplier">
				<?php if(!empty($bomitem['supplier']['InventorySupplier']['name'])) { ?>
					<option value="<?php echo $bomitem['supplier']['InventorySupplier']['id']; ?>">
					<?php echo $bomitem['supplier']['InventorySupplier']['name']; ?>
					</option> 
				<?php } ?>
					<option value="">-Related Supplier-</option>
				<?php foreach($bomitem['suppliers'] as $supplier) { ?>
					<option value="<?php echo $supplier['InventorySupplier']['id']; ?>">
					<?php echo $supplier['InventorySupplier']['name']; ?>
					</option>
				<?php } ?> 
					<option value="">-Others Supplier-</option> 
				</select> 
				 
			<?php if(!empty($bomitem['supplier']['InventorySupplierItem'])) { ?>
				<input type="hidden" name="data[PlanningItem][inventory_supplier_item_id][]" value="<?php echo $bomitem['supplier']['InventorySupplierItem']['id']; ?>" id="priceBook<?php echo $i; ?>"> 
			<?php } else { ?>
				<input type="hidden" name="data[PlanningItem][inventory_supplier_item_id][]" value="0" id="priceBook<?php echo $i; ?>">
			<?php } ?>
			</td>
			<td>
			<p id="minOrder<?php echo $i; ?>"><?php if(!empty($bomitem['supplier']['InventorySupplierItem'])) { ?>
				<?php echo _n2($bomitem['supplier']['InventorySupplierItem']['min_order']); ?><br/>
				<small><?php echo $bomitem['supplier']['MinOrderUnit']['name']; ?></small>
<input class="MinOrderUnit" type="hidden" value="<?php echo $bomitem['supplier']['MinOrderUnit']['id']; ?>" id="MinOrderUnit<?php echo $i; ?>">
			<?php } else { 
				echo 'N/A'; ?>
<input class="MinOrderUnit" type="hidden" value="" id="MinOrderUnit<?php echo $i; ?>">


			<?php } ?></p>
			</td>
			<td>
			<p id="conversion<?php echo $i; ?>">
			<?php if(!empty($bomitem['supplier']['InventorySupplierItem'])) { ?>
				1 <?php echo $bomitem['supplier']['MinOrderUnit']['name']; ?> = 
				<?php echo _n2($bomitem['supplier']['InventorySupplierItem']['conversion_qty']); ?> 
				<small><?php echo $bomitem['supplier']['UnitConversion']['name']; ?></small>
			<?php } else { 
				echo 'N/A'; 
			} ?></p>
			</td>
			<td>
			<p id="leadTime<?php echo $i; ?>"><?php if(!empty($bomitem['supplier']['InventorySupplierItem'])) { ?>
				<?php echo $bomitem['supplier']['InventorySupplierItem']['lead_time']; ?> Days
			<?php } else { 
				echo 'N/A'; 
			} ?></p>
			</td>
			<td>
			<p><?php if(!empty($bomitem['supplier']['InventorySupplierItem'])) { ?>
				<span id="symbol<?php echo $i; ?>"><?php echo $bomitem['supplier']['GeneralCurrency']['iso_code']; ?></span>
				<span id="price<?php echo $i; ?>"><?php echo _n2($bomitem['supplier']['InventorySupplierItem']['price_per_unit']); ?></span> 
			<?php } else { ?>
				<span id="symbol<?php echo $i; ?>"></span>
				<span id="price<?php echo $i; ?>"></span>
			<?php } ?></p>
			</td>
		</tr> 
	<?php 
	$i++;
	endforeach; ?> 
	</tbody>
		<tr>
			<td colspan="13">
				<p>Review and Click Export To Planning button. You can add, edit items later.</p>
			</td> 
			<td><?php echo $this->Form->submit('Export', array('div' => false, 'class' => 'btn btn-success', 'id' => 'submit')); ?></td>
		</tr> 
		<?php $this->Form->end(); ?>
	</table>
	</div>

</div> 
</div> 
</div> 
</div> 

<?php $this->start('script'); ?>
<?php echo $this->Html->script(array(
        '/vendors/DataTables-1.10.13/media/js/jquery.dataTables.min.js' 
      )); ?> 
<script type="text/javascript">

$(document).ready(function() {
	$('#tableScrollAble').DataTable( {
        "scrollY": 540,
        "scrollX": true,
        "paging":   false,
        "ordering": false,
        "info":     false
    });

	/*$('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        alert("Please check at least one item to purchase.");
        return false;
      } 
    });*/

    var json_unit =<?php echo json_encode($unitJson); ?>;
	$.each(json_unit, function (index, data) {
		$('.planningUnit').each(function() {
			var html = '<option value="'+data.id+'">'+data.name+'</option>';
	        $(this).append(html);
	    });
	});

	var orderUnit = 1;
	$('.MinOrderUnit').each(function() {
		$('#uom' + orderUnit).val($(this).val());
		orderUnit++;
	});

	
}); 

function getSupplier(item_id, row) {
	//console.log(item_id + ' - ' + $('#supplier'+row).val());
	if($('#supplier'+row).val() != '') {
		$.ajax({
	        type: "GET",                        
	        url:baseUrl + 'inventory_supplier_items/ajaxfinditem',           
	        contentType: "application/json",
	        dataType: "json",
	        data: "inventory_item_id=" + item_id + "&supplier_id=" + $('#supplier'+row).val(), 
	        success: function (json) { 
	        	if(json.InventorySupplierItem != undefined) {
	        		$('#conversion'+row).html(json.InventorySupplierItem['conversion_qty'] + '<br/>' + '<small>' + json.UnitConversion['name'] + '</small>'); 
		        	$('#minOrder'+row).html(json.InventorySupplierItem['min_order'] + '<br/>' + '<small>' + json.MinOrderUnit['name'] + '</small>');
		        	$('#leadTime'+row).html(json.InventorySupplierItem['lead_time'] + ' Days');
		        	$('#price'+row).html(json.InventorySupplierItem['price_per_unit']);
		        	$('#symbol'+row).html(json.GeneralCurrency['iso_code']);	
		        	$('#priceBook' + row).val(json.InventorySupplierItem['id']);
	        	} else {
	        		$('#minOrder'+row).html('Not Found');
		        	$('#leadTime'+row).html('Not Found');
		        	$('#price'+row).html('Not Found');
		        	$('#symbol'+row).html('Not Found');
		        	$('#priceBook' + row).val(0);
	        	} 
	    	}
	    });	
	}
	
} 

$(document).ready(function() {
	$('#submit').click(function() {
		var checked = $("input[type=checkbox]:checked").length; 
		if(checked == 0) {
			alert("You must check at least one item.");
			return false;
		}  
	});

	$("#checkall").change(function () {  
	    $(".check").prop('checked', $(this).prop("checked"));  
	    $(".check").each(function() {  
			var getId = $(this).attr('id');
			var getVal = $("#check" + getId).val();
			if($(this).prop("checked") == true) {
				$("#check" + getId).val(1);
			} else {
				$("#check" + getId).val(0);
			}  
		});
	});

	$(".check").each(function() {
		$(this).change(function() {
			var getId = $(this).attr('id');
			var getVal = $("#check" + getId).val();
			if(getVal == 0) {
				$("#check" + getId).val(1);
			} else {
				$("#check" + getId).val(0);
			} 
		}); 
	});
});
</script>
<?php $this->end(); ?>
 

<?php

	function calc_min_qty($min_order, $conversion, $required_qty) {
		if($min_order != NULL && $conversion != NULL) {
			$supply = $min_order * $conversion;
			if($supply >= $required_qty) {
				return _n2($min_order);
			} else {
				$shortage = $required_qty - $supply;

				$buffer = ceil($shortage / $conversion);

				$total = $min_order + $buffer;
				return _n2($total);
			}	
		} else {
			return 0;
		}
		
	}
	function status($status) {
		if($status == 0) {
			return 'Draft';
		} else if($status == 1) {
			return 'Waiting HOS Verification';
		} else if($status == 2) {
			return 'Waiting HOD Verification';
		} else if($status == 3) {
			return 'Waiting GM Approval';
		} else if($status == 4) {
			return 'Verified';
		} else if($status == 5) {
			return 'Rejected';
		}
	}

	function type($type) {
		if($type == 1) {
			return 'Finished Goods';
		} else {
			return 'Raw Material';
		} 
	}

	function start_production($fat, $period) {
		$start = date($fat, strtotime('-'.$period.' days'));
		$start = new DateTime($start);
		$end = new DateTime($fat);
		// otherwise the  end date is excluded (bug?)
		$end->modify('+1 day'); 

		$interval = $end->diff($start);

		// total days
		$days = $interval->days;

		// create an iterateable period of date (P1D equates to 1 day)
		$period = new DatePeriod($start, new DateInterval('P1D'), $end);
 

		foreach($period as $dt) {
		    $curr = $dt->format('D');

		    // substract if Saturday or Sunday
		    if ($curr == 'Sat' || $curr == 'Sun') {
		        $days--;
		    } 
		} 

		return $start;
	}


?>


