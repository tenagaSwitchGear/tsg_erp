<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit: <?php echo $planning['Planning']['name']; ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

		<div class="plannings form">
		<?php echo $this->Form->create('Planning', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id'); ?>
		<?php echo $this->Form->input('name', array('type' => 'hidden')); ?>
			 
		 
			<div class="form-group">
				<label class="col-sm-3">Note (Optional)</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Status</label>
				<div class="col-sm-9">
					<?php $status = array('0' => 'Created', '1' => 'Submit For Verification'); ?>
					<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			 
		    <div class="form-group"> 
		    	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
		    </div>	  
			<?php $this->Form->end(); ?>

		</div> 
	</div>
	</div>
	</div>
</div>

 