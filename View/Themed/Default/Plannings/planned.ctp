<?php echo $this->Html->link('Plan Order', array('action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link('Job Lists', array('action' => 'job'), array('class' => 'btn btn-primary')); ?>  
<?php echo $this->Html->link('Planned Job Lists', array('action' => 'planned'), array('class' => 'btn btn-success')); ?>  

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Planned Order</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="table-responsive">
	<p>Jobs below already exported to Plan Order.</p>
	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr>	 
		<th><?php echo $this->Paginator->sort('station_name'); ?></th>  
		<th><?php echo $this->Paginator->sort('name'); ?></th>     
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('fat_date'); ?></th>
		<th><?php echo $this->Paginator->sort('delivery_date'); ?></th> 
		<th><?php echo $this->Paginator->sort('status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody> 
	<?php foreach ($plannings as $planning): ?>
	<tr> 
		<td><?php echo h($planning['SaleJobChild']['station_name']); ?>&nbsp;</td> 
		<td><?php echo h($planning['SaleJobChild']['name']); ?>&nbsp;</td> 
		   
		<td><?php echo h($planning['SaleJob']['created']); ?>&nbsp;</td>
		<td><?php echo h($planning['SaleJobChild']['fat_date']); ?>&nbsp;</td>
		<td><?php echo h($planning['SaleJobChild']['delivery_date']); ?>&nbsp;</td> 
		<td><?php echo h($planning['SaleJobChild']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('controller' => 'sale_jobs', 'action' => 'view', $planning['SaleJob']['id'])); ?>  
		</td>
	</tr>
	<?php endforeach; ?>
	
	 	    
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
		<?php
		  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
		  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
		?>
	</ul>
</div>
 
</div>
</div>
</div>
</div>