<?php echo $this->Html->link('Plan Order', array('action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link('Job Lists', array('action' => 'job'), array('class' => 'btn btn-primary')); ?>  
<?php echo $this->Html->link('Planned Job Lists', array('action' => 'planned'), array('class' => 'btn btn-success')); ?>  
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Plan Order</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 

	<?php echo $this->Html->link('Draft', array('action' => 'index'), array('class' => 'btn btn-default')); ?>  
	<?php echo $this->Html->link('Waiting Approval', array('action' => 'index/1'), array('class' => 'btn btn-default')); ?>  
	<?php echo $this->Html->link('Approved', array('action' => 'index/2'), array('class' => 'btn btn-default')); ?>  
	<?php echo $this->Html->link('Rejected', array('action' => 'index/3'), array('class' => 'btn btn-default')); ?>
	<?php echo $this->Html->link('Submitted To Procurement', array('action' => 'index/4'), array('class' => 'btn btn-default')); ?>  
	<?php echo $this->Html->link('Purchased', array('action' => 'index/5'), array('class' => 'btn btn-default')); ?>    
	<?php echo $this->Session->flash(); ?>
 
	<?php echo $this->Form->create('Planning', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Planning No', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('from', array('type' => 'text', 'id' => 'dateonly', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('to', array('type' => 'text', 'id' => 'dateonly_2', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>

	<table cellpadding="0" cellspacing="0" class="table">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name', 'Plan No'); ?></th>     
			<th><?php echo $this->Paginator->sort('created'); ?></th> 
			<th><?php echo $this->Paginator->sort('user_id'); ?></th> 
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($plannings as $planning): ?>
	<tr> 
		<td><?php echo h($planning['Planning']['name']); ?>&nbsp;</td>
		 
		 
		<td><?php echo h($planning['Planning']['created']); ?>&nbsp;</td> 
		<td>
			<?php echo $this->Html->link($planning['User']['username'], array('controller' => 'users', 'action' => 'view', $planning['User']['id'])); ?>
		</td> 
		<td><?php echo status($planning['Planning']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Import Data'), array('action' => 'importedreview', $planning['PlanningCapture']['id'])); ?>
			<?php echo $this->Html->link(__('View'), array('action' => 'planorderreview', $planning['Planning']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $planning['Planning']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $planning['Planning']['id']), array(), __('Are you sure you want to delete # %s?', $planning['Planning']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
 
</div>
</div>
</div>
</div>

<?php 

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	}
	if($status == 1) {
		$data = 'Submitted For Approval';
	}
	if($status == 2) {
		$data = 'Approved';
	}
	if($status == 3) {
		$data = 'Rejected';
	}
	if($status == 4) {
		$data = 'Submitted To Procurement';
	} 
	if($status == 5) {
		$data = 'Purchased';
	} 
	return $data;
}

?>
