<?php echo $this->Html->link('Back', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Html->link('Review Purchase Requisition', array('action' => 'planorderreview', $plan['PlanningCaptureItem']['planning_id']), array('class' => 'btn btn-success')); ?>  

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Planning</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 
	<h4>Raw Material</h4> 
	<p><b>Note:</b> Please review every item and change the Buffer quantity (if required) until you get the values of X = Y</p>
	 
	<div class="table-responsive">
	<!--    -->
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	 
	<tbody style="overflow-y: scroll">
	<?php  
	foreach ($items as $bomitem): ?> 
		<?php 
		$onhand = !empty($bomitem['onhand']) ? $bomitem['onhand'] : 0;
		$onhold = !empty($bomitem['onhold']) ? $bomitem['onhold'] : 0;
		$ordered = !empty($bomitem['ordered']) ? $bomitem['ordered'] : 0;
		$production =  $bomitem['production']; 

		$min_order = !empty($bomitem['pricebook']['min_order']) ? $bomitem['pricebook']['min_order'] : 0;
		$conversion_qty = !empty($bomitem['pricebook']['conversion_qty']) ? $bomitem['pricebook']['conversion_qty'] : 0; 
		$required = ($bomitem['required']) - ($onhand + $onhold + $ordered); 
		 
		if($required > 0) { 
			$qty_to_purchase = calc_min_qty($min_order, $conversion_qty, $required); 
		} else { 
			$qty_to_purchase = 0; 
		} 
		$total_to_purchase = $bomitem['pricebook']['conversion_qty'] * $qty_to_purchase;
		?>
		 
		<form id="formdata-<?php echo $bomitem['id']; ?>" onsubmit="return submitData(<?php echo $bomitem['id']; ?>)" method="POST">

		<tr style="background: #cecece;">   
			<td width="200">Item</td>  
			<td>Qty</td>
			<td style="width:60px">Purchase</td>
			<td style="width:60px">X</td>
			<td>Onhand</td>
			<td>Ordered</td>
			<td>Onhold</td>
			<td>Alc</td> 
			<td>Supplier</td>
			<td>Price</td>
			<td>Unt/Price</td>   
			<td>MOQ</td>
			<td>Conv.</td>
			<td>Lead</td>  
			<td>Action</td>
		</tr> 
		<tr style="background: #cecece; padding:0;">  
			<td width="200"> 
			 <small><?php echo $bomitem['code']; ?><br/>
			  <?php echo $bomitem['name']; ?></small>
			 
			</td> 
			
			<td> 
			<small><?php echo $bomitem['required']; ?> <?php echo $bomitem['unit']; ?></small>
			</td>

			
			<input type="hidden" name="required" value="<?php echo $bomitem['required']; ?>">
			<input type="hidden" name="planning_general_unit" value="<?php echo $bomitem['supplier_unit']['id']; ?>">			  
			<input type="hidden" name="general_unit_id" value="<?php echo $bomitem['general_unit_id']; ?>">
			<input type="hidden" name="is_sub_assembly" value="<?php echo $bomitem['is_sub_assembly']; ?>">
			<input type="hidden" name="item_id" value="<?php echo $bomitem['inventory_item_id']; ?>">  
			<input type="hidden" name="on_hand" value="<?php echo $onhand; ?>">  
			<input type="hidden" name="ordered" value="<?php echo $ordered; ?>">  
			<input type="hidden" name="production" value="<?php echo $production; ?>"> 

			<input type="hidden" name="planning_id" value="<?php echo $planning['PlanningCapture']['planning_id']; ?>">

			<td style="width:60px">
			<input class="plan_qty" id="<?php echo $bomitem['id']; ?>" style="width:60px" type="text" name="planning_qty" value="<?php echo $qty_to_purchase; ?>">

			

			<input id="conv_qty-<?php echo $bomitem['id']; ?>" type="hidden" value="<?php echo $bomitem['pricebook']['conversion_qty']; ?>">
			<small><?php echo $bomitem['supplier_unit']['name']; ?></small></td>
			  
			<td style="width:60px">
			<input id="hide_conv_qty-<?php echo $bomitem['id']; ?>" name="total_sum" type="hidden" value="<?php echo $total_to_purchase; ?>">

			 <small><span id="total_qty-<?php echo $bomitem['id']; ?>"><?php echo $total_to_purchase; ?></span> <?php echo $bomitem['unit']; ?></small>
			 </td> 
			 <td>
			 <small class="onhand" id="onhand-<?php echo $bomitem['inventory_item_id']; ?>"><?php echo !empty($bomitem['onhand']) ? $bomitem['onhand'] : 0; ?></small>
			</td>
			<td> 
			 <small class="onorder" id="onorder-<?php echo $bomitem['inventory_item_id']; ?>"><?php echo !empty($bomitem['ordered']) ? $bomitem['ordered'] : 0; ?></small>
			</td>
			<td> 
			 <small class="onhold" id="onhold-<?php echo $bomitem['inventory_item_id']; ?>"><?php echo !empty($bomitem['onhold']) ? $bomitem['onhold'] : 0; ?></small>
			</td>
			<td>  
			<small><?php echo $bomitem['production']; ?></small>
			</td> 
			<td>
			<select name="supplier" id="supplier<?php echo $bomitem['id']; ?>" style="width:160px;" onchange="getSupplier(<?php echo $bomitem['inventory_item_id']; ?>, <?php echo $bomitem['id']; ?>);" class="supplier">
			 <option value="<?php echo $bomitem['supplier']['id']; ?>"><?php echo $bomitem['supplier']['name']; ?></option> 
			 
		 	<?php foreach($bomitem['suppliers'] as $supplier) { ?>
				<option value="<?php echo $supplier['InventorySupplier']['id']; ?>">
				<?php echo $supplier['InventorySupplier']['name']; ?>
				</option>
			<?php } ?> 
			 </select>
			</td>
			<td style="width:60px">
			<input class="price_supplier" id="priceSupplier-<?php echo $bomitem['id']; ?>" style="width:60px" type="text" name="price" value="<?php echo $bomitem['unit_price']; ?>" name="price">  
			</td> 
			<?php 
			if(!empty($bomitem['unit_price']) && !empty($bomitem['pricebook']['conversion_qty']) && $bomitem['pricebook']['conversion_qty'] != 0 && $bomitem['unit_price'] != 0) {
				$each_price = $bomitem['unit_price'] / $bomitem['pricebook']['conversion_qty'];
			} else {
				$each_price = 0;
			}
			 
			$each_price = number_format($each_price, 2, '.', '');
			?>
			<td style="width:60px" id="unitPrice-<?php echo $bomitem['id']; ?>">
			<input class="unit_price_supplier" id="unit_price_supplier-<?php echo $bomitem['id']; ?>" style="width:60px" type="text" name="unit_price" value="<?php echo $each_price; ?>">  
			</td> 
			 
			<td>  
			 <small><?php echo $bomitem['pricebook']['min_order']; ?> <?php echo $bomitem['supplier_unit']['name']; ?></small> 
			</td>
			<td>  
			 <small><?php echo $bomitem['pricebook']['conversion_qty']; ?> <?php echo $bomitem['unit']; ?>/<?php echo $bomitem['supplier_unit']['name']; ?></small> 
			</td>

			<td> 
			 <small><?php echo $bomitem['pricebook']['lead_time']; ?> Days</small>
			</td>
			<td> 
			<input type="hidden" name="general_currency_id" value="<?php echo $bomitem['SupplierCurrency']['id']; ?>">
			<input type="hidden" name="inventory_supplier_item_id" value="<?php echo $bomitem['pricebook']['id']; ?>">
			<input type="hidden" name="planning_capture_id" value="<?php echo $bomitem['planning_capture_id']; ?>"> 	
			<input type="submit" name="submit" value="BUY" class="btn btn-xs btn-success">
			</td>
		</tr>   
		<tr id="ajaxMsg-<?php echo $bomitem['id']; ?>"></tr>
		<tr style="padding:0;"> 
			<td colspan="15" style=""> 
				<table class="table-bordered" style="width: 100%;"> 
<tr> 
			<td width="10">
			P
			</td>  
			<td width="80">
			Job
			</td> 
			<td>FAT  
			</td>
			<td> 
			Qty
			</td>
			<td>Plan Qty   
			</td>
			<td>Buffer  
			</td>
			<td>Y: <span class="sumQtyTr" id="sumQtyTr-<?php echo $bomitem['id']; ?>"></span>  
			</td>
			<td>Delivery 
			</td> 
			<td>U.Price (<?php echo $bomitem['SupplierCurrency']['iso_code']; ?>) 
			</td> 
			<td>S.Total 
			</td> 
			<!--<td>Budget(RM)</td> -->
			<td>RM  
			</td> 
		</tr>	

			<?php foreach ($bomitem['similars'] as $itm) { ?>
			<tr> 
			<td style="width:10px;">
			<input name="checkitem[]" type="checkbox" id="check-<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" class="checkitem" value="1"checked> 

			<input name="is_buy[]" type="hidden" id="is_buy-<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" value="1"> 

			</td>  
			<td style="width:80px;"><small><?php echo $itm['SaleJobChild']['station_name']; ?></small></td> 
			<td style="width:60px;">
			<small><?php echo $itm['SaleJobChild']['fat_date']; ?></small>
			</td> 
			<td style="width:80px;"> 
			 <small><?php echo $itm['Quantity']; ?> <?php echo $bomitem['unit']; ?></small>
			</td>
			<td style="width:60px;"><input type="text" value="<?php echo $itm['Quantity']; ?>" style="width:60px;"readonly> 
			<input class="quantity" id="quantity-<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" type="hidden" name="quantity[]" value="<?php echo $itm['Quantity']; ?>">  
			</td>
			<td style="width:60px;"><input class="buffer" id="buffer-<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" type="text" name="buffer[]" value="" style="width:60px;">   
			</td>
			<td style="width:60px;"><input class="totalqty sumLineTotal-<?php echo $bomitem['id']; ?>" id="totalqty-<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" type="text" name="totalqty[]" value="<?php echo $itm['Quantity']; ?>" style="width:60px;">   
			</td>
			<?php 
			$lead = $bomitem['pricebook']['lead_time']; 
			$expect_purchase = 5 + $lead;
			$expect_deliver = date('Y-m-d', strtotime("+$expect_purchase days"));
			

			?>
			<td style="width:100px;"><input style="width:100px;" type="text" class="date" name="partial_date[]" value="<?php echo $expect_deliver; ?>">	
			</td>
			<td style="width:180px;"> 
			<?php 
			$rm = $bomitem['SupplierCurrency']['rate'] * $each_price; 
			$total_price = $rm * $itm['Quantity']; 
			?> 
			<input style="width:100px" type="text" name="sub_price[]" class="unit-price-id unit_price_supplier-line<?php echo $bomitem['id']; ?>" id="jobsub-<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" value="<?php echo $each_price; ?>"> 

			<input type="hidden" id="rate<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" class="rate" value="<?php echo $bomitem['SupplierCurrency']['rate']; ?>"> 
			</td> 

			<td style="width:100px"><input style="width:100px" type="text" id="subtotalprice<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" value="<?php echo $each_price * $itm['Quantity']; ?>">  
			</td> 
			<!--<td>
			<div class="budget" id="budget-<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $itm['PlanningCaptureItem']['id']; ?>">
			</div>
			</td> -->
			<td style="width:100px"><input style="width:100px" type="text" id="total_price_rm<?php echo $itm['SaleJobChild']['id']; ?>-<?php echo $bomitem['id']; ?>" name="total_price_rm[]" value="<?php echo $total_price; ?>">  
			</td> 

		</tr>
		<input type="hidden" name="sale_job_child_id[]" value="<?php echo $itm['SaleJobChild']['id']; ?>">	
		
		
		<input type="hidden" name="required_qty[]" value="<?php echo $itm['Quantity']; ?>"> 
			 
					<?php } ?>

				</table> 
			</td> 
		</tr>

		</form>
	<?php endforeach; ?> 
	</tbody>
	 	 
	</table> 

	</div>
<?php echo $this->Html->link(__('Back'), array('action' => 'importedreview/'.$id.'?page='.$back), array('class' => 'btn btn-primary btn-sm')); ?>
<?php echo $this->Html->link(__('Next'), array('action' => 'importedreview/'.$id.'?page='.$next), array('class' => 'btn btn-primary btn-sm')); ?>
</div> 
</div> 
</div> 
</div> 

<?php $this->start('script'); ?>
 
<script type="text/javascript">

function sum(id) {  
	var totalqty = 0;
	$('.sumLineTotal-'+id).each(function() {
		quantity = parseInt($(this).val());
		if (!isNaN(quantity)) {
            totalqty += quantity;
        }
	}); 
    $('#sumQtyTr-'+id).html(totalqty);  

}

$(document).ready(function() {   


	$('.sumQtyTr').each(function() {
		var totalqty=0;
		var element = $(this).attr('id'); 
		var itemId = element.split('-');
		var id = itemId[1];
		$('.sumLineTotal-'+id).each(function() {
			quantity = parseInt($(this).val());
			if (!isNaN(quantity)) {
	            totalqty += quantity;
	        }
		}); 
        $('#sumQtyTr-'+id).html(totalqty);  
	});

	$('.price_supplier').each(function() {
		$(this).on('keyup', function() {
			var element = $(this).attr('id'); 
			var itemId = element.split('-'); 
			var id = itemId[1];
			var conversion = $('#conv_qty-'+id).val();
			//if(conversion != '' || conversion != 0) {
				var total = parseInt($(this).val()) / parseInt(conversion);
			//}
			$('.unit_price_supplier-line'+id).val(total); 
			$('#unit_price_supplier-'+id).val(total);

  			$('.unit_price_supplier-line'+id).each(function() { 
  				var elmn = $(this).attr('id'); 
  				var itemId = elmn.split('-');
				var id = itemId[1] + '-' + itemId[2];
  				var lineQty = parseFloat($('#totalqty-'+id).val());
  				var subTotal = parseFloat($('#subtotalprice'+id).val());
  				var rate = parseFloat($('#rate'+id).val());
  				var subtotalprice = lineQty * total;
  				var totalrm = rate * subtotalprice;
  				$('#subtotalprice'+id).val(subtotalprice);
  				$('#total_price_rm'+id).val(totalrm); 
  			}); 
  			
		});
	});

	$('.unit_price_supplier').each(function() {
		$(this).on('keyup change', function() {
			var element = $(this).attr('id'); 
			var itemId = element.split('-'); 
			var id = itemId[1];
			$('.unit_price_supplier-line'+id).val($(this).val()); 
		});
	});

	$('.buffer').each(function() {
		$(this).on('keyup change', function() {
			var id = $(this).attr('id').replace('buffer-', ''); 
			var splitId = id.split('-');
			var sumQtyId = splitId[1];

			var buffer = parseFloat($(this).val());
			var quantity = parseFloat($('#quantity-'+id).val()); 
			 
			var total = buffer + quantity;
			var unitprice = parseFloat($('#jobsub-'+id).val());
			var subtotalprice = total * unitprice;

			$('#totalqty-' + id).val(parseFloat(total.toFixed(2))); 
			$('#subtotalprice'+id).val(parseFloat(subtotalprice.toFixed(2)));

			var rate = parseFloat($('#rate'+id).val());
			var totalrm = subtotalprice * rate;
			$('#total_price_rm'+id).val(parseFloat(totalrm.toFixed(2)));

			var totalqty=0;
			$('.sumLineTotal-'+sumQtyId).each(function() {
				quantity = parseInt($(this).val());
				if (!isNaN(quantity)) {
		            totalqty += quantity;
		        }
			}); 
	        $('#sumQtyTr-'+sumQtyId).html(totalqty);  

		});
	});
	// plan_qty- conv_qty- total_qty-
	$('.plan_qty').each(function() {
		$(this).on('keyup', function() {
			var id = $(this).attr('id'); 
			var conversion = parseFloat($('#conv_qty-'+id).val());
			var total = parseFloat($(this).val()) * conversion;
			$('#total_qty-' + id).html(total);
			$('#hide_conv_qty-' + id).val(total);
		});
	});

	$('.date').datepicker({
      dateFormat: 'yy-mm-dd', 
      ampm: true
    });

	 $('.budget').each(function() {
	 	var attrid = $(this).attr('id');
	 	$.ajax({
	        type: "GET",                        
	        url:baseUrl + 'project_budgets/ajaxfindbudgetbyid',           
	        contentType: "application/json",
	        dataType: "json",
	        data: "data=" + attrid, 
	        success: function (json) { 
	        	var html = ''; 
	        	 
	        	//$('#budget-'+json.id).html(json.total);
	    	}
	    });
	 });

	 $("#checkall").change(function () {  
	    $(".check").prop('checked', $(this).prop("checked"));  
	    $(".check").each(function() {  
			var getId = $(this).attr('id');
			var getVal = $("#check" + getId).val();
			if($(this).prop("checked") == true) {
				$("#check" + getId).val(1);
			} else {
				$("#check" + getId).val(0);
			}  
		});
	});

	 /*
 

	 */

	$(".checkitem").each(function() {
		$(this).change(function() {

			var getId = $(this).attr('id'); 
			getId = getId.split('-');
			var idElement = getId[1] + "-" + getId[2];
			var getVal = $("#is_buy-" + idElement).val(); 

			if(getVal == 0) {
				$("#is_buy-" + idElement).val(1);
			} else {
				$("#is_buy-" + idElement).val(0);
			} 
			if($("#is_buy-" + idElement).val() == 0) {
				var quantity = $("#quantity-" + idElement).val();
				quantity = 0 - quantity;
				$("#buffer-" + idElement).val(quantity);
				$("#totalqty-" + idElement).val(0);
				sum(getId[2]);
			} else {
				var quantity = $("#quantity-" + idElement).val(); 
				$("#buffer-" + idElement).val('');
				$("#totalqty-" + idElement).val(quantity);
				sum(getId[2]);
			}
		}); 
	});  
}); 
function submitData(id) {
	//$("form").serialize()
	var data = $('#formdata-' + id).serialize();
	// /console.log(data);
	$.ajax({
        type: "GET",                        
        url:baseUrl + 'plannings/ajaxsaveplanorder',           
        contentType: "application/json",
        dataType: "json",
        data: $('#formdata-' + id).serialize(),  
        success: function (json) { 
        	console.log(json);
        	var html = '';
        	if(json.status == false) {
        		html += '<td colspan="14" class="red-bg">' + json.msg + '</td>';
        	} else {
        		html += '<td colspan="14" class="green-bg">' + json.msg + '</td>';
        	}   
        	$('#ajaxMsg-'+id).html(html);
    	}, error: function(e) {
    		console.log(e);
    	}
    });	
    return false;
}


function getSupplier(item_id, row) {
	//console.log(item_id + ' - ' + $('#supplier'+row).val());

	if($('#supplier'+row).val() != '') {
		$.ajax({
	        type: "GET",                        
	        url:baseUrl + 'inventory_supplier_items/ajaxfinditem',           
	        contentType: "application/json",
	        dataType: "json",
	        data: "inventory_item_id=" + item_id + "&supplier_id=" + $('#supplier'+row).val(), 
	        success: function (json) { 
	        	console.log(json);
	        	if(json.InventorySupplierItem != undefined) {
	        		var html = json.GeneralCurrency.iso_code + ' ' + json.InventorySupplierItem.price_per_unit
	        		$('#priceBook-'+row).html('');
	        	} else {
	        		 
	        	} 
	    	}
	    });	
	} 
} 

</script>
<?php $this->end(); ?>
 

<?php

	function calc_min_qty($min_order, $conversion, $required_qty) {
		if($min_order > 0  && $conversion > 0) {
			$supply = $min_order * $conversion;
			if($supply >= $required_qty) {
				return $min_order;
			} else {
				$shortage = $required_qty - $supply;

				$buffer = ceil($shortage / $conversion);

				$total = $min_order + $buffer;
				return $total;
			}	
		} else {
			return 0;
		}
		
	}
	function status($status) {
		if($status == 0) {
			return 'Draft';
		} else if($status == 1) {
			return 'Waiting HOS Verification';
		} else if($status == 2) {
			return 'Waiting HOD Verification';
		} else if($status == 3) {
			return 'Waiting GM Approval';
		} else if($status == 4) {
			return 'Verified';
		} else if($status == 5) {
			return 'Rejected';
		}
	}

	function type($type) {
		if($type == 1) {
			return 'Finished Goods';
		} else {
			return 'Raw Material';
		} 
	}

	function start_production($fat, $period) {
		$start = date($fat, strtotime('-'.$period.' days'));
		$start = new DateTime($start);
		$end = new DateTime($fat);
		// otherwise the  end date is excluded (bug?)
		$end->modify('+1 day'); 

		$interval = $end->diff($start);

		// total days
		$days = $interval->days;

		// create an iterateable period of date (P1D equates to 1 day)
		$period = new DatePeriod($start, new DateInterval('P1D'), $end);
 

		foreach($period as $dt) {
		    $curr = $dt->format('D');

		    // substract if Saturday or Sunday
		    if ($curr == 'Sat' || $curr == 'Sun') {
		        $days--;
		    } 
		} 

		return $start;
	}


?>


