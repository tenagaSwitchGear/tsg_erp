<?php echo $this->Html->link('Purchase Planning', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Plan Purchase: <?php echo h($planning['Planning']['name']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
<?php

$group = $this->Session->read('Auth.User.group_id');

$role = $this->Session->read('Auth.User.role');
 
?>
 

<div class="plannings view-data"> 
	<dl> 
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['name']); ?>
			&nbsp;
		</dd> 
		 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planning['User']['username'], array('controller' => 'users', 'action' => 'view', $planning['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($planning['Planning']['status']); ?>
			&nbsp;
		</dd>
		 
	</dl>
</div>


 
<div class="related">
<h3>Budget</h3>
<table class="table table-bordered">
	<tr>  
		<th>Job No</th> 
		<th>Indirect Cost</th>
		<th>Soft Cost</th>
		<th>Material Budget</th> 

		<th>Used</th> 
		<th>Balance</th>   
		<th>Require Amount</th>  
		<th>Shortage</th> 
		<th>Action (New Tab)</th> 
	</tr> 
<?php foreach ($job_budgets as $budget) { ?>
	<tr>  
		<td><?php echo $budget['SaleJobChild']['station_name']; ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['indirect_cost']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['soft_cost']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['material_cost']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['used']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['balance']); ?></td>   
		<td <?php if($budget['RequireBudget'] > $budget['ProjectBudget']['material_cost']) { ?>class="red-bg"<?php } ?>><?php echo _n2($budget['RequireBudget']); ?></td> 
		<td>
		<?php if($budget['RequireBudget'] > $budget['ProjectBudget']['material_cost']) { 
			echo $budget['RequireBudget'] - $budget['ProjectBudget']['material_cost'];
		} else {
			echo 'N/A';
		} ?>
		</td>   
		<th>
		<?php if($budget['RequireBudget'] > $budget['ProjectBudget']['material_cost']) { 
			echo $this->Html->link('Transfer Budget', array('action' => 'job'), array('class' => 'btn btn-danger btn-xs'));
		} else {
			echo 'N/A';
		} ?>
		</th>
	</tr> 
 
<?php } ?>
</table>
	<h3><?php echo __('Plan Order Items'); ?></h3>  
	
	<?php echo $this->Form->create('InventoryPurchaseRequisition', array('class' => 'form-horizontal')); ?> 
	<?php  
	foreach ($suppliers as $supplier): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	
	<tr style="background: #cecece;">   
		<td>
		<p><?php echo $supplier['InventorySupplier']['name']; ?></p>
		</td> 
		<td>
		<p><?php echo $supplier['InventorySupplier']['city']; ?></p>
		</td>
		<td>
		<p><?php echo $supplier['InventorySupplier']['valid_to']; ?></p>
		</td>
		<td>
		<p><?php echo $supplier['InventorySupplier']['phone_office']; ?></p>
		</td>
		
		<td></td>
	</tr>
	<?php if(isset($supplier['PlanningItemRemarks'])) { ?> 
	<?php foreach ($supplier['PlanningItemRemarks'] as $remark) { ?>
		<tr class="red-bg">   
		<td>
		<p>Status: Rejected</p>
		</td>
		<td colspan="2">
		 <p><?php echo $remark['PlanningItemRemark']['note']; ?></p>
		<td>
		<p><?php echo $remark['PlanningItemRemark']['created']; ?></p>
		</td>
		
		<td></td>
	</tr>
	<?php } } ?>
	<tr>
	<td colspan="5">
	<table class="table table-bordered">
	<tr>  
		<th>Item</th> 
		<!--<th>BOM Qty</th>
		<th>MRN</th>
		<th>Req. Qty</th>  -->
		<th>Purchased</th>  
		<th>BOM Qty</th>  
		<th>Conv.</th> 
		<th>Price Book</th>
		<th>Price</th> 
		 
		<th>Total Price</th> 
		 
		<th>Lead Time</th>   
		
		<th>Partial Delivery</th>  
	</tr> 


	

	<?php foreach ($supplier['PlanningItem'] as $planningItem): ?>
		<tr>   
			<td width="220"><?php echo $planningItem['InventoryItem']['code']; ?><br/>
			<small><?php echo $planningItem['InventoryItem']['name']; ?></small>
			
			</td>
  

			<td><?php echo number_format($planningItem['PlanningItem']['planning_qty'], 2); ?><br/>
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small>
			</td>
 

			<?php $total_qty = $planningItem['PlanningItem']['total']; ?>

			<td><?php echo number_format($total_qty, 2); ?> <small><?php echo $planningItem['GeneralUnit']['name']; ?></small></td> 
			
			<td><?php echo _n2($planningItem['InventorySupplierItem']['conversion_qty']); ?> <?php echo $planningItem['GeneralUnit']['name']; ?>/<?php echo $planningItem['PlanningUnit']['name']; ?></td> 

			<td><?php echo number_format($planningItem['InventorySupplierItem']['price'], 2); ?> 
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small></td>

			<td><?php echo number_format($planningItem['PlanningItem']['price'], 2); ?> / 
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small></td>
 

			<td><?php 
			$subtotal_price = $planningItem['PlanningItem']['price'] * $planningItem['PlanningItem']['planning_qty'];

			echo $planningItem['GeneralCurrency']['iso_code']; ?> <?php echo _n2($subtotal_price); 

			$total_rm = $planningItem['GeneralCurrency']['rate'] * $subtotal_price;

			?></td>
 
 
			<td><?php echo $planningItem['InventorySupplierItem']['lead_time']; ?> Days</td>    
 			 
			<input type="hidden" name="inventory_supplier_id[]" value="<?php echo $supplier['InventorySupplier']['id']; ?>"required>
 			<input type="hidden" name="inventory_item_id[]" value="<?php echo $planningItem['PlanningItem']['inventory_item_id']; ?>"required>
 			<input type="hidden" name="inventory_supplier_item_id[]" value="<?php echo $planningItem['PlanningItem']['inventory_supplier_item_id']; ?>"required>

 			<input type="hidden" name="general_unit_id[]" value="<?php echo $planningItem['PlanningItem']['planning_general_unit']; ?>"required>

 			<input type="hidden" name="general_currency_id[]" value="<?php echo $planningItem['GeneralCurrency']['id']; ?>"required>

 			<input type="hidden" name="quantity[]" value="<?php echo $planningItem['PlanningItem']['planning_qty']; ?>"required>

 			<input type="hidden" name="unit_price[]" value="<?php echo $planningItem['PlanningItem']['unit_price']; ?>"required>

 			<input type="hidden" name="amount[]" value="<?php echo $planningItem['PlanningItem']['price'] * $total_qty; ?>"required> 
 			<input type="hidden" name="total_rm[]" value="<?php echo $total_rm; ?>"required> 
			<td>
			<!--<?php echo $this->Html->link('Partial', array('controller' => 'planning_items', 'action' => 'partialdelivery', $planningItem['PlanningItem']['id'], $planning['Planning']['id']), array('class' => 'btn btn-primary btn-xs')); ?> 

			<?php echo $this->Html->link('Edit', array('controller' => 'planning_items', 'action' => 'edit', $planningItem['PlanningItem']['id'], $planning['Planning']['id']), array('class' => 'btn btn-warning btn-xs')); ?> -->
			<textarea name="delivery_remark[]" placeholder="EG: 10unt 21 Jan, 15unt 11 Feb"></textarea>
 
			</td>  
		</tr>
		<tr>
			<td colspan="10"> 
			<table class="table table-bordered">
			<tr>
				<td>Jobs</td>
				<td>Qty Required</td>
				<td>Buffer</td>
				<td>Total Qty</td>
				<td>Price/Unt</td>
				<td>Subtotal</td>
				<td>RM</td>
			</tr> 
			<?php 
			$jobs = '';
			$date = '';
			$job_childs = '';
			foreach ($planningItem['Jobs'] as $job) { 
				?> 

			<?php  
			$jobs .= $job['PlanningItemJob']['id'].','; 
			$job_childs .= $job['PlanningItemJob']['sale_job_child_id'].','; 
			$date .= $job['PlanningItemJob']['partial_date'].',';
			?>

			<tr>
				<td><?php echo $job['SaleJobChild']['station_name']; ?> (<?php echo $job['SaleJobChild']['name']; ?>)</td>
				<td><?php echo $job['PlanningItemJob']['quantity']; ?></td>
				<td><?php echo $job['PlanningItemJob']['buffer']; ?></td>
				<td><?php 
				$total_qty = $job['PlanningItemJob']['quantity'] + $job['PlanningItemJob']['buffer']; 
				$sub_price = $job['PlanningItemJob']['unit_price'] * $total_qty;
				echo $total_qty;
				?></td>
				<td><?php echo $job['GeneralCurrency']['iso_code']; ?> <?php echo $job['PlanningItemJob']['unit_price']; ?></td>
				<td><?php echo $job['GeneralCurrency']['iso_code']; ?> <?php echo $sub_price; ?></td>
				<td><?php echo $sub_price * $job['GeneralCurrency']['rate']; ?></td>
			</tr> 
			
			<?php } ?>
			<input type="hidden" name="default_delivery[]" value="<?php echo $date; ?>"> 
			<input type="hidden" name="planning_item_jobs[]" value="<?php echo $jobs; ?>"> 

			<input type="hidden" name="dateline[]" value="<?php echo $date; ?>">  
			<input type="hidden" name="jobs[]" value="<?php echo $jobs; ?>">
			<input type="hidden" name="sale_job_child_id[]" value="<?php echo $job_childs; ?>">
			</table>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	</table>
	<?php  
	endforeach; ?>

	

<?php 
//if($planning['Planning']['status'] == 2) { 
	?>
<p><b>Note:</b> Once click Export to Purchase Requisition (under Purchasing menu), it will generate Purchase Requisition as a Draft. Please change status as Active when needed.</p>
<?php echo $this->Form->button('Export To Purchase Requisition', array('id' => 'submit', 'class' => 'btn btn-success pull-right')); ?>
<?php 
//} 
?>
<?php echo $this->Form->end(); ?>
</div>

</div>
</div>
</div>
</div> 

<?php 

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	}
	if($status == 1) {
		$data = 'Submitted For Approval';
	}
	if($status == 2) {
		$data = 'Approved';
	}
	if($status == 3) {
		$data = 'Rejected';
	}
	if($status == 4) {
		$data = 'Submitted To Procurement';
	} 
	if($status == 5) {
		$data = 'Purchased';
	} 
	return $data;
}

?>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
$(document).on('focus', '.date', function() {
    $(this).datepicker({
	  dateFormat: 'yy-mm-dd', 
	  ampm: true
	});
}); 

$(document).ready(function() { 

	// Load each item by job
	/*
	$('.loadJobItem').each(function() { 
	    var param = $(this).attr('id');
	    $.ajax({ 
	        type: "GET", 
	        dataType: 'json',
	        data: 'item=' + param,
	        url: baseUrl + 'project_boms/ajaxbomitembyjob', 
	        success: function(json) { 
	        	console.log(json.qty);
	            $('#'+param).html(json.qty + ' ' + json.unit);	 
	        }
	    }); 
	}); */

	$('#dateonly').on('change', function() {
		var date = $('#dateonly').val();
		$('.date').each(function() {
			$(this).val(date);
		});
	});
});
</script>
<?php $this->end(); ?>