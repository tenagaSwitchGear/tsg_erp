<?php echo $this->Html->link('Purchase Planning', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
  
<?php
if($planning['Planning']['status'] == 0) { 
	echo $this->Html->link('Submit Approval', array('action' => 'planorderreview', $planning['Planning']['id'].'?status=1'), array('class' => 'btn btn-success'));
} ?>


<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Plan Purchase: <?php echo h($planning['Planning']['name']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
<?php

$group = $this->Session->read('Auth.User.group_id');

$role = $this->Session->read('Auth.User.role');

if($group == 1 || $group == 12) { 
	if($planning['Planning']['status'] == 1) { 
?>
	<div class="table-bordered" style="background: #c8efba;">
	<?php echo $this->Form->create('Planning', array('class' => 'form-horizontal')); ?>
	<?php echo $this->Form->input('id'); ?>
	<div class="form-group">
	<label class="col-sm-12"><h4><b>Approval Form</b></h4><p>Once approved, User can convert to Purchase Requisition.</p></label> 
	</div>
	<div class="form-group">
		<label class="col-sm-3">Note (Please fill if reject)</label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('approval_note', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Status</label>
		<div class="col-sm-9">
			<?php $status = array('2' => 'Approve', '3' => 'Reject'); ?>
			<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
		</div>
	</div>
	 
	<div class="form-group"> 
		<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
	</div>	  
	<?php $this->Form->end(); ?> 
	</div>
<?php } } ?>

<div class="plannings view-data"> 
	<dl> 
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['name']); ?>
			&nbsp;
		</dd> 
		 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planning['User']['username'], array('controller' => 'users', 'action' => 'view', $planning['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($planning['Planning']['status']); ?>
			&nbsp;
		</dd>
		 
	</dl>
</div>


 
<div class="related">
<h3>Budget</h3>
<table class="table table-bordered">
	<tr>  
		<th>Job No</th> 
		<th>Indirect Cost</th>
		<th>Soft Cost</th>
		<th>Material Budget</th> 

		<th>Used</th> 
		<th>Balance</th>   
		<th>Require Amount</th>  
		<th>Shortage</th> 
		<th>Action (New Tab)</th> 
	</tr> 
<?php foreach ($job_budgets as $budget) { ?>
	<tr>  
		<td><?php echo $budget['SaleJobChild']['station_name']; ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['indirect_cost']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['soft_cost']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['material_cost']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['used']); ?></td> 
		<td><?php echo _n2($budget['ProjectBudget']['balance']); ?></td>   
		<td <?php if($budget['RequireBudget'] > $budget['ProjectBudget']['material_cost']) { ?>class="red-bg"<?php } ?>><?php echo _n2($budget['RequireBudget']); ?></td> 
		<td>
		<?php if($budget['RequireBudget'] > $budget['ProjectBudget']['material_cost']) { 
			echo $budget['RequireBudget'] - $budget['ProjectBudget']['material_cost'];
		} else {
			echo 'N/A';
		} ?>
		</td>   
		<th>
		<?php if($budget['RequireBudget'] > $budget['ProjectBudget']['material_cost']) { 
			echo $this->Html->link('Transfer Budget', array('action' => 'job'), array('class' => 'btn btn-danger btn-xs'));
		} else {
			echo 'N/A';
		} ?>
		</th>
	</tr> 
 
<?php } ?>
</table>
	<h3><?php echo __('Plan Order Items'); ?></h3>  
	
	
	<?php  
	foreach ($suppliers as $supplier): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover">
	<input type="hidden" name="inventory_supplier_id[]" value="<?php echo $supplier['InventorySupplier']['id']; ?>"required>
	<tr style="background: #cecece;">   
		<td>
		<p><?php echo $supplier['InventorySupplier']['name']; ?></p>
		</td> 
		<td>
		<p><?php echo $supplier['InventorySupplier']['city']; ?></p>
		</td>
		<td>
		<p><?php echo $supplier['InventorySupplier']['valid_to']; ?></p>
		</td>
		<td>
		<p><?php echo $supplier['InventorySupplier']['phone_office']; ?></p>
		</td> 
	</tr>

	<tr>
	<form id="formdata-<?php echo $supplier['InventorySupplier']['id']; ?>" onsubmit="return submitRejectData(<?php echo $supplier['InventorySupplier']['id']; ?>)" method="GET"> 
	 
	<td colspan="3">Reject Remark<br/>
	<textarea style="width:80%" name="note" id="note-<?php echo $supplier['InventorySupplier']['id']; ?>"></textarea> 
	<input type="hidden" name="supplier_id" id="supplier_id-<?php echo $supplier['InventorySupplier']['id']; ?>" value="<?php echo $supplier['InventorySupplier']['id']; ?>">
	<input type="hidden" name="planning_id" id="planning_id-<?php echo $supplier['InventorySupplier']['id']; ?>" value="<?php echo $planning['Planning']['id']; ?>">
	</td>
	<td><input type="submit" name="submit" value="Reject" class="btn btn-danger">
	</td> 
	</form>
	</tr>
	<tr id="ajaxMsg-<?php echo $supplier['InventorySupplier']['id']; ?>"></tr>
	<tr>
	<td colspan="4">
	<table class="table table-bordered">
	<tr>  
		<th>Item</th> 
		<!--<th>BOM Qty</th>
		<th>MRN</th>
		<th>Req. Qty</th>  -->
		<th>Purchased</th>  
		<th>BOM Qty</th>  
		<th>Conv.</th> 
		<th>Price Book</th>
		<th>Price</th>
		<th>Price/Unit</th> 
		 
		<th>Total Price</th> 
		 
		<th>Lead Time</th>    
	</tr> 


	

	<?php  
	foreach ($supplier['PlanningItem'] as $planningItem): ?>
		<tr>   
			<td width="220"><?php echo $planningItem['InventoryItem']['code']; ?><br/>
			<small><?php echo $planningItem['InventoryItem']['name']; ?></small>
			
			</td>
 
			<!--<td><?php echo number_format($planningItem['PlanningItem']['bom_qty'], 2); ?><br/> 
			</td> 
			<td><?php echo number_format($planningItem['PlanningItem']['mrn'], 2); ?><br/> 
			</td> 
			<td><?php echo number_format($planningItem['PlanningItem']['required_qty'], 2); ?><br/>
			<small><?php echo $planningItem['GeneralUnit']['name']; ?></small>
			</td> -->

			<td><?php echo number_format($planningItem['PlanningItem']['planning_qty'], 2); ?><br/>
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small>
			</td>
 

			<?php $total_qty = $planningItem['PlanningItem']['total']; ?>

			<td><?php echo number_format($total_qty, 2); ?> <small><?php echo $planningItem['GeneralUnit']['name']; ?></small></td> 
			
			<td><?php echo _n2($planningItem['InventorySupplierItem']['conversion_qty']); ?> <?php echo $planningItem['GeneralUnit']['name']; ?>/<?php echo $planningItem['PlanningUnit']['name']; ?></td> 

			<td><?php echo number_format($planningItem['InventorySupplierItem']['price'], 2); ?> 
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small></td>

			<td><?php echo number_format($planningItem['PlanningItem']['price'], 2); ?> / 
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small></td>

			<td><?php echo number_format($planningItem['PlanningItem']['unit_price'], 2); ?> / 
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small></td>

			<td><?php echo $planningItem['GeneralCurrency']['iso_code']; ?> <?php echo number_format($planningItem['PlanningItem']['unit_price'] * $total_qty, 2); ?></td>
 
			<td><?php echo $planningItem['InventorySupplierItem']['lead_time']; ?> Days</td>    
 			
 		 
		</tr>
		<tr>
			<td colspan="10"> 
			<table class="table table-bordered">
			<tr>
				<td>Jobs</td>
				<td>Qty Required</td>
				<td>Buffer</td>
				<td>Total Qty</td>
				<td>Price/Unt</td>
				<td>Subtotal</td>
				<td>RM</td>
			</tr> 
			<?php foreach ($planningItem['Jobs'] as $job) { ?> 
			<tr>
				<td><?php echo $job['SaleJobChild']['station_name']; ?> (<?php echo $job['SaleJobChild']['name']; ?>)</td>
				<td><?php echo $job['PlanningItemJob']['quantity']; ?></td>
				<td><?php echo $job['PlanningItemJob']['buffer']; ?></td>
				<td><?php 
				$total_qty = $job['PlanningItemJob']['quantity'] + $job['PlanningItemJob']['buffer']; 
				$sub_price = $job['PlanningItemJob']['unit_price'] * $total_qty;
				echo $total_qty;
				?></td>
				<td><?php echo $job['GeneralCurrency']['iso_code']; ?> <?php echo $job['PlanningItemJob']['unit_price']; ?></td>
				<td><?php echo $job['GeneralCurrency']['iso_code']; ?> <?php echo $sub_price; ?></td>
				<td><?php echo $job['PlanningItemJob']['total_price_rm']; ?></td>
			</tr> 
			<?php } ?>
			</table>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	</table>
	<?php  
	endforeach; ?>

	


</div>

</div>
</div>
</div>
</div>





<?php 

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	}
	if($status == 1) {
		$data = 'Submitted For Approval';
	}
	if($status == 2) {
		$data = 'Approved';
	}
	if($status == 3) {
		$data = 'Rejected';
	}
	if($status == 4) {
		$data = 'Submitted To Procurement';
	} 
	if($status == 5) {
		$data = 'Purchased';
	} 
	return $data;
}

?>


<?php $this->start('script'); ?>
 
<script type="text/javascript">
 
function submitRejectData(id) {
	//$("form").serialize()
	var data = {
		note: $("#note-"+id).val(),
		supplier_id: $("#supplier_id-"+id).val(),
		planning_id: $("#planning_id-"+id).val()
	};
	// /console.log(data);
	$.ajax({
        type: "POST",                        
        url:baseUrl + 'plannings/ajaxreject',           
        contentType: "application/json",
        dataType: "json",
        data: data,  
        success: function (json) { 
        	console.log(json);
        	var html = '';
        	if(json.status == false) {
        		html += '<td colspan="4" class="red-bg">' + json.msg + '</td>';
        	} else {
        		html += '<td colspan="4" class="green-bg">' + json.msg + '</td>';
        	}   
        	$('#ajaxMsg-'+id).append(html);
    	}, error: function(e) {
    		console.log(e);
    	}
    });	
    return false;
} 

</script>
<?php $this->end(); ?>
 