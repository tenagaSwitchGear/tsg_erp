<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Job No: <?php echo h($saleJob['SaleJob']['name']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleJobs view-data">
<h2><?php echo __('Sale Job'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['id']); ?>
			&nbsp;
		</dd> 

		<dt><?php echo __('Job No'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['name']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Customer PO No'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['contract_no']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Quotation'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleQuotation']['name']); ?>
			&nbsp; 
		</dd> 
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleJob['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleJob['Customer']['id'])); ?>
			&nbsp;
		</dd> 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['modified']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($saleJob['SaleJob']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleJob['User']['username'], array('controller' => 'users', 'action' => 'view', $saleJob['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div> 
	<h4>Item</h4>
	<div class="table table-responsive">
	 <table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Substation'); ?></th>
		<th><?php echo __('Item Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Unit'); ?></th>
		<th><?php echo __('Type'); ?></th>
		 
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($items as $item): ?>
		<tr>
			<td><?php echo $item['SaleJobItem']['id']; ?></td>
			<td><?php echo $item['SaleJobChild']['station_name']; ?></td>
			<td><?php echo $item['SaleJobItem']['name']; ?></td>
			<td><?php echo $item['SaleJobItem']['code']; ?></td>
			<td><?php echo $item['SaleJobItem']['quantity']; ?></td>
			<td><?php echo $item['GeneralUnit']['name']; ?></td>
			<td><?php echo type($item['SaleJobItem']['type']); ?></td> 
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('action' => 'viewitem', $item['SaleJobItem']['id'])); ?>
				<?php echo $this->Html->link(__('Add To Planning'), array('action' => 'add', $item['SaleJobItem']['id'])); ?> 
			</td>
		</tr> 
	<?php endforeach; ?>
	</table>
	</div>

	<h4>Raw Material</h4>
	<div class="table table-responsive">
	 <table cellpadding = "0" cellspacing = "0" class="table">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Item Name'); ?></th> 
		<th><?php echo __('Code'); ?></th> 
		<th><?php echo __('Qty'); ?></th>
		<th><?php echo __('Unit'); ?></th>
		<th><?php echo __('Stock Qty'); ?></th>
		<th><?php echo __('Qty Required'); ?></th>
		<th><?php echo __('Buffer'); ?></th>
		<th><?php echo __('Default Supplier'); ?></th>
		
		 
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($bomitems as $bomitem): ?>
		<tr>
			<td><?php echo $bomitem['item_id']; ?></td>
			<td><?php echo $bomitem['name']; ?></td> 
			<td><?php echo $bomitem['code']; ?></td> 
			<td><?php echo $bomitem['total']; ?></td>
			<td><?php echo $bomitem['unit']; ?></td>
			
		</tr> 
	<?php endforeach; ?>
	</table>
	</div>

</div> 
</div> 
</div> 
</div> 

<?php
	function status($status) {
		if($status == 0) {
			return 'Draft';
		} else if($status == 1) {
			return 'Waiting HOS Verification';
		} else if($status == 2) {
			return 'Waiting HOD Verification';
		} else if($status == 3) {
			return 'Waiting GM Approval';
		} else if($status == 4) {
			return 'Verified';
		} else if($status == 5) {
			return 'Rejected';
		}
	}

	function type($type) {
		if($type == 1) {
			return 'Finished Goods';
		} else {
			return 'Raw Material';
		} 
	}
?>