<?php echo $this->Html->link('Purchase Planning', array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Html->link('Job Lists', array('action' => 'job'), array('class' => 'btn btn-default')); ?>  



<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Plan Purchase: <?php echo h($planning['Planning']['name']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
<?php

$group = $this->Session->read('Auth.User.group_id');

$role = $this->Session->read('Auth.User.role');

if($group == 1 || $group == 12 || $group == 6 && $role == 'HOD' || $role == 'HOS') { 
	if($planning['Planning']['status'] == 1) { 
?>
	<div class="table-bordered" style="background: #c8efba;">
	<?php echo $this->Form->create('Planning', array('class' => 'form-horizontal')); ?>
	<?php echo $this->Form->input('id'); ?>
	<div class="form-group">
	<label class="col-sm-12"><h4><b>Approval Form</b></h4><p>Once approved, User can convert to Purchase Requisition.</p></label> 
	</div>
	<div class="form-group">
		<label class="col-sm-3">Note (Please fill if reject)</label>
		<div class="col-sm-9">
			<?php echo $this->Form->input('approval_note', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Status</label>
		<div class="col-sm-9">
			<?php $status = array('2' => 'Approve', '3' => 'Reject'); ?>
			<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
		</div>
	</div>
	 
	<div class="form-group"> 
		<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
	</div>	  
	<?php $this->Form->end(); ?> 
	</div>
<?php } } ?>

<div class="plannings view-data">
<h2><?php echo __('Plan Purchase'); ?></h2>
	<dl> 
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['name']); ?>
			&nbsp;
		</dd>
		 
		<dt><?php echo __('Sale Job'); ?></dt>
		<dd>
			 Job
		</dd> 
		 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($planning['User']['username'], array('controller' => 'users', 'action' => 'view', $planning['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($planning['Planning']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($planning['Planning']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>


 
<div class="related">
<h3><?php echo __('Items'); ?></h3> 
<table cellpadding = "0" cellspacing = "0" class="table table-bordered table-hover">
	<tr>  
		<th><?php echo __('Item'); ?></th>
		<th><?php echo __('Job'); ?></th>  
		<th><?php echo __('FAT Date'); ?></th> 
		<th><?php echo __('Delivery Date'); ?></th> 
		<th><?php echo __('Total Qty'); ?></th>  
		<th><?php echo __('Action'); ?></th>  
	</tr>
	<?php  
	foreach ($jobitems as $jobitem): ?>
		<tr>  
 
			<td><?php echo $jobitem['InventoryItem']['code']; ?><br/>
			<small><?php echo $jobitem['InventoryItem']['name']; ?></small>
			</td>

			<td><?php echo $jobitem['SaleJobChild']['name']; ?><br/>
			<small><?php echo $jobitem['SaleJobChild']['station_name']; ?></small>
			</td> 

			<td>
				<?php echo $jobitem['SaleJobChild']['fat_date']; ?> 
			</td> 
			<td>
				<?php echo $jobitem['SaleJobChild']['delivery_date']; ?> 
			</td> 

			<td>
				<?php echo $jobitem['SaleJobItem']['quantity']; ?> 
				<small><?php echo $jobitem['GeneralUnit']['name']; ?></small>
			</td>
			<td>
			<?php echo $this->Html->link('<i class="fa fa-search"></i> View', array('controller' => 'sale_jobs', 'action' => 'view', $jobitem['SaleJob']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'target' => '_blank')); ?>  
			</td>  
		</tr>
	<?php  
	endforeach; ?>
	</table>

	<h3><?php echo __('Plan Order Items'); ?></h3> 
	<?php if (!empty($planning['PlanningItem'])): ?>
	<table cellpadding = "0" id="tableScrollAble" cellspacing = "0" class="table table-bordered table-hover">
	<tr>  
		<th>Item</th>
		<th>BOM. Qty</th>  
		<th>Plan Qty</th> 
		<th>Buffer</th> 
		<th>Total Qty</th> 
		<th>Min Order</th>
		<th>Price/Unit</th>  
		<th>Total Price</th>  
		<th>Supplier</th>
		<th>Lead Time</th> 
		<th><?php echo __('Action'); ?></th>  
	</tr> 
	<?php  
	foreach ($items as $planningItem): ?>
		<tr>  

			<td><?php echo $planningItem['InventoryItem']['code']; ?><br/>
			<small><?php echo $planningItem['InventoryItem']['name']; ?></small>
			</td>

			<td><?php echo number_format($planningItem['PlanningItem']['quantity'], 2); ?><br/>
			<small><?php echo $planningItem['GeneralUnit']['name']; ?></small>
			</td> 

			<td><?php echo number_format($planningItem['PlanningItem']['planning_qty'], 2); ?><br/>
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small>
			</td>

			<td><?php echo number_format($planningItem['PlanningItem']['buffer'], 2); ?><br/>
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small></td> 

			<?php $total_qty = $planningItem['PlanningItem']['planning_qty'] + $planningItem['PlanningItem']['buffer']; ?>

			<td><?php echo number_format($total_qty, 2); ?><br/>
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small></td> 
			
			<td><?php echo number_format($planningItem['InventorySupplierItem']['min_order'], 2); ?><br/>
			<small><?php echo $planningItem['PlanningUnit']['name']; ?></small>
			</td>

			<td><?php echo number_format($planningItem['InventorySupplierItem']['price'], 2); ?></td>

			<td><?php echo number_format($planningItem['InventorySupplierItem']['price'] * $total_qty, 2); ?></td>

			<td><?php echo $planningItem['InventorySupplier']['name']; ?></td>
			<td><?php echo $planningItem['InventorySupplierItem']['lead_time']; ?> Days</td> 
			 
 
			<td>
			<?php echo $this->Html->link('Edit', array('controller' => 'planning_items', 'action' => 'edit', $planningItem['PlanningItem']['id'], $planning['Planning']['id']), array('class' => 'btn btn-warning btn-xs')); ?> 

			<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'planning_items', 'action' => 'delete', $planningItem['PlanningItem']['id'], $planning['Planning']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure you want to delete # %s?', $planningItem['InventoryItem']['code'])); ?> 
			</td>  
		</tr>
	<?php  
	endforeach; ?>
	</table>
<?php endif; ?>
 
	<p>Once Review Purchase Requisition is clicked, you can add Partial delivery, Delivery date etc.</p>
	<?php echo $this->Html->link('Review Purchase Requisition', array('action' => 'planorderreview', $planning['Planning']['id']), array('class' => 'btn btn-success')); ?> 
 
 
</div>

</div>
</div>
</div>
</div>



<?php 

function status($status) {
	if($status == 0) {
		$data = 'Draft';
	}
	if($status == 1) {
		$data = 'Submitted For Approval';
	}
	if($status == 2) {
		$data = 'Approved';
	}
	if($status == 3) {
		$data = 'Rejected';
	}
	if($status == 4) {
		$data = 'Submitted To Procurement';
	} 
	if($status == 5) {
		$data = 'Purchased';
	} 
	return $data;
}

?>


<?php $this->start('script'); ?>
<?php echo $this->Html->script(array(
        '/vendors/DataTables-1.10.13/media/js/jquery.dataTables.min.js' 
      )); ?> 
<script type="text/javascript">

$(document).ready(function() {
	$('#tableScrollAble').DataTable( {
        "scrollY": 540,
        "scrollX": true,
        "paging":   false,
        "ordering": false,
        "info":     false
    });

	/*$('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        alert("Please check at least one item to purchase.");
        return false;
      } 
    });*/

    var json_unit =<?php echo json_encode($unitJson); ?>;
	$.each(json_unit, function (index, data) {
		$('.planningUnit').each(function() {
			var html = '<option value="'+data.id+'">'+data.name+'</option>';
	        $(this).append(html);
	    });
	});

	var orderUnit = 1;
	$('.MinOrderUnit').each(function() {
		$('#uom' + orderUnit).val($(this).val());
		orderUnit++;
	});

	
}); 

function getSupplier(item_id, row) {
	//console.log(item_id + ' - ' + $('#supplier'+row).val());
	if($('#supplier'+row).val() != '') {
		$.ajax({
	        type: "GET",                        
	        url:baseUrl + 'inventory_supplier_items/ajaxfinditem',           
	        contentType: "application/json",
	        dataType: "json",
	        data: "inventory_item_id=" + item_id + "&supplier_id=" + $('#supplier'+row).val(), 
	        success: function (json) { 
	        	if(json.InventorySupplierItem != undefined) {
	        		$('#conversion'+row).html(json.InventorySupplierItem['conversion_qty'] + '<br/>' + '<small>' + json.UnitConversion['name'] + '</small>'); 
		        	$('#minOrder'+row).html(json.InventorySupplierItem['min_order'] + '<br/>' + '<small>' + json.MinOrderUnit['name'] + '</small>');
		        	$('#leadTime'+row).html(json.InventorySupplierItem['lead_time'] + ' Days');
		        	$('#price'+row).html(json.InventorySupplierItem['price_per_unit']);
		        	$('#symbol'+row).html(json.GeneralCurrency['iso_code']);	
		        	$('#priceBook' + row).val(json.InventorySupplierItem['id']);
	        	} else {
	        		$('#minOrder'+row).html('Not Found');
		        	$('#leadTime'+row).html('Not Found');
		        	$('#price'+row).html('Not Found');
		        	$('#symbol'+row).html('Not Found');
		        	$('#priceBook' + row).val(0);
	        	} 
	    	}
	    });	
	}
	
} 

$(document).ready(function() {
	$('#submit').click(function() {
		var checked = $("input[type=checkbox]:checked").length; 
		if(checked == 0) {
			alert("You must check at least one item.");
			return false;
		}  
	});

	$("#checkall").change(function () {  
	    $(".check").prop('checked', $(this).prop("checked"));  
	    $(".check").each(function() {  
			var getId = $(this).attr('id');
			var getVal = $("#check" + getId).val();
			if($(this).prop("checked") == true) {
				$("#check" + getId).val(1);
			} else {
				$("#check" + getId).val(0);
			}  
		});
	});

	$(".check").each(function() {
		$(this).change(function() {
			var getId = $(this).attr('id');
			var getVal = $("#check" + getId).val();
			if(getVal == 0) {
				$("#check" + getId).val(1);
			} else {
				$("#check" + getId).val(0);
			} 
		}); 
	});
});
</script>
<?php $this->end(); ?> 