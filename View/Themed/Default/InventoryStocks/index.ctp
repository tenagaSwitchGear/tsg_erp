<?php echo $this->Html->link(__('Available Stock List'), array('action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('Issued Lists'), array('action' => 'index', 1), array('class'=>'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('Item General List'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link(__('Add Stock'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12">

  	
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Stocks'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

        	<?php echo $this->Form->create('InventoryStock', array('action' => 'index', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Item name', 'class' => 'form-control', 'required' => false, 'id' => 'findProduct', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('code', array('type' => 'text', 'placeholder' => 'Code', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_item_category_id', array('options' => $categories, 'class' => 'form-control', 'required' => false, 'empty' => '-All Category-', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_location_id', array('options' => $locations, 'class' => 'form-control', 'required' => false, 'empty' => '-All Warehouse-', 'label' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('inventory_store_id', array('options' => $stores, 'class' => 'form-control', 'required' => false, 'empty' => '-All Store-', 'label' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->Form->end(); ?>

        	<!-- content start-->
        	
        	<div class="inventoryStocks index">
			<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
			<thead>
			<tr> 
				<th><?php echo $this->Paginator->sort('InventoryItem.code', 'Code'); ?></th> 
				<th><?php echo $this->Paginator->sort('InventoryItem.name', 'Name'); ?></th> 
				<th><?php echo $this->Paginator->sort('qty_in', 'Qty In'); ?></th>
				<th><?php echo $this->Paginator->sort('issued_quantity', 'Issued Qty'); ?></th>
				<th><?php echo $this->Paginator->sort('quantity', 'Balance'); ?></th>
				<th><?php echo $this->Paginator->sort('inventory_location_id', 'Warehouse'); ?></th>
				<th><?php echo $this->Paginator->sort('inventory_store_id', 'Store'); ?></th>
				<th><?php echo $this->Paginator->sort('rack', 'Rack'); ?></th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php  
				foreach ($inventoryStocks as $inventoryStock): ?>
			<tr> 
			<td><?php echo ($inventoryStock['InventoryItem']['code']); ?></td> 
				<td><?php echo ($inventoryStock['InventoryItem']['name']); ?></td> 
				<td><?php echo ($inventoryStock['InventoryStock']['qty_in']); ?></td> 
				<td><?php echo ($inventoryStock['InventoryStock']['issued_quantity']); ?> </td>
				<td><?php echo ($inventoryStock['InventoryStock']['quantity']); ?> <?php echo ($inventoryStock['GeneralUnit']['name']); ?></td> 
				<td><?php echo ($inventoryStock['InventoryLocation']['name']); ?></td>
				<td><?php echo ($inventoryStock['InventoryStore']['name']); ?></td>
				<td><?php echo ($inventoryStock['InventoryStock']['rack']); ?></td>
				<td class="actions">
					<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryStock['InventoryStock']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
					<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventoryStock['InventoryStock']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
					<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventoryStock['InventoryStock']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.($inventoryStock['InventoryItem']['name']).'"', $inventoryStock['InventoryStock']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
			</tbody>
			</table>
			<p>
			<?php
				//echo $this->Paginator->counter(array(
				//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				//));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>