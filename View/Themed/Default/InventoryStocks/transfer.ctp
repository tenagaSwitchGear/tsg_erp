<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Inventory Item'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Inventory Item Categories'), array('controller' => 'inventory_item_categories', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Inventory Supplier Items'), array('controller' => 'inventory_supplier_items', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php //echo $this->Html->link(__('Transfer History'), array('controller' => 'inventorymaterialhistories', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Inventory Stocks'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
            
			<div class="table">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
					<tr> 
                        <th class="text-center">#</th>
						<th><?php echo $this->Paginator->sort('code'); ?></th>
						<th><?php echo $this->Paginator->sort('name', 'Items'); ?></th>
						<th><?php echo $this->Paginator->sort('quantity'); ?></th>
						<th><?php echo ('General Unit'); ?></th>
                        <th><?php echo ('Warehouse'); ?></th>
						
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryStock']['page']) ? 1 : $this->Paginator->params['paging']['InventoryStock']['page']; $limit = $this->Paginator->params['paging']['InventoryStock']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryStocks as $inventoryStock): 
					?>
					<tr> 
                        <td class="text-center"><?php echo $startSN++; ?></td>
						<td><?php echo ($inventoryStock['InventoryItem']['code']); ?>&nbsp;</td>
						<td><?php echo ($inventoryStock['InventoryItem']['name']); ?>&nbsp;</td>
						<td><?php echo ($inventoryStock['InventoryStock']['quantity']); ?>&nbsp;</td>
						<td><?php echo $inventoryStock['GeneralUnit']['name']; ?></td>
                        <th><?php echo $inventoryStock['InventoryLocation']['name']; ?></th>
						
						<td class="actions">
							<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventoryStock['InventoryStock']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
							<?php echo $this->Html->link('<i class="fa fa-exchange"></i>', array('action' => 'exchange', $inventoryStock['InventoryStock']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>	
						</td>
					</tr>
				<?php endforeach; ?>
					</tbody>
				</table>
			<p>
			<?php
				//echo $this->Paginator->counter(array(
				//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				//));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
