<?php
echo $this->Html->css(array(
    '/js/libs/jsgrid/dist/jsgrid.min.css',
    '/js/libs/jsgrid/dist/jsgrid-theme.min.css'  
  )); 
?>

<?php echo $this->Html->link(__('Stock Takes'), array('action' => 'stock_take'), array('class'=>'btn btn-info btn-sm')); ?>
         <?php if(empty($period['Period']) || $period['Period'] == 'Closed'){?>
        <?php echo $this->Html->link(__('Start Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'start_stock_take'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php }else{ ?>
            <?php echo $this->Html->link(__('Stop Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'stop_stock_take?startdate='.$period['StartDate']), array('class'=>'btn btn-danger btn-sm')); ?>
           <?php  } ?>
        <?php echo $this->Html->link(__('Stock Take List'), array('controller'=>'inventory_stocks', 'action' => 'st_list'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Stock Take Verification'), array('controller'=>'inventory_stocks', 'action' => 'stock_take_verify'), array('class'=>'btn btn-info btn-sm')); ?>

<div class="row"> 
    <div class="col-xs-12">  

        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo "Stock Take"; ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <h5><?php echo "Stock Take No :".$st['StockTakes']['code']; ?></h5>
            <h5><?php echo strtoupper('Warehouse'); ?>: <?php echo $st['InventoryLocations']['name']; ?></h5>
            <h5><?php echo strtoupper('Store'); ?>: <?php echo $st['InventoryStores']['name']; ?></h5>    
            <?php echo $this->Session->flash(); ?>

            <div id="jsGrid"></div>

            <table cellpadding="0" cellspacing="0" class="table table-bordered" id="dataTable">
                <thead>
                <tr> 
                    <th>Item</th>
                    <th>Onhand</th>
                    <th>Actual</th>
                    <th>Variance</th>
                    <th>Reason</th>
                    <th>Status</th> 
                </tr>
                </thead>
                
            </table>
        </div>
    </div> 
</div>
</div>

 
<div class="span4 proj-div" data-toggle="modal" data-target="#GSCCModal">Clickable content, graphics, whatever</div> 
<div id="GSCCModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <form action="" method="POST" id="pForm">
      <div class="modal-body">
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" value="" class="from-control">
        </div>
        <div class="form-group">
            <label>Code</label>
            <input type="text" name="name" value="" class="from-control">
        </div>
        <div class="form-group">
            <label>Quantity</label>
            <input type="text" name="name" value="" class="from-control">
        </div>
        <div class="form-group">
            <label>Variance</label>
            <input type="text" name="name" value="" class="from-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>


<?php $this->start('script'); ?>
<?php
    echo $this->Html->script(array(
        '/js/libs/jsgrid/jsgrid.min.js' 
    ));
?>
<script type="text/javascript">

$(document).ready(function() { 

    // Open modal
    $('.openModalLogin').each(function() {
      $(this).click(function() { 
        $('#modalLogin').modal({show:true});
        $('#modalRegisterContent').removeClass('active');
        $('#modalLoginContent').addClass('active'); 
        return false;
      }); 
    });
     
    $('#submit').click(function() {
        var checked = $("input[type=checkbox]:checked").length; 
        if(checked == 0) {
            alert("Please check at least one item.");
            return false;
        }  
    });

    $("#checkall").change(function () {  
        $(".check").prop('checked', $(this).prop("checked"));  
        $(".check").each(function() {  
            var getId = $(this).attr('id');
            var getVal = $("#check" + getId).val();
            if($(this).prop("checked") == true) {
                $("#check" + getId).val(1);
            } else {
                $("#check" + getId).val(0);
            }  
        });
    });

    $(".check").each(function() {
        $(this).change(function() {
            var getId = $(this).attr('id');
            var getVal = $("#check" + getId).val();
            if(getVal == 0) {
                $("#check" + getId).val(1);
            } else {
                $("#check" + getId).val(0);
            } 
        }); 
    });
}); 

function get_variance(row){
    var onhand = $('#onhand'+row).val();
    var actual = $('#actual'+row).val();

    var n = parseInt(actual) - parseInt(onhand);

    $('#variant'+row).val(n);
    $('#vary'+row).html(n);
    if(n != 0){
        $('#reason'+row).prop("disabled", false); 
    }else{
        $('#reason'+row).prop("disabled", true); 
    }

    $('#new_actual'+row).val(actual);
   
}

function edit_st(row){
    var max_q = $('#onhand'+row).val();
    var rs = $('#rs_notes'+row).val();
    var actual_input = '<input type="number" id="actual'+row+'" name="q" max="'+max_q+'" class="form-control" min="0" onchange="get_variance('+row+')" onKeyPress="get_variance('+row+')" onKeyUp="get_variance('+row+')"/>'
    

    var reason_st = '<select class="form-control" onchange="set_reason('+row+', this.value);" id="reason'+row+'" required="required" disabled="disabled"><option value="0">Select Reason</option><?php foreach($reasons as $reason){ ?><option value="<?php echo $reason['id']; ?>"><?php echo $reason['notes']; ?></option><?php } ?></select><input type="hidden" id="rs_notes'+row+'" value="'+rs+'" />';

    //var acts = '<button type="submit" class="btn btn-primary btn-circle-sm"><i class="fa fa-floppy-o"></i></button><button type="button" class="btn btn-danger btn-circle-sm" onclick="reset('+row+')"><i class="fa fa-times"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#rs'+row).html(reason_st);
    $('#submit_button'+row).show();
    $('#reset_button'+row).show();
    $('#edit_button'+row).hide();
}

function set_reason(row, nilai){
    $('#reason_st'+row).val(nilai);
}

function reset_field(row){
    
    var res = $('#rs_notes'+row).val();
    var actual_input = $('#actual_2'+row).val();
    var variant = $('#variant'+row).val();
    var rs = res+'<input type="hidden" id="rs_notes'+row+'" value="'+res+'">';
    //var acts = '<button type="button" class="btn btn-success btn-circle-sm" onclick="edit_st('+row+')"><i class="fa fa-pencil"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#vary'+row).html(variant);
    $('#rs'+row).html(rs);
    $('#submit_button'+row).hide();
    $('#reset_button'+row).hide();
    $('#edit_button'+row).show();
}
</script>

<?php
    echo $this->Html->script(array(
        '/js/libs/jsgrid/dist/jsgrid.min.js' 
    ));
?>
<script type="text/javascript">
    $(document).ready(function() { 
        // Jsgrid
        var clients = [
            { "Name": "Otto Clay", "Age": 25, "Country": 1, "Address": "Ap #897-1459 Quam Avenue", "Married": false },
            { "Name": "Connor Johnston", "Age": 45, "Country": 2, "Address": "Ap #370-4647 Dis Av.", "Married": true },
            { "Name": "Lacey Hess", "Age": 29, "Country": 3, "Address": "Ap #365-8835 Integer St.", "Married": false },
            { "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
            { "Name": "Ramona Benton", "Age": 32, "Country": 3, "Address": "Ap #614-689 Vehicula Street", "Married": false }
        ];
     
        var countries = [
            { Name: "", Id: 0 },
            { Name: "United States", Id: 1 },
            { Name: "Canada", Id: 2 },
            { Name: "United Kingdom", Id: 3 }
        ];
     
        $("#jsGrid").jsGrid({
            width: "100%",
            height: "400px",
     
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,
     
            data: clients,
     
            fields: [
                { name: "Name", type: "text", width: 150, validate: "required" },
                { name: "Age", type: "number", width: 50 },
                { name: "Address", type: "text", width: 200 },
                { name: "Country", type: "select", items: countries, valueField: "Id", textField: "Name" },
                { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
                { type: "control" }
            ]
        });
        // Datatable
        /*
        $('#dataTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseUrl + "inventory_stocks/ajax_stock_take_verification/<?php echo $st['StockTakes']['id']; ?>",
                type : "POST",
                dataType: "json",
                contentType: "application/json",
                data : function (data) {
                    console.log(data);
                }
            }, 
            "columns": [ 
                { "data": 0 },
                { "data": 1 },
                { "data": 2 },
                { "data": 3 },
                { "data": 4 },
                { "data": 5 } 
            ]
        }); */
    });
</script>
<?php $this->end(); ?>


