<div class="row"> 
    <div class="col-xs-12"> 
        <?php echo $this->Html->link(__('Stock List'), array('action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryStock['InventoryStock']['id']), array('class'=>'btn btn-warning btn-sm')); ?>

        <?php echo $this->Html->link(__('Transfer'), array('action' => 'exchange', $inventoryStock['InventoryStock']['id']), array('class'=>'btn btn-info btn-sm')); ?>

        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __($inventoryStock['InventoryItem']['code']); ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?> 

<div class="inventoryStocks view-data">
<h2><?php echo __('Inventory Stock'); ?></h2>
    <dl>
         
        <dt><?php echo __('Code'); ?></dt>
        <dd>
            <?php echo __($inventoryStock['InventoryItem']['code']); ?>
            &nbsp;
        </dd>

        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo $this->Html->link($inventoryStock['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryStock['InventoryItem']['id'])); ?>
            &nbsp;
        </dd> 
        <dt><?php echo __('Price Per Unit'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['price_per_unit']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Quantity'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['quantity']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('UOM'); ?></dt>
        <dd>
            <?php echo $this->Html->link($inventoryStock['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryStock['GeneralUnit']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Issued Quantity'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['issued_quantity']); ?>
            &nbsp;
        </dd>
         
        <dt><?php echo __('Receive Date'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['receive_date']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Expiry Date'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['expiry_date']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Warehouse'); ?></dt>
        <dd>
            <?php echo $this->Html->link($inventoryStock['InventoryLocation']['name'], array('controller' => 'inventory_locations', 'action' => 'view', $inventoryStock['InventoryLocation']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Store'); ?></dt>
        <dd>
            <?php echo $this->Html->link($inventoryStock['InventoryStore']['name'], array('controller' => 'inventory_stores', 'action' => 'view', $inventoryStock['InventoryStore']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Rack'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['rack']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Type'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['type']); ?>
            &nbsp;
        </dd> 
        
        <dt><?php echo __('Created'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['InventoryStock']['created']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('PIC'); ?></dt>
        <dd>
            <?php echo h($inventoryStock['User']['username']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
</div>


</div>
</div>
</div>
</div>