<?php echo $this->Html->link(__('Stock Takes'), array('action' => 'stock_take'), array('class'=>'btn btn-info btn-sm')); ?>
         <?php if(empty($period['Period']) || $period['Period'] == 'Closed'){?>
        <?php echo $this->Html->link(__('Start Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'start_stock_take'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php }else{ ?>
            <?php echo $this->Html->link(__('Stop Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'stop_stock_take?startdate='.$period['StartDate']), array('class'=>'btn btn-danger btn-sm')); ?>
           <?php  } ?>
        <?php echo $this->Html->link(__('Stock Take List'), array('controller'=>'inventory_stocks', 'action' => 'st_list'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Stock Take Verification'), array('controller'=>'inventory_stocks', 'action' => 'stock_take_verify'), array('class'=>'btn btn-info btn-sm')); ?>

<div class="row"> 
    <div class="col-xs-12">  

        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo "Stock Take"; ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <h5><?php echo "Stock Take No :".$st['StockTakes']['code']; ?></h5>
            <h5><?php echo strtoupper('Warehouse'); ?>: <?php echo $st['InventoryLocations']['name']; ?></h5>
            <h5><?php echo strtoupper('Store'); ?>: <?php echo $st['InventoryStores']['name']; ?></h5>    
            <?php echo $this->Session->flash(); ?>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkall"></th> 
                    <th><?php echo 'Item'; ?></th>
                    <th class="col-md-1"><?php echo 'Quantity On Hand'; ?></th>
                    <th class="col-md-1"><?php echo 'Quantity Actual'; ?></th>
                    <th class="col-md-1"><?php echo 'Variance'; ?></th>
                    <th><?php echo 'Reason'; ?></th>
                    <th><?php echo 'Status'; ?></th>
                    <th class="actions" colspan="2"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php echo $this->Form->create('InventoryStock', array('class' => 'form-horizontal')); ?>
                    <?php $bil = 1; ?>
                    <?php foreach ($st['StockTakeItems'] as $stock) { ?>
                    
                <tr>
                    
                    <?php echo $this->Form->input('stock_take_id', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$st['StockTakes']['id'])); ?>
                    <?php echo $this->Form->input('stock_take_item_id.', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$stock['StockTakeItems']['id'])); ?>
                    
                    <td>
                    <input name="data[InventoryStock][check][]" type="checkbox" id="<?php echo $bil; ?>" class="check" value="0">
                    <input id="check<?php echo $bil; ?>" type="hidden" name="data[InventoryStock][include][]" value="0">
                    </td> 
                    <td>
                    <?php echo $stock['InventoryItem']['code']; ?><br/>
                    <small><?php echo $stock['InventoryItem']['name']; ?></small>
                    </td>
                    <td><?php echo $stock['StockTakeItems']['quantity_onhold']; ?>&nbsp;</td>
                    <td id="actual_q<?php echo $bil; ?>"><?php echo $stock['StockTakeItems']['quantity_actual']; ?></td>
                    <td id="vary<?php echo $bil; ?>"><?php echo $stock['StockTakeItems']['quantity_variant']; ?></td>
                    <td id="rs<?php echo $bil; ?>"><?php if(!empty($stock['StockTakeReasons'])){ ?><?php echo $stock['StockTakeReasons']['notes']; ?><input type="hidden" name="rs_notes<?php echo $bil; ?>" id="rs_notes<?php echo $bil; ?>" value="<?php echo $stock['StockTakeReasons']['notes']; ?>"><?php } else { ?><input type="hidden" name="rs_notes<?php echo $bil; ?>" id="rs_notes<?php echo $bil; ?>" value=""><?php } ?></td>
                    <td>Waiting For Verification</td>
                    <td class="actions<?php echo $bil; ?>">
                        <?php $app_status = array('2' => 'Approved', '3'=>'Rejected'); ?>
                        <?php echo $this->Form->input('approved.', array('options' => $app_status, 'class' => 'form-control', 'required' => true, 'label' => false)); ?>
                    </td>
                    <td><?php echo $this->Form->input('approval_remark.', array('type'=>'textarea', 'class' => 'form-control', 'required' => false, 'label' => false)); ?></td>
                   

                    <?php echo $this->Form->input('stock_take_id', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value' => $st['StockTakes']['id'])); ?>

                    <?php echo $this->Form->input('inventory_item_id.', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$stock['StockTakeItems']['inventory_item_id'])); ?>

                    <?php echo $this->Form->input('quantity_actual.', array('type' => 'hidden', 'value' => $stock['StockTakeItems']['quantity_actual'], 'required' => false, 'label' => false)); ?>

                    <?php echo $this->Form->input('quantity_variant.', array('type'=>'hidden', 'value' => $stock['StockTakeItems']['quantity_variant'], 'required' => false, 'label' => false)); ?> 

                      
                    
                    </tr>
                    
                        <?php  $bil++ ; } ?>
                    <tr>
                        <td colspan="9">
                        With selected     
                                <?php 
                                $status = 2;
                                $statusoptions=array(2 => 'Approve &nbsp &nbsp &nbsp', 3 => 'Reject &nbsp &nbsp &nbsp');
                                $statusattributes=array('legend'=>false, 'value'=>$status);
                                echo $this->Form->radio('status',$statusoptions, $statusattributes); ?>
 
                        <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success', 'id' => 'submit')); ?></td>
                    </tr>
                    <?php echo $this->Form->end(); ?>
                </tbody>
            </table>
        </div>
    </div> 
</div>
</div>

 

<?php $this->start('script'); ?>
<script type="text/javascript">

$(document).ready(function() { 
    $('#submit').click(function() {
        var checked = $("input[type=checkbox]:checked").length; 
        if(checked == 0) {
            alert("Please check at least one item.");
            return false;
        }  
    });

    $("#checkall").change(function () {  
        $(".check").prop('checked', $(this).prop("checked"));  
        $(".check").each(function() {  
            var getId = $(this).attr('id');
            var getVal = $("#check" + getId).val();
            if($(this).prop("checked") == true) {
                $("#check" + getId).val(1);
            } else {
                $("#check" + getId).val(0);
            }  
        });
    });

    $(".check").each(function() {
        $(this).change(function() {
            var getId = $(this).attr('id');
            var getVal = $("#check" + getId).val();
            if(getVal == 0) {
                $("#check" + getId).val(1);
            } else {
                $("#check" + getId).val(0);
            } 
        }); 
    });
}); 

function get_variance(row){
    var onhand = $('#onhand'+row).val();
    var actual = $('#actual'+row).val();

    var n = parseInt(actual) - parseInt(onhand);

    $('#variant'+row).val(n);
    $('#vary'+row).html(n);
    if(n != 0){
        $('#reason'+row).prop("disabled", false); 
    }else{
        $('#reason'+row).prop("disabled", true); 
    }

    $('#new_actual'+row).val(actual);
   
}

function edit_st(row){
    var max_q = $('#onhand'+row).val();
    var rs = $('#rs_notes'+row).val();
    var actual_input = '<input type="number" id="actual'+row+'" name="q" max="'+max_q+'" class="form-control" min="0" onchange="get_variance('+row+')" onKeyPress="get_variance('+row+')" onKeyUp="get_variance('+row+')"/>'
    

    var reason_st = '<select class="form-control" onchange="set_reason('+row+', this.value);" id="reason'+row+'" required="required" disabled="disabled"><option value="0">Select Reason</option><?php foreach($reasons as $reason){ ?><option value="<?php echo $reason['id']; ?>"><?php echo $reason['notes']; ?></option><?php } ?></select><input type="hidden" id="rs_notes'+row+'" value="'+rs+'" />';

    //var acts = '<button type="submit" class="btn btn-primary btn-circle-sm"><i class="fa fa-floppy-o"></i></button><button type="button" class="btn btn-danger btn-circle-sm" onclick="reset('+row+')"><i class="fa fa-times"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#rs'+row).html(reason_st);
    $('#submit_button'+row).show();
    $('#reset_button'+row).show();
    $('#edit_button'+row).hide();
}

function set_reason(row, nilai){
    $('#reason_st'+row).val(nilai);
}

function reset_field(row){
    
    var res = $('#rs_notes'+row).val();
    var actual_input = $('#actual_2'+row).val();
    var variant = $('#variant'+row).val();
    var rs = res+'<input type="hidden" id="rs_notes'+row+'" value="'+res+'">';
    //var acts = '<button type="button" class="btn btn-success btn-circle-sm" onclick="edit_st('+row+')"><i class="fa fa-pencil"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#vary'+row).html(variant);
    $('#rs'+row).html(rs);
    $('#submit_button'+row).hide();
    $('#reset_button'+row).hide();
    $('#edit_button'+row).show();
}
</script>
<?php $this->end(); ?>
