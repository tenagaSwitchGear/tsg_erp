<div class="row"> 
  	<div class="col-xs-12">
        <?php echo $this->Html->link(__('Add Stock'), array('controller'=>'inventory_stocks', 'action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Rejected Items'), array('controller'=>'inventorystockrejecteds', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Inventory Item'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('Stock List'), array('action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Rejected Items'), array('controller'=>'inventory_stock_rejecteds', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('Item General List'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('Add Stock'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Stocks Aging'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
        	<div class="inventoryStocks index">
			<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
			<thead>
			<tr> 
				<th>Code</th> 
				<th><?php echo "Items"; ?></th> 
				<th class="text-center"><?php echo '< 30 days'; ?></th>
				<th class="text-center"><?php echo '< 90 days'; ?></th>
				<th class="text-center"><?php echo '< 180 days'; ?></th>
				<th class="text-center"><?php echo '< 365 days'; ?></th>
				<th class="text-center"><?php echo '> 365 days'; ?></th>
			</tr>
			</thead>
			<tbody>
            <?php
                        $currentPage = empty($this->Paginator->params['paging']['InventoryStock']['page']) ? 1 : $this->Paginator->params['paging']['InventoryStock']['page']; $limit = $this->Paginator->params['paging']['InventoryStock']['limit'];
                        $startSN = (($currentPage * $limit) + 1) - $limit;

                        foreach ($output as $age): 
                ?>
			<tr>
                <td><?php echo ($age['InventoryItem']['code']); ?></td> 
				<td><?php echo ($age['InventoryItem']['name']); ?></td> 
				<td class="text-center"><?php echo ($age['sum_30']); ?></td>
				<td class="text-center"><?php echo ($age['sum_90']); ?></td>
				<td class="text-center"><?php echo ($age['sum_180']); ?></td>
				<td class="text-center"><?php echo ($age['sum_365']); ?></td>
				<td class="text-center"><?php echo ($age['sum_more']); ?></td>
			</tr>
		  <?php endforeach; ?>
			</tbody>
			</table>
			<p>
			<?php
				//echo $this->Paginator->counter(array(
				//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				//));
			?>	</p>
			<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
		</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>