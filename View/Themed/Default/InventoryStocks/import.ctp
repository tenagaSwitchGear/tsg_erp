<div class="row"> 
  	<div class="col-xs-12">
  		 
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Stocks'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
 

        	<!-- content start-->
        	
        	<div class="inventoryStocks index">
			<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
			 
			<tbody>
			<?php  
				foreach ($stocks as $inventoryStock): ?>
			<tr> 
			<td><?php echo ($inventoryStock['StockTemp']['id']); ?></td>
			<td><?php echo ($inventoryStock['StockTemp']['code']); ?></td> 
				<td><?php echo ($inventoryStock['StockTemp']['description']); ?></td> 
				<td><?php echo ($inventoryStock['StockTemp']['quantity']); ?> <?php echo ($inventoryStock['GeneralUnit']['name']); ?></td>
				<td><?php echo ($inventoryStock['StockTemp']['warehouse']); ?></td>
				<td><?php echo ($inventoryStock['InventoryStore']['name']); ?></td>
				<td><?php echo ($inventoryStock['StockTemp']['rack']); ?></td> 
			</tr>
		<?php endforeach; ?>
			</tbody>
			</table>
			 
			 
		</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>