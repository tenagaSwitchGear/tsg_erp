 

<div class="row"> 
    <div class="col-xs-12">  

        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo "Stock Take"; ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <h5><?php echo "Stock Take No :".$st['StockTakes']['code']; ?></h5>
            <h5><?php echo strtoupper('Warehouse'); ?>: <?php echo $st['InventoryLocations']['name']; ?></h5>
            <h5><?php echo strtoupper('Store'); ?>: <?php echo $st['InventoryStores']['name']; ?></h5>    
            <?php echo $this->Session->flash(); ?>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <thead>
                <tr>
                    <th><?php echo '#'; ?></th>
                    <th><?php echo $this->Paginator->sort('InventoryItem.code', 'Code'); ?></th>   
                    <th><?php echo $this->Paginator->sort('InventoryRack.name', 'Rack'); ?></th> 
                    <th class="col-md-1"><?php echo 'Quantity On Hand'; ?></th>
                    <th class="col-md-1"><?php echo 'Quantity Actual'; ?></th> 
                    
                </tr>
                </thead>
                <tbody>
                    <?php $bil = 1; ?>
                    <?php foreach ($st['StockTakeItems'] as $stock){ ?>
                    
                <tr>
                    <?php echo $this->Form->create('InventoryStock', array('controller'=>'InventoryStocks', 'action' => 'update_st', 'class' => 'form-horizontal submitForm', 'id' => 'formId-'.$bil)); ?>
                    
                    <?php echo $this->Form->input('type', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>'new')); ?>
                    <?php echo $this->Form->input('stock_take_id', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$st['StockTakes']['id'])); ?>
                    <td><?php echo $bil; ?>&nbsp;</td>
                    <td>
                    <?php echo $stock['InventoryItem']['code']; ?><br/>
                    <small><?php echo $stock['InventoryItem']['name']; ?></small>
                    </td> 

                    <td>
                    <?php echo $stock['InventoryRack']['name']; ?> 
                    </td> 

                    <td><?php echo $stock['StockTakeItems']['quantity_onhold']; ?>&nbsp;</td>
                    <td> &nbsp;</td>
                     
                    <?php echo $this->Form->end(); ?>
                    </tr>
                    
                        <?php  $bil++ ; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
function get_variance(row){
    var onhand = $('#onhand'+row).val();
    var actual = $('#actual'+row).val();

    var n = parseInt(actual) - parseInt(onhand);

    $('#variant'+row).val(n);
    $('#vary'+row).html(n);
    if(n != 0){
        $('#reason'+row).prop("disabled", false); 
    }else{
        $('#reason'+row).prop("disabled", true); 
    }

    $('#new_actual'+row).val(actual);
   
}

function edit_st(row){
    var max_q = $('#onhand'+row).val();
    var rs = $('#rs_notes'+row).val();
    var actual_input = '<input type="number" id="actual'+row+'" name="q" max="'+max_q+'" class="form-control" min="0" onchange="get_variance('+row+')" onKeyPress="get_variance('+row+')" onKeyUp="get_variance('+row+')"/>'
    

    var reason_st = '<select class="form-control" onchange="set_reason('+row+', this.value);" id="reason'+row+'" required="required" disabled="disabled"><option value="0">Select Reason</option><?php foreach($reasons as $reason){ ?><option value="<?php echo $reason['id']; ?>"><?php echo $reason['notes']; ?></option><?php } ?></select><input type="hidden" id="rs_notes'+row+'" value="'+rs+'" />';

    //var acts = '<button type="submit" class="btn btn-primary btn-circle-sm"><i class="fa fa-floppy-o"></i></button><button type="button" class="btn btn-danger btn-circle-sm" onclick="reset('+row+')"><i class="fa fa-times"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#rs'+row).html(reason_st);
    $('#submit_button'+row).show();
    $('#reset_button'+row).show();
    $('#edit_button'+row).hide();
}

function set_reason(row, nilai){
    $('#reason_st'+row).val(nilai);
}

function reset_field(row){
    
    var res = $('#rs_notes'+row).val();
    var actual_input = $('#actual_2'+row).val();
    var variant = $('#variant'+row).val();
    var rs = res+'<input type="hidden" id="rs_notes'+row+'" value="'+res+'">';
    //var acts = '<button type="button" class="btn btn-success btn-circle-sm" onclick="edit_st('+row+')"><i class="fa fa-pencil"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#vary'+row).html(variant);
    $('#rs'+row).html(rs);
    $('#submit_button'+row).hide();
    $('#reset_button'+row).hide();
    $('#edit_button'+row).show();
}

$(document).ready(function() {
    $('.submitForm').each(function() {
        $(this).on('submit', function(e) {
            e.preventDefault();
            var formId = $(this).attr('id');
            var no = formId.split('-');
            no = no[1];
            var data = $(this).serializeArray();
            console.log(data);
            $.ajax({
                url: baseUrl + 'inventory_stocks/update_st',
                type: 'post',
                data: data, 
                dataType: 'json',
                beforeSend: function() { 
                },
                complete: function() { 
                },
                success: function(json) { 
                    var html = '';
                    if(json.status === true) {
                        html += '<button id="edit_button'+no+'" type="button" class="btn btn-success btn-circle-sm" onclick="edit_st('+no+')"><i class="fa fa-pencil"></i></button>';
                    } 
                    $('#status-'+no).html('Waiting for Verification');
                    $('#button-'+no).html(html);
                    console.log(json);
                }, error: function(data) {
                   console.log(data);
                }
            });
        });
    });
});
</script>
<?php $this->end(); ?>
