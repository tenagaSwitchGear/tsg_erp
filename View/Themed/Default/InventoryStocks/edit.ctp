<div class="row"> 
  	<div class="col-xs-12"> 
  		<?php echo $this->Html->link(__('Stock List'), array('action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
 
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add Stock'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 
			<?php echo $this->Form->create('InventoryStock', array('class' => 'form-horizontal')); ?> 
			<?php echo $this->Form->input('id'); ?>
			<div class="form-group">
				<label class="col-sm-3">Item Code</label>
				<div class="col-sm-9">
				<?php echo $this->Form->input('InventoryItem.code', array('id' => 'findItem', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Item code / name')); ?>
				<?php echo $this->Form->input('inventory_item_id', array('id' => 'productId', 'type' => 'hidden')); ?>
			</div>
			</div>

			
			 
			<?php echo $this->Form->input('inventory_delivery_order_item_id', array('type' => 'hidden', 'value' => 0)); ?>
			  
			<?php echo $this->Form->input('general_unit_converter_id', array('type' => 'hidden', 'value' => 0)); ?>
			 
			<div class="form-group">
				<label class="col-sm-3">Price per unit</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('price_per_unit', array('class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Quantity</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('quantity', array('class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Unit</label>
				<div class="col-sm-9">
					<?php echo $this->Form->input('general_unit_id', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Issued Qty</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('issued_quantity', array('value' => 0, 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div> 
			<?php echo $this->Form->input('qc_status', array('type' => 'hidden', 'label' => false, 'value' => 1)); ?>
			<div class="form-group">
				<label class="col-sm-3">Receive Date</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('receive_date', array('id' => 'dateonly', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Expiry Date (Optional)</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('expiry_date', array('id' => 'dateonly_2', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Warehouse</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('inventory_location_id', array('empty' => '-Select Warehouse-', 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Store</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('inventory_store_id', array('empty' => '-Select Store-', 'class' => 'form-control', 'label' => false)); ?>
			</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3">Rack</label>
				<div class="col-sm-9">
			<?php echo $this->Form->input('rack', array('class' => 'form-control', 'label' => false)); ?>
			</div>
			</div> 
			<?php echo $this->Form->input('inventory_delivery_location_id', array('type' => 'hidden', 'value' => 0)); ?>
			 
	 
			<?php echo $this->Form->input('type', array('type' => 'hidden', 'value' => 0)); ?> 
			<?php echo $this->Form->input('finished_good_id', array('type' => 'hidden', 'value' => 0)); ?>  
			<div class="form-group"> 
      			<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
      		</div>	  
		  <?php $this->Form->end(); ?>
		</div>
	</div>
</div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
 
$(function() { 
    $('#findItem').autocomplete({ 
	    source: function (request, response){ 
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'inventory_items/ajaxfinditem',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#findItem').val(),                                                    
	            success: function (data) { 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.code,
		                    name : item.name,
		                    price: item.price,
		                    code: item.code,
		                    type: item.type
		                }
		            }));
		        }
	        });
	    },
	    select: function (event, ui) {  
	        $('#productId').val( ui.item.id );  
	    },
	    minLength: 3
	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.name + "<br><small>" + item.code + "</small><br/><small>" + item.type + "</small><br>" +  "</div>" ).appendTo( ul );
    };
});
</script>
<?php $this->end(); ?>

 
