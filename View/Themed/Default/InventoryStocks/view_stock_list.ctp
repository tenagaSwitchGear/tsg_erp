<?php if(isset($_GET['location_id']) && $_GET['location_id']!=''){ $location_id = $_GET['location_id']; }else{ $location_id = ''; } ?>
<?php if(isset($_GET['store_id']) && $_GET['store_id']!=''){ $store_id = $_GET['store_id']; }else{ $store_id = ''; } ?>
<?php $arr = array('location_id'=>$location_id, 'store_id'=>$store_id); ?>
<div class="book">
    <div class="page">
        <div id="content" class="container page-break">  
        <h3><strong><?php echo strtoupper('Stock Take'); ?></strong></h3>
        <h3><?php echo strtoupper('Warehouse'); ?>: <?php echo $stocks[0]['InventoryLocation']['name']; ?></h3>
        <h3><?php echo strtoupper('Store'); ?>: <?php echo $stocks[0]['InventoryStore']['name']; ?></h3>
            <div class="table-responsive">  
                <table style="font-size: 14px; width: 100%;" class="table table-bordered">
                    <thead>
                    <tr> 
                        <th>#</th>
                        <th><?php echo 'Items'; ?></th>
                        <th><?php echo 'Unit'; ?></th>
                        <th><?php echo 'Quantity On Hand'; ?></th>
                        <th><?php echo 'Quantity Actual'; ?></th>
                        <th><?php echo 'Variance'; ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $bil=1; ?>
                    <?php foreach ($stocks as $stock): ?>
                    <tr>
                        <td><?php echo $bil; ?></td>
                        <td><?php echo $stock['InventoryItem']['code']; ?><br/>
                        <small><?php echo $stock['InventoryItem']['name']; ?></small>
                        </td>
                        <td><?php echo $stock['GeneralUnit']['name']; ?></td>
                        <td><?php echo $stock['Sum']['total_quantity']; ?>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        
                    </tr>
                <?php $bil++;  endforeach; ?>
                    </tbody>
                </table>               
            </div>
        </div>
    </div>
</div>
