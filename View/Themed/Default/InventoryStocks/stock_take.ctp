<?php echo $this->Html->link(__('Stock Takes'), array('action' => 'stock_take'), array('class'=>'btn btn-info btn-sm')); ?>
         <?php if(empty($period['Period']) || $period['Period'] == 'Closed'){?>
        <?php echo $this->Html->link(__('Start Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'start_stock_take'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php }else{ ?>
            <?php echo $this->Html->link(__('Stop Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'stop_stock_take?startdate='.$period['StartDate']), array('class'=>'btn btn-danger btn-sm')); ?>
           <?php  } ?>
        <?php echo $this->Html->link(__('Stock Take List'), array('controller'=>'inventory_stocks', 'action' => 'st_list'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Stock Take Verification'), array('controller'=>'inventory_stocks', 'action' => 'stock_take_verify'), array('class'=>'btn btn-info btn-sm')); ?>

<div class="row"> 
  	<div class="col-xs-12">      
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Stock Takes'); ?></h2>
                <?php if(isset($_GET['location_id']) && $_GET['location_id']!=''){ $location_id = $_GET['location_id']; }else{ $location_id = ''; } ?>
                <?php if(isset($_GET['store_id']) && $_GET['store_id']!=''){ $store_id = $_GET['store_id']; }else{ $store_id = ''; } ?>
                <?php if(empty($period['Period']) || $period['Period'] == 'Closed'){}else{ ?>
                <?php if(isset($period['StartDate']) && $period['StartDate']!=''){ $start_date = $period['StartDate']; }else{ $start_date = ''; } ?>
                <?php 
                    echo $this->Html->link('<i class="fa fa-print"></i> Create Stock Take', array('action' => 'view_stock_list_2?location_id='.$location_id.'&store_id='.$store_id.'&search=Search&startdate='.$start_date), array('class' => 'btn btn-success pull-right btn-sm', 'escape'=>false, 'target'=>"_blank")); 
                 ?>
                 <?php } ?>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
            <?php echo $this->Session->flash(); ?> 
        	<!-- content start-->
            <?php echo $this->Form->create('InventoryStock', array('action' => 'stock_take', 'class' => 'form-horizontal', 'type' => 'GET')); ?>
            <table cellpadding="0" cellspacing="0" class="table">
                <tr>
                    <td><?php echo $this->Form->input('location_id', array('options' => $locations, 'class' => 'form-control', 'required' => false, 'empty' => '-All Warehouse-', 'label' => false, 'onchange'=>'get_store(this.value)')); ?>  
                    </td>
                    <td id="store"><?php echo $this->Form->input('store_id', array('class' => 'form-control', 'required' => true, 'empty' => '-All Store-', 'label' => false)); ?>  
                    </td> 
                    <td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
                </tr>
            </table>
            <?php $this->Form->end(); ?>    
        	<div class="table-responsive">
                <table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th><?php echo $this->Paginator->sort('InventoryItem.name', 'Items'); ?></th>
                            <th><?php echo 'Quantity'; ?></th>
                            <th><?php echo 'Warehouse'; ?></th>
                            <th><?php echo 'Store'; ?></th>
                            <th><?php echo 'Rack'; ?></th>    
                            <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $currentPage = empty($this->Paginator->params['paging']['InventoryStock']['page']) ? 1 : $this->Paginator->params['paging']['InventoryStock']['page']; $limit = $this->Paginator->params['paging']['InventoryStock']['limit'];
                            $startSN = (($currentPage * $limit) + 1) - $limit;
                            foreach ($stocks as $stock): ?>
                        <tr>
                            <td class="text-center"><?php echo $startSN++; ?></td>
                            <td><?php echo h($stock['InventoryItem']['name']); ?><br><?php echo h($stock['InventoryItem']['code']); ?>&nbsp;</td>
                            <td><?php echo h($stock['Sum']['total_quantity']); ?>&nbsp;</td>
                            <td><?php echo !empty($stock['InventoryLocation']['name']) ? $stock['InventoryLocation']['name'] : ''; ?>&nbsp;</td>
                            <td><?php echo !empty($stock['InventoryStore']['name']) ? $stock['InventoryStore']['name'] : ''; ?>&nbsp;</td>
                            <td><?php if(empty($stock['InventoryRack'])){ ?><?php echo $stock['InventoryStock']['rack']; ?><?php }else{ ?><?php echo $stock['InventoryRack']['name']; ?><?php } ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link('<i class="fa fa-search"></i>', '', array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
                                <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', '', array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
                                <?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', '', array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false)); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <p><?php
                    //echo $this->Paginator->counter(array(
                    //  'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                    //));
                ?></p>
                <ul class="pagination">
                <?php
                  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
                  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
                ?>
                </ul>
            </div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript"> 
function get_store(nilai){
    $.ajax({
        type: "GET",                        
        url:baseUrl + 'inventory_stocks/ajaxfindstore',           
        contentType: "application/json",
        dataType: "json",
        data: "location_id=" + nilai,                                                    
        success: function (response) {
            console.log(response);
            //$("#item").html(JSON.stringify(response));
            //var data = JSON.stringify(response);
            html = '<select class="form-control" required name="store_id">';
            html += '<option value="">-Select Store-</option>';
            html += '<option value="x">All Store</option>';
            
            for (i = 0; i < response.length; i++) {
                html += '<option value="'+response[i].InventoryStore.id+'">';
                html += response[i].InventoryStore.name;
                html += '</option>';
            }
            html += '</select>';
            $("#store").html(html);
        }
    });
}
</script>
<?php $this->end(); ?>