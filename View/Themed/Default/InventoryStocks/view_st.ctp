<?php echo $this->Html->link(__('Stock Takes'), array('action' => 'stock_take'), array('class'=>'btn btn-info btn-sm')); ?>
         <?php if(empty($period['Period']) || $period['Period'] == 'Closed'){?>
        <?php echo $this->Html->link(__('Start Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'start_stock_take'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php }else{ ?>
            <?php echo $this->Html->link(__('Stop Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'stop_stock_take?startdate='.$period['StartDate']), array('class'=>'btn btn-danger btn-sm')); ?>
           <?php  } ?>
        <?php echo $this->Html->link(__('Stock Take List'), array('controller'=>'inventory_stocks', 'action' => 'st_list'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Stock Take Verification'), array('controller'=>'inventory_stocks', 'action' => 'stock_take_verify'), array('class'=>'btn btn-info btn-sm')); ?>

<?php echo $this->Html->link('<i class="fa fa-print"></i> Print', array('action' => 'print_st', $st['InventoryStores']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false, 'target' => '_blank')); ?>

<div class="row"> 
    <div class="col-xs-12">  

        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo "Stock Take"; ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <h5><?php echo "Stock Take No :".$st['StockTakes']['code']; ?></h5>
            <h5><?php echo strtoupper('Warehouse'); ?>: <?php echo $st['InventoryLocations']['name']; ?></h5>
            <h5><?php echo strtoupper('Store'); ?>: <?php echo $st['InventoryStores']['name']; ?></h5>    
            <?php echo $this->Session->flash(); ?>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <thead>
                <tr>
                    <th><?php echo '#'; ?></th>
                    <th><?php echo $this->Paginator->sort('InventoryItem.code', 'Code'); ?></th>   
                    <th><?php echo $this->Paginator->sort('InventoryRack.name', 'Rack'); ?></th>   
                    <th class="col-md-1"><?php echo 'Quantity On Hand'; ?></th>
                    <th class="col-md-1"><?php echo 'Quantity Actual'; ?></th>
                    <th class="col-md-1"><?php echo 'Variance'; ?></th>
                    <th><?php echo 'Reason'; ?></th>
                    <th><?php echo 'Status'; ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php $bil = 1; ?>
                    <?php foreach ($st['StockTakeItems'] as $stock){ ?>
                    
                <tr>
                    <?php echo $this->Form->create('InventoryStock', array('controller'=>'InventoryStocks', 'action' => 'update_st', 'class' => 'form-horizontal submitForm', 'id' => 'formId-'.$bil)); ?>
                    
                    <?php echo $this->Form->input('type', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>'new')); ?>
                    <?php echo $this->Form->input('stock_take_id', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$st['StockTakes']['id'])); ?>
                    <td><?php echo $bil; ?>&nbsp;</td>
                    <td>
                    <?php echo $stock['InventoryItem']['code']; ?><br/>
                    <small><?php echo $stock['InventoryItem']['name']; ?></small>
                    </td> 

                    <td>
                    <?php echo $stock['InventoryStock']['InventoryStock']['rack']; ?> 
                    </td> 

                    <td><?php echo $stock['StockTakeItems']['quantity_onhold']; ?>&nbsp;</td>
                    <?php if($stock['StockTakeItems']['status'] == 0 ){ ?>
                    <td><?php echo $this->Form->input('stock_take_item_id', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$stock['StockTakeItems']['id'])); ?>
                    <?php echo $this->Form->input('quantity_onhand', array('id'=>'onhand'.$bil, 'type'=>'hidden', 'label' => false, 'value'=>$stock['StockTakeItems']['quantity_onhold'])); ?>
                    <?php echo $this->Form->input('quantity_actual', array('min'=>'0', 'onchange'=>'get_variance('.$bil.')', 'onKeyPress'=>'get_variance('.$bil.')', 'onKeyUp'=>'get_variance('.$bil.')', 'type'=>'text','max'=>$stock['StockTakeItems']['quantity_onhold'], 'id' => 'actual'.$bil, 'class' => 'form-control', 'required' => true, 'label' => false)); ?>
                    <?php echo $this->Form->input('quantity_variant', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'id' => 'variant'.$bil, 'label' => false)); ?>
                    <td id="vary<?php echo $bil; ?>"></td>
                    <td id="reason-<?php echo $bil; ?>"><select class="form-control" name="data[InventoryStock][stock_take_reason_id]" id="reason<?php echo $bil; ?>" required="required" disabled="disabled"><option value="0">Select Reason</option>
                    <?php foreach($reasons as $reason){ ?>
                    <option value="<?php echo $reason['id']; ?>"><?php echo $reason['notes']; ?></option>
                    <?php } ?>
                    </select></td>
                    <td id="status-<?php echo $bil; ?>">
                    <?php if($stock['StockTakeItems']['status'] == 0){ $status = 'Pending'; }
                    else if($stock['StockTakeItems']['status'] == 1){ $status = 'Waiting for Verification'; }
                    else if($stock['StockTakeItems']['status'] == 2){ $status = 'Verified'; }
                    else{ $status = 'Rejected'; } ?><?php echo $status; ?></td>
                    <td class="actions" id="button-<?php echo $bil; ?>">
                        <?php echo $this->Form->button('<i class="fa fa-floppy-o"></i>', array('type'=>'submit', 'class' => 'btn btn-primary btn-circle-sm', 'escape'=>false)); ?>
                       
                    </td>
                    <?php }else{ ?>
                    <?php echo $this->Form->input('type', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>'edit')); ?>
                    <?php echo $this->Form->input('stock_take_item_id', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'label' => false, 'value'=>$stock['StockTakeItems']['id'])); ?>
                    <?php echo $this->Form->input('quantity_onhand', array('id'=>'onhand'.$bil, 'type'=>'hidden', 'label' => false, 'value'=>$stock['StockTakeItems']['quantity_onhold'])); ?>
                    <?php echo $this->Form->input('quantity_actual', array('id'=>'actual_2'.$bil, 'type'=>'hidden', 'label' => false, 'value'=>$stock['StockTakeItems']['quantity_actual'])); ?>
                    <?php echo $this->Form->input('new_quantity_actual', array('id'=>'new_actual'.$bil, 'type'=>'hidden', 'label' => false)); ?>
                    <?php echo $this->Form->input('quantity_variant', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'id' => 'variant'.$bil, 'label' => false, 'value'=>$stock['StockTakeItems']['quantity_variant'])); ?>
                    <?php echo $this->Form->input('stock_take_reason_id', array('type'=>'hidden', 'class' => 'form-control', 'required' => false, 'id' => 'reason_st'.$bil, 'label' => false)); ?>
                    <td id="actual_q<?php echo $bil; ?>"><?php echo $stock['StockTakeItems']['quantity_actual']; ?></td>
                    <td id="vary<?php echo $bil; ?>"><?php echo $stock['StockTakeItems']['quantity_variant']; ?></td>
                    <td id="rs<?php echo $bil; ?>"><?php if(!empty($stock['StockTakeReasons'])){ ?><?php echo $stock['StockTakeReasons']['notes']; ?><input type="hidden" name="rs_notes<?php echo $bil; ?>" id="rs_notes<?php echo $bil; ?>" value="<?php echo $stock['StockTakeReasons']['notes']; ?>"><?php }else{ ?><input type="hidden" name="rs_notes<?php echo $bil; ?>" id="rs_notes<?php echo $bil; ?>" value=""><?php } ?></td>
                    <td><?php if($stock['StockTakeItems']['status'] == 0){ $status = 'Pending'; $remark=''; }
                    else if($stock['StockTakeItems']['status'] == 1){ $status = 'Waiting for Verification'; $remark=''; }
                    else if($stock['StockTakeItems']['status'] == 2){ $status = 'Verified'; $remark =''; }
                    else{ $status = 'Rejected'; $remark = 'Remark : '.$stock['StockTakeItems']['approval_remark']; } ?><?php echo $status.'<br>'.$remark; ?></td>
                    <td class="actions<?php echo $bil; ?>">
                        <?php if($stock['StockTakeItems']['status']==2){

                            }else{ ?>
                        <?php echo $this->Form->button('<i class="fa fa-times"></i>', array('onClick'=>'reset_field('.$bil.');', 'id'=>'reset_button'.$bil, 'type'=>'button', 'class' => 'btn btn-danger btn-circle-sm', 'escape'=>false, 'style'=>'display:none', )); ?>
                        <?php echo $this->Form->button('<i class="fa fa-pencil"></i>', array('id'=>'edit_button'.$bil,'type'=>'button', 'class' => 'btn btn-success btn-circle-sm', 'escape'=>false, 'onClick' => 'edit_st('.$bil.')')); ?>
                        <?php echo $this->Form->button('<i class="fa fa-floppy-o"></i>', array('id'=>'submit_button'.$bil, 'type'=>'submit', 'class' => 'btn btn-primary btn-circle-sm', 'escape'=>false, 'style'=>'display:none')); ?>
                       <?php } ?>
                    </td>
                    <?php } ?>
                    <?php echo $this->Form->end(); ?>
                    </tr>
                    
                        <?php  $bil++ ; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<?php $this->start('script'); ?>
<script type="text/javascript">
function get_variance(row){
    var onhand = $('#onhand'+row).val();
    var actual = $('#actual'+row).val();

    var n = parseInt(actual) - parseInt(onhand);

    $('#variant'+row).val(n);
    $('#vary'+row).html(n);
    if(n != 0){
        $('#reason'+row).prop("disabled", false); 
    }else{
        $('#reason'+row).prop("disabled", true); 
    }

    $('#new_actual'+row).val(actual);
   
}

function edit_st(row){
    var max_q = $('#onhand'+row).val();
    var rs = $('#rs_notes'+row).val();
    var actual_input = '<input type="number" id="actual'+row+'" name="q" max="'+max_q+'" class="form-control" min="0" onchange="get_variance('+row+')" onKeyPress="get_variance('+row+')" onKeyUp="get_variance('+row+')"/>'
    

    var reason_st = '<select class="form-control" onchange="set_reason('+row+', this.value);" id="reason'+row+'" required="required" disabled="disabled"><option value="0">Select Reason</option><?php foreach($reasons as $reason){ ?><option value="<?php echo $reason['id']; ?>"><?php echo $reason['notes']; ?></option><?php } ?></select><input type="hidden" id="rs_notes'+row+'" value="'+rs+'" />';

    //var acts = '<button type="submit" class="btn btn-primary btn-circle-sm"><i class="fa fa-floppy-o"></i></button><button type="button" class="btn btn-danger btn-circle-sm" onclick="reset('+row+')"><i class="fa fa-times"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#rs'+row).html(reason_st);
    $('#submit_button'+row).show();
    $('#reset_button'+row).show();
    $('#edit_button'+row).hide();
}

function set_reason(row, nilai){
    $('#reason_st'+row).val(nilai);
}

function reset_field(row){
    
    var res = $('#rs_notes'+row).val();
    var actual_input = $('#actual_2'+row).val();
    var variant = $('#variant'+row).val();
    var rs = res+'<input type="hidden" id="rs_notes'+row+'" value="'+res+'">';
    //var acts = '<button type="button" class="btn btn-success btn-circle-sm" onclick="edit_st('+row+')"><i class="fa fa-pencil"></i></button>';

    $('#actual_q'+row).html(actual_input);
    $('#vary'+row).html(variant);
    $('#rs'+row).html(rs);
    $('#submit_button'+row).hide();
    $('#reset_button'+row).hide();
    $('#edit_button'+row).show();
}

$(document).ready(function() {
    $('.submitForm').each(function() {
        $(this).on('submit', function(e) {
            e.preventDefault();
            var formId = $(this).attr('id');
            var no = formId.split('-');
            no = no[1];
            var data = $(this).serializeArray();
            console.log(data);
            $.ajax({
                url: baseUrl + 'inventory_stocks/update_st',
                type: 'post',
                data: data, 
                dataType: 'json',
                beforeSend: function() { 
                },
                complete: function() { 
                },
                success: function(json) { 
                    var html = '';
                    if(json.status === true) {
                        html += '<button id="edit_button'+no+'" type="button" class="btn btn-success btn-circle-sm" onclick="edit_st('+no+')"><i class="fa fa-pencil"></i></button>';
                    } 
                    $('#status-'+no).html('Waiting for Verification');
                    $('#button-'+no).html(html);
                    console.log(json);
                }, error: function(data) {
                   console.log(data);
                }
            });
        });
    });
});
</script>
<?php $this->end(); ?>
