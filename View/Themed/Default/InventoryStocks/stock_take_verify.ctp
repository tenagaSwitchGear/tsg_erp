<?php echo $this->Html->link(__('Stock Takes'), array('action' => 'stock_take'), array('class'=>'btn btn-info btn-sm')); ?>
         <?php if(empty($period['Period']) || $period['Period'] == 'Closed'){?>
        <?php echo $this->Html->link(__('Start Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'start_stock_take'), array('class'=>'btn btn-success btn-sm')); ?>
        <?php }else{ ?>
            <?php echo $this->Html->link(__('Stop Stock Take'), array('controller'=>'inventory_stocks', 'action' => 'stop_stock_take?startdate='.$period['StartDate']), array('class'=>'btn btn-danger btn-sm')); ?>
           <?php  } ?>
        <?php echo $this->Html->link(__('Stock Take List'), array('controller'=>'inventory_stocks', 'action' => 'st_list'), array('class'=>'btn btn-primary btn-sm')); ?>
        <?php echo $this->Html->link(__('Stock Take Verification'), array('controller'=>'inventory_stocks', 'action' => 'stock_take_verify'), array('class'=>'btn btn-info btn-sm')); ?>

<div class="row"> 
    <div class="col-xs-12">  
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo "Stock Take List"; ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php echo $this->Session->flash(); ?>
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <thead>
                <tr>
                    <th><?php echo '#'; ?></th>
                    <th><?php echo 'Stock Take No'; ?></th>
                    <th><?php echo 'Created'; ?></th>
                    <th><?php echo 'Start Date'; ?></th>
                    <th><?php echo 'Status'; ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php $bil = 1; ?>
                    <?php foreach ($st as $stock){ ?>
                <tr>
                    <td><?php echo $bil; ?>&nbsp;</td>
                    <td><?php echo $stock['StockTakes']['code']; ?>&nbsp;</td>
                    <td><?php echo date('d-m-Y', strtotime($stock['StockTakes']['created'])); ?>&nbsp;</td>
                    <?php if($stock['StockTakes']['start_date'] == '000-00-00 00:00:00') { $start = ''; }else{ $start = $stock['StockTakes']['start_date']; } ?>
                    <td><?php echo $start; ?>&nbsp;</td>
                    <?php if($stock['StockTakes']['end_date'] == '000-00-00 00:00:00') { $end = ''; }else{ $end = $stock['StockTakes']['end_date']; } ?>
                    <td><?php echo $end; ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view_st_verify', $stock['StockTakes']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
                        <?php //echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit_st', $stock['StockTakes']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
                        <?php //echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete_st', $stock['StockTakes']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($stock['StockTakes']['code']).'"?', $stock['StockTakes']['id'])); ?>
                    </td>
                    </tr>
                        <?php  $bil++ ; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
