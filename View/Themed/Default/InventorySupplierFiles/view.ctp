<div class="inventorySupplierFiles view">
<h2><?php echo __('Inventory Supplier File'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierFile['InventorySupplierFile']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Supplier'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierFile['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $inventorySupplierFile['InventorySupplier']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dir'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierFile['InventorySupplierFile']['dir']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierFile['InventorySupplierFile']['file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierFile['InventorySupplierFile']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierFile['InventorySupplierFile']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Supplier File'), array('action' => 'edit', $inventorySupplierFile['InventorySupplierFile']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Supplier File'), array('action' => 'delete', $inventorySupplierFile['InventorySupplierFile']['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierFile['InventorySupplierFile']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Files'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier File'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
