<div class="inventorySupplierFiles index">
	<h2><?php echo __('Inventory Supplier Files'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_supplier_id'); ?></th>
			<th><?php echo $this->Paginator->sort('dir'); ?></th>
			<th><?php echo $this->Paginator->sort('file'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventorySupplierFiles as $inventorySupplierFile): ?>
	<tr>
		<td><?php echo h($inventorySupplierFile['InventorySupplierFile']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventorySupplierFile['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $inventorySupplierFile['InventorySupplier']['id'])); ?>
		</td>
		<td><?php echo h($inventorySupplierFile['InventorySupplierFile']['dir']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierFile['InventorySupplierFile']['file']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierFile['InventorySupplierFile']['type']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierFile['InventorySupplierFile']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventorySupplierFile['InventorySupplierFile']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventorySupplierFile['InventorySupplierFile']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventorySupplierFile['InventorySupplierFile']['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierFile['InventorySupplierFile']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Inventory Supplier File'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
