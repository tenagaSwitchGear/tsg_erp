<div class="inventorySupplierFiles form">
<?php echo $this->Form->create('InventorySupplierFile'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Supplier File'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('inventory_supplier_id');
		echo $this->Form->input('dir');
		echo $this->Form->input('file');
		echo $this->Form->input('type');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventorySupplierFile.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventorySupplierFile.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Files'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Suppliers'), array('controller' => 'inventory_suppliers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'add')); ?> </li>
	</ul>
</div>
