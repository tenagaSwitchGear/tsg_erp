<?php echo $this->Html->link(__('Add New Job'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
<?php
	function status($status) {
		if($status == 0) {
			return 'Draft';
		} else if($status == 1) {
			return 'Waiting HOS Verification';
		} else if($status == 2) {
			return 'Waiting HOD Verification';
		} else if($status == 3) {
			return 'Waiting GM Approval';
		} else if($status == 4) {
			return 'Verified';
		} else if($status == 5) {
			return 'Rejected';
		}
	}
?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Jobs List</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
<div class="table-responsive">
	<h2><?php echo __('Sale Jobs'); ?></h2>
	<?php echo $this->Session->flash(); ?>
	<table class="table">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('contract_no'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_quotation_id'); ?></th> 
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th> 
			<th><?php echo $this->Paginator->sort('created'); ?></th>  
			<th><?php echo $this->Paginator->sort('status'); ?></th> 
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleJobs as $saleJob): ?>
	<tr>
		<td><?php echo h($saleJob['SaleJob']['id']); ?>&nbsp;</td>
		<td><?php echo h($saleJob['SaleJob']['name']); ?>&nbsp;</td>
		<td><?php echo h($saleJob['SaleJob']['contract_no']); ?>&nbsp;</td>
		<td><?php echo h($saleJob['SaleQuotation']['name']); ?>&nbsp;</td> 
		<td>
			<?php echo $this->Html->link($saleJob['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleJob['Customer']['id'])); ?>
		</td> 
		<td><?php echo h($saleJob['SaleJob']['created']); ?>&nbsp;</td>  
		<td><?php echo status($saleJob['SaleJob']['status']); ?>&nbsp;</td> 
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleJob['SaleJob']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleJob['SaleJob']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleJob['SaleJob']['id']), array(), __('Are you sure you want to delete # %s?', $saleJob['SaleJob']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
 

</div>
    </div>
  </div> 
</div>

