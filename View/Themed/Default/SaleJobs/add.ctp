<?php echo $this->Html->link(__('Active Job'), array('action' => 'index'), array('class' => 'btn btn-success')); ?>
<?php echo $this->Html->link(__('Draft Job'), array('action' => 'index', 0), array('class' => 'btn btn-default')); ?> 
<?php echo $this->Html->link(__('Cancelled Job'), array('action' => 'index', 5), array('class' => 'btn btn-danger')); ?> 

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add New Job</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 		<div class="saleOrders form">
		<?php echo $this->Form->create('SaleJob', array('class' => 'form-horizontal form-label-left', 'id' => 'form')); ?> 
	 	
	 	<div class="form-group">
	    	<label class="col-sm-3">Enter Quotation No *</label>
	    	<div class="col-sm-9">
	    		<?php echo $this->Form->input('SaleQuotation.name', array('id' => 'find_quotation', 'class' => 'form-control', 'label' => false)); ?>

	    		<div>
	    		<?php echo $this->Form->input('sale_tender_id', array('id' => 'tender_id', 'value' => 0, 'type' => 'hidden', 'label' => false)); ?>
	    		<?php echo $this->Form->input('sale_quotation_id', array('id' => 'quotation_id', 'value' => 0, 'type' => 'hidden', 'label' => false)); ?>
	    		 </div>
	    	</div>
        </div>  


	 	<div class="form-group">
	    	<label class="col-sm-3">Customer *</label>
	    	<div class="col-sm-9">
	    		<?php echo $this->Form->input('customer_id', array('options' => $Customer, 'empty' => '-Select-', 'id' => 'customer_id', 'label' => false, 'class' => 'form-control')); ?>

	    		<div> 
	    		</div>
	    	</div>
        </div>  

 		
        		<div class="form-group">
					<label class="col-sm-3">Contract No *</label>
					<div class="col-sm-9">
				<?php echo $this->Form->input('contract_no', array('class' => 'form-control', 'label' => false)); ?>
				</div>
				</div> 
 
				<?php echo $this->Form->input('name', array('id' => 'name', 'type' => 'hidden', 'class' => 'form-control', 'label' => false)); ?>
				 
				
				<div class="form-group">
					<label class="col-sm-3">Status *</label>
					<div class="col-sm-9">
					<?php $status = array(4 => 'Active', 0 => 'Draft'); ?>
					<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
					</div>
				</div> 

				<div class="form-group">
					<h4>Add Station *</h4>
					<p>Please click Add Station button to add Station name.</p>
				</div>

				<div id="output"></div>

				<div class="form-group"><div id="error"></div></div>

				<div class="form-group"> 
		            <div class="col-sm-12">
		                <a href="#" id="addMore" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Station</a>
		            </div>
		        </div>

				<div class="form-group">
				<label class="col-sm-3">&nbsp;</label>
				<div class="col-sm-9">
					<?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
				</div>
			</div> 
			 
		<?php echo $this->Form->end(); ?>
		</div>
		 
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">


    function findBomByCategory(row) {    
        var category_id = $('#bomCategory'+row).val();
        $.ajax({ 
            type: "GET", 
            dataType: 'json',
            data: 'category_id=' + $('#bomCategory'+row).val(),
            url: baseUrl + 'boms/ajaxfindbombycategoryid', 
            success: function(respond) { 
                var option = '<option value="">Select BOM</option>'; 
                $.each(respond, function(i, item) {
                    console.log(90); 
                    option += '<option value="' + item.id + '">' + item.name + '</option>';
                }); 
                $('#moreBomId'+row).html(option);
            }
        }); 
        return false; 
    } 

    function removeBom(id) {
        $('#removeBom'+id).html('');
        return false;
    }

    function selectDate(row) {
	 	$('#datepicker'+row).datepicker({
	      dateFormat: 'yy-mm-dd', 
	      ampm: true
	    }); 
    }

    function selectDateTwo(row) {
	 	$('#datepicker2'+row).datepicker({
	      dateFormat: 'yy-mm-dd', 
	      ampm: true
	    }); 
    }
$(document).on('focus', '.datepicker', function() {
    $(this).datepicker({
  dateFormat: 'yy-mm-dd', 
  ampm: true
});
});

$(document).on('focus', '.datepicker2', function() {
    $(this).datepicker({
  dateFormat: 'yy-mm-dd', 
  ampm: true
});
});

$(document).ready(function() { 
	$('#form').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        return false;
      }
    }); 
    var row = 1;
    $('#addMore').click(function() { 
    	var mainCode = $('#name').val();
    	//if(mainCode == '') {
		//	$('#error').html('<p style="color: red">Please fill Job Number before add Sub Station.</p>');  
		//} else {
			$('#error').html('');
			var html = '<div class="row" id="removeBom'+row+'">'; 
	       
	        html += '<div class="col-sm-3">';
	        html += '<input type="text" name="data[SaleJobChild][name][]" class="form-control" placeholder="Enter Station Name"required>'; 
	        html += '</div>';
	        html += '<div class="col-sm-3">';
	        html += '<input type="text" name="data[SaleJobChild][old_job_no][]" class="form-control" placeholder="Job Ref">'; 
	        html += '</div>';

	        html += '<div class="col-sm-5">';
	        html += '<textarea name="data[SaleJobChild][description][]" class="form-control" placeholder="Remark"></textarea>';
	        html += '</div>';

	        html += '<div class="col-sm-1">';
	        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+row+'); return false"><i class="fa fa-times"></i></a>';
	        html += '</div>';
	        html += '</div>';     
	        row++; 

 
	        $("#output").append(html); 	   
		//}
        
        return false;
    });  
  	


    $("#find_quotation").autocomplete({
	    source: function (request, response) {
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_quotations/ajaxfindquotation',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#find_quotation').val(),                                                    
	            success: function (data) {
	            	console.log(data); 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.name,
		                    customer: item.customer,
		                    tender_name: item.tender_name,
		                    tender_no: item.tender_no,
		                    tender_id: item.tender_id,
		                    customer_id: item.customer_id
		                }
		            }));
	        	}
	        });
	    },
	    select: function (event, ui) {
	        $("#quotation_id").val(ui.item.id);//Put Id in a hidden field
	        $("#customer_id").val(ui.item.customer_id);
	        $("#tender_id").val(ui.item.tender_id);
	    },
	    minLength: 3

	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.customer + "<br>" + item.tender_no + "<br>" +  "</div>" ).appendTo( ul );
    };

});  
</script>
<?php $this->end(); ?>