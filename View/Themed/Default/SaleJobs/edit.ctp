

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit Job</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 		<div class="saleOrders form">
		<?php echo $this->Form->create('SaleJob', array('class' => 'form-horizontal form-label-left')); ?> 
	 	
	 	<div class="form-group">
	    	<label class="col-sm-3">Job No *</label>
	    	<div class="col-sm-9">
	    		<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control', 'readonly' => true)); ?>

	    		<div> 
	    		</div>
	    	</div>
        </div>  

		<div class="form-group">
	    	<label class="col-sm-3">Customer *</label>
	    	<div class="col-sm-9">
	    		<?php echo $this->Form->input('customer_id', array('options' => $Customer, 'empty' => '-Select-', 'id' => 'customer_id', 'label' => false, 'class' => 'form-control')); ?>

	    		<div> 
	    		</div>
	    	</div>
        </div>  

 		<div class="form-group">
	    	<label class="col-sm-3">Enter Quotation No</label>
	    	<div class="col-sm-9">
	    	<?php echo $this->Form->input('id'); ?>
	    	
	    		<?php echo $this->Form->input('SaleQuotation.name', array('id' => 'find_quotation', 'class' => 'form-control', 'label' => false)); ?>

	    		<div>
	    		<?php echo $this->Form->input('sale_tender_id', array('id' => 'tender_id', 'type' => 'hidden', 'label' => false)); ?>
	    		<?php echo $this->Form->input('sale_quotation_id', array('id' => 'quotation_id', 'type' => 'hidden', 'label' => false)); ?>
	    		<?php echo $this->Form->input('customer_id', array('id' => 'customer_id', 'type' => 'hidden', 'label' => false)); ?> 
	    		</div>
	    	</div>
        </div>  


        		<div class="form-group">
					<label class="col-sm-3">Contract No</label>
					<div class="col-sm-9">
				<?php echo $this->Form->input('contract_no', array('class' => 'form-control', 'label' => false)); ?>
				</div>
				</div> 
 
				<?php echo $this->Form->input('name', array('id' => 'name', 'type' => 'hidden', 'class' => 'form-control', 'label' => false)); ?>
				 
				
				<div class="form-group">
					<label class="col-sm-3">Status</label>
					<div class="col-sm-9">
					<?php $status = array(0 => 'Draft', 4 => 'Active', 5 => 'Cancel'); ?>
					<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
					</div>
				</div> 

				<div class="form-group">
					<div class="col-sm-12"><h4>Stations</h4></div>
				</div>
		<?php 
		$i = 1;
		foreach ($stations as $station) { ?> 
			<div class="form-group" id="removeBom<?php echo $i; ?>">
       		

       		<div class="col-sm-2">
	        <input type="text" value="<?php echo $station['SaleJobChild']['station_name']; ?>" name="data[SaleJobChild][<?php echo $i; ?>][station_name]" id="station<?php echo $i; ?>" class="form-control"disabled>
	        </div>

	        <div class="col-sm-3">
	        <input type="hidden" value="<?php echo $station['SaleJobChild']['id']; ?>" name="data[SaleJobChild][<?php echo $i; ?>][id]">

	        <input type="text" value="<?php echo $station['SaleJobChild']['name']; ?>" name="data[SaleJobChild][<?php echo $i; ?>][name]" class="form-control" placeholder="Enter Job Name"required>
	        </div>

	        <div class="col-sm-2">
	        <input type="text" value="<?php echo $station['SaleJobChild']['old_job_no']; ?>" name="data[SaleJobChild][<?php echo $i; ?>][old_job_no]" id="fat<?php echo $i; ?>" class="form-control" placeholder="Job Ref">
	        </div>


	         

	        <div class="col-sm-4">
	        <textarea name="data[SaleJobChild][<?php echo $i; ?>][description]" id="desc<?php echo $i; ?>" class="form-control" placeholder="Remark"><?php echo $station['SaleJobChild']['description']; ?></textarea>
	        </div>

	        <div class="col-sm-1">
	        <a href="#" class="btn btn-danger" onclick="removeBom(<?php echo $i; ?>); return false"><i class="fa fa-times"></i></a>
	        </div>
	        </div>
        <?php $i++; } ?>
				<div id="output"></div>

				<div class="form-group"><div id="error"></div></div>

				<!--<div class="form-group"> 
		            <div class="col-sm-12">
		                <a href="#" id="addMore" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Stations</a>
		            </div>
		        </div>-->

				<div class="form-group">
				<label class="col-sm-3">&nbsp;</label>
				<div class="col-sm-9">
					<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
				</div>
			</div> 
			 
		<?php echo $this->Form->end(); ?>
		</div>
		 
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">


    function findBomByCategory(row) {    
        var category_id = $('#bomCategory'+row).val();
        $.ajax({ 
            type: "GET", 
            dataType: 'json',
            data: 'category_id=' + $('#bomCategory'+row).val(),
            url: baseUrl + 'boms/ajaxfindbombycategoryid', 
            success: function(respond) { 
                var option = '<option value="">Select BOM</option>'; 
                $.each(respond, function(i, item) {
                    console.log(90); 
                    option += '<option value="' + item.id + '">' + item.name + '</option>';
                }); 
                $('#moreBomId'+row).html(option);
            }
        }); 
        return false; 
    } 

    function removeBom(id) {
        $('#removeBom'+id).html('');
        return false;
    }

    function selectDate(row) {
	 	$('#datepicker'+row).datepicker({
	      dateFormat: 'yy-mm-dd', 
	      ampm: true
	    }); 
    }

    function selectDateTwo(row) {
	 	$('#datepicker2'+row).datepicker({
	      dateFormat: 'yy-mm-dd', 
	      ampm: true
	    }); 
    }
$(document).on('focus', '.datepicker', function() {
    $(this).datepicker({
  dateFormat: 'yy-mm-dd', 
  ampm: true
});
});

$(document).on('focus', '.datepicker2', function() {
    $(this).datepicker({
  dateFormat: 'yy-mm-dd', 
  ampm: true
});
});

$(document).ready(function() { 
	
    var row = <?php echo $i; ?>;
    $('#addMore').click(function() { 
    	var mainCode = $('#name').val();
    	//if(mainCode == '') {
		//	$('#error').html('<p style="color: red">Please fill Job Number before add Sub Station.</p>'); 
		//} else {
			$('#error').html('');
			var html = '<div class="form-group" id="removeBom'+row+'">'; 
	       
	        html += '<div class="col-sm-5">';
	        html += '<input type="text" name="data[SaleJobChild]['+row+'][name]" class="form-control" placeholder="Enter Job Name"required>'; 
	        html += '</div>';
	        html += '<div class="col-sm-2">';
	        html += '<input type="text" name="data[SaleJobChild]['+row+'][fat_date]" id="fat'+row+'" class="datepicker form-control" placeholder="FAT Date"required>';
	        html += '</div>';

	        html += '<div class="col-sm-2">';
	        html += '<input type="text" name="data[SaleJobChild]['+row+'][delivery_date]" id="del'+row+'" class="datepicker2 form-control" placeholder="Delivery Date"required>';
	        html += '</div>';

	        html += '<div class="col-sm-2">';
	        html += '<textarea name="data[SaleJobChild]['+row+'][description]" class="form-control" placeholder="Remark"></textarea>';
	        html += '</div>';

	        html += '<div class="col-sm-1">';
	        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+row+'); return false"><i class="fa fa-times"></i></a>';
	        html += '</div>';
	        html += '</div>';     
	        row++; 

 
	        $("#output").append(html); 	   
		//}
        
        return false;
    });  
  	


    $("#find_quotation").autocomplete({
	    source: function (request, response) {
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_quotations/ajaxfindquotation',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#find_quotation').val(),                                                    
	            success: function (data) {
	            	console.log(data); 
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.name,
		                    customer: item.customer,
		                    tender_name: item.tender_name,
		                    tender_no: item.tender_no,
		                    tender_id: item.tender_id,
		                    customer_id: item.customer_id
		                }
		            }));
	        	}
	        });
	    },
	    select: function (event, ui) {
	        $("#quotation_id").val(ui.item.id);//Put Id in a hidden field
	        $("#customer_id").val(ui.item.customer_id);
	        $("#tender_id").val(ui.item.tender_id);
	    },
	    minLength: 3

	}).autocomplete( "instance" )._renderItem = function( ul, item ) {
      	return $( "<li>" ).append( "<div>" + item.customer + "<br>" + item.tender_no + "<br>" +  "</div>" ).appendTo( ul );
    };

});  
</script>
<?php $this->end(); ?>