<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Job No: <?php echo $salejob['SaleJob']['name']; ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content">  
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('ProjectBom', array('class'=>'form-horizontal')); ?> 
        
        <?php foreach ($stations as $station) { ?> 
            <div class="form-group">
            	<label class="col-sm-7"><?php echo $station['SaleJobChild']['station_name']; ?> </label>
            	<div class="col-sm-5">
            		<?php echo $station['SaleJobChild']['description']; ?> 
                    <?php 
                     echo $this->Form->input('sale_job_child_id.', array('type' => 'hidden', 'value' => $station['SaleJobChild']['id'], 'label' => false)); 
                    ?> 
            	</div>
            </div>  
            <div id="loadMoreBom-<?php echo $station['SaleJobChild']['id']; ?>"></div>  
            <div class="form-group">
                <div class="col-sm-12"><a href="#" onclick="addBom(<?php echo $station['SaleJobChild']['id']; ?>); return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Item</a></div>
            </div>  

        <?php } ?>    

        <div class="form-group"> 
        	<?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success')); ?> 
        </div>	  
		<?php $this->Form->end(); ?>
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'inventory_items/ajaxfindbycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.id + '">' + item.name + '</option>';
            }); 
            $('#loadItemId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function findBomByItemId(id, row) {
    var item_id = $('#loadItemId'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'inventory_item_id=' + $('#loadItemId'+id+row).val(),
        url: baseUrl + 'inventory_items/ajaxfindbombyitem', 
        success: function(json) {  
            if(json.status === false) { 
                var html = '<div class="alert alert-danger">' + json.message + '</div>';
                $('#removeBom'+id+row).append(html);
            } else {
                $('#isBom'+id+row).val(json.is_bom);
                $('#bomId'+id+row).val(json.bom_id);
            }
            console.log(json);
        }
    }); 
    return false; 
}

function removeBom(id, row) {
    $('#removeBom'+id+row).html('');
    return false;
}
 
var station = 1;
function addBom(id) {
    var html = '<div id="removeBom'+id+station+'">'; 
    html += '<div class="form-group">'; 
    html += '<div class="col-sm-3">';
    html += '<input type="hidden" name="data[ProjectBom][sale_job_child_id_ss][]" value="'+id+'">';
    html += '<select name="data[ProjectBom][category_id]['+id+'][]" id="bomCategory'+id+station+'" class="form-control" onchange="findBomByCategory('+id+', '+station+'); return false">';
    
    html += '<option value="">--Select Category--</option>';
    html += '<?php foreach($items as $item) { ?><option value="<?php echo $item['InventoryItemCategory']['id']; ?>"><?php echo $item['InventoryItemCategory']['name']; ?></option><?php } ?>';

    html += '</select></div><div class="col-sm-4">';
    html += '<select name="data[ProjectBom][item_id]['+id+'][]" id="loadItemId'+id+station+'" class="form-control" onchange="findBomByItemId('+id+', '+station+'); return false"required><option value="">Select Item</option></select>';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<input type="text" name="data[ProjectBom][quantity]['+id+'][]" class="form-control" placeholder="Qty"required>';
    html += '</div>';
    html += '<div class="col-sm-2">';
    html += '<select name="data[ProjectBom][general_unit_id]['+id+'][]" class="form-control"required>';

    html += '<option value="">Unit</option>';
    html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
    html += '</select>';

    html += '<input type="hidden" name="data[ProjectBom][is_bom]['+id+'][]" id="isBom'+id+station+'">'; 
    html += '<input type="hidden" name="data[ProjectBom][bom_id]['+id+'][]" id="bomId'+id+station+'">'; 

    html += '<input type="hidden" name="data[ProjectBom][unit_price]['+id+'][]" value="0">'; 
    html += '<input type="hidden" name="data[ProjectBom][discount]['+id+'][]" value="0">';  
    html += '<input type="hidden" name="data[ProjectBom][tax]['+id+'][]" value="0">';
    
    html += '</div>';
    

    html += '<div class="col-sm-1">';
    html += '<a href="#" class="btn btn-danger" onclick="removeBom('+id+', '+station+'); return false"><i class="fa fa-times"></i></a>';
    html += '</div>';
    html += '</div></div>';   
    station++;
    $("#loadMoreBom-"+id).append(html); 
    return false;
}

$(document).ready(function() {
    var row = 1;
    $('#addMore').click(function() { 
       
    }); 


    $("#findTender").autocomplete({
	    source: function (request, response){
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'sale_tenders/ajaxfindtender',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#findTender').val(),                                                    
	            success: function (data) {
	            	console.log(data);
	            response($.map(data, function (item) {
	                return {
	                    id: item.id,
	                    value: item.title
	                }
	            }))
	        }
	        });
	    },
	    select: function (event, ui) {
	        $("#tenderId").val(ui.item.id);//Put Id in a hidden field
	    },
	    minLength: 3
	});

});
</script>
<?php $this->end(); ?>