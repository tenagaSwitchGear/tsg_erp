<div class="saleJobs form">
<?php echo $this->Form->create('SaleJob'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sale Job'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('sale_quotation_id');
		echo $this->Form->input('sale_order_id');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('customer_station_id');
		echo $this->Form->input('contract_no');
		echo $this->Form->input('remark');
		echo $this->Form->input('status');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SaleJob.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SaleJob.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Jobs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Stations'), array('controller' => 'customer_stations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Station'), array('controller' => 'customer_stations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Job Children'), array('controller' => 'sale_job_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job Child'), array('controller' => 'sale_job_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
