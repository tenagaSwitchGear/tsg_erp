<?php echo $this->Html->link(__('List Jobs'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 		<div class="saleJobs form">
		<?php echo $this->Form->create('SaleJob', array('class' => 'form-horizontal form-label-left')); ?> 

		<div class="form-group">
			<label class="col-sm-3">Select Quotation</label>
			<div class="col-sm-9">
			<?php echo $this->Form->input('sale_quotation_id', array('class' => 'form-control', 'label' => false)); ?>
			</div>
		</div> 
		<div class="form-group">
			<label class="col-sm-3">Customer PO No</label>
			<div class="col-sm-9">
		<?php echo $this->Form->input('contract_no', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">Job No (Unique)</label>
			<div class="col-sm-9">
		<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-3">Status</label>
			<div class="col-sm-9">
			<?php $status = array(0 => 'Save As Draft', 1 => 'Submit For Verification'); ?>
			<?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?> 
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3">Remark</label>
			<div class="col-sm-9">
		<?php echo $this->Form->input('remark', array('type' => 'textarea', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3">&nbsp;</label>
			<div class="col-sm-9"> 
			<?php echo $this->Form->input('customer_id', array('type' => 'hidden')); ?> 
			<?php echo $this->Form->input('user_id', array('type' => 'hidden')); ?>  
			<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
		</div>
		</div> 

		<?php echo $this->Form->end(); ?>
		</div>
      
      </div>
    </div>
  </div> 
</div>
 