<?php
	function status($status) {
		if($status == 0) {
			return 'Draft';
		} else if($status == 1) {
			return 'Waiting HOS Verification';
		} else if($status == 2) {
			return 'Waiting HOD Verification';
		} else if($status == 3) {
			return 'Waiting GM Approval';
		} else if($status == 4) {
			return 'Active';
		} else if($status == 5) {
			return 'Cancelled';
		}
	}
?>
<?php echo $this->Html->link(__('Job Lists'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>

<?php echo $this->Html->link(__('Add New Job'), array('action' => 'add', $saleJob['SaleJob']['id']), array('class' => 'btn btn-success')); ?>

<?php echo $this->Html->link(__('Edit Job'), array('action' => 'edit', $saleJob['SaleJob']['id']), array('class' => 'btn btn-warning')); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo h($saleJob['SaleJob']['name']); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="saleJobs view-data">
<h2><?php echo __('View Job'); ?></h2>
	<dl>
		 

		<dt><?php echo __('Job No'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['name']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Customer PO No'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['contract_no']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Quotation'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleQuotation']['name']); ?>
			&nbsp; 
		</dd> 
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleJob['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $saleJob['Customer']['id'])); ?>
			&nbsp;
		</dd> 
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['modified']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($saleJob['SaleJob']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo status($saleJob['SaleJob']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleJob['User']['username'], array('controller' => 'users', 'action' => 'view', $saleJob['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div> 
 
<div class="related">
	<h3><?php echo __('Stations'); ?></h3>
	<?php if (!empty($childs)): ?>
	
	<?php $i = 1; ?>
	<?php foreach ($childs as $station): ?>
		<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('No'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Name'); ?></th>  
		<th><?php echo __('Ref'); ?></th> 
		<th><?php echo __('FAT Date'); ?></th>
		<th><?php echo __('Delivery Date'); ?></th>
		<th>Actions</th> 
	</tr>
		<tr>
			<td><?php echo $i; ?></td>
			<td><?php echo h($station['SaleJobChild']['station_name']); ?></td>  
			<td><?php echo h($station['SaleJobChild']['name']); ?></td>   
			<td><?php echo h($station['SaleJobChild']['old_job_no']); ?></td>  
			<td><?php echo $station['SaleJobChild']['fat_date']; ?></td>  
			<td><?php echo $station['SaleJobChild']['delivery_date']; ?></td> 
			<td><?php echo $this->Html->link('Edit', array('controller' => 'sale_job_childs', 'action' => 'edit', $station['SaleJobChild']['id']), array('class' => 'btn btn-warning')); ?></td> 
		</tr>

		<tr>

			<td colspan="7">  
				<table cellpadding = "0" cellspacing = "0" class="table">
				<tr>
					<th><?php echo __('No'); ?></th>
					<th><?php echo __('Name'); ?></th>
					<th><?php echo __('Code'); ?></th> 
					<th><?php echo __('Qty'); ?></th> 
					 
				</tr>

				<?php  
				$c = 1;
				foreach ($station['SaleJobItem'] as $item) {  ?>
		<tr>
			<td><?php echo $c; ?></td>
			<td><?php echo h($item['InventoryItem']['name']); ?></td>  
			<td><?php echo h($item['InventoryItem']['code']); ?></td>  
			<td><?php echo $item['quantity']; ?></td>  
			 
		</tr>
		<?php $c++; } ?>
		</table>
			</td> 

		
		</tr>
</table>
	<?php 
	$i++; 

	endforeach; ?>
	
<?php endif; ?>

	 
</div>


	</div>
    </div>
  </div> 
</div>










