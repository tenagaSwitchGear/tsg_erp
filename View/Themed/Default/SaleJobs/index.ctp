
<?php
	function status($status) {
		if($status == 0) {
			return 'Draft';
		} else if($status == 1) {
			return 'Waiting HOS Verification';
		} else if($status == 2) {
			return 'Waiting HOD Verification';
		} else if($status == 3) {
			return 'Waiting GM Approval';
		} else if($status == 4) {
			return 'Verified';
		} else if($status == 5) {
			return 'Rejected';
		}
	}
?>
<?php echo $this->Html->link(__('Active Job'), array('action' => 'index'), array('class' => 'btn btn-success')); ?>
<?php echo $this->Html->link(__('Draft Job'), array('action' => 'index', 0), array('class' => 'btn btn-default')); ?> 
<?php echo $this->Html->link(__('Cancelled Job'), array('action' => 'index', 5), array('class' => 'btn btn-danger')); ?> 
<?php echo $this->Html->link(__('Add New Job'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>


<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Jobs</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

         <?php echo $this->Form->create('SaleJob', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Job No', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td>
		<td><?php echo $this->Form->input('customer_id', array('type' => 'text', 'placeholder' => 'Customer', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('from', array('placeholder' => 'From', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('to', array('placeholder' => 'To', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 

<div class="saleOrders table-responsive"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr>
			
			<th><?php echo $this->Paginator->sort('name', 'Job No'); ?></th>
			<th><?php echo $this->Paginator->sort('Customer.name', 'Customer'); ?></th>
			<th><?php echo $this->Paginator->sort('contract_no'); ?></th> 

			<th>Station</th>  
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleJobs as $job): ?>
	<tr>
		
		<td><?php echo h($job['SaleJob']['name']); ?>&nbsp;</td>
		<td><?php echo h($job['Customer']['name']); ?>&nbsp;</td>
		<td><?php echo h($job['SaleJob']['contract_no']); ?>&nbsp;</td>   
		<td><?php echo count($job['SaleJobChild']); ?>&nbsp;</td>  
		 
		<td class="actions">
			<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $job['SaleJob']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $job['SaleJob']['id']), array('class' => 'btn btn-warning btn-circle-sm', 'escape'=>false)); ?> 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
      
      </div>
    </div>
  </div> 
</div>


