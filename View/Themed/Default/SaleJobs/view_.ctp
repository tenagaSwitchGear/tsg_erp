<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Tender: <?php echo $this->Html->link($saleOrder['SaleTender']['title'], array('controller' => 'sale_tenders', 'action' => 'view', $saleOrder['SaleTender']['id'])); ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 
      <div class="view-data">
<h2><?php echo __('Sale Order'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt> 
		<dd>
			<?php echo h($saleOrder['SaleOrder']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Tender'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrder['SaleTender']['title'], array('controller' => 'sale_tenders', 'action' => 'view', $saleOrder['SaleTender']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Quotation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrder['SaleQuotation']['id'], array('controller' => 'sale_quotations', 'action' => 'view', $saleOrder['SaleQuotation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Currency'); ?></dt> 
		<dd>
			<?php echo $this->Html->link($saleOrder['GeneralCurrency']['name'], array('controller' => 'general_currencies', 'action' => 'view', $saleOrder['GeneralCurrency']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Purchase Order No'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['customer_purchase_order_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer File'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrder['CustomerFile']['name'], array('controller' => 'customer_files', 'action' => 'view', $saleOrder['CustomerFile']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Order Pricing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrder['SaleOrderPricing']['name'], array('controller' => 'sale_order_pricings', 'action' => 'view', $saleOrder['SaleOrderPricing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Fat'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['date_fat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Delivery'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['date_delivery']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleOrder['User']['id'], array('controller' => 'users', 'action' => 'view', $saleOrder['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($saleOrder['SaleOrder']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="saleOrders">
	<h2>Stations</h2>
	<table class="table">
	<thead>
	<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Description</th> 
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleOrder['SaleOrderChild'] as $child): ?>
	<tr>
		<td><?php echo h($child['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($child['name'], array('controller' => 'saleorderchilds', 'action' => 'view', $child['id'])); ?>
		</td>
		<td>
			<?php echo $child['description']; ?>
		</td> 
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
 
</div>
<!--
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Order'), array('action' => 'edit', $saleOrder['SaleOrder']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Order'), array('action' => 'delete', $saleOrder['SaleOrder']['id']), array(), __('Are you sure you want to delete # %s?', $saleOrder['SaleOrder']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Currencies'), array('controller' => 'general_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Currency'), array('controller' => 'general_currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Files'), array('controller' => 'customer_files', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer File'), array('controller' => 'customer_files', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Pricings'), array('controller' => 'sale_order_pricings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Pricing'), array('controller' => 'sale_order_pricings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Jobs'), array('controller' => 'sale_jobs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job'), array('controller' => 'sale_jobs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Jobs'); ?></h3>
	<?php if (!empty($saleOrder['SaleJob'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Quotation Id'); ?></th>
		<th><?php echo __('Sale Order Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Customer Station Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Contract No'); ?></th>
		<th><?php echo __('Remark'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($saleOrder['SaleJob'] as $saleJob): ?>
		<tr>
			<td><?php echo $saleJob['id']; ?></td>
			<td><?php echo $saleJob['sale_quotation_id']; ?></td>
			<td><?php echo $saleJob['sale_order_id']; ?></td>
			<td><?php echo $saleJob['customer_id']; ?></td>
			<td><?php echo $saleJob['customer_station_id']; ?></td>
			<td><?php echo $saleJob['created']; ?></td>
			<td><?php echo $saleJob['modified']; ?></td>
			<td><?php echo $saleJob['contract_no']; ?></td>
			<td><?php echo $saleJob['remark']; ?></td>
			<td><?php echo $saleJob['status']; ?></td>
			<td><?php echo $saleJob['user_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_jobs', 'action' => 'view', $saleJob['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_jobs', 'action' => 'edit', $saleJob['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_jobs', 'action' => 'delete', $saleJob['id']), array(), __('Are you sure you want to delete # %s?', $saleJob['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Job'), array('controller' => 'sale_jobs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>-->
      </div>
    </div>
  </div> 
</div>


