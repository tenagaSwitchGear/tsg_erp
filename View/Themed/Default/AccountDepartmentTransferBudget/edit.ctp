<div class="row"> 
<<<<<<< HEAD
    <div class="col-xs-12">
        <?php echo $this->Html->link(__('New Budgets'), array('controller' => 'accountdepartmentbudgets', 'action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('List Department Account'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2>Department Account</h2> 
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>
            <!-- content start-->
            
            <div class="container table-responsive">
                <table class="table table-hover table-bordered">
                    <tr>
                        <td colspan="1"><b>Id</b></td>
                        <td colspan="3">#<?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['id']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Date Request</b></td>
                        <td colspan="3"><?php echo date('Y-m-d', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['created'])); ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <b>Transfer To</b>
                        </td>
                        <td colspan="2" class="text-center">
                            <b>Transfer From</b>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2"><b>Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['GroupsTo']['name']); ?></td>
                        <td class="col-md-2"><b>Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['GroupFrom']['name']); ?></td>
                    </tr>
                    <tr>
                        <td class="col-md-2"><b>Budget Allocation Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['AccountDepartmentBudgetTo']['name']); ?></td>
                        <td class="col-md-2"><b>Budget Allocation Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['AccountDepartmentBudgetFrom']['name']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Amount Request</b></td>
                        <td colspan="3"><?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['amount']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Amount Revised</b></td>
                        <td colspan="3"><?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['revised_budget']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Requested By</b></td>
                        <td colspan="1"><?php echo $accountDepartmentTransferBudget['Request_by']['firstname'].' '.$accountDepartmentTransferBudget['Request_by']['lastname']; ?></td>
                        <td colspan="1"><b>Date</b></td>
                        <td colspan="1"><?php echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['created'])); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Reviewed By</b></td>
                        <?php  
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){
                            ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <td colspan="1">
                            <?php
                                echo $accountDepartmentTransferBudget['Review_by']['firstname'].' '.$accountDepartmentTransferBudget['Review_by']['lastname']; 
                            ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['review_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['review_date'])); }?></td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Verified By</b></td>
                        <?php  
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){
                            ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Verify_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Finance verification</td>
                            <?php }else{ ?> 
                            <td colspan="1"><?php echo $accountDepartmentTransferBudget['Verify_by']['firstname'].' '.$accountDepartmentTransferBudget['Verify_by']['lastname']; ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['verify_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['verify_date'])); }?></td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Recommended By</b></td>
                        <?php 
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){
                            ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Verify_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Finance verification</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Recommended_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Department recommended</td>
                            <?php }else{ ?>
                            <td colspan="1"><?php echo $accountDepartmentTransferBudget['Recommended_by']['firstname'].' '.$accountDepartmentTransferBudget['Recommended_by']['lastname']; ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['recommended_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['recommended_date'])); }?></td>
                            <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Approved By</b></td>
                        <?php
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){ ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Verify_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Finance verification</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Recommended_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Department recommended</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Approved_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Subsidiary approval</td>
                            <?php }else{ ?>
                            <td colspan="1"><?php echo $accountDepartmentTransferBudget['Approved_by']['firstname'].' '.$accountDepartmentTransferBudget['Approved_by']['lastname']; ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['approval_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['approval_date'])); }?></td>
                            <?php } ?>
                            <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <?php echo $this->Form->create('AccountDepartmentTransferBudget'); ?>
                    <input type="hidden" name="id" value="<?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['id']); ?>">
                    <input type="hidden" name="created" value="<?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['created']); ?>">
                    <tr>
                        <td colspan="1"><b>Action</b></td>
                        <?php if($role == '2'){ ?>
                        <td colspan="3"><select class="form-control" name="actions">
                            <option value="0">Select Action</option>
                            <option value="1">Review</option>
                        </select></td>
                        <?php }else if($role == '3'){ ?>
                        <td colspan="3"><select class="form-control" name="actions" <?php if(empty($accountDepartmentTransferBudget['Review_by']['id'])){ echo "disabled='disabled'"; } ?>>
                            <option value="0">Select Action</option>
                            <option value="2">Verified</option>
                        </select></td>
                        <?php }else if($role == '4'){ ?>
                        <td colspan="3"><select class="form-control" name="actions" <?php if(empty($accountDepartmentTransferBudget['Verify_by']['id'])){ echo "disabled='disabled'"; } ?>>
                            <option value="0">Select Action</option>
                            <option value="3">Recommended</option>
                        </select></td>
                        <?php }else if($role == '5'){ ?>
                        <td colspan="3"><select class="form-control" name="actions" <?php if(empty($accountDepartmentTransferBudget['Recommended_by']['id'])){ echo "disabled='disabled'"; } ?>>
                            <option value="0">Select Action</option>
                            <option value="4">Approved</option>
                            <option value="5">Rejected</option>
                        </select></td>
                        <?php } ?>
                    </tr>
                </table>
            </div>
            <?php 
            echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm'));
            echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
            ?>
            <?php echo $this->Form->end(); ?>
            <!-- content end -->
        </div>
    </div>
    </div>
</div>
=======
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Department Account Budget</h2> 
        		<div class="clearfix"></div>
      		</div>

      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
        		<!-- content start-->
        		<pre><?php print_r($existed_budget); ?></pre>	
                <?php if(!empty($existed_budget)){
                        $a = '';
                        for($i=0; $i<count($existed_budget); $i++){
                            $a = $a + $existed_budget[$i]['account_department_budgets']['total'];
                        }
                        $exist = $a;
                        $allocated = $acc_dept[0]['account_departments']['total'] - $exist;
                    }else{
                        $allocated = $acc_dept[0]['account_departments']['total'];
                        } ?>
        		<div class="accountDepartmentBudget form">
                    <h4>Budget Allocated :  <?php echo number_format($allocated, 4); ?> </h4>
				<?php echo $this->Form->create('AccountDepartmentBudgets', array('class' => 'form-horizontal')); ?>
					<fieldset>
                        <?php echo $this->Form->input("account_department_id", array("type"=>"hidden", "class"=> "form-control", "value"=>$acc_dept[0]['account_departments']['id'], "label"=>false)); ?>
                        <?php echo $this->Form->input("year", array("type"=>"hidden", "class"=> "form-control", "value"=>$acc_dept[0]['account_departments']['year'], "label"=>false)); ?>
                        <?php echo $this->Form->input("maxbudget", array("type"=>"hidden", "class"=> "form-control", "value"=>$allocated, "label"=>false)); ?>
                        <?php echo $this->Form->input("id", array("type"=>"hidden", "class"=> "form-control", "value"=>$this->request->data['AccountDepartmentBudgets']['id'], "label"=>false)); ?>
                        <?php echo $this->Form->input("old_total", array("type"=>"hidden", "class"=> "form-control", "value"=>$this->request->data['AccountDepartmentBudgets']['total'], "label"=> false)); ?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Name</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("name", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Total Budget Given</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("total", array("type"=>"number", "id"=>"totalbudget", "class"=> "form-control", "placeholder" => "Total", "label"=> false)); ?>
							</div> 
						</div>
					</fieldset>
					<div class="clearfix">&nbsp;</div>
					<div>
					<?php
						echo $this->Html->link(__('Back'), array('controller'=>'accountdepartment', 'action' => 'view', $acc_dept[0]['account_departments']['id']), array('class' => 'btn btn-warning btn-sm'));
						//echo $this->Form->button('Reset', array('type'=>'reset', 'class' => 'btn btn-danger btn-sm','div' => false));
						echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
					?>
					</div>
				</div>
        		<!-- content end -->
      		</div>
    	</div>
  	</div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
var d = new Date();
var n = d.getFullYear();
var m = n+9;
var x = '';
for(i=n; i<m; i++){
	x += '<option value='+i+'>';
	x += i;
	x += '</option>';
}
var x = x;

$('#yearpicker').html(x);
//alert(m);

</script>
<?php $this->end(); ?>
>>>>>>> Prakash
