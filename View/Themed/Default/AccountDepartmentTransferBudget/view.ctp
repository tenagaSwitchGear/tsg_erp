<div class="row"> 
    <div class="col-xs-12">
        <?php echo $this->Html->link(__('New Budgets'), array('controller' => 'accountdepartmentbudgets', 'action' => 'index'), array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo $this->Html->link(__('List Department Account'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <div class="x_panel tile">
            <div class="x_title">
                <h2>Department Account</h2> 
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <?php echo $this->Session->flash(); ?>
            <!-- content start-->
            
            <div class="container table-responsive">
                <table class="table table-hover table-bordered">
                    <tr>
                        <td colspan="1"><b>Id</b></td>
                        <td colspan="3">#<?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['id']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Date Request</b></td>
                        <td colspan="3"><?php echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['created'])); ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <b>Transfer To</b>
                        </td>
                        <td colspan="2" class="text-center">
                            <b>Transfer From</b>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-2"><b>Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['GroupsTo']['name']); ?></td>
                        <td class="col-md-2"><b>Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['GroupFrom']['name']); ?></td>
                    </tr>
                    <tr>
                        <td class="col-md-2"><b>Budget Allocation Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['AccountDepartmentBudgetTo']['name']); ?></td>
                        <td class="col-md-2"><b>Budget Allocation Department</b></td>
                        <td><?php echo ($accountDepartmentTransferBudget['AccountDepartmentBudgetFrom']['name']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Amount Request</b></td>
                        <td colspan="3"><?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['amount']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Amount Revised</b></td>
                        <td colspan="3"><?php echo ($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['revised_budget']); ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Requested By</b></td>
                        <td colspan="1"><?php echo $accountDepartmentTransferBudget['Request_by']['firstname'].' '.$accountDepartmentTransferBudget['Request_by']['lastname']; ?></td>
                        <td colspan="1"><b>Date</b></td>
                        <td colspan="1"><?php echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['created'])); ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Reviewed By</b></td>
                        <?php  
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){
                            ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <td colspan="1">
                            <?php
                                echo $accountDepartmentTransferBudget['Review_by']['firstname'].' '.$accountDepartmentTransferBudget['Review_by']['lastname']; 
                            ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['review_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['review_date'])); }?></td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Verified By</b></td>
                        <?php  
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){
                            ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Verify_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Finance verification</td>
                            <?php }else{ ?> 
                            <td colspan="1"><?php echo $accountDepartmentTransferBudget['Verify_by']['firstname'].' '.$accountDepartmentTransferBudget['Verify_by']['lastname']; ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['verify_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['verify_date'])); }?></td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Recommended By</b></td>
                        <?php 
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){
                            ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Verify_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Finance verification</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Recommended_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Department recommended</td>
                            <?php }else{ ?>
                            <td colspan="1"><?php echo $accountDepartmentTransferBudget['Recommended_by']['firstname'].' '.$accountDepartmentTransferBudget['Recommended_by']['lastname']; ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['recommended_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['recommended_date'])); }?></td>
                            <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td colspan="1"><b>Approved By</b></td>
                        <?php
                            if(empty($accountDepartmentTransferBudget['Review_by']['id'])){ ?>
                            <td colspan="3">Waiting for Finance Officer review</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Verify_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Finance verification</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Recommended_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Department recommended</td>
                            <?php }else{ ?>
                            <?php if(empty($accountDepartmentTransferBudget['Approved_by']['id'])){ ?>
                            <td colspan="3">Waiting for Head of Subsidiary approval</td>
                            <?php }else{ ?>
                            <td colspan="1"><?php echo $accountDepartmentTransferBudget['Approved_by']['firstname'].' '.$accountDepartmentTransferBudget['Approved_by']['lastname']; ?></td>
                            <td colspan="1"><b>Date</b></td>
                            <td colspan="1"><?php if($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['approval_date'] == '0000-00-00 00:00:00'){ echo ""; }else{ echo date('d-m-Y', strtotime($accountDepartmentTransferBudget['AccountDepartmentTransferBudget']['approval_date'])); }?></td>
                            <?php } ?>
                            <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                </table>
            </div>
            <?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm'));
            ?>
            <!-- content end -->
        </div>
    </div>
    </div>
</div>