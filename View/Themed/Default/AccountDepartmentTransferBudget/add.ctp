<style type="text/css">
    .text{
        padding-top: 8px
    }
</style>
<div class="row"> 
    <div class="col-xs-12">
        <div class="x_panel tile">
            <div class="x_title">
                <h2>New Transfer Budget Request</h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                <!-- content start-->
                <?php echo $this->Form->create('AccountDepartmentTransferBudget'); ?>
                <div class="accountDepartmentTransferBudget form">
                    <h4>Department/Section  : <?php echo $department[0]['groups']['name']; ?></h4>
                    <h4>Financial Year      : <?php echo date('Y'); ?></h4>
                    <p>&bull; Transfer Detail</p>
                    <table class="table">
                        <tr>
                            <td colspan="2"><label>Transfer to :</label></td>
                            <td>&nbsp;</td>
                            <td colspan="2"><label>Transfer from :</label></td>
                        </tr>
                        <tr>
                            <td>Department</td>
                            <td><select class="form-control" name="group_id_to" onchange="getallocatedbudget_a(this.value)">
                                <option>Select Department</option>
                                <?php foreach ($groups as $key => $group) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $group; ?></option>   
                                <?php } ?>
                            </select></td>
                            <td>&nbsp;</td>
                            <td>Department</td>
                            <td ><select class="form-control" name="group_id_from" onchange="getallocatedbudget_b(this.value)">
                                <option>Select Department</option>
                                <?php foreach ($groups as $key => $group) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $group; ?></option>   
                                <?php } ?>
                            </select></td>
                        </tr>
                        <tr>
                            <td>Budget</td>
                            <td id="output"><select class="form-control">
                                <option>Select Budget</option>
                            </select></td>
                            <td>&nbsp;</td>
                            <td>Budget</td>
                            <td id="output_2"><select class="form-control">
                                <option>Select Budget</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2"><label>Actual YTD&sup1; as at <?php echo date('d/m/Y'); ?> : <span id="output_2a">0</span></label></td>
                            <td>&nbsp;</td>
                            <td colspan="2"><label>Actual YTD&sup1; as at <?php echo date('d/m/Y'); ?> : <span id="output_2b">0</span></label></td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-md-2 text">Amount Request : </div>
                        <div class="col-md-4"><input type="text" name="amount" class="form-control"></div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-2 text">Revised Budget : </div>
                        <div class="col-md-4"><input type="text" name="revised" class="form-control"></div>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                    <?php 
                        echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
                    ?>
                </div>
                <!-- content end -->
            </div>
        </div>
    </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
function getallocatedbudget_a(nilai){
    //alert(nilai);
    var getParam = {
                acc_budget_id: nilai
            }
        $.ajax({ 
            type: "GET", 
            data: 'json',
            dataType: 'json',
            data: getParam,
            url: baseUrl + 'accountDepartmentTransferBudget/ajaxbudget', 
            success: function(data) { 
                //console.log(data);
                var html = '<select class="form-control" name="transfer_to_id" onchange="budget_detail_a(this.value)"><option value="0">Select Budget</option>';
                //var row = 0;
                $.each(data, function(i, item) { 
                    //$('#gu'+i+' option[value = "'+item.general_unit+'"]').attr("selected", "selected");
                    //console.log(item);
                    html += '<option value='+item.id+'>';
                    //html += '<div class="col-sm-1"><input type="checkbox" name="data[InventoryPurchaseRequisition][id][]" value="'+item.id+'"></div>';
                    html += item.name;
                    html += '</option>';
                    //row++;
                }); 
                html += "</select>";
                html = html;
                 
                $('#output').html(html);    
            },
            error: function(err){
                console.log(err);
            }
        }); 
        return false;
}

function getallocatedbudget_b(nilai){
    //alert(nilai);
    var getParam = {
                acc_budget_id: nilai
            }
        $.ajax({ 
            type: "GET", 
            data: 'json',
            dataType: 'json',
            data: getParam,
            url: baseUrl + 'accountDepartmentTransferBudget/ajaxbudget', 
            success: function(data) { 
                //console.log(data);
                var html = '<select class="form-control" name="transfer_from_id" onchange="budget_detail_b(this.value)"><option value="0">Select Budget</option>';
                //var row = 0;
                $.each(data, function(i, item) { 
                    //$('#gu'+i+' option[value = "'+item.general_unit+'"]').attr("selected", "selected");
                    //console.log(item);
                    html += '<option value='+item.id+'>';
                    //html += '<div class="col-sm-1"><input type="checkbox" name="data[InventoryPurchaseRequisition][id][]" value="'+item.id+'"></div>';
                    html += item.name;
                    html += '</option>';
                    //row++;
                }); 
                html += "</select>";
                html = html;
                 
                $('#output_2').html(html);    
            },
            error: function(err){
                console.log(err);
            }
        }); 
        return false;
}

function budget_detail_a(nilai){
    //alert(nilai);
    var getParam = {
                id: nilai
            }
        $.ajax({ 
            type: "GET", 
            data: 'json',
            dataType: 'json',
            data: getParam,
            url: baseUrl + 'accountDepartmentTransferBudget/ajaxbudgetdetail', 
            success: function(data) { 
                //console.log(data);
                 
                $('#output_2a').html('RM '+data);    
            },
            error: function(err){
                console.log(err);
            }
        }); 
        return false;
}

function budget_detail_b(nilai){
    //alert(nilai);
    var getParam = {
                id: nilai
            }
        $.ajax({ 
            type: "GET", 
            data: 'json',
            dataType: 'json',
            data: getParam,
            url: baseUrl + 'accountDepartmentTransferBudget/ajaxbudgetdetail', 
            success: function(data) { 
                //console.log(data);
                 
                $('#output_2b').html('RM '+data);    
            },
            error: function(err){
                console.log(err);
            }
        }); 
        return false;
}
</script>
<?php $this->end(); ?>