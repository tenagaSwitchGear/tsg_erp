<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Transfer Request'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Department Account'), array('controller' => 'accountdepartment','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Customer Files'), array('controller' => 'customer_files','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Sale Quotation'), array('controller' => 'sale_quotations','action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Capital & Operating Budget (CAPEX/OPEX) Transfer Budget</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
            
        	<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<th class="text-center" rowspan="2">#</th>
							<th colspan="2" class="text-center"><?php echo 'Budget'; ?></th>
							<th rowspan="2"><?php echo 'Amount Request'; ?></th>
							<th rowspan="2"><?php echo 'Amount Revised'; ?></th>
							<th rowspan="2"><?php echo 'Status'; ?></th>
							<th class="actions" rowspan="2"><?php echo __('Actions'); ?></th>
						</tr>
                        <tr>
                            <th class="col-md-2 text-center">Transfer From</th>
                            <th class="col-md-2 text-center">Transfer To</th>
                        </tr>
					</thead>
					<tbody>
						<?php 
							$currentPage = empty($this->Paginator->params['paging']['AccountDepartmentTransferBudget']['page']) ? 1 : $this->Paginator->params['paging']['AccountDepartmentTransferBudget']['page']; $limit = $this->Paginator->params['paging']['AccountDepartmentTransferBudget']['limit'];
							$startSN = (($currentPage * $limit) + 1) - $limit;
							foreach ($accountDepartmentTransferBudget as $transferbudget): ?>
						<tr>
							<td class="text-center"><?php echo $startSN++; ?></td>
							<td><?php echo '<label>Department : '.$transferbudget['GroupFrom']['name'].'</label><br> &bull; '.$transferbudget['AccountDepartmentBudgetFrom']['name']; ?></td>
                            <td><?php echo '<label>Department :'.$transferbudget['GroupsTo']['name'].'</label><br> &bull; '.$transferbudget['AccountDepartmentBudgetTo']['name'] ?></td>
							<td><?php echo $transferbudget['AccountDepartmentTransferBudget']['amount']; ?></td>
							<td><?php echo $transferbudget['AccountDepartmentTransferBudget']['revised_budget']; ?></td>
							<td><?php 
                            if($transferbudget['AccountDepartmentTransferBudget']['status'] == '0'){ 
                                $status = "Waiting for Finance Officer Review"; 
                            }else if($transferbudget['AccountDepartmentTransferBudget']['status'] == '1'){ 
                                $status = 'Waiting for Head of Finance Verification'; 
                            }else if($transferbudget['AccountDepartmentTransferBudget']['status'] == '2'){
                                $status = 'Waiting for Head of Department Recommended'; 
                            }else if($transferbudget['AccountDepartmentTransferBudget']['status'] == '3'){
                                $status = 'Waiting for Head of Subsidiary Approval'; 
                            }else if($transferbudget['AccountDepartmentTransferBudget']['status'] == '4'){
                                $status = 'Approved'; 
                            }else{
                                $status = 'Rejected';
                            }

                            echo $status; ?></td>
							<td class="actions">
								<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $transferbudget['AccountDepartmentTransferBudget']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $transferbudget['AccountDepartmentTransferBudget']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
								<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $transferbudget['AccountDepartmentTransferBudget']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "#'.h($transferbudget['AccountDepartmentTransferBudget']['id']).'"', $transferbudget['AccountDepartmentTransferBudget']['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p><?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?></p>
				<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
						echo $this->Paginator->numbers(array('separator' => ''), array('class' => 'btn btn-default btn-sm'));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
					?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>