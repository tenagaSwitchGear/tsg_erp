<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Supplier Item'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('Items General'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Price Book Required</h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
    <?php echo $this->Form->create('InventorySupplierItemRequired', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Item name', 'class' => 'form-control', 'required' => false, 'id' => 'find_item', 'label' => false)); ?>  
			 
		</td>
		<td><?php echo $this->Form->input('code', array('type' => 'text', 'placeholder' => 'Item code', 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td>   
		<td><?php echo $this->Form->input('inventory_item_category_id', array('options' => $category, 'class' => 'form-control', 'required' => false, 'label' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?>
	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('InventoryItem.code', 'Code'); ?></th>
			<th><?php echo $this->Paginator->sort('InventoryItem.name', 'Name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th> 
			<th>Add Price Book</th> 
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventorySupplierItemRequireds as $inventorySupplierItemRequired): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($inventorySupplierItemRequired['InventoryItem']['code'], array('controller' => 'inventory_items', 'action' => 'view', $inventorySupplierItemRequired['InventoryItem']['id'])); ?>
		</td> 
		<td>
			<?php echo $this->Html->link($inventorySupplierItemRequired['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventorySupplierItemRequired['InventoryItem']['id'])); ?>
		</td>
		<td><?php echo h($inventorySupplierItemRequired['InventorySupplierItemRequired']['created']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventorySupplierItemRequired['User']['username'], array('controller' => 'users', 'action' => 'view', $inventorySupplierItemRequired['User']['id'])); ?>
		</td>
		<td><?php echo status($inventorySupplierItemRequired['InventorySupplierItemRequired']['status']); ?>&nbsp;</td>
		<td><?php echo $this->Html->link('<i class="fa fa-plus"></i>', array('controller' => 'inventory_supplier_items', 'action' => 'add', $inventorySupplierItemRequired['InventoryItem']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false, 'target' => '_blank')); ?></td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 
</div>
</div> 
</div>

<?php
function status($no) {
	if($no == 0) {
		return '<span class="label label-warning">Waiting</span>';
	} else {
		return '<span class="label label-success">Added</span>';
	}
}