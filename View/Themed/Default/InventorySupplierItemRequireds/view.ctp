<div class="inventorySupplierItemRequireds view">
<h2><?php echo __('Inventory Supplier Item Required'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItemRequired['InventorySupplierItemRequired']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItemRequired['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventorySupplierItemRequired['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItemRequired['InventorySupplierItemRequired']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventorySupplierItemRequired['User']['id'], array('controller' => 'users', 'action' => 'view', $inventorySupplierItemRequired['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($inventorySupplierItemRequired['InventorySupplierItemRequired']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Supplier Item Required'), array('action' => 'edit', $inventorySupplierItemRequired['InventorySupplierItemRequired']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Supplier Item Required'), array('action' => 'delete', $inventorySupplierItemRequired['InventorySupplierItemRequired']['id']), array(), __('Are you sure you want to delete # %s?', $inventorySupplierItemRequired['InventorySupplierItemRequired']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Item Requireds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Supplier Item Required'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
