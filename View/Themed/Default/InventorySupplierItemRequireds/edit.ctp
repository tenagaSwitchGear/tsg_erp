<div class="inventorySupplierItemRequireds form">
<?php echo $this->Form->create('InventorySupplierItemRequired'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Supplier Item Required'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('inventory_item_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventorySupplierItemRequired.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventorySupplierItemRequired.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Supplier Item Requireds'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
