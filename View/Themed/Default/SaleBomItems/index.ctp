<div class="saleBomItems index">
	<h2><?php echo __('Sale Bom Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('bom_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_bom_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_bom_child_id_temp'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity_total'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('price_total'); ?></th>
			<th><?php echo $this->Paginator->sort('margin'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_order'); ?></th>
			<th><?php echo $this->Paginator->sort('margin_unit'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_quotation_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleBomItems as $saleBomItem): ?>
	<tr>
		<td><?php echo h($saleBomItem['SaleBomItem']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleBomItem['Bom']['name'], array('controller' => 'boms', 'action' => 'view', $saleBomItem['Bom']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleBomItem['SaleBom']['name'], array('controller' => 'sale_boms', 'action' => 'view', $saleBomItem['SaleBom']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($saleBomItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $saleBomItem['InventoryItem']['id'])); ?>
		</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['sale_bom_child_id_temp']); ?>&nbsp;</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['quantity']); ?>&nbsp;</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['quantity_total']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleBomItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleBomItem['GeneralUnit']['id'])); ?>
		</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['price']); ?>&nbsp;</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['price_total']); ?>&nbsp;</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['margin']); ?>&nbsp;</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['no_of_order']); ?>&nbsp;</td>
		<td><?php echo h($saleBomItem['SaleBomItem']['margin_unit']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleBomItem['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $saleBomItem['SaleQuotation']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleBomItem['SaleBomItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleBomItem['SaleBomItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleBomItem['SaleBomItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleBomItem['SaleBomItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Boms'), array('controller' => 'boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom'), array('controller' => 'boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
	</ul>
</div>
