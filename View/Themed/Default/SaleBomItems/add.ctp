<div class="saleBomItems form">
<?php echo $this->Form->create('SaleBomItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Sale Bom Item'); ?></legend>
	<?php
		echo $this->Form->input('bom_id');
		echo $this->Form->input('sale_bom_id');
		echo $this->Form->input('inventory_item_id');
		echo $this->Form->input('sale_bom_child_id_temp');
		echo $this->Form->input('quantity');
		echo $this->Form->input('quantity_total');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('price');
		echo $this->Form->input('price_total');
		echo $this->Form->input('margin');
		echo $this->Form->input('no_of_order');
		echo $this->Form->input('margin_unit');
		echo $this->Form->input('sale_quotation_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sale Bom Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Boms'), array('controller' => 'boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom'), array('controller' => 'boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
	</ul>
</div>
