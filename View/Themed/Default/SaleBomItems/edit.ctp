<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit Item</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?> 

<div class="saleBomItems form">
<?php echo $this->Form->create('SaleBomItem'); ?> 
	<?php echo $this->Form->input('id'); ?>
  <?php echo $this->Form->input('InventoryItem.code', array('class' => 'form-control'));  ?>
	<?php echo $this->Form->input('inventory_item_id', array('class' => 'form-control'));  ?>
	<?php echo $this->Form->input('quantity', array('class' => 'form-control')); ?>
	<?php echo $this->Form->input('quantity_total', array('class' => 'form-control')); ?>
	<?php echo $this->Form->input('general_unit_id', array('class' => 'form-control')); ?>
	<?php echo $this->Form->input('price', array('class' => 'form-control')); ?>
 
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-success')); ?> 
 
</div>
</div>
</div>
</div>