<div class="saleBomItems view">
<h2><?php echo __('Sale Bom Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bom'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleBomItem['Bom']['name'], array('controller' => 'boms', 'action' => 'view', $saleBomItem['Bom']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Bom'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleBomItem['SaleBom']['name'], array('controller' => 'sale_boms', 'action' => 'view', $saleBomItem['SaleBom']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleBomItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $saleBomItem['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Bom Child Id Temp'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['sale_bom_child_id_temp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity Total'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['quantity_total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleBomItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $saleBomItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Total'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['price_total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Margin'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['margin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Of Order'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['no_of_order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Margin Unit'); ?></dt>
		<dd>
			<?php echo h($saleBomItem['SaleBomItem']['margin_unit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Quotation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleBomItem['SaleQuotation']['name'], array('controller' => 'sale_quotations', 'action' => 'view', $saleBomItem['SaleQuotation']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Bom Item'), array('action' => 'edit', $saleBomItem['SaleBomItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Bom Item'), array('action' => 'delete', $saleBomItem['SaleBomItem']['id']), array(), __('Are you sure you want to delete # %s?', $saleBomItem['SaleBomItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Bom Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Boms'), array('controller' => 'boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bom'), array('controller' => 'boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Boms'), array('controller' => 'sale_boms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Bom'), array('controller' => 'sale_boms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotations'), array('controller' => 'sale_quotations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation'), array('controller' => 'sale_quotations', 'action' => 'add')); ?> </li>
	</ul>
</div>
