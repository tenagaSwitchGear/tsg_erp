<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Edit Internal Lofa'); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content">
			<div class="internalLofas form">
			<?php echo $this->Form->create('InternalLofa', array('class'=>'form-horizontal')); ?>
				<fieldset>
				<?php echo $this->Form->input('id'); ?>
				<?php echo $this->Form->input('price_min', array('class' => 'form-control')); ?>
				<?php echo $this->Form->input('price_max', array('class' => 'form-control')); ?>
				<?php $role = array('HOD' => 'HOD', 'MD' => 'MD'); ?>
				<?php echo $this->Form->input('role', array('options' => $role, 'empty' => '-Select Role-', 'class' => 'form-control')); ?>
				<div class="clearfix">&nbsp;</div>
				</fieldset>
			<?php echo $this->Form->end(__('Submit')); ?>
			</div>
		</div>
	</div>
</div>
</div>

