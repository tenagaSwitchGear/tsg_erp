<div class="row"> 
    <div class="col-xs-12">
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Add Internal Lofa'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content"> 
                <?php echo $this->Session->flash(); ?>
                <!-- content start-->
                <div class="internalLofas form">
                <?php echo $this->Form->create('InternalLofa' , array('class'=>'form-horizontal')); ?>
                    <fieldset>
                    <?php $category = array("CAPEX" => "CAPEX", "OPEX" => "OPEX", "SERVING_CLIENT" => "SERVING CLIENTS/CUSTOMERS"); ?>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Category</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("category", array("empty"=>array('0'=>'- SELECT CATEGORY -'), "options"=>$category, "class"=> "form-control", "label"=> false, 'required'=>false)); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Min Price</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("price_min", array("placeholder"=>"Min Price", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false)); ?>
                        </div> 
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Max Price</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("price_max", array("placeholder"=>"Max Price", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false)); ?>
                        </div> 
                    </div> 

                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Valid From (Temporary MD/HOD/HOS for PR)</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("from", array("placeholder"=>"DD-MM-YYYY", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false, "id" => "dateonly")); ?>
                        </div> 
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Valid To (Temporary MD/HOD/HOS for PR)</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("to", array("placeholder"=>"DD-MM-YYYY", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false, "id" => "dateonly_2")); ?>
                        </div> 
                    </div> 

                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Type (For PR Only)</label>
                        <div class="col-sm-9">
                        <?php $type = array(
                            0 => 'Permanent', 
                            1 => 'Temporary'
                            );
                            ?>
                        <?php echo $this->Form->input("type", array("options"=>"DD-MM-YYYY", "type"=>"text", "class"=> "form-control", "label"=> false, 'required'=>false, "id" => "dateonly_2")); ?>
                        </div> 
                    </div>

                    <?php $role = array('HOS' => 'HOS', 'HOD' => 'HOD', 'MD' => 'MD'); ?>
                    <div class="form-group">
                        <label class="col-sm-3" style="padding-top: 8px">Role</label>
                        <div class="col-sm-9">
                        <?php echo $this->Form->input("role", array("empty"=>array('0'=>'- SELECT ROLE -'), "class"=> "form-control", "label"=> false, 'required'=>false, 'options' => $role)); ?>
                        </div> 
                    </div>
                    </fieldset>
                <?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
                </div>
                
            </div>
        </div>
    </div>
</div>