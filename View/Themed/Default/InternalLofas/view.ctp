<div class="row"> 
    <div class="col-xs-12">
        <div class="x_panel tile">
            <div class="x_title">
                <h2><?php echo __('Internal Lofa'); ?></h2> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
				<div class="container table-responsive">
					<dl>
						<dt class="col-sm-2"><?php echo __('Price Min'); ?></dt>
						<dd class="col-sm-9">
							<?php echo h($internalLofa['InternalLofa']['price_min']); ?>
							&nbsp;
						</dd>
						<dt class="col-sm-2"><?php echo __('Price Max'); ?></dt>
						<dd class="col-sm-9">
							<?php echo h($internalLofa['InternalLofa']['price_max']); ?>
							&nbsp;
						</dd>
						<dt class="col-sm-2"><?php echo __('Approval'); ?></dt>
						<dd class="col-sm-9">
							<?php echo h($internalLofa['InternalLofa']['role']); ?>
							&nbsp;
						</dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
</div>