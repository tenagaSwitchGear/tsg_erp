<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('New Internal Lofa'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php //echo $this->Html->link(__('Items General'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Internal Lofas'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	
			<div class="internalLofas index">
				<table cellpadding="0" cellspacing="0" class="table table-bordered">
				<thead>
				<tr>
						<th><?php echo $this->Paginator->sort('id'); ?></th>
						<th><?php echo $this->Paginator->sort('price_min'); ?></th>
						<th><?php echo $this->Paginator->sort('price_max'); ?></th>
						<th><?php echo $this->Paginator->sort('Category'); ?></th>
						<th><?php echo $this->Paginator->sort('Approval'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($internalLofas as $internalLofa): ?>
				<tr>
					<td><?php echo h($internalLofa['InternalLofa']['id']); ?>&nbsp;</td>
					<td><?php echo h($internalLofa['InternalLofa']['price_min']); ?>&nbsp;</td>
					<td><?php echo h($internalLofa['InternalLofa']['price_max']); ?>&nbsp;</td>
					<td>
						<?php echo h($internalLofa['InternalLofa']['category']); ?>
					</td>
					<td><?php echo h($internalLofa['InternalLofa']['role']); ?></td>
					<td class="actions">

						<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $internalLofa['InternalLofa']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
						<?php if($_SESSION['Auth']['User']['role'] == 'ADMIN' || $_SESSION['Auth']['User']['role'] == 'HOD'){ ?>
						<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'view', $internalLofa['InternalLofa']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
						<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $internalLofa['InternalLofa']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($internalLofa['InternalLofa']['id']).'"', $internalLofa['InternalLofa']['id'])); ?>
						<?php } ?>
					</td>
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>	</p>
				
				<?php
					//echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					//echo $this->Paginator->numbers(array('separator' => ''));
					//echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				<ul class="pagination">
				<?php
				  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
				  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
				  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
				?>
				</ul>
			</div>
		</div>
	</div>
</div>
</div>