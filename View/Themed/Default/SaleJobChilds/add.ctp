<div class="saleJobChildren form">
<?php echo $this->Form->create('SaleJobChild'); ?>
	<fieldset>
		<legend><?php echo __('Add Sale Job Child'); ?></legend>
	<?php
		echo $this->Form->input('sale_job_id');
		echo $this->Form->input('customer_station_id');
		echo $this->Form->input('station_name');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('status');
		echo $this->Form->input('number');
		echo $this->Form->input('fat_date');
		echo $this->Form->input('delivery_date');
		echo $this->Form->input('code');
		echo $this->Form->input('po_created');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sale Job Children'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Jobs'), array('controller' => 'sale_jobs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job'), array('controller' => 'sale_jobs', 'action' => 'add')); ?> </li>
	</ul>
</div>
