<div class="saleJobChildren index">
	<h2><?php echo __('Sale Job Children'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_job_id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_station_id'); ?></th>
			<th><?php echo $this->Paginator->sort('station_name'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('number'); ?></th>
			<th><?php echo $this->Paginator->sort('fat_date'); ?></th>
			<th><?php echo $this->Paginator->sort('delivery_date'); ?></th>
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th><?php echo $this->Paginator->sort('po_created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleJobChildren as $saleJobChild): ?>
	<tr>
		<td><?php echo h($saleJobChild['SaleJobChild']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($saleJobChild['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleJobChild['SaleJob']['id'])); ?>
		</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['customer_station_id']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['station_name']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['name']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['description']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['status']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['number']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['fat_date']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['delivery_date']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['code']); ?>&nbsp;</td>
		<td><?php echo h($saleJobChild['SaleJobChild']['po_created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleJobChild['SaleJobChild']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleJobChild['SaleJobChild']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleJobChild['SaleJobChild']['id']), array(), __('Are you sure you want to delete # %s?', $saleJobChild['SaleJobChild']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Sale Job Child'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Jobs'), array('controller' => 'sale_jobs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Job'), array('controller' => 'sale_jobs', 'action' => 'add')); ?> </li>
	</ul>
</div>
