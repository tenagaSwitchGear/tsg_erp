 
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit Station</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

        	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name', 'SO No'); ?></th> 
			<th><?php echo $this->Paginator->sort('SaleJobChild.station_name', 'Job No'); ?></th>   
			<th>Po No</th>  
			<th><?php echo $this->Paginator->sort('date', 'PO Date'); ?></th>  
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($jobs as $saleOrder): ?>
	<tr> 
		<td>
			<?php echo h($saleOrder['SaleJobChild']['name']); ?>
		</td> 
		<td><?php echo h($saleOrder['SaleJobChild']['station_name']); ?> - <?php echo h($saleOrder['SaleJobChild']['name']); ?></td>
		<td>&nbsp;</td>
	   
		<td>&nbsp;</td>  
		<td>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $saleOrder['SaleJobChild']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?> 
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>

 
		</div>
	    </div>
	</div>
</div>

<?php

function status($status) {
	if($status == 1) {
		return 'Active';
	} else {
		return 'Draft';
	}
}
?>