<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><?php echo $saleJobChild['SaleJob']['name']; ?></h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleJobChildren view-data"> 
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Job'); ?></dt>
		<dd>
			<?php echo $this->Html->link($saleJobChild['SaleJob']['name'], array('controller' => 'sale_jobs', 'action' => 'view', $saleJobChild['SaleJob']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Station Id'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['customer_station_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Station Name'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['station_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fat Date'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['fat_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Delivery Date'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['delivery_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Po Created'); ?></dt>
		<dd>
			<?php echo h($saleJobChild['SaleJobChild']['po_created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
 
</div>
</div>
</div>
</div>