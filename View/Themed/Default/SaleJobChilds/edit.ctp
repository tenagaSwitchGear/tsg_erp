<?php echo $this->Html->link(__('Back To View'), array('controller' => 'sale_jobs', 'action' => 'view', $saleJob['SaleJob']['id']), array('class' => 'btn btn-success')); ?>

 

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit Station</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create('SaleJobChild', array('class' => 'form-horizontal form-label-left')); ?> 
	
	    <?php echo $this->Form->input('id'); ?>  
		<div class="form-group">
	    	<label class="col-sm-3">Job No</label>
	    	<div class="col-sm-9">
			<?php echo $this->Form->input('station_name', array('class' => 'form-control', 'label' => false, 'disabled'=> true)); ?>
			</div>
		</div> 
		<div class="form-group">
	    	<label class="col-sm-3">Station Name *</label>
	    	<div class="col-sm-9">
			<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
			</div>
		</div>   
		<div class="form-group">
	    	<label class="col-sm-3">FAT Date *</label>
	    	<div class="col-sm-9">
		<?php echo $this->Form->input('fat_date', array('id' => 'dateonly', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div>
		<div class="form-group">
	    	<label class="col-sm-3">Delivery Date *</label>
	    	<div class="col-sm-9">
			<?php echo $this->Form->input('delivery_date', array('id' => 'dateonly_2', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
		</div>
		</div> 

		<div class="form-group">
	    	<label class="col-sm-3">Remark</label>
	    	<div class="col-sm-9">
			<?php echo $this->Form->input('description', array('type' => 'textarea', 'class' => 'form-control', 'label' => false, 'required' => false, 'placeholder' => 'If change FAT / Delivery date, please add remark.')); ?>
		</div>
		</div> 
 
		<div class="form-group">
			<label class="col-sm-3">&nbsp;</label>
			<div class="col-sm-9">
				<?php echo $this->Form->button('Save', array('class' => 'btn btn-success pull-right')); ?>
			</div>
		</div>  
		<?php echo $this->Form->end(); ?>
 
</div>
</div>
</div>
</div>