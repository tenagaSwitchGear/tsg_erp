<?php  
function selected($id, $value) {
    if($id == $value) {
        return 'selected';
    }
}
?>
<?php echo $this->Html->link(__('Back To Costing'), array('controller' => 'costings', 'action' => 'view', $item['SaleQuotationItem']['sale_quotation_id']), array('class' => 'btn btn-success btn-sm')); ?>
<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Costing</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('SaleQuotationItemCost', array('class'=>'form-horizontal')); ?> 
 
          <div class="form-group">
            <div class="col-sm-12">
            <?php echo $item['SaleQuotationItem']['name']; ?> - <?php echo $item['InventoryItem']['name']; ?>
            </div>
             
          </div>  

<?php if($costs) { 
$i = 0;
?>  
<h4>Indirect Cost</h4>
<?php foreach($costs as $cost) { ?> 
<div id="removeCost<?php echo $cost['SaleQuotationItemCost']['id']; ?>">
<div class="form-group">
<div class="col-sm-4">
<input type="text" name="data[SaleQuotationItemCost][name][]" class="form-control inventoryItem" value="<?php echo $cost['SaleQuotationItemCost']['name']; ?>" id="findProduct<?php echo $i; ?>"required>
<input type="hidden" name="data[SaleQuotationItemCost][sale_quotation_item_id][]" value="<?php echo $item['SaleQuotationItem']['id']; ?>">

<input type="hidden" name="data[SaleQuotationItemCost][inventory_item_id][]" value="<?php echo $cost['SaleQuotationItemCost']['inventory_item_id']; ?>" id="productId<?php echo $i; ?>">

<input type="hidden" name="data[SaleQuotationItemCost][type][]" value="<?php echo $cost['SaleQuotationItemCost']['type']; ?>" id="productId<?php echo $i; ?>">
</div>
     
<div class="col-sm-1">
<input type="text" name="data[SaleQuotationItemCost][quantity][]" class="form-control"  value="<?php echo $cost['SaleQuotationItemCost']['quantity']; ?>"required>
</div>
<div class="col-sm-1">
<select name="data[SaleQuotationItemCost][general_unit_id][]" class="form-control"required>
<option value="">-Unit-</option>
<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"<?php echo selected($key, $cost['SaleQuotationItemCost']['general_unit_id']); ?>><?php echo $unit; ?></option><?php } ?>
</select></div>

<div class="col-sm-2"><input type="text" name="data[SaleQuotationItemCost][price][]" class="form-control markup<?php echo $i; ?>"  value="<?php echo $cost['SaleQuotationItemCost']['price']; ?>" id="price<?php echo $i; ?>"required></div>

<div class="col-sm-1"><input type="text" name="data[SaleQuotationItemCost][margin][]" class="form-control markup<?php echo $i; ?>"  value="<?php echo $cost['SaleQuotationItemCost']['margin']; ?>" id="margin<?php echo $i; ?>"required></div>

<div class="col-sm-2"><input type="text" name="data[SaleQuotationItemCost][total_price][]" class="form-control"  value="<?php echo $cost['SaleQuotationItemCost']['total_price']; ?>" id="totalprice<?php echo $i; ?>"required></div>
        
<div class="col-sm-1">
<a href="#" class="btn btn-danger" onclick="removeCost('<?php echo $cost['SaleQuotationItemCost']['id']; ?>'); return false"><i class="fa fa-times"></i></a>
</div>
</div>
</div>
<?php $i += 1; ?>
<?php } ?>

<?php } else { ?> 
<?php $i = 0; ?> 
<?php } ?>
 
<div id="loadMoreBom"></div> 
<div class="form-group"> 
    <div class="col-sm-12">
        <a href="#" id="addItem" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Indirect Cost</a>
    </div>
</div>

<?php if($softcosts) { ?>
<h4>Soft Cost</h4> 

<?php foreach($softcosts as $cost) { ?> 
<div id="removeCost<?php echo $cost['SaleQuotationItemCost']['id']; ?>">
<div class="form-group">
<div class="col-sm-4">
<input type="text" name="data[SaleQuotationItemCost][name][]" class="form-control inventoryItem" value="<?php echo $cost['SaleQuotationItemCost']['name']; ?>" id="findProduct<?php echo $i; ?>"required>
<input type="hidden" name="data[SaleQuotationItemCost][sale_quotation_item_id][]" value="<?php echo $item['SaleQuotationItem']['id']; ?>">

<input type="hidden" name="data[SaleQuotationItemCost][inventory_item_id][]" value="<?php echo $cost['SaleQuotationItemCost']['inventory_item_id']; ?>" id="productId<?php echo $i; ?>">

<input type="hidden" name="data[SaleQuotationItemCost][type][]" value="<?php echo $cost['SaleQuotationItemCost']['type']; ?>" id="productId<?php echo $i; ?>">

</div>

<div class="col-sm-5">
<input type="text" class="form-control" value="<?php echo $cost['InventoryItem']['name']; ?>" id="displayName<?php echo $i; ?>"readonly> 
</div>

<input type="hidden" name="data[SaleQuotationItemCost][quantity][]" class="form-control"  value="<?php echo $cost['SaleQuotationItemCost']['quantity']; ?>"required> 
<input type="hidden" name="data[SaleQuotationItemCost][general_unit_id][]" class="form-control" value="<?php echo $cost['SaleQuotationItemCost']['general_unit_id']; ?>">
 

<input type="hidden" name="data[SaleQuotationItemCost][total_price][]" class="form-control"  value="<?php echo $cost['SaleQuotationItemCost']['total_price']; ?>" id="totalprice<?php echo $i; ?>"required>
<input type="hidden" name="data[SaleQuotationItemCost][price][]" class="form-control"  value="<?php echo $cost['SaleQuotationItemCost']['price']; ?>" id="price<?php echo $i; ?>"required>

<div class="col-sm-2"><input type="text" name="data[SaleQuotationItemCost][margin][]" class="form-control markup<?php echo $i; ?>"  value="<?php echo $cost['SaleQuotationItemCost']['margin']; ?>" id="margin<?php echo $i; ?>"required></div>
 
        
<div class="col-sm-1">
<a href="#" class="btn btn-danger" onclick="removeCost('<?php echo $cost['SaleQuotationItemCost']['id']; ?>'); return false"><i class="fa fa-times"></i></a>
</div>
</div>
</div>
<?php $i += 1; ?>
<?php } ?>


<?php } else { 
$i = 0; ?>
 
<?php } ?> 

<div id="softCost"></div>

<div class="form-group"> 
    <div class="col-sm-12">
        <a href="#" id="addSoftCost" onclick="return false" class="btn btn-default"><i class="fa fa-plus"></i> Add Soft Cost</a>
    </div>
</div>
        


        <div class="form-group"> 
            <?php echo $this->Form->submit('Save', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
        </div>   
    
        <?php $this->Form->end(); ?>
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?> 
<script type="text/javascript"> 
function findBomByCategory(id, row) {    
    var category_id = $('#bomCategory'+id+row).val();
    $.ajax({ 
        type: "GET", 
        dataType: 'json',
        data: 'category_id=' + $('#bomCategory'+id+row).val(),
        url: baseUrl + 'boms/ajaxfindbombycategoryid', 
        success: function(respond) { 
            var option = '<option value="">Select Item</option>'; 
            $.each(respond, function(i, item) { 
                option += '<option value="' + item.status + '-' + item.id + '">' + item.name + '</option>';
            }); 
            $('#moreBomId'+id+row).html(option);
        }
    }); 
    return false; 
} 

function removeBom(row) {
    $('#removeBom'+row).html('');
    return false;
}

function removeCost(row) {
    $('#removeCost'+row).html('');
    return false;
}

function removeSoftCost(row) {
    $('#removeSoftCost'+row).html('');
    return false;
}

function findItem(row, search) { 
    console.log(search);
    $('#findProduct'+row).autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct'+row).val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            price: item.price,
                            unit: item.unit,
                            note: item.note
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#productId'+row).val( ui.item.id ); 
            $('#price'+row).val( ui.item.price ); 
            $('#total'+row).val( ui.item.price ); 
            $('#bom_id'+row).val( ui.item.bom_id ); 
            $('#quantity'+row).val(1);
            $('#displayName'+row).val(ui.item.name);
            $('#unit'+row).val(ui.item.unit);
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>" + item.price + "</small><br><small>" + item.note +  "</small></div>" ).appendTo( ul );
    }; 
}
  
function calculateCost(row, field) {
    $('.markup'+row).on('keyup', function() {
        if($.isNumeric($('#margin' + row).val())) {
            var price = parseFloat($('#price' + row).val());
            var margin = parseFloat($('#margin' + row).val()); 
            if(margin > 0) {
                var percent = price / 100 * margin;
                var total = price + percent;
                $('#totalprice' + row).val(total.toFixed(4));

                if($(this).hasClass('border-red')) {
                    $(this).removeClass('border-red');
                }
            }    
        } else {
            $(this).addClass('border-red');
            //alert('Please enter valid number format');
        } 
    }); 
}



$(document).ready(function() { 
    $(".inventoryItem").each(function(row){
        $(this).on({
    keypress: function() { 
        typed_into = true; 
    },
    change: function() {
            if (typed_into) {
                alert('type');
                typed_into = false; //reset type listener
            } else {
                alert('not type');
            }
        }
    });
        $('#findProduct'+row).autocomplete({ 
            source: function (request, response){ 
                $.ajax({
                    type: "GET",                        
                    url:baseUrl + 'inventory_items/ajaxfinditem',           
                    contentType: "application/json",
                    dataType: "json",
                    data: "term=" + $('#findProduct'+row).val(),                                                    
                    success: function (data) { 
                        response($.map(data, function (item) {
                            return {
                                id: item.id,
                                value: item.code,
                                name: item.name,
                                price: item.price
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {  
                $('#productId'+row).val( ui.item.id ); 
                $('#price'+row).val( ui.item.price ); 
                $('#total'+row).val( ui.item.price );  
                $('#quantity'+row).val(1);
            },
            minLength: 3
        }).autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br/><small>" + item.price + "</small><br>" +  "</div>" ).appendTo( ul );
        }; 

        $('.markup'+row).on('keyup', function() {
            if($.isNumeric($('#margin' + row).val())) {
                var price = parseFloat($('#price' + row).val());
                var margin = parseFloat($('#margin' + row).val());

                if(margin > 0) {
                    var percent = price / 100 * margin;
                    var total = price + percent;
                    $('#totalprice' + row).val(total.toFixed(4));

                    if($(this).hasClass('border-red')) {
                        $(this).removeClass('border-red');
                    }
                }    
            } else {
                $(this).addClass('border-red');
                alert('Please enter valid number format');
            } 
        }); 
    });



    var station = "<?php echo $i; ?>";
    $('#addItem').click(function() {
        var html = '<div id="removeBom'+station+'">'; 
        html += '<div class="form-group">';  
        html += '<div class="col-sm-4" id="autoComplete">';
        html += '<input type="text" name="data[SaleQuotationItemCost][name][]" id="findProduct'+station+'" class="form-control findProduct" placeholder="Enter Cost Code / Name (Ie: Transport, Labour)"autofocus><input type="hidden" name="data[SaleQuotationItemCost][sale_quotation_item_id][]" value="<?php echo $item['SaleQuotationItem']['id']; ?>">'; 
        html += '<input type="hidden" name="data[SaleQuotationItemCost][type][]" value="Indirect Cost">';
        html += '<input type="hidden" name="data[SaleQuotationItemCost][inventory_item_id][]" id="productId'+station+'"></div>';
         
       html += '<input type="hidden" class="form-control" id="displayName'+station+'"readonly>'; 
       

        html += '<div class="col-sm-1">';
        html += '<input type="text" name="data[SaleQuotationItemCost][quantity][]" id="quantity'+station+'" class="form-control" placeholder="Qty"required>';
        html += '</div>';

        html += '<div class="col-sm-1">';
        html += '<select name="data[SaleQuotationItemCost][general_unit_id][]" id="unit'+station+'" class="form-control"required>'; 
        html += '<option value="">-Unit-</option>';
        html += '<?php foreach($units as $key => $unit) { ?><option value="<?php echo $key; ?>"><?php echo $unit; ?></option><?php } ?>';
        html += '</select></div>';

        html += '<div class="col-sm-2"><input type="text" name="data[SaleQuotationItemCost][price][]" class="form-control markup'+station+'" id="price'+station+'" placeholder="Price" required></div>'; 

        html += '<div class="col-sm-1"><input type="text" name="data[SaleQuotationItemCost][margin][]" class="form-control markup'+station+'" id="margin'+station+'" placeholder="%Margin" required></div>'; 

        html += '<div class="col-sm-2"><input type="text" name="data[SaleQuotationItemCost][total_price][]" class="form-control" id="totalprice'+station+'" placeholder="Total" required></div>'; 
        
        html += '<div class="col-sm-1">';
        html += '<a href="#" class="btn btn-danger" onclick="removeBom('+station+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';    
        $("#loadMoreBom").append(html);   
        findItem(station, $(this).val());  
        calculateCost(station, $(this).val())
        station++; 
    });  

    var soft = 100000000;
    $('#addSoftCost').click(function() {
        var html = '<div id="removeSoftCost'+soft+'">'; 
        html += '<div class="form-group">';  
        html += '<div class="col-sm-4" id="autoComplete">';
        html += '<input type="text" name="data[SaleQuotationItemCost][name][]" id="findProduct'+soft+'" class="form-control findProduct" placeholder="Enter Cost Code / Name (Ie: Transport, Labour)"autofocus><input type="hidden" name="data[SaleQuotationItemCost][sale_quotation_item_id][]" value="<?php echo $item['SaleQuotationItem']['id']; ?>">'; 
        html += '<input type="hidden" name="data[SaleQuotationItemCost][type][]" value="Soft Cost">';
        html += '<input type="hidden" name="data[SaleQuotationItemCost][inventory_item_id][]" id="productId'+soft+'"></div>';
      
        html += '<input type="hidden" name="data[SaleQuotationItemCost][quantity][]" id="quantity'+soft+'" class="form-control" placeholder="Qty" value="1" required>';
        html += '<input type="hidden" name="data[SaleQuotationItemCost][general_unit_id][]" id="quantity'+soft+'" class="form-control" placeholder="Qty" value="1" required>';
 
       html += '<div class="col-sm-5">';
       html += '<input type="text" class="form-control" id="displayName'+soft+'"readonly>'; 
       html += '</div>';

        html += '<input type="hidden" name="data[SaleQuotationItemCost][price][]" class="form-control markup'+soft+'" id="price'+soft+'" placeholder="Price" required>'; 

        html += '<div class="col-sm-2"><input type="text" name="data[SaleQuotationItemCost][margin][]" class="form-control markup'+soft+'" id="margin'+soft+'" placeholder="%Margin" required></div>'; 

        html += '<input type="hidden" name="data[SaleQuotationItemCost][total_price][]" class="form-control" id="totalprice'+soft+'" placeholder="Total" value="0" required>'; 
        
        html += '<div class="col-sm-1">';
        html += '<a href="#" class="btn btn-danger" onclick="removeSoftCost('+soft+'); return false"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';    
        $("#softCost").append(html);   
        findItem(soft, $(this).val());  
        calculateCost(soft, $(this).val())
        soft++; 
    });
});
</script>
<?php $this->end(); ?>


