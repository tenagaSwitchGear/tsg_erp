<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Margin</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 

		<h4>Quotation No: <?php echo $item['SaleQuotation']['name']; ?></h4>

		<h4>Item Name: <?php echo $item['Product']['name']; ?></h4>

		<h4>Item Code: <?php echo $item['Product']['code']; ?></h4>

		<h4>Item Price: <?php echo $item['SaleQuotationItem']['total_price']; ?></h4>

		<h4>Additional Cost: <?php echo $item['SaleQuotationItem']['planning_price']; ?></h4>

        <?php echo $this->Session->flash(); ?>

        <?php echo $this->Form->create('SaleQuotationItemCost', array('class'=>'form-horizontal')); ?> 

        <?php $this->Form->end(); ?>
      </div>
    </div>
  </div> 
</div>