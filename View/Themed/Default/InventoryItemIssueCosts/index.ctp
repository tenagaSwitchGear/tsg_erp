<div class="inventoryItemIssueCosts index">
	<h2><?php echo __('Inventory Item Issue Costs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_material_request_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('production_order_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryItemIssueCosts as $inventoryItemIssueCost): ?>
	<tr>
		<td><?php echo h($inventoryItemIssueCost['InventoryItemIssueCost']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryItemIssueCost['InventoryMaterialRequestItem']['id'], array('controller' => 'inventory_material_request_items', 'action' => 'view', $inventoryItemIssueCost['InventoryMaterialRequestItem']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($inventoryItemIssueCost['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryItemIssueCost['ProductionOrder']['id'])); ?>
		</td>
		<td><?php echo h($inventoryItemIssueCost['InventoryItemIssueCost']['created']); ?>&nbsp;</td>
		<td><?php echo h($inventoryItemIssueCost['InventoryItemIssueCost']['amount']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryItemIssueCost['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryItemIssueCost['InventoryItem']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryItemIssueCost['InventoryItemIssueCost']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryItemIssueCost['InventoryItemIssueCost']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryItemIssueCost['InventoryItemIssueCost']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryItemIssueCost['InventoryItemIssueCost']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Inventory Item Issue Cost'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
