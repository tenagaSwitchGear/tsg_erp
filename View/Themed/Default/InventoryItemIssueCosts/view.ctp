<div class="inventoryItemIssueCosts view">
<h2><?php echo __('Inventory Item Issue Cost'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryItemIssueCost['InventoryItemIssueCost']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Material Request Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryItemIssueCost['InventoryMaterialRequestItem']['id'], array('controller' => 'inventory_material_request_items', 'action' => 'view', $inventoryItemIssueCost['InventoryMaterialRequestItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Production Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryItemIssueCost['ProductionOrder']['name'], array('controller' => 'production_orders', 'action' => 'view', $inventoryItemIssueCost['ProductionOrder']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryItemIssueCost['InventoryItemIssueCost']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($inventoryItemIssueCost['InventoryItemIssueCost']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryItemIssueCost['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryItemIssueCost['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Item Issue Cost'), array('action' => 'edit', $inventoryItemIssueCost['InventoryItemIssueCost']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Item Issue Cost'), array('action' => 'delete', $inventoryItemIssueCost['InventoryItemIssueCost']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryItemIssueCost['InventoryItemIssueCost']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Item Issue Costs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item Issue Cost'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
