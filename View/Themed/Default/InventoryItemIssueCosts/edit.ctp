<div class="inventoryItemIssueCosts form">
<?php echo $this->Form->create('InventoryItemIssueCost'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Item Issue Cost'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('inventory_material_request_item_id');
		echo $this->Form->input('production_order_id');
		echo $this->Form->input('amount');
		echo $this->Form->input('inventory_item_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventoryItemIssueCost.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventoryItemIssueCost.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Item Issue Costs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('controller' => 'inventory_material_request_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('controller' => 'inventory_material_request_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'production_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order'), array('controller' => 'production_orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
