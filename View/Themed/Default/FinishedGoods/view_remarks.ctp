
<div class="row"> 
  <div class="col-xs-12">
  <?php echo $this->Html->link(__('Rejected List'), array('action' => 'rework'), array('class' => 'btn btn-success btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="view-data">
		<h2><?php echo __('Product Details'); ?></h2>
			<dl>
				<dt><?php echo __('Bom Name'); ?></dt>
				<dd>
					<?php echo h($FinishedGood['InventoryItem']['name']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Bom Code'); ?></dt>
				<dd>
					<?php echo h($FinishedGood['InventoryItem']['code']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Serial #'); ?></dt>
				<dd>
					<?php echo h($FinishedGood['FinishedGood']['serial_no']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('QAT Status'); ?></dt>
				<dd>
					<a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" class="editable" data-type="select" data-pk="<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-name="#status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-emptytext="Select Status" data-url="<?php echo BASE_URL.'finishedgoods/setStatus'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $FinishedGood['FinishedGood']['status']; ?>" data-source="<?php echo BASE_URL.'finishedgoods/getStatus?status='.$FinishedGood['FinishedGood']['status'] .'&mfg=1'; ?>"></a>					
				</dd>

        <dt><?php echo __('FAT Status'); ?></dt>
        <dd>
          <?php if($FinishedGood['FinishedGood']['fat_status'] != 'Waiting') { ?>
          <a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="fat_status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" class="editable" data-type="select" data-pk="<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-name="#fat_status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-emptytext="Select Status" data-url="<?php echo BASE_URL.'finishedgoods/setFatStatus'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $FinishedGood['FinishedGood']['fat_status']; ?>" data-source="<?php echo BASE_URL.'finishedgoods/getFatStatus'; ?>"></a>   
          <?php } else { ?>
            <?php echo $FinishedGood['FinishedGood']['fat_status']; ?>
          <?php } ?>
               
        </dd>

       <dt><?php echo __('Final Inspection'); ?></dt>
        <dd>
        <?php if($FinishedGood['FinishedGood']['final_inspect'] != 'Waiting') { ?>
          <a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" class="editable" data-type="select" data-pk="<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-name="#status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-emptytext="Select Status" data-url="<?php echo BASE_URL.'finishedgoods/setFinalInspect'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $FinishedGood['FinishedGood']['final_inspect']; ?>" data-source="<?php echo BASE_URL.'finishedgoods/getFinalInspect'; ?>"></a> 
          <?php } else { ?>
            <?php echo $FinishedGood['FinishedGood']['final_inspect']; ?>
          <?php } ?>        
        </dd>

        <dt><?php echo __('Date Submitted'); ?></dt>
				<dd>
					<?php echo h($FinishedGood['FinishedGood']['created']); ?>
					&nbsp;
				</dd>			 
			</dl>
		</div>	
      
      </div>
      <div class="x_title">
        <h2>Remarks</h2> 
        <div class="clearfix"></div>
      </div>
      
      <div class="x_content">
        <div class="view-data">
        <?php echo $this->Form->create('FinishedGood', array('action' => 'view_remarks/'.$FinishedGood['FinishedGood']['id']), array('class' => 'form-horizontal form-label-left', 'type' => 'file')); ?>
        <div id="remarks">
  <?php if($remarks) { ?>
    <table class="table table-bordered">
    <tr>
    <th>Remark</th>
    <th>Attachment</th>
    <th>Category</th>
    <th>Status</th>
    <th>Action</th>
    </tr>
  <?php 
			foreach($remarks as $remark)
			{ 
				?>

        <tr>
        <td><?php echo h($remark['FinishedGoodRemark']['remark']); ?><input type="hidden" name="data[FinishedGoodRemark][finished_good_remark_id][]" value="<?php echo $remark['FinishedGoodRemark']['id']; ?>" id="FinishedGoodRemarkFinishedGoodId"></td>
        <td><?php if($remark['FinishedGoodRemark']['attachment']) { ?>
                       <a href="<?php echo BASE_URL . 'files/finished_good_remark/attachment/' . $remark['FinishedGoodRemark']['attachment_dir'] . '/' . $remark['FinishedGoodRemark']['attachment']; ?>" target="_blank"><i class="fa fa-paperclip"></i> <?php echo $remark['FinishedGoodRemark']['attachment']; ?></a>
                      <?php } else { ?>
                        <p>No attachment</p>
                      <?php } ?></td>
        <td><?php echo $remark['FinishedGoodComment']['name']; ?> </td>
        <td> <select id="status-<?php echo $remark['FinishedGoodRemark']['id']; ?>" name="data[FinishedGoodRemark][status][]" class="form-control"<?php echo ($remark['FinishedGoodRemark']['status'] == 'Closed' ? 'disabled' : ''); ?>>
                       <option value="">Select Status</option>
                       
                       <option value="Finished" <?php echo ($remark['FinishedGoodRemark']['status'] == 'Finished' ? 'selected="selected"' : ''); ?>>Finished</option>
                        
                     </select></td>
        <td><?php if($remark['FinishedGoodRemark']['status'] == 'Closed') { ?>
                     <a class="btn btn-default"><i class="fa fa-save"></i></a>
                     <?php } else { ?>
                     <a class="btn btn-success" onclick="saveRemark(<?php echo $remark['FinishedGoodRemark']['id']; ?>); return false"><i class="fa fa-save"></i></a>
                     <div id="ajaxstatus-<?php echo $remark['FinishedGoodRemark']['id']; ?>"></div>
                     <?php } ?> </td>
        </tr>
      <?php } ?>
      </table>
      <?php } ?>
        </div>    
          <?php echo $this->Form->end(); ?>          
        </div>
      </div>
      
    </div>
  </div> 
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript">	

function saveRemark(id) { 
  var postdata = { 
    'status' : $('#status-'+id).val(), 
    'id' : id,
    'type' : 'prod'
  }
  $.ajax({ 
      type: "POST", 
      dataType: 'json',
      data: postdata,
      url: baseUrl + 'finished_goods/ajaxsaveremark', 
      beforeSend: function() {  
          
      },
      complete: function() { 
          $('#ajaxstatus-'+id).html('Saved');
      },
      success: function(json) { 
        $('#ajaxstatus-'+id).html('Saved');
      }, 
      error: function(error) {
        console.log(error);
      }
  }); 
  return false; 
}

$(function() {	
	
	$('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
        }
    });
});
</script>
<?php $this->end(); ?>