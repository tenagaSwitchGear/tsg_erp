<style type="text/css">
.accordion h2 {
  cursor: pointer;
  padding: 10px 20px;
  background-color:#CCC;
  margin-top:0px;
  margin-bottom:10px;
  font-size:18px;
  font-weight:700;
}
.accordion h2:first-child {
  border-radius: 10px 10px 0 0;
}
.accordion h2:last-of-type {
  border-radius: 0 0 10px 10px;
}
.accordion h3 {
  cursor: pointer;
  padding: 7px 15px;
  background-color:#E4E4E4;
  margin-top:0px;
  margin-bottom:10px;
  font-size:15px;
  font-weight:500;
}
.accordion div {
  display: none;
  padding:10px;
}
.accordion .opened-for {
  display: block;
}
.accordion p {
  cursor:no-drop;
  padding: 10px 20px;
  background-color:#CCC;
  margin-top:0px;
  margin-bottom:10px;
  font-size:18px;
  font-weight:700;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'productionorders', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('New Production Order'), array('controller' => 'productionorders', 'action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Production Order</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="container table-responsive">
				<dl>
                  <dt class="col-sm-3"><?php echo __('Id'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['name']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Sale Job'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['SaleJob']['name'], array('controller' => 'salejobs', 'action' => 'view', $productionOrder['ProductionOrder']['sale_job_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Station'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['SaleJobChild']['name']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('BOM'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['SaleJobItem']['name'], array('controller' => 'boms', 'action' => 'view', $productionOrder['ProductionOrder']['bom_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Quantity'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['quantity']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Created'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['created']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['modified']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Start'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['start']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('End'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['end']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Status'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['status']); ?>
                      &nbsp;
                  </dd>
              </dl>
			</div>
			<div class="clearfix">&nbsp;</div>

			<div class="clearfix">&nbsp;</div>
			<div class="related table-responsive">
				<h4><?php echo __('List of Materials'); ?></h4>
                
                <?php if($results) { ?>
                <aside class="accordion">
                <?php
				echo $accordian_content;
				?>
                </aside>
                <?php die; ?>
				<?php foreach($main_childs as $main_child) { ?>
                <?php if($main_child['accordian'] == 'Yes') { ?>
                <h2><?php echo $main_child['child']['name']; ?></h2>
                <?php
				  $childs_or_items = $this->requestAction('productionorders/productionBomChildsORItems/'.$productionOrder['ProductionOrder']['id'].'/'.$productionOrder['ProductionOrder']['bom_id'].'/'.$main_child['child']['id'].'/'.$main_child['child']['bom_parent']);
				  echo "<pre>";			  
				  print_r($childs_or_items);
				  if(isset($childs_or_items['childs']))
				  {
					  foreach($childs_or_items['childs'] as $sub_childs)
					  {
						  if($main_child['accordian'] == 'Yes') 
						  {
						  }
						  else
						  {
						  }												  
					  }
				  }
				  if(isset($childs_or_items['items']))
				  {
					  ?>
                      <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                      <thead>
                          <tr>
                              <th class="text-center"><?php echo __('Item'); ?></th>
                              <th><?php echo __('Description'); ?></th>
                              <th><?php echo __('Unit'); ?></th>
                              <th><?php echo __('Qty Required'); ?></th>
                              <th><?php echo __('Actual Qty'); ?></th>
                              <th><?php echo __('Qty Issued'); ?></th>
                              <th><?php echo __('Qty Bal'); ?></th>
                          </tr>
                      </thead>
                      <?php
					  foreach($childs_or_items['items'] as $sub_items)
					  {
						  ?>
                          <tr>
                            <td><?php echo $sub_items['item']['inventory_item_id']; ?></td>
                            <td><?php echo $sub_items['item']['inventory_item_id']; ?></td>
                            <td><?php echo $sub_items['item']['inventory_item_id']; ?></td>
                            <td><?php echo $sub_items['item']['quantity_total']; ?></td>
                            <td><?php echo $sub_items['item']['quantity']; ?></td>
                            <td><?php echo $sub_items['item']['quantity_issued']; ?></td>
                            <td><?php echo $sub_items['item']['quantity_bal']; ?></td>
                          </tr>
                          <?php
						  						  												  
					  }
					  ?>
                      </table>
                      <?php
				  }
				?>
                <?php } else { ?>
                <p><?php echo $main_child['child']['name']; ?></p>
                <?php } ?>
                                
                <?php } ?>
                </aside>
                
                <?php } ?>
                
               <?php die;?>
                  
                <aside class="accordion">
                    <h2>News</h2>
                    <div>
                        <h3>News Item #1</h3>
                        <div>
                            <h3>News Item #1a</h3>
                            <div>
                                <h3>News Subitem 1</h3>
                                <div>
                                <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center"><?php echo __('Item'); ?></th>
                                        <th><?php echo __('Description'); ?></th>
                                        <th><?php echo __('Unit'); ?></th>
                                        <th><?php echo __('Qty Required'); ?></th>
                                        <th><?php echo __('Actual Qty'); ?></th>
                                        <th><?php echo __('Qty Issued'); ?></th>
                                        <th><?php echo __('Qty Bal'); ?></th>
                                    </tr>
                                </thead>
                                </table>                                
                                </div>
                                
                                <h3>News Subitem 2</h3>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                
                                <h3>News Subitem 3</h3>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            </div>
                            
                            <h3>News Item #1b</h3>
                            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            
                            <h3>News Item #1c</h3>
                            <div>
                                <h3>News Subitem 1</h3>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                
                                <h3>News Subitem 2</h3>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            </div>
                        </div>
                        
                        <h3>News Item #2</h3>
                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                    
                        <h3>News Item #3</h3>
                        <div>
                            <h3>News Item #3a</h32>
                            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            
                            <h3>News Item #3b</h3>
                            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                  
                    <h2>Updates</h2>
                    <div>
                        <h2>Update #1</h2>
                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                  
                        <h2>Update #2</h2>
                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                    
                        <h2>Update #3</h2>
                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                      
                        <h2>Update #4</h2>
                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                    </div>
                    
                    <h2>Miscellaneous</h2>
                    <div>
                        <h2>Misc. #1</h2>
                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                  
                        <h2>Misc. #2</h2>
                        <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                    
                        <h2>Misc. #3</h2>
                        <div>
                            <h2>Misc. Item #1a</h2>
                            <div>
                                <h2>Misc. Subitem 1</h2>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                
                                <h2>Misc. Subitem 2</h2>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                
                                <h2>Misc. Subitem 3</h2>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            </div>
                            <h2>Misc. Item #2a</h2>
                            <div>
                                <h2>Misc. Subitem 1</h2>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                
                                <h2>Misc. Subitem 2</h2>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                            </div>
                        </div>
                    </div>
                </aside>
                  
                
				<?php if (!empty($productionOrder['ProductionOrderChild']) || !empty($productionOrder['ProductionOrderItem'])): ?>
				<table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
					<thead>
						<tr>
							<th class="text-center"><?php echo __('Item'); ?></th>
							<th><?php echo __('Description'); ?></th>
							<th><?php echo __('Unit'); ?></th>
							<th><?php echo __('Qty Required'); ?></th>
							<th><?php echo __('Actual Qty'); ?></th>
							<th><?php echo __('Qty Issued'); ?></th>
							<th><?php echo __('Qty Bal'); ?></th>
						</tr>
					</thead>
					<?php foreach ($productionOrder['ProductionOrderChild'] as $ProductionOrderChild): ?>
					<tbody>
						<tr>
							<td colspan="7"><?php echo $ProductionOrderChild['name']; ?></td>
						</tr>
                        <?php foreach ($productionOrder['ProductionOrderItem'] as $ProductionOrderItem): ?>
                        <?php if($ProductionOrderChild['id'] == $ProductionOrderItem['production_order_child_id']): ?>
                        <tr>
							<td class="text-center"><?php echo $ProductionOrderItem['item_code']; ?></td>
							<td><?php echo $ProductionOrderItem['item_name']; ?></td>							
							<td><?php echo $ProductionOrderItem['unit_name']; ?></td>
							<td><?php echo $ProductionOrderItem['quantity_total']; ?></td>
							<td><?php echo $ProductionOrderItem['quantity']; ?></td>
							<td><?php echo $ProductionOrderItem['quantity_issued']; ?></td>
                            <td><?php echo $ProductionOrderItem['quantity_bal']; ?></td>
						</tr>
                        <? endif; ?>
                        <?php endforeach; ?>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php else: ?>
					&bull; No Related Materials for this order
				<?php endif; ?>
			</div>	

        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>

<?php $this->start('script'); ?> 
<script type="text/javascript">
$(function() { 
	var headers = ["H2", "H3"];

	$(".accordion").click(function(e) {
	  var target = e.target,
		  name = target.nodeName.toUpperCase();
	  
	  if($.inArray(name,headers) > -1) {
		var subItem = $(target).next();
		
		//slideUp all elements (except target) at current depth or greater
		var depth = $(subItem).parents().length;
		var allAtDepth = $(".accordion div").filter(function() {
		  if($(this).parents().length >= depth && this !== subItem.get(0)) {
			return true; 
		  }
		});
		$(allAtDepth).slideUp("fast");
		
		//slideToggle target content and adjust bottom border if necessary
		subItem.slideToggle("fast",function() {
			$(".accordion :visible:last").css("border-radius","0 0 10px 10px");
		});
		$(target).css({"border-bottom-right-radius":"0", "border-bottom-left-radius":"0"});
	  }
	});
});
</script>
<?php $this->end(); ?>