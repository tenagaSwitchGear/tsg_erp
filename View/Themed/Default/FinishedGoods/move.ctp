<div class="row"> 
	<div class="col-xs-12">    
  	<?php echo $this->Html->link(__('Final Products'), array('action' => 'final_products'), array('class' => 'btn btn-success btn-sm')); ?> 
    <div class="x_panel tile">
  		<div class="x_title">
    		<h2><?php echo h($item['FinishedGood']['serial_no']); ?></h2>
      	<div class="clearfix"></div>
    	</div>
    	<div class="x_content"> 
      	<?php echo $this->Session->flash(); ?>

        <div class="view-data">
        <h2><?php echo __('FinishedGood'); ?></h2>
          <dl>
            <dt><?php echo __('Id'); ?></dt>
            <dd>
              <?php echo h($item['FinishedGood']['id']); ?>
              &nbsp;
            </dd> 
            <dt><?php echo __('Serial Number'); ?></dt>
            <dd>
              <?php echo h($item['FinishedGood']['serial_no']); ?>
              &nbsp;
            </dd>
            <dt><?php echo __('Production Order Number'); ?></dt>
            <dd>
              <?php echo h($item['ProductionOrder']['name']); ?>
              &nbsp;
            </dd>

            <dt><?php echo __('Job Number'); ?></dt>
            <dd>
              <?php echo h($job['SaleJob']['name']); ?>
              &nbsp;
            </dd>

            <dt><?php echo __('Customer'); ?></dt>
            <dd>
              <?php echo $this->Html->link($job['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $job['Customer']['id'])); ?>
              &nbsp;
            </dd>

            <dt><?php echo __('Item Name'); ?></dt>
            <dd>
              <?php echo $this->Html->link($inventory_item['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventory_item['InventoryItem']['id'])); ?> 
            </dd>

            <dt><?php echo __('Item Code'); ?></dt>
            <dd>
              <?php echo $inventory_item['InventoryItem']['code']; ?>
              &nbsp;
            </dd>
            
            <dt><?php echo __('Created'); ?></dt>
            <dd>
              <?php echo h($item['FinishedGood']['created']); ?>
              &nbsp;
            </dd>
            <dt><?php echo __('Modified'); ?></dt>
            <dd>
              <?php echo h($item['FinishedGood']['modified']); ?>
              &nbsp;
            </dd>
            <dt><?php echo __('User'); ?></dt>
            <dd>
              <?php echo $this->Html->link($item['User']['username'], array('controller' => 'users', 'action' => 'view', $item['User']['id'])); ?>
              &nbsp;
            </dd>
            <dt><?php echo __('Status'); ?></dt>
            <dd>
              <?php echo h($item['FinishedGood']['status']); ?>
              &nbsp;
            </dd>
             
          </dl>
        </div>

        <h2>Move Finished Good To Stock Balance</h2>
        <?php echo $this->Session->flash(); ?> 
        <?php echo $this->Form->create('InventoryStock', array('class' => 'form-horizontal')); ?> 
          
        <div class="form-group">
            <label class="col-sm-3">Warehouse</label>
            <div class="col-sm-9">
              <?php echo $this->Form->input('inventory_location_id', array('options' => $warehouse, 'id' => 'inventory_location_id', 'class' => 'form-control', 'label' => false, 'empty' => '-Warehouse-', 'required' => false)); ?> 
              
              <?php echo $this->Form->input('general_unit_id', array('type' => 'hidden', 'value' => $inventory_item['InventoryItem']['general_unit_id'])); ?>  
              <?php echo $this->Form->input('inventory_item_id', array('type' => 'hidden', 'value' => $inventory_item['InventoryItem']['id'])); ?> 
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3">Store</label>
            <div class="col-sm-9"> 
              <?php echo $this->Form->input('inventory_store_id', array('empty' => '-Store-', 'id' => 'job_childs_id', 'type' => 'select', 'options' => $store, 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
               
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3">Rack / Space</label>
            <div class="col-sm-9"> 
                <?php echo $this->Form->input('inventory_rack_id', array('id' => 'job_items_id', 'empty' => '-Rack or Space-', 'options' => $rack, 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
              
            </div>
          </div>  
           
          <div class="form-group">
            <label class="col-sm-3">&nbsp;</label>
            <div class="col-sm-9">
                <?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
          </div>
        </div>  
        <?php echo $this->Form->end(); ?> 

      </div>
    </div>
  </div>
</div>