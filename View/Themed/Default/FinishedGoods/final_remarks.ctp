<?php echo $this->Html->css('/js/libs/jstree/themes/apple/style.css'); ?>
<div class="row"> 
  <div class="col-xs-12">
  <?php echo $this->Html->link(__('Submit To QC List'), array('action' => 'submit_qc'), array('class' => 'btn btn-success btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2>View</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
        <div class="view-data">
    <h2><?php echo __('Product Details'); ?></h2>
      <dl>
        <dt><?php echo __('Bom Name'); ?></dt>
        <dd>
          <?php echo h($FinishedGood['FinishedGood']['bom_name']); ?>
          &nbsp;
        </dd>
        <dt><?php echo __('Bom Code'); ?></dt>
        <dd>
          <?php echo h($FinishedGood['FinishedGood']['bom_code']); ?>
          &nbsp;
        </dd>
        <dt><?php echo __('Serial #'); ?></dt>
        <dd>
          <?php echo h($FinishedGood['FinishedGood']['serial_no']); ?>
          &nbsp;
        </dd>
        <dt><?php echo __('Final Inspect Status'); ?></dt>
        <dd>
          <a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" class="editable" data-type="select" data-pk="<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-name="#status_<?php echo $FinishedGood['FinishedGood']['id']; ?>" data-emptytext="Select Status" data-url="<?php echo BASE_URL.'finishedgoods/setFinalInspect'; ?>" data-placement="right" title="Select Status" data-value="<?php echo $FinishedGood['FinishedGood']['final_inspect']; ?>" data-source="<?php echo BASE_URL.'finishedgoods/getFinalInspect'; ?>"></a>         
        </dd>
                <dt><?php echo __('Date Submitted'); ?></dt>
        <dd>
          <?php echo h($FinishedGood['FinishedGood']['created']); ?>
          &nbsp;
        </dd>      
      </dl>
    </div>  
      
      </div>
      <div class="x_title">
        <h2>Remarks</h2> 
        <div class="clearfix"></div>
      </div>
      
      <div class="x_content">
        <div class="view-data">
        
        
        <?php
    if($FinishedGood['FinishedGoodRemark'])
    { $i = 0;
      foreach($FinishedGood['FinishedGoodRemark'] as $remark)
      { 
        if($remark['remark_type'] == 1)
        {
        ?>
                <div class="form-group"><div class="row">
                  <div class="col-sm-4">
                     <div class="input text">
                       <textarea name="data[FinishedGoodRemark][<?php echo $i; ?>][remark]" class="form-control" placeholder="Enter Remark" type="text" id="remark-<?php echo $remark['id']; ?>"><?php echo h($remark['remark']); ?></textarea>
                     </div>
                     <input type="hidden" name="data[FinishedGoodRemark][<?php echo $i; ?>][finished_good_id]" value="<?php echo $remark['finished_good_id']; ?>" id="finishedGoodId-<?php echo $remark['id']; ?>">
                     <input type="hidden" name="data[FinishedGoodRemark][<?php echo $i; ?>][remark_type]" value="1">
                   </div>
                   <div class="col-sm-2">
                     <select name="data[FinishedGoodRemark][<?php echo $i; ?>][status]" class="form-control" id="status-<?php echo $remark['id']; ?>">
                       <option value="">Select Status</option>
                       <option value="Waiting" <?php echo ($remark['status'] == 'Waiting' ? 'selected="selected"' : ''); ?>>Waiting</option>
                       <option value="WIP" <?php echo ($remark['status'] == 'WIP' ? 'selected="selected"' : ''); ?>>WIP</option>
                       <option value="Finished" <?php echo ($remark['status'] == 'Finished' ? 'selected="selected"' : ''); ?>>Finished</option>
                       <option value="Rejected" <?php echo ($remark['status'] == 'Rejected' ? 'selected="selected"' : ''); ?>>Rejected</option>
                       <option value="Closed" <?php echo ($remark['status'] == 'Closed' ? 'selected="selected"' : ''); ?>>Closed</option>
                     </select>
                   </div>
                    <div class="col-sm-2">
                     <select name="data[FinishedGoodRemark][<?php echo $i; ?>][finished_good_comment_id]" class="form-control" id="comment-<?php echo $remark['id']; ?>">
                       <option value="">-Category-</option>
                       <?php foreach($comments as $comment) { ?>
                        <option value="<?php echo $comment['FinishedGoodComment']['id']; ?>" <?php echo ($remark['finished_good_comment_id'] == $comment['FinishedGoodComment']['id'] ? 'selected="selected"' : ''); ?>><?php echo $comment['FinishedGoodComment']['name']; ?></option> 
                       <?php } ?> 
                     </select>
                   </div> 
                   <div class="col-sm-3">
                    <?php if($remark['attachment']) { ?>
                     
                     <a href="<?php echo BASE_URL . 'files/finished_good_remark/attachment/' . $remark['attachment_dir'] . '/' . $remark['attachment']; ?>" target="_blank"><i class="fa fa-paperclip"></i> <?php echo $remark['attachment']; ?></a>
                    <?php } else { ?>
                      <p>No attachment</p>
                    <?php } ?>
                   </div>
                   <div class="col-sm-1"> 
                     <a class="btn btn-success" onclick="saveRemark(<?php echo $remark['id']; ?>); return false"><i class="fa fa-save"></i></a>
                   </div>
                </div></div>
                <hr/>
                <?php $i += 1;
        } 
      }
    }
    ?>

    <?php echo $this->Form->create('FinishedGood', array('class' => 'form-horizontal form-label-left', 'type' => 'file')); ?>
          <div id="remarks"></div>

          <div class="form-group">  
              <div class="col-sm-12">
                   <a href="#" class="btn btn-primary" onclick="addChild(); return false"><i class="fa fa-plus"></i></a>
              </div> 
          </div>
          
          <div class="form-group">
              <label class="col-sm-3">&nbsp;</label>
              <div class="col-sm-9">
                  <?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
              </div>
          </div>
            
          <?php echo $this->Form->end(); ?>          
        </div>
      </div>
      
    </div>
  </div> 
</div>


<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript">

function saveRemark(id) { 
  var postdata = {
    'remark' : $('#remark-'+id).val(),
    'finished_good_id' : $('#finishedGoodId-'+id).val(),
    'status' : $('#status-'+id).val(),
    'finished_good_comment_id' : $('#comment-'+id).val(),
    'id' : id,
    'type' : 'qc'
  }
  $.ajax({ 
      type: "POST", 
      dataType: 'json',
      data: postdata,
      url: baseUrl + 'finished_goods/ajaxsaveremark', 
      beforeSend: function() {  
          
      },
      complete: function() { 
          
      },
      success: function(json) { 
        console.log(json)
      }, 
      error: function(error) {
        console.log(error);
      }
  }); 
  return false; 
}

  var row = 0;
  function addChild() {
    var html = '<div class="form-group"><div class="row">';
      html += '<div class="col-sm-4">';
      html += '<textarea name="data[FinishedGoodRemark]['+row+'][remark]" class="form-control"></textarea>';

       
      html += '</div>';
        
      html += '<div class="col-sm-2">';
      html += '<select name="data[FinishedGoodRemark]['+row+'][status]" class="form-control"><option value="">Select Status</option>';
    <?php foreach($remark_status as $remark_stat) { ?> html += '<option value="<?php echo $remark_stat; ?>"><?php echo $remark_stat; ?></option>'; <?php } ?> 
      html += '</select></div>';   

      html += '<div class="col-sm-2">';
      html += '<select name="data[FinishedGoodRemark]['+row+'][finished_good_comment_id]" class="form-control"><option value="">-Category-</option>';
    <?php foreach($comments as $comment) { ?> html += '<option value="<?php echo $comment['FinishedGoodComment']['id']; ?>"><?php echo $comment['FinishedGoodComment']['name']; ?></option>'; <?php } ?> 
      html += '</select></div>'; 

      html += '<div class="col-sm-3">'; 
      html += '<input type="file" name="data[FinishedGoodRemark]['+row+'][attachment]" class="form-control">';
      html += '<input type="hidden" name="data[FinishedGoodRemark]['+row+'][attachment_dir]" class="form-control">';
       html += '<input type="hidden" name="data[FinishedGoodRemark]['+row+'][finished_good_id]" value="<?php echo $FinishedGood['FinishedGood']['id']; ?>" class="form-control">';
       html += '<input type="hidden" name="data[FinishedGoodRemark]['+row+'][remark_type]" value="1">';

        html += '</div>';   

        html += '<div class="col-sm-1">';
        html += '<a class="btn btn-danger remove_remark"><i class="fa fa-times"></i></a>';
        html += '</div>';
        html += '</div></div>';
        row += 1;
        $('#remarks').append(html);
        return false;
    }

    $(function() {
    $(document).on("click",".remove_remark",function() {
      if(confirm('Are you sure you want to remove this remark?'))
      {
        $(this).parent().parent().remove();   
      }       
    });
  
  $('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
        }
    });
});
</script>
<?php $this->end(); ?>