<div class="row"> 
  <div class="col-xs-12">
    <?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'productionorders', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add New Production Order</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 		<div class="saleOrders form">
		<?php echo $this->Form->create('ProductionOrder', array('class' => 'form-horizontal form-label-left')); ?> 
	 		
 		<div class="form-group">
	    	<label class="col-sm-3">Enter Job No</label>
	    	<div class="col-sm-9">
	    		<?php echo $this->Form->input('find_job', array('id' => 'find_job', 'class' => 'form-control', 'label' => false)); ?>
	    		<div>
	    		<?php echo $this->Form->input('sale_job_id', array('id' => 'job_id', 'type' => 'hidden', 'label' => false)); ?> 
	    		</div>
	    	</div>
        </div>
        
        <div class="form-group">
	    	<label class="col-sm-3">Select Station</label>
	    	<div class="col-sm-9">
	    		<?php $stations = array('' => '-Select Station-'); ?>
                <?php echo $this->Form->input('sale_job_childs_id', array('onChange' => 'findItemsByStation(); return false','id' => 'job_childs_id', 'type' => 'select', 'options' => $stations, 'class' => 'form-control', 'label' => false)); ?>
	    		<div>
	    		</div>
	    	</div>
        </div>
        
        <div class="form-group">
	    	<label class="col-sm-3">Select Item</label>
	    	<div class="col-sm-9">
	    		<?php $items = array('' => '-Select Item-'); ?>
                <?php echo $this->Form->input('sale_job_items_id', array('onChange' => 'checkItemType(); return false', 'id' => 'job_items_id', 'type' => 'select', 'options' => $items, 'class' => 'form-control', 'label' => false)); ?>
	    		<div>
	    		<?php echo $this->Form->input('bom_id', array('id' => 'bomid', 'type' => 'hidden', 'label' => false)); ?>
				<?php echo $this->Form->input('is_bom', array('id' => 'is_bom', 'type' => 'hidden', 'value' => '1', 'label' => false)); ?> 
	    		</div>
	    	</div>
        </div>  
        <div class="form-group">
            <label class="col-sm-3">Production Order # (Unique)</label>
            <div class="col-sm-9">
        <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
        </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-3">Quantity</label>
            <div class="col-sm-9">
        <?php echo $this->Form->input('quantity', array('type' => 'text', 'id' => 'qty', 'class' => 'form-control', 'label' => false, 'readonly' => 'readonly')); ?>
        </div>
        </div>		

        <div class="form-group">
            <label class="col-sm-3">Start Date</label>
            <div class="col-sm-9">
        <?php echo $this->Form->input('start', array('id' => 'datepicker', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
        </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3">End Date</label>
            <div class="col-sm-9">
        <?php echo $this->Form->input('end', array('id' => 'datepicker_2', 'type' => 'text', 'class' => 'form-control', 'label' => false)); ?>
        </div>
        </div>
				
        <div class="form-group">
            <label class="col-sm-3">Status</label>
            <div class="col-sm-9">
            <?php $status = array(0 => 'Pending', 1 => 'WIP', 2 => 'Final Assembly', 3 => 'FAT', 4 => 'Rejected', 5 => 'Rework', 6 => 'Approve', 7 => 'Final Inspection', 8 => 'Packing', 9 => 'Completed'); ?>
        <?php echo $this->Form->input('status', array('options' => $status, 'class' => 'form-control', 'label' => false)); ?>
        </div>
        </div>			

        <div class="form-group">
        <label class="col-sm-3">&nbsp;</label>
        <div class="col-sm-9">
            <?php echo $this->Form->button('Next Step', array('class' => 'btn btn-success pull-right')); ?>
        </div>
			</div> 
			 
		<?php echo $this->Form->end(); ?>
		</div>
		 
      </div>
    </div>
  </div> 
</div>

<?php $this->start('script'); ?>
<script type="text/javascript">
function findItemsByStation()
{
	var job_childs_id = $('#job_childs_id').val();
	$.ajax({ 
		type: "GET", 
		dataType: 'json',
		data: 'job_childs_id=' + job_childs_id,
		url: baseUrl + 'productionorders/ajaxfinditemsbystationid', 
		success: function(respond) { 
			var option = '<option value="">-Select Item-</option>'; 
			$.each(respond, function(i, item) {
				console.log(90); 
				option += '<option data-qty="'+item.quantity+'" data-bom_id="'+item.bom_id+'" value="' + item.id + '">' + item.name + '</option>';
			}); 
			$('#job_items_id').html(option);
		}
	}); 
	return false;
}

function checkItemType()
{
	var job_items_id = $('#job_items_id').val();
	var bom_id = $('#job_items_id option:selected').data('bom_id');
	var quantity = $('#job_items_id option:selected').data('qty');
	
	$("#bomid").val(bom_id);
	$("#qty").val(quantity);
	return false;
}

$(document).ready(function() {
    $("#find_job").autocomplete({
	    source: function (request, response) {
	        $.ajax({
	            type: "GET",                        
	            url:baseUrl + 'salejobs/ajaxfindjob',           
	            contentType: "application/json",
	            dataType: "json",
	            data: "term=" + $('#find_job').val(),                                                    
	            success: function (data) {
	            	console.log(data);
		            response($.map(data, function (item) {
		                return {
		                    id: item.id,
		                    value: item.name
		                }
		            }));
	        	}
	        });
	    },
	    select: function (event, ui) {
	        var selected_id = ui.item.id;
			$("#job_id").val(selected_id);//Put Id in a hidden field
			
			$.ajax({ 
				type: "GET", 
				dataType: 'json',
				data: 'job_id=' + selected_id,
				url: baseUrl + 'productionorders/ajaxfindstationsyjobid', 
				success: function(respond) { 
					var option = '<option value="">-Select Station-</option>'; 
					$.each(respond, function(i, item) {
						console.log(90); 
						option += '<option value="' + item.id + '">' + item.name + '</option>';
					}); 
					$('#job_childs_id').html(option);
				}
			});       
	    },
	    minLength: 3

	});

});  
</script>
<?php $this->end(); ?>