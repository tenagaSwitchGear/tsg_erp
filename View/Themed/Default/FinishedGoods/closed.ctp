<div class="row"> 
  	<div class="col-xs-12">
    <?php $role = $this->Session->read('Auth.User.group_id'); ?>
  	<?php if($role == 15) { ?>
  		<?php echo $this->Html->link(__('Draft'), array('action' => 'index'), array('class' => 'btn btn-default btn-sm')); ?> 
	    <?php echo $this->Html->link(__('Inspection & Testing'), array('action' => 'submit_qc'), array('class' => 'btn btn-default btn-sm')); ?>
	    <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-default btn-sm')); ?> 
  	<?php } else { ?>
		<?php echo $this->Html->link(__('Inspection & Testing'), array('action' => 'submit_qc'), array('class' => 'btn btn-default btn-sm')); ?>
	    <?php echo $this->Html->link(__('Rejected'), array('action' => 'rejected'), array('class' => 'btn btn-default btn-sm')); ?>
	    
	    <?php echo $this->Html->link(__('Waiting FAT'), array('action' => 'final_products'), array('class' => 'btn btn-default btn-sm')); ?>
	    <?php echo $this->Html->link(__('FAT Pass'), array('action' => 'fat_pass'), array('class' => 'btn btn-success btn-sm')); ?>
	    <?php echo $this->Html->link(__('Final Inspection'), array('action' => 'final_inspection'), array('class' => 'btn btn-primary btn-sm')); ?>
	    <?php echo $this->Html->link(__('Completed (Ready To Pack)'), array('action' => 'completed'), array('class' => 'btn btn-success btn-sm')); ?> 
  	<?php } ?>
        <div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Complete Finished Goods'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->				
        	<p>Item lists is ready for delivery to customer.</p>	
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
				<thead>
				<tr>
						<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
						<th><?php echo $this->Paginator->sort('serial_no'); ?></th>
						<th>Job No</th>
						<th>Station</th>
						<th>Customer</th>
                        <th><?php echo $this->Paginator->sort('production_order_name', 'Production Order #'); ?></th>
                        <th><?php echo $this->Paginator->sort('bom_name', 'Name'); ?></th> 
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
						<th><?php echo $this->Paginator->sort('created'); ?></th>
						<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
					$currentPage = empty($this->Paginator->params['paging']['FinishedGood']['page']) ? 1 : $this->Paginator->params['paging']['FinishedGood']['page']; $limit = $this->Paginator->params['paging']['FinishedGood']['limit'];
					$startSN = (($currentPage * $limit) + 1) - $limit;

					foreach ($finalGoods as $finishedGood): ?>
				<tr>
					<td class="text-center"><?php echo $startSN++; ?></td>
                    <td>
					<?php echo h($finishedGood['FinishedGood']['serial_no']); ?>
					</td>
					<td>
					<?php echo h($finishedGood['SaleJob']['name']); ?>
					</td>
					<td>
					<?php echo h($finishedGood['SaleJobChild']['name']); ?>
					</td> 
					<td>
					<?php echo h($finishedGood['Customer']['name']); ?>
					</td> 
					<td>
						<?php echo $this->Html->link($finishedGood['FinishedGood']['production_order_name'], array('controller' => 'production_orders', 'action' => 'view', $finishedGood['FinishedGood']['production_order_id'])); ?>
					</td>
                    <td>
						<?php echo $this->Html->link($finishedGood['FinishedGood']['bom_name'], array('controller' => 'boms', 'action' => 'view', $finishedGood['FinishedGood']['bom_id'])); ?>
					</td> 
                    <td>
                    <?php echo h($finishedGood['FinishedGood']['status']); ?>
                    </td>
					<td><?php echo h($finishedGood['FinishedGood']['created']); ?>&nbsp;</td>

					<td>
					<?php echo $this->Html->link('<i class="fa fa-search"></i> View', array('action' => 'view', $finishedGood['FinishedGood']['id']), array('class' => 'btn btn-success btn-sm', 'escape' => false)); ?>
					</td>
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript">
$(document).ready(function() {
	//$('.editable').editable();
	$('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
        }
    });
});
</script>
<?php $this->end(); ?>