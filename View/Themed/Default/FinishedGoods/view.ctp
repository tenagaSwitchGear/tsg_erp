<style type="text/css">
.listing h2 {
  cursor: pointer;
  padding: 10px 20px;
  background-color:#CCC;
  margin-top:0px;
  margin-bottom:10px;
  font-size:18px;
  font-weight:700;
}
</style>
<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Production Orders'), array('controller' => 'productionorders', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('New Production Order'), array('controller' => 'productionorders', 'action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Production Order</h2>
                <?php if($productionOrder['ProductionOrder']['status'] != 9) { ?>
                <?php echo $this->Html->link(__('Complete'), array('controller' => 'productionorders', 'action' => 'complete', $productionOrder['ProductionOrder']['id']), array('class' => 'btn btn-success pull-right', 'escape' => false), __('Are you sure Production Order # %s completed?',$productionOrder['ProductionOrder']['name'])); ?> 
                <?php } ?>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
        	<div class="container table-responsive">
				<dl>
                  <dt class="col-sm-3"><?php echo __('Id'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['name']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Sale Job'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['SaleJob']['name'], array('controller' => 'salejobs', 'action' => 'view', $productionOrder['ProductionOrder']['sale_job_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Station'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['SaleJobChild']['name']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('BOM'); ?></dt>
                  <dd>
                      <?php echo $this->Html->link($productionOrder['SaleJobItem']['name'], array('controller' => 'boms', 'action' => 'view', $productionOrder['ProductionOrder']['bom_id'])); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Quantity'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['quantity']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Created'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['created']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Modified'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['modified']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Start'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['start']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('End'); ?></dt>
                  <dd>
                      <?php echo h($productionOrder['ProductionOrder']['end']); ?>
                      &nbsp;
                  </dd>
                  <dt class="col-sm-3"><?php echo __('Status'); ?></dt>
                  <dd>
                      <?php echo $status[$productionOrder['ProductionOrder']['status']]; ?>
                      &nbsp;
                  </dd>
              </dl>
			</div>
			<div class="clearfix">&nbsp;</div>

			<div class="clearfix">&nbsp;</div>
			<div class="related table-responsive listing">
				<h4><?php echo __('List of Materials'); ?></h4>
                <?php if($materials) { ?>
                  <?php if(isset($materials['childs'])) { ?>
                    <?php foreach($materials['childs'] as $material) { ?>
                     <h2><?php echo $this->Html->link($material['name'], array('action' => 'view', $material['production_order_id'], $material['id'], $material['bom_parent'])); ?></h2>
                    <?php } ?>
                  <?php } ?>
                  <?php if(isset($materials['items'])) { ?>
                  <table cellpadding = "0" cellspacing = "0" class="table table-hover table-bordered">
                  <thead>
                      <tr>
                          <th class="text-center"><?php echo __('Item'); ?></th>
                          <th><?php echo __('Description'); ?></th>
                          <th><?php echo __('Unit'); ?></th>
                          <th><?php echo __('BOM Qty'); ?></th>
                          <th><?php echo __('Qty Required'); ?></th>                          
                          <th><?php echo __('Qty Issued'); ?></th>
                          <th><?php echo __('Qty Bal'); ?></th>
                          <th width="11%"><?php echo __('Qty Requested'); ?></th>
                          <th width="10%"><?php echo __('Qty Request'); ?></th>
                      </tr>
                      <?php
					  echo $this->Form->create('ProductionOrder', array('url' => array('action'=>'view', $productionOrder['ProductionOrder']['id'], $child_id, $parent_id)));
					  //echo "<pre>";
					  //print_r($materials['items']);
					  //die;
					  ?>
                      <?php foreach($materials['items'] as $material) { ?>
                       <tr>
							<td class="text-center"><?php echo $material['item_code']; ?></td>
							<td><?php echo $material['item_name']; ?></td>							
							<td><?php echo $material['unit_name']; ?></td>
                            <td><?php echo $material['quantity']; ?></td>
							<td><?php echo $material['quantity_total']; ?></td>							
							<td><?php echo $material['quantity_issued']; ?></td>
                            <td><?php echo $material['quantity_bal']; ?></td>
                            <td><?php echo $material['quantity_req']; ?></td>
                            <td>
                            <?php echo $this->Form->input('quantity_req', array('class' => 'form-control input-mini', 'label' => false, 'name'=>'quantity_req['.$material['id'].'][]')); ?>
                            <?php echo $this->Form->input('production_order_id', array('type' => 'hidden', 'label' => false, 'value' => $productionOrder['ProductionOrder']['id'])); ?>
                            <?php echo $this->Form->input('production_order_child_id', array('type' => 'hidden', 'label' => false, 'value' => $child_id)); ?>
                            <?php echo $this->Form->input('inventory_item_id', array('type' => 'hidden', 'label' => false, 'name'=>'inventory_item_id['.$material['id'].'][]', 'value' => $material['inventory_item_id'])); ?>
                            <?php echo $this->Form->input('general_unit_id', array('type' => 'hidden', 'label' => false, 'name'=>'general_unit_id['.$material['id'].'][]', 'value' => $material['general_unit_id'])); ?>
                            </td>
						</tr>
                      <?php } ?>
                      <tr>
                       <td colspan="9">
                         <?php echo $this->Form->button('Submit', array('class' => 'btn btn-success pull-right')); ?>
                       </td>
                      </tr>
                      <?php echo $this->Form->end(); ?>
                  </thead>
                  </table>
                  <?php } ?>
				  
                <?php } else { ?>
                <h6><?php echo __('No Items found'); ?></h6>
                <?php } ?>
                  
                
				
			</div>	

        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>