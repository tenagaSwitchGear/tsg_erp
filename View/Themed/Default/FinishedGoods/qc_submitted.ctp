<div class="row"> 
  	<div class="col-xs-12"> 
  	<?php $role = $this->Session->read('Auth.User.group_id'); ?>
    <?php echo $this->Html->link(__('Draft'), array('action' => 'index'), array('class' => 'btn btn-primary btn-sm')); ?> 
    <?php echo $this->Html->link(__('Inspection & Testing'), array('action' => 'qc_submitted'), array('class' => 'btn btn-warning btn-sm')); ?>
    <?php echo $this->Html->link(__('Rejected'), array('action' => 'rework'), array('class' => 'btn btn-danger btn-sm')); ?> 

    <?php echo $this->Html->link(__('Dismantle'), array('action' => 'dismantlerequest'), array('class' => 'btn btn-primary btn-sm')); ?>

    <?php echo $this->Html->link(__('Completed (Ready To Pack)'), array('action' => 'pass'), array('class' => 'btn btn-success btn-sm')); ?>
  	
        <div class="x_panel tile">
      		<div class="x_title">
        		<h2>In Process Inspection & Testing (QAT)</h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->					
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-hover table-bordered">
				<thead>
				<tr>
						<th class="text-center"><?php echo $this->Paginator->sort('#'); ?></th>
						<th><?php echo $this->Paginator->sort('serial_no'); ?></th>
                        <th><?php echo $this->Paginator->sort('production_order_name', 'Production Order #'); ?></th>
                        <th><?php echo $this->Paginator->sort('InventoryItem.code', 'Code'); ?></th>
                        <th><?php echo $this->Paginator->sort('InventoryItem.name', 'Name'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
						<th><?php echo $this->Paginator->sort('created'); ?></th> 
				</tr>
				</thead>
				<tbody>
				<?php
					$currentPage = empty($this->Paginator->params['paging']['FinishedGood']['page']) ? 1 : $this->Paginator->params['paging']['FinishedGood']['page']; $limit = $this->Paginator->params['paging']['FinishedGood']['limit'];
					$startSN = (($currentPage * $limit) + 1) - $limit;

					foreach ($submitToQC as $finishedGood): ?>
				<tr>
					<td class="text-center"><?php echo $startSN++; ?></td>
                    <td>
					<a style="text-decoration: none !important;border-bottom: dashed 1px #0088cc !important;" href="#" id="serial_no<?php echo $finishedGood['FinishedGood']['id']; ?>" class="editable" data-type="text" data-pk="<?php echo $finishedGood['FinishedGood']['id']; ?>" data-name="#serial_no<?php echo $finishedGood['FinishedGood']['id']; ?>" data-emptytext="Enter Serial" data-url="<?php echo BASE_URL.'finished_goods/updateSerial'; ?>" data-placement="right" title="Enter Serial #"><?php echo $finishedGood['FinishedGood']['serial_no']; ?></a>
					</td>
					<td>
						<?php echo $this->Html->link($finishedGood['FinishedGood']['production_order_name'], array('controller' => 'productionorders', 'action' => 'view', $finishedGood['FinishedGood']['production_order_id'])); ?>
					</td>
                     <td>
						<?php echo $this->Html->link($finishedGood['InventoryItem']['code'], array('controller' => 'inventory_items', 'action' => 'view', $finishedGood['InventoryItem']['id'])); ?>
					</td>
                    <td><?php echo h($finishedGood['InventoryItem']['name']); ?>&nbsp;</td>
                    <td>
                    <?php echo $finishedGood['FinishedGood']['status']; ?>
                    </td>
					<td><?php echo h($finishedGood['FinishedGood']['created']); ?>&nbsp;</td>
                     
				</tr>
			<?php endforeach; ?>
				</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>
<?php $this->start('script'); ?>
<?php echo $this->Html->script('/vendors/bootstrap3-editable/js/bootstrap-editable.min.js'); ?>
<script type="text/javascript">
$(document).ready(function() {
	//$('.editable').editable();
	$('.editable').editable({
        validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
        }
    });
});
</script>
<?php $this->end(); ?>