<div class="productionOrders form">
<?php echo $this->Form->create('ProductionOrder'); ?>
	<fieldset>
		<legend><?php echo __('Edit Production Order'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('sale_tender_id');
		echo $this->Form->input('project_schedule_id');
		echo $this->Form->input('start');
		echo $this->Form->input('end');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProductionOrder.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProductionOrder.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Production Orders'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Tenders'), array('controller' => 'sale_tenders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Tender'), array('controller' => 'sale_tenders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedules'), array('controller' => 'project_schedules', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule'), array('controller' => 'project_schedules', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Production Order Children'), array('controller' => 'production_order_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Production Order Child'), array('controller' => 'production_order_children', 'action' => 'add')); ?> </li>
	</ul>
</div>
