
<div class="row"> 
  	<div class="col-xs-12">
  		<?php //echo $this->Html->link(__('New Purchase Requisitions'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Inventory Item'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Inventory History'); ?></h2>
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
        	<!-- content start-->
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0"  class="table table-hover table-bordered">
					<thead>
					<tr>
							<th class="text-center"><?php echo ('#'); ?></th>
                            <th><?php echo ('PR Number'); ?></th>
                            <th><?php echo ('Price Per Unit'); ?></th>
                            <th><?php echo ('Date Purchased'); ?></th>
                            <th><?php echo ('Discount'); ?></th>
                            <th><?php echo ('Tax'); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						$currentPage = empty($this->Paginator->params['paging']['InventoryPurchaseRequisitionItem']['page']) ? 1 : $this->Paginator->params['paging']['InventoryPurchaseRequisitionItem']['page']; $limit = $this->Paginator->params['paging']['InventoryPurchaseRequisitionItem']['limit'];
						$startSN = (($currentPage * $limit) + 1) - $limit;

						foreach ($inventoryPurchaseRequisitionItems as $inventoryPurchaseRequisitionItems): 
					?>
					<tr>
                        <td class="text-center"><?php echo $startSN++; ?></td>
                        <td><?php echo ($inventoryPurchaseRequisitionItems['InventoryPurchaseRequisition']['pr_no']); ?>&nbsp;</td>
                        <td><?php echo ($inventoryPurchaseRequisitionItems['InventoryPurchaseRequisitionItem']['price_per_unit']); ?>&nbsp;</td>
                        <td><?php echo (date('d/m/Y', strtotime($inventoryPurchaseRequisitionItems['InventoryPurchaseRequisition']['created']))); ?>&nbsp;</td>
                        <?php if($inventoryPurchaseRequisitionItems['InventoryPurchaseRequisitionItem']['discount_type'] == '1'){ $type = "(RM)"; }else{ $type = "(%)"; } ?>
                        <td><?php echo ($inventoryPurchaseRequisitionItems['InventoryPurchaseRequisitionItem']['discount']).' '.$type; ?>&nbsp;</td>
                        <td><?php echo ($inventoryPurchaseRequisitionItems['InventoryPurchaseRequisitionItem']['tax']).'%'; ?>&nbsp;</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<p>
				<?php
					//echo $this->Paginator->counter(array(
					//	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
					//));
				?>	</p>
				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled btn btn-default btn-sm'));
					echo $this->Paginator->numbers(array('separator' => ''), array('class'=>'btn btn-default btn-sm'));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled btn btn-default btn-sm'));
				?>
				</div>
			</div>
        	<!-- content end -->
      	</div>
    </div>
    </div>
</div>