<div class="inventoryPurchaseRequisitionItems form">
<?php echo $this->Form->create('InventoryPurchaseRequisitionItem'); ?>
	<fieldset>
		<legend><?php echo __('Edit Inventory Purchase Requisition Item'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('inventory_purchase_requisition_id');
		echo $this->Form->input('quantity');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('remark');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('InventoryPurchaseRequisitionItem.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('InventoryPurchaseRequisitionItem.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisition Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisitions'), array('controller' => 'inventory_purchase_requisitions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition'), array('controller' => 'inventory_purchase_requisitions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
	</ul>
</div>
