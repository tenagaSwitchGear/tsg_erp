<div class="inventoryPurchaseRequisitionItems view">
<h2><?php echo __('Inventory Purchase Requisition Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Purchase Requisition'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisition']['title'], array('controller' => 'inventory_purchase_requisitions', 'action' => 'view', $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisition']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryPurchaseRequisitionItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryPurchaseRequisitionItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['remark']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Purchase Requisition Item'), array('action' => 'edit', $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Purchase Requisition Item'), array('action' => 'delete', $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryPurchaseRequisitionItem['InventoryPurchaseRequisitionItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisition Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Purchase Requisitions'), array('controller' => 'inventory_purchase_requisitions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Purchase Requisition'), array('controller' => 'inventory_purchase_requisitions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
	</ul>
</div>
