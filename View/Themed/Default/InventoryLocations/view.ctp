<?php echo $this->Html->link(__('Add New Location'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Locations'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Warehouse Location</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryLocations view-data">
<h2><?php echo __('Inventory Location'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryLocation['InventoryLocation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($inventoryLocation['InventoryLocation']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($inventoryLocation['InventoryLocation']['address']); ?>
			&nbsp;
		</dd>
	</dl>
</div>


</div>
</div>
</div>
</div>
