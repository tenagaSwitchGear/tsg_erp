<?php echo $this->Html->link(__('Add New Location'), array('action' => 'add'), array('class' => 'btn btn-success btn-sm')); ?>
<?php echo $this->Html->link(__('Locations'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?> 

<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Warehouse Location</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventoryLocations form">
<?php echo $this->Form->create('InventoryLocation', array('class' => 'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Add Inventory Location'); ?></legend>
	<div class="form-group">
	<label class="col-sm-3">Name</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3">Full Address</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group"> 
    	<label class="col-sm-3"></label>
		<div class="col-sm-9">
    		<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success pull-right')); ?> 
    	</div>
    </div>	    
    </fieldset>
	<?php $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Inventory Locations'), array('action' => 'index')); ?></li>
	</ul>
</div>

</div>
</div>
</div>
</div>