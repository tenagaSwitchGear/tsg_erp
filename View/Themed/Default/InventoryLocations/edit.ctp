<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Warehouse Location'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Customers Contact Person'), array('controller' => 'customer_contact_people', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('List Customers Files'), array('controller' => 'customer_files', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Edit Inventory Location'); ?></h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
				<div class="inventoryLocations form">
				<?php echo $this->Form->create('InventoryLocation', array('class'=>'form-horizontal')); ?>
					<fieldset>
						
					<?php
						echo $this->Form->input('id');
						//echo $this->Form->input('name');
						//echo $this->Form->input('address');
					?>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Name</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("name", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
						<div class="form-group">
							<label class="col-sm-3" style="padding-top: 8px">Address</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input("address", array("class"=> "form-control", "label"=> false)); ?>
							</div> 
						</div>
					</fieldset>
				<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm')); ?>
				</div>
			</div>
		</div>
	</div>
</div>
