<?php echo $this->Html->link(__('Customer List'), array('controller'=>'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<?php echo $this->Html->link(__('Address List'), array('action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Customer Address</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>
       	 		<div class="customerAddresses form">
<?php echo $this->Form->create('CustomerAddress', array('class' => 'form-horizontal')); ?>
 	<div class="form-group">
	<label class="col-sm-3" style="padding-top: 8px">Company Name</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('customer_id', array('class' => 'form-control', "selected" => $_GET['id'], 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3" style="padding-top: 8px">Address</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3" style="padding-top: 8px">Postcode</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('postcode', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3" style="padding-top: 8px">City</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('city', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3" style="padding-top: 8px">State</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('state_id', array('empty' => array(0 =>'-Select State-'), 'class' => 'form-control', 'label' => false, 'required' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3" style="padding-top: 8px">Country</label>
	<div class="col-sm-9">
	<?php echo $this->Form->input('country_id', array('class' => 'form-control', 'label' => false)); ?>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3" style="padding-top: 8px"></label>
	<div class="col-sm-9">
		<?php 
			echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success btn-sm'));
		?>
		</div>
		</div>
 
<?php echo $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>