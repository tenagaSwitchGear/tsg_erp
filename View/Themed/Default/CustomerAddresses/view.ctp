<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php //echo $this->Html->link(__('New Customer Files'), array('controller' => 'customer_files', 'action' => 'add?id='.$customer['Customer']['id']), array('class' => 'btn btn-success btn-sm')); ?> 
  		<?php //echo $this->Html->link(__('New Customer Contact Person'), array('controller' => 'customer_contact_persons', 'action' => 'add?id='.$customer['Customer']['id']), array('class' => 'btn btn-success btn-sm')); ?> 
  		<?php //echo $this->Html->link(__('New Tender'), array('controller' => 'sale_tenders', 'action' => 'add?id='.$customer['Customer']['id']), array('class' => 'btn btn-success btn-sm')); ?> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo $customerAddress['Customer']['name']; ?></h2> 
        	<div class="clearfix"></div>
      	</div><div class="x_content"> 
      	<div class="container table-responsive">
       	 	<?php echo $this->Session->flash(); ?>
			<div class="customerAddresses view-data">
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($customerAddress['CustomerAddress']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Customer'); ?></dt>
				<dd>
					<?php echo $this->Html->link($customerAddress['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $customerAddress['Customer']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Address'); ?></dt>
				<dd>
					<?php echo h($customerAddress['CustomerAddress']['address']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Postcode'); ?></dt>
				<dd>
					<?php echo h($customerAddress['CustomerAddress']['postcode']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('City'); ?></dt>
				<dd>
					<?php echo h($customerAddress['CustomerAddress']['city']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('State'); ?></dt>
				<dd>
					<?php echo $this->Html->link($customerAddress['State']['name'], array('controller' => 'states', 'action' => 'view', $customerAddress['State']['id'])); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Country'); ?></dt>
				<dd>
					<?php echo $this->Html->link($customerAddress['Country']['name'], array('controller' => 'countries', 'action' => 'view', $customerAddress['Country']['id'])); ?>
					&nbsp;
				</dd>
			</dl>
		</div>
	</div> 
</div>


