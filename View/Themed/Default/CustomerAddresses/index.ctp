<?php echo $this->Html->link(__('Add Address'), array('controller' => 'customer_addresses', 'action' => 'add'), array('class' => 'btn btn-info btn-sm')); ?>

<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Customers Address</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>

    <?php echo $this->Form->create('CustomerContactPerson', array('class' => 'form-horizontal', 'type' => 'GET')); ?>
	<table cellpadding="0" cellspacing="0" class="table">
		<tr>
		<td><?php echo $this->Form->input('name', array('placeholder' => 'Contact Name', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('company_name', array('type' => 'text', 'placeholder' => 'Company Name', 'class' => 'form-control', 'label' => false, 'required' => false)); ?>  
		</td> 
		<td><?php echo $this->Form->input('office_phone', array('placeholder' => 'Office Phone', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly')); ?></td>
		<td><?php echo $this->Form->input('mobile_phone', array('placeholder' => 'Mobile Phone', 'class' => 'form-control', 'label' => false, 'id' => 'dateonly2')); ?></td>
		<td><?php echo $this->Form->submit('Search', array('type' => 'submit', 'name' => 'search', 'class' => 'btn btn-success pull-right')); ?></td> 
		</tr>
	</table>
	<?php $this->end(); ?> 
    
    <div class="customerAddresses index"> 
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name'); ?></th> 
			<th><?php echo $this->Paginator->sort('phone'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th> 
			<th><?php echo $this->Paginator->sort('address'); ?></th> 
			<th><?php echo $this->Paginator->sort('postcode'); ?></th> 
			<th><?php echo $this->Paginator->sort('city'); ?></th> 
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($customerAddresses as $customerAddress): ?>
	<tr> 
		<td>
			<?php echo $this->Html->link($customerAddress['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $customerAddress['Customer']['id'])); ?>
		</td>
		<td><?php echo h($customerAddress['CustomerAddress']['phone']); ?>&nbsp;</td> 
		<td><?php echo h($customerAddress['CustomerAddress']['email']); ?>&nbsp;</td> 
		<td><?php echo h($customerAddress['CustomerAddress']['address']); ?>&nbsp;</td> 
		<td><?php echo h($customerAddress['CustomerAddress']['postcode']); ?>&nbsp;</td> 
		<td><?php echo h($customerAddress['CustomerAddress']['city']); ?>&nbsp;</td> 
		<td>
			<?php echo $this->Html->link($customerAddress['Country']['name'], array('controller' => 'countries', 'action' => 'view', $customerAddress['Country']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $customerAddress['CustomerAddress']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $customerAddress['CustomerAddress']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $customerAddress['CustomerAddress']['id']), array(), __('Are you sure you want to delete # %s?', $customerAddress['CustomerAddress']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>
 

</div>
</div>
</div>
</div>
