<?php echo $this->Html->link(__('Add Address'), array('controller' => 'customer_addresses', 'action' => 'add'), array('class' => 'btn btn-info btn-sm')); ?>

<div class="row"> 
  	<div class="col-xs-12">
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Add Customer Address</h2> 
        		<div class="clearfix"></div>
      		</div>
      		<div class="x_content"> 
       	 		<?php echo $this->Session->flash(); ?>

       	 		<div class="customerAddresses form">
<?php echo $this->Form->create('CustomerAddress'); ?>
	<fieldset>
		<legend><?php echo __('Edit Customer Address'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('address');
		echo $this->Form->input('postcode');
		echo $this->Form->input('city');
		echo $this->Form->input('state_id');
		echo $this->Form->input('country_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
 
</div>
</div>
</div>
</div>