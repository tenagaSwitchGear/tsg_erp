<div class="row"> 
  	<div class="col-xs-12">
  	<?php echo $this->Html->link(__('New Supplier Item'), array('action' => 'add'), array('class'=>'btn btn-success btn-sm')); ?>
  		<?php echo $this->Html->link(__('Items General'), array('controller'=>'inventory_items', 'action' => 'index'), array('class'=>'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Supplier'), array('controller' => 'inventory_suppliers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Supplier Addresses'), array('controller' => 'inventory_supplier_addresses', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('List Supplier Contact Person'), array('controller' => 'inventory_supplier_contacts', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
        <?php echo $this->Html->link(__('List Inventory Supplier Items'), array('controller' => 'inventory_supplier_items', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
    	<div class="x_panel tile">
    		<h2><?php echo __('Edit Inventory Supplier Address'); ?></h2> 
        	<div class="clearfix"></div>
      	<div class="x_content"> 
      		<div class="x_title">
				<div class="inventorySupplierAddresses form">
					<?php echo $this->Form->create('InventorySupplierAddress', array('class' => 'form-horizontal')); ?>
					<?php echo $this->Form->input('id'); ?>
					<?php echo $this->Form->input('user_id', array('type' => 'hidden', 'class' => 'form-control', 'label' => false)); ?> 
					<fieldset>
						<div class="form-group">
							<label class="col-sm-3">Supplier</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('inventory_supplier_id', array('disabled'=>'disabled', 'class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Address</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('address', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Postcode</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('postcode', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">City</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('city', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">State</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('state_id', array('empty'=>array(0=>'Select State'), 'class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">State (Optional)</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('state', array('type'=>'text', 'class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Country</label>
							<div class="col-sm-9">
							<?php echo $this->Form->input('country_id', array('label'=>false, 'class' => 'form-control')); ?>
							</div>
						</div>																									
						<div class="form-group">
							<label class="col-sm-3">Code</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('code', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Phone</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Fax</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('fax', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Contact</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('contact', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Email</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3">Country</label>
							<div class="col-sm-9">
								<?php echo $this->Form->input('country_id', array('class' => 'form-control', 'label' => false)); ?> 
							</div>
						</div>
						
					</fieldset>
					<?php echo $this->Form->submit('Submit', array('type' => 'submit', 'class' => 'btn btn-success pull-right')); ?>
					<?php echo $this->Form->end(); ?>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
