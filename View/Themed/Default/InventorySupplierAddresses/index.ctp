<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Add New Contact'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
  		<?php echo $this->Html->link(__('Supplier Lists'), array('controller'=>'inventory_suppliers', 'action' => 'index'), array('class' => 'btn btn-info')); ?>  
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Supplier Contact</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
<div class="inventorySupplierAddresses index">
	<h2><?php echo __('Inventory Supplier Addresses'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-bordered">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_supplier_id'); ?></th>
			<th><?php echo $this->Paginator->sort('address'); ?></th>
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th><?php echo $this->Paginator->sort('phone'); ?></th>
			<th><?php echo $this->Paginator->sort('fax'); ?></th>
			<th><?php echo $this->Paginator->sort('contact'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventorySupplierAddresses as $inventorySupplierAddress): ?>
	<tr>
		<td><?php echo h($inventorySupplierAddress['InventorySupplierAddress']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventorySupplierAddress['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $inventorySupplierAddress['InventorySupplier']['id'])); ?>
		</td>
		<td><?php echo h($inventorySupplierAddress['InventorySupplierAddress']['address']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierAddress['InventorySupplierAddress']['code']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierAddress['InventorySupplierAddress']['phone']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierAddress['InventorySupplierAddress']['fax']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierAddress['InventorySupplierAddress']['contact']); ?>&nbsp;</td>
		<td><?php echo h($inventorySupplierAddress['InventorySupplierAddress']['email']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventorySupplierAddress['Country']['name'], array('controller' => 'countries', 'action' => 'view', $inventorySupplierAddress['Country']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link('<i class="fa fa-search"></i>', array('action' => 'view', $inventorySupplierAddress['InventorySupplierAddress']['id']), array('class' => 'btn btn-info btn-circle-sm', 'escape'=>false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('action' => 'edit', $inventorySupplierAddress['InventorySupplierAddress']['id']), array('class' => 'btn btn-success btn-circle-sm', 'escape'=>false)); ?>
			<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', array('action' => 'delete', $inventorySupplierAddress['InventorySupplierAddress']['id']), array('class' => 'btn btn-danger btn-circle-sm', 'escape'=>false), __('Are you sure you want to delete "'.h($inventorySupplierAddress['InventorySupplierAddress']['id']).'"', $inventorySupplierAddress['InventorySupplierAddress']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
	<?php
	  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
	  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
	?>
	</div>
</div>

</div>
</div>
</div>