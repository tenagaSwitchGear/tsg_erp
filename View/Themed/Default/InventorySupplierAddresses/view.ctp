<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('List Supplier'), array('controller' => 'inventorySuppliers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('List Addresses'), array('controller' => 'inventory_supplier_addresses', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
 		<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventorySupplierAddress['InventorySupplierAddress']['id']), array('class' => 'btn btn-warning')); ?> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Inventory Supplier Address</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
			<div class="container table-responsive">		
			<dl>
				<dt class="col-sm-2"><?php echo __('Id'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['id']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Inventory Supplier'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo $this->Html->link($inventorySupplierAddress['InventorySupplier']['name'], array('controller' => 'inventory_suppliers', 'action' => 'view', $inventorySupplierAddress['InventorySupplier']['id'])); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Address'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['address']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Postcode'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['postcode']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('City'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['city']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('State'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo $this->Html->link($inventorySupplierAddress['State']['name'], array('controller' => 'states', 'action' => 'view', $inventorySupplierAddress['State']['id'])); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('State(Optional)'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['state']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Code'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['code']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Phone'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['phone']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Fax'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['fax']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Contact'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['contact']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Email'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['email']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Country'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo $this->Html->link($inventorySupplierAddress['Country']['name'], array('controller' => 'countries', 'action' => 'view', $inventorySupplierAddress['Country']['id'])); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Created'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['created']); ?>
					&nbsp;
				</dd>
				<dt class="col-sm-2"><?php echo __('Modified'); ?></dt>
				<dd class="col-sm-9">
					: <?php echo h($inventorySupplierAddress['InventorySupplierAddress']['modified']); ?>
					&nbsp;
				</dd>
			</dl>
		</div>
	</div>
</div>
</div>
</div>
