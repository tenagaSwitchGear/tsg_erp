<div class="row"> 
  	<div class="col-xs-12">
  		<?php echo $this->Html->link(__('Supplier Lists'), array('controller'=>'inventory_suppliers', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?>
  		<?php echo $this->Html->link(__('Address Lists'), array('controller' => 'inventory_supplier_addresses', 'action' => 'index'), array('class' => 'btn btn-info btn-sm')); ?> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2><?php echo __('Add Inventory Supplier Address'); ?></h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>

<div class="inventorySupplierContacts form">
<?php echo $this->Form->create('InventorySupplierAddress', array('class' => 'form-horizontal')); ?>
<?php echo $this->Form->input('user_id', array('label'=>false, 'class' => 'form-control', 'type'=>'hidden', 'value'=>$_SESSION['Auth']['User']['id'])); ?>
	<div class="form-group">
		<label class="col-sm-3">Supplier</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('inventory_supplier_id', array('label'=>false, 'class' => 'form-control', 'selected'=>$_GET['id'])); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Address</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('address', array('label'=>false, 'class' => 'form-control', 'required')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Postcode</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('postcode', array('label'=>false, 'class' => 'form-control', 'required')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">City</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('city', array('label'=>false, 'class' => 'form-control', 'required')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">State</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('state_id', array('label'=>false, 'class' => 'form-control', 'empty'=>array('0'=>'Select State'), 'required'=>false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">State (optional)</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('state', array('label'=>false, 'class' => 'form-control', 'type'=>'text', 'required'=>false)); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Country</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('country_id', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Code</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('code', array('label'=>false, 'class' => 'form-control', 'required')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Phone</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('phone', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Fax</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('fax', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Contact</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('contact', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Email</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('email', array('label'=>false, 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3"></label>
		<div class="col-sm-9"> 
		<?php echo $this->Form->submit('Submit', array('div' => false, 'class' => 'btn btn-success')); ?>
		
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
 
</div>
</div>
</div>
</div>

