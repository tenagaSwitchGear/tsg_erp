<?php echo $this->Html->link('<button type="button" class="btn default"><i class="fa fa-plus"></i> '.__('New Group').'</button>', array('action' => 'add'), array('escape' => false)); ?>
                <?php echo $this->Html->link('<button type="button" class="btn default"><i class="fa fa-list"></i> '.__('List Users').'</button>', array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>
                <?php echo $this->Html->link('<button type="button" class="btn default"><i class="fa fa-plus"></i> '.__('New User').'</button>', array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Groups</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

 
<!--For Search -->
<?php echo $this->Session->flash(); ?> 
<div class="row">
    <div class="col-md-12">
      <div class="table-wrap">
      	<div class="table-head">
            <div class="table-head-lt"><i class="fa fa-filter"></i><?php echo __('Filter'); ?></div>
             <?php echo $this->element('admin/table_toolbar');?>
            <div style="clear:both"></div>
        </div>
        <div style="padding:20px 10px 0px 10px;" class="table-responsive filter-responsive">
        <?php echo $this->Form->create('Group', array('controller' => 'groups', 'action' => 'index','novalidate' => true)); ?>
        	<table cellspacing="0" cellpadding="0" border="0" class="filt-tab table">
            <tbody>
                    <tr>
                       
                       <td style="padding-left:1%"><?php echo $this->Form->input('Group.name', array('type' => 'text','label' => false,'class'=>'form-control ','placeholder'=>'Name'));?></td>
                       
						<td  style="padding-left:1%" class="sandbox-container">
                                <?php echo $this->Form->input('Group.from', array('type'=> 'text' , 'class'=> 'form-control', 'placeholder' => 'From Created','label'=> false , 'div' => false,'hiddenField'=>false)); ?>
                        </td>
						<td  style="padding-left:1%" class="sandbox-container">
                                <?php echo $this->Form->input('Group.to', array('type'=> 'text' , 'class'=> 'form-control', 'placeholder' => 'To Created','label'=> false , 'div' => false,'hiddenField'=>false)); ?>
                        </td>
                                 
                       <td style="padding-left:1%"><?php echo $this->Form->button('<i class="fa fa-search"></i>Filter', array('class' => 'btn blue','title' => 'Click here to Search'),array('escape'=>false) );?> </td>
                     </tr>
            </tbody>
            </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div style="clear:both"> </div>
     </div>
    </div>
</div>
</br>
<!--End Search -->
<div class="row">
    <div class="col-md-12"> 
        <div class="table-wrap"> 
                 <?php echo $this->element('admin/table_toolbar');?>
               
            <div style="padding:20px 10px 0px 10px;" class="table-responsive">
               <table cellspacing="0" cellpadding="0" border="0" class=" table">
                  <thead>
                    <tr>
                    <th><?php echo __('S.No'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('modified'); ?></th>			
					<th><?php echo __('Actions'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
							<?php 
							if(count($groups))
								{
									$currentPage = empty($this->Paginator->params['paging']['Group']['page']) ? 1 : $this->Paginator->params['paging']['Group']['page'];
									$limit = $this->Paginator->params['paging']['Group']['limit'];	
									$startSN = (($currentPage * $limit) + 1) - $limit;
							 foreach ($groups as $group){ ?>	
							<tr>
							<td>
								<?php echo $startSN++; ?>
							</td>
							<td>
								<?php echo h($group['Group']['name']); ?>
							
							</td>
							<td>
								<?php echo h($group['Group']['modified']); ?>
							</td>
							
							<td>
							
							<?php 
							echo $this->Html->link('<i class="fa fa-pencil"></i>',   array('action'=>'edit', $group['Group']['id']),array("class" => "btn default btn-sm",'escape' => false,'title'=>'Edit' ) ); 
							?>
                            <?php 
							if($this->Session->read('Auth.User.group_id') != $group['Group']['id']){
							echo $this->Form->postLink('<i class="fa fa-trash-o"></i>', array('action' => 'delete',  $group['Group']['id']), array("class" => "btn default btn-sm", "escape" => false, "alt" => "Delete Record", "title" => "Delete Record"), __('Are you sure you want to delete # %s?',  $group['Group']['id'])); 
							}
							?>
							</td>
							
							</tr>
							 <?php } 
							}
							else{
							?>
							<tr><td colspan='4' align='center'><?php echo NO_RECORD; ?></td></tr>
							<?php
							} 
							 ?>
							
							
							</tbody>
            </table>
				  <div class="col-md-6">
					<p>
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
					</p>
				 </div>
				  <div class="col-md-6">
                  <nav>
                  <ul class="pagination">
                   
                    <li>
					<?php
                    echo $this->Paginator->prev(' Prev ',array('tag'=>'li'),null, array('class' => 'disabled','aria-label'=>'Previous','tag'=>'li'));
                    echo $this->Paginator->numbers(array('separator' => '','tag'=>'li'),null, array('class' => 'disabled','tag'=>'li'));
                    echo $this->Paginator->next( ' Next ',array('tag'=>'li'),null, array('class' => 'disabled','tag'=>'li','aria-label'=>'Next'));
                    ?>
                    </li>
                   
                  </ul>
                </nav>
                </div>
             </div>
           
               <div style="clear:both"> </div>
        </div>
    </div>
    </div>
</div>			


 </div>
    </div>
    </div>
</div>      