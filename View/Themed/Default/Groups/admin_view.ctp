<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<?php echo $this->element('admin/theme_setting')?>

<!-- BEGIN PAGE HEADER-->

<h3 class="page-title">
<?php echo __('Group'); ?>
</h3>

		<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<?php echo $this->Html->link(__('Home'), array('plugin' => false, 'controller' => 'users','action' => 'dashboard')); ?>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<?php echo $this->Html->link(__('Group'), array('plugin' => false, 'controller' => 'groups','action' => 'index')); ?>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<?php echo __('Groups View'); ?>
			</li>
		</ul>
		<?php /*?><?php echo $this->element('admin/action_tool_bar');?><?php */?>
		</div>
<!-- END PAGE HEADER-->



<!--Links-->


<?php echo $this->Session->flash(); ?> 

<div class="row" style="margin-bottom:10px">
	<div class="col-md-12">
<?php

echo $this->Html->link('<button type="button" class="btn purple-plum">'.__('List Groups').'</button>', array('action' => 'index'),array('escape'=> false));

echo $this->Html->link('<button type="button" class="btn blue-madison">'.__('New Group').'</button>', array('action' => 'add'),array('escape'=> false));

echo $this->Html->link('<button type="button" class="btn yellow-crusta">'.__('List Users').'</button>', array('action' => 'index'),array('escape'=> false));

echo $this->Html->link('<button type="button" class="btn green-meadow">'.__('New User').'</button>', array('action' => 'add'),array('escape'=> false));


?>

	</div>
</div>	

<!--End Links-->




<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						
						<div class="">
							
							<div class="tab-pane" id="tab_3">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo __('View Group'); ?>
										</div>
										<?php echo $this->element('admin/table_tool_bar');?>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										 <?php echo $this->Form->create('Group',array('class'=>'form-horizontal')); ?>
											<div class="form-body">
												 <h2 class="margin-bottom-20"> View Group Info  -<?php echo h($group['Group']['name']); ?></h2>
												<!-- <h3 class="form-section">Person Info</h3> -->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3"><?php echo __('Group'); ?>:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																<?php echo h($group['Group']['name']); ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3"><?php echo __('Modified'); ?>:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	<?php echo h($group['Group']['modified']); ?>
																</p>
															</div>
														</div>
													</div>
                                                    <div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3"><?php echo __('Created'); ?>:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	<?php echo h($group['Group']['created']); ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												
												
												</div>
												
												</div>
											</div>
											
										</form>
										<!-- END FORM-->
									</div>
								</div>
								
										<!-- END FORM-->
									</div>
								</div>
							</div>
						
<!-- END PAGE CONTENT-->

<?php echo $this->element('admin/table_setting'); ?>