<?php echo $this->Html->link('<button type="button" class="btn default"><i class="fa fa-list"></i> '.__('List Groups').'</button>', array('controller' => 'groups', 'action' => 'index'), array('escape' => false)); ?>
                <?php echo $this->Html->link('<button type="button" class="btn default"><i class="fa fa-list"></i> '.__('List Users').'</button>', array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?>
                <?php echo $this->Html->link('<button type="button" class="btn default"><i class="fa fa-plus"></i> '.__('New User').'</button>', array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?>

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Edit Group</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
 
<div class="row">
	<div class="col-md-12" >
		<?php echo $this->element('Groups/form');?>
	</div>
</div> 

</div>
</div> 
</div>
</div> 