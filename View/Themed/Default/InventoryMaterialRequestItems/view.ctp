<div class="inventoryMaterialRequestItems view">
<h2><?php echo __('Inventory Material Request Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Material Request'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequestItem['InventoryMaterialRequest']['id'], array('controller' => 'inventory_material_requests', 'action' => 'view', $inventoryMaterialRequestItem['InventoryMaterialRequest']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequestItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialRequestItem['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('General Unit'); ?></dt>
		<dd>
			<?php echo $this->Html->link($inventoryMaterialRequestItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryMaterialRequestItem['GeneralUnit']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Issued Quantity'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Issued Balance'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['note']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Inventory Material Request Item'), array('action' => 'edit', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Inventory Material Request Item'), array('action' => 'delete', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Material Requests'), array('controller' => 'inventory_material_requests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request'), array('controller' => 'inventory_material_requests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
	</ul>
</div>
