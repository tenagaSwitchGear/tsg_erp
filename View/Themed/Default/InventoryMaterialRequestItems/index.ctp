<div class="inventoryMaterialRequestItems index">
	<h2><?php echo __('Inventory Material Request Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_material_request_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('general_unit_id'); ?></th>
			<th><?php echo $this->Paginator->sort('issued_quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('issued_balance'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('note'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($inventoryMaterialRequestItems as $inventoryMaterialRequestItem): ?>
	<tr>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialRequestItem['InventoryMaterialRequest']['id'], array('controller' => 'inventory_material_requests', 'action' => 'view', $inventoryMaterialRequestItem['InventoryMaterialRequest']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialRequestItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $inventoryMaterialRequestItem['InventoryItem']['id'])); ?>
		</td>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['quantity']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($inventoryMaterialRequestItem['GeneralUnit']['name'], array('controller' => 'general_units', 'action' => 'view', $inventoryMaterialRequestItem['GeneralUnit']['id'])); ?>
		</td>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_quantity']); ?>&nbsp;</td>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['issued_balance']); ?>&nbsp;</td>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['created']); ?>&nbsp;</td>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['modified']); ?>&nbsp;</td>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['note']); ?>&nbsp;</td>
		<td><?php echo h($inventoryMaterialRequestItem['InventoryMaterialRequestItem']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id']), array(), __('Are you sure you want to delete # %s?', $inventoryMaterialRequestItem['InventoryMaterialRequestItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Inventory Material Request Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Requests'), array('controller' => 'inventory_material_requests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request'), array('controller' => 'inventory_material_requests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
	</ul>
</div>
