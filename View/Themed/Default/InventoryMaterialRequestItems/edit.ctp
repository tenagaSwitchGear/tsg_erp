<div class="row"> 
  	<div class="col-xs-12"> 
    	<div class="x_panel tile">
      		<div class="x_title">
        		<h2>Edit MRN Item</h2> 
        	<div class="clearfix"></div>
      	</div>
      	<div class="x_content"> 
        	<?php echo $this->Session->flash(); ?>
 
<?php echo $this->Form->create('InventoryMaterialRequestItem', array('class' => 'form-horizontal')); ?>
	  
	<?php echo $this->Form->input('id'); ?>
	<div class="form-group">
		<label class="col-sm-3">Current Item</label>
		<div class="col-sm-9">
		 <?php echo $this->Form->input('dv', array('value' => $item['InventoryItem']['code'], 'disabled' => true, 'class' => 'form-control', 'label' => false)); ?>
		 <?php echo $this->Form->input('item', array('value' => $item['InventoryItem']['code'], 'type' => 'hidden')); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Item *</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('InventoryItem.code', array('id' => 'findProduct', 'class' => 'form-control', 'label' => false)); ?>
		<?php echo $this->Form->input('inventory_item_id', array('id' => 'item_id', 'type' => 'hidden')); ?> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3">Note (Optional)</label>
		<div class="col-sm-9">
		<?php echo $this->Form->input('note', array('class' => 'form-control', 'label' => false)); ?>
		</div>
	</div>
	 
<?php echo $this->Form->end(__('Submit'), array('class' => 'btn btn-success')); ?>
 
</div>
</div>
</div>
</div>


<?php $this->start('script'); ?>
<script type="text/javascript"> 
 
$(document).ready(function() {
    $('#findProduct').autocomplete({ 
        source: function (request, response){ 
            $.ajax({
                type: "GET",                        
                url:baseUrl + 'inventory_items/ajaxfinditem',           
                contentType: "application/json",
                dataType: "json",
                data: "term=" + $('#findProduct').val(),                                                    
                success: function (data) { 
                    response($.map(data, function (item) {
                        return {
                            id: item.id,
                            value: item.code,
                            name: item.name,
                            note: item.note,
                            unit: item.unit
                        }
                    }));
                }
            });
        },
        select: function (event, ui) {  
            $('#item_id').val( ui.item.id );   
        },
        minLength: 3
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" ).append( "<div>" + item.value + "<br><small>" + item.name + "</small><br><small>"+item.note+"</small></div>" ).appendTo( ul );
    };
});
</script>
<?php $this->end(); ?>