<div class="inventoryMaterialRequestItems form">
<?php echo $this->Form->create('InventoryMaterialRequestItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Inventory Material Request Item'); ?></legend>
	<?php
		echo $this->Form->input('inventory_material_request_id');
		echo $this->Form->input('inventory_item_id');
		echo $this->Form->input('quantity');
		echo $this->Form->input('general_unit_id');
		echo $this->Form->input('issued_quantity');
		echo $this->Form->input('issued_balance');
		echo $this->Form->input('note');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Inventory Material Request Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Inventory Material Requests'), array('controller' => 'inventory_material_requests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Material Request'), array('controller' => 'inventory_material_requests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List General Units'), array('controller' => 'general_units', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New General Unit'), array('controller' => 'general_units', 'action' => 'add')); ?> </li>
	</ul>
</div>
