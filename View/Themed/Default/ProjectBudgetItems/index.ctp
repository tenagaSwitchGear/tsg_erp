<div class="projectBudgetItems index">
	<h2><?php echo __('Project Budget Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('project_budget_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_quotation_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_order_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('material_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('indirect_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('soft_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('margin'); ?></th>
			<th><?php echo $this->Paginator->sort('planning_price'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projectBudgetItems as $projectBudgetItem): ?>
	<tr>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($projectBudgetItem['ProjectBudget']['id'], array('controller' => 'project_budgets', 'action' => 'view', $projectBudgetItem['ProjectBudget']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projectBudgetItem['SaleQuotationItem']['name'], array('controller' => 'sale_quotation_items', 'action' => 'view', $projectBudgetItem['SaleQuotationItem']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($projectBudgetItem['SaleOrderItem']['id'], array('controller' => 'sale_order_items', 'action' => 'view', $projectBudgetItem['SaleOrderItem']['id'])); ?>
		</td>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['material_cost']); ?>&nbsp;</td>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['indirect_cost']); ?>&nbsp;</td>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['soft_cost']); ?>&nbsp;</td>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['margin']); ?>&nbsp;</td>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['planning_price']); ?>&nbsp;</td>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['created']); ?>&nbsp;</td>
		<td><?php echo h($projectBudgetItem['ProjectBudgetItem']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $projectBudgetItem['ProjectBudgetItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $projectBudgetItem['ProjectBudgetItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $projectBudgetItem['ProjectBudgetItem']['id']), array(), __('Are you sure you want to delete # %s?', $projectBudgetItem['ProjectBudgetItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project Budget Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Budgets'), array('controller' => 'project_budgets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Budget'), array('controller' => 'project_budgets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
