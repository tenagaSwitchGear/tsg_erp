<div class="projectBudgetItems view">
<h2><?php echo __('Project Budget Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Budget'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectBudgetItem['ProjectBudget']['id'], array('controller' => 'project_budgets', 'action' => 'view', $projectBudgetItem['ProjectBudget']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Quotation Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectBudgetItem['SaleQuotationItem']['name'], array('controller' => 'sale_quotation_items', 'action' => 'view', $projectBudgetItem['SaleQuotationItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Order Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectBudgetItem['SaleOrderItem']['id'], array('controller' => 'sale_order_items', 'action' => 'view', $projectBudgetItem['SaleOrderItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Material Cost'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['material_cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Indirect Cost'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['indirect_cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Soft Cost'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['soft_cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Margin'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['margin']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Planning Price'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['planning_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($projectBudgetItem['ProjectBudgetItem']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project Budget Item'), array('action' => 'edit', $projectBudgetItem['ProjectBudgetItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project Budget Item'), array('action' => 'delete', $projectBudgetItem['ProjectBudgetItem']['id']), array(), __('Are you sure you want to delete # %s?', $projectBudgetItem['ProjectBudgetItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Budget Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Budget Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Budgets'), array('controller' => 'project_budgets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Budget'), array('controller' => 'project_budgets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
