<div class="projectBudgetItems form">
<?php echo $this->Form->create('ProjectBudgetItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Project Budget Item'); ?></legend>
	<?php
		echo $this->Form->input('project_budget_id');
		echo $this->Form->input('sale_quotation_item_id');
		echo $this->Form->input('sale_order_item_id');
		echo $this->Form->input('material_cost');
		echo $this->Form->input('indirect_cost');
		echo $this->Form->input('soft_cost');
		echo $this->Form->input('margin');
		echo $this->Form->input('planning_price');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Project Budget Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Budgets'), array('controller' => 'project_budgets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Budget'), array('controller' => 'project_budgets', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
	</ul>
</div>
