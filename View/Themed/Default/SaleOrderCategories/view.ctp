<div class="saleOrderCategories view">
<h2><?php echo __('Sale Order Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saleOrderCategory['SaleOrderCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($saleOrderCategory['SaleOrderCategory']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sale Order Category'), array('action' => 'edit', $saleOrderCategory['SaleOrderCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sale Order Category'), array('action' => 'delete', $saleOrderCategory['SaleOrderCategory']['id']), array(), __('Are you sure you want to delete # %s?', $saleOrderCategory['SaleOrderCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Sale Orders'); ?></h3>
	<?php if (!empty($saleOrderCategory['SaleOrder'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Sale Job Id'); ?></th>
		<th><?php echo __('Sale Job Child Id'); ?></th>
		<th><?php echo __('Sale Tender Id'); ?></th>
		<th><?php echo __('Sale Quotation Id'); ?></th>
		<th><?php echo __('Customer Purchase Order No'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Date Fat'); ?></th>
		<th><?php echo __('Date Delivery'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Sale Order Category Id'); ?></th>
		<th><?php echo __('Progress'); ?></th>
		<th><?php echo __('Term Of Payment Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($saleOrderCategory['SaleOrder'] as $saleOrder): ?>
		<tr>
			<td><?php echo $saleOrder['id']; ?></td>
			<td><?php echo $saleOrder['sale_job_id']; ?></td>
			<td><?php echo $saleOrder['sale_job_child_id']; ?></td>
			<td><?php echo $saleOrder['sale_tender_id']; ?></td>
			<td><?php echo $saleOrder['sale_quotation_id']; ?></td>
			<td><?php echo $saleOrder['customer_purchase_order_no']; ?></td>
			<td><?php echo $saleOrder['date']; ?></td>
			<td><?php echo $saleOrder['date_fat']; ?></td>
			<td><?php echo $saleOrder['date_delivery']; ?></td>
			<td><?php echo $saleOrder['created']; ?></td>
			<td><?php echo $saleOrder['modified']; ?></td>
			<td><?php echo $saleOrder['user_id']; ?></td>
			<td><?php echo $saleOrder['status']; ?></td>
			<td><?php echo $saleOrder['name']; ?></td>
			<td><?php echo $saleOrder['sale_order_category_id']; ?></td>
			<td><?php echo $saleOrder['progress']; ?></td>
			<td><?php echo $saleOrder['term_of_payment_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'sale_orders', 'action' => 'view', $saleOrder['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'sale_orders', 'action' => 'edit', $saleOrder['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sale_orders', 'action' => 'delete', $saleOrder['id']), array(), __('Are you sure you want to delete # %s?', $saleOrder['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
