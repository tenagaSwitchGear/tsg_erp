<?php echo $this->Html->link(__('Business Category'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Html->link(__('Add New Category'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>  

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Add Business Catehories</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>

<div class="saleOrderCategories form">
<?php echo $this->Form->create('SaleOrderCategory'); ?> 
	<?php
		echo $this->Form->input('name', array('class' => 'form-control'));
	?> 
<?php echo $this->Form->end(__('Submit')); ?>
</div>
 
</div>
</div>
</div>
</div>