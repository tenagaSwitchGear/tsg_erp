<?php echo $this->Html->link(__('Business Category'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Html->link(__('Add New Category'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>  

<div class="row"> 
  <div class="col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2>Business Catehories</h2> 
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
        <?php echo $this->Session->flash(); ?>
  
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover">
	<thead>
	<tr> 
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($saleOrderCategories as $saleOrderCategory): ?>
	<tr> 
		<td><?php echo h($saleOrderCategory['SaleOrderCategory']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saleOrderCategory['SaleOrderCategory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saleOrderCategory['SaleOrderCategory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saleOrderCategory['SaleOrderCategory']['id']), array(), __('Are you sure you want to delete # %s?', $saleOrderCategory['SaleOrderCategory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody> 
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<ul class="pagination">
			<?php
			  echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&laquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
			  echo $this->Paginator->next('&raquo;', array('tag' => 'li', 'escape' => false), '<a href="#">&raquo;</a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
			?>
			</ul>
</div>

</div> 
</div>
</div>  