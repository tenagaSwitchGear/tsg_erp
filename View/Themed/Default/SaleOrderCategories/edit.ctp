<div class="saleOrderCategories form">
<?php echo $this->Form->create('SaleOrderCategory'); ?>
	<fieldset>
		<legend><?php echo __('Edit Sale Order Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SaleOrderCategory.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SaleOrderCategory.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Order Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Sale Orders'), array('controller' => 'sale_orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order'), array('controller' => 'sale_orders', 'action' => 'add')); ?> </li>
	</ul>
</div>
