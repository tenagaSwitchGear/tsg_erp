<div class="finishedGoodDeliveryItems view">
<h2><?php echo __('Finished Good Delivery Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good Delivery'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['FinishedGoodDelivery']['do_number'], array('controller' => 'finished_good_deliveries', 'action' => 'view', $finishedGoodDeliveryItem['FinishedGoodDelivery']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Finished Good'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $finishedGoodDeliveryItem['FinishedGood']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Order Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['SaleOrderItem']['id'], array('controller' => 'sale_order_items', 'action' => 'view', $finishedGoodDeliveryItem['SaleOrderItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale Quotation Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['SaleQuotationItem']['name'], array('controller' => 'sale_quotation_items', 'action' => 'view', $finishedGoodDeliveryItem['SaleQuotationItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Inventory Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $finishedGoodDeliveryItem['InventoryItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Remark'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['remark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['User']['id'], array('controller' => 'users', 'action' => 'view', $finishedGoodDeliveryItem['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['type']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Finished Good Delivery Item'), array('action' => 'edit', $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Finished Good Delivery Item'), array('action' => 'delete', $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Delivery Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Delivery Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Good Deliveries'), array('controller' => 'finished_good_deliveries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Delivery'), array('controller' => 'finished_good_deliveries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
