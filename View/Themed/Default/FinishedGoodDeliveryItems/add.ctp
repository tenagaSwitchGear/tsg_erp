<div class="finishedGoodDeliveryItems form">
<?php echo $this->Form->create('FinishedGoodDeliveryItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Finished Good Delivery Item'); ?></legend>
	<?php
		echo $this->Form->input('finished_good_delivery_id');
		echo $this->Form->input('finished_good_id');
		echo $this->Form->input('sale_order_item_id');
		echo $this->Form->input('sale_quotation_item_id');
		echo $this->Form->input('inventory_item_id');
		echo $this->Form->input('remark');
		echo $this->Form->input('user_id');
		echo $this->Form->input('type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Finished Good Delivery Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Good Deliveries'), array('controller' => 'finished_good_deliveries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Delivery'), array('controller' => 'finished_good_deliveries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
