<div class="finishedGoodDeliveryItems index">
	<h2><?php echo __('Finished Good Delivery Items'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('finished_good_delivery_id'); ?></th>
			<th><?php echo $this->Paginator->sort('finished_good_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_order_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sale_quotation_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('inventory_item_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('remark'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($finishedGoodDeliveryItems as $finishedGoodDeliveryItem): ?>
	<tr>
		<td><?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['FinishedGoodDelivery']['do_number'], array('controller' => 'finished_good_deliveries', 'action' => 'view', $finishedGoodDeliveryItem['FinishedGoodDelivery']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['FinishedGood']['id'], array('controller' => 'finished_goods', 'action' => 'view', $finishedGoodDeliveryItem['FinishedGood']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['SaleOrderItem']['id'], array('controller' => 'sale_order_items', 'action' => 'view', $finishedGoodDeliveryItem['SaleOrderItem']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['SaleQuotationItem']['name'], array('controller' => 'sale_quotation_items', 'action' => 'view', $finishedGoodDeliveryItem['SaleQuotationItem']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['InventoryItem']['name'], array('controller' => 'inventory_items', 'action' => 'view', $finishedGoodDeliveryItem['InventoryItem']['id'])); ?>
		</td>
		<td><?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['created']); ?>&nbsp;</td>
		<td><?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['remark']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($finishedGoodDeliveryItem['User']['id'], array('controller' => 'users', 'action' => 'view', $finishedGoodDeliveryItem['User']['id'])); ?>
		</td>
		<td><?php echo h($finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['type']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id']), array(), __('Are you sure you want to delete # %s?', $finishedGoodDeliveryItem['FinishedGoodDeliveryItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Finished Good Delivery Item'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Finished Good Deliveries'), array('controller' => 'finished_good_deliveries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good Delivery'), array('controller' => 'finished_good_deliveries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Finished Goods'), array('controller' => 'finished_goods', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Finished Good'), array('controller' => 'finished_goods', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Order Items'), array('controller' => 'sale_order_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Order Item'), array('controller' => 'sale_order_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sale Quotation Items'), array('controller' => 'sale_quotation_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sale Quotation Item'), array('controller' => 'sale_quotation_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Inventory Items'), array('controller' => 'inventory_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Inventory Item'), array('controller' => 'inventory_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
