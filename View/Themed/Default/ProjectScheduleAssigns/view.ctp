<div class="projectScheduleAssigns view">
<h2><?php echo __('Project Schedule Assign'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($projectScheduleAssign['ProjectScheduleAssign']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Schedule Child'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectScheduleAssign['ProjectScheduleChild']['title'], array('controller' => 'project_schedule_children', 'action' => 'view', $projectScheduleAssign['ProjectScheduleChild']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Manpower'); ?></dt>
		<dd>
			<?php echo $this->Html->link($projectScheduleAssign['ProjectManpower']['id'], array('controller' => 'project_manpowers', 'action' => 'view', $projectScheduleAssign['ProjectManpower']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project Schedule Assign'), array('action' => 'edit', $projectScheduleAssign['ProjectScheduleAssign']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project Schedule Assign'), array('action' => 'delete', $projectScheduleAssign['ProjectScheduleAssign']['id']), array(), __('Are you sure you want to delete # %s?', $projectScheduleAssign['ProjectScheduleAssign']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedule Assigns'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Assign'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Schedule Children'), array('controller' => 'project_schedule_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Child'), array('controller' => 'project_schedule_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Manpowers'), array('controller' => 'project_manpowers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Manpower'), array('controller' => 'project_manpowers', 'action' => 'add')); ?> </li>
	</ul>
</div>
