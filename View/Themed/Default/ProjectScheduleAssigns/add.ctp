<div class="projectScheduleAssigns form">
<?php echo $this->Form->create('ProjectScheduleAssign'); ?>
	<fieldset>
		<legend><?php echo __('Add Project Schedule Assign'); ?></legend>
	<?php
		echo $this->Form->input('project_schedule_child_id');
		echo $this->Form->input('project_manpower_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Project Schedule Assigns'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Project Schedule Children'), array('controller' => 'project_schedule_children', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Schedule Child'), array('controller' => 'project_schedule_children', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Project Manpowers'), array('controller' => 'project_manpowers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project Manpower'), array('controller' => 'project_manpowers', 'action' => 'add')); ?> </li>
	</ul>
</div>
