<?php
App::uses('AppModel', 'Model');
/**
 * GeneralMedia Model
 *
 * @property InventoryPurchaseRequisitionFile $InventoryPurchaseRequisitionFile
 */
class GeneralMedia extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'general_medias';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryPurchaseRequisitionFile' => array(
			'className' => 'InventoryPurchaseRequisitionFile',
			'foreignKey' => 'general_media_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
