<?php
App::uses('AppModel', 'Model');
/**
 * ItemGeneral Model
 *
 */
class ItemGeneral extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $belongsTo = array(
		'InventoryItemCategory' => array(
			'className' => 'InventoryItemCategory',
			'foreignKey' => false,
			'conditions' => array('InventoryItemCategory.code = ItemGeneral.item_group'),
			'fields' => '',
			'order' => '' 
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => false,
			'conditions' => array('GeneralUnit.name = ItemGeneral.unit'),
			'fields' => '',
			'order' => '' 
		)
	);
}
