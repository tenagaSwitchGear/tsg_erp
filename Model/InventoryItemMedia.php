<?php
App::uses('AppModel', 'Model');
/**
 * InventoryItem Model
 *
 * @property InventoryItemCategory $InventoryItemCategory
 * @property GeneralUnit $GeneralUnit
 * @property BomSaleItem $BomSaleItem
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 * @property InventoryStock $InventoryStock
 * @property InventorySupplierItem $InventorySupplierItem
 * @property ProjectBomItem $ProjectBomItem
 * @property SaleBomItem $SaleBomItem
 */
class InventoryItemMedia extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'name' => array(
                'fields' => array(
                    'dir' => 'files/inventoryItems'
                )
            )
        )
    );

    public $useTable = 'inventory_item_medias';

/**
 * Validation rules
 *
 * @var array
 */
public $validate = array( 
		  
		'name' => array(
			'name' => array(
				
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' =>  array('allowEmpty'=>true)
				//'message' => 'Please supply a valid image.',
			),
		),
	);
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
