<?php
App::uses('AppModel', 'Model');
/**
 * InventoryPurchaseOrder Model
 *
 * @property InventorySupplier $InventorySupplier
 * @property User $User
 * @property InventoryDeliveryOrder $InventoryDeliveryOrder
 * @property InventoryPayment $InventoryPayment
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 */
class InventoryDeliveryLocation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryPurchaseOrder' => array(
			'className' => 'InventoryPurchaseOrder',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
