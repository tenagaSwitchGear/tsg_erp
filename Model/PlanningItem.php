<?php
App::uses('AppModel', 'Model');
/**
 * PlanningItem Model
 *
 * @property Planning $Planning
 * @property InventoryItem $InventoryItem
 * @property GeneralUnit $GeneralUnit
 * @property InventorySupplier $InventorySupplier
 */
class PlanningItem extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Planning' => array(
			'className' => 'Planning',
			'foreignKey' => 'planning_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'GeneralCurrency' => array(
			'className' => 'GeneralCurrency',
			'foreignKey' => 'general_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'PlanningUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'planning_general_unit',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplierItem' => array(
			'className' => 'InventorySupplierItem',
			'foreignKey' => 'inventory_supplier_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'PlanningPartialDelivery' => array(
			'className' => 'PlanningPartialDelivery',
			'foreignKey' => 'planning_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		) 
	);
}
