<?php
App::uses('AppModel', 'Model');
/**
 * FinishedGoodPackagingItem Model
 *
 * @property FinishedGoodPackaging $FinishedGoodPackaging
 * @property FinishedGood $FinishedGood
 * @property ProductionOrder $ProductionOrder
 */
class FinishedGoodPackagingItem extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'FinishedGoodPackaging' => array(
			'className' => 'FinishedGoodPackaging',
			'foreignKey' => 'finished_good_packaging_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGood' => array(
			'className' => 'FinishedGood',
			'foreignKey' => 'finished_good_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJobItem' => array(
			'className' => 'SaleJobItem',
			'foreignKey' => 'sale_job_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
