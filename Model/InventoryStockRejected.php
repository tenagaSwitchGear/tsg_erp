<?php
App::uses('AppModel', 'Model');
/**
 * InventoryStockRejected Model
 *
 * @property InventoryStock $InventoryStock
 */
class InventoryStockRejected extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		/*'inventory_stock_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryDeliveryOrderItem' => array(
			'className' => 'InventoryDeliveryOrderItem',
			'foreignKey' => 'inventory_delivery_order_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnitConverter' => array(
			'className' => 'GeneralUnitConverter',
			'foreignKey' => 'general_unit_converter_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGoodComment' => array(
			'className' => 'FinishedGoodComment',
			'foreignKey' => 'finished_good_comment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
