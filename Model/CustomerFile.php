<?php
App::uses('AppModel', 'Model');
/**
 * CustomerFile Model
 *
 * @property Customer $Customer
 */
class CustomerFile extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'filename' => array(  
                'fields' => array(
                    'dir' => 'dir'
                )
            )
        ) 
    );
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'customer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'filename' => array(
			'filename' => array( 
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' =>  array('allowEmpty'=>true)
				//'message' => 'Please supply a valid image.',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
