<?php
App::uses('AppModel', 'Model');
/**
 * GeneralUnit Model
 *
 * @property BomSaleItem $BomSaleItem
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property InventoryItem $InventoryItem
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 * @property InventoryPurchaseRequisitionItem $InventoryPurchaseRequisitionItem
 * @property InventoryStock $InventoryStock
 * @property InventorySupplierItem $InventorySupplierItem
 * @property ProjectBomItem $ProjectBomItem
 * @property SaleBomItem $SaleBomItem
 */
class GeneralUnit extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		/*'BomSaleItem' => array(
			'className' => 'BomSaleItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),*/
		'InventoryDeliveryOrderItem' => array(
			'className' => 'InventoryDeliveryOrderItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryPurchaseOrderItem' => array(
			'className' => 'InventoryPurchaseOrderItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryPurchaseRequisitionItem' => array(
			'className' => 'InventoryPurchaseRequisitionItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryStock' => array(
			'className' => 'InventoryStock',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventorySupplierItem' => array(
			'className' => 'InventorySupplierItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjectBomItem' => array(
			'className' => 'ProjectBomItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
		/*,
		'SaleBomItem' => array(
			'className' => 'SaleBomItem',
			'foreignKey' => 'general_unit_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		) */
	);

}
