<?php
App::uses('AppModel', 'Model');
/**
 * InventoryDeliveryOrderItem Model
 *
 * @property InventoryDeliveryOrder $InventoryDeliveryOrder
 * @property InventoryItem $InventoryItem
 * @property GeneralUnit $GeneralUnit
 * @property GeneralTax $GeneralTax
 * @property InventoryStock $InventoryStock
 */
class InventoryDeliveryOrderItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array( 
		'inventory_delivery_order_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'inventory_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'general_unit_id' => array( 
		
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array( 
		'InventoryDeliveryOrder' => array(
			'className' => 'InventoryDeliveryOrder',
			'foreignKey' => 'inventory_delivery_order_id', 
			'conditions' => '',
			'fields' => '',
			'order' => '',	
		), 
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',	
		),  
		'InventoryRack' => array(
			'className' => 'InventoryRack',
			'foreignKey' => 'rack_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',	
		), 
		'InventoryStore' => array(
			'className' => 'InventoryStore',
			'foreignKey' => 'store_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',	
		), 
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplierItem' => array(
			'className' => 'InventorySupplierItem',
			'foreignKey' => 'inventory_supplier_item_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGoodComment' => array(
			'className' => 'FinishedGoodComment',
			'foreignKey' => 'finished_good_comment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	); 

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryStock' => array(
			'className' => 'InventoryStock',
			'foreignKey' => 'inventory_delivery_order_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
} 
