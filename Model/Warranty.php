<?php
App::uses('AppModel', 'Model');
/**
 * InventoryItem Model
 *
 * @property InventoryItemCategory $InventoryItemCategory
 * @property GeneralUnit $GeneralUnit
 * @property BomSaleItem $BomSaleItem
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 * @property InventoryStock $InventoryStock
 * @property InventorySupplierItem $InventorySupplierItem
 * @property ProjectBomItem $ProjectBomItem
 * @property SaleBomItem $SaleBomItem
 */
class Warranty extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		
	);

}
