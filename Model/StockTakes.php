<?php
App::uses('AppModel', 'Model');
/**
 * Country Model
 *
 * @property City $City
 * @property State $State
 * @property Website $Website 
 */

class StockTakes extends AppModel {
    

    public $belongsTo = array(
        'InventoryLocations' => array(
            'className' => 'InventoryLocation',
            'foreignKey' => 'inventory_location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'InventoryStores' => array(
            'className' => 'InventoryStore',
            'foreignKey' => 'inventory_store_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    public $hasMany = array(
        
		'StockTakeItems' => array(
            'className' => 'StockTakeItems',
            'foreignKey' => 'stock_take_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),

	);

}
