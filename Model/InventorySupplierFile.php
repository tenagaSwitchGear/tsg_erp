<?php
App::uses('AppModel', 'Model');
/**
 * InventorySupplierFile Model
 *
 * @property InventorySupplier $InventorySupplier
 */
class InventorySupplierFile extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'file' => array(  
                'fields' => array(
                    'dir' => 'dir'
                )
            )
        )
    ); 
 

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'inventory_supplier_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here', 
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'file' => array(
			'file' => array(
				
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' =>  array('allowEmpty'=>true)
				//'message' => 'Please supply a valid image.',
			),
		),
		'subject' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
