<?php
App::uses('AppModel', 'Model');
/**
 * InventoryPurchaseRequisition Model
 *
 * @property User $User
 * @property InternalDepartment $InternalDepartment
 * @property GeneralPurchaseRequisitionType $GeneralPurchaseRequisitionType
 * @property InventoryPurchaseRequisitionBom $InventoryPurchaseRequisitionBom
 * @property InventoryPurchaseRequisitionItem $InventoryPurchaseRequisitionItem
 */
class InventoryPurchaseRequisition extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
        'Upload.Upload' => array(
            'quotation' => array(  
                'fields' => array(
                    'dir' => 'dir'
                )
            ),
            'approval' => array(  
                'fields' => array(
                    'dir' => 'approval_dir'
                )
            )
        )
    );
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),  
		'group_id' => array( 
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_purchase_requisition_type_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'quotation' => array(
			'quotation' => array( 
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' => true
				//'message' => 'Please supply a valid image.',
			),
		),
		'approval' => array(
			'approval' => array( 
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' => true
				//'message' => 'Please supply a valid image.',
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'AccountDepartmentBudget' => array(
			'className' => 'AccountDepartmentBudget',
			'foreignKey' => 'account_department_budget_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TermOfPayment' => array(
			'className' => 'TermOfPayment',
			'foreignKey' => 'term_of_payment_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TermOfDelivery' => array(
			'className' => 'TermOfDelivery',
			'foreignKey' => 'term_of_delivery_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Remark' => array(
			'className' => 'Remark',
			'foreignKey' => 'remark_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Warranty' => array(
			'className' => 'Warranty',
			'foreignKey' => 'warranty_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',  
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_child_id',  
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralPurchaseRequisitionType' => array(
			'className' => 'GeneralPurchaseRequisitionType',
			'foreignKey' => 'general_purchase_requisition_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array( 
		'InventoryPurchaseRequisitionItem' => array(
			'className' => 'InventoryPurchaseRequisitionItem',
			'foreignKey' => 'inventory_purchase_requisition_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
