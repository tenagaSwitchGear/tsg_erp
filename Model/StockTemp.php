<?php
App::uses('AppModel', 'Model');
/**
 * Country Model
 *
 * @property City $City
 * @property State $State
 * @property Website $Website 
 */
class StockTemp extends AppModel {
 
 
    public $belongsTo = array(
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => false,
			'conditions' => array('InventoryItem.code = StockTemp.code'),
			'fields' => '',
			'order' => ''
		),
		'InventoryStore' => array(
			'className' => 'InventoryStore',
			'foreignKey' => false,
			'conditions' => array('InventoryStore.name = StockTemp.store'),
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		) 
	);

}
