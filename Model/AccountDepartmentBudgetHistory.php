<?php
App::uses('AppModel', 'Model');
/**
 * AccountDepartmentBudgetHistory Model
 *
 * @property AccountDepartmentBudget $AccountDepartmentBudget
 * @property InventoryPurchaseOrder $InventoryPurchaseOrder
 * @property User $User
 */
class AccountDepartmentBudgetHistory extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'account_department_budget_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_purchase_order_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'end_user' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AccountDepartmentBudget' => array(
			'className' => 'AccountDepartmentBudget',
			'foreignKey' => 'account_department_budget_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryPurchaseOrder' => array(
			'className' => 'InventoryPurchaseOrder',
			'foreignKey' => 'inventory_purchase_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'EndUser' => array(
			'className' => 'User',
			'foreignKey' => false,
			'conditions' => array('AccountDepartmentBudgetHistory.end_user = EndUser.id'),
			'fields' => '',
			'order' => ''
		)
	);
}
