<?php
App::uses('AppModel', 'Model');
/**
 * SaleQuotation Model
 *
 * @property SaleTender $SaleTender
 * @property Customer $Customer
 * @property SaleBom $SaleBom
 */
class SaleQuotation extends AppModel {

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array( 
		'name' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Quotation No.',
			) 
		),
		'customer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please enter Customer',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'QuotationApproval' => array(
			'className' => 'User',
			'foreignKey' => false,
			'conditions' => array('SaleQuotation.user_approval = QuotationApproval.id'),
			'fields' => '',
			'order' => ''
		),
		'SaleTender' => array(
			'className' => 'SaleTender',
			'foreignKey' => 'sale_tender_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array( 
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array( 
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bom' => array( 
			'className' => 'Bom',
			'foreignKey' => 'bom_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array( 
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TermOfPayment' => array( 
			'className' => 'TermOfPayment',
			'foreignKey' => 'term_of_payment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)   
	);

	/*
	public $hasOne = array(
		'SaleJob' => array(
			'className' => 'SaleJob',
			'foreignKey' => 'sale_quotation_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);*/

	public $hasMany = array(
		'SaleBom' => array(
			'className' => 'SaleBom',
			'foreignKey' => 'sale_quotation_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'SaleQuotationItem' => array(
			'className' => 'SaleQuotationItem',
			'foreignKey' => 'sale_quotation_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		) 
	);

	public function beforeSave($options = array()) {
        if(!empty($this->data['SaleQuotation']['created'])) {
			$this->data['SaleQuotation']['created'] = date('Y-m-d H:i:s');
			return true;
		}

		if(!empty($this->data['SaleQuotation']['modified'])) {
			$this->data['SaleQuotation']['modified'] = date('Y-m-d H:i:s');
			return true;
		}
	}
}
