<?php
App::uses('AppModel', 'Model');
/**
 * City Model
 *
 * @property State $State
 * @property Country $Country
 * @property Website $Website
 *
 * Company : W3itexperts
 * Author : Rahul Dev Sharma
 * Script Name : CakeReady - Ready Admin With Authentication & ACL Management Plugin
 * Created On : 31 December 2015
 * Script Version : CakeReady v 0.1
 * CakePHP Version : 2.5
 *
 *
 * Website: http://www.w3itexperts.com/
 * Contact: admin@w3itexperts.com
 * Follow: www.twitter.com/w3itexperts
 * Like: www.facebook.com/w3itexperts
 */
class TermOfPayment extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
	
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

 
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryPurchaseRequisition' => array(
			'className' => 'InventoryPurchaseRequisition',
			'foreignKey' => 'term_of_payment_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
