<?php
App::uses('AppModel', 'Model');
/**
 * SaleOrderItem Model
 *
 * @property GeneralUnit $GeneralUnit
 * @property Product $Product
 * @property SaleOrder $SaleOrder
 */
class SaleOrderItem extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleOrder' => array(
			'className' => 'SaleOrder',
			'foreignKey' => 'sale_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bom' => array(
			'className' => 'Bom',
			'foreignKey' => 'bom_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasOne = array(
		
	);
}
