<?php
App::uses('AppModel', 'Model');
/**
 * FinishedGoodDeliveryItem Model
 *
 * @property FinishedGoodDelivery $FinishedGoodDelivery
 * @property FinishedGood $FinishedGood
 * @property SaleOrderItem $SaleOrderItem
 * @property SaleQuotationItem $SaleQuotationItem
 * @property InventoryItem $InventoryItem
 * @property User $User
 */
class DeliveryNoteItem extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'DeliveryNote' => array(
			'className' => 'DeliveryNote',
			'foreignKey' => 'delivery_note_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleOrderItem' => array(
			'className' => 'SaleOrderItem',
			'foreignKey' => 'sale_order_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
