<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 * @property CustomerStation $CustomerStation
 * @property State $State
 * @property Country $Country
 * @property CustomerType $CustomerType
 * @property CustomerBusinessCategory $CustomerBusinessCategory
 * @property User $User
 * @property CustomerOwnership $CustomerOwnership
 * @property CustomerContactPerson $CustomerContactPerson
 * @property CustomerFile $CustomerFile
 * @property SaleQuotation $SaleQuotation
 * @property SaleTender $SaleTender
 */
class AccountDepartmentBudgets extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AccountDepartment' => array(
			'className' => 'AccountDepartment',
			'foreignKey' => 'account_department_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		/*'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerType' => array(
			'className' => 'CustomerType',
			'foreignKey' => 'customer_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerBusinessCategory' => array(
			'className' => 'CustomerBusinessCategory',
			'foreignKey' => 'customer_business_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerOwnership' => array(
			'className' => 'CustomerOwnership',
			'foreignKey' => 'customer_ownership_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)*/
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		/*'CustomerFile' => array(
			'className' => 'CustomerFile',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'SaleQuotation' => array(
			'className' => 'SaleQuotation',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'SaleTender' => array(
			'className' => 'SaleTender',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)*/
	);

}
