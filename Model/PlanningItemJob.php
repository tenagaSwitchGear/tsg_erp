<?php
App::uses('AppModel', 'Model');
/**
 * PlanningItemJob Model
 *
 * @property SaleJobChild $SaleJobChild
 * @property PlanningItem $PlanningItem
 * @property Planning $Planning
 * @property GeneralCurrency $GeneralCurrency
 */
class PlanningItemJob extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'sale_job_child_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'planning_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'planning_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'quantity' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_currency_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PlanningItem' => array(
			'className' => 'PlanningItem',
			'foreignKey' => 'planning_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Planning' => array(
			'className' => 'Planning',
			'foreignKey' => 'planning_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralCurrency' => array(
			'className' => 'GeneralCurrency',
			'foreignKey' => 'general_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
