<?php
App::uses('AppModel', 'Model');
/**
 * SaleInvoiceItem Model
 *
 * @property SaleInvoice $SaleInvoice
 * @property SaleOrderItem $SaleOrderItem
 * @property GeneralUnit $GeneralUnit
 * @property FinishedGood $FinishedGood
 */
class SaleInvoiceItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'sale_invoice_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sale_order_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_unit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SaleInvoice' => array(
			'className' => 'SaleInvoice',
			'foreignKey' => 'sale_invoice_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleOrderItem' => array(
			'className' => 'SaleOrderItem',
			'foreignKey' => 'sale_order_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGood' => array(
			'className' => 'FinishedGood',
			'foreignKey' => 'finished_good_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
