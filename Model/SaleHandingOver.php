<?php
App::uses('AppModel', 'Model');
/**
 * SaleHandingOver Model
 *
 * @property SaleQuotation $SaleQuotation
 * @property Customer $Customer
 * @property SaleOrder $SaleOrder
 * @property SaleJob $SaleJob
 * @property User $User
 * @property SaleHandingOverUser $SaleHandingOverUser
 */
class SaleHandingOver extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'sale_quotation_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'customer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'sale_job_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		'status' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'date_hod' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SaleQuotation' => array(
			'className' => 'SaleQuotation',
			'foreignKey' => 'sale_quotation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleOrder' => array(
			'className' => 'SaleOrder',
			'foreignKey' => 'sale_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'SaleJob' => array(
			'className' => 'SaleJob',
			'foreignKey' => 'sale_job_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleOrder' => array(
			'className' => 'SaleOrder',
			'foreignKey' => 'sale_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'SaleHandingOverUser' => array(
			'className' => 'SaleHandingOverUser',
			'foreignKey' => 'sale_handing_over_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}