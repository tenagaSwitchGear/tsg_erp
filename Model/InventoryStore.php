<?php
App::uses('AppModel', 'Model');
/**
 * InventoryStore Model
 *
 * @property InventoryLocation $InventoryLocation
 */
class InventoryStore extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'inventory_location_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array( 
		//'InventoryDeliveryLocations' => array(
		//	'className' => 'InventoryDeliveryLocations',
		//	'foreignKey' => 'inventory_delivery_location_id', 
		'InventoryLocation' => array(
			'className' => 'InventoryLocation',
			'foreignKey' => 'inventory_location_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $actsAs = array('Containable');
	/*
	public $hasAndBelongsToMany = array(
	    'InventoryStore' =>
	        array(
	            'className' => 'InventoryStore',
	            'joinTable' => 'inventory_items',
	            'foreignKey' => 'id',
	            'associationForeignKey' => 'inventory_store_id',
	            'unique' => true,
	            'fields' => array('InventoryStore.name'),
	            'limit' => 1
	        )
	);*/
}
