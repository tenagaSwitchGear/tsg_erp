<?php
App::uses('AppModel', 'Model');
/**
 * PlanningItem Model
 *
 * @property Planning $Planning
 * @property InventoryItem $InventoryItem
 * @property GeneralUnit $GeneralUnit
 * @property InventorySupplier $InventorySupplier
 */
class PlanningPartialDelivery extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Planning' => array(
			'className' => 'Planning',
			'foreignKey' => 'planning_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PlanningItem' => array(
			'className' => 'PlanningItem',
			'foreignKey' => 'planning_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		) 
	);
}
