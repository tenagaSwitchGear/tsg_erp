<?php
App::uses('AppModel', 'Model');
/**
 * InventoryStock Model
 *
 * @property GeneralUnit $GeneralUnit
 * @property InventoryItem $InventoryItem
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property GeneralUnitConverter $GeneralUnitConverter
 * @property InventoryLocation $InventoryLocation
 * @property InventoryStore $InventoryStore
 * @property InventoryRack $InventoryRack
 * @property InventoryDeliveryLocation $InventoryDeliveryLocation
 * @property FinishedGood $FinishedGood
 * @property InventoryMaterialHistory $InventoryMaterialHistory
 * @property InventoryMaterialIssued $InventoryMaterialIssued
 */
class InventoryStock extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'general_unit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'inventory_location_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'location_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'location_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_store_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'receive_date' => array(
            'rule' => 'date',
            'message' => 'Enter a valid Receive Date' 
        )
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryDeliveryOrderItem' => array(
			'className' => 'InventoryDeliveryOrderItem',
			'foreignKey' => 'inventory_delivery_order_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnitConverter' => array(
			'className' => 'GeneralUnitConverter',
			'foreignKey' => 'general_unit_converter_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryLocation' => array(
			'className' => 'InventoryLocation',
			'foreignKey' => 'inventory_location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryStore' => array(
			'className' => 'InventoryStore',
			'foreignKey' => 'inventory_store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryRack' => array(
			'className' => 'InventoryRack',
			'foreignKey' => 'inventory_rack_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),  
		'InventoryRack' => array(
			'className' => 'InventoryRack',
			'foreignKey' => 'inventory_rack_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'FinishedGood' => array(
			'className' => 'FinishedGood',
			'foreignKey' => 'finished_good_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryDeliveryLocation' => array(
			'className' => 'InventoryDeliveryLocation',
			'foreignKey' => 'inventory_delivery_location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGood' => array(
			'className' => 'FinishedGood',
			'foreignKey' => 'finished_good_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryMaterialHistory' => array(
			'className' => 'InventoryMaterialHistory',
			'foreignKey' => 'inventory_stock_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryMaterialIssued' => array(
			'className' => 'InventoryMaterialIssued',
			'foreignKey' => 'inventory_stock_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
