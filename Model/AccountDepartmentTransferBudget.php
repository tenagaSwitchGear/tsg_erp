<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 * @property CustomerStation $CustomerStation
 * @property State $State
 * @property Country $Country
 * @property CustomerType $CustomerType
 * @property CustomerBusinessCategory $CustomerBusinessCategory
 * @property User $User
 * @property CustomerOwnership $CustomerOwnership
 * @property CustomerContactPerson $CustomerContactPerson
 * @property CustomerFile $CustomerFile
 * @property SaleQuotation $SaleQuotation
 * @property SaleTender $SaleTender
 */
class AccountDepartmentTransferBudget extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'GroupFrom' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id_from',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GroupsTo' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id_to',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AccountDepartmentBudgetTo' => array(
			'className' => 'AccountDepartmentBudgets',
			'foreignKey' => 'account_department_budget_id_to',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AccountDepartmentBudgetFrom' => array(
			'className' => 'AccountDepartmentBudgets',
			'foreignKey' => 'account_department_budget_id_from',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Request_by' => array(
			'className' => 'User',
			'foreignKey' => 'request_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Recommended_by' => array(
			'className' => 'User',
			'foreignKey' => 'recommended_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Review_by' => array(
			'className' => 'User',
			'foreignKey' => 'review_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Verify_by' => array(
			'className' => 'User',
			'foreignKey' => 'verify_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Approved_by' => array(
			'className' => 'User',
			'foreignKey' => 'approved_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		/*'User' => array(
			'className' => 'User',
			'foreignKey' => 'id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)*/
	);

	public $hasOne = array(
		/*'GroupsTo' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id_to',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'AccountDepartmentBudgets' => array(
			'className' => 'AccountDepartmentBudgets',
			'foreignKey' => 'account_department_budget_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'SaleTender' => array(
			'className' => 'SaleTender',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)*/
	);

}
