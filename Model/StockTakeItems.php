<?php
App::uses('AppModel', 'Model');
/**
 * Country Model
 *
 * @property City $City
 * @property State $State
 * @property Website $Website 
 */
class StockTakeItems extends AppModel {
    
    /*
    public $hasMany = array( 
        'StockTakeReasons' => array(
            'className' => 'StockTakeReasons',
            'foreignKey' => 'stock_take_reason_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ) 
    ); */
 
    public $belongsTo = array(
		'StockTakes' => array(
            'className' => 'StockTakes',
            'foreignKey' => 'stock_take_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'StockTakeReasons' => array(
            'className' => 'StockTakeReasons',
            'foreignKey' => 'stock_take_reason_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'InventoryItem' => array(
            'className' => 'InventoryItem',
            'foreignKey' => 'inventory_item_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'InventoryRack' => array(
            'className' => 'InventoryRack',
            'foreignKey' => 'inventory_rack_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )    
	);

}
