<?php
App::uses('AppModel', 'Model');
/**
 * ProjectBom Model
 *
 * @property Bom $Bom
 * @property SaleTender $SaleTender
 * @property SaleOrder $SaleOrder
 * @property SaleOrderChild $SaleOrderChild
 * @property User $User
 * @property InventoryPurchaseRequisitionBom $InventoryPurchaseRequisitionBom
 * @property ProjectBomChild $ProjectBomChild
 * @property ProjectBomItem $ProjectBomItem
 * @property ProjectSchedule $ProjectSchedule
 */
class SaleJobItem extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'bom_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'remark' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sale_tender_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sale_job_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sale_job_child_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array( 
		'SaleTender' => array(
			'className' => 'SaleTender',
			'foreignKey' => 'sale_tender_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJob' => array(
			'className' => 'SaleJob',
			'foreignKey' => 'sale_job_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_child_id',
			'conditions' => '',
			'fields' => '', 
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bom' => array(
			'className' => 'Bom',
			'foreignKey' => 'bom_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryPurchaseRequisitionBom' => array(
			'className' => 'InventoryPurchaseRequisitionBom',
			'foreignKey' => 'project_bom_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjectBomChild' => array(
			'className' => 'ProjectBomChild',
			'foreignKey' => 'project_bom_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjectBomItem' => array(
			'className' => 'ProjectBomItem',
			'foreignKey' => 'project_bom_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjectSchedule' => array(
			'className' => 'ProjectSchedule',
			'foreignKey' => 'project_bom_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
