<?php
App::uses('AppModel', 'Model');
/**
 * ProductionOrderChild Model
 *
 * @property ProductionOrder $ProductionOrder
 */
class FinishedGoodRemark extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'attachment' => array(  
                'fields' => array(
                    'dir' => 'attachment_dir'
                )
            )
        )
    ); 

    public $validate = array( 
		'attachment' => array(
			'attachment' => array(
				
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' =>  array('allowEmpty'=>true)
				//'message' => 'Please supply a valid image.',
			),
		) 
	);

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'finished_goods_remarks';
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'FinishedGood' => array(
			'className' => 'FinishedGood',
			'foreignKey' => 'finished_good_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGoodComment' => array(
			'className' => 'FinishedGoodComment',
			'foreignKey' => 'finished_good_comment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
