<?php
App::uses('AppModel', 'Model');
/**
 * ProjectDrawingTracking Model
 *
 * @property ProjectDrawing $ProjectDrawing
 */
class ProjectDrawingTracking extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'project_drawing_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please select status',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array( 
		'ProjectDrawing' => array(
			'className' => 'ProjectDrawing',
			'foreignKey' => 'project_drawing_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjectDrawingFile' => array(
			'className' => 'ProjectDrawingFile',
			'foreignKey' => 'project_drawing_file_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
