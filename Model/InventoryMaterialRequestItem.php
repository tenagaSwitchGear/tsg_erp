<?php
App::uses('AppModel', 'Model');
/**
 * InventoryMaterialRequestItem Model
 *
 * @property InventoryMaterialRequest $InventoryMaterialRequest
 * @property InventoryItem $InventoryItem
 * @property GeneralUnit $GeneralUnit
 * @property InventoryMaterialIssued $InventoryMaterialIssued
 */
class InventoryMaterialRequestItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'inventory_material_request_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_unit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProductionOrderItem' => array(
			'className' => 'ProductionOrderItem',
			'foreignKey' => 'production_order_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProductionOrderChild' => array(
			'className' => 'ProductionOrderChild',
			'foreignKey' => 'production_order_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryMaterialRequest' => array(
			'className' => 'InventoryMaterialRequest',
			'foreignKey' => 'inventory_material_request_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		) 
	);

	 

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryMaterialIssued' => array(
			'className' => 'InventoryMaterialIssued',
			'foreignKey' => 'inventory_material_request_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
		/*,
		'InventoryStock' => array(
			'className' => 'InventoryStock',
			'foreignKey' => false,  
			'conditions' => array('InventoryMaterialRequestItem.inventory_item_id = InventoryStock.inventory_item_id'),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)*/

	); 
/*
	public $hasAndBelongsToMany = array(
	    'InventoryStore' =>
	        array(
	            'className' => 'InventoryStore',
	            'joinTable' => 'inventory_items',
	            'foreignKey' => 'id',
	            'associationForeignKey' => 'inventory_store_id',
	            'unique' => true,
	            'fields' => array('InventoryStore.name'),
	            'limit' => 1
	        )
	);
    
    
	"SELECT `InventoryStore`.`name`, `InventoryItem`.`id`, `InventoryItem`.`name`, `InventoryItem`.`quantity`, `InventoryItem`.`unit_price`, `InventoryItem`.`inventory_item_category_id`, `InventoryItem`.`general_unit_id`, `InventoryItem`.`code`, `InventoryItem`.`qc_inspect`, `InventoryItem`.`quantity_reserved`, `InventoryItem`.`quantity_available`, `InventoryItem`.`quantity_shortage`, `InventoryItem`.`status`, `InventoryItem`.`general_currency_id`, `InventoryItem`.`note`, `InventoryItem`.`inventory_location_id`, `InventoryItem`.`inventory_store_id` FROM `erptsg_dev`.`inventory_stores` AS `InventoryStore` JOIN `erptsg_dev`.`inventory_items` AS `InventoryItem` ON (`InventoryItem`.`id` IN (30, 31, 32) AND `InventoryItem`.`inventory_store_id` = `InventoryStore`.`id`)"

	
	public $hasAndBelongsToMany = array(
        'Items' =>
            array(
                'className' => 'InventoryItem',
                'joinTable' => 'inventory_stores',
                'foreignKey' => 'inventory_store_id',
                'associationForeignKey' => 'id',
                'unique' => false,
                'conditions' => '',
                'fields' => '',
                'order' => '',
                'limit' => '',
                'offset' => '',
                'finderQuery' => '',
                'with' => ''
            )
    );
	*/
}
