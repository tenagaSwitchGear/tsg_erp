<?php
App::uses('AppModel', 'Model');
/**
 * InventorySupplierCategory Model
 *
 * @property InventorySupplier $InventorySupplier
 * @property InventoryItemCategory $InventoryItemCategory
 */
class InventorySupplierCategory extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'inventory_supplier_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_item_category_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItemCategory' => array(
			'className' => 'InventoryItemCategory',
			'foreignKey' => 'inventory_item_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
