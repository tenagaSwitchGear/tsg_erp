<?php
App::uses('AppModel', 'Model');
/**
 * InventoryPurchaseRequisitionItem Model
 *
 * @property InventoryPurchaseRequisition $InventoryPurchaseRequisition
 * @property GeneralUnit $GeneralUnit
 */
class InventoryPurchaseRequisitionItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'inventory_purchase_requisition_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Purchase requisition not found',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Item id not found.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_unit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please select Unit for each items',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventorySupplierItem' => array(
			'className' => 'InventorySupplierItem',
			'foreignKey' => 'inventory_supplier_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryPurchaseRequisition' => array(
			'className' => 'InventoryPurchaseRequisition',
			'foreignKey' => 'inventory_purchase_requisition_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralCurrency' => array(
			'className' => 'GeneralCurrency',
			'foreignKey' => 'general_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
