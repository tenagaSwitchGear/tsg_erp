<?php
App::uses('AppModel', 'Model');
/**
 * InventoryItem Model
 *
 * @property InventoryItemCategory $InventoryItemCategory
 * @property GeneralUnit $GeneralUnit
 * @property BomSaleItem $BomSaleItem
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 * @property InventoryStock $InventoryStock
 * @property InventorySupplierItem $InventorySupplierItem
 * @property ProjectBomItem $ProjectBomItem
 * @property SaleBomItem $SaleBomItem

  $data = $this->State->query("SELECT p.id AS p_id, s.name AS region, SUM(h.bau) AS bau, SUM(h.lcs) AS lcs FROM 
            states AS s
            RIGHT OUTER JOIN projects AS p ON(s.id = p.state_id)
            RIGHT OUTER JOIN project_populations AS h ON(h.project_id = p.id) 
            WHERE p.status = '1' GROUP BY s.id");
            
 */
class InventoryItem extends AppModel {  

	public $actsAs = array(
        'Upload.Upload' => array(
            'attachment' => array(  
                'fields' => array(
                    'dir' => 'attachment_dir'
                )
            ),
            'deleteOnUpdate' => true,
            'deleteFolderOnDelete' => true,
            'mode' => 0777 
        ) 
    );
 

	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_item_category_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_unit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Please select Unit',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Item Code.',
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This code already exist.',
			)
		), 
		'attachment' => array(
			'attachment' => array(
				
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' => true
				//'message' => 'Please supply a valid image.',
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryItemCategory' => array(
			'className' => 'InventoryItemCategory',
			'foreignKey' => 'inventory_item_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryDeliveryLocation' => array(
			'className' => 'InventoryDeliveryLocation',
			'foreignKey' => 'inventory_delivery_location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryLocation' => array(
			'className' => 'InventoryLocation',
			'foreignKey' => 'inventory_location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryStore' => array(
			'className' => 'InventoryStore',
			'foreignKey' => 'inventory_store_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryRack' => array(
			'className' => 'InventoryRack',
			'foreignKey' => 'inventory_rack_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
 
		'BomItem' => array(
			'className' => 'BomItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryDeliveryOrderItem' => array(
			'className' => 'InventoryDeliveryOrderItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryPurchaseOrderItem' => array(
			'className' => 'InventoryPurchaseOrderItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryStock' => array(
			'className' => 'InventoryStock',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		), 
		'InventorySupplierItem' => array(
			'className' => 'InventorySupplierItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjectBomItem' => array(
			'className' => 'ProjectBomItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'SaleBomItem' => array(
			'className' => 'SaleBomItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),  
		'InventoryMaterialRequestItem' => array(
			'className' => 'InventoryMaterialRequestItem', 
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		), 
		'InventoryItemMedia' => array(
			'className' => 'InventoryItemMedia', 
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
