<?php
App::uses('AppModel', 'Model');
/**
 * ProductionOrderChild Model
 *
 * @property ProductionOrder $ProductionOrder
 */
class ProductionOrderItem extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'production_order_items';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'production_order_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'production_order_child_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProductionOrder' => array(
			'className' => 'ProductionOrder',
			'foreignKey' => 'production_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProductionOrderChild' => array(
			'className' => 'ProductionOrderChild',
			'foreignKey' => 'production_order_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
/**
 * hasMany associations
 *
 * @var array
 
	public $hasMany = array(
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);*/
}
