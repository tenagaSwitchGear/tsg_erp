<?php
App::uses('AppModel', 'Model');
/**
 * SaleTenderType Model
 *
 * @property SaleTender $SaleTender
 */
class SaleTenderType extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'SaleTender' => array(
			'className' => 'SaleTender',
			'foreignKey' => 'sale_tender_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
