<?php
App::uses('AppModel', 'Model');
/**
 * SaleJob Model
 *
 * @property SaleTender $SaleTender
 * @property Customer $Customer
 * @property CustomerStation $CustomerStation
 * @property User $User
 * @property SaleJobChild $SaleJobChild
 * @property SaleOrder $SaleOrder
 */
class SaleJob extends AppModel {

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Job No.',
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This Job No already exists.',
			) 
		),
		'sale_tender_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sale_quotation_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'date_fat' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
		'date_delivery' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		) 
	);

	/*public $hasAndBelongsToMany = array(
	    'ProjectBomItem' => array(
	        'className' => 'ProjectBomItem',
	        'joinTable' => 'project_bom_items',
	        'foreignKey' => 'company_id',
	        'associationForeignKey' => 'product_id',
	        'unique' => true,
	    )
	);*/

	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleTender' => array(
			'className' => 'SaleTender',
			'foreignKey' => 'sale_tender_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleQuotation' => array(
			'className' => 'SaleQuotation',
			'foreignKey' => 'sale_quotation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),   
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array( 
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjectBom' => array(
			'className' => 'ProjectBom',
			'foreignKey' => 'sale_job_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
