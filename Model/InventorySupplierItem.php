<?php
App::uses('AppModel', 'Model');
/**
 * InventorySupplierItem Model
 *
 * @property InventoryItem $InventoryItem
 * @property GeneralUnit $GeneralUnit
 */
class InventorySupplierItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	
	public $actsAs = array(
        'Upload.Upload' => array(
            'quotation' => array(  
                'fields' => array(
                    'dir' => 'dir' 
                )
            )
        )
    );

	public $validate = array( 
		'inventory_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_unit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'quotation' => array(
			'quotation' => array( 
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' => true 
				//'message' => 'Please supply a valid image.',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
 
		), 
		'MinOrderUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'min_order_unit',
			'conditions' => '',
			'fields' => '',
			'order' => '' 
		), 
		'GeneralCurrency' => array(
			'className' => 'GeneralCurrency',
			'foreignKey' => 'general_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
 
		),
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => '' 
		)
	);
}
