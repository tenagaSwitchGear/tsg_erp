<?php
App::uses('AppModel', 'Model');
/**
 * FinishedGoodFatRemark Model
 *
 * @property FinishedGood $FinishedGood
 * @property User $User
 * @property FinishedGoodComment $FinishedGoodComment
 */
class InventoryQcRemark extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $actsAs = array(
        'Upload.Upload' => array(
            'attachment' => array(  
                'fields' => array(
                    'dir' => 'attachment_dir'
                )
            )
        )
    ); 
 

	public $validate = array(

	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryQcItem' => array(
			'className' => 'InventoryQcItem',
			'foreignKey' => 'inventory_qc_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
