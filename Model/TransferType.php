<?php

App::uses('AppModel', 'Model');
 
class TransferType extends AppModel {

 	public $belongsTo = array(
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'transfer_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}