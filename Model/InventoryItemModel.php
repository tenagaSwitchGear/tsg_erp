<?php
App::uses('AppModel', 'Model');
/**
 * InventoryItem Model
 *
 * @property InventoryItemCategory $InventoryItemCategory
 * @property GeneralUnit $GeneralUnit
 * @property BomSaleItem $BomSaleItem
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 * @property InventoryStock $InventoryStock
 * @property InventorySupplierItem $InventorySupplierItem
 * @property ProjectBomItem $ProjectBomItem
 * @property SaleBomItem $SaleBomItem
 */
class InventoryItemMedia extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type' => array(
			'numeric' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'inventory_item_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryItemCategory' => array(
			'className' => 'InventoryItemCategory',
			'foreignKey' => 'inventory_item_category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'BomSaleItem' => array(
			'className' => 'BomSaleItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryDeliveryOrderItem' => array(
			'className' => 'InventoryDeliveryOrderItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryPurchaseOrderItem' => array(
			'className' => 'InventoryPurchaseOrderItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryStock' => array(
			'className' => 'InventoryStock',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventorySupplierItem' => array(
			'className' => 'InventorySupplierItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProjectBomItem' => array(
			'className' => 'ProjectBomItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'SaleBomItem' => array(
			'className' => 'SaleBomItem',
			'foreignKey' => 'inventory_item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
