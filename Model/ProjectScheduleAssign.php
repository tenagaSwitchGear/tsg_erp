<?php
App::uses('AppModel', 'Model');
/**
 * ProjectScheduleAssign Model
 *
 * @property ProjectScheduleChild $ProjectScheduleChild
 * @property ProjectManpower $ProjectManpower
 */
class ProjectScheduleAssign extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'project_schedule_child_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'project_manpower_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProjectScheduleChild' => array(
			'className' => 'ProjectScheduleChild',
			'foreignKey' => 'project_schedule_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjectManpower' => array(
			'className' => 'ProjectManpower',
			'foreignKey' => 'project_manpower_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
