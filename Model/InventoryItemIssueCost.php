<?php
App::uses('AppModel', 'Model');
/**
 * InventoryItemIssueCost Model
 *
 * @property InventoryMaterialRequestItem $InventoryMaterialRequestItem
 * @property ProductionOrder $ProductionOrder
 * @property InventoryItem $InventoryItem
 */
class InventoryItemIssueCost extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryMaterialRequestItem' => array(
			'className' => 'InventoryMaterialRequestItem',
			'foreignKey' => 'inventory_material_request_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProductionOrder' => array(
			'className' => 'ProductionOrder',
			'foreignKey' => 'production_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJobItem' => array(
			'className' => 'SaleJobItem',
			'foreignKey' => 'sale_job_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
