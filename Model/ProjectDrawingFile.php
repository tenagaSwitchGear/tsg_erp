<?php
App::uses('AppModel', 'Model');
/**
 * ProjectDrawingFile Model
 *
 * @property ProjectDrawing $ProjectDrawing
 */
class ProjectDrawingFile extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'name' => array(
                'fields' => array(
                    'dir' => 'directory'
                )
            )
        )
    );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array 
 */
	public $validate = array(
		'project_drawing_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'project_drawing_id',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'name' => array( 
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' =>  array('allowEmpty'=>true)
				//'message' => 'Please supply a valid image.',
			)
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProjectDrawing' => array(
			'className' => 'ProjectDrawing',
			'foreignKey' => 'project_drawing_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
 
}
