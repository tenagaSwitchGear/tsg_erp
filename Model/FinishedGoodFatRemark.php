<?php
App::uses('AppModel', 'Model');
/**
 * FinishedGoodFatRemark Model
 *
 * @property FinishedGood $FinishedGood
 * @property User $User
 * @property FinishedGoodComment $FinishedGoodComment
 */
class FinishedGoodFatRemark extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $actsAs = array(
        'Upload.Upload' => array(
            'attachment' => array(  
                'fields' => array(
                    'dir' => 'attachment_dir'
                )
            )
        )
    ); 
 

	public $validate = array(
		'finished_good_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'remark' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'finished_good_comment_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'attachment' => array(
			'attachment' => array(
				
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' =>  array('allowEmpty'=>true)
				//'message' => 'Please supply a valid image.',
			),
		) 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'FinishedGood' => array(
			'className' => 'FinishedGood',
			'foreignKey' => 'finished_good_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGoodComment' => array(
			'className' => 'FinishedGoodComment',
			'foreignKey' => 'finished_good_comment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
