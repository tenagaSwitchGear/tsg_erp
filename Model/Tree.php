<?php

App::uses('AppModel', 'Model');
 
class Tree extends AppModel {

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'TreeGroup' => array(
			'className' => 'TreeGroup',
			'foreignKey' => 'tree_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		) 
	);
}