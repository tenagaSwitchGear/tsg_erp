<?php
App::uses('AppModel', 'Model');
/**
 * ItemGroup Model
 *
 * @property GeneralCurrency $GeneralCurrency
 */
class ItemGroup extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'GeneralCurrency' => array(
			'className' => 'GeneralCurrency',
			'foreignKey' => 'general_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
