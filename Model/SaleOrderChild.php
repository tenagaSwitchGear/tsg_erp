<?php
App::uses('AppModel', 'Model');
/**
 * SaleOrderChild Model
 *
 * @property SaleJob $SaleJob
 */
class SaleOrderChild extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sale_order_childs';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SaleJob' => array(
			'className' => 'SaleJob',
			'foreignKey' => 'sale_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'ProjectBom' => array(
			'className' => 'ProjectBom',
			'foreignKey' => 'sale_order_child_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		) 
	);
}
