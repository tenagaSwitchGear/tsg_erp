<?php
App::uses('AppModel', 'Model');
/**
 * InventoryPurchaseOrder Model
 *
 * @property InventorySupplier $InventorySupplier
 * @property User $User
 * @property InventoryDeliveryOrder $InventoryDeliveryOrder
 * @property InventoryPayment $InventoryPayment
 * @property InventoryPurchaseOrderItem $InventoryPurchaseOrderItem
 */
class InventoryPurchaseOrder extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'group_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'general_purchase_requisition_type_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryLocation' => array(
			'className' => 'InventoryLocation',
			'foreignKey' => 'inventory_location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'EndUser' => array(
			'className' => 'User',
			'foreignKey' => false,
			'conditions' => array('InventoryPurchaseOrder.end_user = EndUser.id'),
			'fields' => '',
			'order' => ''
		),
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralPurchaseRequisitionType' => array(
			'className' => 'GeneralPurchaseRequisitionType',
			'foreignKey' => 'general_purchase_requisition_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TermOfPayment' => array(
			'className' => 'TermOfPayment',
			'foreignKey' => 'term_of_payment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryDeliveryLocation' => array(
			'className' => 'InventoryDeliveryLocation',
			'foreignKey' => 'inventory_delivery_location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryPurchaseRequisition' => array(
			'className' => 'InventoryPurchaseRequisition',
			'foreignKey' => 'inventory_purchase_requisition_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TermOfDelivery' => array(
			'className' => 'TermOfDelivery',
			'foreignKey' => 'term_of_delivery_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Remark' => array(
			'className' => 'Remark',
			'foreignKey' => 'remark_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Warranty' => array(
			'className' => 'Warranty',
			'foreignKey' => 'warranty_id', 
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array( 
		'InventoryPurchaseOrderItem' => array(
			'className' => 'InventoryPurchaseOrderItem',
			'foreignKey' => 'inventory_purchase_order_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		

	);

}