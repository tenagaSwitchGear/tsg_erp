<?php
App::uses('AppModel', 'Model');
/**
 * InventoryPurchaseRequisitionBom Model
 *
 * @property InventoryPurchaseRequisition $InventoryPurchaseRequisition
 * @property ProjectBom $ProjectBom
 */
class InventoryPurchaseRequisitionBom extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'inventory_purchase_requisition_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'project_bom_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryPurchaseRequisition' => array(
			'className' => 'InventoryPurchaseRequisition',
			'foreignKey' => 'inventory_purchase_requisition_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProjectBom' => array(
			'className' => 'ProjectBom',
			'foreignKey' => 'project_bom_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
