<?php
App::uses('AppModel', 'Model');
/**
 * FinishedGoodDeliveryItem Model
 *
 * @property FinishedGoodDelivery $FinishedGoodDelivery
 * @property FinishedGood $FinishedGood
 * @property SaleOrderItem $SaleOrderItem
 * @property SaleQuotationItem $SaleQuotationItem
 * @property InventoryItem $InventoryItem
 * @property User $User
 */
class FinishedGoodDeliveryItem extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'FinishedGoodDelivery' => array(
			'className' => 'FinishedGoodDelivery',
			'foreignKey' => 'finished_good_delivery_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'FinishedGood' => array(
			'className' => 'FinishedGood',
			'foreignKey' => 'finished_good_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleOrderItem' => array(
			'className' => 'SaleOrderItem',
			'foreignKey' => 'sale_order_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleQuotationItem' => array(
			'className' => 'SaleQuotationItem',
			'foreignKey' => 'sale_quotation_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
