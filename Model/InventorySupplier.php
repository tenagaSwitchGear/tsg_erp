<?php
App::uses('AppModel', 'Model');
/**
 * InventorySupplier Model
 *
 * @property State $State
 * @property Country $Country
 * @property InventoryDeliveryOrder $InventoryDeliveryOrder
 * @property InventoryPurchaseOrder $InventoryPurchaseOrder
 */
class InventorySupplier extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array( 
		'name' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Company Name.',
			),
			/*'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This Company Name already exists.',
			) */
		),
		'address' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Address.',
			) 
		),
		'city' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill City.',
			) 
		),
		'postcode' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Postcode.',
			) 
		),
		'phone_office' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Phone Number.',
			) 
		),
		'pic' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill PIC.',
			) 
		),
		'email' => array( 
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill Company Email.',
			) 
		)    
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'State' => array(
			'className' => 'State',
			'foreignKey' => 'state_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralCurrency' => array(
			'className' => 'GeneralCurrency',
			'foreignKey' => 'general_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TermOfPayment' => array(
			'className' => 'TermOfPayment',
			'foreignKey' => 'term_of_payment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryDeliveryOrder' => array(
			'className' => 'InventoryDeliveryOrder',
			'foreignKey' => 'inventory_supplier_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventoryPurchaseOrder' => array(
			'className' => 'InventoryPurchaseOrder',
			'foreignKey' => 'inventory_supplier_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventorySupplierContact' => array(
			'className' => 'InventorySupplierContact',
			'foreignKey' => 'inventory_supplier_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventorySupplierAddress' => array(
			'className' => 'InventorySupplierAddress',
			'foreignKey' => 'inventory_supplier_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventorySupplierFile' => array(
			'className' => 'InventorySupplierFile',
			'foreignKey' => 'inventory_supplier_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InventorySupplierCategory' => array(
			'className' => 'InventorySupplierCategory',
			'foreignKey' => 'inventory_supplier_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
