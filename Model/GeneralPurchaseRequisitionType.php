<?php
App::uses('AppModel', 'Model');
/**
 * GeneralPurchaseRequisitionType Model
 *
 * @property InventoryPurchaseRequisition $InventoryPurchaseRequisition
 */
class GeneralPurchaseRequisitionType extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'InventoryPurchaseRequisition' => array(
			'className' => 'InventoryPurchaseRequisition',
			'foreignKey' => 'general_purchase_requisition_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
