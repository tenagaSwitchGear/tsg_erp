<?php
App::uses('AppModel', 'Model');
/**
 * PlanningItem Model
 *
 * @property Planning $Planning
 * @property InventoryItem $InventoryItem
 * @property GeneralUnit $GeneralUnit
 * @property InventorySupplier $InventorySupplier
 */
class PlanningCaptureItem extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PlanningCapture' => array(
			'className' => 'PlanningCapture',
			'foreignKey' => 'planning_capture_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'GeneralUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'general_unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),  
		'PlanningUnit' => array(
			'className' => 'GeneralUnit',
			'foreignKey' => 'planning_general_unit',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplier' => array(
			'className' => 'InventorySupplier',
			'foreignKey' => 'inventory_supplier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventorySupplierItem' => array(
			'className' => 'InventorySupplierItem',
			'foreignKey' => 'inventory_supplier_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
 
}
