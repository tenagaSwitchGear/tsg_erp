<?php

App::uses('AppModel', 'Model');

class Payment extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'image' => array(
                'fields' => array(
                    'dir' => 'file_dir'
                )
            )
        )
    );

	public $validate = array( 
		'username' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please enter Username.', 
			),
		), 
		'email' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please Fill Email.', 
			), 
			'email' => array(
        		'rule' => array('email', true),
        		'message' => 'Please supply a valid email address.'
   			 ), 
		), 
		'mobile_number' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please fill phone number.', 
			), 
		), 
		'payment_date' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please enter Payment Date.', 
			),
		), 
		'payment_time' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please enter Payment Time.', 
			),
		), 
		'image' => array(
			'image' => array(
				
				'rule' => array('isValidMimeType', array('application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/gif'), false),
    			'message' => 'Invalid file format. Only PDF, JPG, JPEG, GIF & PNG are allow.',
				
				'allowEmpty' =>  array('allowEmpty'=>true)
				//'message' => 'Please supply a valid image.',
			),
		),
	);
}

