<?php
App::uses('AppModel', 'Model');
/**
 * ProductionOrderChild Model
 *
 * @property ProductionOrder $ProductionOrder
 */
class FinishedGood extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'finished_goods';
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProductionOrder' => array(
			'className' => 'ProductionOrder',
			'foreignKey' => 'production_order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'SaleJobChild' => array(
			'className' => 'SaleJobChild',
			'foreignKey' => 'sale_job_child_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'SaleJobItem' => array(
			'className' => 'SaleJobItem',
			'foreignKey' => 'sale_job_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'FinishedGoodRemark' => array(
			'className' => 'FinishedGoodRemark',
			'foreignKey' => 'finished_good_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'FinishedGoodFatRemark' => array(
			'className' => 'FinishedGoodFatRemark',
			'foreignKey' => 'finished_good_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
