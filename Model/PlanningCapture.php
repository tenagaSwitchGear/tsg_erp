<?php
App::uses('AppModel', 'Model');
/**
 * PlanningItem Model
 *
 * @property Planning $Planning
 * @property InventoryItem $InventoryItem
 * @property GeneralUnit $GeneralUnit
 * @property InventorySupplier $InventorySupplier
 */
class PlanningCapture extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(  
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
 
}
