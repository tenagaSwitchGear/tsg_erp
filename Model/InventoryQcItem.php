<?php
App::uses('AppModel', 'Model');
/**
 * InventoryDeliveryOrder Model
 *
 * @property InventorySupplier $InventorySupplier
 * @property InventoryPurchaseOrder $InventoryPurchaseOrder
 * @property User $User
 * @property GeneralPaymentTerm $GeneralPaymentTerm
 * @property InventoryDeliveryOrderItem $InventoryDeliveryOrderItem
 * @property InventoryPayment $InventoryPayment
 */
class InventoryQcItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryDeliveryOrderItem' => array(
			'className' => 'InventoryDeliveryOrderItem',
			'foreignKey' => 'inventory_delivery_order_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'InventoryItem' => array(
			'className' => 'InventoryItem',
			'foreignKey' => 'inventory_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
