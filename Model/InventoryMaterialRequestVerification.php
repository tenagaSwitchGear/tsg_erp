<?php
App::uses('AppModel', 'Model');
/**
 * InventoryMaterialRequestVerification Model
 *
 * @property InventoryMaterialRequest $InventoryMaterialRequest
 * @property User $User
 */
class InventoryMaterialRequestVerification extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'InventoryMaterialRequest' => array(
			'className' => 'InventoryMaterialRequest',
			'foreignKey' => 'inventory_material_request_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
