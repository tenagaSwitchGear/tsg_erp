<?php
App::uses('AppModel', 'Model');
/**
<<<<<<< HEAD
 * InternalDepartment Model
 *
 * @property User $User
 * @property InventoryPurchaseRequisition $InventoryPurchaseRequisition
=======
 * Group Model
 *
 * @property User $User
 * 
 * 
 */
class InternalDepartment extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array( 
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/** 
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/** 
 * hasMany associations
 *
 * @var array
 
	public $hasMany = array(
		'InventoryPurchaseRequisition' => array(
			'className' => 'InventoryPurchaseRequisition',
			'foreignKey' => 'internal_department_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
 */
	public function findHodByGroupId($group_id) {
		$this->find('first', array(
			'conditions' => array(
				'InternalDepartment.group_id' => $group_id
				)
			));
	}  
	 

}
